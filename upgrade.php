<?php

require_once 'application/modules/default/library/sapp/Global.php';
require_once 'public/db_upgrade_constant.php';

if(!empty($_POST))
{
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>XNet</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
     <link href="public/media/css/successstyle.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,300,300italic,100italic,100,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="public/media/jquery/js/jquery-1.7.1.min.js"></script> 
	<script type="text/javascript" src="public/media/jquery/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="public/media/jquery/js/jquery-ui-1.10.3.custom.js"></script>
	<script type="text/javascript" src="public/media/js/hrmsv2.js"></script>
	<script type="text/javascript" src="public/media/jquery/js/jquery.blockUI_2.64.js"></script>
   
	
</head>
  <body>
  <script>
    <?php  if(DB_UPGRADE_STATUS != 'UPGRADING') {?>
						$.ajax({
							url: "upgrade/index.php",
							data: { codeversion: '<?php echo $_POST['codeversion']?>' },
							method: "POST",
							dataType: "json",
							success : function()
							{
								reload();
							},
							error: function(){
								reload();
						 	}
						});
						
					
	<?php }?>
  </script>
      <div class="container" >
     
      	<div class="header"> <div class="logo"></div></div>
        
        <div class="content_wrapper" style="min-height:91px;">
          		<div class="db_upgrade_mess">Please wait while database is getting updated. This may take few minutes.</div>
        </div>
      </div>
  </body>
</html>
<?php }else{
header("Location: index.php");	
}?>
<script>
function reload() {
	window.location = ""
	}
</script>