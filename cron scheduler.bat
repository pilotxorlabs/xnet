echo executing cron commands..
echo off

set Pathname="C:\xampp\htdocs\xnet"

schtasks /query /TN "empdocsexpiry" >NUL 2>&1
    
if %errorlevel% NEQ 0 (
	SchTasks /Create /SC DAILY /TN "empdocsexpiry" /TR "C:\xampp\php\php.exe  %Pathname%\LaunchCron.php empdocsexpiry" /ST 08:00
) ELSE echo ! Cron already exist !

schtasks /query /TN "empexpiry" >NUL 2>&1
    
if %errorlevel% NEQ 0 (
	SchTasks /Create /SC DAILY /TN "empexpiry" /TR "C:\xampp\php\php.exe  %Pathname%\LaunchCron.php empexpiry" /ST 08:00
) ELSE echo ! Cron already exist !

schtasks /query /TN "leaveapprove" >NUL 2>&1
    
if %errorlevel% NEQ 0 (
	SchTasks /Create /SC WEEKLY /D MON /TN "leaveapprove" /TR "C:\xampp\php\php.exe  %Pathname%\LaunchCron.php leaveapprove" /ST 08:00
) ELSE echo ! Cron already exist !

schtasks /query /TN "requisition" >NUL 2>&1
    
if %errorlevel% NEQ 0 (
	SchTasks /Create /SC DAILY /TN "requisition" /TR "C:\xampp\php\php.exe  %Pathname%\LaunchCron.php requisition" /ST 08:00
) ELSE echo ! Cron already exist !

schtasks /query /TN "inactiveusers" >NUL 2>&1
    
if %errorlevel% NEQ 0 (
	SchTasks /Create /SC DAILY /TN "inactiveusers" /TR "C:\xampp\php\php.exe  %Pathname%\LaunchCron.php inactiveusers" /ST 08:00
) ELSE echo ! Cron already exist !

schtasks /query /TN "logcron" >NUL 2>&1
    
if %errorlevel% NEQ 0 (
	SchTasks /Create /SC DAILY /TN "logcron" /TR "C:\xampp\php\php.exe  %Pathname%\LaunchCron.php logcron" /ST 08:00
) ELSE echo ! Cron already exist !

schtasks /query /TN "expireexception" >NUL 2>&1
    
if %errorlevel% NEQ 0 (
	SchTasks /Create /SC DAILY /TN "expireexception" /TR "C:\xampp\php\php.exe  %Pathname%\LaunchCron.php expireexception" /ST 08:00
) ELSE echo ! Cron already exist !

schtasks /query /TN "updateleaves" >NUL 2>&1
    
if %errorlevel% NEQ 0 (
	SchTasks /Create /SC DAILY /TN "updateleaves" /TR "C:\xampp\php\php.exe  %Pathname%\LaunchCron.php updateleaves" /ST 08:00
) ELSE echo ! Cron already exist !

schtasks /query /TN "worklogsheetreminder" >NUL 2>&1
    
if %errorlevel% NEQ 0 (
	SchTasks /Create /SC DAILY /TN "worklogsheetreminder" /TR "C:\xampp\php\php.exe  %Pathname%\LaunchCron.php worklogsheetreminder" /ST 08:00
) ELSE echo ! Cron already exist !

schtasks /query /TN "approvedleavesreminder" >NUL 2>&1
    
if %errorlevel% NEQ 0 (
	SchTasks /Create /SC DAILY /TN "approvedleavesreminder" /TR "C:\xampp\php\php.exe  %Pathname%\LaunchCron.php approvedleavesreminder" /ST 08:00
) ELSE echo ! Cron already exist !

schtasks /query /TN "noc" >NUL 2>&1
    
if %errorlevel% NEQ 0 (
	SchTasks /Create /SC DAILY /TN "noc" /TR "C:\xampp\php\php.exe  %Pathname%\LaunchCron.php noc" /ST 08:00
) ELSE echo ! Cron already exist !


schtasks /query /TN "allprobationcomplete" >NUL 2>&1
    
if %errorlevel% NEQ 0 (
	SchTasks /Create /SC DAILY /TN "allprobationcomplete" /TR "C:\xampp\php\php.exe  %Pathname%\LaunchCron.php allprobationcomplete" /ST 08:00
) ELSE echo ! Cron already exist !

schtasks /query /TN "NotifyProbationEnd" >NUL 2>&1
    
if %errorlevel% NEQ 0 (
	SchTasks /Create /SC DAILY /TN "NotifyProbationEnd" /TR "C:\xampp\php\php.exe %Pathname%\LaunchCron.php  notifyprobationend" /ST 08:00
) ELSE echo ! Cron already exist !

echo all Crons are scheduled.
