<?php
/********************************************************************************* 


 *   

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/

/**
 * Breadcrumbs View Helper
 *
 * A View Helper that creates the menu
 */
class Zend_View_Helper_Breadcrumbs extends Zend_View_Helper_Abstract {
	public function breadcrumbs($baseUrlString = '') {
		$request = Zend_Controller_Front::getInstance ();
		
		$params = $request->getRequest ()->getParams ();
		$controllerName = $request->getRequest ()->getControllerName ();
		$action_Name = $request->getRequest ()->getActionName ();
		
		$auth = Zend_Auth::getInstance ();
		$loginuserRole = '';
		$loginuserGroup = '';
		if ($auth->hasIdentity ()) {
			$loginuserRole = $auth->getStorage ()->read ()->emprole;
			$loginuserGroup = $auth->getStorage ()->read ()->group_id;
		}
		
		$tName = '';
		$vName = '';
		$tUrl = '';
		$serviceUrl = '';
		
		$burl = $controllerName . "/" . $action_Name;
		
		/**
		 *
		 * For service request modifying the breadcrum based on t and v params
		 *
		 * @var t and @var v
		 */
		$param_t = isset ( $params ['t'] ) ? sapp_Global::_decrypt ( $params ['t'] ) : "";
		$param_v = isset ( $params ['v'] ) ? sapp_Global::_decrypt ( $params ['v'] ) : "";
		$service_menu = sapp_Helper::sd_menu_names ();
		$service_action_arr = sapp_Helper::sd_action_names ();
		if ($param_t != '' && isset ( $service_menu [$param_t] )) {
			$tName = $service_menu [$param_t] . ' Summary';
			$tUrl = $baseUrlString . '/' . $controllerName . '/index/t/' . sapp_Global::_encrypt ( $param_t );
		}
		
		if ($param_v != '' && isset ( $service_action_arr [$param_v] ))
			$vName = $service_action_arr [$param_v];
		else {
			
			$vName = ($action_Name != 'index' ? $action_Name : '');
		}
		
		if ($vName != '') {
			if ($tName != '')
				$serviceUrl = '<li><a href=' . $tUrl . '>' . $tName . '</a></li>';
			
			$serviceUrl .= '<li><a>' . ucfirst ( $vName ) . '</a></li>';
		} else {
			$serviceUrl = '<li><a>' . $tName . '</a></li>';
		}
		
		/**
		 * End modifying breadcrum for servicerequest.
		 */
		
		unset ( $params ['module'], $params ['controller'], $params ['action'] );
		if (isset ( $params ['error_handler'] ))
			unset ( $params ['error_handler'] );
		
		$id_name = '';
		if (is_array ( $params ) && ! empty ( $params )) {
			foreach ( $params as $key => $value ) {
				if (! is_array ( $value )) {
					$burl .= "/" . $key . "/" . $value;
				}
			}
			$id_name = "yes";
		}
		$pageUrl = explode ( "/", $_SERVER ['REQUEST_URI'] );
		
		$serverUrl = $_SERVER ['HTTP_HOST'];
		/*
		 * $reportsArr = array('leavesreport'=>'Leaves','holidaygroupreports'=>'Holidays','activeuser'=>'Active Users',
		 * 'employeereport'=>'Employees','rolesgroup'=>'Roles','emprolesgroup'=>'Employee Roles',
		 * 'userlogreport'=>'User Logs','activitylogreport'=>'Activity Logs','requisitionstatusreport'=>'Requisition','performancereport' => 'Year Wise'
		 * );
		 */
		
		$pageName = $controllerName;
		$actionName = $action_Name;
		$breadCrumbsData = '';
		
		$mydetails_arr = array (
				'probation' => 'Probation Details',
				'jobhistory' => 'Employee Job History',
				'certification' => 'Training & Certification Details',
				'experience' => 'Experience Details',
				'education' => 'Education Details',
				'medicalclaims' => 'Medical Claims',
				'leaves' => 'Employee Leaves',
				'skills' => 'Employee Skills',
				'communication' => 'Contact Details',
				'communicationdetailsview' => 'Contact Details',
				'disability' => 'Disability Details',
				'disabilitydetailsview' => 'Disability Details',
				'workeligibility' => 'Work Eligibility Details',
				'workeligibilitydetailsview' => 'Work Eligibility Details',
				'visa' => 'Visa and Immigration Details',
				'visadetailsview' => 'Visa and Immigration Details',
				'additionaldetails' => 'Additional Details',
				'additionaldetailsview' => 'Additional Details',
				'salarydetails' => 'Salary Details',
				'salarydetailsview' => 'Salary Details',
				'personal' => 'Personal Details',
				'personaldetailsview' => 'Personal Details',
				'creditcard' => 'Corporate Card Details',
				'creditcarddetailsview' => 'Corporate Card Details',
				'dependency' => 'Dependency Details',
				'edit' => 'Edit' 
		);
		
		$myemployees_arr = array (
				'additionaldetailsview' => 'Additional Details',
				'jobhistoryview' => 'Job History',
				'perview' => 'Personal Details',
				'expview' => 'Experience Details',
				'prbview' => 'Probation Details',
				'eduview' => 'Education Details',
				'skillsview' => 'Employee Skills',
				'comview' => 'Contact Details',
				'trainingview' => 'Training & Certification Details',
				'view' => 'View',
				'employeereport' => 'My Team Report' 
		);
		
		$myemployeesedit_arr = array (
				'additionaldetailsedit' => 'Additional Details',
				'jobhistoryedit' => 'Job History',
				'peredit' => 'Personal3 Details',
				'expedit' => 'Experience Details',
				'prbedit' => 'Probation Details',
				'eduedit' => 'Education Details',
				'skillsedit' => 'Employee Skills',
				'comedit' => 'Contact Details',
				'trainingedit' => 'Training & Certification Details',
				'edit' => 'Edit' 
		);
		
		$dashboard_actions = array (
				'viewsettings' => $this->dashboard_actions_html ( $baseUrlString, 'Settings' ),
				'viewprofile' => $this->dashboard_actions_html ( $baseUrlString, 'Profile' ),
				'changepassword' => $this->dashboard_actions_html ( $baseUrlString, 'Change Password' ),
				'emailsettings' => $this->dashboard_actions_html ( $baseUrlString, 'Email Settings' ),
				'upgradeapplication' => $this->dashboard_actions_html ( $baseUrlString, 'Upgrade Application' ) 
		);
		
		$emptabarr = array (
				'dependencydetails',
				'creditcarddetails',
				'visaandimmigrationdetails',
				'workeligibilitydetails',
				'disabilitydetails',
				'empcommunicationdetails',
				'empskills',
				'empleaves',
				'empholidays',
				'medicalclaims',
				'educationdetails',
				'experiencedetails',
				'probationdetails',
				'trainingandcertificationdetails',
				'emppersonaldetails',
				'empperformanceappraisal',
				'emppayslips',
				'empbenefits',
				'emprenumerationdetails',
				'emprequisitiondetails',
				'empadditionaldetails',
				'empsecuritycredentials',
				'empsalarydetails',
				'empjobhistory',
				'employeedocs',
				'empremunerationdetails' 
		);
		
		if ($pageName == '' || $pageName == 'welcome') {
			$breadCrumbsData .= '';
		} else if ($pageName == 'dashboard') {
    		$breadCrumbsData = '<div class="breadcrumbs">';
    		if($actionName == 'viewprofile'){
    		    $breadCrumbsData .= '<ul><li><a href="' . $baseUrlString . '">Home</a></li><li><a>View Profile</a></li></ul>';
    		}
    		else if($actionName == 'changepassword'){
    		    $breadCrumbsData .= '<ul><li><a href="' . $baseUrlString . '">Home</a></li><li><a>Change Password</a></li></ul>';
    		}
    		else if($actionName == 'emailsettings'){
    		    $breadCrumbsData .= '<ul><li><a href="' . $baseUrlString . '">Home</a></li><li><a>Email Settings</a></li></ul>';
    		}
    		else{
    		    $breadCrumbsData .= $dashboard_actions [$actionName];
    		}
    		$breadCrumbsData .= '</div>';
		} else if ($pageName == 'configuresite') {
			
			$breadCrumbsData .= $this->menu_home_html ( $baseUrlString, 'Configure Site' );
		} else if ($pageName == 'managemenus') {
			$breadCrumbsData .= $this->menu_home_html ( $baseUrlString, 'Modules' );
		} else if ($pageName == 'logmanager') {
			$breadCrumbsData .= $this->menu_home_html ( $baseUrlString, 'Activity Log' );
		} else if ($pageName == 'userloginlog') {
			$breadCrumbsData .= $this->menu_home_html ( $baseUrlString, 'User Log' );
		} else if ($pageName == 'servicerequests') {
			$breadCrumbsData = '<div class="breadcrumbs">';
			$breadCrumbsData .= '<ul><li><a href="' . $baseUrlString . '">Home</a></li><li><a>Service Request</a></li>' . $serviceUrl . '</ul>';
			$breadCrumbsData .= '</div>';
		} else if ($pageName == 'reports') {
			$breadCrumbsData = '<div class="breadcrumbs">';
			$breadCrumbsData .= '<ul><li><a href="' . $baseUrlString . '">Home</a></li>';
			
			if (isset ( $actionName ) && $actionName != '') {
				$breadCrumbsData .= '<li><a href="' . $baseUrlString . '/reports">Analytics</a></li>';
				
				if ($actionName == 'userlogreport')
					$breadCrumbsData .= '<li><a>Audit Logs</a></li><li><a>User log Report</a></li>';
				else if ($actionName == 'activitylogreport')
					$breadCrumbsData .= '<li><a>Audit Logs</a></li><li><a>Activity log Report</a></li>';
				else if ($actionName == 'businessunits')
					$breadCrumbsData .= '<li><a>Organization</a></li><li><a>Business Units Report</a></li>';
				else if ($actionName == 'departments')
					$breadCrumbsData .= '<li><a>Organization</a></li><li><a>Departments Report</a></li>';
				else if ($actionName == 'leavesreport')
					$breadCrumbsData .= '<li><a>Leave Management</a></li><li><a>Employee Leaves Summary Report</a></li>';
				else if ($actionName == 'leavemanagementreport')
					$breadCrumbsData .= '<li><a>Leave Management</a></li><li><a>Leave Management Summary Report</a></li>';
				else if ($actionName == 'holidaygroupreports')
					$breadCrumbsData .= '<li><a>Holiday Management</a></li><li><a>Holiday Groups & Holidays Report</a></li>';
				else if ($actionName == 'employeereport')
					$breadCrumbsData .= '<li><a>Employees</a></li><li><a>Employees Report</a></li>';
				else if ($actionName == 'rolesgroup')
					$breadCrumbsData .= '<li><a>User Management</a></li><li><a>Groups & Roles Report</a></li>';
				else if ($actionName == 'emprolesgroup')
					$breadCrumbsData .= '<li><a>User Management</a></li><li><a>Groups, Roles & Employees Report</a></li>';
				else if ($actionName == 'activeuser')
					$breadCrumbsData .= '<li><a>User Management</a></li><li><a>Users & Employees Report</a></li>';
				else if ($actionName == 'requisitionstatusreport')
					$breadCrumbsData .= '<li><a>Recruitments</a></li><li><a>Recruitments Report</a></li>';
				else if ($actionName == 'candidatesreport')
					$breadCrumbsData .= '<li><a>Recruitments</a></li><li><a>Candidate Details Report</a></li>';
				else if ($actionName == 'interviewrounds')
					$breadCrumbsData .= '<li><a>Recruitments</a></li><li><a>Scheduled Interviews Report</a></li>';
				else if ($actionName == 'empscreening')
					$breadCrumbsData .= '<li><a>Background Check</a></li><li><a>Employee / Candidate Screening Report</a></li>';
				else if ($actionName == 'agencylistreport')
					$breadCrumbsData .= '<li><a>Background Check</a></li><li><a>Background Check Agencies Report</a></li>';
				else if ($actionName == 'performancereport')
					$breadCrumbsData .= '<li><a>Performance Appraisal</a></li><li><a>Employee Appraisals Report</a></li>';
				else if ($actionName == 'previousappraisals')
					$breadCrumbsData .= '<li><a>Performance Appraisal</a></li><li><a>Employee Appraisals</a></li>';
				else if ($actionName == 'servicedeskreport')
					$breadCrumbsData .= '<li><a>Service Request</a></li><li><a>Service Request Report</a></li>';
			} else
				$breadCrumbsData .= '<li><a>Analytics</a></li>';
			$breadCrumbsData .= '</ul></div>';
		} else if ($pageName == 'employee' && $actionName == 'changeorghead') {
			$breadCrumbsData = '<div class="breadcrumbs">';
			$breadCrumbsData .= '<ul><li><a href="' . $baseUrlString . '">Home</a></li><li><a>Human Resource</a></li><li><a href="' . $baseUrlString . '/employee">Employees</a></li><li><a>Manage Organization Head</a></li>';
			$breadCrumbsData .= '</ul></div>';
		} else if ($pageName == 'appraisalstatus') {
			$breadCrumbsData = '<div class="breadcrumbs">';
			if ($actionName == 'manager')
				$breadCrumbsData .= '<ul><li><a href="' . $baseUrlString . '">Home</a></li><li><a>Performance Appraisal</a></li><li><a href="' . $baseUrlString . '/appraisalstatus/manager">Manager Status</a></li>';
			else
				$breadCrumbsData .= '<ul><li><a href="' . $baseUrlString . '">Home</a></li><li><a>Performance Appraisal</a></li><li><a href="' . $baseUrlString . '/appraisalstatus/employee">Employee Status</a></li>';
			$breadCrumbsData .= '</ul></div>';
		} else if (in_array ( $pageName, $emptabarr )) {
			$breadCrumbsData = '<div class="breadcrumbs">';
			$breadCrumbsData .= '<ul><li><a href="' . $baseUrlString . '">Home</a></li><li><a>Human Resource</a></li><li><a href="' . $baseUrlString . '/employee">Employees</a></li>';
			if ($pageName == 'employeedocs')
				$breadCrumbsData .= '<li><a>Documents</a></li>';
			else
				$breadCrumbsData .= '<li><a>' . ucfirst ( $actionName ) . '</a></li>';
			$breadCrumbsData .= '</ul></div>';
		} else {
			$breadCrumbsData = '<div class="breadcrumbs">';
			$url = "/" . $pageName;
			if ($pageName == 'expensecategories' || $pageName == 'expenses' || $pageName == 'paymentmode' || $pageName == 'receipts' || $pageName == 'trips' || $pageName == 'advances' || $pageName == 'employeeadvances' || $pageName == 'myemployeeexpenses') {
				
				$url = "/expenses/" . $pageName;
				if ($pageName == 'advances')
					$url = "/expenses/" . $pageName . "/myadvances";
			}
			if ($pageName == 'assetcategories' || $pageName == 'assets') {
				$url = "/assets/" . $pageName;
			}
			if ($pageName == 'allexitproc' || $pageName == 'exitproc' || $pageName == 'exitprocsettings' || $pageName == 'exittypes' || $pageName == 'configureexitqs') {
				$url = "/exit/" . $pageName;
			}
			if ($pageName == 'employeerequests') {
				$url = "/" . $pageName . "";
			}
			$breadCrumIds = $this->getBreadCrumDetails ( $url );
			
			$breadCrumNames = array ();
			if (! empty ( $breadCrumIds )) {
				
				$menu_model = new Default_Model_Menu ();
				
				$breadCrumbsData .= '<ul><li><a class="firstbreadcrumb" href=\'#\' onclick="window.location=\'' . $baseUrlString . '\'">Home</a></li>';
				$breadCrumIds ['nav_ids_arr'] = array_merge ( $breadCrumIds ['nav_ids_arr'] );
				
				for($b = 0; $b < sizeof ( $breadCrumIds ['nav_ids_arr'] ); $b ++) {
					
					$loop_menu_name = $breadCrumIds ['menu_arr'] [$breadCrumIds ['nav_ids_arr'] [$b]] ['menuName'];
					$loop_menu_url = $breadCrumIds ['menu_arr'] [$breadCrumIds ['nav_ids_arr'] [$b]] ['url'];
					$loop_menu_name = $menu_model->getMenuText ( $loop_menu_name );
					
					if ($b == 0) {
						if ($loop_menu_url == '/sitepreference') {
							$breadCrumbsData .= '<li><a>' . $loop_menu_name . '</a></li>';
						} else if (! empty ( $loop_menu_name )) {
							$breadCrumbsData .= '<li><a>' . $loop_menu_name . '</a></li> ';
						}
					} else if ($b == (sizeof ( $breadCrumIds ['nav_ids_arr'] ) - 1)) {
						if ($actionName == '') {
							$breadCrumbsData .= '<li><a>' . $loop_menu_name . '</a></li>';
						} else if (! empty ( $loop_menu_name )) {
							$breadCrumbsData .= '<li><a href="' . $baseUrlString . $loop_menu_url . '" >' . $loop_menu_name . '</a></li>';
						}
					} else if (! empty ( $loop_menu_name )) {
						$breadCrumbsData .= '<li><a>' . $loop_menu_name . '</a></li>';
					}
				}
				
				if ((($actionName == 'add' || ($actionName == 'edit' && $id_name == '')) || ($actionName != '' && $actionName != 'view')) && $id_name != 'pd') {
					if ($actionName == 'edit' || $actionName != '') {
						$idvalindex = '';
						if (in_array ( 'id', $pageUrl )) {
							$idindex = array_search ( 'id', $pageUrl );
							$idvalindex = $idindex + 1;
						} else if (in_array ( 'userid', $pageUrl )) {
							$idindex = array_search ( 'userid', $pageUrl );
							$idvalindex = $idindex + 1;
						}
						
						if ((in_array ( 'id', $pageUrl ) || in_array ( 'userid', $pageUrl )) && $pageName != 'myemployees') {
							$idval = intval ( $pageUrl [$idvalindex] );
							
							if ($idval != 0 || $pageUrl [$idvalindex] != '')
								$breadCrumbsData .= '<li><a>Edit</a></li>';
							else
								
								$breadCrumbsData .= '<li><a>Add</a></li>';
						} else {
							if ($pageName == 'mydetails') {
								if (isset ( $actionName ) && $actionName != '') {
									if (array_key_exists ( $actionName, $mydetails_arr ) !== false)
										$breadCrumbsData .= '<li><a>' . $mydetails_arr [$actionName] . '</a></li>';
								} else {
									$breadCrumbsData .= '<li><a>Edit</a></li>';
								}
							} else if ($pageName == 'myemployees') {
								if (isset ( $actionName ) && $actionName != '') {
									if (array_key_exists ( $actionName, $myemployees_arr ) !== false)
										$breadCrumbsData .= '<li><a>' . $myemployees_arr [$actionName] . '</a></li>';
									else if (array_key_exists ( $actionName, $myemployeesedit_arr ) !== false)
										$breadCrumbsData .= '<li><a>' . $myemployeesedit_arr [$actionName] . '</a></li>';
									else if ($actionName == 'add')
										$breadCrumbsData .= '<li><a>Add</a></li>';
								} else {
									$breadCrumbsData .= '<li><a>View</a></li>';
								}
							} 

							else {
								if ($actionName == 'multipleresume')
									$breadCrumbsData .= '<li><a>Add multiple CVs</a></li>';
								if ($actionName == 'edit' && ($pageName == 'heirarchy' || $pageName == 'appraisalself'))
									$breadCrumbsData .= '<li><a>Edit</a></li>';
								else if ($actionName == 'edit' && ($pageName == 'empconfiguration'))
									$breadCrumbsData .= '<li><a>Edit</a></li>';
								else if ($actionName == 'edit' || $actionName == 'add')
									$breadCrumbsData .= '<li><a>Add</a></li>';
								else
									$breadCrumbsData .= '';
							}
						}
					} else {
						$breadCrumbsData .= '<li><a>Add</a></li>';
					}
				} else if ($actionName == 'edit') {
					$idvalindex = '';
					if (in_array ( 'id', $pageUrl )) {
						$idindex = array_search ( 'id', $pageUrl );
						$idvalindex = $idindex + 1;
					} else if (in_array ( 'userid', $pageUrl )) {
						$idindex = array_search ( 'userid', $pageUrl );
						$idvalindex = $idindex + 1;
					}
					if (in_array ( 'id', $pageUrl ) || in_array ( 'userid', $pageUrl )) {
						$idval = intval ( $pageUrl [$idvalindex] );
						if ($idval != '')
							$breadCrumbsData .= '<li><a>Edit</a></li>';
						else
							$breadCrumbsData .= '<li><a>Add</a></li>';
					} else
						$breadCrumbsData .= '<li><a>Add</a></li>';
				} else if ($actionName == 'view') {
					$breadCrumbsData .= '<li><a>View</a></li>';
				}
				$breadCrumbsData .= '</ul>';
			} else {
				$breadCrumbsData = '';
			}
		}
		/*
		 * $expense_data = '';
		 * if ($pageName == 'expensecategories' || $pageName == 'expenses' || $pageName == 'paymentmode' || $pageName == 'receipts' || $pageName == 'trips' || $pageName == 'advances' || $pageName == 'employeeadvances' || $pageName == 'myemployeeexpenses') {
		 * if ($loginuserRole == SUPERADMINROLE) {
		 * $expense_data = '<div class="dt_btn" style="margin: 0;position: absolute;right: 10px;top: 10px;">
		 * <a class="dropdown-button" href="#"data-activates="addbtna"><i class="fa fa-plus"></i> Add</a>
		 * <ul id="addbtna" class="dropdown-content" style="list-style: none; margin: 0; padding: 0;">
		 *
		 * <li><a href="' . BASE_URL . 'expenses/expensecategories/edit";>Category</a></li>
		 * <li><a href="' . BASE_URL . 'expenses/paymentmode/edit";>Payment Mode</a></li>
		 * </ul>
		 * </div>';
		 * } else if ($loginuserGroup == HR_GROUP) {
		 * $expense_data = '<div class="dt_btn" style="margin: 0;position: absolute;right: 10px;top: 10px;">
		 * <a class="dropdown-button" href="#"data-activates="addbtna"><i class="fa fa-plus"></i> Add</a>
		 * <ul id="addbtna" class="dropdown-content" style="list-style: none; margin: 0; padding: 0;">
		 * <li><a href="' . BASE_URL . 'expenses/expenses/edit";>Expense</a></li>
		 * <li><a href="' . BASE_URL . 'expenses/expenses/bulkexpenses";">Bulk Expenses</a></li>
		 * <li><a href="' . BASE_URL . 'expenses/expensecategories/edit";>Category</a></li>
		 * <li><a href="' . BASE_URL . 'expenses/paymentmode/edit";>Payment Mode</a></li>
		 * <li><a href="' . BASE_URL . 'expenses/receipts/";>Receipts</a></li>
		 * <li><a href="' . BASE_URL . 'expenses/trips/edit";>Trip</a></li>
		 * <li><a href="' . BASE_URL . 'expenses/employeeadvances/edit";>Advance</a></li>
		 *
		 *
		 * </ul>
		 * </div>';
		 * } else {
		 * $expense_data = '<div class="dt_btn" style="margin: 0;position: absolute;right: 10px;top: 10px;">
		 * <a class="dropdown-button" href="#"data-activates="addbtna"><i class="fa fa-plus"></i> Add</a>
		 * <ul id="addbtna" class="dropdown-content" style="list-style: none; margin: 0; padding: 0;">
		 * <li><a href="' . BASE_URL . 'expenses/expenses/edit";>Expense</a></li>
		 * <li><a href="' . BASE_URL . 'expenses/expenses/bulkexpenses";">Bulk Expenses</a></li>
		 * <li><a href="' . BASE_URL . 'expenses/receipts/";>Receipts</a></li>
		 * <li><a href="' . BASE_URL . 'expenses/trips/edit";>Trip</a></li>
		 * <li><a href="' . BASE_URL . 'expenses/employeeadvances/edit";>Advance</a></li>
		 * </ul>
		 * </div>';
		 * }
		 * }
		 */
		echo $breadCrumbsData;
	}
	public function dashboard_actions_html($base_url, $menu_name) {
		return '<a href="' . $base_url . '">Home</a>' . $menu_name;
	}
	public function menu_home_html($base_url, $menu_name) {
		if ($menu_name == 'Activity Log') {
			return '<div class="breadcrumbs"><ul><li><a href="' . $base_url . '">Home</a></li><li><a>Logs</a></li><li><a>' . $menu_name . '</a></li></div>';
		} else if ($menu_name == 'User Log') {
			return '<div class="breadcrumbs"><ul><li><a href="' . $base_url . '">Home</a></li><li><a>Logs</a></li><li><a>' . $menu_name . '</a></li></div>';
		}
		return '<div class="breadcrumbs"><ul><li><a href="' . $base_url . '">Home</a></li><li><a>' . $menu_name . '</a></li></div>';
	}
	public function getBreadCrumDetails($url) {
		$output = array ();
		$selectQuery = "select mm.nav_ids FROM main_menu mm where mm.url = '" . $url . "'";
		try {
			$menu_arr = $nav_ids_arr = array ();
			$db = Zend_Db_Table::getDefaultAdapter ();
			$sql = $db->query ( $selectQuery );
			$result = $sql->fetchAll ();
			if (! empty ( $result )) {
				$nav_ids = $result [0] ['nav_ids'];
				$nav_ids_arr = array_filter ( explode ( ',', $nav_ids ) );
				$query = "SELECT id,menuName,url,nav_ids FROM main_menu WHERE  FIND_IN_SET(id,'" . $result [0] ['nav_ids'] . "') ;";
				$breadcrumbsData = $db->query ( $query );
				$breadcrumbnames = $breadcrumbsData->fetchAll ();
				if (count ( $breadcrumbnames ) > 0) {
					foreach ( $breadcrumbnames as $bdata ) {
						$menu_arr [$bdata ['id']] ['menuName'] = $bdata ['menuName'];
						$menu_arr [$bdata ['id']] ['url'] = $bdata ['url'];
					}
				}
				$output = array (
						'nav_ids_arr' => $nav_ids_arr,
						'menu_arr' => $menu_arr 
				);
			}
		} catch ( Exception $e ) {
		}
		return $output;
	}
}
?>