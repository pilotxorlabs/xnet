<?php
class Default_Form_configmandatoryhours extends Zend_Form
{
	public function init(){

		$working_hours = new Zend_Form_Element_Select('duration');
		$working_hours->setMultiOptions(array(
				'1'=>'Half Day' ,
				'2'=>'Daily',
				'3'=>'Weekly' ,
				'4'=>'Monthly' ,
				'5'=>'Quarterly' ,
				'6'=>'Half Yearly' ,
				'7'=>'Yearly' ,
		));
		$working_hours->setRegisterInArrayValidator(false);
		$working_hours->setRequired(true);

		$min_hours = new Zend_Form_Element_Text('minimum_hours');
		$min_hours->setAttrib('maxLength', 3);
		$min_hours->addFilter(new Zend_Filter_StringTrim());
		$min_hours->setRequired(true);
		$min_hours->addValidator('NotEmpty', false, array('messages' => 'Please enter number of hours.'));
		$min_hours->addValidator("regex",true,array(
				'pattern'=>'/^[0-9]+$/',

				'messages'=>array(
						'regexNotMatch'=>'Please enter only numbers.'
				)
		));

		$minhours_action = new Zend_Form_Element_Select('minimum_hours_action');
		$minhours_action->setMultiOptions(array(
				'1'=>'Absent' ,
				'2'=>'Half Day',
				'3'=>'Ignore' ,
		));

		$minhours_action->setRegisterInArrayValidator(false);
		$minhours_action->setRequired(true);

		$max_hours = new Zend_Form_Element_Text('maximum_hours');
		$max_hours->setAttrib('maxLength', 3);
		$max_hours->addFilter(new Zend_Filter_StringTrim());
		$max_hours->setRequired(true);
		$max_hours->addValidator('NotEmpty', false, array('messages' => 'Please enter number of hours.'));
		$max_hours->addValidator("regex",true,array(
				'pattern'=>'/^[0-9]+$/',

				'messages'=>array(
						'regexNotMatch'=>'Please enter only numbers.'
				)
		));


		$maxhours_action = new Zend_Form_Element_Select('maximum_hours_action');
		$maxhours_action->setMultiOptions(array(
				'1'=>'Absent' ,
				'2'=>'Half Day',
				'3'=>'Ignore' ,
		));
		$maxhours_action->setRegisterInArrayValidator(false);
		$maxhours_action->setRequired(true);

        $submit = new Zend_Form_Element_Button('submit');
		$submit->setAttrib('id', 'submitbutton');
		$submit->setAttrib('style', 'margin-right:10px');
		$submit->setLabel('Save');

		$this->addElements(array($working_hours,$min_hours,$minhours_action,$max_hours,$maxhours_action,$submit));
        $this->setElementDecorators(array('ViewHelper'));
	}
}