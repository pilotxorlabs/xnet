<?php
/********************************************************************************* 

 *  Copyright (C) 2014 Sapplica
 *   

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/

class Default_Form_Probationdetails extends Zend_Form
{ 
	public function init()
	{
		
		$this->setMethod('post');		
        $this->setAttrib('id', 'formid');
        $this->setAttrib('name','probationdetails');
        
		
        $id = new Zend_Form_Element_Hidden('id');
		$user_id = new Zend_Form_Element_Hidden('user_id');
		 
		//$employeeName = new Zend_Form_Element_Text('employeeName');
		
		$employeeName = new Zend_Form_Element_Text("employeeName");
		$employeeName->setLabel("Employee Name");
		$employeeName->setAttrib("class", "formDataElement");
		$employeeName->setAttrib("readonly", "readonly");
		
		$employeeId = new Zend_Form_Element_Text("employeeId");
		$employeeId->setLabel("Employee Id");
		$employeeId->setAttrib("class", "formDataElement");
		$employeeId->setAttrib("readonly", "readonly");
		
        $start_date = new ZendX_JQuery_Form_Element_DatePicker('start_date');
		$start_date->setRequired(true);
        $start_date->addValidator('NotEmpty', false, array('messages' => 'Please select start date.'));
        $start_date->setAttrib('readonly', 'true');
        $start_date->setAttrib('onfocus', 'this.blur()');  
		
        
        $end_date = new ZendX_JQuery_Form_Element_DatePicker('end_date');
		$end_date->setRequired(true);
        $end_date->addValidator('NotEmpty', false, array('messages' => 'Please select end date.'));
        $end_date->setAttrib('readonly', 'true');	
		$end_date->setAttrib('onfocus', 'this.blur()');  
		
		$duration = new Zend_Form_Element_Text("duration");
		$duration->setLabel("Duration");
		$duration->setAttrib("class", "formDataElement");
		$duration->setAttrib("readonly", "readonly");
		
		$status = new Zend_Form_Element_Select('status');
		$status->setMultiOptions(array(
				'1'=>'Yet to Start' ,
				'2'=>'In progress',
				'3'=>'Complete' ,
		));
		
		$comments = new Zend_Form_Element_Textarea('comments');
        $comments->setAttrib('rows', 10);
        $comments->setAttrib('cols', 50);	
		$comments->setRequired(true);
        $comments->addValidator('NotEmpty', false, array('messages' => 'Please enter comments.'));
		 
	   //Form Submit....
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setAttrib('id', 'submitbutton');
		$submit->setLabel('Save');
		
		$notify_on = new ZendX_JQuery_Form_Element_DatePicker('notify_on');
		$notify_on->setAttrib('readonly', 'true');
		$notify_on->setAttrib('onfocus', 'this.blur()');  

		$this->addElements(array($id,$user_id,$employeeName,$employeeId,$start_date,$end_date,$duration,$status,$comments,$notify_on,$submit));
        $this->setElementDecorators(array('ViewHelper')); 
		
		$this->setElementDecorators(array(
                    'UiWidgetElement',
        ),array('start_date','end_date','notify_on'));
	}
}
         