<?php
class Default_Form_attendancereport extends Zend_Form{
	
	public function init(){
		
		
		$this->setMethod('post');
		$this->setAttrib('action',BASE_URL.'attendancereport/downloadreport');
		$this->setAttrib('id', 'attendancereportfrm');
		$this->setAttrib('name', 'attendancereport');
		
		
		$months = new Zend_Form_Element_Select('month');
		$months->setLabel("Month");
		$months->setMultiOptions(array(
				'01'=>'January',
				'02'=>'February',
				'03'=>'March',
				'04'=>'April',
				'05'=>'May',
				'06'=>'June',
				'07'=>'July',
				'08'=>'August',
				'09'=>'September',
				'10'=>'October',
				'11'=>'November',
				'12'=>'December'));
		$months->isRequired(true);
		$months->setValue(date('m'));
		
		$from_date = new ZendX_JQuery_Form_Element_DatePicker('from_date');
		$from_date->setLabel("Start Date");
		$from_date->setAttrib('readonly', 'true');
		$from_date->addValidator('NotEmpty', false, array('messages' => 'Please select date.'));
		$from_date->isRequired(true);
		
		$startdate = new Zend_Form_Element_Hidden("fromdate");
		$enddate = new Zend_Form_Element_Hidden("todate");
		
		$to_date = new ZendX_JQuery_Form_Element_DatePicker('to_date');
		$to_date->setLabel("End Date");
		$to_date->setAttrib('readonly', 'true');
		$to_date->addValidator('NotEmpty', false, array('messages' => 'Please select date.'));
		$to_date->isRequired(true);
		
		$submit = new Zend_Form_Element_Button('download');
		$submit->setAttrib('id', 'downloadbutton');
		$submit->setLabel('Download');
		$this->addElements(array($months,$from_date,$to_date,$submit,$custom_date,$enddate,$startdate,$from_date_errormessage,$to_date_errormessage));
	}
}