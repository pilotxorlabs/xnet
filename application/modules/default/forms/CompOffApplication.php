<?php

class Default_Form_CompOffApplication extends Zend_Form
{
    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('id', 'formid');
        $this->setAttrib('name', 'compffapplication');
        
        
        $option = new Zend_Form_Element_Radio('option');
        $option->setLabel('Reimbursement Option:')->addMultiOptions(array(
            'Leave' => 'Leave',
            'Encashment' => 'Encashment'
        ))->setSeparator('');
        
        $leave_date = new ZendX_JQuery_Form_Element_DatePicker('leave_date');
        $leave_date->setAttrib('onfocus', 'this.blur()');
        $leave_date->setOptions(array('class' => 'brdr_none'));
        $leave_date->addValidator('NotEmpty', false, array('messages' => 'Please select date.'));
        
        $comment = new Zend_Form_Element_Textarea('comment');
        $comment->setAttrib('rows', 10);
        $comment->setAttrib('cols', 100);
        $comment->setAttrib('maxlength', '1000');
        $comment->setAttrib("style", 'width: 409px;height:  100px;');
        $comment->setRequired(true);
        $comment->addValidator('NotEmpty', false, array('messages' => 'Please enter comment.'));
        
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id', 'submitbutton');
        $submit->setLabel('Apply');
        
        $url = "'leaverequest/compoffrequest/format/json'";
        $dialogMsg = "''";
        $toggleDivId = "''";
        $jsFunction = "''";
        
        
        $submit->setOptions(array('onclick' => "saveDetails($url,$dialogMsg,$toggleDivId,$jsFunction);"));
        
        $this->addElements(array($option,$leave_date,$comment,$submit));
        $this->setElementDecorators(array('ViewHelper'));
        $this->setElementDecorators(array(
            'UiWidgetElement',
),array('leave_date'));   		 
        
    }
}