<?php

class Default_Form_exceptionrequest extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id', 'formid');
		$this->setAttrib('name', 'exceptionrequest');
		
		$id = new Zend_Form_Element_Hidden('id');
					
		$appliedexceptioncount = new Zend_Form_Element_Text('appliedexceptioncount');
        $appliedexceptioncount->setAttrib('readonly', 'true');
		$appliedexceptioncount->setAttrib('onfocus', 'this.blur()');		
		
		$repmanagerid = new Zend_Form_Element_Text('rep_mang_id');
        $repmanagerid->setAttrib('readonly', 'true');
		$repmanagerid->setAttrib('onfocus', 'this.blur()');
		
		$issatholiday = new Zend_Form_Element_Hidden('is_sat_holiday');        		
       
        $exceptionday = new Zend_Form_Element_Select('exceptionday');
        $exceptionday->setRegisterInArrayValidator(false);
		$exceptionday->setAttrib('onchange', 'hidetodatecalender(this)');
        $exceptionday->setMultiOptions(array(							
							'1'=>'Full Day' ,
							'2'=>'Half Day',
							));
        $exceptionday->setRequired(true);
		$exceptionday->addValidator('NotEmpty', false, array('messages' => 'Please select date.'));	

        $from_date = new ZendX_JQuery_Form_Element_DatePicker('from_date');
		$from_date->setAttrib('readonly', 'true');
		$from_date->setAttrib('onfocus', 'this.blur()');
		$from_date->setOptions(array('class' => 'brdr_none'));	
		$from_date->setRequired(true);
        $from_date->addValidator('NotEmpty', false, array('messages' => 'Please select date.'));
		
		$to_date = new ZendX_JQuery_Form_Element_DatePicker('to_date');
		$to_date->setAttrib('readonly', 'true');
		$to_date->setAttrib('onfocus', 'this.blur()');
		$to_date->setAttrib('onblur', 'validate_todate()');
		$to_date->setOptions(array('class' => 'brdr_none'));	
		
		$reason = new Zend_Form_Element_Textarea('reason');
        $reason->setAttrib('rows', 10);
        $reason->setAttrib('cols', 50);
		$reason ->setAttrib('maxlength', '250');
		$reason->setRequired(true);
        $reason->addValidator('NotEmpty', false, array('messages' => 'Please enter reason.'));
		
		$exceptionstatus = new Zend_Form_Element_Text('exceptionstatus');
        $exceptionstatus->setAttrib('readonly', 'true');
		$exceptionstatus->setAttrib('onfocus', 'this.blur()');
		
		$comments = new Zend_Form_Element_Textarea('comments');
        $comments->setAttrib('readonly', 'true');
		$comments->setAttrib('onfocus', 'this.blur()');
        
		$createddate = new Zend_Form_Element_Text('createddate');
        $createddate->setAttrib('readonly', 'true');
		$createddate->setAttrib('onfocus', 'this.blur()');

        $submit = new Zend_Form_Element_Submit('submit');
		$submit->setAttrib('id', 'submitbutton');
		$submit->setLabel('Apply');
		
		$url = "'exceptionrequest/saveexceptionrequestdetails/format/json'";
		$dialogMsg = "''";
		$toggleDivId = "''";
		$jsFunction = "''";
		 

		$submit->setOptions(array('onclick' => "saveDetails($url,$dialogMsg,$toggleDivId,$jsFunction);"
		));

		$this->addElements(array($id,$reason,$repmanagerid,$comments,$exceptionday,$from_date,$to_date,$issatholiday,$appliedexceptioncount,$exceptionstatus,$createddate,$submit));
        $this->setElementDecorators(array('ViewHelper'));
        $this->setElementDecorators(array(
                    'UiWidgetElement',
        ),array('from_date','to_date')
        );   		 
	}
}