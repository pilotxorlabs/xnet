<?php
/*********************************************************************************
 *  This file is part of Sentrifugo.
 *  Copyright (C) 2014 Sapplica
 *
 *  Sentrifugo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Sentrifugo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Sentrifugo.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Sentrifugo Support <support@sentrifugo.com>
 ********************************************************************************/

class Default_Form_mypayslip extends Zend_Form
{
    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('action',BASE_URL.'mypayslip/download');
        $this->setAttrib('id', 'payslipformdw');
        $this->setAttrib('name', 'payslipform');
        
        $year = new Zend_Form_Element_Select('year');
        $year->setRegisterInArrayValidator(false);
        $year->setValue(date('Y'));
        $year->isRequired(true);
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		
		$employeeModel = new Default_Model_Employees ();
		$employee = $employeeModel->getLoggedInEmployeeDetails ( $loginUserId );
		$dateofJoing = new DateTime ( $employee [0] ['date_of_joining'] );
		
		$years = array ();
		$yearToAdd = $dateofJoing->format ( 'Y' );
		while ( $yearToAdd <= (new DateTime ())->format ( 'Y' ) ) {
			$years [$yearToAdd] = $yearToAdd;
			$yearToAdd ++;
		}
		$year->setMultiOptions ( $years );
		
        
        $months = array(
            '1'=>'January' ,
            '2'=>'February',
            '3'=>'March' ,
            '4'=>'April',
            '5'=>'May' ,
            '6'=>'June',
            '7'=>'July' ,
            '8'=>'August',
            '9'=>'September' ,
            '10'=>'October',
            '11'=>'November' ,
            '12'=>'December'
        );
        $month = new Zend_Form_Element_Select('month');
        $month->setRegisterInArrayValidator(false);
        $month->setValue(date('m') - 1);
        $month->isRequired(true);
        $month->setMultiOptions($months);		
		$month->setAttrib ( 'class', 'joiningmonth_' . $dateofJoing->format ( 'm' ) );
        $this->addElements(array($month,$year));
        $this->setElementDecorators(array('ViewHelper'));
    }
}