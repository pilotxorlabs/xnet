<?php
class Default_Form_managerexceptionrequest extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id', 'formid');
		$this->setAttrib('name', 'managerexceptionrequest');
		
		
		$id = new Zend_Form_Element_Hidden('id');
			
		$appliedexceptioncount = new Zend_Form_Element_Text('appliedexceptioncount');
		$appliedexceptioncount->setAttrib('readonly', 'true');
		$appliedexceptioncount->setAttrib('onfocus', 'this.blur()');
		
		$employeename = new Zend_Form_Element_Text('employeename');
		$employeename->setAttrib('readonly', 'true');
		$employeename->setAttrib('onfocus', 'this.blur()');
		
		$managerstatus = new Zend_Form_Element_Select('managerstatus');
		$managerstatus->setLabel("Approve or Reject or Cancel");
		$managerstatus->setRegisterInArrayValidator(false);
		$managerstatus->setMultiOptions(array(
				'1'=>'Approve' ,
				'2'=>'Reject',
				'3'=>'Cancel',
		));
		
		$comments = new Zend_Form_Element_Textarea('comments');
		$comments->setLabel("Comments");
		$comments->setAttrib('rows', 10);
		$comments->setAttrib('cols', 50);
		$comments ->setAttrib('maxlength', '50');
		$comments->setRequired(true);
		$comments->addValidator('NotEmpty', false, array('messages' => 'Please enter comment.'));
			
		/* $leavetypeid = new Zend_Form_Element_Select('leavetypeid');
		$leavetypeid->setAttrib('class', 'selectoption');
		$leavetypeid->setRegisterInArrayValidator(false);
		$leavetypeid->setAttrib('readonly', 'true');
		$leavetypeid->setAttrib('onfocus', 'this.blur()'); */
		 
		$exceptionday = new Zend_Form_Element_Select('exceptionday');
		$exceptionday->setRegisterInArrayValidator(false);
		$exceptionday->setMultiOptions(array(
				'1'=>'Full Day' ,
				'2'=>'Half Day',
		));
		$exceptionday->setAttrib('readonly', 'true');
		$exceptionday->setAttrib('onfocus', 'this.blur()');
			
		$from_date = new Zend_Form_Element_Text('from_date');
		$from_date->setAttrib('readonly', 'true');
		$from_date->setAttrib('onfocus', 'this.blur()');
		
		$to_date = new Zend_Form_Element_Text('to_date');
		$to_date->setAttrib('readonly', 'true');
		$to_date->setAttrib('onfocus', 'this.blur()');
		
		$reason = new Zend_Form_Element_Textarea('reason');
		$reason->setAttrib('rows', 10);
		$reason->setAttrib('cols', 50);
		$reason ->setAttrib('maxlength', '400');
		$reason->setAttrib('readonly', 'true');
		$reason->setAttrib('onfocus', 'this.blur()');
		
		$exceptionstatus = new Zend_Form_Element_Text('exceptionstatus');
		$exceptionstatus->setAttrib('readonly', 'true');
		$exceptionstatus->setAttrib('onfocus', 'this.blur()');
		
		$createddate = new Zend_Form_Element_Text('createddate');
		$createddate->setAttrib('readonly', 'true');
		$createddate->setAttrib('onfocus', 'this.blur()');
		
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setAttrib('id', 'submitbutton');
		$submit->setLabel('Save');
		$submit->setAttrib("onclick", "updateemprequestdetails(event, '". Zend_Controller_Front::getInstance()->getRequest()->getParam('id') ."', 'exception')");
		
		$this->addElements(array($id,$employeename,$managerstatus,$comments,$reason,$exceptionday,$from_date,$to_date,$appliedexceptioncount,$exceptionstatus,$createddate,$submit));
		$this->setElementDecorators(array('ViewHelper'));
		
		
	}
}