<?php
class Default_Form_attendanceconfiguration extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id', 'formid');
		$this->setAttrib('name', 'attendanceconfiguration');
		$this->setAttrib('action', BASE_URL.'attendanceconfiguration/add');


        $id = new Zend_Form_Element_Hidden('id');

		$business_unit = new Zend_Form_Element_Select('businessunit');
		$business_unit->setRegisterInArrayValidator(false);
      	$business_unit->setRequired(true);
		$business_unit->addValidator('NotEmpty', false, array('messages' => 'Please select business unit.'));

		$configuration_id = new Zend_Form_Element_Select('configuration_type_id');
       	$configuration_id->setRequired(true);
		$configuration_id->addValidator('NotEmpty', false, array('messages' => 'Please select configuration .'));


		$this->addElements(array($business_unit,$configuration_id,$action,$submit));
        $this->setElementDecorators(array('ViewHelper'));
	}
}