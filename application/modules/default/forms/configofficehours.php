<?php
class Default_Form_configofficehours extends Zend_Form
{
	public function init(){

		$work_culture = new Zend_Form_Element_Radio('work_culture');
        $work_culture->setRequired(True);
        $work_culture->setValue('1');
        $work_culture->addMultiOptions(array(
        		'1' => 'Open Culture',
        		'2' => 'Clock Time Range',
        ));

        $start_time = new Zend_Form_Element_Text('start_time');
        $start_time->setAttrib('readonly', 'readonly');
        $start_time->setAttrib('class', 'time');     
        $start_time->addValidator('NotEmpty', true, array('messages' => 'Please enter start time.'));
        $start_time->setRequired(true);
       
        

        $end_time = new Zend_Form_Element_Text('end_time');
        $end_time->addValidator('NotEmpty', true, array('messages' => 'Please enter end time.'));
        $end_time->setRequired(true);
        $end_time->setAttrib('readonly', 'readonly');
        $end_time->setAttrib('class', 'time');     
        

        $entry_threshold = new Zend_Form_Element_Checkbox('is_entry_threshold');
        $entry_threshold->setLabel('Entry Threshold');

        $entry_threshold_time = new Zend_Form_Element_Text('entry_time');
        $entry_threshold_time->addValidator('NotEmpty', true, array('messages' => 'Please enter entry threshold time.'));
        $entry_threshold_time->setRequired(true);        
        $entry_threshold_time->setAttrib('readonly', 'readonly');
        $entry_threshold_time->setAttrib('class', 'time');  
        

        $entry_violation_action = new Zend_Form_Element_Select('entry_time_action');
        $entry_violation_action->setRegisterInArrayValidator(false);
        $entry_violation_action->setRequired(true);
        $entry_violation_action->setMultiOptions(array(
        		'0'=>'Default to threshold',
        		'1'=>'Absent' ,
        		'2'=>'Half Day',
        		'3'=>'Ignore'));

        $exit_threshold = new Zend_Form_Element_Checkbox('is_exit_threshold');
        $exit_threshold->setLabel('Exit Threshold');


        $exit_threshold_time = new Zend_Form_Element_Text('exit_time');
        $exit_threshold_time->addValidator('NotEmpty', true, array('messages' => 'Please enter exit threshold time.'));
        $exit_threshold_time->setRequired(true);
        $exit_threshold_time->setAttrib('readonly', 'readonly');
        $exit_threshold_time->setAttrib('class', 'time');  
       


        $exit_violation_action = new Zend_Form_Element_Select('exit_time_action');
       $exit_violation_action->setRegisterInArrayValidator(false);
        $exit_violation_action->setRequired(true);
       $exit_violation_action->setMultiOptions(array(
        		'0'=>'Default to threshold',
        		'1'=>'Absent' ,
        		'2'=>'Half Day',
        		'3'=>'Ignore'));

		$submit = new Zend_Form_Element_Button('submit');
		$submit->setAttrib('id', 'submitbutton');
		$submit->setAttrib('style', 'margin-right:10px');
		$submit->setLabel('Save');

		$this->addElements(array($work_culture,$start_time,$end_time,$threshold,$entry_threshold,$entry_threshold_time,$entry_violation_action,$exit_threshold,$exit_threshold_time,$exit_violation_action,$submit));
        $this->setElementDecorators(array('ViewHelper'));
	}
}