<?php
class Default_Form_configmissedpunches extends Zend_Form
{
	public function init(){

		$threshold = new Zend_Form_Element_Text('threshold');
		$threshold->setRequired(true);
		$threshold->setAttrib('maxLength', 3);
		$threshold->addValidator('NotEmpty', false, array('messages' => 'Please enter the threshold.'));
		$threshold->addValidator("regex",true,array(
				'pattern'=>'/^[0-9]+$/',
				'messages'=>array(
						'regexNotMatch'=>'Please enter only numbers.'
				)
		));

		$action = new Zend_Form_Element_Select('action');

		$action->setMultiOptions(array(
				'1'=>'Absent' ,
				'2'=>'Half Day',
				'3'=>'Ignore' ,
		));

		$submit = new Zend_Form_Element_Button('submit');
		$submit->setAttrib('id', 'submitbutton');
		$submit->setAttrib('style', 'margin-right:10px');
		$submit->setLabel('Save');

		$this->addElements(array($threshold,$action,$submit));
        $this->setElementDecorators(array('ViewHelper'));
	}
}