<?php
/********************************************************************************* 

 *  Copyright (C) 2014 Sapplica
 *   

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/

class Default_Form_detailedattendsummary extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');
		//$this->setAttrib('action',BASE_URL.'attendancestatuscode/edit');
		$this->setAttrib('id', 'formid');
		$this->setAttrib('name', 'detailedattendsummary');
		
		$id = new Zend_Form_Element_Hidden('id');
		
		$download = new Zend_Form_Element_Submit('download');
		$download->setAttrib('id', 'downloadbutton');
		$download->setLabel('Download');
		
		$this->addElements(array($id,$download));
		$this->setElementDecorators(array('ViewHelper'));
		$this->setElementDecorators(array(
				'UiWidgetElement',
		),array('detailedattendsummary'));
	}
}