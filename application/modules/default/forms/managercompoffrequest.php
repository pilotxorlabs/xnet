<?php
class Default_Form_managercompoffrequest extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id', 'formid');
		$this->setAttrib('name', 'managercompoffrequest');

		$id = new Zend_Form_Element_Hidden('id');

		$employeename = new Zend_Form_Element_Text('employeename');
		$employeename->setAttrib('readonly', 'true');
		$employeename->setAttrib('onfocus', 'this.blur()');

		$managerstatus = new Zend_Form_Element_Select('managerstatus');
		$managerstatus->setLabel("Approve or Reject or Cancel");
		$managerstatus->setRegisterInArrayValidator(false);
		$managerstatus->setMultiOptions(array(
				'1'=>'Approve' ,
				'2'=>'Reject',
				'3'=>'Cancel',
		));

		$comments = new Zend_Form_Element_Textarea('comments');
		$comments->setLabel("Comments");
		$comments->setAttrib('rows', 10);
		$comments->setAttrib('cols', 50);
		$comments ->setAttrib('maxlength', '50');
		$comments->setRequired(true);
		$comments->addValidator('NotEmpty', false, array('messages' => 'Please enter comment.'));

		/* $leavetypeid = new Zend_Form_Element_Select('leavetypeid');
		$leavetypeid->setAttrib('class', 'selectoption');
		$leavetypeid->setRegisterInArrayValidator(false);
		$leavetypeid->setAttrib('readonly', 'true');
		$leavetypeid->setAttrib('onfocus', 'this.blur()'); */

		$availday = new Zend_Form_Element_Select('availday');
		$availday->setRegisterInArrayValidator(false);
		$compoffday =  new Zend_Form_Element_Select('compoffday');
		$compoffday->setMultiOptions(array(
				'1'=>'Encashment' ,
				'2'=>'avialday',
		));
		$availday->setAttrib('readonly', 'true');
		$availday->setAttrib('onfocus', 'this.blur()');

		$avail_date = new Zend_Form_Element_Text('avail_date');
		$avail_date->setAttrib('readonly', 'true');
		$avail_date->setAttrib('onfocus', 'this.blur()');

		$leavedate = new Zend_Form_Element_Select('leave_date');
		$leavedate->setAttrib('readonly', 'true');
		$leavedate->setAttrib('onfocus', 'this.blur()');
		$reason = new Zend_Form_Element_Textarea('reason');
		$reason->setAttrib('rows', 10);
		$reason->setAttrib('cols', 50);
		$reason ->setAttrib('maxlength', '400');
		$reason->setAttrib('readonly', 'true');
		$reason->setAttrib('onfocus', 'this.blur()');

		$compoffstatus = new Zend_Form_Element_Text('status');
		$compoffstatus->setAttrib('readonly', 'true');
		$compoffstatus->setAttrib('onfocus', 'this.blur()');

		$createddate = new Zend_Form_Element_Text('createddate');
		$createddate->setAttrib('readonly', 'true');
		$createddate->setAttrib('onfocus', 'this.blur()');

		$submit = new Zend_Form_Element_Submit("submit");
		$submit->setAttrib('id', 'submitbutton');
		$submit->setLabel('Save');
		$submit->setAttrib("onclick", "updateemprequestdetails(event, '". Zend_Controller_Front::getInstance()->getRequest()->getParam('id') ."', 'compoff')");


		$this->addElements(array($id,$employeename,$managerstatus,$comments,$reason,$availday,$avail_date,$leavedate,$compoffstatus,$createddate,$submit));
		$this->setElementDecorators(array('ViewHelper'));


	}
}