<?php
class Default_Form_configcompensatoryoff extends Zend_Form
{
	public function init(){

		$halfDay = new Zend_Form_Element_Text('halfDay');
		$halfDay->setRequired(true);
		$halfDay->setAttrib('maxLength', 3);
		$halfDay->addValidator('NotEmpty', false, array('messages' => 'Please enter hours for Half Day.'));
		$halfDay->addValidator("regex",true,array(
				'pattern'=>'/^[0-9]+$/',
				'messages'=>array(
						'regexNotMatch'=>'Please enter only numbers.'
				)
		));
		
		$fullDay = new Zend_Form_Element_Text('fullDay');
		$fullDay->setRequired(true);
		$fullDay->setAttrib('maxLength', 3);
		$fullDay->addValidator('NotEmpty', false, array('messages' => 'Please enter hours for Full Day.'));
		$fullDay->addValidator("regex",true,array(
				'pattern'=>'/^[0-9]+$/',
				'messages'=>array(
						'regexNotMatch'=>'Please enter only numbers.'
				)
		));
		
		$leaveWindow = new Zend_Form_Element_Text('leaveWindow');
		$leaveWindow->setRequired(true);
		$leaveWindow->setAttrib('maxLength', 3);
		$leaveWindow->addValidator('NotEmpty', false, array('messages' => 'Please enter months for leave window.'));
		$leaveWindow->addValidator("regex",true,array(
				'pattern'=>'/^[0-9]+$/',
				'messages'=>array(
						'regexNotMatch'=>'Please enter only numbers.'
				)
		));

		$considerationWindow = new Zend_Form_Element_Select('considerationWindow');

		$considerationWindow->setMultiOptions(array(
				'1'=>'Actual date' ,
				'2'=>'Applied date',
				'3'=>'Approved date' ,
		));

		$submit = new Zend_Form_Element_Button('submit');
		$submit->setAttrib('id', 'submitbutton');
		$submit->setAttrib('style', 'margin-right:10px');
		$submit->setLabel('Save');

		$this->addElements(array($halfDay,$fullDay,$leaveWindow,$considerationWindow,$submit));
        $this->setElementDecorators(array('ViewHelper'));
	}
}