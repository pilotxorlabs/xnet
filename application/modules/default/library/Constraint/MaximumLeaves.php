<?php

class Constraint_MaximumLeaves extends Constraint_MinimumLeaves
{
    protected $days = 2;
    
    public function createForm()
    {
        $form = new Zend_Form();
        $form->setName("Constraint_MaximumLeaves");
        $form->setAttrib("id", "Constraint_MaximumLeaves");
        
        $dayselement = new Zend_Form_Element_Text('days');
        $dayselement->setLabel("Number of days");
        $dayselement->setAttrib('maxLength', 3);
        $dayselement->addFilter(new Zend_Filter_StringTrim());
        $dayselement->setRequired(true);
        $dayselement->addValidator('NotEmpty', false, array('messages' => 'Please enter number of leaves.'));
        $dayselement->addValidator("regex",true,array(
            'pattern'=>'/^[0-9]+$/',
            
            'messages'=>array(
                'regexNotMatch'=>'Please enter only numbers.'
            )
        ));
        
        
        
        $form->addElement($dayselement);
        return $form;
    }
    
    public function apply()
    {
        if($this->leaverequest['appliedleavescount'] > $this->days){
            throw new Exception("Leaves more than ". $this->days . " days not allowed.");
        }
    }
}

