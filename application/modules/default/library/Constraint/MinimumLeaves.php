<?php
class Constraint_MinimumLeaves extends Constraint_BaseAbstract{
    
    protected $days = 3; 
    
    /**
     * {@inheritDoc}
     * @see BaseConstraint_Abstract::createForm()
     */
    public function createForm()
    {
    	$minimumleavesform = new Zend_Form();
    	$minimumleavesform->setName('Constraint_MinimumLeaves');
    	$minimumleavesform->setAttrib("id", 'Constraint_MinimumLeaves');
    	
    	$minimumleaves = new Zend_Form_Element_Text('days');
    	$minimumleaves->setLabel("Number of days");
    	$minimumleaves->setAttrib('maxLength', 3);
    	$minimumleaves->addFilter(new Zend_Filter_StringTrim());
    	$minimumleaves->setRequired(true);
    	$minimumleaves->addValidator('NotEmpty', false, array('messages' => 'Please enter number of leaves.'));
    	$minimumleaves->addValidator("regex",true,array(
    			'pattern'=>'/^[0-9]+$/',
    	
    			'messages'=>array(
    					'regexNotMatch'=>'Please enter only numbers.'
    			)
    	));
    	
    	
    	
    	$minimumleavesform->addElement($minimumleaves);
        return $minimumleavesform;        
    }

    /**
     * {@inheritDoc}
     * @see BaseConstraint_Abstract::apply()
     */
    public function apply()
    {
        if($this->leaverequest['appliedleavescount'] < $this->days){
            throw new Exception("Leaves less than ". $this->days . " days not allowed.");
        }
    }

    
}