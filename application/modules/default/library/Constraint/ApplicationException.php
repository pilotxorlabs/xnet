<?php
class Constraint_ApplicationException extends Zend_Exception{
    
    private $message;
    
    public function __construct($errorMessage){
        parent::__construct($errorMessage);
        $this->message = $errorMessage;
    }
    
    public function getMessage(){
        return $this->message;
    }
}