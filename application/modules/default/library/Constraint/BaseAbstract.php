<?php
abstract class Constraint_BaseAbstract {

    protected $leaverequest;
    
    public static function initialize($parameters,$classname) {
        $object = new $classname();
        $reflectionClass = new ReflectionClass($classname);
        foreach ($parameters as $member) {
            $key = array_search($member, $parameters);
            $property = $reflectionClass->getProperty($key);
            $property->setAccessible(true);
            $property->setValue($object, $member);
        }
        return $object;
    }
    
    
    abstract public function createForm();
    
    abstract public function apply();
    
    
}