<?php
class Constraint_MinimumNoticePeriod extends Constraint_BaseAbstract{
	private $days = 2;
	/**
	 * {@inheritDoc}
	 * @see BaseConstraint_Abstract::createForm()
	 */
	public function createForm(){
		$minimumleavesform = new Zend_Form();
		$minimumleavesform->setName("Constraint_MinimumNoticePeriod");
		$minimumleavesform->setAttrib("id", "Constraint_MinimumNoticePeriod");
		 
		$minimumleaves = new Zend_Form_Element_Text('days');
		$minimumleaves->setLabel("Minimum Notice period");
		$minimumleaves->setAttrib('maxLength', 2);
		$minimumleaves->addFilter(new Zend_Filter_StringTrim());
		$minimumleaves->setRequired(true);
		$minimumleaves->addValidator('NotEmpty', false, array('messages' => 'Please enter number of days.'));
		$minimumleaves->addValidator("regex",true,array(
				'pattern'=>'/^[0-9]+$/',
				 
				'messages'=>array(
						'regexNotMatch'=>'Please enter only numbers.'
				)
		));
		 
		 
		 
		$minimumleavesform->addElement($minimumleaves);
		return $minimumleavesform;
	}
	/**
	 * {@inheritDoc}
	 * @see BaseConstraint_Abstract::apply()
	 */
	
	public function apply(){
		$from_date = new DateTime($this->leaverequest['from_date']) ;
		$current_date = new DateTime(date('m/d/y'));
		
		if($from_date->diff($current_date, true)->days < $this->days){
			throw new Exception("This Leave type must be applied ". $this->days . " before.");
		}
	}
	
}