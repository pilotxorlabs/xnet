<?php
$path = dirname(__FILE__). '\\..\\..\..\\..\\';

set_include_path(get_include_path() . PATH_SEPARATOR . $path);

require_once 'Zend/Db/Adapter/Abstract.php';
require_once 'Zend/Db/Table/Abstract.php';
require_once 'Zend/Db/Select.php';
require_once 'Zend/Db/Adapter/Pdo/Mysql.php';
require_once 'Zend/Db.php';
require 'public\db_constants.php';

abstract class Default_PHPUnit_Framework_TestCase extends PHPUnit_Framework_TestCase {
    
}