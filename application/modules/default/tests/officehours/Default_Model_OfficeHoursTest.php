<?php
require '..\\Default_PHPUnit_TestCase.php';
require 'application\modules\default\models\officehours.php';


class Default_Model_OfficeHoursTest extends Default_PHPUnit_Framework_TestCase{
    private $default_Model_OfficeHours;
    
    protected function setUp()
    {
        parent::setUp();
        
        $db = Zend_Db::factory('Pdo_Mysql', array(
            'host'     => SENTRIFUGO_HOST,
            'username' => SENTRIFUGO_USERNAME,
            'password' => SENTRIFUGO_PASSWORD,
            'dbname'   => SENTRIFUGO_DBNAME
        ));
        $this->default_Model_OfficeHours = new Default_Model_OfficeHours(array('db' => $db));
    }
    
    protected function tearDown()
    {
        $this->default_Model_OfficeHours = null;
        parent::tearDown();
    }
    
    public function testRunCustomValidationsNoActivities(){
       $messageArrayActual = array();
       $messageArrayExpected = array();
       $data = array();
       $this->default_Model_OfficeHours->runCustomValidation($data, $messageArrayActual);
       $this->assertTrue($messageArrayActual === $messageArrayExpected,"Incorrect validation for No activites.");
    }
    
    public function testRunCustomValidationsEntryTime(){
        $messageArrayActual = array();
        $messageArrayExpected = array();
        $data = array('start_time' => '08:00','end_time'=>'05:00','entry_time'=>'10:00');
        $this->default_Model_OfficeHours->runCustomValidation($data, $messageArrayActual);
        $this->assertTrue($messageArrayActual === $messageArrayExpected,"Incorrect validation for Entry time.");
    }
    public function testRunCustomValidationsWrongEntryTime(){
        $messageArrayActual = array();
        $messageArrayExpected = array('entry_time' => 'Entry threshold time should be within start time and end time.');
        $data = array('start_time' => '08:00','end_time'=>'05:00','entry_time'=>'07:00');
        $this->default_Model_OfficeHours->runCustomValidation($data, $messageArrayActual);
        $this->assertTrue($messageArrayActual === $messageArrayExpected,"Incorrect validation for Entry time.");
    }
    
    public function testRunCustomValidationsExitTime(){
        $messageArrayActual = array();
        $messageArrayExpected = array();
        $data = array('start_time' => '08:00','end_time'=>'05:00','exit_time'=>'10:00');
        $this->default_Model_OfficeHours->runCustomValidation($data, $messageArrayActual);
        $this->assertTrue($messageArrayActual === $messageArrayExpected,"Incorrect validation for Exit time.");
    }
    public function testRunCustomValidationsWrongExitTime(){
        $messageArrayActual = array();
        $messageArrayExpected = array('exit_time' => 'Exit threshold time should be within start time and end time.');
        $data = array('start_time' => '08:00','end_time'=>'05:00','exit_time'=>'07:00');
        $this->default_Model_OfficeHours->runCustomValidation($data, $messageArrayActual);
        $this->assertTrue($messageArrayActual === $messageArrayExpected,"Incorrect validation for Exit time.");
    }
    
    public function testRunCustomValidationsWrongEntryExitTime(){
        $messageArrayActual = array();
        $messageArrayExpected = array('entry_time' => 'Entry threshold should be less than exit threshold.');
        $data = array('start_time' => '08:00','end_time'=>'05:00','entry_time'=>'19:00','exit_time'=>'17:00');
        $this->default_Model_OfficeHours->runCustomValidation($data, $messageArrayActual);
        $this->assertTrue($messageArrayActual === $messageArrayExpected,"Incorrect validation for Exit time.");
    }
    
    public function testRunCustomValidationsEntryExitTime(){
        $messageArrayActual = array();
        $messageArrayExpected = array();
        $data = array('start_time' => '08:00','end_time'=>'05:00','entry_time'=>'10:00','exit_time'=>'17:00');
        $this->default_Model_OfficeHours->runCustomValidation($data, $messageArrayActual);
        $this->assertTrue($messageArrayActual === $messageArrayExpected,"Incorrect validation for Exit time.");
    }
    
    public function testValidateOfficeHoursNoEntryExitThreshold(){
        $dailyruleOfficeHours = array();
        $faultyDays = array();
        $loggedDay = array(array());
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_OfficeHours->validateOfficeHours($dailyruleOfficeHours,$loggedDay,0 , $faultyDays, $absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    
    public function testValidateOfficeHoursEntryThresholdIgnoreAction(){
        $dailyruleOfficeHours = array('is_entry_threshold'=>1,'entry_time_action' => 3,'entry_time'=>'10:30');
        $faultyDays = array();
        $loggedDay = array(array('punch_date'=>'2018-03-03','check_in'=>'11:00:00'));
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_OfficeHours->validateOfficeHours($dailyruleOfficeHours,$loggedDay,0, $faultyDays, $absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    
    public function testValidateOfficeHoursEntryThresholdAbsentAction(){
        $dailyruleOfficeHours = array('is_entry_threshold'=>1,'entry_time_action' => 1,'entry_time'=>'10:30');
        $faultyDays = array();
        $loggedDay = array(array('punch_date'=>'2018-03-03','check_in'=>'11:00:00'));
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_OfficeHours->validateOfficeHours($dailyruleOfficeHours,$loggedDay,0, $faultyDays, $absentFlag,$halfDayFlag);
        $this->assertNotEmpty($faultyDays,"Day should be added to faulty days.");
        $this->assertEquals(1,$absentFlag, "Day should be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    public function testValidateOfficeHoursEntryThresholdHalfDayAction(){
        $dailyruleOfficeHours = array('is_entry_threshold'=>1,'entry_time_action' => 2,'entry_time'=>'10:30');
        $faultyDays = array();
        $loggedDay = (array('punch_date'=>'2018-03-03','check_in'=>'11:00:00'));
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_OfficeHours->validateOfficeHours($dailyruleOfficeHours,$loggedDay,0, $faultyDays, $absentFlag,$halfDayFlag);
        $this->assertNotEmpty($faultyDays,"Day should be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(1,$halfDayFlag, "Day should be marked as Half day.");
    }
    
    public function testValidateOfficeHoursEntryThresholdDefaultToThresholdAction(){
        $dailyruleOfficeHours = array('is_entry_threshold'=>1,'entry_time_action' => 0,'entry_time'=>'10:30');
        $faultyDays = array();
        $loggedDay = array(array('punch_date'=>'2018-03-03','check_in'=>'11:00:00'));
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_OfficeHours->validateOfficeHours($dailyruleOfficeHours,$loggedDay,0, $faultyDays, $absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day should be marked as Half day.");
        $this->assertEquals($loggedDay[0]['check_in'],$dailyruleOfficeHours['entry_time'], "Check in time should be changed to threshold time.");
    }
    
    public function testValidateOfficeHoursExitThresholdIgnoreAction(){
        $dailyruleOfficeHours = array('is_exit_threshold'=>1,'exit_time_action' => 3,'exit_time'=>'19:30');
        $faultyDays = array();
        $loggedDay = array(array('punch_date'=>'2018-03-03','check_in'=>'11:00:00','check_out'=>'19:45'));
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_OfficeHours->validateOfficeHours($dailyruleOfficeHours,$loggedDay,0, $faultyDays, $absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    
    public function testValidateOfficeHoursExitThresholdAbsentAction(){
        $dailyruleOfficeHours = array('is_exit_threshold'=>1,'exit_time_action' => 1,'exit_time'=>'19:30');
        $faultyDays = array();
        $loggedDay = array(array('punch_date'=>'2018-03-03','check_in'=>'11:00:00','check_out'=>'19:45'));
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_OfficeHours->validateOfficeHours($dailyruleOfficeHours,$loggedDay,0, $faultyDays, $absentFlag,$halfDayFlag);
        $this->assertNotEmpty($faultyDays,"Day should be added to faulty days.");
        $this->assertEquals(1,$absentFlag, "Day should be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    public function testValidateOfficeHoursExitThresholdHalfDayAction(){
        $dailyruleOfficeHours = array('is_exit_threshold'=>1,'exit_time_action' => 2,'exit_time'=>'19:30');
        $faultyDays = array();
        $loggedDay = array(array('punch_date'=>'2018-03-03','check_in'=>'11:00:00','check_out'=>'19:45'));
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_OfficeHours->validateOfficeHours($dailyruleOfficeHours,$loggedDay,0, $faultyDays, $absentFlag,$halfDayFlag);
        $this->assertNotEmpty($faultyDays,"Day should be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(1,$halfDayFlag, "Day should be marked as Half day.");
    }
    
    public function testValidateOfficeHoursExitThresholdDefaultToThresholdAction(){
        $dailyruleOfficeHours = array('is_exit_threshold'=>1,'exit_time_action' => 0,'exit_time'=>'19:30');
        $faultyDays = array();
        $loggedDay = array(array('punch_date'=>'2018-03-03','check_in'=>'11:00:00','check_out'=>'19:45'));
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_OfficeHours->validateOfficeHours($dailyruleOfficeHours,$loggedDay,0, $faultyDays, $absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day should be marked as Half day.");
        $this->assertEquals($loggedDay[0]['check_out'],$dailyruleOfficeHours['exit_time'], "Check out time should be changed to threshold time.");
    }
    
    public function testValidateOfficeHoursEntryExitThresholdDefaultToThresholdAction(){
        $dailyruleOfficeHours = array('is_entry_threshold'=>1,'entry_time_action' => 0,'entry_time'=>'10:30','is_exit_threshold'=>1,'exit_time_action' => 0,'exit_time'=>'19:30');
        $faultyDays = array();
        $loggedDay = array(array('punch_date'=>'2018-03-03','check_in'=>'11:00:00','check_out'=>'19:45'));
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_OfficeHours->validateOfficeHours($dailyruleOfficeHours,$loggedDay,0, $faultyDays, $absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day should be marked as Half day.");
        $this->assertEquals($loggedDay[0]['check_out'],$dailyruleOfficeHours['exit_time'], "Check out time should be changed to threshold time.");
        $this->assertEquals($loggedDay[0]['check_in'],$dailyruleOfficeHours['entry_time'], "Check out time should be changed to threshold time.");
        
    }
    
    public function testValidateOfficeHoursEntryThresholdIgnoreExitThresholdDefaultToThresholdAction(){
        $dailyruleOfficeHours = array('is_entry_threshold'=>1,'entry_time_action' => 3,'entry_time'=>'10:30','is_exit_threshold'=>1,'exit_time_action' => 0,'exit_time'=>'19:30');
        $faultyDays = array();
        $loggedDay = array(array('punch_date'=>'2018-03-03','check_in'=>'11:00:00','check_out'=>'19:45'));
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_OfficeHours->validateOfficeHours($dailyruleOfficeHours,$loggedDay,0, $faultyDays, $absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day should be marked as Half day.");
        $this->assertEquals($loggedDay[0]['check_out'],$dailyruleOfficeHours['exit_time'], "Check out time should be changed to threshold time.");
        
    }
    
    public function testValidateOfficeHoursEntryThresholdAbsentExitThresholdDefaultToThresholdAction(){
        $dailyruleOfficeHours = array('is_entry_threshold'=>1,'entry_time_action' => 1,'entry_time'=>'10:30','is_exit_threshold'=>1,'exit_time_action' => 0,'exit_time'=>'19:30');
        $faultyDays = array();
        $loggedDay = array(array('punch_date'=>'2018-03-03','check_in'=>'11:00:00','check_out'=>'19:45'));
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_OfficeHours->validateOfficeHours($dailyruleOfficeHours,$loggedDay,0, $faultyDays, $absentFlag,$halfDayFlag);
        $this->assertNotEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(1,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day should be marked as Half day.");
        $this->assertEquals($loggedDay[0]['check_out'],$dailyruleOfficeHours['exit_time'], "Check out time should be changed to threshold time.");
        
    }
    
    public function testValidateOfficeHoursEntryThresholdHalfDaytExitThresholdDefaultToThresholdAction(){
        $dailyruleOfficeHours = array('is_entry_threshold'=>1,'entry_time_action' => 2,'entry_time'=>'10:30','is_exit_threshold'=>1,'exit_time_action' => 0,'exit_time'=>'19:30');
        $faultyDays = array();
        $loggedDay = array(array('punch_date'=>'2018-03-03','check_in'=>'11:00:00','check_out'=>'19:45'));
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_OfficeHours->validateOfficeHours($dailyruleOfficeHours,$loggedDay,0, $faultyDays, $absentFlag,$halfDayFlag);
        $this->assertNotEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(1,$halfDayFlag, "Day should be marked as Half day.");
        $this->assertEquals($loggedDay[0]['check_out'],$dailyruleOfficeHours['exit_time'], "Check out time should be changed to threshold time.");
        
    }
}