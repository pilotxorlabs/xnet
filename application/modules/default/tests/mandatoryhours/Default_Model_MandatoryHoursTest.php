<?php
require '..\\Default_PHPUnit_TestCase.php';
require 'application\modules\default\models\mandatoryhours.php';


class Default_Model_MandatoryHoursTest extends Default_PHPUnit_Framework_TestCase{
    private $default_Model_MandatoryHours;
    
    protected function setUp()
    {
        parent::setUp();
        
        $db = Zend_Db::factory('Pdo_Mysql', array(
            'host'     => SENTRIFUGO_HOST,
            'username' => SENTRIFUGO_USERNAME,
            'password' => SENTRIFUGO_PASSWORD,
            'dbname'   => SENTRIFUGO_DBNAME
        ));
        $this->default_Model_MandatoryHours = new Default_Model_MandatoryHours(array('db' => $db));
    }
    
    protected function tearDown()
    {
        $this->default_Model_MandatoryHours = null;
        parent::tearDown();
    }
    
    public function testHalfDayRuleCalculationNoRule(){
        $halfdayrule = array();
        $workinghrs = 0;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->halfDayRuleCalculation($halfdayrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    
    public function testHalfDayRuleCalculationNoRuleWorkingHoursSet(){
        $halfdayrule = array();
        $workinghrs = 8;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->halfDayRuleCalculation($halfdayrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    
    public function testHalfDayRuleCalculationNoRuleAbsentFlagSet(){
        $halfdayrule = array();
        $workinghrs = 9;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 1;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->halfDayRuleCalculation($halfdayrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn be added to faulty days.");
        $this->assertEquals(1,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    
    public function testHalfDayRuleCalculationNoRuleHalfDayFlagSet(){
        $halfdayrule = array();
        $workinghrs = 7;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 1;
        $this->default_Model_MandatoryHours->halfDayRuleCalculation($halfdayrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(1,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    
    public function testHalfDayRuleCalculationNoRuleAbsentHalfDayFlagSet(){
        $halfdayrule = array();
        $workinghrs = 10;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 1;
        $halfDayFlag = 1;
        $this->default_Model_MandatoryHours->halfDayRuleCalculation($halfdayrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(1,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(1,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    
    public function testHalfDayRuleCalculationHafDayAction(){
        $halfdayrule = array(
            array(
                'minimum_hours' => '4',
                'minimum_hours_action' => '2',
                'maximum_hours' => '8',
                'maximum_hours_action' => '3'
            )
        );
        $workinghrs = 3;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->halfDayRuleCalculation($halfdayrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertNotEmpty($faultyDays,"Day should be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(1,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    
    public function testHalfDayRuleCalculationAbsentAction(){
        $halfdayrule = array(
            array(
                'minimum_hours' => '4',
                'minimum_hours_action' => '1',
                'maximum_hours' => '8',
                'maximum_hours_action' => '3'
            )
        );
        $workinghrs = 3;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->halfDayRuleCalculation($halfdayrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertNotEmpty($faultyDays,"Day shouldn be added to faulty days.");
        $this->assertEquals(1,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    
    public function testHalfDayRuleCalculationNoAction(){
        $halfdayrule = array(
            array(
                'minimum_hours' => '4',
                'minimum_hours_action' => '2',
                'maximum_hours' => '8',
                'maximum_hours_action' => '3'
            )
        );
        $workinghrs = 10;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->halfDayRuleCalculation($halfdayrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    
    public function testHalfDayRuleCalculationIgnoreAction(){
        $halfdayrule = array(
            array(
                'minimum_hours' => '4',
                'minimum_hours_action' => '1',
                'maximum_hours' => '8',
                'maximum_hours_action' => '3'
            )
        );
        $workinghrs = 9;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->halfDayRuleCalculation($halfdayrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    
    public function testHalfDayRuleCalculationMaxAbsentAction(){
        $halfdayrule = array(
            array(
                'minimum_hours' => '4',
                'minimum_hours_action' => '2',
                'maximum_hours' => '8',
                'maximum_hours_action' => '1'
            )
        );
        $workinghrs = 10;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->halfDayRuleCalculation($halfdayrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertNotEmpty($faultyDays,"Day should be added to faulty days.");
        $this->assertEquals(1,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    }
    
    public function testDailyRuleCalculationNoRuleAction(){
        $dailyrule = array();
        $workinghrs = 0;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->dailyRuleCalculation($dailyrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    } 
    
    public function testDailyRuleCalculationNoRuleWorkingHoursSet(){
        $dailyrule = array();
        $workinghrs = 10;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->dailyRuleCalculation($dailyrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    } 
    public function testDailyRuleCalculationNoRuleAbsentFlagSet(){
        $dailyrule = array();
        $workinghrs = 9;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 1;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->dailyRuleCalculation($dailyrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(1,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    } 
    
    public function testDailyRuleCalculationNoRuleHalfDayFlagSet(){
        $dailyrule = array();
        $workinghrs = 0;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 1;
        $this->default_Model_MandatoryHours->dailyRuleCalculation($dailyrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(1,$halfDayFlag, "Day shouldn't be marked as Half day.");
    } 
    
    public function testDailyRuleCalculationNoRuleAbsentHalfDayFlagsSet(){
        $dailyrule = array();
        $workinghrs = 0;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 1;
        $halfDayFlag = 1;
        $this->default_Model_MandatoryHours->dailyRuleCalculation($dailyrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(1,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(1,$halfDayFlag, "Day shouldn't be marked as Half day.");
    } 
    
    public function testDailyRuleCalculationHalfDayAction(){
        $dailyrule = array(
            array(
                'minimum_hours' => '8',
                'minimum_hours_action' => '2',
                'maximum_hours' => '24',
                'maximum_hours_action' => '3'
            )
        );
        $workinghrs = 5;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->dailyRuleCalculation($dailyrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertNotEmpty($faultyDays,"Day shouldn be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(1,$halfDayFlag, "Day shouldn't be marked as Half day.");
    } 
    
    public function testDailyRuleCalculationAbsentAction(){
        $dailyrule = array(
            array(
                'minimum_hours' => '8',
                'minimum_hours_action' => '1',
                'maximum_hours' => '8',
                'maximum_hours_action' => '3'
            )
        );
        $workinghrs = 6;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->dailyRuleCalculation($dailyrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertNotEmpty($faultyDays,"Day shouldn be added to faulty days.");
        $this->assertEquals(1,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    } 
    
    public function testDailyRuleCalculationNoAction(){
        $dailyrule = array(
            array(
                'minimum_hours' => '8',
                'minimum_hours_action' => '1',
                'maximum_hours' => '24',
                'maximum_hours_action' => '3'
            )
        );
        $workinghrs = 9;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->dailyRuleCalculation($dailyrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    } 
    
    public function testDailyRuleCalculationValidHours(){
        $dailyrule = array(
            array(
                'minimum_hours' => '8',
                'minimum_hours_action' => '2',
                'maximum_hours' => '20',
                'maximum_hours_action' => '3'
            )
        );
        $workinghrs = 9;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->dailyRuleCalculation($dailyrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertEmpty($faultyDays,"Day shouldn't be added to faulty days.");
        $this->assertEquals(0,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    } 
    
    public function testDailyRuleCalculationMaxAbsentAction(){
        $dailyrule = array(
            array(
                'minimum_hours' => '8',
                'minimum_hours_action' => '2',
                'maximum_hours' => '10',
                'maximum_hours_action' => '1'
            )
        );
        $workinghrs = 11;
        $faultyDays = array();
        $loggedday = array(
            array('punch_date'=>'2018-04-10')
        );
        $absentFlag = 0;
        $halfDayFlag = 0;
        $this->default_Model_MandatoryHours->dailyRuleCalculation($dailyrule,$workinghrs,$faultyDays,$loggedday,$absentFlag,$halfDayFlag);
        $this->assertNotEmpty($faultyDays,"Day shouldn be added to faulty days.");
        $this->assertEquals(1,$absentFlag, "Day shouldn't be marked as Absent.");
        $this->assertEquals(0,$halfDayFlag, "Day shouldn't be marked as Half day.");
    } 
}