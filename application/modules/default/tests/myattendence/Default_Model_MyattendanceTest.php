<?php
require '..\\Default_PHPUnit_TestCase.php';
require 'application\modules\default\models\Myattendance.php';


class Default_Model_MyattendanceTest extends Default_PHPUnit_Framework_TestCase
{
    private $default_Model_Myattendance;

    protected function setUp()
    {
        parent::setUp();
        
        $db = Zend_Db::factory('Pdo_Mysql', array(
            'host'     => SENTRIFUGO_HOST,
            'username' => SENTRIFUGO_USERNAME,
            'password' => SENTRIFUGO_PASSWORD,
            'dbname'   => SENTRIFUGO_DBNAME
        ));
        $this->default_Model_Myattendance = new Default_Model_Myattendance(array('db' => $db));
    }

    protected function tearDown()
    {
        $this->default_Model_Myattendance = null;
        parent::tearDown();
    }

    public function testValidHoursToSeconds()
    {   
        $hours = 8;
        $this->assertEquals(28800, $this->default_Model_Myattendance->hoursToSeconds($hours));
    }
    
    public function testValidFloatingHoursToSeconds()
    {
        $hours = 8.2;
        $this->assertEquals(30000, $this->default_Model_Myattendance->hoursToSeconds($hours));
    }
    
    public function testValidFloatingZeroFillHoursToSeconds()
    {
        $hours = 8.02;
        $this->assertEquals(28920, $this->default_Model_Myattendance->hoursToSeconds($hours));
    }
    
    public function testInvalidHoursToSeconds()
    {
        $hours = 'invalidhours';
        $this->assertEquals(0, $this->default_Model_Myattendance->hoursToSeconds($hours));
    }
    
    public function testNullHoursToSeconds()
    {
        $hours = null;
        $this->assertEquals(0, $this->default_Model_Myattendance->hoursToSeconds($hours));
    }
    
    public function testGetMissPunchestodeductAbsentAction()
    {
        $misspunch = 5;
        $missedpunchesrule = array(array('threshold'=>1,'action' => 1));
        $this->assertEquals(4, $this->default_Model_Myattendance->getMissPunchestodeduct($misspunch, $missedpunchesrule));
    }
    
    public function testGetMissPunchestodeductHalfDayAction()
    {
        $misspunch = 5;
        $missedpunchesrule = array(array('threshold'=>1,'action' => 2));
        $this->assertEquals(2, $this->default_Model_Myattendance->getMissPunchestodeduct($misspunch, $missedpunchesrule));
    }
    
    public function testGetMissPunchestodeductNoRule()
    {
        $misspunch = 5;
        $missedpunchesrule = array();
        $this->assertEquals(0, $this->default_Model_Myattendance->getMissPunchestodeduct($misspunch, $missedpunchesrule));
    }
}

