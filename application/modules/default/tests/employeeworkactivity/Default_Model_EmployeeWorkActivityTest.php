<?php
require '..\\Default_PHPUnit_TestCase.php';
require 'application\modules\default\models\Employeeworkactivity.php';


class Default_Model_EmployeeWorkActivityTest extends Default_PHPUnit_Framework_TestCase{
    private $default_Model_EmployeeWorkActivity;
    
    protected function setUp()
    {
        parent::setUp();
        
        $db = Zend_Db::factory('Pdo_Mysql', array(
            'host'     => SENTRIFUGO_HOST,
            'username' => SENTRIFUGO_USERNAME,
            'password' => SENTRIFUGO_PASSWORD,
            'dbname'   => SENTRIFUGO_DBNAME
        ));
        $this->default_Model_EmployeeWorkActivity = new Default_Model_EmployeeWorkActivity(array('db' => $db));
    }
    
    protected function tearDown()
    {
        $this->default_Model_EmployeeWorkActivity = null;
        parent::tearDown();
    }
    
    public function testCreateActivityIntervalForOddActivitiesNo(){
        $actvitiesArray = array();
        $dailyActivitiesActual = array();
        $dailyActivitiesExpected = array();
        array_push($actvitiesArray,array("punch_time" => "2018-07-09 09:18:00", "status" =>"WFH"));
        array_push($actvitiesArray,array("punch_time" => "2018-07-09 13:18:00", "status" =>"WFH"));
        array_push($actvitiesArray,array("punch_time" => "2018-07-09 22:18:00", "status" =>"WFH"));
        array_push($dailyActivitiesExpected,array("status" =>"Work From Home", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesExpected,array("status" =>"Work From Home", "punchin" => "2018-07-09 22:18:00", "punchout"=>null));
        $this->default_Model_EmployeeWorkActivity->createActivityInterval($actvitiesArray,$dailyActivitiesActual,'');
        $this->assertTrue($dailyActivitiesExpected === $dailyActivitiesActual,"Wrong intervals created.");
       
    }
    
    public function testCreateActivityIntervalForEvenActivitiesNo(){
        $actvitiesArray = array();
        $dailyActivitiesActual = array();
        $dailyActivitiesExpected = array();
        array_push($actvitiesArray,array("punch_time" => "2018-07-09 09:18:00", "status" =>"WFH"));
        array_push($actvitiesArray,array("punch_time" => "2018-07-09 13:18:00", "status" =>"WFH"));
        array_push($actvitiesArray,array("punch_time" => "2018-07-09 22:18:00", "status" =>"WFH"));
        array_push($actvitiesArray,array("punch_time" => "2018-07-09 22:20:00", "status" =>"WFH"));
        array_push($dailyActivitiesExpected,array("status" =>"Work From Home", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesExpected,array("status" =>"Work From Home", "punchin" => "2018-07-09 22:18:00", "punchout"=>"2018-07-09 22:20:00"));
        $this->default_Model_EmployeeWorkActivity->createActivityInterval($actvitiesArray,$dailyActivitiesActual,'');
        $this->assertTrue($dailyActivitiesExpected === $dailyActivitiesActual,"Wrong intervals created.");
        
    }
    
    public function testCreateActivityIntervalForNoActivities(){
        $actvitiesArray = array();
        $dailyActivitiesActual = array();
        $dailyActivitiesExpected = array();
        $this->default_Model_EmployeeWorkActivity->createActivityInterval($actvitiesArray,$dailyActivitiesActual,'');
        $this->assertTrue($dailyActivitiesExpected === $dailyActivitiesActual,"Wrong intervals created.");
        
    }
    
    public function testCalculateWorkhoursForCompleteIntervals(){
        $dailyActivities = array();
        array_push($dailyActivities,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivities,array("status" =>"WFH", "punchin" => "2018-07-09 22:18:00", "punchout"=>"2018-07-09 22:20:00"));
        $expectedWorkhours = 4.02;
        $actualWorkhours = $this->default_Model_EmployeeWorkActivity->calculateTotalHours($dailyActivities);
        $this->assertTrue($expectedWorkhours === $actualWorkhours,"Wrong workhours calculated.");        
    }
    
    public function testCalculateWorkhoursForIncompleteIntervals(){
        $dailyActivities = array();
        array_push($dailyActivities,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivities,array("status" =>"WFH", "punchin" => "2018-07-09 22:18:00", "punchout"=>"2018-07-09 22:20:00"));
        array_push($dailyActivities,array("status" =>"WFH", "punchin" => "2018-07-09 22:18:00",null));
        $expectedWorkhours = 4.02;
        $actualWorkhours = $this->default_Model_EmployeeWorkActivity->calculateTotalHours($dailyActivities);
        $this->assertTrue($expectedWorkhours === $actualWorkhours,"Wrong workhours calculated.");
    }
    
    public function testCalculateWorkhoursForNoIntervals(){
        $dailyActivities = array();
        $expectedWorkhours = 0.0;
        $actualWorkhours = $this->default_Model_EmployeeWorkActivity->calculateTotalHours($dailyActivities);
        $this->assertTrue($expectedWorkhours === $actualWorkhours,"Wrong workhours calculated.");
    }
    
    public function testCalculateWorkhoursForInactvities(){
        $dailyActivities = array();
        array_push($dailyActivities,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivities,array("status" =>"Inactivity", "punchin" => "2018-07-09 22:18:00", "punchout"=>"2018-07-09 22:20:00"));
        array_push($dailyActivities,array("status" =>"WFH", "punchin" => "2018-07-09 22:18:00",null));
        $expectedWorkhours = 4.0;
        $actualWorkhours = $this->default_Model_EmployeeWorkActivity->calculateTotalHours($dailyActivities);
        $this->assertTrue($expectedWorkhours === $actualWorkhours,"Wrong workhours calculated.");
    }
    
    public function testAddBreaksInActivitiesWithBreaks(){
        $dailyActivitiesActual = array();
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 22:18:00", "punchout"=>"2018-07-09 22:20:00"));
        $dailyActivitiesExpected = array();
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 22:18:00", "punchout"=>"2018-07-09 22:20:00"));
        array_push($dailyActivitiesExpected,array("punchin" => "2018-07-09 13:18:01", "punchout"=>"2018-07-09 22:17:59", "status" =>"Inactivity"));
        $this->default_Model_EmployeeWorkActivity->addBreaksInActivities($dailyActivitiesActual);
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Breaks not added correctly.");            
    }
    
    public function testAddBreaksInActivitiesWithoutBreaks(){
        $dailyActivitiesActual = array();
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 13:18:01", "punchout"=>"2018-07-09 22:20:00"));
        $dailyActivitiesExpected = array();
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 13:18:01", "punchout"=>"2018-07-09 22:20:00"));
        $this->default_Model_EmployeeWorkActivity->addBreaksInActivities($dailyActivitiesActual);
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Breaks not added correctly.");
    }
    
    public function testAddBreaksInActivitiesOverlapped(){
        $dailyActivitiesActual = array();
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 18:18:00"));
        array_push($dailyActivitiesActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 13:18:01", "punchout"=>"2018-07-09 22:20:00"));
        $dailyActivitiesExpected = array();
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 18:18:00"));
        array_push($dailyActivitiesExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 13:18:01", "punchout"=>"2018-07-09 22:20:00"));
        $this->default_Model_EmployeeWorkActivity->addBreaksInActivities($dailyActivitiesActual);
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Breaks should not added.");
    }
    
    public function testAddBreaksInActivitiesWithMissedPunches(){
        $dailyActivitiesActual = array();
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>null));
        array_push($dailyActivitiesActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 13:18:01", "punchout"=>"2018-07-09 22:20:00"));
        $dailyActivitiesExpected = array();
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>null));
        array_push($dailyActivitiesExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 13:18:01", "punchout"=>"2018-07-09 22:20:00"));
        $this->default_Model_EmployeeWorkActivity->addBreaksInActivities($dailyActivitiesActual);
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Breaks should not added.");
    }
    
    public function testAddBreaksInActivitiesUnsorted(){
        $dailyActivitiesActual = array();
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 12:18:01", "punchout"=>"2018-07-09 14:20:00"));
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 11:18:01"));  
        $dailyActivitiesExpected = array();
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 12:18:01", "punchout"=>"2018-07-09 14:20:00"));
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 11:18:01"));  
        $this->default_Model_EmployeeWorkActivity->addBreaksInActivities($dailyActivitiesActual);
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Breaks should not added .");
    }
    
    public function testAddBreaksInActivitiesWithoutActivities(){
        $dailyActivitiesActual = array();
        $dailyActivitiesExpected = array();
        $this->default_Model_EmployeeWorkActivity->addBreaksInActivities($dailyActivitiesActual);
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Breaks not added correctly.");
    }
    
    public function testAddInactivitiesForZeroActivity(){
        $dailyActivitiesActual = array();
        $dailyActivitiesExpected = array();
        $this->default_Model_EmployeeWorkActivity->addInactivities($dailyActivitiesActual,'+330 minutes',false);
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Inactivities should not be added.");
    }
    
    public function testAddInactivities(){
        $dailyActivitiesActual = array();
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesActual,array("punchin" => "2018-07-09 13:18:01", "punchout"=>"2018-07-09 22:17:59", "status" =>"Inactivity"));
        array_push($dailyActivitiesActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 22:18:00", "punchout"=>"2018-07-09 22:20:00"));
        $dailyActivitiesExpected = array();
        array_push($dailyActivitiesExpected,array("punchin" => "2018-07-09 00:00:00", "punchout"=>"2018-07-09 09:17:59", "status" =>"Inactivity"));
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesExpected,array("punchin" => "2018-07-09 13:18:01", "punchout"=>"2018-07-09 22:17:59", "status" =>"Inactivity"));
        array_push($dailyActivitiesExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 22:18:00", "punchout"=>"2018-07-09 22:20:00"));
        array_push($dailyActivitiesExpected,array("punchin" => "2018-07-09 22:20:01", "punchout"=>"2018-07-09 23:59:59", "status" =>"Inactivity"));
        $this->default_Model_EmployeeWorkActivity->addInactivities($dailyActivitiesActual,'+0 minutes',false);
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Inactivities add incorrectly.");
    }
    
    public function testAddInactivitiesForMissedPunchAfterOfficeHours(){
        $dailyActivitiesActual = array();
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesActual,array("punchin" => "2018-07-09 13:18:01", "punchout"=>"2018-07-09 09:17:59", "status" =>"Inactivity"));
        array_push($dailyActivitiesActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 22:18:00", "punchout"=>null));
        $dailyActivitiesExpected = array();
        array_push($dailyActivitiesExpected,array("punchin" => "2018-07-09 00:00:00", "punchout"=>"2018-07-09 09:17:59", "status" =>"Inactivity"));
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesExpected,array("punchin" => "2018-07-09 13:18:01", "punchout"=>"2018-07-09 09:17:59", "status" =>"Inactivity"));
        array_push($dailyActivitiesExpected,array("status" =>"Missed Punch", "punchin" => "2018-07-09 22:18:00", "punchout"=>"2018-07-09 22:18:01"));
        array_push($dailyActivitiesExpected,array("punchin" => "2018-07-09 22:18:02", "punchout"=>"2018-07-09 23:59:59", "status" =>"Inactivity"));
        $this->default_Model_EmployeeWorkActivity->addInactivities($dailyActivitiesActual,'+0 minutes',false);
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Inactivities added incorrectly.");
    }
    


    
    public function testCombineConsecutiveOfficeActivitiesForZeroActivity(){
        $dailyActivitiesActual = array();
        $dailyActivitiesExpected = array();
        $dailyActivitiesActual = $this->default_Model_EmployeeWorkActivity->combineConsecutiveOfficeActivities($dailyActivitiesActual);
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Inactivities should not be added.");
    }
    
    public function testCombineConsecutiveOfficeActivities(){
        $dailyActivitiesActual = array();
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 15:18:00", "punchout"=>"2018-07-09 16:20:00"));
        array_push($dailyActivitiesActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 18:18:00", "punchout"=>"2018-07-09 22:20:00"));
        $dailyActivitiesExpected = array();
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 15:18:00", "punchout"=>"2018-07-09 22:20:00"));
        $dailyActivitiesActual = $this->default_Model_EmployeeWorkActivity->combineConsecutiveOfficeActivities($dailyActivitiesActual);
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Inactivities combined incorrectly.");
    }
    
    public function testCombineConsecutiveWFHActivities(){
        $dailyActivitiesActual = array();
        array_push($dailyActivitiesActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 15:18:00", "punchout"=>"2018-07-09 16:20:00"));
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 18:18:00", "punchout"=>"2018-07-09 22:20:00"));
        $dailyActivitiesExpected = array();
        array_push($dailyActivitiesExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 15:18:00", "punchout"=>"2018-07-09 16:20:00"));
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 18:18:00", "punchout"=>"2018-07-09 22:20:00"));
        $dailyActivitiesActual = $this->default_Model_EmployeeWorkActivity->combineConsecutiveOfficeActivities($dailyActivitiesActual);
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Inactivities must not be combined.");
    }
    
    
    public function testProcessOverlappedActivitiesForZeroActivity(){
        $dailyActivitiesActual = array();
        $dailyActivitiesExpected = array();
        $this->default_Model_EmployeeWorkActivity->processOverlappedActivities($dailyActivitiesActual,null,null,null,null);
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Activities must not change.");
    }
    
    public function testProcessUnoverlappedActivities(){
        $dailyActivitiesActual = array();
        array_push($dailyActivitiesActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 15:18:00", "punchout"=>"2018-07-09 16:20:00"));
        $dailyActivitiesExpected = array();
        array_push($dailyActivitiesExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:18:00"));
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 15:18:00", "punchout"=>"2018-07-09 16:20:00"));
        $this->default_Model_EmployeeWorkActivity->processOverlappedActivities($dailyActivitiesActual,new DateTime("2018-07-09 13:18:00"),new DateTime("2018-07-09 09:18:00"),new DateTime("2018-07-09 15:18:00"),new DateTime("2018-07-09 16:20:00"));
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Activities must not change.");
    }
    
    
    public function testProcessOverlappedActivitiesOfficeHoursInsideWFH(){
        $dailyActivitiesActual = array();
        array_push($dailyActivitiesActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 15:18:00", "punchout"=>"2018-07-09 16:18:00"));
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 19:20:00"));
        $dailyActivitiesExpected = array();
        array_push($dailyActivitiesExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 15:18:00", "punchout"=>"2018-07-09 16:18:00"));
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 15:17:59"));
        array_push($dailyActivitiesExpected,array("punchin" => "2018-07-09 16:18:01", "punchout"=>"2018-07-09 19:20:00","status" =>"WFH"));
        $this->default_Model_EmployeeWorkActivity->processOverlappedActivities($dailyActivitiesActual,new DateTime("2018-07-09 16:18:00"),new DateTime("2018-07-09 15:18:00"),new DateTime("2018-07-09 19:20:00"),new DateTime("2018-07-09 09:18:00"));
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Activities processed incorrectly.");
    }
    
    public function testProcessOverlappedActivitiesWFHInsideOfficeHours(){
        $dailyActivitiesActual = array();
        array_push($dailyActivitiesActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 19:20:00"));
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 15:18:00", "punchout"=>"2018-07-09 16:18:00"));
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 17:18:00", "punchout"=>"2018-07-09 18:18:00"));
        $dailyActivitiesExpected = array();
        array_push($dailyActivitiesExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 15:17:59"));
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 15:18:00", "punchout"=>"2018-07-09 16:18:00"));
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 17:18:00", "punchout"=>"2018-07-09 18:18:00"));
        array_push($dailyActivitiesExpected,array("punchin" => "2018-07-09 18:18:01", "punchout"=>"2018-07-09 19:20:00","status" =>"Office Hours"));
        $this->default_Model_EmployeeWorkActivity->processOverlappedActivities($dailyActivitiesActual,new DateTime("2018-07-09 19:20:00"),new DateTime("2018-07-09 09:18:00"),new DateTime("2018-07-09 18:18:00"),new DateTime("2018-07-09 15:18:00"));
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Activities processed incorrectly.");
    }
    
    public function testProcessOverlappedActivitiesWFHBeforeOfficeHours(){
        $dailyActivitiesActual = array();
        array_push($dailyActivitiesActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 13:18:00", "punchout"=>"2018-07-09 19:20:00"));
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 16:18:00"));
        $dailyActivitiesExpected = array();
        array_push($dailyActivitiesExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 13:18:00", "punchout"=>"2018-07-09 16:17:59"));
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:17:59"));
        array_push($dailyActivitiesExpected,array("punchin" => "2018-07-09 16:18:01", "punchout"=>"2018-07-09 19:20:00","status" =>"Office Hours"));
        $this->default_Model_EmployeeWorkActivity->processOverlappedActivities($dailyActivitiesActual,new DateTime("2018-07-09 19:20:00"),new DateTime("2018-07-09 13:18:00"),new DateTime("2018-07-09 16:18:00"),new DateTime("2018-07-09 09:18:00"));
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Activities processed incorrectly.");
    }
    
    public function testProcessOverlappedActivitiesWFHAfterOfficeHours(){
        $dailyActivitiesActual = array();
        array_push($dailyActivitiesActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 16:18:00"));
        array_push($dailyActivitiesActual,array("status" =>"WFH", "punchin" => "2018-07-09 13:18:00", "punchout"=>"2018-07-09 19:20:00"));
        $dailyActivitiesExpected = array();
        array_push($dailyActivitiesExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:17:59"));
        array_push($dailyActivitiesExpected,array("status" =>"WFH", "punchin" => "2018-07-09 13:18:00", "punchout"=>"2018-07-09 16:17:59"));
        array_push($dailyActivitiesExpected,array("punchin" => "2018-07-09 16:18:01", "punchout"=>"2018-07-09 19:20:00","status" =>"WFH"));
        $this->default_Model_EmployeeWorkActivity->processOverlappedActivities($dailyActivitiesActual,new DateTime("2018-07-09 16:18:00"),new DateTime("2018-07-09 09:18:00"),new DateTime("2018-07-09 19:20:00"),new DateTime("2018-07-09 13:18:00"));
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Activities processed incorrectly.");
    }
    
    public function testProcessMissedPunchesNoActivity(){
        $dailyActivitiesActual = array();
        $dailyActivitiesExpected = array();
        $this->default_Model_EmployeeWorkActivity->processMissedPunches($dailyActivitiesActual,null,null,null,null,null);
        $this->assertTrue($dailyActivitiesActual === $dailyActivitiesExpected,"Activities processed incorrectly.");
        
    }
    
    public function testProcessMissedPunchesBothMissedPunches(){
        $dailyActivitiesOfficeWFHActual = array();
        $dailyActivitiesWFHOfficeActual = array();
        array_push($dailyActivitiesOfficeWFHActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>null));
        array_push($dailyActivitiesOfficeWFHActual,array("status" =>"WFH", "punchin" => "2018-07-09 13:18:00", "punchout"=>null));
        array_push($dailyActivitiesWFHOfficeActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 13:18:00", "punchout"=>null));
        array_push($dailyActivitiesWFHOfficeActual,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>null));
        $dailyActivitiesWFHOfficeExpected = array();
        $dailyActivitiesOfficeWFHExpected = array();
        array_push($dailyActivitiesOfficeWFHExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:17:59"));
        array_push($dailyActivitiesOfficeWFHExpected,array("status" =>"WFH", "punchin" => "2018-07-09 13:18:00", "punchout"=>null));
        array_push($dailyActivitiesWFHOfficeExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 13:18:00", "punchout"=>null));
        array_push($dailyActivitiesWFHOfficeExpected,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:17:59"));        
        $this->default_Model_EmployeeWorkActivity->processMissedPunches($dailyActivitiesOfficeWFHActual,null,new DateTime("2018-07-09 09:18:00"),null,new DateTime("2018-07-09 13:18:00"),new DateTime("2018-07-09"));
        $this->default_Model_EmployeeWorkActivity->processMissedPunches($dailyActivitiesWFHOfficeActual,null,new DateTime("2018-07-09 13:18:00"),null,new DateTime("2018-07-09 09:18:00"),new DateTime("2018-07-09"));
        $this->assertTrue($dailyActivitiesWFHOfficeExpected === $dailyActivitiesWFHOfficeActual,"Activities processed incorrectly.");
        $this->assertTrue($dailyActivitiesOfficeWFHExpected === $dailyActivitiesOfficeWFHActual,"Activities processed incorrectly.");
        
        
    }
    
    public function testProcessMissedPunchesBefore(){
        $dailyActivitiesOfficeActual = array();
        $dailyActivitiesWFHActual = array();
        array_push($dailyActivitiesOfficeActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>null));
        array_push($dailyActivitiesOfficeActual,array("status" =>"WFH", "punchin" => "2018-07-09 13:18:00", "punchout"=>"2018-07-09 19:18:00"));
        array_push($dailyActivitiesWFHActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 13:18:00", "punchout"=>"2018-07-09 19:18:00"));
        array_push($dailyActivitiesWFHActual,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>null));
        $dailyActivitiesWFHExpected = array();
        $dailyActivitiesOfficeExpected = array();
        array_push($dailyActivitiesOfficeExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:17:59"));
        array_push($dailyActivitiesOfficeExpected,array("status" =>"WFH", "punchin" => "2018-07-09 13:18:00",  "punchout"=>"2018-07-09 19:18:00"));
        array_push($dailyActivitiesWFHExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 13:18:00", "punchout"=>"2018-07-09 19:18:00"));
        array_push($dailyActivitiesWFHExpected,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 13:17:59"));
        $this->default_Model_EmployeeWorkActivity->processMissedPunches($dailyActivitiesOfficeActual,null,new DateTime("2018-07-09 09:18:00"),null,new DateTime("2018-07-09 13:18:00"),new DateTime("2018-07-09"));
        $this->default_Model_EmployeeWorkActivity->processMissedPunches($dailyActivitiesWFHActual,null,new DateTime("2018-07-09 13:18:00"),null,new DateTime("2018-07-09 09:18:00"),new DateTime("2018-07-09"));
        $this->assertTrue($dailyActivitiesOfficeActual === $dailyActivitiesOfficeExpected,"Activities processed incorrectly.");
        $this->assertTrue($dailyActivitiesWFHActual === $dailyActivitiesWFHExpected,"Activities processed incorrectly.");
        
        
    }
    
    public function testProcessMissedPunchesAfter(){
        $dailyActivitiesOfficeActual = array();
        $dailyActivitiesWFHActual = array();
        array_push($dailyActivitiesOfficeActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 17:18:00", "punchout"=>null));
        array_push($dailyActivitiesOfficeActual,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 15:18:00"));
        array_push($dailyActivitiesWFHActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 15:18:00"));
        array_push($dailyActivitiesWFHActual,array("status" =>"WFH", "punchin" => "2018-07-09 17:18:00", "punchout"=>null));
        $dailyActivitiesWFHExpected = array();
        $dailyActivitiesOfficeExpected = array();
        array_push($dailyActivitiesOfficeExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 17:18:00", "punchout"=>null));
        array_push($dailyActivitiesOfficeExpected,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00",  "punchout"=>"2018-07-09 15:18:00"));
        array_push($dailyActivitiesWFHExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 15:18:00"));
        array_push($dailyActivitiesWFHExpected,array("status" =>"WFH", "punchin" => "2018-07-09 17:18:00", "punchout"=>null));
        $this->default_Model_EmployeeWorkActivity->processMissedPunches($dailyActivitiesOfficeActual,null,new DateTime("2018-07-09 17:18:00"),new DateTime("2018-07-09 15:18:00"),new DateTime("2018-07-09 09:18:00"),new DateTime("2018-07-09"));
        $this->default_Model_EmployeeWorkActivity->processMissedPunches($dailyActivitiesWFHActual,new DateTime("2018-07-09 15:18:00"),new DateTime("2018-07-09 09:18:00"),null,new DateTime("2018-07-09 17:18:00"),new DateTime("2018-07-09"));
        $this->assertTrue($dailyActivitiesOfficeActual === $dailyActivitiesOfficeExpected,"Activities processed incorrectly.");
        $this->assertTrue($dailyActivitiesWFHActual === $dailyActivitiesWFHExpected,"Activities processed incorrectly.");
        
        
    }
    
    public function testProcessMissedPunchesWithin(){
        $dailyActivitiesOfficeActual = array();
        $dailyActivitiesWFHActual = array();
        array_push($dailyActivitiesOfficeActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 17:18:00", "punchout"=>null));
        array_push($dailyActivitiesOfficeActual,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 19:18:00"));
        array_push($dailyActivitiesWFHActual,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 19:18:00"));
        array_push($dailyActivitiesWFHActual,array("status" =>"WFH", "punchin" => "2018-07-09 17:18:00", "punchout"=>null));
        $dailyActivitiesWFHExpected = array();
        $dailyActivitiesOfficeExpected = array();
        array_push($dailyActivitiesOfficeExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 17:18:00", "punchout"=>"2018-07-09 19:17:59"));
        array_push($dailyActivitiesOfficeExpected,array("status" =>"WFH", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 17:17:59"));        
        array_push($dailyActivitiesOfficeExpected,array("punchin" => "2018-07-09 19:18:00", "punchout"=>null));
        array_push($dailyActivitiesWFHExpected,array("status" =>"Office Hours", "punchin" => "2018-07-09 09:18:00", "punchout"=>"2018-07-09 17:17:59"));
        array_push($dailyActivitiesWFHExpected,array("status" =>"WFH", "punchin" => "2018-07-09 17:18:00", "punchout"=>"2018-07-09 19:17:59"));
        array_push($dailyActivitiesWFHExpected,array("punchin" => "2018-07-09 19:18:00", "punchout"=>null));
        $this->default_Model_EmployeeWorkActivity->processMissedPunches($dailyActivitiesOfficeActual,null,new DateTime("2018-07-09 17:18:00"),new DateTime("2018-07-09 19:18:00"),new DateTime("2018-07-09 09:18:00"),new DateTime("2018-07-09"));
        $this->default_Model_EmployeeWorkActivity->processMissedPunches($dailyActivitiesWFHActual,new DateTime("2018-07-09 19:18:00"),new DateTime("2018-07-09 09:18:00"),null,new DateTime("2018-07-09 17:18:00"),new DateTime("2018-07-09"));
        $this->assertTrue($dailyActivitiesOfficeActual === $dailyActivitiesOfficeExpected,"Activities processed incorrectly.");
        $this->assertTrue($dailyActivitiesWFHActual === $dailyActivitiesWFHExpected,"Activities processed incorrectly.");
        
        
    }
    
   
    
    
    
    
    
    
    
    
    
}