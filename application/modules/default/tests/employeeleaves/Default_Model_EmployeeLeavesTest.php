<?php
require_once '..\Default_PHPUnit_TestCase.php';
require_once 'application\modules\default\models\Employeeleaves.php';

class Default_Model_EmployeeLeavesTest extends Default_PHPUnit_Framework_TestCase
{

    private $empLeavesModel;

    private $leavetype;

    protected function setUp()
    {
        parent::setUp();

        $db = Zend_Db::factory('Pdo_Mysql', array(
            'host' => SENTRIFUGO_HOST,
            'username' => SENTRIFUGO_USERNAME,
            'password' => SENTRIFUGO_PASSWORD,
            'dbname' => SENTRIFUGO_DBNAME
        ));
        $this->empLeavesModel = new Default_Model_Employeeleaves(array(
            'db' => $db
        ));

        $this->leavetype = array(
            'leavepreallocated' => 1,
            'cancarryforward' => 1,
            'used_leaves' => 0,
            'numberofdays' => 12
        );
    }

    protected function tearDown()
    {
        $this->empLeavesModel = null;
        parent::tearDown();
    }

    public function testFirstRunAndAllocationIsQuaterlyAndEmployeeIsFullTime()
    {
        $this->leavetype['allotmentmode'] = 'Quarterly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1
        );

        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $allocationmode->setOnsetDate(date("2018-01-01"));
        $allocationmode->allocate($dummyEmployee);

        $this->assertEquals(3, $dummyEmployee['leavetype']['emp_leave_limit']);
    }

    public function testUsedLeavesAndAllocationIsQuaterlyAndEmployeeIsFullTime()
    {
        $this->leavetype['allotmentmode'] = 'Quarterly';
        $this->leavetype['used_leaves'] = 2;
        $this->leavetype['modifieddate'] = date("2018-01-01");
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1
        );

        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $allocationmode->setOnsetDate(date("2018-01-02"));
        $allocationmode->allocate($dummyEmployee);

        $this->assertEquals(1, $dummyEmployee['leavetype']['emp_leave_limit']);
    }

    public function testUsedLeavesAndAllocationIsQuaterlyAndEmployeeIsFullTimeAndModifiedDateIsOut()
    {
        $this->leavetype['allotmentmode'] = 'Quarterly';
        $this->leavetype['used_leaves'] = 2;
        $this->leavetype['modifieddate'] = date("2018-01-02");
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1
        );

        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $allocationmode->setOnsetDate(date("2018-04-01"));
        $allocationmode->allocate($dummyEmployee);

        $this->assertEquals(4, $dummyEmployee['leavetype']['emp_leave_limit']);
    }

    public function testUsedLeavesAndAllocationIsQuaterlyAndEmployeeIsFullTimeAndModifiedDateIsNotCurrentYear()
    {
        $this->leavetype['allotmentmode'] = 'Quarterly';
        $this->leavetype['used_leaves'] = 2;
        $this->leavetype['modifieddate'] = date("2018-01-02");
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1
        );

        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $allocationmode->setOnsetDate(date("2019-01-01"));
        $allocationmode->allocate($dummyEmployee);

        $this->assertEquals(3, $dummyEmployee['leavetype']['emp_leave_limit']);
    }

    public function testAllocationIsMonthlyAndEmployeeIsFullTime()
    {
        $this->leavetype['allotmentmode'] = 'Monthly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1
        );

        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $allocationmode->setOnsetDate(date("2018-01-01"));
        $allocationmode->allocate($dummyEmployee);

        $this->assertEquals(1, $dummyEmployee['leavetype']['emp_leave_limit']);
    }

    public function testUsedLeavesAndAllocationIsMonthlyAndEmployeeIsFullTimeAndModifiedIsIn()
    {
        $this->leavetype['allotmentmode'] = 'Monthly';
        $this->leavetype['used_leaves'] = 1;
        $this->leavetype['modifieddate'] = date("2018-01-02");
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1
        );

        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $allocationmode->setOnsetDate(date("2018-08-31"));
        $allocationmode->allocate($dummyEmployee);

        $this->assertEquals(7, $dummyEmployee['leavetype']['emp_leave_limit']);
    }

    public function testUsedLeavesAndAllocationIsMonthlyAndEmployeeIsFullTimeAndModifiedIsInAndNoCarry()
    {
        $this->leavetype['allotmentmode'] = 'Monthly';
        $this->leavetype['used_leaves'] = 1;
        $this->leavetype['modifieddate'] = date("2018-01-02");
        $this->leavetype['cancarryforward'] = 0;
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1
        );

        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $allocationmode->setOnsetDate(date("2018-01-31"));
        $allocationmode->allocate($dummyEmployee);

        $this->assertEquals(0, $dummyEmployee['leavetype']['emp_leave_limit']);
    }

    public function testUsedLeavesAndAllocationIsMonthlyAndEmployeeIsFullTime()
    {
        $this->leavetype['allotmentmode'] = 'Monthly';
        $this->leavetype['used_leaves'] = 1;
        $this->leavetype['modifieddate'] = date("2018-01-02");
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1
        );

        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $allocationmode->setOnsetDate(date("2018-02-28"));
        $allocationmode->allocate($dummyEmployee);

        $this->assertEquals(1, $dummyEmployee['leavetype']['emp_leave_limit']);
    }

    public function testUsedLeavesAndAllocationIsMonthlyAndEmployeeIsFullTimeNoCarry()
    {
        $this->leavetype['allotmentmode'] = 'Monthly';
        $this->leavetype['used_leaves'] = 1;
        $this->leavetype['modifieddate'] = date("2018-01-02");
        $this->leavetype['cancarryforward'] = 0;
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1
        );

        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $allocationmode->setOnsetDate(date("2018-02-28"));
        $allocationmode->allocate($dummyEmployee);

        $this->assertEquals(1, $dummyEmployee['leavetype']['emp_leave_limit']);
    }
    
    public function testCountLeavesYearlyJanStart()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-01-01")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(10, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    } 
    
    public function testCountLeavesYearlyJanMid()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-01-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(10, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyJanEnd()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-01-16")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(9, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyFebStart()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-02-02")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(9, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyFebMid()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-02-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(9, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyFebEnd()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-02-28")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(8.5, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyMarStart()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-03-02")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyMarMid()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-03-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyMarEnd()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-03-17")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyAprStart()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-04-04")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyAprMid()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-04-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyAprEnd()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-04-18")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyMayStart()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-05-05")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyMayMid()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-05-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyMayEnd()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-05-19")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyJuneStart()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-06-06")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyJuneMid()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-06-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyJuneEnd()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-06-20")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyJulStart()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-07-07")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyJulMid()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-07-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyJulEnd()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-07-21")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyAugStart()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-08-08")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyAugMid()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-08-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyAugEnd()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-08-22")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlySepStart()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-09-09")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlySepMid()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-09-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlySepEnd()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-09-23")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyOctStart()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-10-10")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyOctMid()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-10-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyOctEnd()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-10-24")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyNovStart()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-11-11")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyNovMid()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-11-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyNovEnd()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-11-30")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyDecStart()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-12-12")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyDecMid()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-12-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyDecEnd()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2018-12-31")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(0, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyPrevJan()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2017-01-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(10, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyPrevJun()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2017-06-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(10, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
    
    public function testCountLeavesYearlyPrevDec()
    {
        $this->leavetype['allotmentmode'] = 'Yearly';
        $dummyEmployee = array(
            'leavetype' => $this->leavetype,
            'emp_status_id' => 1,
            'date_of_joining' => date("2017-12-15")
        );
        
        $numberofdays = 10;
        $allocationmode = $this->empLeavesModel->getAllocationModeByType($this->leavetype);
        $allocationmode->setEmployeeLeavesModel($this->empLeavesModel);
        $this->assertEquals(10, $allocationmode->countLeaves($dummyEmployee, $numberofdays));
    }
}