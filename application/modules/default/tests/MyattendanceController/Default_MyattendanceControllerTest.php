<?php
require '..\\Default_PHPUnit_TestCase.php';
require 'application\modules\default\controllers\MyattendanceController.php';

class Default_MyattendanceControllerTest extends Default_PHPUnit_Framework_TestCase
{
    private $default_MyattendanceController;
    
    protected function setUp()
    {
        parent::setUp();
        
        $db = Zend_Db::factory('Pdo_Mysql', array(
            'host' => SENTRIFUGO_HOST,
            'username' => SENTRIFUGO_USERNAME,
            'password' => SENTRIFUGO_PASSWORD,
            'dbname' => SENTRIFUGO_DBNAME
        ));
        
        $this->default_MyattendanceController = new Default_MyattendanceController(array('db' => $db));
    }
    
    protected function tearDown()
    {
        $this->default_MyattendanceController = null;
        parent::tearDown();
    }
    
    public function testValidformatHMS()
    {
        $duration=6.3;
        $this->assertEquals('6h 3m', $this->default_MyattendanceController->formatHMS($duration));
    }
}
?>