<?php
class Default_Model_EmployeeWorkActivity extends Zend_Db_Table_Abstract {
	protected $_name = 'main_emp_work_activity';
	protected $_primary = 'id';
	public function SaveorUpdateData($data, $where) {
		if ($where != '') {
			$this->update ( $data, $where );
			return 'update';
		} else {
			$this->insert ( $data );
			$id = $this->getAdapter ()->lastInsertId ( 'main_emp_work_activity' );
			return $id;
		}
	}
    public function getWFHActivitiesOn($userId, $date) {
        $db = Zend_Db_Table::getDefaultAdapter ();
        $query = $db->query ( "Select * from main_emp_work_activity where user_id=" . $userId . " AND status = 'WFH' AND DATE(punch_time) = STR_TO_DATE('" . $date->format ( DATEFORMAT_PHP ) . "','" . DATEFORMAT_MYSQL . "')" );
        return $query->fetchAll ();
    }
	public function getActivitiesOn($userId, $date) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = $db->query ( "Select * from main_emp_work_activity where user_id=" . $userId . " AND DATE(punch_time) = STR_TO_DATE('" . $date->format ( DATEFORMAT_PHP ) . "','" . DATEFORMAT_MYSQL . "')" );
		return $query->fetchAll ();
	}
	public function getActivitiesBetween($userId, $curDate, $officeHoursRule, $modifier) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$date = new DateTime ( $curDate );
		if ($officeHoursRule == null || $officeHoursRule == false) {
			$dayStartTime = $date->setTime ( 0, 0, 0 );
			$dayStartTime->modify ( $this->invertModifier ( $modifier ) );
			$dayEndTime = new DateTime ( $dayStartTime->format ( "Y-m-d H:i:s" ) );
			$dayEndTime->modify ( '+23 hours 59 minutes 59 seconds' );
		} else {
			$dayStartTime = new DateTime ( $date->format ( 'Y-m-d' ) . ' ' . $officeHoursRule ['start_time'] );
			$dayStartTime->modify ( $this->invertModifier ( $modifier ) );
			$dayEndTime = new DateTime ( $dayStartTime->format ( "Y-m-d H:i:s" ) );
			$dayEndTime->modify ( '+23 hours 59 minutes 59 seconds' );
			if ($officeHoursRule ['is_entry_threshold'] == 1 && $officeHoursRule ['entry_threshold_action'] == 1) {
				$dayStartTime = new DateTime ( $date->format ( 'Y-m-d' ) . ' ' . $officeHoursRule ['entry_time'] );
				$dayStartTime->modify ( $this->invertModifier ( $modifier ) );
			}
			if ($officeHoursRule ['is_exit_threshold'] == 1 && $officeHoursRule ['exit_threshold_action'] == 1) {
				$dayEndTime = new DateTime ( $date->format ( 'Y-m-d' ) . ' ' . $officeHoursRule ['exit_time'] );
				$dayEndTime->modify ( $this->invertModifier ( $modifier ) );
				if ($dayEndTime < $dayStartTime) {
					$dayEndTime->modify ( "+24 hours" );
				}
			}
		}
		
		$query = $db->query ( "Select * from main_emp_work_activity where user_id=" . $userId . " AND punch_time between STR_TO_DATE('" . $dayStartTime->format ( DATEFORMAT_PHP . " H:i:s" ) . "','" . DATEFORMAT_MYSQL . " %H:%i:%s') and STR_TO_DATE('" . $dayEndTime->format ( DATEFORMAT_PHP . " H:i:s" ) . "','" . DATEFORMAT_MYSQL . " %H:%i:%s') order by punch_time" );
		return $query->fetchAll ();
	}
	public function invertModifier($modifier) {
		$newModifier = '';
		if (strpos ( $modifier, "+" ) === false) {
			$newModifier = str_ireplace ( "-", "+", $modifier );
		} else {
			$newModifier = str_ireplace ( "+", "-", $modifier );
		}
		return $newModifier;
	}

	public function createActivityInterval($activitiesData, &$dailyActivity, $modifier) {
		$dailyActivityWFH = array ();
		$dailyActivityOffice = array ();
        $punchInfo = array();
        $punchInfoOffice = array();

		foreach ( $activitiesData as $key => $activityData ) {
			if ($activityData ['status'] == 'WFH') {
				$activityData ['status'] = 'Work From Home';
				if ($punchInfo ['status'] == null) {
					
					$punchInfo ['status'] = $activityData ['status'];
					$punchInfo ['punchin'] = $activityData ['punch_time'];
					if ($activitiesData [$key + 1] == null) {
						if ((new DateTime ( $activityData ['punch_time'] ))->format ( "Y-m-d" ) == (new DateTime ())->format ( "Y-m-d" )) {
							$punchInfo ['punchout'] = (new DateTime ( gmdate ( "Y-m-d H:i:s" ) ))->modify ( $modifier )->format ( "Y-m-d H:i:s" );
						} else {
							$punchInfo ['punchout'] = null;
						}
						array_push ( $dailyActivityWFH, $punchInfo );
					}
				} else {
					$punchInfo ['punchout'] = $activityData ['punch_time'];
					array_push ( $dailyActivityWFH, $punchInfo );
					$punchInfo ['status'] = null;
				}
			} else if ($activityData ['status'] == 'OFFICE') {
				if ($punchInfoOffice ['status'] == null) {
					
					$punchInfoOffice ['status'] = $activityData ['status'];
					$punchInfoOffice ['punchin'] = $activityData ['punch_time'];
					if ($activitiesData [$key + 1] == null) {
						if ((new DateTime ( $activityData ['punch_time'] ))->format ( "Y-m-d" ) == (new DateTime ())->format ( "Y-m-d" )) {
							if (((new DateTime ( gmdate ( "Y-m-d H:i:s" ) ))->modify ( $modifier )->format ( "Y-m-d H:i:s" )) > ((new DateTime ( $activityData ['punch_time'] ))->format ( "Y-m-d H:i:s" )))
							{
								$punchInfoOffice ['punchout'] = (new DateTime ( gmdate ( "Y-m-d H:i:s" ) ))->modify ( $modifier )->format ( "Y-m-d H:i:s" );
							}
							else {
								$punchInfoOffice ['punchout'] = null;
							}
						} else {
							$punchInfoOffice ['punchout'] = null;
						}
						array_push ( $dailyActivityOffice, $punchInfoOffice );
					}
				} else {
					$punchInfoOffice ['punchout'] = $activityData ['punch_time'];
					array_push ( $dailyActivityOffice, $punchInfoOffice );
					$punchInfoOffice ['status'] = null;
				}
			}
		}
		$dailyActivity = array_merge ( $dailyActivity, $dailyActivityWFH, $dailyActivityOffice );

        $mixedMode = false;

        if (($dailyActivityWFH != null && count ( $dailyActivityWFH ) > 0) && ($dailyActivityOffice != null && count ( $dailyActivityOffice ) > 0)) {
            $mixedMode = true;
        }

        usort ( $dailyActivity, function ($a, $b) {
            if (new DateTime ( $a ['punchin'] ) == new DateTime ( $b ['punchin'] ))
                return 0;
            return (new DateTime ( $a ['punchin'] ) < new DateTime ( $b ['punchin'] )) ? - 1 : 1;
        } );

        if ($mixedMode) {
            $this->processMissedPunchesAndOverlapping ( $dailyActivity );
        }
	}

	public function processMissedPunchesAndOverlapping(&$dailyActivity) {
		$lastMissedPunch = false;
		foreach ( $dailyActivity as $key => $activity ) {
			if ($activity ['punchin'] != null) {
				$currentPunchIn = new DateTime ( $activity ['punchin'] );
			}
			if ($key > 0 && $dailyActivity [$key - 1] ['punchout'] != null) {
				$lastPunchOut = new DateTime ( $dailyActivity [$key - 1] ['punchout'] );
			}
			if ($lastMissedPunch || ($lastPunchOut > $currentPunchIn)) {
				$dailyActivity [$key - 1] ['punchout'] = $currentPunchIn->modify ( '-1 second' )->format ( "Y-m-d H:i:s" );
				$lastMissedPunch = false;
			}
			
			if ($activity ['punchout'] == null) {
				$lastMissedPunch = true;
			}
		}
	}

	public function addBreaksInActivities(&$dailyActivity) {
		$dailyActivityList = $dailyActivity;
		for($i = 0; $i < (count ( $dailyActivityList ) - 1); $i ++) {
			$startTime = (new DateTime ( $dailyActivityList [$i] ['punchout'] ))->modify ( '+1 second' );
			$endTime = (new DateTime ( $dailyActivityList [$i + 1] ['punchin'] ))->modify ( '-1 second' );
			if ($startTime < $endTime) {
				$punchinfo ['punchin'] = $startTime->format ( "Y-m-d H:i:s" );
				$punchinfo ['punchout'] = $endTime->format ( "Y-m-d H:i:s" );
				$punchinfo ['status'] = 'Inactivity';
				array_push ( $dailyActivity, $punchinfo );
			}
		}
	}
	public function addInactivities(&$dailyActivity, $modifier, $officeHoursRule) {
		if (count ( $dailyActivity ) > 0) {
			if ($officeHoursRule == false) {
				$dayStartTime = (new DateTime ( $dailyActivity [0] ['punchin'] ))->setTime ( 0, 0, 0 );
			} else {
				$dayStartTime = new DateTime ( (new DateTime ( $dailyActivity [0] ['punchin'] ))->format ( 'Y-m-d' ) . ' ' . $officeHoursRule ['start_time'] );
			}
			$dayEndTime = new DateTime ( $dayStartTime->format ( "Y-m-d H:i:s" ) );
			$dayEndTime->modify ( '+23 hours 59 minutes 59 seconds' );
			
			$startTime = (new DateTime ( $dailyActivity [0] ['punchin'] ))->modify ( '-1 second' );
			$lastPunch = $dailyActivity [count ( $dailyActivity ) - 1] ['punchout'];
			if ($lastPunch != null) {
				$endTime = (new DateTime ( $dailyActivity [count ( $dailyActivity ) - 1] ['punchout'] ))->modify ( '+1 second' );
			} else {
				$lastPunchStart = new DateTime ( $dailyActivity [count ( $dailyActivity ) - 1] ['punchin'] );
				if ($lastPunchStart->format ( "Y-m-d" ) == (new DateTime ())->format ( "Y-m-d" )) {
					$dailyActivity [count ( $dailyActivity ) - 1] ['punchout'] = (new DateTime ( gmdate ( "Y-m-d H:i:s" ) ))->modify ( $modifier )->format ( "Y-m-d H:i:s" );
					$endTime = (new DateTime ( $dailyActivity [count ( $dailyActivity ) - 1] ['punchout'] ))->modify ( '+1 second' );
				} else {
					$dailyActivity [count ( $dailyActivity ) - 1] ['punchout'] = (new DateTime ( $lastPunchStart->format ( "Y-m-d H:i:s" ) ))->modify ( '+1 second' )->format ( "Y-m-d H:i:s" );
					$dailyActivity [count ( $dailyActivity ) - 1] ['status'] = 'Missed Punch';
					$endTime = (new DateTime ( $dailyActivity [count ( $dailyActivity ) - 1] ['punchout'] ))->modify ( '+1 second' );
				}
			}
			
			$punchinfo ['punchin'] = $dayStartTime->format ( "Y-m-d H:i:s" );
			$punchinfo ['punchout'] = $startTime->format ( "Y-m-d H:i:s" );
			$punchinfo ['status'] = 'Inactivity';
			array_unshift ( $dailyActivity, $punchinfo );
			
			$punchinfo ['punchin'] = $endTime->format ( "Y-m-d H:i:s" );
			$punchinfo ['punchout'] = $dayEndTime->format ( "Y-m-d H:i:s" );
			$punchinfo ['status'] = 'Inactivity';
			array_push ( $dailyActivity, $punchinfo );
		}
	}
	public function calculateTotalHours($dailyActivity) {
		$workHours = 0.0;
		$min = 0;
		foreach ( $dailyActivity as $activityData ) {
			if (strcmp ( $activityData ['status'], 'Inactivity' ) != 0) {
				$startTime = (new DateTime ( $activityData ['punchin'] ))->getTimestamp ();
				if ($activityData ['punchout'] != null) {
					$endTime = (new DateTime ( $activityData ['punchout'] ))->getTimestamp ();
					$min += floor ( ($endTime - $startTime) / 60 );
				}
			}
		}
		$workHours = floor ( $min / 60 ) + (($min % 60) / 100);
		return $workHours;
	}
	public function combineConsecutiveOfficeActivities($dailyActivity) {
		$dailyActivityList = array ();
		foreach ( $dailyActivity as $key => $activity ) {
			if ($key == 0) {
				array_push ( $dailyActivityList, $activity );
			} else {
				if (strcmp ( $activity ['status'], 'OFFICE' ) == 0 && strcmp ( $dailyActivityList [count ( $dailyActivityList ) - 1] ['status'], 'OFFICE' ) == 0) {
				    if($activity ['punchout'] != null) {
                        $dailyActivityList [count($dailyActivityList) - 1] ['punchout'] = $activity ['punchout'];
                    } else {
                        $dailyActivityList [count($dailyActivityList) - 1] ['punchout'] = $activity ['punchin'];
                    }
				} else {
					array_push ( $dailyActivityList, $activity );
				}
			}
		}
		
		return $dailyActivityList;
	}
	public function getLoggedDaysBetween($id, $fromdate, $todate) {
		$endDate = new DateTime ( $todate );
		$endDate->modify ( "+1 day" );
		$query = "SELECT count( distinct cast(punch_time as date) )as punch_count
                    FROM main_emp_work_activity
                    where user_id = " . $id . "
                    and punch_time between str_to_date('" . $fromdate . "','" . DATEFORMAT_MYSQL . "') and str_to_date('" . $endDate->format ( DATEFORMAT_PHP ) . "','" . DATEFORMAT_MYSQL . "');";
		
		$db = Zend_Db_Table::getDefaultAdapter ();
		return $db->query ( $query )->fetch ();
	}

    public function getEmployeeActivities($startDate, $id, &$workhours, $activityBreakdown) {
        $myattendancemodel = new Default_Model_Myattendance ();
        $officeHoursModel = new Default_Model_OfficeHours ();
        $employeeModel = new Default_Model_Employee ();
        $employeeInfo = $employeeModel->getActiveEmployeeData ( $id );
        if($employeeInfo [0] != null && $employeeInfo [0] ['businessunit_id'] != null) {
            $officeHoursRule = $officeHoursModel->getOfficeHoursRule($employeeInfo [0] ['businessunit_id']);
        }
        $dailyActivity = array ();
        $timezone = $myattendancemodel->getCurrentTimeZone ();
        $modifier = $myattendancemodel->getModifierStringFromOfffset ( $timezone [0] ['offet_value'] );

        $activitiesData = $this->getActivitiesBetween ( $id, $startDate->format ( DATEFORMAT_PHP ), $officeHoursRule, $modifier );

        foreach ( $activitiesData as $key => $activityData ) {
            $newTime = (new DateTime ( $activitiesData [$key] ['punch_time'] ))->modify ( $modifier );
            $activitiesData [$key] ['punch_time'] = $newTime->format ( "Y-m-d H:i:s" );
        }

        $this->createActivityInterval ( $activitiesData, $dailyActivity, $modifier );

        if ($activityBreakdown == "true") {
            $this->addBreaksInActivities ( $dailyActivity );
            usort ( $dailyActivity, function ($a, $b) {
                if (new DateTime ( $a ['punchin'] ) == new DateTime ( $b ['punchin'] ))
                    return 0;
                return (new DateTime ( $a ['punchin'] ) < new DateTime ( $b ['punchin'] )) ? - 1 : 1;
            } );
            $this->addInactivities ( $dailyActivity, $modifier, $officeHoursRule );
            $dailyActivity = $this->combineConsecutiveOfficeActivities ( $dailyActivity );
        } else {
            $dailyActivity = $this->combineConsecutiveOfficeActivities ( $dailyActivity );
            usort ( $dailyActivity, function ($a, $b) {
                if (new DateTime ( $a ['punchin'] ) == new DateTime ( $b ['punchin'] ))
                    return 0;
                return (new DateTime ( $a ['punchin'] ) < new DateTime ( $b ['punchin'] )) ? - 1 : 1;
            } );
            $this->addBreaksInActivities ( $dailyActivity );
            usort ( $dailyActivity, function ($a, $b) {
                if (new DateTime ( $a ['punchin'] ) == new DateTime ( $b ['punchin'] ))
                    return 0;
                return (new DateTime ( $a ['punchin'] ) < new DateTime ( $b ['punchin'] )) ? - 1 : 1;
            } );
            $this->addInactivities ( $dailyActivity, $modifier, $officeHoursRule );
        }

        $workhours = $this->calculateTotalHours ( $dailyActivity );
        $workhours = number_format ( $workhours, 2 );
        return $dailyActivity;
    }
}