<?php

class HalfYearly extends AllotmentMode
{

    /**
     *
     * {@inheritdoc}
     *
     * @see AllotmentMode::allocate()
     */
    public function allocate(&$employee)
    {
        $year = date('Y', strtotime($this->getOnsetDate()));
        $totalWorkingDaysCurrentYear = $this->getEmployeeLeavesModel()->getDaysBetween(date($year . '-01-01'), $this->getOnsetDate());

        $leavetype = $employee['leavetype'];

        if ($leavetype['leavepreallocated'] == 1) {
            $halfs = ceil($totalWorkingDaysCurrentYear / 180);
        } else {
            $halfs = floor($totalWorkingDaysCurrentYear / 180);
        }

        $usedLeaves = 0;
        if ($leavetype['cancarryforward'] == 1) {
            $usedLeaves = $leavetype['used_leaves'];
        } else if (isset($leavetype['modifieddate']) && $this->isLastRunInRange($leavetype['modifieddate'])) {
            $usedLeaves = $leavetype['used_leaves'];
            $halfs = 1;
        } else {
            $halfs = 1;
        }

        if (isset($leavetype['modifieddate'])) {
            $modifiedyear = date('Y', strtotime($leavetype['modifieddate']));
            if ($year != $modifiedyear) {
                $usedLeaves = 0;
            }
        }

        if ($employee['emp_status_id'] == 1) {
            $employee['leavetype']['emp_leave_limit'] = (($leavetype['numberofdays'] * $halfs) / 2) - $usedLeaves;
        }
    }

    public function isLastRunInRange($runDate)
    {
        $month = date('m', strtotime($this->getOnsetDate()));
        $empLeavesModel = $this->getEmployeeLeavesModel();
        $first = $empLeavesModel->getFirstDayOfHalfYear($month / 6);
        $last = $empLeavesModel->getLastDayOfHalfYear($month / 6);
        $runDate = new DateTime($runDate);
        return $first <= $runDate && $runDate <= $last;
    }
    
    public function countLeaves(&$employee, $numberofdays)
    {
        return 0;
    }
}