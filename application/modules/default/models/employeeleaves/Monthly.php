<?php

class Monthly extends AllotmentMode
{

    /**
     *
     * {@inheritdoc}
     *
     * @see AllotmentMode::allocate()
     */
    public function allocate(&$employee)
    {
        $year = date('Y', strtotime($this->getOnsetDate()));
        $totalWorkingDaysCurrentYear = $this->getEmployeeLeavesModel()->getDaysBetween(date($year . '-01-01'), $this->getOnsetDate());

        $leavetype = $employee['leavetype'];

        if ($leavetype['leavepreallocated'] == 1) {
            $months = date('m', strtotime($this->getOnsetDate()));
        } else {
            $months = date('m', strtotime($this->getOnsetDate())) - 1;
        }

        $usedLeaves = 0;
        if ($leavetype['cancarryforward'] == 1) {
            $usedLeaves = $leavetype['used_leaves'];
        } else if (isset($leavetype['modifieddate']) && $this->isLastRunInRange($leavetype['modifieddate'])) {
            $usedLeaves = $leavetype['used_leaves'];
            $months = 1;
        } else {
            $months = 1;
        }

        if (isset($leavetype['modifieddate'])) {
            $modifiedyear = date('Y', strtotime($leavetype['modifieddate']));
            if ($year != $modifiedyear) {
                $usedLeaves = 0;
            }
        }

        if ($employee['emp_status_id'] == 1) {
            $employee['leavetype']['emp_leave_limit'] = (($leavetype['numberofdays'] * $months) / 12) - $usedLeaves;
        }
    }

    public function isLastRunInRange($runDate)
    {
        $month = date('F', strtotime($this->getOnsetDate()));
        $year = date('Y', strtotime($this->getOnsetDate()));
        $first = new DateTime("first day of $month $year");
        $last = new DateTime("last day of $month $year");
        $runDate = new DateTime($runDate);
        return $first <= $runDate && $runDate <= $last;
    }
    
    public function countLeaves(&$employee, $numberofdays)
    {
        return 0;
    }
}