<?php

abstract class AllotmentMode
{

    private $employeeLeavesModel;

    private $onsetDate;

    /**
     *
     * @param field_type $employeeLeavesModel
     */
    public function setEmployeeLeavesModel($employeeLeavesModel)
    {
        // For Unit Testing Only
        $this->employeeLeavesModel = $employeeLeavesModel;
    }

    /**
     *
     * @return the $employeeLeavesModel
     */
    protected function getEmployeeLeavesModel()
    {
        if (! $this->employeeLeavesModel) {
            $this->employeeLeavesModel = new Default_Model_Employeeleaves();
        }
        return $this->employeeLeavesModel;
    }

    /**
     *
     * @return the $onsetDate
     */
    protected function getOnsetDate()
    {
        if (! $this->onsetDate) {
            $this->onsetDate = date('Y-m-d');
        }
        return $this->onsetDate;
    }

    /**
     *
     * @param field_type $onsetDate
     */
    public function setOnsetDate($onsetDate)
    {
        // For Unit Testing Only
        $this->onsetDate = $onsetDate;
    }

    /**
     *
     * @return the $roundOfLeaves
     */
    public function roundOfLeaves($leaves)
    {
        $fraction = $leaves - floor($leaves);
        if ($fraction <= 1 / 4)
            return floor($leaves);
        if (3 / 4 >= $fraction && $fraction > 1 / 4)
            return (floor($leaves) + 1 / 2);
        if ($fraction >= 3 / 4)
            return ($leaves);
    }
    
    public function getMonthsBetween($startDate, $endDate)
    {
        $ts1 = strtotime($startDate);
        $ts2 = strtotime($endDate);
        $diff = ((date('Y', $ts2) - date('Y', $ts1)) * 12) + ((date('m', $ts2) - date('m', $ts1)));
        return $diff;
    }
    
    public abstract function allocate(&$employee);
    
    public abstract function countLeaves(&$employee, $numberofdays);
}