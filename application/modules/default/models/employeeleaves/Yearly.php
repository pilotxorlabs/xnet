<?php

class Yearly extends AllotmentMode
{

    /**
     *
     * {@inheritdoc}
     *
     * @see AllotmentMode::allocate()
     */
    public function allocate(&$employee)
    {
        $leavetype = $employee['leavetype'];
        $usedLeaves = 0;

        if ($leavetype['cancarryforward'] == 1 || (isset($leavetype['modifieddate']) && $this->isLastRunInRange($leavetype['modifieddate']))) {
            $usedLeaves = $leavetype['used_leaves'];
        }

        if ($employee['emp_status_id'] == 1) {
            $employee['leavetype']['emp_leave_limit'] = $leavetype['numberofdays'] - $usedLeaves;
        }
    }

    public function isLastRunInRange($runDate)
    {
        $empLeavesModel = $this->getEmployeeLeavesModel();
        $year = date('Y', strtotime($this->getOnsetDate()));
        $first = new DateTime("first day of January $year");
        $last = new DateTime("last day of December $year");
        $runDate = new DateTime($runDate);
        return $first <= $runDate && $runDate <= $last;
    }
    
    public function countLeaves(&$employee, $numberofdays)
    {
        $date_of_joining = $employee['date_of_joining'];
        $firstDay = date('Y-01-01');
        
        if ($this->getMonthsBetween($date_of_joining, date('Y-m-d')) >= 6) {
            if (date('Y') == date('Y', strtotime($date_of_joining))) {
                $months = 12 - $this->getMonthsBetween($firstDay, $date_of_joining);
                $joiningdate = date('d', strtotime($date_of_joining));
                
                if ($joiningdate <= '15')
                    $leaves = ($numberofdays * $months) / 12;
                else
                    $leaves = ($numberofdays * ($months - 1)) / 12;
                $leaves = $this->roundOfLeaves($leaves);
                return ($leaves);
            } else {
                return $numberofdays;
            }
        }
        return 0;
    }
}