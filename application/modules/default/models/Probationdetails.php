<?php
/********************************************************************************* 

 *  Copyright (C) 2014 Sapplica
 *   

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/

class Default_Model_Probationdetails extends Zend_Db_Table_Abstract
{
	protected $_name = 'main_empprobationdetails';
    protected $_primary = 'id';
	
	public function getexperiencedetailsData($sort, $by, $pageNo, $perPage,$searchQuery,$userid)
	{	
		$where = "isactive = 1  AND user_id = ".$userid." " ;
		
		if($searchQuery != '')
			$where .= " AND ".$searchQuery." ";
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$expData = $this->select()
					->from(array('ep'=>'main_empprobationdetails'),array('id'=>'id','start_date'=>'DATE_FORMAT(start_date,"'.DATEFORMAT_MYSQL.'")','end_date'=>'DATE_FORMAT(end_date,"'.DATEFORMAT_MYSQL.'")','duration'=>'duration','status'=>'status','comments'=>'comments','notify_on'=>'notify_on'))
    					   ->where($where)
    					   ->order("$by $sort") 
    					   ->limitPage($pageNo,$perPage);
		
		
		return $expData;         		
	}
	public function getexperiencedetailsRecord($id=0)
	{  
		$empdependencyDetailsArr="";$where = "";
		$db = Zend_Db_Table::getDefaultAdapter();		
		if($id != 0)
		{
			$where = "id =".$id;
			$empdependencyDetails = $this->select()
									->from(array('e'=>'main_empprobationdetails'))
									->where($where);
		
			
			$empdependencyDetailsArr = $this->fetchAll($empdependencyDetails)->toArray(); 
        }
		return $empdependencyDetailsArr;       		
	}
    
	public function SaveorUpdateEmployeeprobationData($data, $where)
	{
	    if($where != ''){
			$this->update($data, $where);
			return 'update';
		} else {
			$this->insert($data);
			$id=$this->getAdapter()->lastInsertId('main_empprobationdetails');
			return $id;
		}
		
	}
	public function getGrid($sort,$by,$pageNo,$perPage,$searchData,$call,$dashboardcall,$exParam1='',$exParam2='',$exParam3='',$exParam4='')
	{		
        $searchQuery = '';$tablecontent = '';
        $searchArray = array();$data = array();$id='';
        $dataTmp = array();
        
		if($searchData != '' && $searchData!='undefined')
		{
			$searchValues = json_decode($searchData);
			foreach($searchValues as $key => $val)
			{
				if($key == 'start_date' || $key == 'end_date')
				{
					$searchQuery .= " ".$key." like '%".  sapp_Global::change_date($val,'database')."%' AND ";
				}
				else
					$searchQuery .= " ".$key." like '%".$val."%' AND ";
				$searchArray[$key] = $val;
			}
			 $searchQuery = rtrim($searchQuery," AND");					
		}
		/** search from grid - END **/
		$objName = 'probationdetails';
		$tableFields = array('action'=>'Action','start_date'=>'Start Date','end_date'=>'End Date','duration'=>'Duration','status'=>'Status','comments'=>'Comments','notify_on'=>'Notify On');
					
		$tablecontent = $this->getexperiencedetailsData($sort, $by, $pageNo, $perPage,$searchQuery,$exParam1);     
		
		$dataTmp = array('userid'=>$exParam1,
						'sort' => $sort,
						'by' => $by,
						'pageNo' => $pageNo,
						'perPage' => $perPage,				
						'tablecontent' => $tablecontent,
						'objectname' => $objName,
						'extra' => array(),
						'tableheader' => $tableFields,
						'jsGridFnName' => 'getEmployeeAjaxgridData',
						'jsFillFnName' => '',
						'searchArray' => $searchArray,
						'dashboardcall'=>$dashboardcall,
						'add'=>'add',
						'menuName'=>'Probation',
						'formgrid' => 'true',
						'unitId'=>$exParam1,
						'call'=>$call,
						'context'=>$exParam2,
						'search_filters' => array(
						'start_date' =>array('type'=>'datepicker'),
						'end_date' =>array('type'=>'datepicker')											
									)									
						);	
		return $dataTmp;
	}
	
	public function completeProbation($user_id)
	{
		$db = Zend_Db_Table::getDefaultAdapter ();
		$db->beginTransaction ();
		$query = "UPDATE main_empprobationdetails SET status = 'Complete' WHERE user_id=" . $user_id . "";
		$db->query ( $query );
		$db->commit ();
		return 'success';
	}
	
	public function updateProbationStatus($user_id,$id)
	{
		$db = Zend_Db_Table::getDefaultAdapter ();
		$db->beginTransaction ();
		$query = "UPDATE main_empprobationdetails SET status = 'Complete' WHERE user_id=" . $user_id . " AND id = '".$id."'";
		$db->query ( $query );
		$db->commit ();
		return 'success';
	}
	
	public function getProbationRule($userId,$currdate)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$probationRule = $db->query("select * from  main_empprobationdetails e where e.user_id = ".$userId." AND e.end_date = '".$currdate."';");
		$probationRuleArr = $probationRule->fetchAll();
		return $probationRuleArr;
	}
	
	public function verifyOtherProbationExists($userId,$currentdate)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$query = "select * from  main_empprobationdetails e where e.user_id = ".$userId." AND e.end_date > '".$currentdate."';";
		$probationRule = $db->query($query);
		$probationRuleArr = $probationRule->fetchAll();
		return $probationRuleArr;
	}
	
	public function ValidateNotificationDate($notifyDate, $startDate, $endDate){
	    $start = new DateTime($startDate);
	    $end = new DateTime($endDate);
	    $notificationDate =  new DateTime($notifyDate);
	    if($notificationDate > $end){
	        $messageArray['notify_on'] = "Notification should be less probation end date.";
	    }
	    if($notificationDate < $start){
	        $messageArray['notify_on'] = "Notification should be more than probation end date.";
	    }
	    if($notificationDate < new DateTime()){
	        $messageArray['notify_on'] = "Notification should be a future date.";
	    }
	    
	    return $messageArray;
	}
	
	public function getInProgressProbation(){
	    $db = Zend_Db_Table::getDefaultAdapter();
	    $query = "select * from  main_empprobationdetails e where e.status = 'In Progress' and e.isactive = 1;";
	    $probationRule = $db->query($query);
	    return $probationRule->fetchAll();
	}
	

}
?>