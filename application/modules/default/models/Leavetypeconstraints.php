<?php
class Default_Model_LeaveTypeConstraints extends Zend_Db_Table_Abstract{
    protected $_name = 'main_leavetype_constraints';
    protected $_primary = 'id';
    
    public function getActiveConstraintsbyType($leavetypeid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = "SELECT * FROM main_leavetype_constraints m, leave_constraints l 
                    where m.leavetype_id = $leavetypeid  and l.id = m.constraint_id and l.isactive = 1;";
        $data = $db->query($query)->fetchAll();
        return $data;
    }
    
    
    public function getContraintRequestParameters($constraintid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = "select p.name,r.parameter_id,r.value,p.default_value
        from main_leaveconstraint_request_parameters r, leave_constraint_parameters p
        where p.parameter_id = r.parameter_id and p.constraint_id = $constraintid;";
        
        $parameters = array();
        $data = $db->query($query)->fetchAll();
        foreach($data as $datum){
            $parameters[$datum['name']] =  isset($datum['value']) ? $datum['value'] : $datum['default_value'];
        }
        return $parameters;
    }
    
    public function getActiveConstraints(){
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$query = "SELECT * FROM leave_constraints where isactive=1";
    	$data = $db->query($query)->fetchAll();
    	return $data;
    }
    
    public function getLeaveConstraintParameters($constraintid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = "SELECT * FROM leave_constraint_parameters where constraint_id=$constraintid";
        $data = $db->query($query)->fetchAll();
        return $data;
    }
    
    public function getConstraintByClassName($classname){
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = "SELECT * FROM leave_constraints l where l.classname='". $classname. "'";
        $data = $db->query($query)->fetch();
        return $data;
    }
    
    public function saveOrupdateleaveConstraints($data){
        $result = $this->isConstraintPresent($data['leavetype_id'], $data['constraint_id']);
        if($result != null){
            $this->update($data, array('id=?'=>$result->toArray()['id']));
            $data['id'] = $result->toArray()['id'];
            return $data['id'];
        } else {
            $this->insert($data);
            $id=$this->getAdapter()->lastInsertId($this->_name);
            return $id;
        }
        
    }
    
    public function isConstraintPresent($leavetypeid,$constraintid){
        $result = $this->select()
        ->setIntegrityCheck(false)
        ->from(array('e'=>'main_leavetype_constraints'),array('e.*'))
        ->where("e.leavetype_id = $leavetypeid AND e.constraint_id = $constraintid");
        return $this->fetchRow($result);
    }
    
    public function deleteleaveConstraints($leavetypeid, $constraintid){
    	 $db = Zend_Db_Table::getDefaultAdapter();
        $query = "delete FROM main_leavetype_constraints where leavetype_id='". $leavetypeid. "' and constraint_id='".$constraintid."'";
        $data = $db->query($query)->execute();
        return $data;
    
    }
}