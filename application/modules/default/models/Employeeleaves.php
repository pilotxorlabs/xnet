<?php

/*********************************************************************************

 *  Copyright (C) 2014 Sapplica
 *

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/
class Default_Model_Employeeleaves extends Zend_Db_Table_Abstract
{

    protected $_name = 'main_employeeleaves';

    protected $_primary = 'id';

    public function getEmpLeavesData($sort, $by, $pageNo, $perPage, $searchQuery, $id)
    {
        $where = " e.user_id = " . $id . " AND e.isactive = 1 ";
        if ($searchQuery)
            $where .= " AND " . $searchQuery;
        $db = Zend_Db_Table::getDefaultAdapter();

        $empskillsData = $this->select()
            ->setIntegrityCheck(false)
            ->from(array(
            'e' => 'main_employeeleaves'
        ), array(
            'id' => 'e.id',
            'remainingleaves' => 'e.emp_leave_limit',
            'used_leaves' => 'e.used_leaves',
            'emp_leave_limit' => new Zend_Db_Expr('e.emp_leave_limit + e.used_leaves'),
            'e.alloted_year'
        ))
            ->join(array(
            'l' => 'main_employeeleavetypes'
        ), 'e.leavetypeid = l.id', array(
            'leavetype'
        ))
            ->where($where)
            ->order("$by $sort")
            ->limitPage($pageNo, $perPage);

        return $empskillsData;
    }

    public function getsingleEmployeeleaveData($id)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array(
            'el' => 'main_employeeleaves'
        ), array(
            'el.*'
        ))
            ->join(array(
            'l' => 'main_employeeleavetypes'
        ), 'el.leavetypeid = l.id', array(
            'leavetype'
        ))
            ->where('el.user_id=' . $id . ' AND el.isactive = 1 AND el.alloted_year = year(now())');

        return $this->fetchAll($select)->toArray();
    }

    public function getsingleEmployeeleaveDataByType($id, $leavetypeid)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array(
            'el' => 'main_employeeleaves'
        ), array(
            'el.*'
        ))
            ->where('el.user_id=' . $id . ' AND el.leavetypeid =' . $leavetypeid . ' AND el.isactive = 1')
            ->order('el.id DESC')
            ->limit(1);;

        return $this->fetchAll($select)->toArray();
    }

    public function getPreviousYearEmployeeleaveData($id)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array(
            'el' => 'main_employeeleaves'
        ), array(
            'el.*',
            'remainingleaves' => new Zend_Db_Expr('el.emp_leave_limit')
        ))
            ->where('el.user_id=' . $id . ' AND el.isactive = 1 AND el.alloted_year = year(now())-1');

        return $this->fetchAll($select)->toArray();
    }

    public function getsingleEmpleavesrow($id)
    {
        $row = $this->fetchRow("id = '" . $id . "'");
        if (! $row) {
            throw new Exception("Could not find row $id");
        }
        return $row->toArray();
    }

    public function SaveorUpdateEmpLeaves($data, $where)
    {
        if ($where != '') {
            $this->update($data, $where);
            return 'update';
        } else {
            $this->insert($data);
            $id = $this->getAdapter()->lastInsertId('main_employeeleaves');
            return $id;
        }
    }

    public function SaveorUpdateEmployeeLeaves($user_id, $emp_leave_limit, $isleavetrasnfer, $loginUserId)
    {
        $date = gmdate("Y-m-d H:i:s");

        $db = Zend_Db_Table::getDefaultAdapter();
        $rows = $db->query("INSERT INTO `main_employeeleaves` (user_id,emp_leave_limit,used_leaves,alloted_year,createdby,modifiedby,createddate,modifieddate,isactive,isleavetrasnferset) VALUES (" . $user_id . "," . $emp_leave_limit . ",'0',year(now())," . $loginUserId . "," . $loginUserId . ",'" . $date . "','" . $date . "',1," . $isleavetrasnfer . ") ON DUPLICATE KEY UPDATE emp_leave_limit='" . $emp_leave_limit . "',modifiedby=" . $loginUserId . ",modifieddate='" . $date . "',isactive = 1,isleavetrasnferset=" . $isleavetrasnfer . " ");
        $id = $this->getAdapter()->lastInsertId('main_employeeleaves');
        return $id;
    }

    public function SaveorUpdateEmployeeLeaveType($user_id, $emp_leave_limit, $usedleaves, $loginUserId, $leavetypeid)
    {
        $date = gmdate("Y-m-d H:i:s");

        $db = Zend_Db_Table::getDefaultAdapter();
        $rows = $db->query("INSERT INTO `main_employeeleaves` (user_id,emp_leave_limit,used_leaves,alloted_year,leavetypeid,createdby,modifiedby,createddate,modifieddate,isactive,isleavetrasnferset) VALUES (" . $user_id . "," . $emp_leave_limit . "," . $usedleaves . ",year(now())," . $leavetypeid . "," . $loginUserId . "," . $loginUserId . ",'" . $date . "','" . $date . "',1,0) ON DUPLICATE KEY UPDATE emp_leave_limit='" . $emp_leave_limit . "',used_leaves=" . $usedleaves . ",leavetypeid=" . $leavetypeid . ",modifiedby=" . $loginUserId . ",modifieddate='" . $date . "',isactive = 1,isleavetrasnferset = 0 ");
        $id = $this->getAdapter()->lastInsertId('main_employeeleaves');
        return $id;
    }

    public function saveallotedleaves_normalquery($postedArr, $totLeaves, $userid, $loginUserId)
    {
        $date = gmdate("Y-m-d H:i:s");
        $db = Zend_Db_Table::getDefaultAdapter();
        $rows = $db->query("INSERT INTO main_allottedleaveslog (userid,assignedleaves,totalleaves,year,
				createdby,modifiedby,createddate,modifieddate) VALUES (" . $userid . "," . $postedArr['leave_limit'] . "," . $totLeaves . "," . $postedArr['alloted_year'] . ",
				" . $loginUserId . "," . $loginUserId . ",'" . $date . "','" . $date . "');");

        $id = $this->getAdapter()->lastInsertId('main_allottedleaveslog');
        return $id;
    }

    public function saveallotedleaves($postedArr, $totLeaves, $userid, $loginUserId)
    {
        $allotedLeavesDetailsmodel = new Default_Model_Allottedleaveslog();
        $date = gmdate("Y-m-d H:i:s");
        $data = array(
            'userid' => $userid,
            'assignedleaves' => ! empty($postedArr['leave_limit']) ? $postedArr['leave_limit'] : $totLeaves,
            'totalleaves' => $totLeaves,
            'year' => ! empty($postedArr['alloted_year']) ? $postedArr['alloted_year'] : date("Y"),
            'createdby' => $loginUserId,
            'modifiedby' => $loginUserId,
            'createddate' => $date,
            'modifieddate' => $date
        );
        $id = $allotedLeavesDetailsmodel->SaveorUpdateAllotedLeavesDetails($data, '');
        return $id;
    }

    public function getGrid($sort, $by, $perPage, $pageNo, $searchData, $call, $dashboardcall, $exParam1 = '', $exParam2 = '', $exParam3 = '', $exParam4 = '')
    {
        $searchQuery = '';
        $tablecontent = '';
        $level_opt = array();
        $searchArray = array();
        $data = array();
        $id = '';
        $dataTmp = array();
        if ($searchData != '' && $searchData != 'undefined') {
            $searchValues = json_decode($searchData);
            foreach ($searchValues as $key => $val) {
                if ($key == 'remainingleaves')
                    $searchQuery .= "  e.emp_leave_limit - e.used_leaves   like '%" . $val . "%' AND ";
                else
                    $searchQuery .= " " . $key . " like '%" . $val . "%' AND ";
                $searchArray[$key] = $val;
            }
            $searchQuery = rtrim($searchQuery, " AND");
        }
        /**
         * search from grid - END *
         */
        $objName = 'empleaves';
        $tableFields = array(
            'action' => 'Action',
            'leavetype' => 'Leave Type',
            'emp_leave_limit' => 'Allotted Leaves',
            'alloted_year' => 'Allotted Year',
            'used_leaves' => 'Used Leaves',
            'remainingleaves' => 'Leave Balance',
        );

        $tablecontent = $this->getEmpLeavesData($sort, $by, $pageNo, $perPage, $searchQuery, $exParam1);

        $dataTmp = array(
            'userid' => $exParam1,
            'sort' => $sort,
            'by' => $by,
            'pageNo' => $pageNo,
            'perPage' => $perPage,
            'tablecontent' => $tablecontent,
            'objectname' => $objName,
            'extra' => array(),
            'tableheader' => $tableFields,
            'jsGridFnName' => 'getEmployeeAjaxgridData',
            'jsFillFnName' => '',
            'searchArray' => $searchArray,
            'add' => 'add',
            'menuName' => 'Leave',
            'formgrid' => 'true',
            'unitId' => $exParam1,
            'dashboardcall' => $dashboardcall,
            'call' => $call,
            'context' => $exParam2
        );
        return $dataTmp;
    }

    public function countLeaves($leaveTypeData, $daysSinceJoined, $date_of_joining, $userId = '', $leavetypeid, &$usedLeaves = '')
    {
        $allotmentmode = $leaveTypeData['allotmentmode'];
        $numberofdays = $leaveTypeData['numberofdays'];
        $maxLimit= $leaveTypeData['max_limit'];
        $leavesPreAllocated= $leaveTypeData['leavepreallocated'];
        $minimumDuration = $leaveTypeData['minimumduration'];
        $firstDay = date('Y-01-01');
        $isConfirmed = false;
        $daysSinceFirstDay = $this->getDaysBetween($firstDay, date(DATEFORMAT_PHP));
        $employeesModel = new Default_Model_Employee();
        if($userId != ''){
            $empData = $employeesModel->getsingleEmployeeData($userId);
            $isConfirmed = $empData[0]['emp_status_id'] == 1 ? true:false;
        }
        $leaveRequestModel = new Default_Model_Leaverequest();
        if ($leavesPreAllocated == '1') {
            switch ($allotmentmode) {
                case '5':
                case 'Yearly':
                    if ($isConfirmed || $leaveTypeData['isallocatedinprobation'] == 'yes' || $leaveTypeData['isallocatedinprobation'] == 1) {
                        if (date('Y') == date('Y', strtotime($date_of_joining))) {
                            $months = 12 - $this->getMonthsBetween($firstDay, $date_of_joining);
                            $joiningdate = date('d', strtotime($date_of_joining));

                            if ($joiningdate <= '15')
                                $leaves = ($numberofdays * $months) / 12;
                            else
                                $leaves = ($numberofdays * ($months - 1)) / 12;
                            $leaves = $this->roundOfLeaves($leaves);
                            return ($leaves);
                        } else {
                            return $numberofdays;
                        }
                    }
                    return 0;
                case '4':
                case 'Half-yearly':
                    $halfs = ceil($daysSinceFirstDay / 180);
                    return ($numberofdays * $halfs) / 2;
                case '3':
                case 'Quarterly':
                    $quarters = ceil($daysSinceFirstDay / 90);
                    return ($numberofdays * $quarters) / 4;
                case '2':
                case 'Monthly':
                    $usedLeaves = $leaveRequestModel->getUsedLeavesByTypeWithin($userId, $leavetypeid, new DateTime('first day of this month'), new DateTime('last day of this month'));
                    $months = ceil($daysSinceFirstDay / 365);
                    return (($numberofdays * $months) / 12) - $usedLeaves;
                case '1':
                case 'Pro-Rata':
                    $total = ($numberofdays * $daysSinceJoined) / 365;
                    return round(min($total, $maxLimit), 2);
            }
        } else {
            switch ($allotmentmode) {
                case '5':
                case 'Yearly':
                    return $daysSinceJoined > 365 ? $numberofdays : 0;
                case '4':
                case 'Half-yearly':
                    $halfs = floor($daysSinceFirstDay / 180);
                    return $daysSinceFirstDay > 180 ? (($numberofdays * $halfs) / 2) : 0;
                case '3':
                case 'Quarterly':
                    $quarters = floor($daysSinceFirstDay / 90);
                    return $daysSinceFirstDay > 90 ? (($numberofdays * $quarters) / 4) : 0;
                case '2':
                case 'Monthly':
                    $quarters = floor($daysSinceFirstDay / 30);
                    return $daysSinceFirstDay > 90 ? (($numberofdays * $quarters) / 12) : 0;
                case '1':
                case 'Pro-Rata':
				if ($isConfirmed || $leaveTypeData['isallocatedinprobation'] == 'yes' || $leaveTypeData['isallocatedinprobation'] == 1) {
                    $earned_leave_model = new Default_Model_EmployeeEarnedLeaves();
                    return $earned_leave_model->getEarnedLeaves($date_of_joining, $userId, $numberofdays, $maxLimit);
				}
				return 0;
            }
        }
    }

    public function roundOfLeaves($leaves)
    {
        $fraction = $leaves - floor($leaves);
        if ($fraction <= 1 / 4)
            return floor($leaves);
        if (3 / 4 >= $fraction && $fraction > 1 / 4)
            return (floor($leaves) + 1 / 2);
        if ($fraction >= 3 / 4)
            return ($leaves);
    }

    public function getDaysBetween($startDate, $endDate)
    {
        $start_ts = strtotime($startDate);
        $end_ts = strtotime($endDate);
        $diff = $end_ts - $start_ts;
        return round($diff / 86400) + 1;
    }

    public function getMonthsBetween($startDate, $endDate)
    {
        $ts1 = strtotime($startDate);
        $ts2 = strtotime($endDate);
        $diff = ((date('Y', $ts2) - date('Y', $ts1)) * 12) + ((date('m', $ts2) - date('m', $ts1)));
        return $diff;
    }

    public function getFirstDayOfQuarter($currentQuarter)
    {
        switch ($currentQuarter) {
            case 1:
                return new DateTime('first day of January');
            case 2:
                return new DateTime('first day of April');
            case 3:
                return new DateTime('first day of July');
            case 4:
                return new DateTime('first day of October');
        }
    }

    public function getLastDayOfQuarter($currentQuarter)
    {
        switch ($currentQuarter) {
            case 1:
                return new DateTime('last day of March');
            case 2:
                return new DateTime('last day of June');
            case 3:
                return new DateTime('last day of September');
            case 4:
                return new DateTime('last day of December');
        }
    }

    public function getFirstDayOfHalfYear($currentHalf)
    {
        switch ($currentHalf) {
            case 1:
                return new DateTime('first day of January');
            case 2:
                return new DateTime('first day of July');
        }
    }

    public function getLastDayOfHalfYear($currentHalf)
    {
        switch ($currentHalf) {
            case 1:
                return new DateTime('last day of July');
            case 2:
                return new DateTime('last day of December');
        }
    }

    public function getAllocationModeByType($leavetype)
    {
        require_once 'application\modules\default\models\employeeleaves\AllotmentMode.php';
        require_once 'application\modules\default\models\employeeleaves\Quarterly.php';
        require_once 'application\modules\default\models\employeeleaves\Yearly.php';
        require_once 'application\modules\default\models\employeeleaves\HalfYearly.php';
        require_once 'application\modules\default\models\employeeleaves\Monthly.php';

        switch ($leavetype['allotmentmode']) {
            case '5':
            case 'Yearly':
                return new Yearly();
            case '4':
            case 'Half-yearly':
                return new HalfYearly();
            case '3':
            case 'Quarterly':
                return new Quarterly();
            case '2':
            case 'Monthly':
                return new Monthly();
            case '1':
            case 'Pro-Rata':
                return new Propotional();
        }
    }

    public function getEmployeeLeaveLimit($userid, $leavetypeid)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array(
            'el' => 'main_employeeleaves'
        ), array(
            'el.emp_leave_limit'
        ))
            ->where('el.user_id=' . $userid . ' AND el.isactive = 1 AND el.alloted_year = year(now()) AND el.leavetypeid = '.$leavetypeid.'');

        return $this->fetchAll($select)->toArray();
    }
}
?>