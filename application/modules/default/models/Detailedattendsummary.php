<?php

class Default_Model_Detailedattendsummary extends Zend_Db_Table_Abstract
{
    protected $_name = 'main_detailed_attendence';
    protected $_primary = 'id';
    
    public function getDetailedAttend($start_Date, $end_Date)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$query = "select * from main_emp_attendence where punch_date >= STR_TO_DATE('". $start_Date."','%m/%d/%Y') and punch_date <= STR_TO_DATE('". $end_Date."','%m/%d/%Y') ";
    	$result =  $db->query($query)->fetchAll();

		// Date difference
		$diff = abs(strtotime($end_Date)- strtotime($start_Date));
		$years = floor($diff / (365*60*60*24));        	
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));        	
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		
    	$finalArray = array();
		$userIdArray = array();
        foreach($result as $row)
        {
        	if(!in_array($row['user_id'], $userIdArray))
        	{
        		array_push($userIdArray, $row['user_id']);
        	}
        }
		
		foreach($userIdArray as $currentUserId)
		{
		    $empNameQuery = "select userfullname from main_users where id='".$currentUserId."'";
        	$checkOutQuery = "select check_out from main_emp_attendence where punch_date >= STR_TO_DATE('". $start_Date."','%m/%d/%Y') and punch_date <= STR_TO_DATE('". $end_Date."','%m/%d/%Y')and user_id='".$currentUserId."' and check_out is NULL ";
        	$halfDayQuery = "select overall_status from main_emp_attendence where punch_date >= STR_TO_DATE('". $start_Date."','%m/%d/%Y') and punch_date <= STR_TO_DATE('". $end_Date."','%m/%d/%Y')and user_id='".$currentUserId."' and overall_status = 'HALF DAY' ";
        	$fullabsentQuery = "select status from main_emp_attendence where punch_date >= STR_TO_DATE('". $start_Date."','%m/%d/%Y') and punch_date <= STR_TO_DATE('". $end_Date."','%m/%d/%Y')and user_id='".$currentUserId."' and status = 'A A' ";
        	$absentQuery = "select status from main_emp_attendence where punch_date >= STR_TO_DATE('". $start_Date."','%m/%d/%Y') and punch_date <= STR_TO_DATE('". $end_Date."','%m/%d/%Y') and user_id='".$currentUserId."' and (status like 'A %' or status like '% A') ";
        	$fullLWPQuery = "select status from main_emp_attendence where punch_date >= STR_TO_DATE('". $start_Date."','%m/%d/%Y') and punch_date <= STR_TO_DATE('". $end_Date."','%m/%d/%Y')and user_id='".$currentUserId."' and status = 'LWP LWP' ";
        	$lwpQuery = "select status from main_emp_attendence where punch_date >= STR_TO_DATE('". $start_Date."','%m/%d/%Y') and punch_date <= STR_TO_DATE('". $end_Date."','%m/%d/%Y')and user_id='".$currentUserId."' and (status like 'LWP %' or status like '% LWP') ";
        	$compOffQuery = "select status from main_emp_attendence where punch_date >= STR_TO_DATE('". $start_Date."','%m/%d/%Y') and punch_date <= STR_TO_DATE('". $end_Date."','%m/%d/%Y')and user_id='".$currentUserId."' and status = 'WO' and overall_status = 'FULL DAY'";
        	// Employee Name
        	$empName = $db->query($empNameQuery)->fetchAll();
        	if(count($empName) > 0)
        	{
        		$empAttendSummary["Name"] = $empName[0]["userfullname"];        		
        	}
        	else
        	{
        		$empAttendSummary["Name"] = "Not an user";        	
        	}
        	// Number of miss punch
        	$checkOutDetails = $db->query($checkOutQuery)->fetchAll();
        	$missPunchCount = 0;
        	$missPunch = 0;
        	if(empty($checkOutDetails))
        	{
        		$empAttendSummary["Miss Punch"] = 0;
        	}
        	else
        	{
        		$missPunchCount = count($checkOutDetails);
        		
        		if($missPunchCount > 2)
        		{
        			if($missPunchCount <= 4)
        			{
        				$missPunch = 0.5;
        			}
        			if($missPunchCount >=5)
        			{
        				$missPunch = $missPunch + 0.5;
        			}
        			$empAttendSummary["Miss Punch"] = $missPunchCount;
        		}
        		else 
        		{
        			$empAttendSummary["Miss Punch"] = $missPunchCount;
        		}
        	}
        	// Number of half day
        	$allhalfDay = $db->query($halfDayQuery)->fetchAll();
        	$halfday = 0;
        	if(empty($allhalfDay))
        	{
        		$empAttendSummary["Half Day"] = 0;
        	}
        	else
        	{
        		$halfday = count($allhalfDay);
        		$empAttendSummary["Half Day"] = $halfday;
        	}
        	// Full day absent
        	$fullAbsentDetails = $db->query($fullabsentQuery)->fetchAll();
        	$fullAbsentCount = 0;
        	if(empty($fullAbsentDetails))
        	{
        		$fullAbsentCount = 0;
        	}
        	else 
        	{
        		$fullAbsentCount = count($fullAbsentDetails);
        	}
        	// Total absent (full day and half day)
        	$absentDetails = $db->query($absentQuery)->fetchAll();
        	$halfAbsentCount = 0;
        	$absent = 0;
        	if(empty($absentDetails))
        	{
        		$empAttendSummary["Absent"] = 0;
        	}
        	else
        	{
        		$count = 0;
         	    $count = count($absentDetails);
        		$halfAbsentCount = $count - $fullAbsentCount;
        		$halfAbsent = $halfAbsentCount/2;
        		$absent = $halfAbsent + $fullAbsentCount;
        		$absent = $absent - $missPunchCount;
        		$empAttendSummary["Absent"] = $absent;
        	}
        	// Full day LWP    
        	$fullLWPDetails = $db->query($fullLWPQuery)->fetchAll();
        	$fullLWPCount = 0;
        	if(empty($fullLWPDetails))
        	{
        		$fullLWPCount = 0;
        	}
        	else
        	{
        		$fullLWPCount = count($fullLWPDetails);
        	}
        	// Total LWP(full day and half day)
        	$lwpDetails = $db->query($lwpQuery)->fetchAll();
        	$halfLWPCount = 0;
        	$lwp = 0;
        	if(empty($lwpDetails))
        	{
        		$empAttendSummary["LWP"] = 0;
        	}
        	else
        	{
        		$count = 0;
        		$count = count($lwpDetails);
        		$halfLWPCount = $count - $fullLWPCount;
        		$halfLWP = $halfLWPCount/2;
        		$lwp = $halfLWP + $fullLWPCount;
        		$empAttendSummary["LWP"] = $lwp;
        	}
        	// Comp off
        	$compOffDetails = $db->query($compOffQuery)->fetchAll();
        	$compOffCount = 0;
        	if(empty($compOffDetails))
        	{
        		$empAttendSummary["Comp Off"] = 0;
        	}
        	else
        	{
        		$compOffCount = count($compOffDetails);
        		$empAttendSummary["Comp Off"] = $compOffCount;    
        	}
        	// Total working days excluding absent, lwp and including comp off and weekends
        	$workingDays = $days - ($missPunch + $absent + $lwp) + $compOffCount;
        	$empAttendSummary["Working Days"] = $workingDays;
        	
        	array_push($finalArray, $empAttendSummary);
	
		}
		
    	ob_end_clean();
    	$fp = fopen('php://output','w');
 	
		fputcsv($fp, array("Name", "Miss Punch", "Half Day", "Absent", "LWP", "Comp Off", "Working Days", "Deduction", "Comments"));
    	foreach($finalArray as $line)
    	{
    		fputcsv($fp, $line);
    		fputs($fp, "\n");
    	}
    			 
          header('Content-Type: text/csv; charset=utf-8');
          header('Content-Disposition: attachment; filename=detailedAttendance.csv');
          
         exit();
         
    }
       
}