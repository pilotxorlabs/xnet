<?php
class Default_Model_MandatoryHours extends Zend_Db_Table_Abstract
{
    protected $_name = 'main_mandatory_hours';
    protected $_primary = 'id';

	public function SaveorUpdateData($data, $where)
	{
		if($where != ''){
			$this->update($data, $where);
			return 'update';
		} else {
			$this->insert($data);
			$id=$this->getAdapter()->lastInsertId('main_mandatory_hours');
			return $id;
		}
	}

	public function getConfigurationData($configuration_id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'm' => 'main_mandatory_hours'
		), array (
				'm.*',
		) )->where ( "m.configuration_id = $configuration_id"  );

		return $this->fetchAll ( $result )->toArray ();
	}
	
	public function getMandatoryRule($duration,$businessUnitId) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$duration = "'".$duration."'";
		$query = "SELECT m.minimum_hours , m.minimum_hours_action , m.maximum_hours , m.maximum_hours_action FROM main_mandatory_hours as m INNER JOIN main_attendance_configuration as a on m.configuration_id = a.id where a.business_unit_id = " .$businessUnitId." and m.duration = ".$duration;
		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	
	public function deleteAttendanceConfigurationDetails($id) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "DELETE FROM `main_mandatory_hours` WHERE `configuration_id` = ".$id." ";
		$db->query ( $query );
	}
	
	public function checkConfigurationExists($data){
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "SELECT mm.id as id FROM main_attendance_configuration as ac inner join  main_mandatory_hours as mm on ac.id = mm.configuration_id where ac.configuration_type_id = ".$data['configuration_type_id']." and ac.business_unit_id= ".$data['business_unit_id']." and  mm.duration = ".$data['duration'];
		$result = $db->query ( $query )->fetch();
		return $result['id'];
	}
	
	public function runCustomValidation($data, &$messageArray){
		
	}
	
	public function halfDayRuleCalculation($halfdayrule,$workinghrs,&$faultyDays,$loggedday,&$absentFlag,&$halfDayFlag){
		$halfDayRuleResult = array();
		if ($workinghrs < $halfdayrule[0]['minimum_hours'])
		{
			if ($halfdayrule[0]['minimum_hours_action'] == 1)
			{
			    $absentFlag = 1;
			    array_push($faultyDays,$loggedday['punch_date']);
			}
			else if ($halfdayrule[0]['minimum_hours_action'] == 2)
			{
			    $halfDayFlag = 1;
			    array_push($faultyDays,$loggedday['punch_date']);
			}
		}
		if ($workinghrs > $halfdayrule[0]['maximum_hours'])
		{
			if ($halfdayrule[0]['maximum_hours_action'] == 1)
			{
			    $absentFlag = 1;
			    array_push($faultyDays,$loggedday['punch_date']);
			}
			else if ($halfdayrule[0]['maximum_hours_action'] == 2)
			{
			    $halfDayFlag = 1;
			    array_push($faultyDays,$loggedday['punch_date']);
			}
		}
	}
	
	public function dailyRuleCalculation($dailyrule,$workinghrs,&$faultyDays,$loggedday,&$absentFlag,&$halfDayFlag){
		$dailyRuleResult = array();
		if ($workinghrs < $dailyrule[0]['minimum_hours'])
		{
			if ($dailyrule[0]['minimum_hours_action'] == 1)
			{
			    $absentFlag = 1;
			    array_push($faultyDays,$loggedday['punch_date']);
			}
			else if ($dailyrule[0]['minimum_hours_action'] == 2)
			{
			    $halfDayFlag = 1;
			    array_push($faultyDays,$loggedday['punch_date']);
			}
		}
		if ($workinghrs > $dailyrule[0]['maximum_hours'])
		{
			if ($dailyrule[0]['maximum_hours_action'] == 1)
			{
			    $absentFlag = 1;
			    array_push($faultyDays,$loggedday['punch_date']);
			}
			else if ($dailyrule[0]['maximum_hours_action'] == 2)
			{
			    $halfDayFlag = 1;
			    array_push($faultyDays,$loggedday['punch_date']);
			}
		}
	}
}