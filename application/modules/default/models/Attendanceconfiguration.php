<?php
/*********************************************************************************

 *  Copyright (C) 2014 Sapplica
 *

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/

class Default_Model_Attendanceconfiguration extends Zend_Db_Table_Abstract
{
    protected $_name = 'main_attendance_configuration';
    protected $_primary = 'id';

	public function getConfigurationData($sort, $by, $pageNo, $perPage,$searchQuery)
	{
	    /* Removing isactive checking from configuration table */


		//$where = "l.isactive = 1 AND d.isactive=1 AND b.isactive=1";


		if($searchQuery)
			$where .= " AND ".$searchQuery;
		$db = Zend_Db_Table::getDefaultAdapter();

		$configurationData = $this->select()
    					   ->setIntegrityCheck(false)
    					   ->from(array('a'=>'main_attendance_configuration'),
    					   						          array( 'c.unitname','a.id',
    					   						          		'b.configuration_name'
    					   										 ))

    					   	->joinLeft(array('b'=>'main_configuration_types'), 'a.configuration_type_id=b.configuration_type_id',array())
    					   	->joinLeft(array('c'=>'main_businessunits'), 'c.id=a.business_unit_id',array())

    					   ->limitPage($pageNo, $perPage);

		return $configurationData;
	}

	
	public function getMainOfficeHours($id)
	{
		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('l'=>'main_office_hours'))
		->where('l.configuration_id = '.$id.'');
	
		return $this->fetchAll($select)->toArray();
	
	}	

	public function getBusinessUnitData($id)
	{
		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('a'=>'main_attendance_configuration'),
				array( 'a.business_unit_id as businessunit','c.unitname','a.id',
						'b.configuration_name','b.configuration_type_id'	))
	
						->joinLeft(array('b'=>'main_configuration_types'), 'a.configuration_type_id=b.configuration_type_id',array())
						->joinLeft(array('c'=>'main_businessunits'), 'c.id=a.business_unit_id',array())
						->where('a.id = '.$id.'');
	
						return $this->fetchAll($select)->toArray();
	
	}
	
	
	
	public function getGrid($sort,$by,$perPage,$pageNo,$searchData,$call,$dashboardcall,$a='',$b='',$c='',$d='')
	{	
		$monthslistmodel = new Default_Model_Monthslist();
		$weekdaysmodel = new Default_Model_Weekdays(); 	
        $searchQuery = '';
        $searchArray = array();
        $data = array();

		if($searchData != '' && $searchData!='undefined')
			{
				$searchValues = json_decode($searchData);

				foreach($searchValues as $key => $val)
				{
				    if($key == 'configuration_name')
					 $searchQuery .= " l.".$key." like '%".$val."%' AND ";
					
					else 
					 $searchQuery .= " ".$key." like '%".$val."%' AND ";
					$searchArray[$key] = $val;
				}
				$searchQuery = rtrim($searchQuery," AND");					
			}
		
		$objName = 'attendanceconfiguration';

		$tableFields = array('action'=>'Action',
				'unitname' => 'Business Unit',
				'configuration_name' => 'Configuration Type');
		$tablecontent = $this->getConfigurationData($sort, $by, $pageNo, $perPage,$searchQuery);

        $monthslistdata = $monthslistmodel->getMonthlistData();
                $month_opt = array();

                if(sizeof($monthslistdata) > 0)
                {
                    foreach ($monthslistdata as $monthslistres)
                    {
                        $month_opt[$monthslistres['month_id']] = $monthslistres['month_name'];
                    }
                }
                $bool_arr = array('' => 'All',1 => 'Yes',2 => 'No');
                $week_arr = array();
                $weekdaysdata = $weekdaysmodel->getWeeklistData();

                if(sizeof($weekdaysdata) > 0)
                {
                    foreach ($weekdaysdata as $weekdaysres)
                    {
                        $week_arr[$weekdaysres['day_id']] = $weekdaysres['day_name'];
                    }
                }

		$dataTmp = array(
			'sort' => $sort,
			'by' => $by,
			'pageNo' => $pageNo,
			'perPage' => $perPage,
			'tablecontent' => $tablecontent,
			'objectname' => $objName,
			'extra' => array(),
			'tableheader' => $tableFields,
			'jsGridFnName' => 'getAjaxgridData',
			'jsFillFnName' => '',
			'searchArray' => $searchArray,
			'add' =>'add',
			'call'=>$call,
			'dashboardcall'=>$dashboardcall,
                        'search_filters' => array(
                            'cal_startmonth' => array(
                                'type' => 'select',
                                'filter_data' => array('' => 'All')+$month_opt,
                            ),
                            'weekend_startday' => array(
                                'type' => 'select',
                                'filter_data' => array('' => 'All')+$week_arr,
                            ),
                            'weekend_endday' => array(
                                'type' => 'select',
                                'filter_data' => array('' => 'All')+$week_arr,
                            ),
                            'is_leavetransfer' => array(
                                'type' => 'select',
                                'filter_data' => $bool_arr,
                            ),
                            'is_skipholidays' => array(
                                'type' => 'select',
                                'filter_data' => $bool_arr,
                            ),
                            'is_halfday' => array(
                                'type' => 'select',
                                'filter_data' => $bool_arr,
                            ),
                        ),
		    );
		return $dataTmp;
	}


	public function SaveorUpdateMainAttendanceConfiguration($data, $where)
	{
		if($where != ''){
			$this->update($data, $where);
			return 'update';
		} else {
			$this->insert($data);
			$id=$this->getAdapter()->lastInsertId('main_attendance_configuration');
			return $id;
		}

	}

	public function getAttendanceConfiguration()
	{
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'a' => 'main_attendance_configuration'
		), array (
				'a.*'
		) )->where ( "a.business_unit_id = 1" );

		return $this->fetchAll ( $result )->toArray ();
	}

	public function getAttendanceConfigurationType()
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$query ="select * from main_configuration_types";
		$res = $db->query($query)->fetchAll();
		return $res;
	}

	public function getAttendanceAction()
	{
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'a' => 'main_attendance_action'
		), array (
				'a.action_id'));

		return $this->fetchAll ( $result )->toArray ();
	}
	

	public function getmisspunchruleId()
	{
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'a' => 'main_attendance_configuration'
		), array (
				'a.id'
		) )->where ( "a.business_unit_id = ".'1'." "."and"." a.configuration_type_id = ".'3'."" );
	
		return $this->fetchAll ( $result )->toArray ();
	}
	
	public function getMandatoryHoursRuleId()
	{
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'a' => 'main_attendance_configuration'
		), array (
				'a.id'
		) )->where ( "a.business_unit_id = ".'1'." "."and"." a.configuration_type_id = ".'2'."" );
	
		return ($this->fetchAll ( $result )->toArray ());
	} 
	
	public function checkMissedPunchRuleExistsByBusinessUnit($businessunit) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'a' => 'main_attendance_configuration'
		), array (
				'a.id'
		) )->where ( "a.business_unit_id = ".$businessunit." "."and"." a.configuration_type_id = ".'3'."" );
	
		return $this->fetchAll ( $result )->toArray ();
	}
	
	public function checkMandatoryHoursRuleExists($businessunit,$duration) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$duration = "'".$duration."'";
		$query = "SELECT * FROM xnet.main_mandatory_hours as m INNER JOIN xnet.main_attendance_configuration as a on m.configuration_id = xnet.a.id where a.business_unit_id = ".$businessunit." and m.duration = ".$duration;
		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	
	public function getattendanceconfigurationtypeid($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'a' => 'main_attendance_configuration'
		), array (
				'a.configuration_type_id'
		) )->where ( "a.id = ".$id."" );
	
		return $this->fetchAll ( $result )->toArray ();
	}
	
	public function deleteAttendanceConfigurationDetails($id) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "DELETE FROM `main_attendance_configuration` WHERE `id` = ".$id." ";
		$db->query ( $query );
	}
}