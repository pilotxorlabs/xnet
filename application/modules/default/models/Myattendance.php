<?php
defined ( 'ATTENDENCEAUDITSTARTDAY' ) || define ( "ATTENDENCEAUDITSTARTDAY", "Friday" );
defined ( 'ATTENDENCEAUDITENDDAY' ) || define ( "ATTENDENCEAUDITENDDAY", "Thursday" );
defined ( 'HOURSPERDAY' ) || define ( "HOURSPERDAY", 9 );
class Default_Model_Myattendance extends Zend_Db_Table_Abstract {
	protected $_name = 'main_emp_attendence';
	protected $_primary = 'id';
	public function calculateIdealTime($id, $wfhType) {
		$startAudit = $this->getAuditStartDate ();
		$endAudit = $this->getAuditEndDate ();
		$idealTime = 5 * HOURSPERDAY;
		
		$employeesModel = new Default_Model_Employees ();
		$leavesModel = new Default_Model_Leaverequest ();
		$holidayModel = new Default_Model_Holidaydates ();
		
		$holidayGroup = $employeesModel->getHolidayGroupForEmployee ( $id );
		$leaves = $leavesModel->getApprovedLeavesBetween ($id, $startAudit, $endAudit );

        if (count($leaves) > 0 && $leaves [0] ['leavecount'] != null) {
            foreach ($leaves as $leaveData) {
                if ($leaveData ['leavetypeid'] != '4' && $leaveData ['leavecount'] != null) {
                    $idealTime -= ($leaveData ['leavecount'] * HOURSPERDAY);
                }
            }
        }
		if ($holidayGroup ['0'] ['holiday_group'] != null) {
			$holidays = $holidayModel->getHolidaysWithin ( $startAudit, $endAudit, $holidayGroup ['0'] ['holiday_group'] );
			$idealTime -= ($holidays [0] ['holidaydates'] * HOURSPERDAY);
		}
		
		return $idealTime;
	}
	public function getCurrentTimeZone() {
		$select = $this->select ()->setIntegrityCheck ( false )->from ( array (
				's' => 'main_sitepreference' 
		), array (
				't.timezone' 
		) )->joinLeft ( array (
				't' => 'main_timezone' 
		), 't.id=s.timezoneid AND t.isactive=1' )->where ( 's.isactive = 1' );
		
		return $this->fetchAll ( $select )->toArray ();
		;
	}
	public function calculateCurrentWeekLoggedHours($id, &$isUserWorking, &$currentDayHours, $fromDate, $toDate, $employeeExceptionModel) {
		$loggedTime = 0;
		$workingHours = 0;
        $empWorkActivityModel = new Default_Model_EmployeeWorkActivity ();
        $startDate = new DateTime($fromDate);
        $endDate = new DateTime ( $toDate );
        $attendanceexception = $employeeExceptionModel->getUsersApprovedExceptionsBetween ( $id, $fromDate, $toDate );
        if ($attendanceexception > 0) {
            $loggedTime += $this->hoursToSeconds ( HOURSPERDAY * $attendanceexception );
        }
        while ( $startDate <= $endDate ) {
            $loggedActivities = $empWorkActivityModel->getEmployeeActivities($startDate, $id, $workingHours, false);
            $punchDate = $startDate->format(DATEFORMAT_PHP);
            $dayCount = 0;
            if ($punchDate)
                $day = $this->getDayFromDate($punchDate);
            if ($day === "Saturday" || $day === "Sunday") {
                $startDate->modify ( '+1 day' );
                continue;
            }
            if ($employeeExceptionModel->checkExceptionExist($id, $punchDate, $dayCount)) {
                $startDate->modify ( '+1 day' );
                continue;
            }

            if ($punchDate == date(DATEFORMAT_PHP)) {
                $timeZone = $this->getCurrentTimeZone();
                if ($timeZone != null && $timeZone [0] != null && $timeZone [0] ['timezone'] != null) {
                    date_default_timezone_set($timeZone [0] ['timezone']);
                }
                $currentTime = new DateTime ();
                $checkOutTime = $this->GetCheckOutFromActivities($loggedActivities);

                if($currentTime->format('Y-m-d H:i') == $checkOutTime->format("Y-m-d H:i")) {
                    $isUserWorking = true;
                }

                $currentDayHours = $this->hoursToSeconds($workingHours);
                $loggedTime += $currentDayHours;
            } elseif($loggedActivities != null && count($loggedActivities) == 3 && $loggedActivities[1]['status'] == 'Missed Punch') {
                $loggedTime += $this->hoursToSeconds(HOURSPERDAY);
            } else {
                $loggedTime += $this->hoursToSeconds($workingHours);
            }
            $startDate->modify ( '+1 day' );
        }

		return $this->secondsToHours ( $loggedTime );
	}

	public function GetCheckinFromActivities($loggedActivities) {
        foreach ($loggedActivities as $activity) {
            if($activity['status'] != 'Inactivity') {
                return new DateTime($activity ['punchin']);
            }
        }
    }
    public function GetCheckOutFromActivities($loggedActivities) {
	    $checkOutTime = new DateTime();
        foreach ($loggedActivities as $activity) {
            if($activity['status'] != 'Inactivity' && $activity ['punchout'] != null) {
                $checkOutTime = new DateTime($activity ['punchout']);
            }
        }
        return $checkOutTime;
    }

	public function hoursToSeconds($hours) {
		$hours = sprintf ( "%01.2f", $hours );
		$parsed = explode ( ".", $hours, 2 );
		$seconds = $parsed ['0'] * 3600 + $parsed ['1'] * 60;
		return $seconds;
	}
	public function secondsToHours($seconds) {
		$hours = floor ( $seconds / 3600 );
		$minutes = floor ( ($seconds / 60) % 60 );
		$minutes = sprintf ( "%02d", $minutes );
		return "$hours.$minutes";
	}
	public function getLoggedActivity($from_Date, $to_Date, $id) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "SELECT distinct cast(punch_time as date) as punch_date from main_emp_work_activity where user_id = " . $id . " and DATE(punch_time) >= STR_TO_DATE('" . $from_Date . "','" . DATEFORMAT_MYSQL . "') and DATE(punch_time) <= STR_TO_DATE('" . $to_Date . "','" . DATEFORMAT_MYSQL . "') order by punch_time";
		$db->query ( $query )->fetchAll ();
		return $db->query ( $query )->fetchAll ();
	}
    public function getLoggedActivityPerDay($on_Date, $id) {
        $db = Zend_Db_Table::getDefaultAdapter ();
        $query = "SELECT * from main_emp_work_activity where user_id = " . $id . " and DATE(punch_time) = STR_TO_DATE('" . $on_Date . "','" . DATEFORMAT_MYSQL . "')";
        $db->query ( $query )->fetchAll ();
        return $db->query ( $query )->fetchAll ();
    }
	public function getCombinedActiveEmployeeData($startDate, $endDate, $employeeId, $activityBreakdown) {
		$empWorkActivityModel = new Default_Model_Employeeworkactivity ();
		$dailyPunchDetails = array ();
		$attendanceData = array ();
		$workhours = 0;
		while ( $startDate <= $endDate ) {
            $startDate = (new DateTime ( $startDate ));
            $dailyActivity = $empWorkActivityModel->getEmployeeActivities($startDate, $employeeId, $workhours, $activityBreakdown);
			if ($dailyActivity [0] != null) {
                $dailyPunchDetails ['id'] = $employeeId;
                $dailyPunchDetails ['punch_date'] = date ( 'Y-m-d', strtotime ( $dailyActivity [0] ['punchin'] ) );
                $dailyPunchDetails ['check_in'] = (new DateTime ( $dailyActivity [0] ['punchout'] ))->format ( "H:i:s" );
                $dailyPunchDetails ['check_out'] = (new DateTime ( $dailyActivity [count ( $dailyActivity ) - 2] ['punchout'] ))->format ( "H:i:s" );
                $dailyPunchDetails ['working_hours'] = $workhours;
				
				if ($dailyPunchDetails ['working_hours'] >= 7) {
                    $dailyPunchDetails ["overall_status"] = "FULL DAY";
				} elseif ($dailyPunchDetails ['working_hours'] >= 4) {
                    $dailyPunchDetails ["overall_status"] = "HALF DAY";
				} else {
                    $dailyPunchDetails ["overall_status"] = "ABSENT";
				}

                if (count ( $dailyActivity ) == 3 && $dailyActivity [count ( $dailyActivity ) - 2] ['status'] == "Missed Punch") {
                    $dailyPunchDetails ["overall_status"] = "MISSED PUNCH";
                    $dailyPunchDetails ['working_hours'] = HOURSPERDAY;
                }
				
				array_push ( $attendanceData, $dailyPunchDetails );
			}

			$startDate->modify ( '+1 day' );
			$startDate = $startDate->format ( 'Y-m-d' );
		}
		return $attendanceData;
	}
	public function getPunchDetail($from_Date, $id) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select * from main_emp_attendence where user_id = " . $id . " and punch_date = '" . $from_Date . "'";
		return $db->query ( $query )->fetchAll ();
	}
	public function getAuditStartDate() {
		if (date ( 'l' ) == ATTENDENCEAUDITSTARTDAY) {
			return date ( DATEFORMAT_PHP );
		}
		return date ( DATEFORMAT_PHP, strtotime ( "previous " . ATTENDENCEAUDITSTARTDAY ) );
	}
	public function getAuditEndDate() {
		if (date ( 'l' ) == ATTENDENCEAUDITENDDAY) {
			return date ( DATEFORMAT_PHP );
		}
		return date ( DATEFORMAT_PHP, strtotime ( "next " . ATTENDENCEAUDITENDDAY ) );
	}
	public function getRemainingDays() {
		$days = $this->getDaysBetween ( date ( DATEFORMAT_PHP ), $this->getAuditEndDate () ) + 1;
		return $days >= 5 ? 5 : $days;
	}
	public function getRemainingHours($max, $now, $currentDayHours) {
		$max = $this->hoursToSeconds ( $max );
		$now = $this->hoursToSeconds ( $now );
		$result = $max - $now + $currentDayHours < 0 ? 0 : $max - $now + $currentDayHours;
		return $this->secondsToHours ( $result );
	}
	public function getDayFromDate($date) {
		return date ( 'l', strtotime ( $date ) );
	}
	public function getDaysBetween($startDate, $endDate) {
		$start_ts = strtotime ( $startDate );
		$end_ts = strtotime ( $endDate );
		$diff = $end_ts - $start_ts;
		return round ( $diff / 86400 );
	}
	public function getHoursBetween($startTime, $endTime) {
		$interval = $endTime->diff ( $startTime );
		$hours = $interval->format ( '%H' ) . "." . sprintf ( "%02d", $interval->format ( '%i' ) );
		return round ( $hours, 2 );
	}
	public function getApprovedLeaveDetails($id, $leaveDate) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$leaveQuery = "select CONCAT('((',GROUP_CONCAT(leavecode SEPARATOR ')('),'))') as leavetype from main_leaverequest, main_employeeleavetypes
                       where user_id = " . $id . " and from_date <= STR_TO_DATE('" . $leaveDate . "','" . DATEFORMAT_MYSQL . "') and to_date >= STR_TO_DATE('" . $leaveDate . "','" . DATEFORMAT_MYSQL . "')
                       and leavestatus in ('Approved')
                       and main_leaverequest.leavetypeid = main_employeeleavetypes.id
                       and main_leaverequest.isactive = 1
                       group by from_date";
		return $db->query ( $leaveQuery )->fetchAll ();
	}
	public function getLeaveDetails($id, $leaveDate) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$leaveQuery = "select CONCAT('(',GROUP_CONCAT(leavetype SEPARATOR ')('),')') as leavetype from main_leaverequest, main_employeeleavetypes
                       where user_id = " . $id . " and from_date <= STR_TO_DATE('" . $leaveDate . "','" . DATEFORMAT_MYSQL . "') and to_date >= STR_TO_DATE('" . $leaveDate . "','" . DATEFORMAT_MYSQL . "')
                       and leavestatus in ('Pending', 'Approved')
                       and main_leaverequest.leavetypeid = main_employeeleavetypes.id
                       and main_leaverequest.isactive = 1
                       group by from_date";
		return $db->query ( $leaveQuery )->fetchAll ();
	}
	public function getHolidayDetails($holiday) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$holidayQuery = "select * from main_holidaydates, main_holidaygroups where holidaydate = STR_TO_DATE('" . $holiday . "','" . DATEFORMAT_MYSQL . "') and main_holidaydates.groupid = main_holidaygroups.id";
		return $db->query ( $holidayQuery )->fetchAll ();
	}
	public function getMissPunchesBetween($id, $fromDate, $toDate) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select count(*) as misspunches from main_emp_attendence where user_id = " . $id . " and punch_date <= STR_TO_DATE('" . $toDate . "','" . DATEFORMAT_MYSQL . "') and punch_date >= STR_TO_DATE('" . $fromDate . "','" . DATEFORMAT_MYSQL . "') and check_out is null";
		return $db->query ( $query )->fetchAll ();
	}
	public function getHalfDaysBetween($id, $fromDate, $toDate) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select count(*) as halfdays from main_emp_attendence where user_id = " . $id . " and punch_date <= STR_TO_DATE('" . $toDate . "','%d-%m-%Y') and punch_date >= STR_TO_DATE('" . $fromDate . "','%d-%m-%Y') and overall_status = 'Half Day'";
		return $db->query ( $query )->fetchAll ();
	}
	public function getCheckInTime($id, $date) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select check_in from main_emp_attendence where user_id = " . $id . " and punch_date = '" . $date . "'";
		return $db->query ( $query )->fetchAll ();
	}
	public function getCheckOutTime($id, $date) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select check_out from main_emp_attendence where user_id = " . $id . " and punch_date = '" . $date . "'";
		return $db->query ( $query )->fetchAll ();
	}
	public function getWorkingHours($id, $date) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select working_hours from main_emp_attendence where user_id = " . $id . " and punch_date = '" . $date . "'";
		return ($db->query ( $query )->fetchAll ());
	}
	public function getUserWorkingHours($id, $date) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select working_hours from main_emp_attendence where user_id = " . $id . " and punch_date = '" . $date . "'";
		$result = $db->query ( $query )->fetchAll ();
		return $result ['0'] ['working_hours'];
	}
	public function getLeaveTypeCode($leaveType) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$leaveType = substr ( $leaveType, 1, - 1 );
		$query = "select leavecode from main_employeeleavetypes where leavetype = '" . $leaveType . "' and isactive = '1'";
		$leaveTypeCodeArray = $db->query ( $query )->fetchAll ();
		$leaveTypeCode = $leaveTypeCodeArray [0] [leavecode];
		if ($leaveTypeCode != null) {
			return $leaveTypeCode;
		} else {
			return $leaveType;
		}
	}
	public function getEmployeeAttendence($id, $start, $end, $employeeExceptionModel, $activityBreakdown) {
		$userAttendance = $this->getCombinedActiveEmployeeData ( $start, $end, $id, $activityBreakdown );
		$compOffModel = new  Default_Model_Compoffrequest ();
		$compensatoryOffModel = new Default_Model_CompensatoryOff();
		$employeesModel = new Default_Model_Employees();
		$businessUnitId = $employeesModel->getBusinessUnitId($id);
		$currentIterationDate = date ( DATEFORMAT_PHP, strtotime ( $start ) );
		$counter = 0;
		$tempData = array ();
        $dayCount = 0;
		
		while ( strtotime ( $end ) >= strtotime ( $currentIterationDate ) ) {
			$dayOfWeek = date ( l, strtotime ( $currentIterationDate ) );
			if ($currentIterationDate > date ( DATEFORMAT_PHP )) {
				break;
			}
			if ($employeeExceptionModel->checkExceptionExist ( $id, $currentIterationDate, $dayCount )) {
				$tempRow = array ();
				$tempRow ["user_id"] = $id;
				$tempRow ["punch_date"] = $currentIterationDate;
				$tempRow ["status"] = EXCEPTION;
				$tempRow ["overall_status"] = EXCEPTION_STATUS;
				array_push ( $tempData, $tempRow );
				$counter ++;
			} else {
				$userLeaveDetails = $this->getApprovedLeaveDetails ( $id, $currentIterationDate );
				if (strtotime ( $userAttendance [$counter] ["punch_date"] ) == strtotime ( $currentIterationDate )) {
					if (! empty ( $userLeaveDetails )) {
						$pos = strpos ( $userAttendance [$counter] ['status'], ABSENT );
						$leaves = explode ( ')(', $userLeaveDetails [0] ["leavetype"], 2 );
						if (count ( $leaves ) > 1) {
							$leaveType = $userLeaveDetails [0] ["leavetype"];
							$leavesMultiple = explode ( ')', $leaveType, 2 );
							$leavesMultiple [0] = $leavesMultiple [0] . ')';
							$leaveTypeCode = $this->getLeaveTypeCode ( $leavesMultiple [0] ) . ' ' . $this->getLeaveTypeCode ( $leavesMultiple [1] );
							$userAttendance [$counter] ['status'] = $leaveTypeCode;
						} else if ($pos >= 0) {
							$leaveType = $userLeaveDetails [0] ["leavetype"];
							$leaveTypeCode = $this->getLeaveTypeCode ( $leaveType );
							$userAttendance [$counter] ['status'] = substr_replace ( $userAttendance [$counter] ['status'], $leaveTypeCode, $pos, strlen ( ABSENT ) );
						}
					}
					$holidayDetails = $this->getHolidayDetails ( $currentIterationDate );
					if (! empty ( $holidayDetails )) {
						
						if ($tempVar = $compOffModel->checkCompoffByDate($currentIterationDate, $id))
						{
							if ($tempVar['option'] == "Leave")
							{
								$userAttendance [$counter] ['status'] = 'COMPENSATORY-OFF (LEAVE)';
								$userAttendance [$counter] ['overall_status'] = $holidayDetails [0] ["holidayname"];
							}
							else 
							{
								$userAttendance [$counter] ['status'] = 'COMPENSATORY-OFF (ENCASHMENT)';
								$userAttendance [$counter] ['overall_status'] = $holidayDetails [0] ["holidayname"];
							}
						}
						else 
						{
							$userAttendance [$counter] ['overall_status'] = $holidayDetails [0] ["holidayname"];
						}
					}
					if ($currentIterationDate != date ( DATEFORMAT_PHP )) {
						if ($this->isCompOffAvailable ( $id, $currentIterationDate, $userAttendance [$counter] ['working_hours'] )) {
							$compoffrequest = $this->isCompOffAvailed ( $id, $currentIterationDate );
							if ($compoffrequest) {
								if ($compoffrequest [0] ['status'] == 'Pending') {
									$userAttendance [$counter] ['overall_status'] = COMPOFF . " RAISED";
								}
								else if ($compoffrequest [0] ['status'] == 'Approved') 
								{
									if ($compoffrequest [0] ['option'] == 'Encashment') {
										$userAttendance [$counter] ['status'] = 'COMPENSATORY-OFF (LEAVE)';
									} else {
										$userAttendance [$counter] ['status'] = 'COMPENSATORY-OFF (ENCASHMENT)';
									}
								}
							} else {
								$compoffLeaveWindow = $compensatoryOffModel->getCompensatoryOffLeaveWindow($businessUnitId);
								$diff = ((date('Y', strtotime(date('Y-m-d'))) - date('Y', strtotime($currentIterationDate))) * 12) + (date('m', strtotime(date('Y-m-d'))) - date('m', strtotime($currentIterationDate)));
								if ($diff < $compoffLeaveWindow)
									$userAttendance [$counter] ['avail_compoff'] = true;
								$userAttendance [$counter] ['overall_status'] = "";
							}
						}
					} else {
						$userAttendance [$counter] ['status'] = "";
						$userAttendance [$counter] ['overall_status'] = "";
					}
					array_push ( $tempData, $userAttendance [$counter] );
					$counter ++;
				} else {
					$tempRow = array ();
					$tempRow ["user_id"] = $id;
					$tempRow ["punch_date"] = $currentIterationDate;
					if (! empty ( $userLeaveDetails )) {
						$leaveType = $userLeaveDetails [0] ["leavetype"];
						$leaveTypeCode = $this->getLeaveTypeCode ( $leaveType );
						if($leaveTypeCode=="COMP-OFF"){
                            $tempRow ["status"]= "COMPENSATED";
                        }
                        else $tempRow ["status"] = $leaveTypeCode;
						if($leaveTypeCode == "LWP") {
                            $tempRow ["overall_status"] = "ABSENT";
                        } else {
                            $tempRow ["overall_status"] = "LEAVE";
                        }
						array_push ( $tempData, $tempRow );
					} else {
						if ($dayOfWeek == "Saturday" || $dayOfWeek == "Sunday") {
							$tempRow ["overall_status"] = strtoupper ( $dayOfWeek );
							array_push ( $tempData, $tempRow );
						} else {
							$holidayDetails = $this->getHolidayDetails ( $currentIterationDate );
							if (! empty ( $holidayDetails )) {
								$tempRow ["overall_status"] = $holidayDetails [0] ["holidayname"];
								array_push ( $tempData, $tempRow );
							} else {
								$tempRow ["overall_status"] = "ABSENT";
								array_push ( $tempData, $tempRow );
							}
						}
					}
				}
			}
			$currentIterationDate = date ( DATEFORMAT_PHP, strtotime ( "$currentIterationDate +1 day" ) );
		}
		
		return $tempData;
	}
	public function isCompOffAvailable($id, $date, $workingHours) {
		$dayOfWeek = date ( l, strtotime ( $date ) );
		
		$holidayModel = new Default_Model_Holidaydates ();
		$employeesModel = new Default_Model_Employees ();
		$holidayGroup = $employeesModel->getHolidayGroupForEmployee ( $id );
		$isHoliday = false;
		if ($holidayGroup ['0'] ['holiday_group'] != null) {
			$holiday = $holidayModel->getHolidaysWithin ( $date, $date, $holidayGroup ['0'] ['holiday_group'] );
			$isHoliday = $holiday [0] ['holidaydates'] != 0;
		}
		$businessunitid = $this->getEmployeeBusinessUnit ( $id );
		if ($businessunitid != null) {
			$compensatoryOffModel = new Default_Model_CompensatoryOff ();
			$compOffRule = $compensatoryOffModel->getCompensatoryOffRule ( $businessunitid );
			$isFullDayValid = ($workingHours >= $compOffRule ['0'] ['fullDay']);
			$isHalfDayValid = ($workingHours >= $compOffRule ['0'] ['halfDay']);
			return (($dayOfWeek == "Saturday" || $dayOfWeek == "Sunday" || $isHoliday) && $isHalfDayValid);
		}
		return false;
	}
	public function getEmployeeBusinessUnit($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'e' => 'main_employees' 
		), array (
				'e.businessunit_id' 
		) )->where ( "e.isactive = 1 AND e.user_id = " . $id );
		
		$businessUnit = $this->fetchAll ( $result )->toArray ();
		
		return $businessUnit [0] ['businessunit_id'];
	}
	public function isCompOffAvailed($id, $date) {
		$compoffModel = new Default_Model_Compoffrequest ();
		return $compoffModel->checkCompoffByDate ( $date, $id );
	}
	public function getModifierStringFromOfffset($offset) {
		$arr = explode ( ":", $offset );
		$operator = substr ( $arr [0], 0, 1 );
		$hours = substr ( $arr [0], 1 );
		$min = ($hours * 60) + $arr [1];
		return '+ ' . $min . ' minutes';
	}
	public function getMissPunchestodeduct($misspunch, $missedpunchesrule) {
		$daystodeduct = 0;
		if ($missedpunchesrule != null) {
			$threshold = $missedpunchesrule [0] ['threshold'];
			$action = $missedpunchesrule [0] ['action'];
			if ($misspunch > $threshold) {
				$difference = $misspunch - $threshold;
				if ($action == 1)
					$daystodeduct += $difference;
				else if ($action == 2)
					$daystodeduct += ($difference) * (1 / 2);
			}
		}
		return $daystodeduct;
	}
	public function getloggeddatainweek($id, $week, $faultyDays, $loggeddays) {
		$weekday = $week [0];
		$loggeddatainweek = array ();
		$workingdays = 0;
		while ( strtotime ( $weekday ) <= strtotime ( $week [1] ) ) {
			$attendancemodel = new Default_Model_Myattendance ();
			$punchdateindex = array_search ( date ( 'Y-m-d', strtotime ( $weekday ) ), array_column ( $loggeddays, 'punch_date' ) );
			if ($punchdateindex === false) {
				$punchdateindex = - 1;
			}
			$workinghours = $loggeddays [$punchdateindex] ['working_hours'];
			if (($workinghours != null) && (! in_array ( date ( 'Y-m-d', strtotime ( $weekday ) ), $faultyDays ))) {
				$loggeddayduration += $workinghours;
				$workingdays += 1;
			}
			$weekday = date ( 'd-m-Y', strtotime ( $weekday . ' + 1 day' ) );
		}
		array_push ( $loggeddatainweek, $loggeddayduration, $workingdays );
		return $loggeddatainweek;
	}
	public function CalculateWeeks($fromDate, $toDate) {
		$fromday = date ( 'D', strtotime ( $fromDate ) );
		$toDay = date ( 'D', strtotime ( $toDate ) );
		$weeks = array ();
		$week = array ();
		$fridaydate = date ( 'd-m-Y', strtotime ( 'previous friday', strtotime ( $fromDate ) ) );
		while ( strtotime ( $thursdate ) <= strtotime ( $toDate ) ) {
			$week = array ();
			$thursdate = date ( 'd-m-Y', strtotime ( 'next thursday', strtotime ( $fridaydate ) ) );
			array_push ( $week, $fridaydate, $thursdate );
			array_push ( $weeks, $week );
			$fridaydate = date ( 'd-m-Y', strtotime ( (date ( 'Y-m-d', strtotime ( $thursdate ) )) . ' + 1 day' ) );
		}
		return $weeks;
	}
	public function formatHM($input) {
		$parts = explode ( '.', $input );
		
		if ($parts [0] == '')
			$parts [0] = 0;
		if ($parts [1] == '' || $parts [1] == '0' || $parts [1] == '00') {
			return $parts [0] . 'h';
		} else {
			return $parts [0] . 'h' . ' ' . $parts [1] . 'm';
		}
	}
	public function formatHMS($input) {
		$parts = explode ( '.', $input );
		if ($parts [0] == '') {
			$parts [0] = 0;
		}
		if (($parts [1] == '0' || $parts [1] == '00' || $parts [1] == '') && ($parts [2] == '0' || $parts [2] == '00' || $parts [2] == '')) {
			return $parts [0] . 'h';
		} elseif ($parts [1] == '0' || $parts [1] == '00' || $parts [1] == '') {
			return $parts [0] . 'h' . ' ' . $parts [2] . 's';
		} elseif ($parts [2] == '0' || $parts [2] == '00' || $parts [2] == '') {
			return $parts [0] . 'h' . ' ' . $parts [1] . 'm';
		} else
			
			return $parts [0] . 'h ' . $parts [1] . 'm ' . $parts [2] . 's';
	}
	public function checkformat($data) {
		if (DURATION_FORMAT == 'NNh NNm') {
			$data = $this->formatHM ( $data );
		} else if (DURATION_FORMAT == 'NNh NNm NNs') {
			$data = $this->formatHMS ( $data );
		} else if (DURATION_FORMAT == 'NN.NN' && $data == '') {
			$data = '00';
		}
		return $data;
	}
	public function createActivityIntervalFromOfficePunches($officeAttendanceData, $officeHoursRule, $curDate) {
		$activityIntervals = array ();
		$date = new DateTime ( $curDate );
		if ($officeHoursRule == false) {
			$dayStartTime = $date->setTime ( 0, 0, 0 );
			$dayEndTime = new DateTime ( $dayStartTime->format ( "Y-m-d H:i:s" ) );
			$dayEndTime->modify ( '+23 hours 59 minutes 59 seconds' );
		} else {
			$dayStartTime = new DateTime ( $date->format ( 'Y-m-d' ) . ' ' . $officeHoursRule ['start_time'] );
			$dayEndTime = new DateTime ( $dayStartTime->format ( "Y-m-d H:i:s" ) );
			$dayEndTime->modify ( '+23 hours 59 minutes 59 seconds' );
			if ($officeHoursRule ['is_entry_threshold'] == 1 && $officeHoursRule ['entry_threshold_action'] == 1) {
				$dayStartTime = new DateTime ( $date->format ( 'Y-m-d' ) . ' ' . $officeHoursRule ['entry_time'] );
			}
			if ($officeHoursRule ['is_exit_threshold'] == 1 && $officeHoursRule ['exit_threshold_action'] == 1) {
				$dayEndTime = new DateTime ( $date->format ( 'Y-m-d' ) . ' ' . $officeHoursRule ['exit_time'] );
				if ($dayEndTime < $dayStartTime) {
					$dayEndTime->modify ( "+24 hours" );
				}
			}
		}
		foreach ( $officeAttendanceData as $officeAttendanceDatum ) {
			$punchInTime = $officeAttendanceDatum ['punch_date'] . " " . $officeAttendanceDatum ['check_in'];
			$punchOutTime = $officeAttendanceDatum ['punch_date'] . " " . $officeAttendanceDatum ['check_out'];
			if ($officeAttendanceDatum ['check_in'] != null) {
				if (new DateTime ( $punchInTime ) > $dayStartTime && new DateTime ( $punchInTime ) < $dayEndTime) {
					if (! isset ( $activityIntervals ['punchin'] )) {
						$activityIntervals ['punchin'] = $punchInTime;
					} else {
						$activityIntervals ['punchout'] = $punchInTime;
					}
				}
			}
			if ($officeAttendanceDatum ['check_out'] != null) {
				if (new DateTime ( $punchOutTime ) > $dayStartTime && new DateTime ( $punchOutTime ) < $dayEndTime) {
					if (! isset ( $activityIntervals ['punchin'] )) {
						$activityIntervals ['punchin'] = $punchOutTime;
					} else {
						$activityIntervals ['punchout'] = $punchOutTime;
					}
				}
			}
		}
		if (count ( $activityIntervals ) > 0) {
			$activityIntervals ['status'] = 'Office Hours';
		}
		return $activityIntervals;
	}
	public function getPunchDetailsofTwoDays($date, $id) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$day2 = (new DateTime ( $date ))->modify ( "+1 day" )->format ( "Y-m-d" );
		$query = "select * from main_emp_attendence where user_id = " . $id . " and punch_date in( '" . $date . "','" . $day2 . "')";
		return $db->query ( $query )->fetchAll ();
	}
	public function getEmployeeStatus($userid)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array(
            'es' => 'main_employees_summary'
        ), array(
            'es.emp_status_name'
        ))
            ->where('es.user_id=' . $userid . '');
        return $this->fetchAll($select)->toArray();
 	}
}