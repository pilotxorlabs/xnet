<?php
class Default_Model_Compoffrequest extends Zend_Db_Table_Abstract{
	protected $_name = 'main_compoffrequest';

	public function SaveorUpdateCompoffRequest($data, $where) {
	    if ($where != '') {
	        $this->update ( $data, $where );
	        return 'update';
	    } else {
	        $this->insert ( $data );
	        $id = $this->getAdapter ()->lastInsertId ( 'main_compoffrequest' );
	        return $id;
	    }
	}

	public function checkCompoffByDate($avail_date, $loginUserId) {
	    $db = Zend_Db_Table::getDefaultAdapter ();

	    $query = "select * from main_compoffrequest l where l.user_id=" . $loginUserId . " and l.status IN(1,2) and l.isactive = 1
        and l.avail_date = STR_TO_DATE('" . $avail_date . "','". DATEFORMAT_MYSQL."')";

	    $result = $db->query ( $query )->fetchAll ();
	    return $result;
	}

	public function checkCompoffExists($from_date, $to_date, $loginUserId) {
	    $db = Zend_Db_Table::getDefaultAdapter ();

	    $query = "select count(l.id) as compoffexist from main_compoffrequest l where l.user_id=" . $loginUserId . " and l.option = 1 and l.status IN(1) and l.isactive = 1
        and (l.leave_date between '" . $from_date . "' and '" . $to_date . "' OR l.leave_date between '" . $from_date . "' and '" . $to_date . "' )";

	    $result = $db->query ( $query )->fetchAll ();
	    return $result;
	}

	public function getCompOffById($id) {
	    $db = Zend_Db_Table::getDefaultAdapter ();

	    $query = "select * from main_compoffrequest l where id=". $id ."";

	    $result = $db->query ( $query )->fetchAll ();
	    return $result;
	}

	public function getEncashmentsByOption($start_date, $end_date, $loginUserId, $compensatoryoffrule) {
	    $db = Zend_Db_Table::getDefaultAdapter ();

	    if ($compensatoryoffrule['0']['considerationWindow'] == '1')
	    	$considerationDate = 'l.avail_date';
	    else if ($compensatoryoffrule['0']['considerationWindow'] == '2')
	    	$considerationDate = 'l.createddate';
    	else if ($compensatoryoffrule['0']['considerationWindow'] == '3')
    		$considerationDate = 'l.modifieddate';
	    	
	    $query = "select sum(l.appliedleavescount) from main_compoffrequest l where l.user_id=" . $loginUserId . " and l.status IN(2) and l.isactive = 1 and l.option = 2
        and ".$considerationDate." >= STR_TO_DATE('" . $start_date . "','". DATEFORMAT_MYSQL."') and ".$considerationDate." <= STR_TO_DATE('" . $end_date . "','". DATEFORMAT_MYSQL."')";
    	
	    $result = $db->query ( $query )->fetch();
		$result = $result['sum(l.appliedleavescount)'];
	    return $result==null?'0':$result;
	}

	public function getCompoffStatusHistory($sort, $by, $pageNo, $perPage, $searchQuery, $queryflag = '', $loggedinuser, $managerstring = '', $leavetypeid) {
		$auth = Zend_Auth::getInstance ();
		$empRole = $this->getEmployeeRole ( $loggedinuser );
		$request = Zend_Controller_Front::getInstance ();
		$params = $request->getRequest ()->getParams ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		if ($loggedinuser == '')
			$loggedinuser = $loginUserId;

			/* Removing isactive checking from configuration table */
			if ($managerstring != '') {
				$where = "l.isactive = 1 ";
			}
	else {
			if($empRole ==11 && $params['controller'] == 'employeerequests'){
				$where = "l.isactive = 1 AND l.user_id in (select distinct user_id from main_employees where reporting_manager = " . $loggedinuser . ")";
			}
			else if($empRole == 1 && $params['controller'] == 'employeerequests')
			{
				$where = "l.isactive = 1 AND u.isactive = 1 ";
			}
			else if($empRole!=1 &&$empRole!=11 && $params['controller'] == 'employeerequests' ){
			$where = "l.isactive = 1 AND l.user_id in (select distinct user_id from main_employees where reporting_manager = " . $loggedinuser . ")";
		}
		else if(($empRole == 12 || $empRole == 1) && $params['controller'] == 'empleavesummary'){
			$where = "l.isactive = 1 AND u.isactive = 1 ";
		}
		else
			$where = "l.isactive = 1 AND l.user_id = $loggedinuser";
		}


				/* $where = "l.isactive = 1 ";
			} else {

				$where = "l.isactive = 1 ";
			} */

			if ($leavetypeid != '') {
				//$where .= ' AND l.compofftype_id=' . $leavetypeid;
			}
			if ($queryflag != '') {
				if ($queryflag == 'pending') {
					$where .= " AND l.status = 1 ";
				} else if ($queryflag == 'approved') {
					$where .= " AND l.status = 2 ";
				} else if ($queryflag == 'cancelled') {
					$where .= " AND l.status = 4 ";
				} else if ($queryflag == 'rejected') {
					$where .= " AND l.status = 3 ";
				}
				else if ($queryflag == 'expired') {
					$where .= " AND l.status = 5 ";
				}
			} else {
				$where .= " AND l.status = 2 ";
			}

			if ($searchQuery)
				$where .= " AND " . $searchQuery;
				$db = Zend_Db_Table::getDefaultAdapter ();

				$leaveStatusData = $this->select ()->setIntegrityCheck ( false )->from ( array (
						'l' => 'main_compoffrequest'
				), array (
						'l.*',
						'avail_date' => 'DATE_FORMAT(l.avail_date,"' . DATEFORMAT_MYSQL . '")',
						'applieddate' => 'l.createddate',
						new Zend_Db_Expr ( "CASE WHEN l.status=2 and CURDATE()=l.avail_date THEN 'no' WHEN l.status=1 THEN 'yes' WHEN l.status IN (3,4) THEN 'no' ELSE 'yes' END as approved_cancel_flag " )
				) )->joinLeft ( array (
						'u' => 'main_users'
				), 'u.id=l.rep_mang_id', array (
						'reportingmanagername' => 'u.userfullname'
				) )->joinLeft ( array (
						'mu' => 'main_users'
				), 'mu.id=l.user_id', array (
						'userfullname' => 'mu.userfullname'
				) )->joinLeft ( array (
						'mes' => 'main_employees_summary'
				), 'mes.user_id=l.user_id', array (
						'departmentname' => 'mes.department_name'
				) )->where ( $where )->order ( "$by $sort" )->limitPage ( $pageNo, $perPage );
				$emp_data_org = substr ( $leaveStatusData, 0,strrpos($leaveStatusData,"DESC")-1 );
				return $leaveStatusData;
	}

	public function getEmployeeCompoffRequest($sort, $by, $pageNo, $perPage, $searchQuery, $loginUserId) {
		$empRole = $this->getEmployeeRole ( $loginUserId );
		if ($empRole == '1' || $empRole == 12) {
			$where = "l.isactive = 1 AND l.status IN(1,2,3,4,5) AND u.isactive=1";
		} else {
			$where = "l.isactive = 1 AND l.status IN(1,2,3,4,5) AND u.isactive=1 AND l.user_id in (select distinct user_id from main_employees where rep_mang_id = " . $loginUserId . " )";
		}

		if ($searchQuery)
			$where .= " AND " . $searchQuery;
			$db = Zend_Db_Table::getDefaultAdapter ();

			$employeeleaveData = $this->select()->setIntegrityCheck ( false )->from ( array (
					'l' => 'main_compoffrequest'
			), array (
					'l.*',
					'avail_date' => 'DATE_FORMAT(l.avail_date,"' . DATEFORMAT_MYSQL . '")',
					'applieddate' => 'l.createddate',
					'option' => 'l.option',
					'status' =>'l.status'

			) )->joinLeft ( array (
					'u' => 'main_users'
			), 'u.id=l.user_id', array (
					'userfullname' => 'u.userfullname'
			) )->where ( $where )->order ( "$by $sort" )->limitPage ( $pageNo, $perPage );
			return $employeeleaveData;
	}

	public function getGrid($sort, $by, $perPage, $pageNo, $searchData, $call, $dashboardcall, $objName, $queryflag, $unitId = '', $statusidstring = '', $leavetypeid = '') {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		$searchQuery = '';
		$searchArray = array ();
		$data = array ();
		if($objName == 'employeerequests')
		{
			if($searchData != '' && $searchData!='undefined')
			{
				$searchValues = json_decode($searchData);
				foreach($searchValues as $key => $val)
				{
					if($key == 'applieddate')
						$searchQuery .= " l.createddate like '%".  sapp_Global::change_date($val,'database')."%' AND ";
						//modifications in queries
						else if($key == 'avail_date')
						{
							$searchQuery .= " ".$key." = '".  sapp_Global::change_date($val,'database')."'AND";
						}


						//end of modifications
						else
							$searchQuery .= " ".$key." like '%".$val."%' AND ";
							$searchArray[$key] = $val;
				}
				$searchQuery = rtrim($searchQuery," AND");
			}

			$tableFields = array (
					'action' => 'Action',
					'userfullname' => 'Employee',
					'option' => 'Option',
					'avail_date' =>'Avail Date',
					'status' => 'Status',
                    'leave_date' => 'Leave Date',
					'applieddate' => 'Applied On'
			        
			);

			$tablecontent = $this->getEmployeeCompoffRequest ( $sort, $by, $pageNo, $perPage, $searchQuery, $loginUserId );
			$dataTmp = array (
					'sort' => $sort,
					'by' => $by,
					'pageNo' => $pageNo,
					'perPage' => $perPage,
					'tablecontent' => $tablecontent,
					'objectname' => $objName,
					'extra' => array (),
					'tableheader' => $tableFields,
					'jsGridFnName' => 'getAjaxgridData',
					'jsFillFnName' => '',
					'searchArray' => $searchArray,
					'add' => 'add',
					'call' => $call,
					'dashboardcall' => $dashboardcall,
					'search_filters' => array (
							'compoff_date' => array (
									'type' => 'datepicker'
							),
							'applieddate' => array (
									'type' => 'datepicker'
							),
							'modifieddate' => array (
									'type' => 'datepicker'
							)

					)
			);
		} else if ($objName == 'empleavesummary') {
			$managerstring = "true";

			if ($searchData != '' && $searchData != 'undefined') {
				$searchValues = json_decode ( $searchData );
				foreach ( $searchValues as $key => $val ) {
					// if($key !='leavestatus')
					// {
					if ($val != 'undefined') {

						if ($key == 'reportingmanagername')
							$searchQuery .= " u.userfullname like '%" . $val . "%' AND ";
							else if ($key == 'employeename') {
								if (strpos ( $val, '#$$#' ) !== false) {
									$names = explode ( '#$$#', $val );
									$searchQuery .= " mu.userfullname in (";
									foreach ( $names as $filterName ) {
										$searchQuery .= "'" . $filterName . "', ";
									}
									$searchQuery = rtrim ( $searchQuery, ", " );
									$searchQuery .= ") AND";
								} else {
									$searchQuery .= " mu.userfullname like '%" . $val . "%' AND ";
								}
							} else if ($key == 'applieddate') {
								$searchQuery .= " l.createddate  like '%" . sapp_Global::change_date ( $val, 'database' ) . "%' AND ";
							} else if ($key == 'status') {
								if (strpos ( $val, '#$$#' ) !== false) {
									$names = explode ( '#$$#', $val );
									$searchQuery .= " l.status in (";
									foreach ( $names as $filterName ) {
										$searchQuery .= "'" . $filterName . "', ";
									}
									$searchQuery = rtrim ( $searchQuery, ", " );
									$searchQuery .= ") AND";
								} else {
									$searchQuery .= " l.status like '%" . $val . "%' AND ";
								}
							} else if ($key == 'departmentname') {
								if (strpos ( $val, '#$$#' ) !== false) {
									$names = explode ( '#$$#', $val );
									$searchQuery .= " mes.department_name in (";
									foreach ( $names as $filterName ) {
										$searchQuery .= "'" . $filterName . "', ";
									}
									$searchQuery = rtrim ( $searchQuery, ", " );
									$searchQuery .= ") AND";
								} else {
									$searchQuery .= " mes.department_name like '%" . $val . "%' AND ";
								}
							} else if ($key == 'leavetype') {
								if (strpos ( $val, '#$$#' ) !== false) {
									$names = explode ( '#$$#', $val );
									$searchQuery .= " leavetype in (";
									foreach ( $names as $filterName ) {
										$searchQuery .= "'" . $filterName . "', ";
									}
									$searchQuery = rtrim ( $searchQuery, ", " );
									$searchQuery .= ") AND";
								} else {
									$searchQuery .= " leavetype like '%" . $val . "%' AND ";
								}
							} //modifications in queries-paritosh
							else if($key == 'compoff_date')
							{
								$searchQuery .= " ".$key." = '".  sapp_Global::change_date($val,'database')."'";
							}

							// }
							else {
								$searchQuery .= " ".$key." like '%".$val."%' AND ";
								$searchArray [$key] = $val;
							}					
					}
				}
				$searchQuery = rtrim ( $searchQuery, " AND" );
			}

			$statusid = '';
			if ($queryflag != '') {
				$statusid = $queryflag;
				if ($statusid == 1)
					$queryflag = 'pending';
					else if ($statusid == 2)
						$queryflag = 'approved';
						else if ($statusid == 3)
							$queryflag = 'rejected';
							else if ($statusid == 4)
								$queryflag = 'cancel';
			} else {
				$queryflag = 'empleavesummary';
			}

			// $tableFields = array('action'=>'Action','employeename' => 'Leave Applied By','leavetype' => 'Leave Type','leaveday' => 'Leave Duration','from_date' => 'From Date','to_date' => 'To Date','reason' => 'Reason','approver_comments' => 'Comments','reportingmanagername'=>'Reporting Manager','appliedleavescount' => 'Leave Count','applieddate' => 'Applied On');
			$tableFields = array (
					'action' => 'Action',
					'userfullname' => 'Employee',
					'option' => 'Option',
					'avail_date' =>'Avail Date',
					'status' => 'Status',
					'applieddate' => 'Applied On',
			);
			

			/* $leave_arr = array (
					'' => 'All',
					1 => 'Full Day',
					2 => 'Half Day'
			); */

			$search_filters = array (
					'avail_date' => array (
							'type' => 'datepicker'
					),
					'applieddate' => array (
							'type' => 'datepicker'
					)
			);

			/*
			 * This is for dashboard call.
			 * Here one additional column Status is build by passing it to table fields
			 */
			if ($dashboardcall == 'Yes') {
				$tableFields ['status'] = "Status";
				$search_filters ['status'] = array (
						'type' => 'select',
						'filter_data' => array (
								'pending' => 'Pending',
								'approved' => 'Approved',
								'rejected' => 'Rejected',
								'cancel' => 'Cancelled'
						)
				);
				if (isset ( $searchArray ['status'] )) {
					$queryflag = $searchArray ['status'];
					if ($queryflag == '') {
						$queryflag = 'pending';
					}
				}
			}

			$tablecontent = $this->getEmployeeCompoffRequest ( $sort, $by, $pageNo, $perPage, $searchQuery, $loginUserId );

			if (isset ( $queryflag ) && $queryflag != '')
				$formgrid = 'true';
				else
					$formgrid = '';

					$dataTmp = array (
							'sort' => $sort,
							'by' => $by,
							'pageNo' => $pageNo,
							'perPage' => $perPage,
							'tablecontent' => $tablecontent,
							'objectname' => $objName,
							'extra' => array (),
							'tableheader' => $tableFields,
							'jsGridFnName' => 'getAjaxgridData',
							'jsFillFnName' => '',
							'searchArray' => $searchArray,
							'add' => 'add',
							'formgrid' => $formgrid,
							'unitId' => sapp_Global::_encrypt ( $statusid ),
							'call' => $call,
							'dashboardcall' => $dashboardcall,
							'search_filters' => $search_filters
					);
		} else {
			if ($searchData != '' && $searchData != 'undefined') {
				$searchValues = json_decode ( $searchData );
				foreach ( $searchValues as $key => $val ) {
					if ($key == 'reportingmanagername')
						$searchQuery .= " u.userfullname like '%" . $val . "%' AND ";
						else if ($key == 'applieddate') {

							$searchQuery .= " l.createddate  like '%" . sapp_Global::change_date ( $val, 'database' ) . "%' AND ";
						}
						//modification in queries

						else if($key == 'avail_date')
						{
							$searchQuery .= " ".$key." = '".  sapp_Global::change_date($val,'database')."'AND";
						}

						//till here
						else
							$searchQuery .= " mu." . $key . " like '%" . $val . "%' AND ";
							$searchArray [$key] = $val;
				}
				$searchQuery = rtrim ( $searchQuery, " AND" );
			}

			/*
			 * $tableFields = array('leavetype' => 'Leave Type','leaveday' => 'Leave Duration',
			 * 'from_date' => 'From Date','to_date' => 'To Date','reason' => 'Reason','approver_comments' => 'Comments',
			 * "reportingmanagername"=>"Reporting Manager",'appliedleavescount' => 'Leave Count',
			 * 'applieddate' => 'Applied On','action'=>'Action',);
			 */
			if ($objName == 'rejectedcompoff') {
				$tableFields = array (
					'action' => 'Action',
					'userfullname' => 'Employee',
					'option' => 'Option',
					'avail_date' =>'Avail Date',
					'status' => 'Status',
				    'leave_date' => 'Leave Date',
					'applieddate' => 'Applied On',
					'modifieddate' => 'Rejected On'
			);
			} else if ($objName == 'approvedcompoff') {
				$tableFields = array (
					'action' => 'Action',
					'userfullname' => 'Employee',
					'option' => 'Option',
					'avail_date' =>'Avail Date',
					'status' => 'Status',
				    'leave_date' => 'Leave Date',
					'applieddate' => 'Applied On',
					'modifieddate' => 'Approved On'
			);
			}
			else if ($objName == 'cancelledcompoff') {
				$tableFields = array (
					'action' => 'Action',
					'userfullname' => 'Employee',
					'option' => 'Option',
					'avail_date' =>'Avail Date',
					'status' => 'Status',
				    'leave_date' => 'Leave Date',
					'applieddate' => 'Applied On',
					'modifieddate' => 'Cancelled On'
			);
			}
			else if ($objName == 'expiredcompoff') {
				$tableFields = array (
						'action' => 'Action',
						'userfullname' => 'Employee',
						'option' => 'Option',
						'avail_date' =>'Avail Date',
						'status' => 'Status',
                        'leave_date' => 'Leave Date',
						'applieddate' => 'Applied On',
						'modifieddate' => 'Expired On'
				);
			}
			else if ($objName == 'pendingcompoff') {
				$tableFields = array (
					'action' => 'Action',
					'userfullname' => 'Employee',
					'option' => 'Option',
					'avail_date' =>'Avail Date',
					'status' => 'Status',
				    'leave_date' => 'Leave Date',
					'applieddate' => 'Applied On',
			);
			}
			else if ($objName == 'pendingleaves') {
				$tableFields = array (
						'action' => 'Action',
						'option' => 'Option',
						'user_comments' => 'Reason',
						'avail_date' =>'Avail Date',
						'status' => 'Status',
						'leave_date' => 'Leave Date',
						'applieddate' => 'Applied On',
						'modifieddate' => 'Processed On',
						'approver_comments' => 'Comments'
				);
			}
			else if ($objName == 'total') {
				$tableFields = array (
						'action' => 'Action',
						'option' => 'Option',
						'user_comments' => 'Reason',
						'avail_date' =>'Avail Date',
						'status' => 'Status',
						'leave_date' => 'Leave Date',
						'applieddate' => 'Applied On',
						'modifieddate' => 'Processed On',
						'approver_comments' => 'Comments'
				);
			}
			else if ($objName == 'cancelledleaves') {
				$tableFields = array (
						'action' => 'Action',
						'option' => 'Option',
						'user_comments' => 'Reason',
						'avail_date' =>'Avail Date',
						'status' => 'Status',
						'leave_date' => 'Leave Date',
						'applieddate' => 'Applied On',
						'modifieddate' => 'Processed On',
						'approver_comments' => 'Comments'
				);
			}
			else if ($objName == 'approvedleaves') {
				$tableFields = array (
						'action' => 'Action',
						'option' => 'Option',
						'user_comments' => 'Reason',
						'avail_date' =>'Avail Date',
						'status' => 'Status',
						'leave_date' => 'Leave Date',
						'applieddate' => 'Applied On',
						'modifieddate' => 'Processed On',
						'approver_comments' => 'Comments'
				);
			}
			else if ($objName == 'rejectedleaves') {
				$tableFields = array (
						'action' => 'Action',
						'option' => 'Option',
						'user_comments' => 'Reason',
						'avail_date' =>'Avail Date',
						'status' => 'Status',
						'leave_date' => 'Leave Date',
						'applieddate' => 'Applied On',
						'modifieddate' => 'Processed On',
						'approver_comments' => 'Comments'
				);
			}
			else {
				$tableFields = array (
						'action' => 'Action',
						'option' => 'Leave Type',
						'reason' => 'Reason',
						'avail_date' => 'From Date',
						'applieddate' => 'Applied On',
						'modifieddate' => 'Processed On',
						'comments' => 'Comments'

				);
			}
			/* $leave_arr = array (
					'' => 'All',
					1 => 'Full Day',
					2 => 'Half Day'
			); */

			$tablecontent = $this->getcompoffStatusHistory ( $sort, $by, $pageNo, $perPage, $searchQuery, $queryflag, $loginUserId, '', $leavetypeid );

			$dataTmp = array (
					'sort' => $sort,
					'by' => $by,
					'pageNo' => $pageNo,
					'perPage' => $perPage,
					'tablecontent' => $tablecontent,
					'objectname' => $objName,
					'extra' => array (),
					'tableheader' => $tableFields,
					'jsGridFnName' => 'getAjaxgridData',
					'jsFillFnName' => '',
					'searchArray' => $searchArray,
					'add' => 'add',
					'call' => $call,
					'dashboardcall' => $dashboardcall,
					'search_filters' => array (
							'compoff_date' => array (
									'type' => 'datepicker'
							),
							'applieddate' => array (
									'type' => 'datepicker'
							),
							'modifieddate' => array (
									'type' => 'datepicker'
							)
					)
			);
		}

		return $dataTmp;
	}
	public function getEmployeeRole($id) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "select emprole from main_users where id = " . $id . " ";

		$result = $db->query ( $query )->fetchAll ();
		return $result [0] [emprole];
	}
	public function getUserID($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_compoffrequest'
		), array (
				'l.user_id'
		) )->where ( "l.isactive = 1 AND l.id = " . $id );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function getReportingManagerId($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_compoffrequest'
		), array (
				'repmanager' => 'l.rep_mang_id',
				'hrmanager' => 'l.hr_id'
		) )->where ( "l.isactive = 1 AND l.id = " . $id );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function getCompoffRequestDetails($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_compoffrequest'
		), array (
				'l.*'
		) )->where ( "l.isactive = 1 AND l.id = " . $id );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function getmanagercompoffcount($userId,$status = ''){
		$db = Zend_Db_Table::getDefaultAdapter ();
		$leavestatus = "";
		if ($status != '')
			$empRole = $this->getEmployeeRole ( $userId );
			if($empRole ==11){
				$where = "l.isactive = 1 AND l.status = $status AND u.isactive=1 AND l.user_id in (select distinct user_id from main_employees where reporting_manager = " . $userId . ")";
			}
			else if($empRole == 1)
			{
				$where = "l.status = $status AND l.isactive = 1 AND u.isactive = 1";
			}
			else if($empRole == 12)
			{
				$where = "l.status = $status AND l.isactive = 1 AND u.isactive = 1";
			}
			else
				$where = "l.status = $status AND l.isactive = 1 AND u.isactive = 1 AND l.user_id in (select distinct user_id from main_employees where reporting_manager = " . $userId . ")";

			$leavestatus = " and l.leavestatus = $status ";
			$query = "Select count(*) from (SELECT `l`.*, `l`.`createddate` AS
					 `applieddate`, `u`.`userfullname` FROM `main_compoffrequest` AS `l` LEFT JOIN `main_users`
					 AS `u` ON u.id=l.user_id WHERE ($where))as ABC;";
			$result = $db->query ( $query )->fetch ();
			if (! $result ['count(*)'])
				return 0;
				return $result ['count(*)'];
	}
	
	public function getemployeecompoffcount($userId,$status = ''){
		$db = Zend_Db_Table::getDefaultAdapter ();
		if ($status != '')
			
				$where = "c.status = $status AND c.isactive = 1 AND c.user_id = ".$userId."";
				$query = "Select sum(c.appliedleavescount) FROM `main_compoffrequest` c WHERE ($where);";
				
				$result = $db->query ( $query )->fetch ();
				if (! $result['sum(c.appliedleavescount)'])
					return 0;
				return $result['sum(c.appliedleavescount)'];
	}
}