<?php
class Default_Model_MissedPunches extends Zend_Db_Table_Abstract
{
    protected $_name = 'main_missed_punches';
    protected $_primary = 'id';

	public function SaveorUpdateData($data, $where)
	{
		if($where != ''){
			$this->update($data, $where);
			return 'update';
		} else {
			$this->insert($data);
			$id=$this->getAdapter()->lastInsertId('main_missed_punches');
			return $id;
		}
	}

	public function getConfigurationData($configuration_id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'm' => 'main_missed_punches'
		), array (
				'm.*',
		) )->where ( "m.configuration_id = $configuration_id"  );

		return $this->fetchAll ( $result )->toArray ();
	}
	
	public function getMispunchRule($businessUnitId) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		
		$query = "SELECT m.threshold,m.action FROM main_missed_punches as m INNER JOIN main_attendance_configuration as a on m.configuration_id = a.id where a.business_unit_id = " .$businessUnitId." ";
		
		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	
	public function deleteAttendanceConfigurationDetails($id) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "DELETE FROM `main_missed_punches` WHERE `configuration_id` = ".$id." ";
		$db->query ( $query );
	}
	
	public function runCustomValidation($data, &$messageArray){
		
	}
	
	public function checkConfigurationExists($data){
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "SELECT mp.id as id FROM main_attendance_configuration as ac inner join main_missed_punches as mp on ac.id = mp.configuration_id where ac.configuration_type_id = ".$data['configuration_type_id']." and ac.business_unit_id= ".$data['business_unit_id']." ";
		$result = $db->query ( $query )->fetch();
		return $result['id'];
	}
}