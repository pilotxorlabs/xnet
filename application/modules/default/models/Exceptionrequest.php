<?php
class Default_Model_ExceptionRequest extends Zend_Db_Table_Abstract {
	protected $_name = 'main_exceptionrequest';
	protected $_primary = 'id';
	public function getExceptionsCount($userid, $status = '') {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$exceptionstatus = "";
		if ($status != '')
			$leavestatus = " and e.exceptionstatus = $status ";

		$query = "select sum(appliedexceptioncount) cnt from main_exceptionrequest e
				where e.isactive = 1 and e.user_id = $userid " . $leavestatus;
		$result = $db->query ( $query )->fetch ();
		if (! $result ['cnt'])
			return 0;
		return $result ['cnt'];
	}

	public function getmanagerexceptioncount($userId,$status = ''){
		$db = Zend_Db_Table::getDefaultAdapter ();
		$empRole = $this->getEmployeeRole ( $userId );
		if($empRole ==11){
			$where = "l.isactive = 1 AND l.exceptionstatus = $status AND u.isactive=1 AND l.user_id in (select distinct user_id from main_employees where reporting_manager = " . $userId . ")";
		}
		else if($empRole ==12){
			$where = "l.exceptionstatus = $status AND l.isactive = 1 AND u.isactive = 1";
		}
		else if($empRole == 1)
		{
			$where = "l.exceptionstatus = $status AND l.isactive = 1 AND u.isactive = 1";
		}
		else
			$where = "l.exceptionstatus = $status AND l.isactive = 1 AND u.isactive = 1 AND l.user_id in (select distinct user_id from main_employees where reporting_manager = " . $userId . ")";

			$query = " select count(*) from(SELECT `l`.*,  `u`.`userfullname` AS
					 `reportingmanagername`, `mu`.`userfullname` FROM `main_exceptionrequest` AS `l` LEFT JOIN `main_users` AS `u` ON u.id=l.rep_mang_id LEFT JOIN
					 `main_users` AS `mu` ON mu.id=l.user_id WHERE ($where))as abc;";
			$result = $db->query ( $query )->fetch ();
			if (! $result ['count(*)'])
				return 0;
				return $result ['count(*)'];
	}

	public function getAvailableExceptions($loginUserId) {
		$select = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'e' => 'main_exceptionrequest'
		) )->where ( 'e.user_id=' . $loginUserId . ' AND e.isactive = 1' );
		return $this->fetchAll ( $select )->toArray ();
	}
	public function getsinglePendingExceptionData($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_exceptionrequest'
		), array (
				'l.*'
		) )->where ( "l.isactive = 1 AND l.id = " . $id );
		return $this->fetchAll ( $result )->toArray ();
	}
	public function getUserExceptionsData($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_exceptionrequest'
		), array (
				'l.*'
		) )->where ( "l.isactive = 1 AND l.user_id = " . $id );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function getUserApprovedOrPendingExceptionsData($id) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "SELECT `l`.*,IF(l.exceptionstatus = 'Approved', 'A', 'P') as status FROM `main_exceptionrequest` AS `l` WHERE (l.isactive = 1 AND l.user_id = '$id' and l.exceptionstatus IN(1,2))";

		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	public function getManagerApprovedOrPendingExceptionsData($id) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "SELECT `l`.*,IF(l.exceptionstatus = 'Approved', 'A', 'P') as status,u.userfullname
		FROM `main_exceptionrequest` AS `l` left join main_users u on u.id=l.user_id
		WHERE (l.isactive = 1 AND l.rep_mang_id = '$id' and l.exceptionstatus IN(1,2))";

		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	public function getReportingManagerId($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_exceptionrequest'
		), array (
				'repmanager' => 'l.rep_mang_id',
				'hrmanager' => 'l.hr_id'
		) )->where ( "l.isactive = 1 AND l.id = " . $id );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function SaveorUpdateExceptionRequest($data, $where) {
		if ($where != '') {
			$this->update ( $data, $where );
			return 'update';
		} else {
			$this->insert ( $data );
			$id = $this->getAdapter ()->lastInsertId ( 'main_exceptionrequest' );
			return $id;
		}
	}
	public function getExceptionStatusHistory($sort, $by, $pageNo, $perPage, $searchQuery, $queryflag = '', $loggedinuser, $managerstring = '') {
		$empRole = $this->getEmployeeRole ( $loggedinuser );
		$request = Zend_Controller_Front::getInstance ();
		$params = $request->getRequest ()->getParams ();
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		if ($loggedinuser == '')
			$loggedinuser = $loginUserId;

			/* Removing isactive checking from configuration table */
		if ($managerstring != '') {

			$where = "l.isactive = 1 ";
		}
		else {
		if($empRole ==11 && $params['controller'] == 'employeerequests'){
				$where = "l.isactive = 1 AND l.user_id in (select distinct user_id from main_employees where reporting_manager = " . $loggedinuser . ")";
			}
			else if($empRole == 1 && $params['controller'] == 'employeerequests')
			{
				$where = "l.isactive = 1 AND u.isactive = 1 ";
			}
			else if($empRole != 11 && $empRole != 1 && $params['controller'] == 'employeerequests'){
			$where = "l.isactive = 1 AND l.user_id in (select distinct user_id from main_employees where reporting_manager = " . $loggedinuser . ")";
		}
		else if(($empRole == 12 || $empRole == 1) && $params['controller'] == 'empleavesummary'){
			$where = "l.isactive = 1 AND u.isactive = 1 ";
		}
		else
			$where = "l.isactive = 1 AND l.user_id = $loggedinuser";
		}


		if ($queryflag != '') {
			if ($queryflag == 'pending') {
				$where .= " AND l.exceptionstatus = 1 ";
			} else if ($queryflag == 'approved') {
				$where .= " AND l.exceptionstatus = 2 ";
			} else if ($queryflag == 'cancelled') {
				$where .= " AND l.exceptionstatus = 4 ";
			} else if ($queryflag == 'rejected') {
				$where .= " AND l.exceptionstatus = 3 ";
			} else if ($queryflag == 'expired') {
				$where .= " AND l.exceptionstatus = 5 ";
			}
		} else {
			$where .= " AND l.exceptionstatus = 2 ";
		}

		if ($searchQuery)
			$where .= " AND " . $searchQuery;
		$db = Zend_Db_Table::getDefaultAdapter ();
		$empRole = $this->getEmployeeRole ( $loginUserId );

		$exceptionStatusData = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_exceptionrequest'
		), array (
				'l.*',
				'from_date' => 'DATE_FORMAT(l.from_date,"' . DATEFORMAT_MYSQL . '")',
				'to_date' => 'DATE_FORMAT(l.to_date,"' . DATEFORMAT_MYSQL . '")',
				'applieddate' => 'l.createddate',
				new Zend_Db_Expr ( "CASE WHEN l.exceptionstatus=2 and CURDATE()>=l.from_date THEN 'no' WHEN l.exceptionstatus=1 THEN 'yes' WHEN l.exceptionstatus IN (3,4) THEN 'no' ELSE 'yes' END as approved_cancel_flag " )
		) )->joinLeft ( array (
				'u' => 'main_users'
		), 'u.id=l.user_id', array (
				'userfullname' => 'u.userfullname'
		) )/* ->joinLeft ( array (
				'mu' => 'main_users'
		), 'mu.id=l.user_id', array (
				'employeename' => 'mu.userfullname'
		) ) */->where ( $where)->order ( "$by $sort" )->limitPage ( $pageNo, $perPage );
		return $exceptionStatusData;
	}
	public function getEmployeeRole($id) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "select emprole from main_users where id = " . $id . " ";

		$result = $db->query ( $query )->fetchAll ();
		return $result [0] [emprole];
	}
	public function getEmployeeExceptionRequest($sort, $by, $pageNo, $perPage, $searchQuery, $loginUserId) {
		$empRole = $this->getEmployeeRole ( $loginUserId );
		$where = "l.isactive = 1 AND l.exceptionstatus IN(1,2,3,4,5) AND u.isactive=1 AND (" . $empRole . "=1 OR l.rep_mang_id=" . $loginUserId . " OR l.hr_id=" . $loginUserId . " ) and l.user_id!=" . $loginUserId . " ";

		if ($searchQuery)
			$where .= " AND " . $searchQuery;
		$db = Zend_Db_Table::getDefaultAdapter ();

		$employeeexceptionData = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_exceptionrequest'
		), array (
				'l.*',
				'from_date' => 'DATE_FORMAT(l.from_date,"' . DATEFORMAT_MYSQL . '")',
				'to_date' => 'DATE_FORMAT(l.to_date,"' . DATEFORMAT_MYSQL . '")',
				'applieddate' => 'l.createddate',
				new Zend_Db_Expr ( "CASE WHEN l.exceptionstatus=2 and CURDATE()>=l.from_date THEN 'no' WHEN l.exceptionstatus=1 THEN 'yes' WHEN l.exceptionstatus IN (3,4) THEN 'no' ELSE 'yes' END as approved_cancel_flag " )
		) )->joinLeft ( array (
				'u' => 'main_users'
		), 		'u.id=l.user_id',
				array (
				'userfullname' => 'u.userfullname'
		) )->where ( $where )->order ( "$by $sort" )->limitPage ( $pageNo, $perPage );

		$emp_data_org = substr ( $employeeexceptionData, 0,strrpos($employeeexceptionData,"order")-1 );
		$data = $db->query($employeeexceptionData)->fetchAll();
		return $employeeexceptionData;
	}
	public function getUserID($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_exceptionrequest'
		), array (
				'l.user_id'
		) )->where ( "l.isactive = 1 AND l.id = " . $id );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function getExceptionRequestDetails($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_exceptionrequest'
		), array (
				'l.*'
		) )->where ( "l.isactive = 1 AND l.id = " . $id );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function checkdateexists($from_date, $to_date, $loginUserId) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "select count(l.id) as dateexist from main_exceptionrequest l where l.user_id=" . $loginUserId . " and l.exceptionstatus IN(1,2) and l.isactive = 1
        and (l.from_date between '" . $from_date . "' and '" . $to_date . "' OR l.to_date between '" . $from_date . "' and '" . $to_date . "' )";

		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	public function updateemployeeexceptions($exceptionstatus, $employeeid, $comments, $id) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$db->query ( "update main_exceptionrequest  set exceptionstatus = '" . $exceptionstatus . "' , approver_comments ='" . $comments . "' where user_id = " . $employeeid . " AND id ='" . $id . "' AND isactive = 1" );
	}
	public function updatecancelledemployeeexceptions($exceptionstatus, $employeeid, $comments, $id) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$db->query ( "update main_exceptionrequest  set exceptionstatus = '" . $exceptionstatus . "' , approver_comments ='" . $comments . "' where user_id = " . $employeeid . " AND id ='" . $id . "' AND isactive = 1" );
	}
	/*
	 * This function is common to manager employee leaves, employee leaves , approved,cancel,pending and rejected leaves
	 * Here differentiation is done based on objname.
	 */
	public function getGrid($sort, $by, $perPage, $pageNo, $searchData, $call, $dashboardcall, $objName, $queryflag, $unitId = '', $statusidstring = '', $leavetypeid = '') {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		$searchQuery = '';
		$searchArray = array ();
		$data = array ();
		if ($objName == 'employeerequests') {
			if ($searchData != '' && $searchData != 'undefined') {
				$searchValues = json_decode ( $searchData );
				foreach ( $searchValues as $key => $val ) {
					if ($key == 'createddate')
						$searchQuery .= " l.createddate like '%" . sapp_Global::change_date ( $val, 'database' ) . "%' AND ";
						//modifications in queries
						else if($key == 'from_date')
						{
							$searchQuery .= " ".$key." >= '".  sapp_Global::change_date($val,'database')."'AND";
						}
						else if ( $key == 'to_date')
						{$searchQuery .= " ".$key." <= '".  sapp_Global::change_date($val,'database')."'AND";}

						else
						$searchQuery .= " " . $key . " like '%" . $val . "%' AND ";
						$searchArray [$key] = $val;
						}
						$searchQuery = rtrim ( $searchQuery, " AND" );
						}

			$tableFields = array (
					'action' => 'Action',
					'userfullname' => 'Employee',
					'reason'=>'Reason',
					'from_date' => 'From',
					'to_date' => 'To',
					'appliedexceptioncount' => 'Days',
					'exceptionstatus' => 'Exception Status',
					'createddate'=>'Applied On'
			);

			$leave_arr = array (
					'' => 'All'
			);

			$tablecontent = $this->getEmployeeExceptionRequest ( $sort, $by, $pageNo, $perPage, $searchQuery, $loginUserId );
			$dataTmp = array (
					'sort' => $sort,
					'by' => $by,
					'pageNo' => $pageNo,
					'perPage' => $perPage,
					'tablecontent' => $tablecontent,
					'objectname' => $objName,
					'extra' => array (),
					'tableheader' => $tableFields,
					'jsGridFnName' => 'getAjaxgridData',
					'jsFillFnName' => '',
					'searchArray' => $searchArray,
					'add' => 'add',
					'call' => $call,
					'dashboardcall' => $dashboardcall,
					'search_filters' => array (
							'from_date' => array (
									'type' => 'datepicker'
							),
							'to_date' => array (
									'type' => 'datepicker'
							),
							'applieddate' => array (
									'type' => 'datepicker'
							),
							'leaveday' => array (
									'type' => 'select',
									'filter_data' => $leave_arr
							)
					)
			);
		} else if ($objName == 'empleavesummary') {
			$managerstring = "true";

			if ($searchData != '' && $searchData != 'undefined') {
				$searchValues = json_decode ( $searchData );
				foreach ( $searchValues as $key => $val ) {
						if ($key == 'reportingmanagername')
							$searchQuery .= " u.userfullname like '%" . $val . "%' AND ";
						else if ($key == 'employeename')
							$searchQuery .= " mu.userfullname like '%" . $val . "%' AND ";
						else if ($key == 'createddate') {
							$searchQuery .= " l.createddate  like '%" . sapp_Global::change_date ( $val, 'database' ) . "%' AND ";
						} //modifications in queries
						else if($key == 'from_date')
						{
							$searchQuery .= " ".$key." >= '".  sapp_Global::change_date($val,'database')."'AND";
						}
						else if ( $key == 'to_date')
						{$searchQuery .= " ".$key." <= '".  sapp_Global::change_date($val,'database')."'AND";}
					//till here
					 else
							$searchQuery .= " " . $key . " like '%" . $val . "%' AND ";
					$searchArray [$key] = $val;
				}
				$searchQuery = rtrim ( $searchQuery, " AND" );
			}

			$statusid = '';
			if ($queryflag != '') {
				$statusid = $queryflag;
				if ($statusid == 1)
					$queryflag = 'pending';
				else if ($statusid == 2)
					$queryflag = 'approved';
				else if ($statusid == 3)
					$queryflag = 'rejected';
				else if ($statusid == 4)
					$queryflag = 'cancel';
			} else {
				$queryflag = 'approved';
			}

			// $tableFields = array('action'=>'Action','employeename' => 'Leave Applied By','leavetype' => 'Leave Type','leaveday' => 'Leave Duration','from_date' => 'From Date','to_date' => 'To Date','reason' => 'Reason','approver_comments' => 'Comments','reportingmanagername'=>'Reporting Manager','appliedleavescount' => 'Leave Count','applieddate' => 'Applied On');
			$tableFields = array (
					'action' => 'Action',
					'userfullname' => 'Employee',
					'from_date' => 'From Date',
					'to_date' => 'To Date',
					'exceptionstatus' =>'Status',
					'appliedexceptioncount' => 'Exception Count',
					'createddate' => 'Applied On'
			);

			$leave_arr = array (
					'' => 'All',
					1 => 'Full Day',
					2 => 'Half Day'
			);

			$search_filters = array (
					'from_date' => array (
							'type' => 'datepicker'
					),
					'to_date' => array (
							'type' => 'datepicker'
					),
					'applieddate' => array (
							'type' => 'datepicker'
					),
					'leaveday' => array (
							'type' => 'select',
							'filter_data' => $leave_arr
					)
			);

			/*
			 * This is for dashboard call.
			 * Here one additional column Status is build by passing it to table fields
			 */
			if ($dashboardcall == 'Yes') {
				$tableFields ['exceptionstatus'] = "Status";
				$search_filters ['exceptionstatus'] = array (
						'type' => 'select',
						'filter_data' => array (
								'pending' => 'Pending',
								'approved' => 'Approved',
								'rejected' => 'Rejected',
								'cancel' => 'Cancelled'
						)
				);
				if (isset ( $searchArray ['exceptionstatus'] )) {
					$queryflag = $searchArray ['exceptionstatus'];
					if ($queryflag == '') {
						$queryflag = 'pending';
					}
				}
			}

			$tablecontent = $this->getExceptionStatusHistory ( $sort, $by, $pageNo, $perPage, $searchQuery, $queryflag, $loginUserId, $managerstring );

			if (isset ( $queryflag ) && $queryflag != '')
				$formgrid = 'true';
			else
				$formgrid = '';

			$dataTmp = array (
					'sort' => $sort,
					'by' => $by,
					'pageNo' => $pageNo,
					'perPage' => $perPage,
					'tablecontent' => $tablecontent,
					'objectname' => $objName,
					'extra' => array (),
					'tableheader' => $tableFields,
					'jsGridFnName' => 'getAjaxgridData',
					'jsFillFnName' => '',
					'searchArray' => $searchArray,
					'add' => 'add',
					'formgrid' => $formgrid,
					'unitId' => sapp_Global::_encrypt ( $statusid ),
					'call' => $call,
					'dashboardcall' => $dashboardcall,
					'search_filters' => $search_filters
			);
		} else {
			if ($searchData != '' && $searchData != 'undefined') {
				$searchValues = json_decode ( $searchData );
				foreach ( $searchValues as $key => $val ) {
					if ($key == 'reportingmanagername')
						$searchQuery .= " u.userfullname like '%" . $val . "%' AND ";
					else if ($key == 'applieddate') {

						$searchQuery .= " l.createddate  like '%" . sapp_Global::change_date ( $val, 'database' ) . "%' AND ";
					}
					//modifications in queries-paritosh
					else if($key == 'from_date')
					{
						$searchQuery .= " ".$key." >= '".  sapp_Global::change_date($val,'database')."'AND";
					}
					else if ( $key == 'to_date')
					{$searchQuery .= " ".$key." <= '".  sapp_Global::change_date($val,'database')."'AND";}
					//till here
					else
						$searchQuery .= " " . $key . " like '%" . $val . "%' AND ";
					$searchArray [$key] = $val;
				}
				$searchQuery = rtrim ( $searchQuery, " AND" );
			}

			if ($queryflag == 'approved') {
				$tableFields = array (
						'action' => 'Action',
						'userfullname' => 'Employee',
						'reason' => 'Reason',
						'from_date' => 'From Date',
						'to_date' => 'To Date',
						'appliedexceptioncount' => 'Days',
						'createddate' => 'Applied On',
						'modifieddate' => 'Approved On'
				);
			} else if ($queryflag == 'rejected') {
				$tableFields = array (
						'action' => 'Action',
						'userfullname' => 'Employee',
						'reason' => 'Reason',
						'from_date' => 'From Date',
						'to_date' => 'To Date',
						'appliedexceptioncount' => 'Days',
						'createddate' => 'Applied On',
						'modifieddate' => 'Rejected On'
				);
			}
			else if ($queryflag == 'cancelled') {
				$tableFields = array (
						'action' => 'Action',
						'userfullname' => 'Employee',
						'reason' => 'Reason',
						'from_date' => 'From Date',
						'to_date' => 'To Date',
						'appliedexceptioncount' => 'Days',
						'createddate' => 'Applied On',
						'modifieddate' => 'Cancelled On'
				);
			}
			else if ($queryflag == 'expired') {
				$tableFields = array (
						'action' => 'Action',
						'userfullname' => 'Employee',
						'reason' => 'Reason',
						'from_date' => 'From Date',
						'to_date' => 'To Date',
						'appliedexceptioncount' => 'Days',
						'createddate' => 'Applied On',
						'modifieddate' => 'Expired On'
				);
			}
			
			else if ($queryflag == 'pending') {
				$tableFields = array (
						'action' => 'Action',
						'userfullname' => 'Employee',
						'reason' => 'Reason',
						'from_date' => 'From Date',
						'to_date' => 'To Date',
						'appliedexceptioncount' => 'Days',
						'createddate' => 'Applied On'
				);
			}

			else {
				$tableFields = array (
						'action' => 'Action',
						'userfullname' => 'Employee',
						'reason' => 'Reason',
						'exceptionstatus' => 'Status',
						'from_date' => 'From Date',
						'to_date' => 'To Date',
						'appliedexceptioncount' => 'Days',
						'createddate' => 'Applied On'
				);
			}
			$leave_arr = array (
					'' => 'All',
					1 => 'Full Day',
					2 => 'Half Day'
			);

			$tablecontent = $this->getExceptionStatusHistory ( $sort, $by, $pageNo, $perPage, $searchQuery, $queryflag, $loginUserId, '' );

			$dataTmp = array (
					'sort' => $sort,
					'by' => $by,
					'pageNo' => $pageNo,
					'perPage' => $perPage,
					'tablecontent' => $tablecontent,
					'objectname' => $objName,
					'extra' => array (),
					'tableheader' => $tableFields,
					'jsGridFnName' => 'getAjaxgridData',
					'jsFillFnName' => '',
					'searchArray' => $searchArray,
					'add' => 'add',
					'call' => $call,
					'dashboardcall' => $dashboardcall,
					'search_filters' => array (
							'from_date' => array (
									'type' => 'datepicker'
							),
							'to_date' => array (
									'type' => 'datepicker'
							),
							'applieddate' => array (
									'type' => 'datepicker'
							),
							'leaveday' => array (
									'type' => 'select',
									'filter_data' => $leave_arr
							)
					)
			);
		}

		return $dataTmp;
	}
	public function getUsersAppliedExceptions($userId) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_exceptionrequest'
		), array (
				'l.from_date',
				'l.to_date'
		) )->where ( "l.isactive = 1 AND l.user_id = " . $userId . " AND l.exceptionstatus IN(1,2)" );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function getExceptionDetails($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_exceptionrequest_summary'
		), array (
				'l.*'
		) )->where ( "l.isactive = 1 AND l.exceptionrequest_id = " . $id . " " );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function getsingleExceptionData($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_exceptionrequest'
		), array (
				'l.*'
		) )->where ( "l.isactive = 1 AND l.id = " . $id );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function checkExceptionExists($applied_from_date, $applied_to_date, $from_date, $to_date, $loginUserId) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "select count(l.id) as exceptionexist from main_exceptionrequest l where l.user_id=" . $loginUserId . " and l.exceptionstatus IN(1,2) and l.isactive = 1
        and ('" . $from_date . "' between '" . $applied_from_date . "' and '" . $applied_to_date . "' OR '" . $to_date . "' between '" . $applied_from_date . "' and '" . $applied_to_date . "' )";

		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	public function expirePendingExceptionsData($to_date) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "Update main_exceptionrequest l set l.exceptionstatus = 5 WHERE (l.isactive = 1 and l.exceptionstatus =1 and l.to_date<'" . $to_date . "' )";

		$result = $db->query ( $query )->execute ();
		return $result;
	}
	public function getUsersApprovedExceptionsBetween($userId, $fromDate, $toDate) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select l.appliedexceptioncount as exceptioncount, l.from_date, l.to_date from main_exceptionrequest l
				where l.user_id=" . $userId . " and l.exceptionstatus = 2
				and ((l.from_date between STR_TO_DATE('" . $fromDate . "','" . DATEFORMAT_MYSQL . "') and  STR_TO_DATE('" . $toDate . "','" . DATEFORMAT_MYSQL . "'))
						OR (l.to_date between STR_TO_DATE('" . $fromDate . "','" . DATEFORMAT_MYSQL . "') and  STR_TO_DATE('" . $toDate . "','" . DATEFORMAT_MYSQL . "'))
						OR (STR_TO_DATE('" . $fromDate . "','" . DATEFORMAT_MYSQL . "') between l.from_date and l.to_date)
						OR (STR_TO_DATE('" . $toDate . "','" . DATEFORMAT_MYSQL . "') between l.from_date and l.to_date))";

		$result = $db->query ( $query )->fetchAll ();
		return $result == null ? 0 : $this->removeOutOfRangeExceptions ( $result, $fromDate, $toDate );
	}
	public function removeOutOfRangeExceptions($exceptionsData, $fromDate, $toDate) {
		$startDate = new DateTime ( $fromDate );
		$endDate = new DateTime ( $toDate );
		$appliedExceptioncount = 0;

		foreach ( $exceptionsData as $exceptionData ) {
			$exceptionStartDate = new DateTime ( $exceptionData ['from_date'] );
			$exceptionEndDate = new DateTime ( $exceptionData ['to_date'] );
			if ($exceptionStartDate < $startDate) {
				$appliedExceptioncount += ($exceptionData ['exceptioncount'] - $exceptionStartDate->diff ( $startDate )->days);
				if ($exceptionEndDate > $endDate) {
					$appliedExceptioncount -= $endDate->diff ( $exceptionEndDate )->days;
				}
			} else if ($exceptionEndDate > $endDate) {
				$appliedExceptioncount += ($exceptionData ['exceptioncount'] - $endDate->diff ( $exceptionEndDate )->days);
			} else {
				$appliedExceptioncount += $exceptionData ['exceptioncount'];
			}
		}

		return $appliedExceptioncount;
	}
	public function checkExceptionExist($id, $date, &$dayCount) {
	    $dayCount = 0;
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select exceptionday from main_exceptionrequest l where l.user_id=" . $id . " and l.exceptionstatus = 2
					and STR_TO_DATE('" . $date . "','" . DATEFORMAT_MYSQL . "') between l.from_date and l.to_date";

		$result = $db->query ( $query )->fetchAll ();
		if(count($result) > 1) {
            $dayCount = 1;
            return true;
        } else if (count($result) == 1) {
		    if($result[0]['exceptionday'] == 2) {
		        $dayCount = 0.5;
            } else {
		        $dayCount = 1;
            }
            return true;
        } else {
            return false;
        }
	}

    public function checkExceptionEntry($id, $date)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query="select exceptionday from main_exceptionrequest l where l.user_id=" . $id . " and (l.from_date = '" . $date . "' or l.to_date = '" . $date . "' or '" . $date . "' between l.from_date and l.to_date)";
        $result = $db->query($query)->fetchAll();
        if (!empty($result))
            $tempData = true;
        else $tempData = false;
        return $tempData;
    }
}