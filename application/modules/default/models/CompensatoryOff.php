<?php
class Default_Model_CompensatoryOff extends Zend_Db_Table_Abstract
{
    protected $_name = 'main_compensatory_off';
    protected $_primary = 'id';

	public function SaveorUpdateData($data, $where)
	{
		if($where != ''){
			$this->update($data, $where);
			return 'update';
		} else {
			$this->insert($data);
			$id=$this->getAdapter()->lastInsertId('main_compensatory_off');
			return $id;
		}
	}

	public function getConfigurationData($configuration_id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'm' => 'main_compensatory_off'
		), array (
				'm.*',
		) )->where ( "m.configuration_id = $configuration_id"  );

		return $this->fetchAll ( $result )->toArray ();
	}
	
	public function getCompensatoryOffRule($businessUnitId) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		
		$query = "SELECT * FROM main_compensatory_off as co INNER JOIN main_attendance_configuration as a on co.configuration_id = a.id where a.business_unit_id = " .$businessUnitId." ";
		
		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	
	public function getCompensatoryOffLeaveWindow($businessUnitId) {
		$db = Zend_Db_Table::getDefaultAdapter ();
	
		$query = "SELECT leaveWindow FROM main_compensatory_off as co INNER JOIN main_attendance_configuration as a on co.configuration_id = a.id where a.business_unit_id = " .$businessUnitId." ";
	
		$result = $db->query ( $query )->fetch();
		return $result['leaveWindow'];
	}
	
	public function getCompensatoryHalfDay($businessUnitId) {
		$db = Zend_Db_Table::getDefaultAdapter ();
	
		$query = "SELECT halfDay FROM main_compensatory_off as co INNER JOIN main_attendance_configuration as a on co.configuration_id = a.id where a.business_unit_id = " .$businessUnitId." ";
	
		$result = $db->query ( $query )->fetch();
		return $result['halfDay'];
	}
	
	public function getCompensatoryFullDay($businessUnitId) {
		$db = Zend_Db_Table::getDefaultAdapter ();
	
		$query = "SELECT fullDay FROM main_compensatory_off as co INNER JOIN main_attendance_configuration as a on co.configuration_id = a.id where a.business_unit_id = " .$businessUnitId." ";
	
		$result = $db->query ( $query )->fetch();
		return $result['fullDay'];
	}
	
	public function deleteAttendanceConfigurationDetails($id) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "DELETE FROM `main_compensatory_off` WHERE `configuration_id` = ".$id." ";
		$db->query ( $query );
	}
	
	public function runCustomValidation($data, &$messageArray){
		
	}
	
	public function checkConfigurationExists($data){
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "SELECT co.id as id FROM main_attendance_configuration as ac inner join main_compensatory_off as co on ac.id = co.configuration_id where ac.configuration_type_id = ".$data['configuration_type_id']." and ac.business_unit_id= ".$data['business_unit_id']." ";
		$result = $db->query ( $query )->fetch();
		return $result['id'];
	}
}