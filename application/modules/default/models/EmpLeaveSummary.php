<?php
/********************************************************************************* 

 *  Copyright (C) 2014 Sapplica
 *   

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/

class Default_Model_EmpLeaveSummary extends Zend_Db_Table_Abstract
{
    protected $_name = 'main_leaverequest';
    

	public function UpdateLeaveRequestEmpSummary($data, $where)
	{
		$data['parent']=$where['id=?'];
		$this->insert($data);
		$id=$this->getAdapter()->lastInsertId('main_leaverequest');
		
		$isActive = array();
		$isActive['isactive']=0;
		$this->update($isActive, $where);		
	}
	
	public function getParentId($id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$query ="select parent from main_leaverequest where id = ".$id." ";
	
		$result = $db->query($query)->fetchAll();
		return $result;
	
	}
	
}