<?php

class Default_Model_LogUploadPayslip extends Zend_Db_Table_Abstract{
	protected $_name ='tbl_logs_upload_paysilp';
	protected $_primary = 'id';


	public function SaveorUpdatelogs($data, $where)
	{
		if($where != ''){
			$this->update($data, $where);
			return 'update';
		} else {
			$this->insert($data);
			$id=$this->getAdapter()->lastInsertId('tbl_payslip');
			return $id;
		}
	}

	public function getLastuploadId()
	{
		$db = Zend_Db_Table::getDefaultAdapter ();
		$select = "select max(upload_id) as upload_id from tbl_logs_upload_paysilp";
		return $db->query($select)->fetch();
	}
}