<?php
class Default_Model_OfficeHours extends Zend_Db_Table_Abstract {
	protected $_name = 'main_office_hours';
	protected $_primary = 'id';
	public function SaveorUpdateData($data, $where) {
		if ($where != '') {
			$this->update ( $data, $where );
			return 'update';
		} else {
			$this->insert ( $data );
			$id = $this->getAdapter ()->lastInsertId ( 'main_office_hours' );
			return $id;
		}
	}
	public function getOfficeHoursRule($businessUnitId) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "SELECT * FROM main_office_hours mo inner join main_attendance_configuration ac on ac.id = mo.configuration_id  where ac.business_unit_id = " . $businessUnitId . " and ac.configuration_type_id=1";
		$result = $db->query ( $query )->fetch ();
		return $result;
	}
	public function getConfigurationData($id) {
		$select = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_office_hours' 
		) )->where ( 'l.configuration_id = ' . $id . '' );
		
		return $this->fetchAll ( $select )->toArray ();
	}
	public function checkConfigurationExists($data) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "SELECT mo.id as id FROM main_attendance_configuration as ac inner join  main_office_hours as mo on ac.id = mo.configuration_id where ac.configuration_type_id = " . $data ['configuration_type_id'] . " and ac.business_unit_id= " . $data ['business_unit_id'];
		$result = $db->query ( $query )->fetch ();
		return $result ['id'];
	}
	public function runCustomValidation($data, &$messageArray) {
		$startTime = new DateTime ( $data ['start_time'] );
		$endTime = (new DateTime ( $data ['end_time'] ));
		$entryTime = new DateTime ( $data ['entry_time'] );
		$exitTime = new DateTime ( $data ['exit_time'] );
		if ($endTime < $startTime) {
			$endTime->modify ( '+1 day' );
		}
		
		if (($entryTime < $startTime) && ($data ['entry_time'] != null)) {
			$entryTime->modify ( '+1 day' );
		}
		if (($exitTime < $startTime) && ($data ['exit_time'] != null)) {
			$exitTime->modify ( '+1 day' );
		}
		
		if (($data ['entry_time'] != null)) {
			if ($entryTime > $endTime || $entryTime < $startTime) {
				$messageArray ['entry_time'] = "Entry threshold time should be within start time and end time.";
			}
		}
		if (($data ['exit_time'] != null)) {
			if ($exitTime > $endTime || $exitTime < $startTime) {
				$messageArray ['exit_time'] = "Exit threshold time should be within start time and end time.";
			}
		}
		if (count ( $messageArray ) == 0 && $entryTime > $exitTime && $data ['entry_time'] != null && $data ['exit_time'] != null) {
			$messageArray ['entry_time'] = "Entry threshold should be less than exit threshold.";
		}
	}
	public function validateOfficeHours($dailyruleOfficeHours, &$loggedday, $key, &$faultyDays, &$absentFlag, &$halfDayFlag, $loggedDayDetails) {
		if ($dailyruleOfficeHours ['is_entry_threshold'] == 1) {
			
			if (new DateTime ( $loggedday [$key] ['punch_date'] . " " . $loggedday [$key] ['check_in'] ) > new DateTime ( $loggedday [$key] ['punch_date'] . " " . $dailyruleOfficeHours ['entry_time'] )) {
				if ($dailyruleOfficeHours ['entry_time_action'] == 1) {
					$absentFlag = 1;
					array_push ( $faultyDays, $loggedday [$key] ['punch_date'] );
				} else if ($dailyruleOfficeHours ['entry_time_action'] == 2) {
					$halfDayFlag = 1;
					array_push ( $faultyDays, $loggedday [$key] ['punch_date'] );
				} else if ($dailyruleOfficeHours ['entry_time_action'] == 0) {
					$loggedday [$key] ['check_in'] = $dailyruleOfficeHours ['entry_time'];
                    // skip all entry before that time
                    $dailyActivity = $loggedDayDetails[$key];
                    $dailyActivityUpdated = array();
                    foreach ( $dailyActivity as $key => $activity ) {
                        if($activity['check_in'] != null) {
                            $checkin = new DateTime($activity['check_in']);
                        }
                        if($activity['check_out'] != null) {
                            $checkout = new DateTime($activity['check_out']);
                        }
                        if($dailyruleOfficeHours ['entry_time'] != null) {
                            $threshold = new DateTime($dailyruleOfficeHours ['entry_time']);
                        }

                        if($checkin < $threshold && $checkout < $threshold) {
                            // Do nothing
                            continue;
                        } else if($checkin < $threshold && $checkout > $threshold) {
                            $activity['check_in'] = $dailyruleOfficeHours ['entry_time'];
                            array_push ( $dailyActivityUpdated, $activity );
                        } else {
                            array_push ( $dailyActivityUpdated, $activity );
                        }
                    }
				}
			}
		}
		
		if ($dailyruleOfficeHours ['is_exit_threshold'] == 1) {
			
			if (new DateTime ( $loggedday [$key] ['punch_date'] . " " . $loggedday [$key] ['check_out'] ) > new DateTime ( $loggedday [$key] ['punch_date'] . " " . $dailyruleOfficeHours ['exit_time'] )) {
				if ($dailyruleOfficeHours ['exit_time_action'] == 1) {
					$absentFlag = 1;
					array_push ( $faultyDays, $loggedday [$key] ['punch_date'] );
				} else if ($dailyruleOfficeHours ['exit_time_action'] == 2) {
					$halfDayFlag = 1;
					array_push ( $faultyDays, $loggedday [$key] ['punch_date'] );
				} else if ($dailyruleOfficeHours ['exit_time_action'] == 0) {
					$loggedday [$key] ['check_out'] = $dailyruleOfficeHours ['exit_time'];

					// skip all entry after that time
                    $dailyActivity = $loggedDayDetails[$key];
                    $dailyActivityUpdated = array();
                    foreach ( $dailyActivity as $key => $activity ) {
                        if($activity['check_in'] != null) {
                            $checkin = new DateTime($activity['check_in']);
                        }
                        if($activity['check_out'] != null) {
                            $checkout = new DateTime($activity['check_out']);
                        }
                        if($dailyruleOfficeHours ['entry_time'] != null) {
                            $threshold = new DateTime($dailyruleOfficeHours ['entry_time']);
                        }

                        if($checkin > $threshold && $checkout > $threshold) {
                            // Do nothing
                            continue;
                        } else if($checkin < $threshold && $checkout > $threshold) {
                            $activity['check_out'] = $dailyruleOfficeHours ['entry_time'];
                            array_push ( $dailyActivityUpdated, $activity );
                        } else {
                            array_push ( $dailyActivityUpdated, $activity );
                        }
                    }
				}
			}
		}
		
		$loggedday [$key] ['working_hours'] = $this->calculateWorkHours ( new DateTime ( $loggedday [$key] ['punch_date'] . " " . $loggedday [$key] ['check_in'] ), new DateTime ( $loggedday [$key] ['punch_date'] . " " . $loggedday [$key] ['check_out'] ), $dailyActivityUpdated );
	}
	public function calculateWorkHours($startTime, $endTime, $dailyActivityUpdated) {
	    if($dailyActivityUpdated == null) {
            $min = floor(($endTime->getTimestamp() - $startTime->getTimestamp()) / 60);
            return (floor($min / 60) + (($min % 60) / 100));
        } else {
            $empWorkActivityModel = new Default_Model_EmployeeWorkActivity ();
            return $empWorkActivityModel->calculateTotalHours ( $dailyActivityUpdated );
        }
	}
	public function deleteAttendanceConfigurationDetails($id) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "DELETE FROM `main_office_hours` WHERE `configuration_id` = " . $id;
		$db->query ( $query );
	}
}