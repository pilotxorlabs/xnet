<?php
	const DAYS_PER_LEAVE = 20;
	const MAX_WORKING_DAYS = 300;
	const PUBLIC_HOLIDAYS = 10;
	class Default_Model_EmployeeEarnedleaves {
		
		public function getEarnedLeaves($dateOfJoining,$loginUserId,$numberofdays,$maxLimit){
			
			$totalLeaves = $this->getLeavesCountTill2017($dateOfJoining,date('2017-12-31'),$numberofdays,$maxLimit);
			if (date('Y')> 2018){
				$lastYear = date('Y')-1;
				$startdate = new DateTime('2018-01-01');
				$endDate = new DateTime($lastYear.'-12-31');
				$daysSince2018 =  $startdate->diff($endDate, true)->days;
				$lwpCount = 0;
				if ($UserId != NULL){
				$lwpCount = $this->getLWPCount($UserId,$startDate,$endDate);
				}
				$noOfSundays = intval($daysSince2018 / 7) + ($startdate->format('N') + $daysSince2018 % 7 >= 7);
				$totalWorkingDays = $daysSince2018 - $lwpCount - $noOfSundays - PUBLIC_HOLIDAYS;
				$totalLeaves += round(min($totalWorkingDays/DAYS_PER_LEAVE,MAX_WORKING_DAYS/DAYS_PER_LEAVE));
			}
			if($totalLeaves < 0)
			    return 0;
			return $this->roundOfLeaves($totalLeaves);			
		}
			
		public function getLeavesCountTill2017($dateOfJoining,$endDate,$numberofdays,$maxLimit){
			$totaldays = ($numberofdays * $this->getMonthsBetween($dateOfJoining,$endDate)) / 12;
			$date  = new DateTime($dateOfJoining);
			$date->format('d') > 15 ? $totaldays : $totaldays+=  1.25;
			return round(min($totaldays,$maxLimit), 2);
		}
		
		public function getMonthsBetween($startDate,$endDate){
			$ts1 = strtotime($startDate);
			$ts2 = strtotime($endDate);
			$diff =(( date('Y', $ts2) -  date('Y', $ts1))*12) +(( date('m', $ts2) -  date('m', $ts1)));
			return ceil($diff);
		}
		
		public function getLWPCount($UserId,$startDate,$endDate){
			$emp_leave_type_model = new Default_Model_Employeeleavetypes();
			$leaveType = $emp_leave_type_model->getEmployeeLeaveTypeByCode('LWP');
			$leaveTypeId = $leaveType[0]['leavetype_id'];
			
			$leaverequestmodel = new Default_Model_Leaverequest();
			$lwpcount = $leaverequestmodel->getApprovedLeavesByType($loginUserId, $leaveTypeId);
			return $lwpcount[0]['approvedleaves'];
			
		}
		public function roundOfLeaves($leaves)
		{
		    $fraction = $leaves - floor($leaves);
		    if($fraction == 0)
		        return $leaves;
		    if ($fraction <= 1 / 2)
		        return floor($leaves) + 0.5;
		    if ($fraction > 1 / 5)
		        return (ceil($leaves));
		}
	}