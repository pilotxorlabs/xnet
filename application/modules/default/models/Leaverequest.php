<?php
/*********************************************************************************

 *  Copyright (C) 2014 Sapplica
 *

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/
class Default_Model_Leaverequest extends Zend_Db_Table_Abstract {
	protected $_name = 'main_leaverequest';
	public function getAvailableLeaves($loginUserId) {
		$select = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'e' => 'main_employeeleaves'
		), array (
				'remainingleaves' => 'e.emp_leave_limit',
				'leavelimit' => new Zend_Db_Expr ( 'e.emp_leave_limit + e.used_leaves' )
		) )->where ( 'e.user_id=' . $loginUserId . ' AND e.alloted_year = now() AND e.isactive = 1' );
		return $this->fetchAll ( $select )->toArray ();
	}
	public function getsinglePendingLeavesData($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_leaverequest'
		), array (
				'l.*'
		) )->where ( "l.isactive = 1 AND l.id = " . $id );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function getUserLeavesData($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_leaverequest'
		), array (
				'l.*'
		) )->where ( "l.isactive = 1 AND l.user_id = " . $id );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function getUserApprovedOrPendingLeavesData($id) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "SELECT `l`.*,IF(l.leavestatus = 'Approved', 'A', 'P') as status FROM `main_leaverequest` AS `l` WHERE (l.isactive = 1 AND l.user_id = '$id' and l.leavestatus IN(1,2) and l.from_date != '' and l.to_date != '')";
		
		$result = $db->query ( $query )->fetchAll ();
		
		$compquery = "SELECT `c`.*,IF(c.status = 'Pending', 'P', 'P') as status FROM `main_compoffrequest` AS `c` WHERE (c.isactive = 1 AND c.user_id = '$id' AND c.option = 'Leave' and c.status IN(1) and c.leave_date != '')";
		
		$compresult = $db->query ( $compquery )->fetchAll ();
		
		array_push($result,$compresult);
		return $result;
	}
	public function getManagerApprovedOrPendingLeavesData($id) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "SELECT `l`.*,IF(l.leavestatus = 'Approved', 'A', 'P') as status,u.userfullname
				 FROM `main_leaverequest` AS `l` left join main_users u on u.id=l.user_id
				 WHERE (l.isactive = 1 AND l.rep_mang_id = '$id' and l.leavestatus IN(1,2) and l.from_date != '' and l.to_date != '')";

		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}

	public function getReportingManagerId($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'e' => 'main_employees'
		), array (
				'repmanager' => 'e.reporting_manager',
		) )->where ( "e.isactive = 1 AND e.user_id = " . $id );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function SaveorUpdateLeaveRequest($data, $where) {
		if ($where != '') {
			$this->update ( $data, $where );
			return 'update';
		} else {
			$this->insert ( $data );
			$id = $this->getAdapter ()->lastInsertId ( 'main_leaverequest' );
			return $id;
		}
	}

		public function getEmpRole($loginUserId){
			$db = Zend_Db_Table::getDefaultAdapter ();

			$query = "select emprole from main_users where id=$loginUserId;";

			$result = $db->query ( $query )->fetch ();
			return $result[emprole];
		}
	public function SaveorUpdateNotification($data, $where) {
		if ($where != '') {
			$this->_name = 'main_notification';
			$this->update ( $data, $where );
			$this->_name = 'main_leaverequest';
			return 'update';
		} else {
			$this->_name = 'main_notification';
			$this->insert ( $data );
			$id = $this->getAdapter ()->lastInsertId ( 'main_notification' );
			$this->_name = 'main_leaverequest';
			return $id;
		}
	}
	public function getLeaveStatusHistory($sort, $by, $pageNo, $perPage, $searchQuery, $queryflag = '', $loggedinuser, $managerstring = '', $leavetypeid) {
		$empRole = $this->getEmployeeRole ( $loggedinuser );
		$request = Zend_Controller_Front::getInstance ();
		$params = $request->getRequest ()->getParams ();
		$auth = Zend_Auth::getInstance ();
		$roleId = $this->getEmpRole($loggedinuser);
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		if ($loggedinuser == '')
			$loggedinuser = $loginUserId;

			/* Removing isactive checking from configuration table */
		if ($managerstring != '') {

			$where = "l.isactive = 1 ";
		} else {
			if($roleId ==11 && $params['controller'] == 'employeerequests'){
				$where = "l.isactive = 1 AND l.user_id in (select distinct user_id from main_employees where reporting_manager = " . $loggedinuser . ")";
			}
			else if($empRole == 1 && $params['controller'] == 'employeerequests')
			{
				$where = "l.isactive = 1 AND u.isactive = 1 ";
			}
			else if(($empRole == 12 || $empRole == 1) && $params['controller'] == 'empleavesummary')
			{
				$where = "l.isactive = 1 AND u.isactive = 1 ";
			}
			else if($empRole!=1 &&$empRole!=11 && $params['controller'] == 'employeerequests' ){
			$where = "l.isactive = 1 AND l.user_id in (select distinct user_id from main_employees where reporting_manager = " . $loggedinuser . ")";
		}
		else
			$where = "l.isactive = 1 AND l.user_id = $loggedinuser";
		}

		if ($leavetypeid != '') {
			$leavetypeid = preg_replace("/[^0-9.]/", "", $leavetypeid);
			$where .= ' AND l.leavetypeid=' . $leavetypeid;
		}
		if ($queryflag != '') {
			if ($queryflag == 'pending') {
				$where .= " AND l.leavestatus = 1 ";
			} else if ($queryflag == 'approved') {
				$where .= " AND l.leavestatus = 2 ";
			} else if ($queryflag == 'cancelled') {
				$where .= " AND l.leavestatus = 4 ";
			} else if ($queryflag == 'rejected') {
				$where .= " AND l.leavestatus = 3 ";
			}
		} else {
			$where .= " AND l.leavestatus = 2 ";
		}

		if ($searchQuery)
			$where .= " AND " . $searchQuery;
		$db = Zend_Db_Table::getDefaultAdapter ();
		if($params['controller'] == 'employeerequests')
		{
			$leaveStatusData = $this->select ()->setIntegrityCheck ( false )->from ( array (
					'l' => 'main_leaverequest'
			), array (
					'l.*',
					'from_date' => 'DATE_FORMAT(l.from_date,"' . DATEFORMAT_MYSQL . '")',
					'to_date' => 'DATE_FORMAT(l.to_date,"' . DATEFORMAT_MYSQL . '")',
					'applieddate' => 'l.createddate',
					'leaveday' => 'if(l.leaveday = 1,"Full Day","Half Day")',
					new Zend_Db_Expr ( "CASE WHEN l.leavestatus=2 and CURDATE()>=l.from_date THEN 'no' WHEN l.leavestatus=1 THEN 'yes' WHEN l.leavestatus IN (3,4) THEN 'no' ELSE 'yes' END as approved_cancel_flag " )
			) )->joinLeft ( array (
					'et' => 'main_employeeleavetypes'
			), 'et.id=l.leavetypeid', array (
					'leavetype' => 'et.leavetype'
			) )->joinLeft ( array (
					'u' => 'main_users'
			), 'u.id=l.user_id', array (
					'employeename' => 'u.userfullname'
			) )->where ( $where )->order ( "$by $sort" )->limitPage ( $pageNo, $perPage );

		}
		else
		{
		$leaveStatusData = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_leaverequest'
		), array (
				'l.*',
				'from_date' => 'DATE_FORMAT(l.from_date,"' . DATEFORMAT_MYSQL . '")',
				'to_date' => 'DATE_FORMAT(l.to_date,"' . DATEFORMAT_MYSQL . '")',
				'applieddate' => 'l.createddate',
				'leaveday' => 'if(l.leaveday = 1,"Full Day","Half Day")',
				new Zend_Db_Expr ( "CASE WHEN l.leavestatus=2 and CURDATE()>=l.from_date THEN 'no' WHEN l.leavestatus=1 THEN 'yes' WHEN l.leavestatus IN (3,4) THEN 'no' ELSE 'yes' END as approved_cancel_flag " )
		) )->joinLeft ( array (
				'et' => 'main_employeeleavetypes'
		), 'et.id=l.leavetypeid', array (
				'leavetype' => 'et.leavetype'
		) )->joinLeft ( array (
				'u' => 'main_users'
		), 'u.id=l.rep_mang_id', array (
				'reportingmanagername' => 'u.userfullname',
		) )->joinLeft ( array (
				'mu' => 'main_users'
		), 'mu.id=l.user_id', array (
				'employeename' => 'mu.userfullname',
		) )->joinLeft ( array (
				'mes' => 'main_employees_summary'
		), 'mes.user_id=l.user_id', array (
				'departmentname' => 'mes.department_name'
		) )->where ( $where )->order ( "$by $sort" )->limitPage ( $pageNo, $perPage );
		}

		$emp_data_org = substr ( $leaveStatusData, 0,strrpos($leaveStatusData,"DESC")-1 );
		$data = $db->query($leaveStatusData)->fetchAll();
		return $leaveStatusData;

	}
	public function getEmployeeLeaveRequest($sort, $by, $pageNo, $perPage, $searchQuery, $loginUserId) {
		$empRole = $this->getEmployeeRole ( $loginUserId );
		if ($empRole == '1') {
			$where = "l.isactive = 1 AND l.leavestatus IN(1,2) AND u.isactive=1";
		} else {
			$where = "l.isactive = 1 AND l.leavestatus IN(1,2) AND u.isactive=1 AND l.user_id in (select distinct user_id from main_employees where reporting_manager = " . $loginUserId . " )";
		}

		if ($searchQuery)
			$where .= " AND " . $searchQuery;
		$db = Zend_Db_Table::getDefaultAdapter ();

		$employeeleaveData = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_leaverequest'
		), array (
				'l.*',
				'from_date' => 'DATE_FORMAT(l.from_date,"' . DATEFORMAT_MYSQL . '")',
				'to_date' => 'DATE_FORMAT(l.to_date,"' . DATEFORMAT_MYSQL . '")',
				'applieddate' => 'l.createddate',
				'leaveday' => 'if(l.leaveday = 1,"Full Day","Half Day")'
		) )->joinLeft ( array (
				'et' => 'main_employeeleavetypes'
		), 'et.id=l.leavetypeid', array (
				'leavetype' => 'et.leavetype'
		) )->joinLeft ( array (
				'u' => 'main_users'
		), 'u.id=l.user_id', array (
				'userfullname' => 'u.userfullname'
		) )->where ( $where )->order ( "$by $sort" )->limitPage ( $pageNo, $perPage );

		$emp_data_org = substr ( $employeeleaveData, 0,strrpos($employeeleaveData,"order")-1 );
		$data = $db->query($employeeleaveData)->fetchAll();
		return $employeeleaveData;
	}
	public function getEmployeeRole($id) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "select emprole from main_users where id = " . $id . " ";

		$result = $db->query ( $query )->fetchAll ();
		return $result [0] [emprole];
	}
	public function getUserName($id) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "select userfullname from main_users where id = " . $id . " ";

		$result = $db->query ( $query )->fetchAll ();
		return $result [0] [userfullname];
	}
	public function updateemployeeleaves($appliedleavescount, $employeeid, $leavetypeid) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$db->query ( "update main_employeeleaves  set used_leaves = used_leaves+" . $appliedleavescount . " , emp_leave_limit = emp_leave_limit - " . $appliedleavescount . " where user_id = " . $employeeid . " AND alloted_year = year(now()) AND isactive = 1 AND leavetypeid = $leavetypeid" );
	}
	public function updatecancelledemployeeleaves($appliedleavescount, $employeeid, $leavetypeid) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$db->query ( "update main_employeeleaves  set used_leaves = used_leaves-" . $appliedleavescount . " , emp_leave_limit = emp_leave_limit + " . $appliedleavescount . " where user_id = " . $employeeid . " AND alloted_year = year(now()) AND isactive = 1 AND leavetypeid = $leavetypeid" );
	}
	public function getUserID($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_leaverequest'
		), array (
				'l.user_id'
		) )->where ( "l.isactive = 1 AND l.id = " . $id );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function getLeaveID($userId,$leaveTypeId,$leavedate) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_leaverequest'
		), array (
				'l.id'
		) )->where ( "l.isactive = 1 AND l.user_id = ".$userId." AND l.leavetypeid = ".$leaveTypeId." AND l.from_date = '".$leavedate."'" );
	
		return $this->fetchAll ( $result )->toArray ();
	}
	public function getLeaveRequestDetails($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_leaverequest'
		), array (
				'l.*'
		) )->where ( "l.isactive = 1 AND l.id = " . $id );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function checkdateexists($from_date, $to_date, $loginUserId) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "select count(l.id) as dateexist from main_leaverequest l where l.user_id=" . $loginUserId . " and l.leavestatus IN(1,2) and l.isactive = 1
        and (l.from_date between '" . $from_date . "' and '" . $to_date . "' OR l.to_date between '" . $from_date . "' and '" . $to_date . "' )";

		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}

	/*
	 * This function is common to manager employee leaves, employee leaves , approved,cancel,pending and rejected leaves
	 * Here differentiation is done based on objname.
	 */
	public function getGrid($sort, $by, $perPage, $pageNo, $searchData, $call, $dashboardcall, $objName, $queryflag, $unitId = '', $statusidstring = '', $leavetypeid = '') {
		$auth = Zend_Auth::getInstance ();
		$request = Zend_Controller_Front::getInstance ();
		$params = $request->getRequest ()->getParams ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		$searchQuery = '';
		$searchArray = array ();
		$data = array ();
		if($objName == 'employeerequests')
		{
		       if($searchData != '' && $searchData!='undefined')
				{
					$searchValues = json_decode($searchData);
					foreach($searchValues as $key => $val)
					{
						if($key == 'applieddate')
						 $searchQuery .= " l.createddate like '%".  sapp_Global::change_date($val,'database')."%' AND ";
						//modifications in queries
						else if($key == 'from_date')
						{
							$searchQuery .= " ".$key." >= '".  sapp_Global::change_date($val,'database')."'AND";
						}
						else if ( $key == 'to_date')
						{$searchQuery .= " ".$key." <= '".  sapp_Global::change_date($val,'database')."'AND";}

						//end of modifications
						else
						  $searchQuery .= " ".$key." like '%".$val."%' AND ";
						$searchArray[$key] = $val;
					}
					$searchQuery = rtrim($searchQuery," AND");
				}

			$tableFields = array (
					'action' => 'Action',
					'userfullname' => 'Employee',
					'leavetype' => 'Leave Type',
					'from_date' => 'From',
					'to_date' => 'To',
					'appliedleavescount' => 'Days',
					'leavestatus' => 'Leave Status',
					'applieddate' => 'Applied On',
					'modifieddate' => 'Processed On'
			);

			$leave_arr = array (
					'' => 'All',
					1 => 'Full Day',
					2 => 'Half Day'
			);

			$tablecontent = $this->getEmployeeLeaveRequest ( $sort, $by, $pageNo, $perPage, $searchQuery, $loginUserId );

			$dataTmp = array (
					'sort' => $sort,
					'by' => $by,
					'pageNo' => $pageNo,
					'perPage' => $perPage,
					'tablecontent' => $tablecontent,
					'objectname' => $objName,
					'extra' => array (),
					'tableheader' => $tableFields,
					'jsGridFnName' => 'getAjaxgridData',
					'jsFillFnName' => '',
					'searchArray' => $searchArray,
					'add' => 'add',
					'call' => $call,
					'dashboardcall' => $dashboardcall,
					'search_filters' => array (
							'from_date' => array (
									'type' => 'datepicker'
							),
							'to_date' => array (
									'type' => 'datepicker'
							),
							'applieddate' => array (
									'type' => 'datepicker'
							),
							'modifieddate' => array (
									'type' => 'datepicker'
							),
							'leaveday' => array (
									'type' => 'select',
									'filter_data' => $leave_arr
							)
					)
			);
		} else if ($objName == 'empleavesummary') {
			$managerstring = "true";

			if ($searchData != '' && $searchData != 'undefined') {
				$searchValues = json_decode ( $searchData );
				foreach ( $searchValues as $key => $val ) {
					// if($key !='leavestatus')
					// {
					if ($val != 'undefined') {

						if ($key == 'reportingmanagername')
							$searchQuery .= " u.userfullname like '%" . $val . "%' AND ";
						else if ($key == 'employeename') {
							if (strpos ( $val, '#$$#' ) !== false) {
								$names = explode ( '#$$#', $val );
								$searchQuery .= " mu.userfullname in (";
								foreach ( $names as $filterName ) {
									$searchQuery .= "'" . $filterName . "', ";
								}
								$searchQuery = rtrim ( $searchQuery, ", " );
								$searchQuery .= ") AND";
							} else {
								$searchQuery .= " mu.userfullname like '%" . $val . "%' AND ";
							}
						} else if ($key == 'applieddate') {
							$searchQuery .= " l.createddate  like '%" . sapp_Global::change_date ( $val, 'database' ) . "%' AND ";
						} else if ($key == 'leavestatus') {
							if (strpos ( $val, '#$$#' ) !== false) {
								$names = explode ( '#$$#', $val );
								$searchQuery .= " l.leavestatus in (";
								foreach ( $names as $filterName ) {
									$searchQuery .= "'" . $filterName . "', ";
								}
								$searchQuery = rtrim ( $searchQuery, ", " );
								$searchQuery .= ") AND";
							} else {
								$searchQuery .= " l.leavestatus like '%" . $val . "%' AND ";
							}
						} else if ($key == 'departmentname') {
							if (strpos ( $val, '#$$#' ) !== false) {
								$names = explode ( '#$$#', $val );
								$searchQuery .= " mes.department_name in (";
								foreach ( $names as $filterName ) {
									$searchQuery .= "'" . $filterName . "', ";
								}
								$searchQuery = rtrim ( $searchQuery, ", " );
								$searchQuery .= ") AND";
							} else {
								$searchQuery .= " mes.department_name like '%" . $val . "%' AND ";
							}
						} else if ($key == 'leavetype') {
							if (strpos ( $val, '#$$#' ) !== false) {
								$names = explode ( '#$$#', $val );
								$searchQuery .= " leavetype in (";
								foreach ( $names as $filterName ) {
									$searchQuery .= "'" . $filterName . "', ";
								}
								$searchQuery = rtrim ( $searchQuery, ", " );
								$searchQuery .= ") AND";
							} else {
								$searchQuery .= " leavetype like '%" . $val . "%' AND ";
							}
						} //modifications in queries-paritosh
						else if($key == 'from_date')
						{
							$searchQuery .= " ".$key." >= '".  sapp_Global::change_date($val,'database')."'AND";
						}
						else if ( $key == 'to_date')
						{$searchQuery .= " ".$key." <= '".  sapp_Global::change_date($val,'database')."'AND";}

							// }
						$searchArray [$key] = $val;
					}
				}
				$searchQuery = rtrim ( $searchQuery, " AND" );
			}

			$statusid = '';
			if ($queryflag != '') {
				$statusid = $queryflag;
				if ($statusid == 1)
					$queryflag = 'pending';
				else if ($statusid == 2)
					$queryflag = 'approved';
				else if ($statusid == 3)
					$queryflag = 'rejected';
				else if ($statusid == 4)
					$queryflag = 'cancel';
			} else {
				$queryflag = 'empleavesummary';
			}

			// $tableFields = array('action'=>'Action','employeename' => 'Leave Applied By','leavetype' => 'Leave Type','leaveday' => 'Leave Duration','from_date' => 'From Date','to_date' => 'To Date','reason' => 'Reason','approver_comments' => 'Comments','reportingmanagername'=>'Reporting Manager','appliedleavescount' => 'Leave Count','applieddate' => 'Applied On');
			$tableFields = array (
					'action' => 'Action',
					'employeename' => 'Employee',
					'leavetype' => 'Leave Type',
					'from_date' => 'From Date',
					'to_date' => 'To Date',
					'appliedleavescount' => 'Leave Count',
					'applieddate' => 'Applied On',
					'leavestatus' => 'Status',
					'departmentname' => 'Department Name'
			);

			$leave_arr = array (
					'' => 'All',
					1 => 'Full Day',
					2 => 'Half Day'
			);

			$search_filters = array (
					'from_date' => array (
							'type' => 'datepicker'
					),
					'to_date' => array (
							'type' => 'datepicker'
					),
					'applieddate' => array (
							'type' => 'datepicker'
					),
					'leaveday' => array (
							'type' => 'select',
							'filter_data' => $leave_arr
					)
			);

			/*
			 * This is for dashboard call.
			 * Here one additional column Status is build by passing it to table fields
			 */
			if ($dashboardcall == 'Yes') {
				$tableFields ['leavestatus'] = "Status";
				$search_filters ['leavestatus'] = array (
						'type' => 'select',
						'filter_data' => array (
								'pending' => 'Pending',
								'approved' => 'Approved',
								'rejected' => 'Rejected',
								'cancel' => 'Cancelled'
						)
				);
				if (isset ( $searchArray ['leavestatus'] )) {
					$queryflag = $searchArray ['leavestatus'];
					if ($queryflag == '') {
						$queryflag = 'pending';
					}
				}
			}

			$tablecontent = $this->getLeaveStatusHistory ( $sort, $by, $pageNo, $perPage, $searchQuery, $queryflag, $loginUserId, $managerstring, $leavetypeid );

			if (isset ( $queryflag ) && $queryflag != '')
				$formgrid = 'true';
			else
				$formgrid = '';

			$dataTmp = array (
					'sort' => $sort,
					'by' => $by,
					'pageNo' => $pageNo,
					'perPage' => $perPage,
					'tablecontent' => $tablecontent,
					'objectname' => $objName,
					'extra' => array (),
					'tableheader' => $tableFields,
					'jsGridFnName' => 'getAjaxgridData',
					'jsFillFnName' => '',
					'searchArray' => $searchArray,
					'add' => 'add',
					'formgrid' => $formgrid,
					'unitId' => sapp_Global::_encrypt ( $statusid ),
					'call' => $call,
					'dashboardcall' => $dashboardcall,
					'search_filters' => $search_filters
			);
		} else {
			if ($searchData != '' && $searchData != 'undefined') {
				$searchValues = json_decode ( $searchData );
				foreach ( $searchValues as $key => $val ) {
					if ($key == 'reportingmanagername')
						$searchQuery .= " u.userfullname like '%" . $val . "%' AND ";
					else if ($key == 'applieddate') {

						$searchQuery .= " l.createddate  like '%" . sapp_Global::change_date ( $val, 'database' ) . "%' AND ";
					}
					//modification in queries

					else if($key == 'from_date')
					{
						$searchQuery .= " ".$key." >= '".  sapp_Global::change_date($val,'database')."'AND";
					}
					else if ( $key == 'to_date')
					{$searchQuery .= " ".$key." <= '".  sapp_Global::change_date($val,'database')."'AND";}

					//till here
					else
						$searchQuery .= " " . $key . " like '%" . $val . "%' AND ";
					$searchArray [$key] = $val;
				}
				$searchQuery = rtrim ( $searchQuery, " AND" );
			}

			/*
			 * $tableFields = array('leavetype' => 'Leave Type','leaveday' => 'Leave Duration',
			 * 'from_date' => 'From Date','to_date' => 'To Date','reason' => 'Reason','approver_comments' => 'Comments',
			 * "reportingmanagername"=>"Reporting Manager",'appliedleavescount' => 'Leave Count',
			 * 'applieddate' => 'Applied On','action'=>'Action',);
			 */
			if ($objName == 'rejectedleaves') {
				$tableFields = array (
						'action' => 'Action',
						'employeename' => 'Employee',
						'leavetype' => 'Leave Type',
						'reason' => 'Reason',
						'from_date' => 'From Date',
						'to_date' => 'To Date',
						'appliedleavescount' => 'Days',
						'applieddate' => 'Applied On',
						'modifieddate' => 'Rejected On'
				);
			} else if ($objName == 'approvedleaves') {
				$tableFields = array (
						'action' => 'Action',
						'employeename' => 'Employee',
						'leavetype' => 'Leave Type',
						'reason' => 'Reason',
						'from_date' => 'From Date',
						'to_date' => 'To Date',
						'appliedleavescount' => 'Days',
						'applieddate' => 'Applied On',
						'modifieddate' => 'Approved On'
				);
			}
			else if ($objName == 'cancelledleaves') {
					$tableFields = array (
							'action' => 'Action',
							'employeename' => 'Employee',
							'leavetype' => 'Leave Type',
							'reason' => 'Reason',
							'from_date' => 'From Date',
							'to_date' => 'To Date',
							'appliedleavescount' => 'Days',
							'applieddate' => 'Applied On',
							'modifieddate' => 'Cancelled On'
					);
			}
			else if ($objName == 'pendingleaves' && $queryflag != 'pending') {
						$tableFields = array (
							'action' => 'Action',
							'leavetype' => 'Leave Type',
							'reason' => 'Reason',
							'leavestatus' => 'Leave Status',
							'from_date' => 'From Date',
							'to_date' => 'To Date',
							'appliedleavescount' => 'Days',
							'applieddate' => 'Applied On',
							'comments' => 'Comments'
					);
			}
			else if ($objName == 'pendingleaves' && $queryflag == 'pending') {
				$tableFields = array (
						'action' => 'Action',
						'employeename' => 'Employee',
						'leavetype' => 'Leave Type',
						'reason' => 'Reason',
						'from_date' => 'From Date',
						'to_date' => 'To Date',
						'appliedleavescount' => 'Days',
						'applieddate' => 'Applied On'
				);
			}
			else {
				$tableFields = array (
						'action' => 'Action',
						'leavetype' => 'Leave Type',
						'reason' => 'Reason',
						'from_date' => 'From Date',
						'to_date' => 'To Date',
						'appliedleavescount' => 'Days',
						'leavestatus' => 'Leave Status',
						'applieddate' => 'Applied On',
						'modifieddate' => 'Processed On'

				);
			}
			$leave_arr = array (
					'' => 'All',
					1 => 'Full Day',
					2 => 'Half Day'
			);

			$tablecontent = $this->getLeaveStatusHistory ( $sort, $by, $pageNo, $perPage, $searchQuery, $queryflag, $loginUserId, '', $leavetypeid );
			if (isset ( $queryflag ) && $queryflag != '' && $params['controller'] == 'empleavesummary')
				$formgrid = 'true';
				else
					$formgrid = '';
			$dataTmp = array (
					'sort' => $sort,
					'by' => $by,
					'pageNo' => $pageNo,
					'perPage' => $perPage,
					'tablecontent' => $tablecontent,
					'objectname' => $objName,
					'extra' => array (),
					'tableheader' => $tableFields,
					'jsGridFnName' => 'getAjaxgridData',
					'jsFillFnName' => '',
					'formgrid' => $formgrid,
					'searchArray' => $searchArray,
					'add' => 'add',
					'call' => $call,
					'dashboardcall' => $dashboardcall,
					'search_filters' => array (
							'from_date' => array (
									'type' => 'datepicker'
							),
							'to_date' => array (
									'type' => 'datepicker'
							),
							'applieddate' => array (
									'type' => 'datepicker'
							),
							'modifieddate' => array (
									'type' => 'datepicker'
							),
							'leaveday' => array (
									'type' => 'select',
									'filter_data' => $leave_arr
							)
					)
			);
		}

		return $dataTmp;
	}
	public function getUsersAppliedLeaves($userId) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_leaverequest'
		), array (
				'l.from_date',
				'l.to_date'
		) )->where ( "l.isactive = 1 AND l.user_id = " . $userId . " AND l.leavestatus IN(1,2)" );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function getDepartmentName($userId) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'es' => 'main_employees_summary'
		), array (
				'es.department_name'
		) )->where ( "es.isactive = 1 AND es.user_id = " . $userId );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function checkLeaveExists($from_date, $to_date, $userId, $leaveday) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		if ($leaveday == 1) {
			$query = "select count(l.id) as leaveexist,leaveday from main_leaverequest l where l.user_id=" . $userId . " and l.leavestatus IN(1,2) and l.isactive = 1
	        and ((l.from_date between '" . $from_date . "' and '" . $to_date . "' or l.to_date between '" . $from_date . "' and '" . $to_date . "' )
			or ('" . $from_date . "' between l.from_date and l.to_date or '" . $to_date . "' between l.from_date and l.to_date))";
		} else {
			$query = "select count(l.id) as leaveexist,leaveday from main_leaverequest l where l.user_id=" . $userId . " and l.leavestatus IN(1,2) and l.isactive = 1
	        and l.from_date = '" . $from_date . "'";
		}
		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	public function getLeaveTypeId( $userId, $from_date) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_leaverequest'
		), array (
				'l.leavetypeid'
		) )->where ( "l.user_id = '".$userId."' and l.from_date = '".$from_date."'" );

		return $this->fetchAll ( $result )->toArray ();
	}

	public function getLeaveDetails($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_leaverequest_summary'
		), array (
				'l.*'
		) )->where ( "l.isactive = 1 AND l.leave_req_id = " . $id . " " );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function getLeavesCount($userid, $status = '', $leavetypeid) {
		if (! $leavetypeid)
			return 0;
		$db = Zend_Db_Table::getDefaultAdapter ();
		$leavestatus = "";
		if ($status != '')
			$leavestatus = " and l.leavestatus = $status ";

		$query = "select sum(appliedleavescount) cnt from main_leaverequest l
                  where l.isactive = 1 and l.user_id = $userid " . $leavestatus . " and leavetypeid=" . $leavetypeid;
		$result = $db->query ( $query )->fetch ();
		if (! $result ['cnt'])
			return 0;
		return $result ['cnt'];
	}
	public function getmanagerleavecount($userId,$status = ''){
		$db = Zend_Db_Table::getDefaultAdapter ();
		$empRole = $this->getEmployeeRole ( $userId );
		if($empRole ==11){
			$where = "l.isactive = 1 AND l.leavestatus = $status AND u.isactive=1 AND l.user_id in (select distinct user_id from main_employees where reporting_manager = " . $userId . ")";
		}
		else if($empRole == 1)
		{
			$where = "l.leavestatus = $status AND l.isactive = 1 AND u.isactive = 1";
		}
		else if($empRole == 12)
		{
			$where = "l.leavestatus = $status AND l.isactive = 1 AND u.isactive = 1";
		}
		else
			$where = "l.leavestatus = $status AND l.isactive = 1 AND u.isactive = 1 AND l.user_id in (select distinct user_id from main_employees where reporting_manager = " . $userId . ")";

		$leavestatus = "";
		if ($status != '')
			$leavestatus = " and l.leavestatus = $status ";
		$query = "select count(*) from (SELECT `l`.*, CASE WHEN l.leavestatus=2 and CURDATE()>=l.from_date THEN 'no' WHEN
				 l.leavestatus=1 THEN 'yes' WHEN l.leavestatus IN (3,4) THEN 'no' ELSE 'yes' END as approved_cancel_flag ,
				 `et`.`leavetype`, `u`.`userfullname` AS `reportingmanagername`, `mu`.`userfullname` AS `employeename`,
				 `mes`.`department_name` AS `departmentname` FROM `main_leaverequest` AS `l` LEFT JOIN
				 `main_employeeleavetypes` AS `et` ON et.id=l.leavetypeid LEFT JOIN `main_users` AS `u` ON u.id=l.rep_mang_id
				 LEFT JOIN `main_users` AS `mu` ON mu.id=l.user_id LEFT JOIN `main_employees_summary` AS `mes` ON
				 mes.user_id=l.user_id  WHERE ($where) ) as abc;";
		$result = $db->query ( $query )->fetch ();
		if (! $result ['count(*)'])
			return 0;
			return $result ['count(*)'];
	}
	public function getHrApprovedOrPendingLeavesData($id) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "SELECT `l`.*,IF(l.leavestatus = 'Approved', 'A', 'P') as status,u.userfullname
				 FROM `main_leaverequest` AS `l` left join main_users u on u.id=l.user_id
				 WHERE (l.isactive = 1 AND l.hr_id = '$id'  and user_id != '$id' and l.leavestatus IN(1,2))";

		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	public function checkLeavesWithin($from_date, $to_date, $loginUserId) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "select  sum(appliedleavescount) as leaves from main_leaverequest l where l.user_id=" . $loginUserId . " and l.leavestatus IN(2) and l.isactive = 1
        and ((l.from_date >= STR_TO_DATE('" . $from_date . "','%m/%d/%Y') and l.from_date <= STR_TO_DATE('" . $to_date . "','%m/%d/%Y')) OR (l.to_date >= STR_TO_DATE('" . $from_date . "','%m/%d/%Y') and l.to_date <= STR_TO_DATE('" . $to_date . "','%m/%d/%Y')) )";

		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	public function getTotalLeaveByTypes($loginUserId, $leaveTypeId) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "select  l.emp_leave_limit as leaves_limit , l.used_leaves as used from main_employeeleaves l where l.user_id=" . $loginUserId . " and l.isactive = 1 and l.alloted_year = year(now()) and  l.leavetypeid=" . $leaveTypeId;

		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	public function getApprovedLeavesByType($loginUserId, $leaveTypeId) {
		$db = Zend_Db_Table::getDefaultAdapter ();

		$query = "select  sum(l.appliedleavescount) as approvedleaves from main_leaverequest l where l.user_id=" . $loginUserId . " and l.isactive = 1 and l.leavestatus IN(2) and l.leavetypeid=" . $leaveTypeId;

		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	public function getApprovedLeavesBetween($id, $fromDate, $toDate) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select l.from_date, l.to_date, l.leavetypeid as leavetypeid, l.appliedleavescount as leavecount
					from main_leaverequest l where l.user_id=" . $id . " and l.leavestatus IN(2)
					and ((l.from_date between STR_TO_DATE('" . $fromDate . "','" . DATEFORMAT_MYSQL . "') and STR_TO_DATE('" . $toDate . "','" . DATEFORMAT_MYSQL . "')
					or  l.to_date between STR_TO_DATE('" . $fromDate . "','" . DATEFORMAT_MYSQL . "') and STR_TO_DATE('" . $toDate . "','" . DATEFORMAT_MYSQL . "') )
					or ( STR_TO_DATE('" . $fromDate . "','" . DATEFORMAT_MYSQL . "') between l.from_date and l.to_date
					and STR_TO_DATE('" . $toDate . "','" . DATEFORMAT_MYSQL . "') between l.from_date and l.to_date
					))";

		$result = $db->query ( $query )->fetchAll ();
		$leaveData = $this->removeOutOfRangeLeaves ( $result, $fromDate, $toDate );
		return $leaveData;
	}

	public function getApprovedLeavesArrayBetween($id, $fromDate, $toDate) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$rows = $db->query("select from_date, to_date from main_leaverequest where leavestatus='Approved' and user_id = ".$id);
		$result = $rows->fetchAll ();
		$leavesArray = array();
		foreach($result as $row) {
			$leaveFromDate = new DateTime($row['from_date']);
			$leaveToDate = new DateTime($row['to_date']);
			while($leaveFromDate <= $leaveToDate){
				if($leaveFromDate >= $fromDate && $leaveFromDate <= $toDate)
				{
					if(!in_array($leavesArray, $leaveFromDate->format('Y-m-d'))){
						array_push($leavesArray, $leaveFromDate->format('Y-m-d'));
					}
				}
				$leaveFromDate->modify ( '+1 day' );
			}
		}
		return $leavesArray;
	}


	public function removeOutOfRangeLeaves($leavesData, $fromDate, $toDate) {
		$startDate = new DateTime ( $fromDate );
		$endDate = new DateTime ( $toDate );
		$result = array ();
		foreach ( $leavesData as $leaveData ) {
			$appliedLeavecount = 0;
			$leaveStartDate = new DateTime ( $leaveData ['from_date'] );
			$leaveEndDate = new DateTime ( $leaveData ['to_date'] );
			if ($leaveStartDate < $startDate) {

				$appliedLeavecount += ($leaveData ['leavecount'] - $leaveStartDate->diff ( $startDate )->days + $this->getWeekends ( $leaveStartDate, $startDate->modify ( '-1 day' ) ));
				if ($leaveEndDate > $endDate) {

					$appliedLeavecount = $appliedLeavecount - $endDate->diff ( $leaveEndDate )->days + $this->getWeekends ( $endDate->modify ( '+1 day' ), $leaveEndDate );
				}
			} else if ($leaveEndDate > $endDate) {
				$appliedLeavecount += ($leaveData ['leavecount'] - ($endDate->diff ( $leaveEndDate )->days - $this->getWeekends ( $endDate->modify ( '+1 day' ), $leaveEndDate )));
			} else {
				$appliedLeavecount += $leaveData ['leavecount'];
			}

			$leave = array (
					'leavetypeid' => $leaveData ['leavetypeid'],
					'leavecount' => $appliedLeavecount
			);
			array_push ( $result, $leave );
		}
		return $result;
	}
	public function getWeekends($startDate, $endDate) {
		$totalDays = 0;
		$fromDate = $startDate;
		$toDate = $endDate;

		while ( $fromDate <= $toDate ) {
			if ($fromDate->format ( 'w' ) == 0 || $fromDate->format ( 'w' ) == 6) {
				$totalDays ++;
			}
			$fromDate->modify ( '+1 day' );
		}
		return $totalDays;
	}
	public function getSingleApprovedLeaveData($id, $date) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select l.id, l.from_date, l.to_date, l.leavetypeid as leavetypeid, l.appliedleavescount as leavecount
					from main_leaverequest l where l.user_id=" . $id . " and l.leavestatus IN(2)
					and STR_TO_DATE('" . $date . "','%Y-%m-%d') between l.from_date and l.to_date";

		return $db->query ( $query )->fetchAll ();
	}

	public function getSingleApprovedLeaveDataByType($id, $date, $leavetypeid) {
	    $db = Zend_Db_Table::getDefaultAdapter ();
	    $query = "select l.id from main_leaverequest l where l.user_id=" . $id . " and l.leavetypeid = $leavetypeid and l.leavestatus IN(2)
					and STR_TO_DATE('" . $date . "','%Y-%m-%d') between l.from_date and l.to_date";

	    return $db->query ( $query )->fetchAll ();
	}

	public function getUserLeavesDataWithinDate($id, $datebeforecurrentday) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "SELECT count(*) AS count FROM main_leaverequest where user_id='" . $id . "' AND from_date <='" . $datebeforecurrentday . "' AND to_date >='" . $datebeforecurrentday . "' AND (leavestatus = 'Approved' OR leavestatus = 'Pending')";

		$result = $db->query ( $query )->fetch ();
		$leavecount = $result ['count'];
		return $leavecount;
	}
	public function checkAllocationPeriod($leavetypeid, $fromdate, $todate) {
		$leaveTypeModel = new Default_Model_Employeeleavetypes ();
		$employeeleavesModel = new Default_Model_Employeeleaves ();
		$leaveType = $leaveTypeModel->getsingleEmployeeLeavetypeData ( $leavetypeid );
		$allotmentMode = $leaveType [0] ['allotmentmode'];
		if ($leaveType [0] ['cancarryforward'] == 2) {
			switch ($allotmentMode) {
				case 'Monthly' :
					if ($this->isDatebetween ( new DateTime ( 'first day of this month' ), new DateTime ( 'last day of this month' ), $fromdate ) && $this->isDatebetween ( new DateTime ( 'first day of this month' ), new DateTime ( 'last day of this month' ), $todate )) {
						$result ['result'] = 'success';
					} else {
						$result ['result'] = 'error';
						$result ['message'] = $leaveType [0] ['leavetype'] . ' can only be applied for current month only.';
					}
					break;
				case 'Quarterly' :
					if ($this->isDatebetween ( $employeeleavesModel->getFirstDayOfQuarter ( ceil ( date ( 'm' ) / 3 ) ), $employeeleavesModel->getLastDayOfQuarter ( ceil ( date ( 'm' ) / 3 ) ), $fromdate ) && $this->isDatebetween ( $employeeleavesModel->getFirstDayOfQuarter ( ceil ( date ( 'm' ) / 3 ) ), $employeeleavesModel->getLastDayOfQuarter ( ceil ( date ( 'm' ) / 3 ) ), $todate )) {
						$result ['result'] = 'success';
					} else {
						$result ['result'] = 'error';
						$result ['message'] = $leaveType [0] ['leavetype'] . ' can only be applied for current quarter only.';
					}
					break;
				case 'Half-yearly' :
					if ($this->isDatebetween ( $employeeleavesModel->getFirstDayOfHalfYear ( ceil ( date ( 'm' ) / 6 ) ), $employeeleavesModel->getLastDayOfHalfYear ( ceil ( date ( 'm' ) / 6 ) ), $fromdate ) && $this->isDatebetween ( $employeeleavesModel->getFirstDayOfHalfYear ( ceil ( date ( 'm' ) / 6 ) ), $employeeleavesModel->getLastDayOfHalfYear ( ceil ( date ( 'm' ) / 6 ) ), $todate )) {
						$result ['result'] = 'success';
					} else {
						$result ['result'] = 'error';
						$result ['message'] = $leaveType [0] ['leavetype'] . ' can only be applied for current half-year only.';
					}
					break;
				case 'Yearly' :
					if ($this->isDatebetween ( new DateTime ( 'first day of January' ), new DateTime ( 'last day of December' ), $fromdate ) && $this->isDatebetween ( new DateTime ( 'first day of January' ), new DateTime ( 'last day of December' ), $todate )) {
						$result ['result'] = 'success';
					} else {
						$result ['result'] = 'error';
						$result ['message'] = $leaveType [0] ['leavetype'] . ' can only be applied for current year only.';
					}
					break;
			}
		} else {
			$result ['result'] = 'success';
		}

		return $result;
	}
	public function isDatebetween($start, $end, $date) {
		if ($start->format ( 'Y-m-d' ) <= $date->format ( 'Y-m-d' ) && $end->format ( 'Y-m-d' ) >= $date->format ( 'Y-m-d' ))
			return true;
		return false;
	}
	public function getUsedLeavesByTypeWithin($id, $leavetypeid, $fromDate, $toDate) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select sum(l.appliedleavescount) as leavecount from main_leaverequest l where l.user_id=" . $id . " and leavetypeid=" . $leavetypeid . " and l.leavestatus IN(1,2)
					and ((l.from_date between STR_TO_DATE('" . $fromDate->format ( 'd-m-Y' ) . "','%d-%m-%Y') and STR_TO_DATE('" . $toDate->format ( 'd-m-Y' ) . "','%d-%m-%Y')
					or  l.to_date between STR_TO_DATE('" . $fromDate->format ( 'd-m-Y' ) . "','%d-%m-%Y') and STR_TO_DATE('" . $toDate->format ( 'd-m-Y' ) . "','%d-%m-%Y') )
					or ( STR_TO_DATE('" . $fromDate->format ( 'd-m-Y' ) . "','%d-%m-%Y') between l.from_date and l.to_date
					and STR_TO_DATE('" . $toDate->format ( 'd-m-Y' ) . "','%d-%m-%Y') between l.from_date and l.to_date
					))";

		$result = $db->query ( $query )->fetch ();
		return $result ['leavecount'] == null ? 0 : $result ['leavecount'];
	}
	public function getApprovedLeavesByTypeWithin($id, $leavetypeid, $fromDate, $toDate) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select sum(l.appliedleavescount) as leavecount from main_leaverequest l where l.user_id=" . $id . " and leavetypeid=" . $leavetypeid . " and l.leavestatus IN(2)
					and ((l.from_date between STR_TO_DATE('" . $fromDate->format ( 'd-m-Y' ) . "','%d-%m-%Y') and STR_TO_DATE('" . $toDate->format ( 'd-m-Y' ) . "','%d-%m-%Y')
					or  l.to_date between STR_TO_DATE('" . $fromDate->format ( 'd-m-Y' ) . "','%d-%m-%Y') and STR_TO_DATE('" . $toDate->format ( 'd-m-Y' ) . "','%d-%m-%Y') )
					or ( STR_TO_DATE('" . $fromDate->format ( 'd-m-Y' ) . "','%d-%m-%Y') between l.from_date and l.to_date
					and STR_TO_DATE('" . $toDate->format ( 'd-m-Y' ) . "','%d-%m-%Y') between l.from_date and l.to_date
					))";

		$result = $db->query ( $query )->fetch ();
		return $result ['leavecount'] == null ? 0 : $result ['leavecount'];
	}
	public function getUsedLeavesByType($id, $leavetypeid) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select sum(l.appliedleavescount) as leavecount from main_leaverequest l where l.user_id=" . $id . " and leavetypeid=" . $leavetypeid . " and l.leavestatus IN(1,2)";
		$result = $db->query ( $query )->fetch ();
		return $result ['leavecount'] == null ? 0 : $result ['leavecount'];
	}
	public function checkMixedModeLeaves() {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select flagValue from main_configuration where moduleName='Leave Request' and flagName='Allow Mixed Leaves'";
		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	public function checkAppliedLeaveCount($from_date, $loginUserId) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select appliedleavescount from main_leaverequest where user_id=" . $loginUserId . " and leavestatus IN(1,2) and isactive = 1
        and from_date='" . $from_date . "'";
		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	public function getEmployeeSummaryReportData() {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$dataTmp = $_SESSION ['grid_data'];

		$griddata = $dataTmp ['tableheader'];
		$gridRemoveAction = array_shift ( $griddata );
		$emp_data_org = $dataTmp ['tablecontent'];
		$emp_data_org = substr ( $emp_data_org, 0, strrpos ( $emp_data_org, "LIMIT" ) - 1 );
		$emp_arr = $db->query ( $emp_data_org )->fetchAll ();

		return array (
				'emp_arr' => $emp_arr,
				'tableheader' => $griddata
		);
	}
	public function calculatebusinessdays($fromDate, $toDate, $userId) {
		$noOfDays = 0;
		$weekDay = '';
		$employeeDepartmentId = '';
		$employeeGroupId = '';
		$weekend1 = '';
		$weekend2 = '';
		$holidayDatesArr = array ();
		// Calculating the no of days in b/w from date & to date with out taking weekend & holidays....
		$employeesmodel = new Default_Model_Employees ();
		$leavemanagementmodel = new Default_Model_Leavemanagement ();
		$holidaydatesmodel = new Default_Model_Holidaydates ();

		$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $userId );
		if (! empty ( $loggedInEmployeeDetails )) {
			$employeeDepartmentId = $loggedInEmployeeDetails [0] ['department_id'];
			$employeeGroupId = $loggedInEmployeeDetails [0] ['holiday_group'];

			if ($employeeDepartmentId != '' && $employeeDepartmentId != NULL)
				$weekendDetailsArr = $leavemanagementmodel->getWeekendNamesDetails ( $employeeDepartmentId );

			if (! empty ( $weekendDetailsArr )) {
				if ($weekendDetailsArr [0] ['is_skipholidays'] == 1 && isset ( $employeeGroupId ) && $employeeGroupId != '') {
					$holidayDateslistArr = $holidaydatesmodel->getHolidayDatesListForGroup ( $employeeGroupId );
					if (! empty ( $holidayDateslistArr )) {
						for($i = 0; $i < sizeof ( $holidayDateslistArr ); $i ++) {
							$holidayDatesArr [$i] = $holidayDateslistArr [$i] ['holidaydate'];
						}
					}
				}
				$weekend1 = $weekendDetailsArr [0] ['daystartname'];
				$weekend2 = $weekendDetailsArr [0] ['dayendname'];
			}

			$fromdate_obj = new DateTime ( $fromDate );
			$weekDay = $fromdate_obj->format ( 'l' );
			while ( $fromDate <= $toDate ) {
				/* if(($weekDay != 'Saturday'||$weekDay != 'Sunday') && (!empty($holidayDates)) && (!in_array($fromDate,$holidayDates))) */
				if (count ( $holidayDatesArr ) > 0) {
					if ($weekDay != $weekend1 && $weekDay != $weekend2 && (! in_array ( $fromDate, $holidayDatesArr ))) {
						$noOfDays ++;
					}
				} else {
					if ($weekDay != $weekend1 && $weekDay != $weekend2) {
						$noOfDays ++;
					}
				}
				$fromdate_obj->add ( new DateInterval ( 'P1D' ) ); // Increment from date by one day...
				$fromDate = $fromdate_obj->format ( 'Y-m-d' );
				$weekDay = $fromdate_obj->format ( 'l' );
			}
		}

		return $noOfDays;
	}
	public function validateLeaveRequest($leaverequest, $userId, &$msgarray, &$saveableData, $leavetypearg = '') {
		$constantday = '';
		$days = '';
		$errorflag = 'true';
		$leavetypecount = '';
		$leavedayArr = array (
				1,
				2
		);
		$availableleaves = '';
		$rep_mang_id = '';
		$employeeemail = '';
		$reportingManageremail = '';
		$week_startday = '';
		$week_endday = '';
		$ishalf_day = '';
		$userfullname = '';
		$reportingmanagerName = '';
		$businessunitid = '';
		$dateofjoining = '';
		$hremailgroup = '';
		$employeeDepartmentId = '';
		$reportingmanagerId = '';
		$employeeGroupId = '';
		$holidayDatesArr = array ();
		$leavetypeArr = array ();
		$leaverequestmodel = new Default_Model_Leaverequest ();
		$leavetypeconstraintmodel = new Default_Model_Leavetypeconstraints ();
		$employeeleavetypesmodel = new Default_Model_Employeeleavetypes ();
		$leavemanagementmodel = new Default_Model_Leavemanagement ();
		$employeeleavemodel = new Default_Model_Employeeleaves ();
		$usersmodel = new Default_Model_Users ();
		$employeesmodel = new Default_Model_Employees ();
		$weekdaysmodel = new Default_Model_Weekdays ();
		$holidaydatesmodel = new Default_Model_Holidaydates ();
		if ($userId != '' && $userId != NULL) {
			$loggedinEmpId = $usersmodel->getUserDetailsByID ( $userId );
			$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $userId );

			if (! empty ( $loggedInEmployeeDetails )) {

				$reportingmanagerId = $loggedInEmployeeDetails [0] ['reporting_manager'];
				$employeeDepartmentId = $loggedInEmployeeDetails [0] ['department_id'];
				$employeeEmploymentStatusId = $loggedInEmployeeDetails [0] ['emp_status_id'];
				$businessunitid = $loggedInEmployeeDetails [0] ['businessunit_id'];
				$dateofjoining = $loggedInEmployeeDetails [0] ['date_of_joining'];

				if ($reportingmanagerId != '' && $reportingmanagerId != NULL)
					$reportingManagerDetails = $usersmodel->getUserDetailsByID ( $reportingmanagerId );

				if ($employeeDepartmentId != '' && $employeeDepartmentId != NULL)
					$weekendDatailsArr = $leavemanagementmodel->getWeekendDetails ( $employeeDepartmentId );
				$employeeemail = $loggedinEmpId [0] ['emailaddress'];
				$userfullname = $loggedinEmpId [0] ['userfullname'];

				if (! empty ( $reportingManagerDetails )) {
					$reportingManageremail = $reportingManagerDetails [0] ['emailaddress'];
					$reportingmanagerName = $reportingManagerDetails [0] ['userfullname'];
					$rep_mang_id = $reportingManagerDetails [0] ['id'];
					$rMngr = 'Yes';
				} else {
					$msgarray ['rep_mang_id'] = 'Reporting manager is not assigned yet. Please contact your HR.';
					$errorflag = 'false';
				}

				if (! empty ( $weekendDatailsArr )) {
					$week_startday = $weekendDatailsArr [0] ['weekendstartday'];
					$week_endday = $weekendDatailsArr [0] ['weekendday'];
					$ishalf_day = $weekendDatailsArr [0] ['is_halfday'];
					$isskip_holidays = $weekendDatailsArr [0] ['is_skipholidays'];
				} else {
					$msgarray ['from_date'] = 'Leave management options are not configured yet.';
					$msgarray ['to_date'] = 'Leave management options are not configured yet.';
					$errorflag = 'false';
				}
			} else {
				$errorflag = 'false';
				$msgarray ['rep_mang_id'] = 'Reporting manager is not assigned yet. Please contact your HR.';
				$msgarray ['from_date'] = 'Leave management options are not configured yet.';
				$msgarray ['to_date'] = 'Leave management options are not configured yet.';
			}
		}

		/*
		 * START- Validating if employee has been allotted leaves
		 * Validating if employee has not been assigned any leaves
		 */
		$getavailbaleleaves = $leaverequestmodel->getAvailableLeaves ( $userId );

		if (! empty ( $getavailbaleleaves )) {
			$availableleaves = $getavailbaleleaves [0] ['remainingleaves'];
		} else {
			$errorflag = 'false';
			$msgarray ['no_of_days'] = 'You have not been allotted leaves for this financial year. Please contact your HR';
		}
		/*
		 * END- Validating if employee has been allotted leaves
		 */

		$id = $leaverequest ['id'];
		$reason = $leaverequest ['reason']; // reason
		$leavetypeparam = $leaverequest ['leavetypeid'];
		if($leavetypearg){
		    $leavetypeArr = $leavetypearg;
		    $leavetypeid = $leavetypearg ['0']['id'];
		}else if (isset ( $leavetypeparam ) && $leavetypeparam != '') {
			$leavetypeArr = explode ( "!@#", $leaverequest ['leavetypeid'] );
			$leavetypeid = $leavetypeArr [0];
			$leavetypeArr = $employeeleavetypesmodel->getLeavetypeDataByID ( $leavetypeid );
			if($leavetypeArr[0]['leavecode'] == COMPOFF){
				$errorflag = 'false';
				$msgarray ['leavetypeid'] = 'Invalid leave type selected';
			}
		}

		/*
		 * START- Leave Type Validation
		 * Server side validation for leavetype count based on user selection.
		 * This is to validate or negate if user manipulates the data in the browser or firebug.
		 */
		if (! empty ( $leavetypeArr )) {
			$leaverequestmodel = new Default_Model_Leaverequest ();
			$employeeleaves = $employeeleavemodel->getsingleEmployeeleaveDataByType ( $userId, $leavetypeArr [0] ['id'] );
			if($leavetypearg){
			    $leavetypecount = $leavetypearg [0] ['max_limit'];
			}else{
			    $leavetypecount = $employeeleaves [0] ['emp_leave_limit'];
			}
			// $leavetypecount = $leavetypeArr[0]['numberofdays'];
			$leavetypetext = $leavetypeArr [0] ['leavetype'];
		} else {
			if (isset ( $leavetypeparam ) && $leavetypeparam != '') {
				$msgarray ['leavetypeid'] = 'Wrong inputs given.';
				$errorflag = 'false';
			} else if ($leavetypeparam == '') {
				$msgarray ['leavetypeid'] = 'Please select leave type.';
				$errorflag = 'false';
			} else {
				$msgarray ['leavetypeid'] = 'Leave types are not configured yet.';
				$errorflag = 'false';
			}
		}

		/*
		 * END- Leave Type Validation
		 */

		$leaveday = $leaverequest ['leaveday'];
		/*
		 * START- Leave Day Validation
		 * Server side validation for halfday and full day based on user selection.
		 * This is to validate or negate if user manipulates the data in the browser or firebug.
		 */
		if (! in_array ( $leaveday, $leavedayArr )) {
			$msgarray ['leaveday'] = 'Wrong inputs given.';
			$errorflag = 'false';
		}

		/*
		 * END- Leave Day Validation
		 */

		$from_date = $leaverequest ['from_date'];
		$from_date = sapp_Global::change_date ( $from_date, 'database' );

		$to_date = $leaverequest ['to_date'];
		$to_date = sapp_Global::change_date ( $to_date, 'database' );

		$appliedleavesdaycount = $leaverequest ['appliedleavesdaycount']; // no of leaves applied

		/*
		 * START- Day calculation and validations.
		 * I. Calculation of days based on start date and end data.
		 * II. Also checking whether Applied no of days is less than leavetype configuration.
		 * III. Also If leaveday is selected as full day then making todata as manadatory and
		 * if leave day is selected as half day then no mandatory validation for todate.
		 */

		if ($from_date != '' && $to_date != '' && $leavetypecount != '') {
			
			if($leavetypeArr[0]['leavecode'] != 'WFH')
			{
				$days = $this->calculatebusinessdays ( $from_date, $to_date, $userId );
			}
			else 
			{
				$days = ((strtotime($to_date) - strtotime($from_date))/(60 * 60 * 24)) + 1;
			}
			if ((is_numeric ( $days ) && $days == 0 && $leavetypeArr[0]['leavecode'] != 'WFH')) {
			    $msgarray ['to_date'] = 'You cannot opt leave on Weekend/Holidays.';
			    $errorflag = 'false';
			} else {
				if (! is_numeric ( $days )) {
					$msgarray ['to_date'] = 'To date should be greater than from date.';
					$errorflag = 'false';
				}
			}
		} else {
			if ($leaveday == 1) {
				if ($to_date == '' && ! empty ( $weekendDatailsArr )) {
					$msgarray ['to_date'] = "Please select date.";
					$errorflag = 'false';
				}
			}
		}
		$employeeGroupId = $loggedInEmployeeDetails [0] ['holiday_group'];
		$holidayDateslistArr = $holidaydatesmodel->getHolidayDatesListForGroup ( $employeeGroupId );
		if (! empty ( $holidayDateslistArr )) {
			for($i = 0; $i < sizeof ( $holidayDateslistArr ); $i ++) {
				$holidayDatesArr [$i] = $holidayDateslistArr [$i] ['holidaydate'];
			}
		}
		if ((date("w", strtotime($from_date)) == $week_startday || date("w", strtotime($from_date)) == $week_endday  || in_array ( $from_date, $holidayDatesArr )) && ($to_date == '' || $to_date == null) && $leavetypeArr[0]['leavecode'] != 'WFH') {
				$msgarray ['from_date'] = 'You cannot opt leave on Weekend/Holidays.';
			    $errorflag = 'false';
			}
		/*
		 * END- Day calculation and validations.
		 */

		/*
		 * START- Validating Half day requests based on Leave management options
		 * Validation for half day leaves.
		 * If halfday leave is configure in leave management options then only half day leave can be applied.
		 */
		if ($ishalf_day == 2) {
			if ($leaveday == 2) {
				$errorflag = 'false';
				$msgarray ['leaveday'] = 'Half day leave cannot be applied.';
			}
		}

		/*
		 * END- Validating Half day requests based on Leave management options
		 */

		/*
		 * START- Validating if leave request has been previoulsy applied
		 * I.Validating from and to dates to check whether previously
		 * any leave has been raised with the same dates.
		 * II.If full day leave is applied then fromdate and todate are passed as parameter to query.
		 * III.If half day leave is applied then fromdate and fromdate are passed as a parameter to query.
		 */

		$userAppliedLeaves = $leaverequestmodel->getUsersAppliedLeaves ( $userId );
		$compoffModel = new Default_Model_Compoffrequest();
		$mixLeaves = $leaverequestmodel->checkMixedModeLeaves ();
		$clubTemp =new Default_Model_Employeeleavetypes();
		$appliedcount=0;
		if (! empty ( $userAppliedLeaves )) {
			foreach ( $userAppliedLeaves as $leave ) {
				$leavesDateExists = $leaverequestmodel->checkLeaveExists ( $from_date, $to_date, $userId, $leaveday );
				$compoffExists = $compoffModel->checkCompoffExists($from_date, $to_date, $userId);
				$leavecount = $leavesDateExists [0] ['leaveexist'];
				if ($leavecount == 0)
					$leavecount = $compoffExists [0] ['compoffexist'];
				$leavedayPre = $leavesDateExists [0] ['leaveday'];
				if (($leavecount != null) && ($leavecount != 0))
				{
					$leavetypeidpretemp = $leaverequestmodel->getLeaveTypeId($userId, $from_date);
					$leavetypeidpre = $leavetypeidpretemp[0][leavetypeid];
					if ($leavetypeidpre != null)
					{
						$clubVarTemp = $clubTemp->checkClubbableLeaves($leavetypeidpre);
						$clubValPre = $clubVarTemp[0]['isclubbable'];
					}
				}

				if ($leavecount >0 && $mixLeaves [0] ['flagValue'] == 1) {

				if (($leaveday != 1 && $leavecount > 1) || $leaveday == 1 || ($leavedayPre ==1 && $leavecount >=1)) {
						$errorflag = 'false';
						$msgarray ['from_date'] = ' Leave has already been applied for the above dates.';
					}
				// check for clubbable leaves

				else if ($leaveday==2 && $clubValPre==2 && $leavecount>=1)
				{
					$errorflag = 'false';
					$msgarray ['leavetypeid'] = ' Leaves are not clubbable.';
					break;
				}

				else if ($leaveday==2 && $clubValPre ==1 && $leavecount >=1)
				{

					$clubValPre = $clubVarTemp[0]['isclubbable'];
					$clubVarTemp = $clubTemp->checkClubbableLeaves($leavetypeid);
					$clubVal = $clubVarTemp[0]['isclubbable'];
					//$clubValPre = $_SESSION['clubvalpre'];

					if($clubVal==1 && $clubValPre ==1)
					{
						continue;}
					else
					{
						$errorflag = 'false';
						$msgarray ['leavetypeid'] = ' Leaves are not clubbable.';
						break;
					}
				}

				else if ($leavesDateExists [0] ['leaveexist'] > 0) {
					$errorflag = 'false';
					$msgarray ['from_date'] = ' Leave has already been applied for the above dates.';
					break;
				}
				else if($compoffExists [0] ['compoffexist'] > 0){
				    $errorflag = 'false';
				    $msgarray ['from_date'] = ' Compensatory Leave has already been applied for the above dates.';
				    break;
				}
				}
			}

		}

		/*
		 * END- Validating if leave request has been previoulsy applied
		 */

		/* START Validating whether applied date is prior to date of joining */
		if ($dateofjoining >= $from_date && $from_date != '') {
			$errorflag = 'false';
			$msgarray ['from_date'] = ' Leave cannot be applied before date of joining.';
		}
		/* End */

		if ($leaveday == 2)
			$appliedleavescount = 0.5;
		else if ($leaveday == 1)
			$appliedleavescount = ($days != '' ? $days : $appliedleavesdaycount);

		if (! empty ( $leavetypeArr ) && $appliedleavescount > $leavetypecount) {
			$msgarray ['appliedleavesdaycount'] = "Insufficient " . $leavetypetext . " balance of " . $leavetypecount . " days.";
			$errorflag = 'false';
		}

		// get hr_id from leavemanagemnt table based on login user dept id
		$configure_hr_id = $leavemanagementmodel->gethrDetails ( $employeeDepartmentId );
		if (! empty ( $configure_hr_id )) {
			$hr_id = $configure_hr_id [0] ['hr_id'];
		}

		$saveableData = array (
				'user_id' => $userId,
				'reason' => $reason,
				'leavetypeid' => $leavetypeid,
				'leaveday' => $leaveday,
				'from_date' => $from_date,
				'to_date' => ($to_date != '') ? $to_date : $from_date,
				'leavestatus' => 1,
				'rep_mang_id' => $rep_mang_id,
				'hr_id' => $hr_id,
				'no_of_days' => ($availableleaves >= 0) ? $availableleaves : 0,
				'appliedleavescount' => $appliedleavescount,
				'modifiedby' => $userId,
				'modifieddate' => gmdate ( "Y-m-d H:i:s" )
		);

		try {
			if ($leavetypeid != null) {
				$constraints = $leavetypeconstraintmodel->getActiveConstraintsbyType ( $leavetypeid );
				foreach ( $constraints as $constraint ) {
					$parameters = $leavetypeconstraintmodel->getContraintRequestParameters ( $constraint ['id'] );
					$parameters ['leaverequest'] = $saveableData;
					$iConstraint = Constraint_BaseAbstract::initialize ( $parameters, $constraint ['classname'] );
					$iConstraint->apply ();
				}
			}
		} catch ( Exception $e ) {
			$msgarray ['leavetypeid'] = $e->getMessage ();
			$errorflag = 'false';
		}
		return $errorflag;
	}
	public function getParentLeaveRequestDetails($id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'l' => 'main_leaverequest'
		), array (
				'l.*'
		) )->where ( "l.id = " . $id );

		return $this->fetchAll ( $result )->toArray ();
	}
	public function getLeavesWithinDateByType($id, $leavetypeid,$fromDate) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "select count(l.appliedleavescount) as leavecount from main_leaverequest l where l.user_id=" . $id . " and leavetypeid=" . $leavetypeid . " AND from_date <='" . $fromDate . "' AND to_date >='" . $fromDate . "' and l.leavestatus IN(1,2)";
		$result = $db->query ( $query )->fetch ();
		return $result ['leavecount'] == null ? 0 : $result ['leavecount'];
	}

	public function getNotificationRequestDetails($notificationRequestId){
		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('ml'=>'main_leaverequest'),array('ml.*'))
		->where('ml.id =\''.$notificationRequestId.'\' AND ml.leavestatus ="Approved" AND ml.isactive = 1');
		return $this->fetchAll($select)->toArray();
	}

	public function getNotificationDetails(){
		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('mn'=>'main_notification'),array('mn.*'))
		->where('mn.is_active = 1');
		return $this->fetchAll($select)->toArray();
	}

	public function getNotificationTypeById($notificationTypeId){
		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('mnt'=>'main_notification_type'),array('mnt.notification_type'))
		->where('mnt.id =\''.$notificationTypeId.'\' AND mnt.is_active = 1');
		return $this->fetchAll($select)->toArray();
	}

	public function getLeaveTypeByTypeId($leaveTypeId){
		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('me'=>'main_employeeleavetypes'),array('me.leavetype'))
		->where('me.id =\''.$leaveTypeId.'\' AND me.isactive = 1');
		return $this->fetchAll($select)->toArray();
	}

	public function getNotificationTypeId($notificationType){
		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('mnt'=>'main_notification_type'),array('mnt.id'))
		->where('mnt.notification_type =\''.$notificationType.'\' AND mnt.is_active = 1');
		return $this->fetchAll($select)->toArray();
	}

	public function getLeaveStatus($id){
		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('mlr'=>'main_leaverequest'),array('mlr.leavestatus'))
		->where('mlr.id =\''.$id.'\' AND mlr.isactive = 1');
		return $this->fetchAll($select)->toArray();
	}
	
	public function checkLeaveExistsExceptWFH($date, $wfhId, $id){
	    $db = Zend_Db_Table::getDefaultAdapter ();
	    $query = "select count(l.appliedleavescount) as leavecount from main_leaverequest l where l.user_id=" . $id . " and leavetypeid !=" . $wfhId. " AND str_to_date('".$date."','".DATEFORMAT_PHP."') between from_date and to_date  and l.leavestatus IN(1)";
	    $result = $db->query ( $query )->fetch ();
	    return $result ['leavecount'] == 0 ? false : true;
	}

	public function deleteEntriesForCancelledWFH($userId, $from, $to, $officeStartTime, $officeEndTime){
		$db = Zend_Db_Table::getDefaultAdapter ();
		$date = $from;
		$timezone_offset = TIMEZONE_OFFSET.':00'; 
		$sign = TIMEZONE_OFFSET[0]=='+'?'-':'+';
		$hoursToSubtract = $sign.substr(explode(":", TIMEZONE_OFFSET)['0'],1).' hours';
		$minutesToSubtract = $sign.explode(":", TIMEZONE_OFFSET)['1'].' minutes';
		while ($date <= $to)
		{
			$currentStartTime = date("Y-m-d H:i:s", strtotime($hoursToSubtract, strtotime($date.' '.$officeStartTime)));
			$currentStartTime = date("Y-m-d H:i:s", strtotime($minutesToSubtract, strtotime($currentStartTime)));
			$currentEndTime = date("Y-m-d H:i:s", strtotime($hoursToSubtract, strtotime($date.' '.$officeEndTime)));
			$currentEndTime = date("Y-m-d H:i:s", strtotime($minutesToSubtract, strtotime($currentEndTime)));
			if (strtotime($currentEndTime) < strtotime($currentStartTime))
				$currentEndTime = date("Y-m-d H:i:s",strtotime('+1 day', strtotime($currentEndTime)));
			$query = "Delete from `main_emp_work_activity` where user_id =" .$userId. " and status = 'WFH' and punch_time between '$currentStartTime' and '$currentEndTime'";
			$result = $db->query ( $query );
			$date = date('Y-m-d', strtotime($date . ' +1 day'));
		}
	    return 'update';
	}

}
