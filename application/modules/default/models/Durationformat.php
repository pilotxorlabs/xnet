<!-- Model for Duration format  -->
<?php 
class Default_Model_Durationformat extends Zend_Db_Table_Abstract
{
protected $_name ='main_durationformat';
protected $primary = 'id';

public function getDurationFormatData($sort,$by,$pageNo,$perpage,$searchQuery)
{
	$where ="isactive=1";
	if ($searchQuery)
		$where .= "AND".$searchQuery;
	$db= Zend_Db_Table:: getDefaultAdapter();
	
	$durationFormatData = $this->select()
							->setIntegrityCheck(false)
							->where ($where)
							->order("$by $sort")
							->limitPage($pageNo, $perPage);
	return $durationFormatData;
}	

	public function getSingleDurationFormatData($id)
	{
		$row =$this->fetchRow("id= '".$id."'");
		if(!$row)
		{
			throw new Exception("could not find row $id");
		}
			return $row->toArray();
						
	}
	
	public function SaveorUpdateDurationFormatData($data, $where)
	{
		if ($where != '')
		{
			$this->update($data, $where);
			return 'update';
		} else 
			{
				$this->insert($data);
				$id= $this->getAdapter()->lastInsertedId('main_durationformat');
				return $id;
			}
			
	}
	
	public function getDurationFormatDataByID($id)
	{
		$select= $this->select()
						->setIntegrityCheck(false)
						->from(array('du'=>'main_durationformat'),array('du.*'))
						->where('du.isactive = 1 AND du.id='.$id.' ');
			return $this->fetchAll($select)->toArray();
			
	}
	public function getDurationFormatList()
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from (array('du'=>'main_durationformat'),array('du.id',''))
						->where('du.isactive = 1');
		return $this->fetchAll($select)->toArray();
	}
	
	public function getAllDurationFormats()
	{
		$select = $this -> select()
						->setIntegrityCheck(false)
						->from(array('du'=>'main_durationformat'),array('du.*'))
						->where('du.isactive = 1');
		return $this->fetchAll($select)->toArray();
	}	
	
}