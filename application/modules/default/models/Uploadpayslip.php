<?php

class Default_Model_Uploadpayslip extends Zend_Db_Table_Abstract
{
	protected $_name = 'tbl_payslip';
	protected $_primary = 'id';
	
	public function SaveorUpdateEmpPayslip($data, $where)
	{
		if($where != ''){
			$this->update($data, $where);
			return 'update';
		} else {
			$this->insert($data);
			$id=$this->getAdapter()->lastInsertId('tbl_payslip');
			return $id;
		}
	}
	
	public function checkPayslipNameByUserIdAndPayslipId($userId, $month, $year, $docId='')
	{
		$where = '';
		if($docId)
			$where = ' AND id != '.$docId;
			$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('ed'=>'tbl_payslip'),array('ed.*'))
			->where('ed.user_id =\''.$userId.'\' AND ed.payslip_month = "'.$month.'" AND ed.payslip_year = "'.$year.'"'.$where);
				
			return $this->fetchAll($select)->toArray();
	}
	
	public function isFileNameValid($file_original_name)
	{
		$splitname = substr($file_original_name, strpos($file_original_name, "_")+1);
		$re = '/PaySlip-(XOR\d+)(\D+)_(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC|January|February|March|April|May|June|July|August|September|October|November|December)_(\d{4})/i';
		$matches = '';
		$isFileNameValid = preg_match($re, $file_original_name, $matches, PREG_OFFSET_CAPTURE, 0);
		return $matches;
	}
		
	public function changeMonthToFullName($month){
		switch (strtoupper($month)){
			case 'JAN':
				return "January";
			case 'FEB':
				return "February";
			case 'MAR':
				return "March";
			case 'APR':
				return "April";
			case 'MAY':
				return "May";
			case 'JUN':
				return "June";
			case 'JUL':
				return "July";
			case 'AUG':
				return "August";
			case 'SEP':
				return "September";
			case 'OCT':
				return "October";
			case 'NOV':
				return "November";
			case 'DEC':
				return "December";
		}
	}
}