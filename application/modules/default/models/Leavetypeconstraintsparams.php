<?php
class Default_Model_LeaveTypeConstraintsParams extends Zend_Db_Table_Abstract{
    protected $_name = 'main_leaveconstraint_request_parameters';
    protected $_primary = 'id';
    
    public function saveOrupdateleaveConstraintParameter($data){
        $result = $this->isConstraintParamPresent($data['leaveconstraint_id'], $data['parameter_id']);
        if($result != null){
            $this->update($data, array('id=?'=>$result->toArray()['id']));
            $data['id'] = $result->toArray()['id'];
            return 'update';
        } else {
            $this->insert($data);
            $id=$this->getAdapter()->lastInsertId($this->_name);
            return $id;
        }
        
    }
    
    public function isConstraintParamPresent($leaveconstraint,$paramid){
        $result = $this->select()
        ->setIntegrityCheck(false)
        ->from(array('e'=>'main_leaveconstraint_request_parameters'),array('e.*'))
        ->where("e.leaveconstraint_id = $leaveconstraint AND e.parameter_id = $paramid");
        return $this->fetchRow($result);
    }
    
}