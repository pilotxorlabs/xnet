<?php

class Default_EmployeeexceptionsController extends Zend_Controller_Action
{

	private $options;
	public function preDispatch()
	{
			

	}

	public function init()
	{
		$this->_options= $this->getInvokeArg('bootstrap')->getOptions();

	}

	public function indexAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$loginUserId = $auth->getStorage()->read()->id;
		}
		
		$exceptionrequestmodel = new Default_Model_ExceptionRequest();
		$call = $this->_getParam('call');
		if($call == 'ajaxcall')
			$this->_helper->layout->disableLayout();
		
			$view = Zend_Layout::getMvcInstance()->getView();
			$objname = $this->_getParam('objname');
			$refresh = $this->_getParam('refresh');
			$dashboardcall = $this->_getParam('dashboardcall');
		
			$data = array();
			$searchQuery = '';
			$searchArray = array();
			$tablecontent='';
		
			if($refresh == 'refresh')
			{
				if($dashboardcall == 'Yes')
					$perPage = DASHBOARD_PERPAGE;
					else
						$perPage = PERPAGE;
						$sort = 'ASC';$by = 'exceptionstatus';$pageNo = 1;$searchData = '';
			}
			else
			{
				$sort = ($this->_getParam('sort') !='')? $this->_getParam('sort'):'ASC';
				$by = ($this->_getParam('by')!='')? $this->_getParam('by'):'exceptionstatus';
				if($dashboardcall == 'Yes')
					$perPage = $this->_getParam('per_page',DASHBOARD_PERPAGE);
					else
						$perPage = $this->_getParam('per_page',PERPAGE);
						$pageNo = $this->_getParam('page', 1);
						/** search from grid - START **/
						$searchData = $this->_getParam('searchData');
						$searchData = rtrim($searchData,',');
						/** search from grid - END **/
			}
		
				
			$objName = 'employeeexceptions';
			$queryflag = '';
			$dataTmp = $exceptionrequestmodel->getGrid($sort, $by, $perPage, $pageNo, $searchData,$call,$dashboardcall,$objName,$queryflag);
		
			array_push($data,$dataTmp);
			$this->view->dataArray = $data;
			$this->view->call = $call ;
			$this->view->messages = $this->_helper->flashMessenger->getMessages();
		
	}
	
	public function viewAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$loginUserId = $auth->getStorage()->read()->id;
		}
		$id = $this->getRequest()->getParam('id');
		$callval = $this->getRequest()->getParam('call');
		if($callval == 'ajaxcall')
			$this->_helper->layout->disableLayout();
			$objName = 'employeeexceptions';
			$managerexceptionrequestform = new Default_Form_managerexceptionrequest();
			$managerexceptionrequestform->removeElement("submit");
			$elements = $managerexceptionrequestform->getElements();
			if(count($elements)>0)
			{
				foreach($elements as $key=>$element)
				{
					if(($key!="Cancel")&&($key!="Edit")&&($key!="Delete")&&($key!="Attachments")){
						$element->setAttrib("disabled", "disabled");
					}
				}
			}
	
			try
			{
				if($id && is_numeric($id) && $id>0)
				{
					$this->_redirect('employeeexceptions/edit/id/'.$id);
					$exceptionrequestmodel = new Default_Model_ExceptionRequest();
					$usersmodel= new Default_Model_Users();
					$flag = 'true';
						
					$userid = $exceptionrequestmodel->getUserID($id);
					$getreportingManagerArr = $exceptionrequestmodel->getReportingManagerId($id);
					$reportingManager = $getreportingManagerArr[0]['repmanager'];
					$hrmanager = $getreportingManagerArr[0]['hrmanager'] ;
					if($reportingManager != $loginUserId && $hrmanager != $loginUserId)
						$flag = 'false';
						if(!empty($userid))
						 $isactiveuser = $usersmodel->getUserDetailsByID($userid[0]['user_id']);
						 else
						 	$this->view->rowexist = "rows";
	
						 	if(!empty($userid) && !empty($isactiveuser) && $flag == 'true')
						 	{
						 		$data = $exceptionrequestmodel->getExceptionRequestDetails($id);
						 		if(!empty($data) && $data[0]['exceptionstatus'] == 'Pending')
						 		{
						 			$data = $data[0];
						 			$usersmodel = new Default_Model_Users();
						 			if($data['exceptionday'] == 1)
						 			{
									  $managerexceptionrequestform->exceptionday->addMultiOption($data['exceptionday'],'Full Day');
						 			}
						 			else
						 			{
									  $managerexceptionrequestform->exceptionday->addMultiOption($data['exceptionday'],'Half Day');
						 			}
	
						 			$employeenameArr = $usersmodel->getUserDetailsByID($data['user_id']);
						 			$managerexceptionrequestform->populate($data);
						 				
						 			$from_date = sapp_Global::change_date($data['from_date'], 'view');
						 			$to_date = sapp_Global::change_date($data['to_date'], 'view');
						 			$appliedon = sapp_Global::change_date($data['createddate'], 'view');
						 				
						 			$managerexceptionrequestform->from_date->setValue($from_date);
						 			$managerexceptionrequestform->to_date->setValue($to_date);
						 			$managerexceptionrequestform->createddate->setValue($appliedon);
						 			$managerexceptionrequestform->appliedexceptioncount->setValue($data['appliedexceptioncount']);
						 			$managerexceptionrequestform->employeename->setValue($employeenameArr[0]['userfullname']);
						 			$managerexceptionrequestform->setDefault('exceptionday',$data['exceptionday']);
						 			$this->view->controllername = $objName;
						 			$this->view->id = $id;
						 			$this->view->form = $managerexceptionrequestform;
						 			$this->view->data = $data;
						 		}
						 		else
						 		{
						 			$this->view->rowexist = "rows";
						 		}
						 	}
						 	else
						 	{
						 		$this->view->rowexist = "rows";
						 	}
				}
				else
				{
					$this->view->rowexist = "rows";
				}
					
			}
			catch(Exception $e)
			{
				$this->view->rowexist = 'norows';
			}
				
	}
	
	
	public function editAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$loginUserId = $auth->getStorage()->read()->id;
		}
		$id = $this->getRequest()->getParam('id');
		$callval = $this->getRequest()->getParam('call');
		if($callval == 'ajaxcall')
			$this->_helper->layout->disableLayout();
	
	
			try
			{
				if($id && is_numeric($id) && $id>0)
				{
					$managerexceptionrequestform = new Default_Form_managerexceptionrequest();
					$exceptionrequestmodel = new Default_Model_ExceptionRequest();
					$usersmodel= new Default_Model_Users();
					$flag = 'true';
					$userid = $exceptionrequestmodel->getUserID($id);
					$getreportingManagerArr = $exceptionrequestmodel->getReportingManagerId($id);
					$reportingManager = $getreportingManagerArr[0]['repmanager'];
					$hrmanager = $getreportingManagerArr[0]['hrmanager'] ;
					if($reportingManager != $loginUserId && $hrmanager !=  $loginUserId )
						$flag = 'false';
						if(!empty($userid))
						 $isactiveuser = $usersmodel->getUserDetailsByID($userid[0]['user_id']);
						 else
						 	$this->view->rowexist = "rows";
	
						 	if(!empty($userid) && !empty($isactiveuser) && $flag=='true')
						 	{
						 		$data = $exceptionrequestmodel->getExceptionRequestDetails($id);
						 		if(!empty($data) && ($data[0]['exceptionstatus'] == 'Pending' || $data[0]['exceptionstatus'] == 'Approved'))
						 		{
						 			$data = $data[0];
						 			$reason = $data['reason'];
						 			$appliedexceptioncount = $data['appliedexceptioncount'];
						 			$exceptionstatus =$data['exceptionstatus'];
						 			$employeeid = $data['user_id'];
						 			//$exceptiontypeid = $data['exceptiontypeid'];
						 			//$employeeexceptiontypemodel = new Default_Model_Employeeexceptiontypes();
						 			$usersmodel = new Default_Model_Users();
						 			$employeesmodel = new Default_Model_Employees();
						 			$businessunitid = '';
						 			$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails($employeeid);
									 if($loggedInEmployeeDetails[0]['businessunit_id'] != '')
									 	$businessunitid = $loggedInEmployeeDetails[0]['businessunit_id'];
	
	
									 	if($data['exceptionday'] == 1)
									 	{
									 		$managerexceptionrequestform->exceptionday->addMultiOption($data['exceptionday'],'Full Day');
									 		$data['exceptionday']=	'Full Day';
									 	}
									 	else
									 	{
									 		$managerexceptionrequestform->exceptionday->addMultiOption($data['exceptionday'],'Half Day');
									 		$data['exceptionday']='Half Day';
									 	}
	
									 	$employeenameArr = $usersmodel->getUserDetailsByID($data['user_id']);
									 	$employeeemail = $employeenameArr[0]['emailaddress'];
									 	$employeename = $employeenameArr[0]['userfullname'];
									 	$managerexceptionrequestform->populate($data);
	
									 	if($data['exceptionstatus'] == 'Approved') {
									 		$managerexceptionrequestform->managerstatus->setLabel("Cancel");
									 		$managerexceptionrequestform->managerstatus->clearMultiOptions();
									 		$managerexceptionrequestform->managerstatus->addMultiOption(3,utf8_encode("Cancel"));
									 	}
	
									 	$from_date = sapp_Global::change_date($data['from_date'], 'view');
									 	$to_date = sapp_Global::change_date($data['to_date'], 'view');
									 	$appliedon = sapp_Global::change_date($data['createddate'], 'view');
									 	//to show Exception Request history in view
									 	$exceptionrequesthistory_model = new Default_Model_Exceptionrequesthistory();
									 	$exception_history = $exceptionrequesthistory_model->getExceptionRequestHistory($id);
									 	$this->view->exception_history = $exception_history;
									 	//end
									 	$managerexceptionrequestform->from_date->setValue($from_date);
									 	$managerexceptionrequestform->to_date->setValue($to_date);
									 	$managerexceptionrequestform->createddate->setValue($appliedon);
									 	$managerexceptionrequestform->appliedexceptioncount->setValue($data['appliedexceptioncount']);
									 	$managerexceptionrequestform->employeename->setValue($employeenameArr[0]['userfullname']);
									 	$managerexceptionrequestform->setDefault('exceptionday',$data['exceptionday']);
									 	$this->view->id = $id;
									 	$this->view->form = $managerexceptionrequestform;
									 	$this->view->data = $data;
									 	$managerexceptionrequestform->setAttrib('action',BASE_URL.'employeeexceptions/edit/id/'.$id);
						 		}
						 		else
						 		{
						 			$this->view->rowexist = "rows";
						 		}
						 	}
						 	else
						 	{
						 		$this->view->rowexist = "rows";
						 	}
				}
				else
				{
					$this->view->rowexist = "rows";
				}
					
			}
			catch(Exception $e)
			{
				$this->view->rowexist = 'norows';
			}
			if($this->getRequest()->getPost()){
				$result = $this->save($managerexceptionrequestform,$appliedexceptioncount,$employeeemail,$employeeid,$employeename,$from_date,$to_date,$reason,$businessunitid,$data);
				$this->view->msgarray = $result;
			}
	}
	
	public function save($managerexceptionrequestform,$appliedexceptioncount,$employeeemail,$employeeid,$userfullname,$from_date,$to_date,$reason,$businessunitid,$exceptionreqdata)
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$loginUserId = $auth->getStorage()->read()->id;
		}
		if($managerexceptionrequestform->isValid($this->_request->getPost())){
			$id = $this->_request->getParam('id');
			$managerstatus = $this->_request->getParam('managerstatus');
			$comments = $this->_request->getParam('comments');
			$date = new Zend_Date();
			$exceptionrequestmodel = new Default_Model_Exceptionrequest();
			$usersmodel = new Default_Model_Users();
			$actionflag = '';
			$tableid  = '';
			$status = '';
			$messagestr = '';
			$successmessagestr = '';
		
			$repManagerDetails = $usersmodel->getUserDetailsByID($exceptionreqdata['rep_mang_id']);
			if(!empty($repManagerDetails)) {
				$repMgrEmail = $repManagerDetails[0]['emailaddress'];
				$repMgrName = $repManagerDetails[0]['userfullname'];
			}
			if($managerstatus == 1)
			{
				$exceptionstatus ="Approved";
				$updateemployeeexception = $exceptionrequestmodel->updateemployeeexceptions($exceptionstatus,$employeeid,$comments,$id);
				$status = 2;
				$messagestr = "Exception request approved";
				$successmessagestr  = "Exception request approved successfully.";
				//$exceptiontypetext = $exceptiontypeArr[0]['exceptiontype'];
			}else if($managerstatus == 2)
			{
				$exceptionstatus ="Rejected";
				$updateemployeeexception = $exceptionrequestmodel->updateemployeeexceptions($exceptionstatus,$employeeid,$comments,$id);
				$status = 3;
				$messagestr = "Exception request rejected";
				$successmessagestr  = "Exception request rejected successfully.";
			}else if($managerstatus == 3)
			{
				$exceptionstatus ="Cancelled";
				if($exceptionreqdata['exceptionstatus']=='Approved') {
					
						$updateemployeeexception = $exceptionrequestmodel->updatecancelledemployeeexceptions($exceptionstatus,$employeeid, $comments,$id);
				}
				$status = 4;
				$messagestr = "Exception request cancelled";
				$successmessagestr  = "Exception request cancelled successfully.";
			}
	
			if($managerstatus == 1 || $managerstatus == 2 || $managerstatus == 3)
			{
				$data = array( 'exceptionstatus'=>$status,
						'approver_comments'=> $comments,
						'modifiedby'=>$loginUserId,
						'modifieddate'=>gmdate("Y-m-d H:i:s")
				);
				if($id!=''){
					$where = array('id=?'=>$id);
					$actionflag = 2;
				}
				else
				{
					$data['createdby'] = $loginUserId;
					$data['createddate'] = gmdate("Y-m-d H:i:s");
					$data['isactive'] = 1;
					$where = '';
					$actionflag = 1;
				}
				$Id = $exceptionrequestmodel->SaveorUpdateExceptionRequest($data, $where);
				if($Id == 'update')
				{
					$tableid = $id;
					$this->_helper->getHelper("FlashMessenger")->addMessage($successmessagestr);
				}
				else
				{
					$tableid = $Id;
					$this->_helper->getHelper("FlashMessenger")->addMessage($successmessagestr);
				}
	
				/**
				 exception request history
				 **/
				if($Id == 'update')
				{
	
					if($managerstatus == 1)
					{
						$exceptionstatus='Approved';
					}
					else if($managerstatus == 2)
					{
						$exceptionstatus='Rejected';
					}
					else
					{
						$exceptionstatus='Cancelled';
					}
					$history = 'Exception Request has been '.$exceptionstatus.' by ';
					$exceptionrequesthistory_model = new Default_Model_Exceptionrequesthistory();
					$exception_history = array(
							'exceptionrequest_id' =>$id ,
							'description' => $history,
							'createdby' => $loginUserId,
							'modifiedby' => $loginUserId,
							'isactive' => 1,
							'createddate' =>gmdate("Y-m-d H:i:s"),
							'modifieddate'=>gmdate("Y-m-d H:i:s"),
					);
					$where = '';
					$exceptionhistory = $exceptionrequesthistory_model->saveOrUpdateExceptionRequestHistory($exception_history,$where);
				}
					
				/** MAILING CODE **/
					
				if($to_date == '' || $to_date == NULL)
					$to_date = $from_date;
	
						
					/* Mail to Employee */
					$options['header'] = 'Exception Request';
					$options['toEmail'] = $employeeemail;
					$options['toName'] = $userfullname;
					if($messagestr ==  'Exception request approved'){
						$options['subject'] = $messagestr;
						$options['message'] = '<div>Hi,</div><div>The below exception(s) has been approved.</div>';
					}elseif($messagestr == 'Exception request rejected'){
						$options['subject'] = $messagestr;
						$options['message'] = '<div>Hi,</div><div>The below exception(s) has been rejected. </div>';
					}else{
						$options['subject'] = $messagestr;
						$options['message'] = '<div>Hi,</div><div>The below exception(s) has been cancelled. </div>';
					}
					$options['message'] .= '<div>
                <table width="100%" cellspacing="0" cellpadding="15" border="0" style="border:3px solid #BBBBBB; font-size:16px; font-family:Arial, Helvetica, sans-serif; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody><tr>
                        <td width="28%" style="border-right:2px solid #BBBBBB;">Employee Name</td>
                        <td width="72%">'.$userfullname.'</td>
                      </tr>
                      <tr bgcolor="#e9f6fc">
                        <td style="border-right:2px solid #BBBBBB;">No. of Day(s)</td>
                        <td>'.$appliedexceptioncount.'</td>
                      </tr>
                      <tr>
                        <td style="border-right:2px solid #BBBBBB;">From</td>
                        <td>'.$from_date.'</td>
                      </tr>
                      <tr bgcolor="#e9f6fc">
                        <td style="border-right:2px solid #BBBBBB;">To</td>
                        <td>'.$to_date.'</td>
            	     </tr>
                      <tr bgcolor="#e9f6fc">
                        <td style="border-right:2px solid #BBBBBB;">Reason for exception</td>
                        <td>'.$reason.'</td>
                  </tr>
                </tbody></table>
	
            </div>
            <div style="padding:20px 0 10px 0;">Please <a href="'.BASE_URL.'/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the exception details.</div>';
					$result = sapp_Global::_sendEmail($options);
					/* END */
	
	
					/* Mail to Reporting Manager */
					if(!empty($repMgrEmail) && !empty($repMgrName)) {
						$options['header'] = 'Exception Request';
						$options['toEmail'] = $repMgrEmail;
						$options['toName'] = $repMgrName;
						if($messagestr == 'Exception request approved'){
							$options['subject'] = $messagestr;
							$options['message'] = '<div>Hi,</div><div>The below exception(s) has been approved.</div>';
						}elseif($messagestr == 'Exception request rejected'){
							$options['subject'] = $messagestr;
							$options['message'] = '<div>Hi,</div><div>The below exception(s) has been rejected. </div>';
						}else{
							$options['subject'] = $messagestr;
							$options['message'] = '<div>Hi,</div><div>The below exception(s) has been cancelled. </div>';
						}
						$options['message'] .= '<div>
                <table width="100%" cellspacing="0" cellpadding="15" border="0" style="border:3px solid #BBBBBB; font-size:16px; font-family:Arial, Helvetica, sans-serif; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody><tr>
                        <td width="28%" style="border-right:2px solid #BBBBBB;">Employee Name</td>
                        <td width="72%">'.$userfullname.'</td>
                      </tr>
                      <tr bgcolor="#e9f6fc">
                        <td style="border-right:2px solid #BBBBBB;">No. of Day(s)</td>
                        <td>'.$appliedexceptioncount.'</td>
                      </tr>
                      <tr>
                        <td style="border-right:2px solid #BBBBBB;">From</td>
                        <td>'.$from_date.'</td>
                      </tr>
                      <tr bgcolor="#e9f6fc">
                        <td style="border-right:2px solid #BBBBBB;">To</td>
                        <td>'.$to_date.'</td>
            	     </tr>
                      <tr bgcolor="#e9f6fc">
                        <td style="border-right:2px solid #BBBBBB;">Reason for Exception</td>
                        <td>'.$reason.'</td>
                  </tr>
                </tbody></table>
	
            </div>
            <div style="padding:20px 0 10px 0;">Please <a href="'.BASE_URL.'/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the exception details.</div>';
						$result = sapp_Global::_sendEmail($options);
					}
					/* END */
					 	
					/* Mail to HR */
					if (defined('LV_HR_'.$businessunitid) && $businessunitid !='')
					{
						$options['header'] = 'Exception Request';
						$options['toEmail'] = constant('LV_HR_'.$businessunitid);
						$options['toName'] = 'Exception Management';
						if($messagestr == 'Exception request approved'){
							$options['subject'] = $messagestr;
							$options['message'] = '<div>Hi,</div><div>The below exception(s) has been approved.</div>';
						}elseif($messagestr == 'Exception request rejected'){
							$options['subject'] = $messagestr;
							$options['message'] = '<div>Hi,</div><div>The below exception(s) has been rejected. </div>';
						}else{
							$options['subject'] = $messagestr;
							$options['message'] = '<div>Hi,</div><div>The below exception(s) has been cancelled. </div>';
						}
						$options['message'] .= '<div>
                <table width="100%" cellspacing="0" cellpadding="15" border="0" style="border:3px solid #BBBBBB; font-size:16px; font-family:Arial, Helvetica, sans-serif; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
	                      <tr>
	                        <td width="28%" style="border-right:2px solid #BBBBBB;">Employee Name</td>
	                        <td width="72%">'.$userfullname.'</td>
	                      </tr>
	                      <tr bgcolor="#e9f6fc">
	                        <td style="border-right:2px solid #BBBBBB;">No. of Day(s)</td>
	                        <td>'.$appliedexceptioncount.'</td>
	                      </tr>
	                      <tr>
	                        <td style="border-right:2px solid #BBBBBB;">From</td>
	                        <td>'.$from_date.'</td>
	                      </tr>
	                      <tr bgcolor="#e9f6fc">
	                        <td style="border-right:2px solid #BBBBBB;">To</td>
	                        <td>'.$to_date.'</td>
     	            	  </tr>
		    	          <tr bgcolor="#e9f6fc">
	                        <td style="border-right:2px solid #BBBBBB;">Reason for Exception</td>
	                        <td>'.$reason.'</td>
	                      </tr>
                		</tbody>
                </table>
	
            </div>
            <div style="padding:20px 0 10px 0;">Please <a href="'.BASE_URL.'/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the exception details.</div>';
						$result = sapp_Global::_sendEmail($options);
					}
					/* END */
					$menuID = MANAGEREMPLOYEEVACATIONS;
					$result = sapp_Global::logManager($menuID,$actionflag,$loginUserId,$tableid);
					$this->_redirect('employeeexceptions');
			}
				
		}else
		{
			$messages = $managerexceptionrequestform->getMessages();
			foreach ($messages as $key => $val)
			{
				foreach($val as $key2 => $val2)
				{
					$msgarray[$key] = $val2;
					break;
				}
			}
			return $msgarray;
		}
	}
	
}