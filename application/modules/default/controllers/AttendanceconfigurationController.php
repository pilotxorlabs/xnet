<?php

class Default_AttendanceconfigurationController extends Zend_Controller_Action
{

    private $options;
    public function init()
    {
        $this->_options= $this->getInvokeArg('bootstrap')->getOptions();
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('gethremployees', 'json')->initContext();
        $ajaxContext->addActionContext('addconfigtype', 'html')->initContext();
        $ajaxContext->addActionContext('save', 'json')->initContext();

    }

    public function indexAction()
    {
		$attendanceconfigurationmodel = new Default_Model_Attendanceconfiguration();

        $call = $this->_getParam('call');
		if($call == 'ajaxcall')
				$this->_helper->layout->disableLayout();

		$view = Zend_Layout::getMvcInstance()->getView();
		$objname = $this->_getParam('objname');
		$refresh = $this->_getParam('refresh');
		$dashboardcall = $this->_getParam('dashboardcall');

		$data = array();
		$searchQuery = '';
		$searchArray = array();
		$tablecontent='';

		if($refresh == 'refresh')
		{
		    if($dashboardcall == 'Yes')
				$perPage = DASHBOARD_PERPAGE;
			else
				$perPage = PERPAGE;
			$sort = 'DESC';$by = 'l.modifieddate';$pageNo = 1;$searchData = '';$searchQuery = '';$searchArray='';
		}
		else
		{
			$sort = ($this->_getParam('sort') !='')? $this->_getParam('sort'):'DESC';
			$by = ($this->_getParam('by')!='')? $this->_getParam('by'):'l.modifieddate';
			if($dashboardcall == 'Yes')
				$perPage = $this->_getParam('per_page',DASHBOARD_PERPAGE);
			else
			    $perPage = $this->_getParam('per_page',PERPAGE);
			$pageNo = $this->_getParam('page', 1);
			// search from grid - START
			$searchData = $this->_getParam('searchData');
			$searchData = rtrim($searchData,',');
			// search from grid - END
		}
		$dataTmp = $attendanceconfigurationmodel->getGrid($sort, $by, $perPage, $pageNo, $searchData,$call,$dashboardcall);
		array_push($data,$dataTmp);
		$this->view->dataArray = $data;
		$this->view->call = $call ;
		$this->view->messages = $this->_helper->flashMessenger->getMessages();
    }



	public function addAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$loginUserId = $auth->getStorage()->read()->id;
			$department_id = $auth->getStorage()->read()->department_id;
		}
		$callval = $this->getRequest()->getParam('call');
		if($callval == 'ajaxcall')
			$this->_helper->layout->disableLayout();

		$attendanceconfigurationform = new Default_Form_attendanceconfiguration();
		$attendanceconfigurationmodel = new Default_Model_Attendanceconfiguration();
		$busineesUnitModel = new Default_Model_Businessunits();
		$msgarray = array();
		$businessunitData = $busineesUnitModel->getDeparmentList();
		$attConfTypes = $attendanceconfigurationmodel->getAttendanceConfigurationType();
		if(sizeof($businessunitData) > 0)
		{
			$attendanceconfigurationform->businessunit->addMultiOption('0','No Business Unit');
			foreach ($businessunitData as $businessunitres){
				$attendanceconfigurationform->businessunit->addMultiOption($businessunitres['id'],$businessunitres['unitname']);
			}
		}
		else
		{
			$msgarray['business_unit'] = 'Business units are not added yet.';
			$bu_msg = 'no bu';
		}
		if(sizeof($attConfTypes) > 0)
		{
			$attendanceconfigurationform->configuration_type_id->addMultiOption('0','Configuration Type');
			foreach ($attConfTypes as $attConfType){
				$attendanceconfigurationform->configuration_type_id->addMultiOption($attConfType['configuration_type_id'],$attConfType['configuration_name']);
			}
		}
		else
		{
			$msgarray['configuration_type_id'] = 'Attendance configurations are not added yet.';
			$bu_msg = 'no bu';
		}
		$this->view->bu_msg = $bu_msg;
		$this->view->months_msg = $months_msg;
		$this->view->days_msg = $days_msg;
		$this->view->msgarray = $msgarray;
		$this->view->form = $attendanceconfigurationform;
	}

    public function viewAction()
	{
	    $auth = Zend_Auth::getInstance();
     	if($auth->hasIdentity()){
					$loginUserId = $auth->getStorage()->read()->id;
					$loginuserRole = $auth->getStorage()->read()->emprole;
					$loginuserGroup = $auth->getStorage()->read()->group_id;
		}
		$id = $this->getRequest()->getParam('id');
		$callval = $this->getRequest()->getParam('call');
		if($callval == 'ajaxcall')
			$this->_helper->layout->disableLayout();


		$objName = 'attendanceconfiguration';
			try
			{
				if($id)
				{
				    if(is_numeric($id) && $id>0)
				    {
						$attendanceconfigurationmodel = new Default_Model_Attendanceconfiguration();
						$businessUnitData = $attendanceconfigurationmodel->getBusinessUnitData($id);
						$modelName = "Default_Model_".str_replace(" ","",$businessUnitData[0]['configuration_name']);
						$model = new $modelName();
						$data = $model->getConfigurationData($id);
						if($businessUnitData[0]['configuration_type_id'] == 1){
							$data[0]['entry_time_action'] = $this->getOfficeHoursAction($data[0]['entry_time_action']);
							$data[0]['exit_time_action'] = $this->getOfficeHoursAction($data[0]['exit_time_action']);
						}
						
						if($businessUnitData[0]['configuration_type_id'] == 2){
							$data[0]['minimum_hours_action'] = $this->getAction($data[0]['minimum_hours_action']);
							$data[0]['maximum_hours_action'] = $this->getAction($data[0]['maximum_hours_action']);
						}
						if($businessUnitData[0]['configuration_type_id'] == 3){
							$data[0]['action'] = $this->getAction($data[0]['action']);
						}
						if($businessUnitData[0]['configuration_type_id'] == 4){
							$data[0]['considerationWindow'] = $this->getConsiderationWindow($data[0]['considerationWindow']);
						}
						if(!empty($data))
							{

 								$data = $data[0];
 								$this->view->rowexist = "";


							}
							else
							{
							   $this->view->rowexist = "norows";
							}
					}else
					{
					   $this->view->rowexist = "norows";
					}
				}else{
				    $this->view->rowexist = "norows";
				}
			}
			catch(Exception $e)
			{
				  $this->view->rowexist = "norows";
			}


			$this->view->controllername = $objName;
			$this->view->id = $id;
			$this->view->data = $data;
			$this->view->businessUnitData = $businessUnitData[0];

	}

public function editAction()
{
	$auth = Zend_Auth::getInstance();
	if($auth->hasIdentity()){
			
		$loginUserId = $auth->getStorage()->read()->id;
		$loginuserRole = $auth->getStorage()->read()->emprole;
		$loginuserGroup = $auth->getStorage()->read()->group_id;
	}
	$id = $this->getRequest()->getParam('id');
	$callval = $this->getRequest()->getParam('call');
	if($callval == 'ajaxcall')
		$this->_helper->layout->disableLayout();

		$objName = 'attendanceconfiguration';
		$attendanceconfigurationform = new Default_Form_attendanceconfiguration();
		$configofficehoursform = new Default_Form_configofficehours();
		$configmandatoryhoursform = new Default_Form_configmandatoryhours();
		$configmissedpunchesform = new Default_Form_configmissedpunches();
		$configcompensatoryoffform = new Default_Form_configcompensatoryoff();

		$attendanceconfigurationform = new Default_Form_attendanceconfiguration();
		$attendanceconfigurationmodel = new Default_Model_Attendanceconfiguration();
		$busineesUnitModel = new Default_Model_Businessunits();
		$msgarray = array();
		$businessunitData = $busineesUnitModel->getDeparmentList();
		$attConfTypes = $attendanceconfigurationmodel->getAttendanceConfigurationType();
		if(sizeof($businessunitData) > 0)
		{
			$attendanceconfigurationform->businessunit->addMultiOption('0','No Business Unit');
			foreach ($businessunitData as $businessunitres){
				$attendanceconfigurationform->businessunit->addMultiOption($businessunitres['id'],$businessunitres['unitname']);
			}
		}
		else
		{
			$msgarray['business_unit'] = 'Business units are not added yet.';
			$bu_msg = 'no bu';
		}
		if(sizeof($attConfTypes) > 0)
		{
			$attendanceconfigurationform->configuration_type_id->addMultiOption('0','Configuration Type');
			foreach ($attConfTypes as $attConfType){
				$attendanceconfigurationform->configuration_type_id->addMultiOption($attConfType['configuration_type_id'],$attConfType['configuration_name']);
			}
		}
		else
		{
			$msgarray['configuration_type_id'] = 'Attendance configurations are not added yet.';
			$bu_msg = 'no bu';
		}
		

		try
		{
			if($id)
			{
				if(is_numeric($id) && $id>0)
				{
					$attendanceconfigurationmodel = new Default_Model_Attendanceconfiguration();
					$businessUnitData = $attendanceconfigurationmodel->getBusinessUnitData($id);
					
					$dataAttendanceConfiguration = $businessUnitData[0]['configuration_type_id'];
			
					if ($dataAttendanceConfiguration['configuration_type_id'] == '1')
					{
						$officehoursmodel = new Default_Model_OfficeHours();
						$data = $officehoursmodel->getConfigurationData($id);						
					}
					else if ($dataAttendanceConfiguration['configuration_type_id'] == '2')
					{
						$mandatoryhoursconfigurationmodel = new Default_Model_MandatoryHours();
						$data = $mandatoryhoursconfigurationmodel->getConfigurationData($id);
					}
					else if ($dataAttendanceConfiguration['configuration_type_id'] == '3')
					{
						$missedpunchesconfigurationmodel = new Default_Model_MissedPunches();
						$data = $missedpunchesconfigurationmodel->getConfigurationData($id);
					}
					else if ($dataAttendanceConfiguration['configuration_type_id'] == '4')
					{
						$compensatoryoffconfigurationmodel = new Default_Model_CompensatoryOff();
						$data = $compensatoryoffconfigurationmodel->getConfigurationData($id);
					}					
					
					if(!empty ($businessUnitData))
					{
						$businessUnitData = $businessUnitData[0];						
						$attendanceconfigurationform->populate($businessUnitData);						
						$this->view->rowexist = "";

					}
					if(!empty($data))
					{

						$data = $data[0];
						
						
						if ($businessUnitData['configuration_type_id'] == '1')
						{
							$configofficehoursform->populate($data);
							$configofficehoursform->submit->setLabel('Update');
						}
						else if ($businessUnitData['configuration_type_id'] == '2')
						{
							$data['duration'] = $this->getDurationId($data['duration']);
							$configmandatoryhoursform->populate($data);
							$configmandatoryhoursform->submit->setLabel('Update');
						}
						else if ($businessUnitData['configuration_type_id'] == '3')
						{
							$configmissedpunchesform->populate($data);
							$configmissedpunchesform->submit->setLabel('Update');
						}
						else if ($businessUnitData['configuration_type_id'] == '4')
						{
							$configcompensatoryoffform->populate($data);
							$configmissedpunchesform->submit->setLabel('Update');
						}
						$this->view->rowexist = "";
							
					}
					else
					{
						$this->view->rowexist = "norows";
					}
				}else
				{
					$this->view->rowexist = "norows";
				}
			}else{
				$this->view->rowexist = "norows";
			}
		}
		catch(Exception $e)
		{
			$this->view->rowexist = "norows";
		}

		$attendanceconfigurationform->setAttrib('action',BASE_URL.'attendanceconfiguration/edit/id/'.$id);
		$this->view->controllername = $objName;
		$this->view->id = $id;
		
		$this->view->data = $data;
				
		$this->view->formAttendanceConfig = $attendanceconfigurationform;
		
		if ($businessUnitData['configuration_type_id'] == '1')
		{
			$this->view->form = $configofficehoursform;
		}
		else if ($businessUnitData['configuration_type_id'] == '2')
		{
			$this->view->form = $configmandatoryhoursform;
		}
		else if ($businessUnitData['configuration_type_id'] == '3')
		{
			$this->view->form = $configmissedpunchesform;
		}
		else if ($businessUnitData['configuration_type_id'] == '4')
		{
			$this->view->form = $configcompensatoryoffform;
		}
		
		if($id!=''){
			$where = array('id=?'=>$id);
			
				
		}

		if($this->getRequest()->getPost()){
			try{
			    $postData = json_decode($this->getRequest()->getPost()['formdata'],true);
			    foreach ($postData as $key => $value){
			        if($value['name']== 'businessunit')
			            $mainFormInputs['business_unit_id'] = trim($value['value']);
			            else
			                $mainFormInputs[$value['name']] = trim($value['value']);
			    }
			    $formName = "Default_Form_config".strtolower(str_replace(" ","",$attConfTypes[$dataAttendanceConfiguration['configuration_type_id']- 1]['configuration_name']));
			    $form = new $formName();
			    $IsFormValid = true;
			    foreach ($form->getElements() as $key=>$element){
			        if(isset($mainFormInputs[$key])){
			            if(!$element->isValid($mainFormInputs[$key])){
			                $IsFormValid = false;
			            }
			        }
			    }
				$dataAttendanceConfiguration = array(
						'business_unit_id'=>trim($mainFormInputs['businessunit']),
						'configuration_type_id'=>trim($mainFormInputs['configuration_type_id'])
				);
				if($IsFormValid){
				    $modelName = "Default_Model_".str_replace(" ","",$attConfTypes[$dataAttendanceConfiguration['configuration_type_id']- 1]['configuration_name']);
				    $model = new $modelName();
				    $messageArray = array();
				    $model->runCustomValidation($mainFormInputs,$messageArray);
				    if(count($messageArray) >0){
				        $this->_helper->json(json_encode($messageArray));
				    }
    				unset($mainFormInputs['business_unit_id']);
    				unset($mainFormInputs['configuration_type_id']);
    				unset($mainFormInputs['submit']);
    				if($id!=''){
    					$where = array('configuration_id=?'=>$id);					
    				}
    				else
    				{
    					$where = '';					
    				}
    		
    				$attConfTypes = $attendanceconfigurationmodel->getAttendanceConfigurationType();
    			
    				$mainFormInputs['configuration_id'] = $id;
    		
    				$modelName = "Default_Model_".str_replace(" ","",$attConfTypes[$dataAttendanceConfiguration['configuration_type_id']- 1]['configuration_name']);
    				$model = new $modelName();
    				$Id = $model->SaveorUpdateData($mainFormInputs, $where);
    		
    				$this->_helper->getHelper("FlashMessenger")->addMessage(array("success"=>"Attendance configuration editted successfully."));
    				$this->_helper->json(json_encode("success"));
				}else{
				    $messages = $form->getMessages();
				    foreach ($messages as $key => $val)
				    {
				        foreach($val as $key2 => $val2)
				        {
				            $msgarray[$key] = $val2;
				            break;
				        }
				    }
				    $this->_helper->json(json_encode($msgarray));
				}
			}catch(Exception $e){
				$msgarray['businessunit'] = 'Something went wrong.Please try again.';
			}
		}	
	}


    public function saveAction(){
        $postData = json_decode($this->getRequest()->getPost()['formdata'],true);
        if($postData){
            try{                
                $attendanceconfigurationmodel = new Default_Model_Attendanceconfiguration();
                $attConfTypes = $attendanceconfigurationmodel->getAttendanceConfigurationType();
                foreach ($postData as $key => $value){
                    if($value['name']== 'businessunit')
                        $mainFormInputs['business_unit_id'] = trim($value['value']);
                    else
                        $mainFormInputs[$value['name']] = trim($value['value']);
                }
                $override = $this->getRequest()->getPost()['override'];
                $actionflag = '';
                $tableid  = '';
                
                $dataAttendanceConfiguration = array(
                    'business_unit_id'=>trim($mainFormInputs['business_unit_id']),
                    'configuration_type_id'=>trim($mainFormInputs['configuration_type_id'])
                );
                $formName = "Default_Form_config".strtolower(str_replace(" ","",$attConfTypes[$dataAttendanceConfiguration['configuration_type_id']- 1]['configuration_name']));
                $form = new $formName();
                $IsFormValid = true;
                foreach ($form->getElements() as $key=>$element){
                    if(isset($mainFormInputs[$key])){
                        if(!$element->isValid($mainFormInputs[$key])){
                            $IsFormValid = false;
                        }
                    }
                }
                if($IsFormValid){
                    $modelName = "Default_Model_".str_replace(" ","",$attConfTypes[$dataAttendanceConfiguration['configuration_type_id']- 1]['configuration_name']);
                    $model = new $modelName();
                    $messageArray = array();
                    $model->runCustomValidation($mainFormInputs,$messageArray);
                    if(count($messageArray) >0){                        
                        $this->_helper->json(json_encode($messageArray));
                    }
                    $id = $model->checkConfigurationExists($mainFormInputs);
                    
                    unset($mainFormInputs['business_unit_id']);
                    unset($mainFormInputs['configuration_type_id']);
                    unset($mainFormInputs['submit']);
                    
                    if($id != ''){
                        if($override === 'true'){
                            $Id = $model->SaveorUpdateData($mainFormInputs, array('id=?'=>$id));
                            $this->_helper->getHelper("FlashMessenger")->addMessage(array("success"=>"Attendance configuration updated successfully."));
                            $this->_helper->json(json_encode("success"));
                        }else{                                
                            $this->_helper->json(json_encode("exists"));
                        }
                    }else{
                    
                        $actionflag = 1;                
                        
                        $configurationId = $attendanceconfigurationmodel->SaveorUpdateMainAttendanceConfiguration($dataAttendanceConfiguration, '');
                        $mainFormInputs['configuration_id'] = $configurationId;
                        
                        $Id = $model->SaveorUpdateData($mainFormInputs, '');
                        
                        $this->_helper->getHelper("FlashMessenger")->addMessage(array("success"=>"Attendance configuration added successfully."));
                         $this->_helper->json(json_encode("success"));
                    }
                }else{
                    $messages = $form->getMessages();
                    foreach ($messages as $key => $val)
                    {
                        foreach($val as $key2 => $val2)
                        {
                            $msgarray[$key] = $val2;
                            break;
                        }
                    }
                }
            }catch(Exception $e){
                $msgarray['businessunit'] = $e->getMessage();
            }
        }
        
        $this->_helper->json(json_encode($msgarray));
    }

	public function getuniqueDepartment($businessunit_id)
	{

		   $attendanceconfigurationmodel = new Default_Model_Attendanceconfiguration();
		   $departmentidsArr = $attendanceconfigurationmodel->getActiveDepartmentIds();
		   $departmentsmodel = new Default_Model_Departments();
			$depatrmentidstr = '';
			$newarr = array();
			if(!empty($departmentidsArr))
				{
					$where = '';
					for($i=0;$i<sizeof($departmentidsArr);$i++)
					{
						$newarr1[] = array_push($newarr, $departmentidsArr[$i]['deptid']);

					}
					$depatrmentidstr = implode(",",$newarr);
					foreach($newarr as $deparr)
					{
					$where.= " id!= $deparr AND ";
					}
					$where = trim($where," AND");
					$querystring = "Select d.id,d.deptname from main_departments as d where d.unitid=$businessunit_id and d.isactive=1 and $where  ";

					$uniquedepartmentids = $departmentsmodel->getUniqueDepartments($querystring);
					return $uniquedepartmentids;
				}
			else
			    {
					$departmentlistArr = $departmentsmodel->getDepartmentList($businessunit_id);
					return $departmentlistArr;
                }
	}

	public function deleteAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$loginUserId = $auth->getStorage()->read()->id;
		}
		$id = $this->_request->getParam('objid');
		
		if($id)
		{
			$attendanceconfigurationmodel = new Default_Model_Attendanceconfiguration();
			$attConfTypes = $attendanceconfigurationmodel->getAttendanceConfigurationType();
			$attendanceconfigurationtypeid = $attendanceconfigurationmodel->getattendanceconfigurationtypeid($id);
			$modelName = "Default_Model_".str_replace(" ","",$attConfTypes[$attendanceconfigurationtypeid[0]['configuration_type_id']- 1]['configuration_name']);
			$model = new $modelName();
			$attendanceconfigurationmodel->deleteAttendanceConfigurationDetails($id);
			$model->deleteAttendanceConfigurationDetails($id);
			$messages['message'] = 'Attendance Configuration deleted successfully.';
			$messages['msgtype'] = 'success';
		}
		else
		{
			$messages['message'] = 'Attendance Configuration cannot be deleted.';
			$messages['msgtype'] = 'error';
		}
		
		$this->_helper->json($messages);
	
	}
	
	public function addconfigtypeAction(){

		$configName =  $this->_request->getParam('configname');
		$formClassName = "Default_Form_config". strtolower(str_replace(" ", "", $configName));

		$data['configid'] = $this->_request->getParam('configid');
		$this->view->data= $data;
		$this->view->form =  new $formClassName();
	}

	public function getAction($actionId){
		if($actionId == 1)
			return 'Absent';
		if ($actionId == 2)
			return  'Half Day';
		if ($actionId == 3)
			return 'Ignore';
	}
	
	public function getOfficeHoursAction($actionId){
		if($actionId == 0)
			return 'Default To Threshold';
		if($actionId == 1)
			return 'Absent';
		if ($actionId == 2)
			return  'Half Day';
		if ($actionId == 3)
			return 'Ignore';
	}
	
	public function getDurationId($duration){
		return (array(
				'Half Day' => '1',
				'Daily' => '2',
				'Weekly' => '3',
				'Monthly' => '4',
				'Quarterly' => '5',
				'Half Yearly' => '6',
				'Yearly' => '7' 
		)[$duration]);
	}
	
	public function getDuration($duration){
		return (array(
				  '1' => 'Half Day',
				  '2' => 'Daily',
				  '3' => 'Weekly',
				  '4' => 'Monthly',
				  '5' => 'Quarterly',
				  '6' => 'Half Yearly',
				  '7' => 'Yearly'
		)[$duration]);
	}
	
	public function getConsiderationWindow($considerationWindow){
		return (array(
				'1' => 'Actual date',
				'2' => 'Applied date',
				'3' => 'Approved date'
		)[$considerationWindow]);
	}
}

