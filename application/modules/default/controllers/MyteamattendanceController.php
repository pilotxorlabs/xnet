<?php
/*********************************************************************************
 *  This file is part of Sentrifugo.
 *  Copyright (C) 2015 Sapplica
 *
 *  Sentrifugo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Sentrifugo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Sentrifugo.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Sentrifugo Support <support@sentrifugo.com>
 ********************************************************************************/
class Default_MyteamattendanceController extends Zend_Controller_Action {
	private $options;
	public function preDispatch() {
		$ajaxContext = $this->_helper->getHelper ( 'AjaxContext' );
		$ajaxContext->addActionContext ( 'getempattendance', 'html' )->initContext ();
		$ajaxContext->addActionContext ( 'getbasicempattendance', 'json' )->initContext ();
	}
	public function init() {
		$this->_options = $this->getInvokeArg ( 'bootstrap' )->getOptions ();
	}
	public function indexAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
			$loginUserRole = $auth->getStorage ()->read ()->emprole;
		}
		$members = array ();
		$employeeModel = new Default_Model_Employee ();
		$orgModel = new Default_Model_Organisationinfo ();
		$organisationInfo = $orgModel->getOrganisationData ( 1 );
		if ($employeeModel->isHrEmployee ( $loginUserId ) || $loginUserRole == SUPERADMINROLE) {
			$myteam = $employeeModel->getEmployees ( '', $loginUserId, '', '', null, null, null );
		} else {
			$myteam = $employeeModel->getEmployeesUnderRM ( $loginUserId );
		}
		
		foreach ( $myteam as $member ) {
			
			$employeeInfo = $employeeModel->getActiveEmployeeData ( $member ['user_id'] );
			if ($employeeInfo [0] ['businessunit_id'] != null) {
				$mem ['user_id'] = $member ['user_id'];
				$mem ['userfullname'] = $member ['userfullname'];
				array_push ( $members, $mem );
			}
		}
		
		$this->view->data = $members;
		if ($organisationInfo ['org_startdate'] != null) {
			$org_start_date = (new DateTime ( $organisationInfo ['org_startdate'] ))->format ( 'Y' );
			$org_start_date = $org_start_date . ',' . ((new DateTime ( $organisationInfo ['org_startdate'] ))->format ( 'm' ) - 1) . ',';
			$org_start_date = $org_start_date . (new DateTime ( $organisationInfo ['org_startdate'] ))->format ( 'd' );
			
			$this->view->data ['org_start_date'] = $org_start_date;
		}
	}
	public function getempattendanceAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}

		$myattendancemodel = new Default_Model_Myattendance ();
		$employeeExceptionModel = new Default_Model_ExceptionRequest ();
		if ($this->_getParam ( 'id' ) != NULL) {
			$id = $this->_getParam ( 'id' );
		} else {
			$id = $loginUserId;
		}
		$start = date ( DATEFORMAT_PHP, strtotime ( $this->_getParam ( 'from_date' ) ) );
		$end = date ( DATEFORMAT_PHP, strtotime ( $this->_getParam ( 'to_date' ) ) );
		$data ['empAttendance'] = $myattendancemodel->getEmployeeAttendence ( $id, $start, $end, $employeeExceptionModel, false );
		$this->view->data = $data;
	}
	public function getbasicempattendanceAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
			$loginUserRole = $auth->getStorage ()->read ()->emprole;
		}

        $empWorkActivityModel = new Default_Model_EmployeeWorkActivity ();
		$employeeModel = new Default_Model_Employee ();
		$date = date ( DATEFORMAT_PHP, strtotime ( $this->_getParam ( 'date' ) ) );
		$activityBreakdown = false;
		if ($this->_getParam ( 'showbreakdown' ) != NULL) {
			$activityBreakdown = $this->_getParam ( 'showbreakdown' );
		}
		
		$dailyPunchDetails = array ();
		if ($employeeModel->isHrEmployee ( $loginUserId ) || $loginUserRole == SUPERADMINROLE) {
			$myteam = $employeeModel->getEmployees ( '', $loginUserId, '', '', null, null, null );
		} else {
			$myteam = $employeeModel->getEmployeesUnderRM ( $loginUserId );
		}
		foreach ( $myteam as $teamMember ) {
			$workhours = 0;
			if ($teamMember ['businessunit_id'] != null) {
				$dailyActivity = $empWorkActivityModel->getEmployeeActivities ( new DateTime ( $date ), $teamMember ['user_id'], $workhours, $activityBreakdown );
				if (count ( $dailyActivity ) > 0) {
					$dailyPunchDetails [$teamMember ['userfullname']] = $dailyActivity;
				}
			}
		}
		$this->_helper->json ( json_encode ( $dailyPunchDetails ) );
	}
	public function getdailyactivityAction() {
		$myattendancemodel = new Default_Model_Myattendance ();
        $empWorkActivityModel = new Default_Model_EmployeeWorkActivity();
		$id = $this->_getParam ( 'id' );
		$start = date ( DATEFORMAT_PHP, strtotime ( $this->_getParam ( 'from_date' ) ) );
		$end = date ( DATEFORMAT_PHP, strtotime ( $this->_getParam ( 'to_date' ) ) );
		$activityBreakdown = false;
		if ($this->_getParam ( 'showbreakdown' ) != NULL) {
			$activityBreakdown = $this->_getParam ( 'showbreakdown' );
		}
		
		$startDate = new DateTime ( $start );
		$endDate = new DateTime ( $end );
		if ($endDate > new DateTime ())
			$endDate = new DateTime ();
		$dailyPunchDetails = array ();
		while ( $startDate <= $endDate ) {
			$workhours = 0;
			$dailyActivity = $empWorkActivityModel->getEmployeeActivities ( $startDate, $id, $workhours, $activityBreakdown );
			$dailyPunchDetails [$startDate->format ( "Y-m-d" )] ['activities'] = $dailyActivity;
            if (count ( $dailyActivity ) == 3 && $dailyActivity [count ( $dailyActivity ) - 2] ['status'] == "Missed Punch") {
                $dailyPunchDetails [$startDate->format("Y-m-d")] ['workhours'] = $myattendancemodel->checkformat(HOURSPERDAY);
            } else {
                $dailyPunchDetails [$startDate->format("Y-m-d")] ['workhours'] = $myattendancemodel->checkformat($workhours);
            }
			$startDate->modify ( '+1 day' );
		}
		$this->_helper->json ( json_encode ( $dailyPunchDetails ) );
	}
}