<?php
class Default_UploadpayslipController extends Zend_Controller_Action
{

	private $options;
	public function preDispatch()
	{
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('validateempid', 'json')->initContext();
	}

 	public function init()
    {
        $this->_options= $this->getInvokeArg('bootstrap')->getOptions();

    }
 	public function indexAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$loginUserId = $auth->getStorage()->read()->id;
			$loginUserdepartment_id = $auth->getStorage()->read()->department_id;
		}

		$id = $this->getRequest()->getParam('userid');

		$payslipform = new Default_Form_uploadpayslip();
		$payslipform->setAttrib('action',BASE_URL.'uploadpayslip');
		$payslipmodel = new Default_Model_Uploadpayslip();
		$this->view->id = $loginUserId;
		$this->view->form = $payslipform;

	}

	public function isValidEmpId($userid)
	{
		$employee = new Default_Model_Employee();
		$useridArray = $employee->getUserIdByEmpId($userid);
		if(count($useridArray) == 0){

			return false;
		}
		return true;
	}

	public function uploadsaveAction()
    {
        $user_id = sapp_Global::_readSession('id');
        $filedata = array();
		$uploadLog = array();
        $empPayslipModel = new Default_Model_Uploadpayslip();
        $logUploadPayslipModel = new Default_Model_LogUploadPayslip();

        $fileName = $_FILES["myfile"]["name"];
        $newName  = time().'_'.$user_id.'_'.str_replace(' ', '_', $fileName);
        $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileName);


        $matches = $empPayslipModel->isFileNameValid($withoutExt);
        $filedata['original_name'] = $fileName;
        $filedata['status'] = FILE_INVALID;
        $filedata['reason'] = "File format not supported.";
        if(sizeof($matches) !=0){
        	$month = $matches[3][0];
        	$name = $matches[2][0];
        	$name = substr($name, 1, $namelen-1);
            $filedata['month'] = $empPayslipModel->changeMonthToFullName($month);
            $filedata['year'] =$matches[4][0];
            $filedata['empId'] = $matches[1][0];
            $filedata['empName'] = $name;
        }

        if($_FILES == null){
        	$filedata['original_name'] = "Invalid File.";
        	$filedata['status'] = FILE_INVALID;
        }else if(stripos($_FILES["myfile"]['type'], FORMAT_PDF) == false){  // Validate file is of pdf format
        	$filedata['status'] = FILE_INVALID;
        	$filedata['reason'] = "File format not supported.";
        }else if(sizeof($matches) ==0){
    		$filedata['reason'] = "File name is not in proper format.";
    		$filedata['status'] = FILE_INVALID;
    	}else if(stripos($withoutExt, ',') != false){
    		$filedata['reason'] = "Comma( , ) not allowed in file names.";
    		$filedata['status'] = FILE_INVALID;
    	}else if($_FILES["myfile"]["size"] > (2*1024*1024)) {
             $filedata['status'] = FILE_INVALID;
             $filedata['reason'] = "File size exceeds 2MB.";
        }else if($_FILES["myfile"]["size"] == 0) {
             $filedata['status'] = FILE_INVALID;
             $filedata['reason'] = "File size can't be 0.";
        }else if($this->IsFileCorrupted($_FILES["myfile"]["tmp_name"])){
        	$filedata['status'] = FILE_INVALID;
        	$filedata['reason'] = "File is corrupted.";
        }else if(!$this->isValidEmpId($matches[1][0])) {
             $filedata['status'] = FILE_INVALID;
             $filedata['reason'] = "Employee Id invalid.";
        }else{


            $employeeId = $matches[1][0];
            $namelen = strlen($name);
            $filedata['new_name'] = $newName;
            $filedata['without_ext_name'] = $withoutExt;
            $filedata['status'] = FILE_VALID;
            $filedata['reason'] = $newName;



        	if (isset($_POST["doc_id"]) && $_POST["doc_id"] != '') {
        		move_uploaded_file($_FILES["myfile"]["tmp_name"],EMP_PAYSLIP_UPLOAD_PATH.$newName);
        	} else {
        		move_uploaded_file($_FILES["myfile"]["tmp_name"],EMP_PAYSLIP_TMP_UPLOAD_PATH.$newName);
        	}


        }


        $uploadLog['filename'] = $filedata['original_name'];

        if($filedata['status'] == FILE_INVALID){
        	$uploadLog['status']= 1;
        	$uploadLog['reason']= $filedata['reason'];
        }else{
        	$uploadLog['status']= 2;
        }
        $_SESSION['upload_log_array'][$filedata['original_name']]= $uploadLog;
        $this->_helper->json(array('filedata' => $filedata));
    }

    public function uploaddeleteAction()
    {
    	if(isset($_POST["op"]) && $_POST["op"] == "delete" && isset($_POST['doc_new_name']))
    	{
    		$filePath = "";
    		$filePath = EMP_PAYSLIP_TMP_UPLOAD_PATH.$_POST['doc_new_name'];

    		// Remove payslip files from upload folder.
    			if (file_exists($filePath)) {
    				unlink($filePath);
    			}

    			$this->_helper->json(array());
    	}
    }

	public function saveAction()
	{
		$auth = Zend_Auth::getInstance();
     	if($auth->hasIdentity()){
			$loginUserId = $auth->getStorage()->read()->id;
		}
		$empPayslipModel = new Default_Model_Uploadpayslip();
		$employee = new Default_Model_Employee();

		$id = $this->getRequest()->getParam('id');
		$userid = $this->getRequest()->getParam('userid');
		$filenames = explode("#@#", $this->getRequest()->getParam('filenames'));
		$file_original_name = $filenames[0];
		$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file_original_name);
		$file_new_name =$filenames[1];
		$matches = $empPayslipModel->isFileNameValid($withoutExt);

		$employeeId = $matches[1][0];
		$month = $matches[3][0];
		$month = $empPayslipModel->changeMonthToFullName($month);
		$year = $matches[4][0];
 		$useridArray = $employee->getUserIdByEmpId($employeeId);

		$userid = $useridArray[0]['user_id'];


		try {
			if ($file_new_name != '')
			{
				copy(EMP_PAYSLIP_TMP_UPLOAD_PATH.$file_new_name, EMP_PAYSLIP_UPLOAD_PATH.$file_new_name);
				unlink(EMP_PAYSLIP_TMP_UPLOAD_PATH.$file_new_name);

				$fileLocation = EMP_PAYSLIP_UPLOAD_PATH . $file_new_name;
				$fp = fopen($fileLocation, 'r');
				$payslipcontent = fread($fp, filesize($fileLocation));
				fclose($fp);

				$empPayslip = $empPayslipModel->checkPayslipNameByUserIdAndPayslipId($userid, $month, $year);
				$count_emp_payslip = count($empPayslip);

				$data = array(
						'user_id' => $userid,
						'file_name' => $withoutExt,
						'payslip' => $payslipcontent,
						'payslip_month'=> $month,
						'payslip_year'=>$year,
						'modified_date'=>gmdate("Y-m-d H:i:s")
				);
				if($count_emp_payslip != 0){
					$where = array('user_id=?'=>$userid, 'payslip_month=?'=>$month, 'payslip_year=?'=>$year);
					$actionflag = 2;
				}
				else
				{
					$data['created_date'] = gmdate("Y-m-d H:i:s");
					$where = '';
					$actionflag = 1;
				}
				$recordId = $empPayslipModel->SaveorUpdateEmpPayslip($data, $where);
				$menuID = EMPLOYEE;
				$result = sapp_Global::logManager($menuID,$actionflag,$loginUserId,$userid);
				unlink(EMP_PAYSLIP_UPLOAD_PATH.$file_new_name);
				 $_SESSION['upload_log_array'][$file_original_name]['status']= 4;
				$this->_helper->json(array('result' => 'success','id'=>$employeeId.'_'.$month.'_'.$year));
			}
		} catch (Exception $e) {

			 $_SESSION['upload_log_array'][$file_original_name]['status']= 3;
			 $_SESSION['upload_log_array'][$file_original_name]['reason']= $e->getMessage();
			$this->_helper->json(array('result' => 'error','reason'=>$e->getMessage(),'id'=>$employeeId.'_'.$month.'_'.$year));
		}


	}

	public function IsFileCorrupted($filename){
		$file = file($filename);
		$endfile= $file[count($file) - 1];
		$actualVal = trim($endfile);
		$expectedVal = "%%EOF";
		if(strcmp($actualVal,$expectedVal) != 0){
			return true;
		}
		return false;
	}

	public function saveuploadlogsAction(){

		$logUploadPayslipModel = new Default_Model_LogUploadPayslip();
		$lastUploadId = $logUploadPayslipModel->getLastuploadId();
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$loginUserId = $auth->getStorage()->read()->id;
		}
		foreach ( $_SESSION['upload_log_array'] as $filename => $logData ){

			$logData['uploaded_by']= $loginUserId;
			$logData['upload_id'] = $lastUploadId['upload_id'] == null ? 1 : ($lastUploadId['upload_id']+1);
			$logData['logged_at'] = gmdate("Y-m-d H:i:s");
			$logUploadPayslipModel->SaveorUpdatelogs($logData, '');
		}

	}


}