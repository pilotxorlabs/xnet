<?php
/*********************************************************************************
 *  This file is part of Sentrifugo.
 *  Copyright (C) 2015 Sapplica
 *
 *  Sentrifugo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Sentrifugo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Sentrifugo.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Sentrifugo Support <support@sentrifugo.com>
 ********************************************************************************/

class Default_DetailedattendsummaryController extends Zend_Controller_Action
{
	private $options;
	public function preDispatch()
	{
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
	}
	
	public function init()
	{
		$this->_options= $this->getInvokeArg('bootstrap')->getOptions();
	
	}
	
	public function indexAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$loginUserId = $auth->getStorage()->read()->id;
			$loginUserdepartment_id = $auth->getStorage()->read()->department_id;
		}

		$detailedattendsummaryform = new Default_Form_detailedattendsummary();
		$detailedattendsummaryform->setAttrib('action',BASE_URL.'detailedattendsummary');
		$detailedattendsummarymodel = new Default_Model_Detailedattendsummary();
		
		$this->view->form = $detailedattendsummaryform;
		
		if($this->_request->getParams()["download"] != null){
			$this->getAttendDetailed();
		}
				
	}	
	
	public function getAttendDetailed()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$loginUserId = $auth->getStorage()->read()->id;
			$loginUserdepartment_id = $auth->getStorage()->read()->department_id;
		}

		$fromDate = str_replace('-', '/', date("m-d-Y", strtotime($this->_getParam('start_date'))));
		$toDate = str_replace('-', '/', date("m-d-Y", strtotime($this->_getParam('end_date'))));
				$detailedattendsummaryform = new Default_Form_detailedattendsummary();
		$detailedattendsummaryform->setAttrib('action',BASE_URL.'detailedattendsummary');
		$detailedattendsummarymodel = new Default_Model_Detailedattendsummary();
		$isUserWorking = false;
		$detailedattendsummarymodel->getDetailedAttend($fromDate, $toDate);
	}
	
}