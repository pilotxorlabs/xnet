<?php
class Default_MyattendanceController extends Zend_Controller_Action {
	private $options;
	public function preDispatch() {
		$ajaxContext = $this->_helper->getHelper ( 'AjaxContext' );
		$ajaxContext->addActionContext ( 'getempattendance', 'html' )->initContext ();
	}
	public function init() {
		$this->_options = $this->getInvokeArg ( 'bootstrap' )->getOptions ();
	}
	public function indexAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
			$businessunitId = $auth->getStorage()->read()->businessunit_id;
		}
		
		$myattendancemodel = new Default_Model_Myattendance ();
		$employeeExceptionModel = new Default_Model_Exceptionrequest ();
		$employeeLeaveTypesModel = new Default_Model_Employeeleavetypes ();
		$leaveReqestModel = new Default_Model_Leaverequest ();
		$empWorkActivityModel = new Default_Model_Employeeworkactivity ();
		$officeHourModel = new Default_Model_OfficeHours();
		$isUserWorking = false;
		$auditStart = $myattendancemodel->getAuditStartDate ();
		$auditEnd = $myattendancemodel->getAuditEndDate ();
		$leaveTypeData = $employeeLeaveTypesModel->getEmployeeLeaveTypeByCode ( 'WFH' );
		$activities = array();
		
		//$wfhCount = false;
		if (sizeof($leaveTypeData) != '0')
		{
			$employeeStatus = ($myattendancemodel->getEmployeeStatus($loginUserId)['0']['emp_status_name']);
			if ($employeeStatus != 'Probationary')
			{
				
				$wfhCount = $leaveReqestModel->getUsedLeavesByTypeWithin ( $loginUserId, $leaveTypeData [0] ['leavetype_id'], new DateTime (), new DateTime () );
				$yesterdayWFH = $leaveReqestModel->getUsedLeavesByTypeWithin ( $loginUserId, $leaveTypeData [0] ['leavetype_id'], new DateTime (date('Y-m-d', strtotime('-1 day', strtotime(date("Y-m-d H:i:s"))))), new DateTime (date('Y-m-d', strtotime('-1 day', strtotime(date("Y-m-d H:i:s"))))) );
			}
				
			else
			{
				$wfhCount = $leaveReqestModel->getApprovedLeavesByTypeWithin ( $loginUserId, $leaveTypeData [0] ['leavetype_id'], new DateTime (), new DateTime () );
				$yesterdayWFH = $leaveReqestModel->getApprovedLeavesByTypeWithin ( $loginUserId, $leaveTypeData [0] ['leavetype_id'], new DateTime (date('Y-m-d', strtotime('-1 day', strtotime(date("Y-m-d H:i:s"))))), new DateTime (date('Y-m-d', strtotime('-1 day', strtotime(date("Y-m-d H:i:s"))))) );
			}
			$max = $myattendancemodel->calculateIdealTime ( $loginUserId, $leaveTypeData [0] ['leavetype_id'] );
			$wfhCount = ($wfhCount != 0 ? true : false);
		}

		//check in button availability as per office hour configuration rule
		if ($businessunitId != null)
			$officeHourule = $officeHourModel->getOfficeHoursRule($businessunitId);
		if (($officeHourule['work_culture'] == '2') && ($wfhCount == true))
		{
			$startTime = $officeHourule['start_time'];
			$endTime = $officeHourule['end_time'];
			$todayStartTime = strtotime($startTime);
			if (strtotime($endTime) < strtotime($startTime))
				$todayEndTime = strtotime('+1 day', strtotime($endTime));
			else
				$todayEndTime = strtotime($endTime);
			if (!(strtotime(date("Y-m-d H:i:s")) > $todayStartTime && strtotime(date("Y-m-d H:i:s")) < $todayEndTime))
				$wfhCount = false;
		}
		elseif(($yesterdayWFH != null) && ($wfhCount == false) && ($officeHourule['work_culture'] == '2'))
		{
			$startTime = $officeHourule['start_time'];
			$endTime = $officeHourule['end_time'];
			$yesterdayStartTime = strtotime('-1 day', strtotime($startTime));
			$yesterdayEndTime = strtotime('-1 day', strtotime($endTime));
			if ($yesterdayEndTime < $yesterdayStartTime)
				$yesterdayEndTime = strtotime($endTime);
			if ((strtotime(date("Y-m-d H:i:s")) > $yesterdayStartTime && strtotime(date("Y-m-d H:i:s")) < $yesterdayEndTime))
				$wfhCount = true;
		}

		$activities = $empWorkActivityModel->getWFHActivitiesOn ( $loginUserId, new DateTime () );
		if($yesterdayWFH != null)
			$activities = $empWorkActivityModel->getWFHActivitiesOn ( $loginUserId, (new DateTime ())->modify('-1 day') );
		$now = $myattendancemodel->calculateCurrentWeekLoggedHours ( $loginUserId, $isUserWorking, $currentDayHours, $auditStart, $auditEnd, $employeeExceptionModel );
		$data ['maxValue'] = $max;
		$data ['nowValue'] = $now;
		$data ['remainingDays'] = $myattendancemodel->getRemainingDays ();
		$data ['remainingHours'] = $myattendancemodel->getRemainingHours ( $max, $now, $currentDayHours );
		$data ['auditStartDate'] = $auditStart;
		$data ['auditEndDate'] = $auditEnd;
		$data ['isUserWorking'] = $isUserWorking;
		$data ['isWorkingFromHome'] = $wfhCount;
		$data ['isCheckin'] = (count ( $activities ) % 2) == 0 ? true : false;
		$this->view->data = $data;
	}
	public function getempattendanceAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		$myattendancemodel = new Default_Model_Myattendance ();
		$employeeExceptionModel = new Default_Model_Exceptionrequest ();
		if ($this->_getParam ( 'id' ) != NULL) {
			$id = $this->_getParam ( 'id' );
		} else {
			$id = $loginUserId;
		}
		$start = date ( DATEFORMAT_PHP, strtotime ( $this->_getParam ( 'from_date' ) ) );
		$end = date ( DATEFORMAT_PHP, strtotime ( $this->_getParam ( 'to_date' ) ) );
		$activityBreakdown = false;
		if ($this->_getParam ( 'showbreakdown' ) != NULL) {
			$activityBreakdown = $this->_getParam ( 'showbreakdown' );
		}
		
		$data ['empAttendance'] = $myattendancemodel->getEmployeeAttendence ( $id, $start, $end, $employeeExceptionModel, $activityBreakdown );
		$this->view->data = $data;
	}
	public function checkinAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		
		$empWorkActivityModel = new Default_Model_Employeeworkactivity ();
		$data = array (
				'user_id' => $loginUserId,
				'punch_time' => gmdate ( "Y-m-d H:i:s" ),
				'status' => 1 
		);
		$empWorkActivityModel->saveOrupdateData ( $data, '' );
		$data ['status'] = '(' . WFH . ') ' . ABSENT;
		$this->_helper->json ( json_encode ( $data ) );
	}
	public function getdailyactivityAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		
		$activityBreakdown = false;
		if ($this->_getParam ( 'showbreakdown' ) != NULL) {
			$activityBreakdown = $this->_getParam ( 'showbreakdown' );
		}
		
		$myattendancemodel = new Default_Model_Myattendance ();
        $empWorkActivityModel = new Default_Model_Employeeworkactivity ();
		if ($this->_getParam ( 'id' ) != NULL) {
			$id = $this->_getParam ( 'id' );
		} else {
			$id = $loginUserId;
		}
		
		$start = date ( DATEFORMAT_PHP, strtotime ( $this->_getParam ( 'from_date' ) ) );
		$end = date ( DATEFORMAT_PHP, strtotime ( $this->_getParam ( 'to_date' ) ) );
		$startDate = new DateTime ( $start );
		$endDate = new DateTime ( $end );
		if ($endDate > new DateTime ())
			$endDate = new DateTime ();
		$dailyPunchDetails = array ();
		while ( $startDate <= $endDate ) {
            $workhours = 0;
            $dailyActivity = $empWorkActivityModel->getEmployeeActivities ( $startDate, $id, $workhours, $activityBreakdown );
			$dailyPunchDetails [$startDate->format ( "Y-m-d" )] ['activities'] = $dailyActivity;
            if (count ( $dailyActivity ) == 3 && $dailyActivity [count ( $dailyActivity ) - 2] ['status'] == "Missed Punch") {
                $dailyPunchDetails [$startDate->format("Y-m-d")] ['workhours'] = $myattendancemodel->checkformat(HOURSPERDAY);
            } else {
                $dailyPunchDetails [$startDate->format("Y-m-d")] ['workhours'] = $myattendancemodel->checkformat($workhours);
            }
			$startDate->modify ( '+1 day' );
		}
		$this->_helper->json ( json_encode ( $dailyPunchDetails ) );
	}
}

