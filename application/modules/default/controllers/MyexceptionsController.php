<?php
class Default_MyexceptionsController extends Zend_Controller_Action {
	public function init() {
		$this->_options = $this->getInvokeArg ( 'bootstrap' )->getOptions ();
	}
	public function indexAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		
		$call = $this->_getParam ( 'call' );
		if ($call == 'ajaxcall')
			$this->_helper->layout->disableLayout ();
		$view = Zend_Layout::getMvcInstance ()->getView ();
		$exceptionrequestmodel = new Default_Model_ExceptionRequest ();
		
		$objname = $this->_getParam ( 'objname' );
		$refresh = $this->_getParam ( 'refresh' );
		$dashboardcall = $this->_getParam ( 'dashboardcall' );
		$flag = $this->_request->getParam ( 'flag' );
		if (! empty ( $flag ) && $flag == 'delete') {
			$this->deleteAction ();
		} else {
			$data = array ();
			$searchQuery = '';
			$searchArray = array ();
			$tablecontent = '';
			
			if ($refresh == 'refresh') {
				if ($dashboardcall == 'Yes')
					$perPage = DASHBOARD_PERPAGE;
				else
					$perPage = PERPAGE;
				
				$sort = 'DESC';
				$by = 'modifieddate';
				$pageNo = 1;
				$searchData = '';
			} else {
				$sort = ($this->_getParam ( 'sort' ) != '') ? $this->_getParam ( 'sort' ) : 'DESC';
				$by = ($this->_getParam ( 'by' ) != '') ? $this->_getParam ( 'by' ) : 'modifieddate';
				if ($dashboardcall == 'Yes')
					$perPage = $this->_getParam ( 'per_page', DASHBOARD_PERPAGE );
				else
					$perPage = $this->_getParam ( 'per_page', PERPAGE );
				$pageNo = $this->_getParam ( 'page', 1 );
				// search from grid - START
				$searchData = $this->_getParam ( 'searchData' );
				$searchData = rtrim ( $searchData, ',' );
				// search from grid - END
			}
			$exceptionsArray = array (
					'pendingexceptions',
					'cancelledexceptions',
					'approvedexceptions',
					'rejectedexceptions',
					'expiredexceptions' 
			);
			$objName = 'myexceptions';
			$queryflag = 'all';
			if (! empty ( $flag )) {
				if (in_array ( $flag, $exceptionsArray )) {
					$queryflag = substr ( $flag, 0, - 10 );
				}
				if ($flag == 'total') {
					$queryflag = 'all';
				}
			}
			try {
				$dataTmp = $exceptionrequestmodel->getGrid ( $sort, $by, $perPage, $pageNo, $searchData, $call, $dashboardcall, $objName, $queryflag, '', '' );
			} catch ( Exception $e ) {
				echo $e;
			}
			
			$exceptionsCountArray = sapp_Helper::getExceptionsCountByCategory ( $loginUserId );
			
			array_push ( $data, $dataTmp );
			$this->view->dataArray = $data;
			$this->view->call = $call;
			$this->view->objName = 'all';
			$this->view->flag = $flag;
			$this->view->exceptionsCountArray = $exceptionsCountArray;
			$this->view->messages = $this->_helper->flashMessenger->getMessages ();
		}
	}
	public function deleteAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
			$loginUserEmail = $auth->getStorage ()->read ()->emailaddress;
			$loginUserName = $auth->getStorage ()->read ()->userfullname;
		}
		$id = $this->_request->getParam ( 'objid' );
		$messages ['message'] = '';
		$actionflag = 5;
		$businessunitid = '';
		if ($id) {
			$exceptionrequestmodel = new Default_Model_ExceptionRequest ();
			$usersmodel = new Default_Model_Users ();
			$employeesmodel = new Default_Model_Employees ();
			
			$data = $exceptionrequestmodel->getsingleExceptionData ( $id );
			$data = $data [0];
			if ($data ['exceptionstatus'] == 'Approved') {
				if (isset ( $data ['from_date'] )) {
					$exceptionDate = date ( $data ['from_date'] );
					$todayDate = date ( DATEFORMAT_PHP );
					if (strtotime ( $todayDate ) >= strtotime ( $exceptionDate )) {
						$messages ['message'] = 'Exception request cannot be cancelled';
						$messages ['msgtype'] = 'error';
						$this->_helper->json ( $messages );
						return false;
					}
				}
			}
			if ($data ['exceptionstatus'] == 'Rejected' || $data ['exceptionstatus'] == 'Cancelled' || $data ['exceptionstatus'] == 'Expired' || $loginUserId != $data ['user_id']) {
				$messages ['message'] = 'Exception request cannot be cancelled';
				$messages ['msgtype'] = 'error';
				$this->_helper->json ( $messages );
				return false;
			}
			
			$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );
			if ($loggedInEmployeeDetails [0] ['businessunit_id'] != '')
				$businessunitid = $loggedInEmployeeDetails [0] ['businessunit_id'];
			
			$dataarr = array (
					'exceptionstatus' => 4,
					'modifieddate' => gmdate ( "Y-m-d H:i:s" ),
					'modifiedby' => $loginUserId 
			);
			$where = array (
					'id=?' => $id 
			);
			
			$Id = $exceptionrequestmodel->SaveorUpdateExceptionRequest ( $dataarr, $where );
			// saving in exceptionrequest history table
			$history = 'Exception Request has been Cancelled by ';
			
			$exceptionrequesthistory_model = new Default_Model_Exceptionrequesthistory ();
			$exception_history = array (
					'exceptionrequest_id' => $id,
					'description' => $history,
					'createdby' => $loginUserId,
					'modifiedby' => $loginUserId,
					'isactive' => 1,
					'createddate' => gmdate ( "Y-m-d H:i:s" ),
					'modifieddate' => gmdate ( "Y-m-d H:i:s" ) 
			);
			$where = '';
			$exceptionhistory = $exceptionrequesthistory_model->saveOrUpdateExceptionRequestHistory ( $exception_history, $where );
			$appliedexceptionsdaycount = $data ['appliedexceptioncount'];
			$to_date = $data ['to_date'];
			$from_date = $data ['from_date'];
			$reason = $data ['reason'];
			$repmngrnameArr = $usersmodel->getUserDetailsByID ( $data ['rep_mang_id'] );
			$reportingmanageremail = $repmngrnameArr [0] ['emailaddress'];
			$reportingmanagername = $repmngrnameArr [0] ['userfullname'];
			if ($Id == 'update') {
				// $menuID = MYEXCEPTIONS;
				// $result = sapp_Global::logManager($menuID,$actionflag,$loginUserId,$id);
				/**
				 * MAILING CODE *
				 */
				
				if ($to_date == '' || $to_date == NULL)
					$to_date = $from_date;
					/* Mail to Employee */
				$options ['subject'] = 'Exception request cancelled';
				$options ['header'] = 'Exception Request';
				$options ['toEmail'] = $loginUserEmail;
				$options ['toName'] = $loginUserName;
				$options ['message'] = '<div>
				<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
					<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
				<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
								<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below exception(s) has been cancelled.</div>
								</div>
								<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody><tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB;color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $loginUserName . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">' . $appliedexceptionsdaycount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
            	     </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Exception</td>
                        <td style="color:#5b5b5b">' . $reason . '</td>
                  </tr>
                </tbody></table>

            </div>
            <div style="padding:20px 0 10px 0;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the exception details.</div>';
				$result = sapp_Global::_sendEmail ( $options );
				/* End */
				
				/* Mail to Reporting Manager */
				$options ['subject'] = 'Exception request cancelled';
				$options ['header'] = 'Exception Request';
				$options ['toEmail'] = $reportingmanageremail;
				$options ['toName'] = $reportingmanagername;
				$options ['message'] = '
				<div>
										<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
											<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
											<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
								<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below exception(s) has been cancelled.</div>
								</div>
								<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody><tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB;color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $loginUserName . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">' . $appliedexceptionsdaycount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
            	     		</tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Exception</td>
                        <td style="color:#5b5b5b">' . $reason . '</td>
                  </tr>
                </tbody></table>

            </div>
            <div style="padding:20px 0 10px 0;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the exception details.</div>';
				$result = sapp_Global::_sendEmail ( $options );
				/* End */
				
				/* Mail to HR */
				if (defined ( 'LV_HR_' . $businessunitid ) && $businessunitid != '') {
					
					$options ['subject'] = 'Exception request cancelled';
					$options ['header'] = 'Exception Request';
					$options ['toEmail'] = constant ( 'LV_HR_' . $businessunitid );
					$options ['toName'] = 'Exception management';
					$options ['message'] = '
					<div>
										<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
											<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
											<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
								<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below exception(s) has been cancelled by the Employee.</div>
								</div>
								<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody><tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB;color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $loginUserName . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">' . $appliedexceptionsdaycount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
            	     </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Exception</td>
                        <td style="color:#5b5b5b">' . $reason . '</td>
                  </tr>
                </tbody></table>

            </div>
            <div style="padding:20px 0 10px 0;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the exception details.</div>';
					$options ['cron'] = 'yes';
					$result = sapp_Global::_sendEmail ( $options );
				}
				
				$messages ['message'] = 'Exception request cancelled succesfully';
				$messages ['msgtype'] = 'success';
			} else {
				$messages ['message'] = 'Exception request cannot be cancelled';
				$messages ['msgtype'] = 'error';
			}
		} else {
			$messages ['message'] = 'Exception request cannot be cancelled';
			$messages ['msgtype'] = 'error';
		}
		$this->_helper->json ( $messages );
	}
}