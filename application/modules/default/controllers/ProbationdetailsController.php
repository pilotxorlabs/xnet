<?php
class Default_ProbationdetailsController extends Zend_Controller_Action
{
	private $options;

	public function preDispatch()
	{

	}

	public function init()
	{
		$this->_options= $this->getInvokeArg('bootstrap')->getOptions();
	}
	public function indexAction()
	{
		if(defined('EMPTABCONFIGS'))
		{
			$empOrganizationTabs = explode(",",EMPTABCONFIGS);

		 if(in_array('probation_details',$empOrganizationTabs)){
		 	$userID="";$objName = 'probationdetails';
		 	$auth = Zend_Auth::getInstance();
		 	if($auth->hasIdentity()){
					$loginUserId = $auth->getStorage()->read()->id;
					$loginUserRole = $auth->getStorage()->read()->emprole;
					$loginUserGroup = $auth->getStorage()->read()->group_id;
		 	}

		 	$userid = $this->getRequest()->getParam('userid');//This is User_id taking from URL
		 	$conText = "";
		 	$call = $this->_getParam('call');
		 	if($call == 'ajaxcall')
		 	{
		 		$this->_helper->layout->disableLayout();
		 		$userID = ($this->_getParam('unitId') !='')? $this->_getParam('unitId'):$this->_getParam('userid');
		 		$conText = ($this->_getParam('context') !='')? $this->_getParam('context'):$this->getRequest()->getParam('context');
		 	}

		 	if($userid == '') $userid =$userID;
		 	$Uid = ($userid)?$userid:$userID;//die;

		 	//Check for this user id record exists or not....
		 	$probationdetailsModel = new Default_Model_Probationdetails();
		 	$employeeModal = new Default_Model_Employee();
		 	try
		 	{
			    if($Uid && is_numeric($Uid) && $Uid>0)
				{
					$usersModel = new Default_Model_Users();
					$empdata = $employeeModal->getActiveEmployeeData($Uid);
					$employeeData = $usersModel->getUserDetailsByIDandFlag($Uid);
					if($empdata == 'norows')
					{
						$this->view->rowexist = "norows";
						$this->view->empdata = "";
					}
					else
					{
						$this->view->rowexist = "rows";
						if(!empty($empdata))
						{
							$view = Zend_Layout::getMvcInstance()->getView();
							$objname = $this->_getParam('objname');
							$refresh = $this->_getParam('refresh');
							$dashboardcall = $this->_getParam('dashboardcall',null);
							$data = array();$searchQuery = '';$searchArray = array();	$tablecontent = '';

							if($refresh == 'refresh')
							{
								if($dashboardcall == 'Yes')
								$perPage = DASHBOARD_PERPAGE;
								else
								$perPage = PERPAGE;
								$sort = 'DESC';$by = 'id';$pageNo = 1;$searchData = '';	
								$searchQuery = '';$searchArray='';
							}
							else
							{
								$sort = ($this->_getParam('sort') !='')? $this->_getParam('sort'):'DESC';
								$by = ($this->_getParam('by')!='')? $this->_getParam('by'):'id';
								if($dashboardcall == 'Yes')
								$perPage = $this->_getParam('per_page',DASHBOARD_PERPAGE);
								else
								$perPage = $this->_getParam('per_page',PERPAGE);

								$pageNo = $this->_getParam('page', 1);
								$searchData = $this->_getParam('searchData');
							}

							$dataTmp = $probationdetailsModel->getGrid($sort, $by, $pageNo, $perPage,$searchData,$call,$dashboardcall,$Uid,$conText);

							array_push($data,$dataTmp);
							$this->view->id=$userid;	
							$this->view->controllername = $objName;
							$this->view->dataArray = $data;
							$this->view->call = $call ;
							$this->view->employeedata = $employeeData[0];
							$this->view->messages = $this->_helper->flashMessenger->getMessages();
						}
						$this->view->empdata = $empdata;
					}
				}	
				else
				{
				   $this->view->rowexist = "norows";
				}	
		 	}
		 	catch(Exception $e)
		 	{
		 		$this->view->rowexist = "norows";
		 	}
		 }else{
		 	$this->_redirect('error');
		 }
		}else{
			$this->_redirect('error');
		}
	}

	public function editAction()
	{
		if(defined('EMPTABCONFIGS'))
		{
			$empOrganizationTabs = explode(",",EMPTABCONFIGS);

		 if(in_array('probation_details',$empOrganizationTabs)){
		 	$userID="";$conText = "";$objName = 'probationdetails';
		 	$auth = Zend_Auth::getInstance();
		 	if($auth->hasIdentity()){
		 		$loginUserId = $auth->getStorage()->read()->id;
		 		$loginUserRole = $auth->getStorage()->read()->emprole;
		 		$loginUserGroup = $auth->getStorage()->read()->group_id;
		 	}
		 	$userid = $this->getRequest()->getParam('userid');
		 	 
		 	$call = $this->_getParam('call');
		 	if($call == 'ajaxcall')
		 	{
		 		$this->_helper->layout->disableLayout();
		 		$userID = ($this->_getParam('unitId') !='')? $this->_getParam('unitId'):$this->_getParam('userid');
		 		$conText = ($this->_getParam('context') !='')? $this->_getParam('context'):$this->getRequest()->getParam('context');
		 	}

		 	if($userid == '') $userid =$userID;
		 	$Uid = ($userid)?$userid:$userID;

		 	//Check for this user id record exists or not....
		 	$probationdetailsModel = new Default_Model_Probationdetails();
		 	$employeeModal = new Default_Model_Employee();

		 	try
		 	{
			    if($Uid && is_numeric($Uid) && $Uid>0 && $Uid!=$loginUserId)
				{
					$usersModel = new Default_Model_Users();
					$empdata = $employeeModal->getActiveEmployeeData($Uid);
					$employeeData = $usersModel->getUserDetailsByIDandFlag($Uid);
					if($empdata == 'norows')
					{
						$this->view->rowexist = "norows";
						$this->view->empdata = "";
					}
					else
					{
						$this->view->rowexist = "rows";
						if(!empty($empdata))
						{
							$view = Zend_Layout::getMvcInstance()->getView();
							$objname = $this->_getParam('objname');
							$refresh = $this->_getParam('refresh');
							$dashboardcall = $this->_getParam('dashboardcall',null);
							$data = array();$searchQuery = '';$searchArray = array();	$tablecontent = '';

							if($refresh == 'refresh')
							{
								if($dashboardcall == 'Yes')
								$perPage = DASHBOARD_PERPAGE;
								else
								$perPage = PERPAGE;
								$sort = 'DESC';$by = 'modifieddate';$pageNo = 1;$searchData = '';					
								$searchQuery = '';$searchArray='';
							}
							else
							{
								$sort = ($this->_getParam('sort') !='')? $this->_getParam('sort'):'DESC';
								$by = ($this->_getParam('by')!='')? $this->_getParam('by'):'id';
								if($dashboardcall == 'Yes')
								$perPage = $this->_getParam('per_page',DASHBOARD_PERPAGE);
								else
								$perPage = $this->_getParam('per_page',PERPAGE);

								$pageNo = $this->_getParam('page', 1);
								$searchData = $this->_getParam('searchData');
								$searchData = rtrim($searchData,',');
							}
							$dataTmp = $probationdetailsModel->getGrid($sort, $by, $pageNo, $perPage,$searchQuery,$call,$dashboardcall,$Uid,$conText);
							array_push($data,$dataTmp);
							$this->view->id=$userid;	
							$this->view->controllername = $objName;
							$this->view->dataArray = $data;
							$this->view->call = $call ;
							$this->view->employeedata = $employeeData[0];
							$this->view->messages = $this->_helper->flashMessenger->getMessages();
						}
						$this->view->empdata = $empdata;
				    }
				}
				else
				{
				   $this->view->rowexist = "norows";
				}
		 	}
		 	catch(Exception $e)
		 	{
		 		$this->view->rowexist = "norows";
		 	}
		 }else{
		 	$this->_redirect('error');
		 }
		}else{
			$this->_redirect('error');
		}
	}

	public function viewAction()
	{
		if(defined('EMPTABCONFIGS'))
		{
			$empOrganizationTabs = explode(",",EMPTABCONFIGS);

			if(in_array('probation_details',$empOrganizationTabs)){
			    $auth = Zend_Auth::getInstance();
			    if($auth->hasIdentity()){
		 		$loginUserId = $auth->getStorage()->read()->id;
		 		$loginUserRole = $auth->getStorage()->read()->emprole;
		 		$loginUserGroup = $auth->getStorage()->read()->group_id;
		 	    }
				$objName = 'probationdetails';$conText="";
				$userid = $this->getRequest()->getParam('userid');	
				$call = $this->_getParam('call');
				if($call == 'ajaxcall')
				{
					$this->_helper->layout->disableLayout();
					$userID = ($this->_getParam('unitId') !='')? $this->_getParam('unitId'):$this->_getParam('userid');
					$conText = ($this->_getParam('context') !='')? $this->_getParam('context'):$this->getRequest()->getParam('context');
				}
				if($userid == '') $userid =$userID;
				$Uid = ($userid)?$userid:$userID;
				$probationdetailsModel = new Default_Model_Probationdetails();
				$employeeModal = new Default_Model_Employee();
				try
				{
					if($Uid && is_numeric($Uid) && $Uid>0 && $Uid!=$loginUserId)
					{
						$usersModel = new Default_Model_Users();
						$empdata = $employeeModal->getActiveEmployeeData($Uid);
						$employeeData = $usersModel->getUserDetailsByIDandFlag($Uid);
						if($empdata == 'norows')
						{
							$this->view->rowexist = "norows";
							$this->view->empdata = "";
						}
						else
						{
							$this->view->rowexist = "rows";
							if(!empty($empdata))
							{
								$view = Zend_Layout::getMvcInstance()->getView();
								$objname = $this->_getParam('objname');
								$refresh = $this->_getParam('refresh');
								$dashboardcall = $this->_getParam('dashboardcall',null);
								$data = array();$searchQuery = '';$searchArray = array();	$tablecontent = '';

								if($refresh == 'refresh')
								{
									if($dashboardcall == 'Yes')
									$perPage = DASHBOARD_PERPAGE;
									else
									$perPage = PERPAGE;

									$sort = 'DESC';$by = 'modifieddate';$pageNo = 1;$searchData = '';
									$searchQuery = '';$searchArray='';

								}
								else
								{
									$sort = ($this->_getParam('sort') !='')? $this->_getParam('sort'):'DESC';
									$by = ($this->_getParam('by')!='')? $this->_getParam('by'):'id';
									if($dashboardcall == 'Yes')
									$perPage = $this->_getParam('per_page',DASHBOARD_PERPAGE);
									else
									$perPage = $this->_getParam('per_page',PERPAGE);

									$pageNo = $this->_getParam('page', 1);
									$searchData = $this->_getParam('searchData');
									$searchData = rtrim($searchData,',');
								}

								$dataTmp = $probationdetailsModel->getGrid($sort, $by, $pageNo, $perPage,$searchData,$call,$dashboardcall,$Uid,$conText);

								array_push($data,$dataTmp);
								$this->view->id=$userid;	//User_id sending to view for tabs navigation....
								$this->view->controllername = $objName;
								$this->view->dataArray = $data;
								$this->view->call = $call ;
								$this->view->employeedata = $employeeData[0];
								$this->view->messages = $this->_helper->flashMessenger->getMessages();
							}
							$this->view->empdata = $empdata;
						}
					}	
					else
					{
					   $this->view->rowexist = "norows";
					}
				}
				catch(Exception $e)
				{
					$this->view->rowexist = "norows";
				}
		  }else{
		 	 $this->_redirect('error');
		  }
		}else{
			$this->_redirect('error');
		}
	}

	public function addpopupAction()
	{
		$msgarray = array();
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
		{
			$loginUserId = $auth->getStorage()->read()->id;
		}
		$userId = $this->getRequest()->getParam('unitId');
		$usersModel = new Default_Model_Users();
		$employeeModel = new Default_Model_Employee();
		$doj = $employeeModel->getDOJ($userId);
		$employeeName = $usersModel->getUserFullName($userId);
		$employeeId = $usersModel->getEmployeeId($userId);
		// For open the form in popup...
		Zend_Layout::getMvcInstance()->setLayoutPath(APPLICATION_PATH."/layouts/scripts/popup/");
		$probationDetailsform = new Default_Form_Probationdetails();

		$probationDetailsform->setAttrib('action',BASE_URL.'probationdetails/addpopup/unitId/'.$userId);
		$probationDetailsform->employeeName->setValue ( $employeeName );
		$probationDetailsform->employeeId->setValue ( $employeeId );
		$this->view->doj = $doj;		
		$this->view->msgarray = $msgarray;
		if($this->getRequest()->getPost())
		{
			$result = $this->validatereferrercontact($this->getRequest()->getPost());
			$probationDetailsform->populate($this->getRequest()->getPost());
			if(empty($result))
			{
				$result = $this->save($probationDetailsform,$userId);
			}
			$this->view->msgarray = $result;
		}
		
		$this->view->form = $probationDetailsform;

	}
	public function validatereferrercontact($postParams)
	{
		$msgarray =array();$fieldValues=array();$totArr=array();
		$probationDetailsModel = new Default_Model_Probationdetails();
		if($postParams['start_date'] != '' && $postParams['end_date'] != '')
		{
			$fieldValues['start_date']=$postParams['start_date'];
			$fieldValues['end_date']=$postParams['end_date'];
			if (new DateTime($fieldValues['end_date']) < new DateTime($fieldValues['start_date']))
			{
				$msgarray['start_date'] = 'End date cannot be earlier than start date.';
				$msgarray['end_date'] = 'End date cannot be earlier than start date.';
			}else if($postParams['notify_on'] != null){
			    $msgarray = $probationDetailsModel->ValidateNotificationDate($postParams['notify_on'], $postParams['start_date'], $postParams['end_date']);
			}
			
		}
		
		$totArr['msgarray']=$msgarray;
		$totArr['fieldValues']=$fieldValues;
		return $msgarray;
	}
	
	public function editpopupAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
		{
			$loginUserId = $auth->getStorage()->read()->id;
		}
		//For opening the form in pop up.....
		Zend_Layout::getMvcInstance()->setLayoutPath(APPLICATION_PATH."/layouts/scripts/popup/");
		$id = $this->_request->getParam('id');	//Taking Id(Primary key in table) from form....
		$user_id = $this->getRequest()->getParam('unitId');	//This is User_id taking from URL set to form...

		$probationDetailsform = new Default_Form_Probationdetails();
		$probationDetailsModel = new Default_Model_Probationdetails();
		$usersModel = new Default_Model_Users();
		$employeeModel = new Default_Model_Employee();
		$doj = $employeeModel->getDOJ($user_id);
		$employeeName = $usersModel->getUserFullName($user_id);
		$employeeId = $usersModel->getEmployeeId($user_id);
		if($id)
		{
			$data = $probationDetailsModel->getexperiencedetailsRecord($id);
			$probationDetailsform->setDefault("employeeName",$employeeName);
			$probationDetailsform->setDefault("employeeId",$employeeId);
			$probationDetailsform->setDefault("id",$data[0]["id"]);
			$probationDetailsform->setDefault("user_id",$user_id);
			$probationDetailsform->setDefault("start_date",$data[0]["start_date"]);
			$probationDetailsform->setDefault("end_date",$data[0]["end_date"]);
			$probationDetailsform->setDefault("status",$data[0]["status"]);
			$probationDetailsform->setDefault("duration",$data[0]["duration"]);
			$probationDetailsform->setDefault("notify_on",$data[0]["notify_on"]);			
			$probationDetailsform->setDefault("comments",$data[0]["comments"]);
			$probationDetailsform->setAttrib('action',BASE_URL.'probationdetails/editpopup/unitId/'.$user_id);
			$this->view->id=$user_id;
		}
		$probationDetailsform->setAttrib('action',BASE_URL.'probationdetails/editpopup/id/'.$id.'/unitId/'.$user_id);
		$this->view->doj = $doj;
		if($this->getRequest()->getPost())
		{
		    
			$result = $this->validatereferrercontact($this->getRequest()->getPost());
			foreach ($result as $key => $error){
			    $probationDetailsform->setDefault($key,'');
			}
			if(empty($result))
			{
			    $result = $this->save($probationDetailsform,$user_id);
			}
			$this->view->msgarray = $result;
		}
		
		$this->view->form = $probationDetailsform;
	}

	public function save($probationDetailsform,$user_id)
	{
		$result ="";
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
		{
			$loginUserId = $auth->getStorage()->read()->id;
		}
		$date = new Zend_Date();
		if($probationDetailsform->isValid($this->_request->getPost()))
		{
			$probationDetailsModel = new Default_Model_Probationdetails();
			$employeeModal = new Default_Model_Employee ();
			$userData = $employeeModal->getsingleEmployeeData ( $user_id );
			$id = $this->getRequest()->getParam('id');	
			$start_date = $this->_request->getParam('start_date',null);
			$end_date = $this->_request->getParam('end_date',null);			
			$notify_on = $this->_request->getParam('notify_on',null);
			$duration = $this->_request->getParam('duration',null);
			$status = $this->_request->getParam('status');
			$comments = $this->_request->getParam('comments');
			$data = array(  'start_date'=>sapp_Global::change_date($start_date,'database'),
								'end_date'=>sapp_Global::change_date($end_date,'database'),
								'duration'=>$duration,
								'status'=>$status,
								'comments'=>$comments,
								'user_id'=>$user_id,
			                     'notify_on'=>$notify_on
			);
			if($id!='')
			{
				$where = array('id=?'=>$id);
				$actionflag = 2;
			}
			else
			{
				$where = '';
				$actionflag = 1;
			}
			$Id = $probationDetailsModel->SaveorUpdateEmployeeprobationData($data,$where);
			if($Id == 'update')
			{
				$tableid = $id;
				$this->view->successmessage = 'Employee probation details updated successfully.';
			}
			else
			{
				$tableid = $Id;
				$this->view->successmessage = 'Employee probation details added successfully.';
			}
			$menuID = EMPLOYEE;
			$result = sapp_Global::logManager($menuID,$actionflag,$loginUserId,$user_id);
			Zend_Layout::getMvcInstance()->setLayoutPath(APPLICATION_PATH."/layouts/scripts/popup/");
			$close = 'close';
			$this->view->popup=$close;
			$this->view->controllername = 'probationdetails';
		}
		else
		{
			$messages = $probationDetailsform->getMessages();
			foreach ($messages as $key => $val)
			{
				foreach($val as $key2 => $val2)
				{
					$msgarray[$key] = $val2;
					break;
				}
			}
			return $msgarray;
		}

	}
	public function viewpopupAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
		{
			$loginUserId = $auth->getStorage()->read()->id;
		}
		//For opening the form in pop up.....
		Zend_Layout::getMvcInstance()->setLayoutPath(APPLICATION_PATH."/layouts/scripts/popup/");
		$id = $this->_request->getParam('id');	//Taking Id(Primary key in table) from form....
		$user_id = $this->getRequest()->getParam('unitId');	//This is User_id taking from URL set to form...
		$probationDetailsform = new Default_Form_Probationdetails();
		$probationDetailsModel = new Default_Model_Probationdetails();
		$probationDetailsform->removeElement("submit");
		$elements = $probationDetailsform->getElements();
		$usersModel = new Default_Model_Users();
		$employeeName = $usersModel->getUserFullName($user_id);
		$employeeId = $usersModel->getEmployeeId($user_id);
		if(count($elements)>0)
		{
			foreach($elements as $key=>$element)
			{
				if(($key!="Cancel")&&($key!="Edit")&&($key!="Delete")&&($key!="Attachments")){
					$element->setAttrib("disabled", "disabled");
				}
			}
		}
		if($id)
		{
			$data = $probationDetailsModel->getexperiencedetailsRecord($id);
			$probationDetailsform->employeeName->setValue ( $employeeName );
			$probationDetailsform->employeeId->setValue ( $employeeId );
			$probationDetailsform->id->setValue ( $data[0]["id"] );
			$probationDetailsform->user_id->setValue ( $user_id );
			$probationDetailsform->start_date->setValue ( $data[0]["start_date"] );
			$probationDetailsform->end_date->setValue ( $data[0]["end_date"] );
			$probationDetailsform->status->setValue ( $data[0]["status"] );
			$probationDetailsform->comments->setValue ( $data[0]["comments"] );
			$probationDetailsform->notify_on->setValue ( $data[0]["notify_on"] );
			$probationDetailsform->setAttrib('action',BASE_URL.'probationdetails/editpopup/unitId/'.$user_id);
			$this->view->id=$user_id;
		}
		$probationDetailsform->setAttrib('action',BASE_URL.'probationdetails/editpopup/id/'.$id.'/unitId/'.$user_id);
		$this->view->form = $probationDetailsform;
		$this->view->data=$data;
		if($this->getRequest()->getPost())
		{
			$result = $this->save($probationDetailsform,$user_id);
			$this->view->msgarray = $result;
		}

	}
	public function deleteAction()
	{

		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$loginUserId = $auth->getStorage()->read()->id;
		}
		$id = $this->_request->getParam('objid');
		$messages['message'] = '';$messages['msgtype'] = '';$messages['flagtype'] = '';
		$actionflag = 3;
		if($id)
		{
			$probationDetailsModel = new Default_Model_Probationdetails();
			$data = array('isactive'=>0,'modifieddate'=>gmdate("Y-m-d H:i:s"));
			$where = array('id=?'=>$id);
			$Id = $probationDetailsModel->SaveorUpdateEmployeeexperienceData($data, $where);
			if($Id == 'update')
			{
				$menuID = EMPLOYEE;
				$result = sapp_Global::logManager($menuID,$actionflag,$loginUserId,$id);
				$messages['message'] = 'Employee probation details deleted successfully.';
				$messages['msgtype'] = 'success';
			}
			else{
				$messages['message'] = 'Employee probation details  cannot be deleted.';
				$messages['msgtype'] = 'error';
			}
		}
		else
		{
			$messages['message'] = 'Employee probation details cannot be deleted.';
			$messages['msgtype'] = 'error';
		}
		$this->_helper->json($messages);
	}
}

