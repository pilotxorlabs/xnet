<?php
/********************************************************************************* 


 *   

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/

class Default_EmployeeleavetypesController extends Zend_Controller_Action
{

    private $options;
	public function preDispatch()
	{
		 
		
	}
	
    public function init()
    {
        $this->_options= $this->getInvokeArg('bootstrap')->getOptions();
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('addconstraint', 'html')->initContext();
		
    }

    public function indexAction()
    {
		$employeeleavetypesmodel = new Default_Model_Employeeleavetypes();	
        $call = $this->_getParam('call');
		if($call == 'ajaxcall')
				$this->_helper->layout->disableLayout();
		
		$view = Zend_Layout::getMvcInstance()->getView();		
		$objname = $this->_getParam('objname');
		$refresh = $this->_getParam('refresh');
		$dashboardcall = $this->_getParam('dashboardcall',null);
		$data = array();$searchQuery = '';	$searchArray = array();	$tablecontent='';
		
		if($refresh == 'refresh')
		{
			if($dashboardcall == 'Yes')
				$perPage = DASHBOARD_PERPAGE;
			else	
				$perPage = PERPAGE;
										
			$sort = 'DESC';$by = 'e.modifieddate';$pageNo = 1;$searchData = '';$searchQuery = '';	
			$searchArray = array();
		}
		else 
		{
			$sort = ($this->_getParam('sort') !='')? $this->_getParam('sort'):'DESC';
			$by = ($this->_getParam('by')!='')? $this->_getParam('by'):'e.modifieddate';
			if($dashboardcall == 'Yes')
				$perPage = $this->_getParam('per_page',DASHBOARD_PERPAGE);
			else 
				$perPage = $this->_getParam('per_page',PERPAGE);
			
			$pageNo = $this->_getParam('page', 1);
			$searchData = $this->_getParam('searchData');	
			$searchData = rtrim($searchData,',');
		}	
		
		$dataTmp = $employeeleavetypesmodel->getGrid($sort,$by,$perPage,$pageNo,$searchData,$call,$dashboardcall);
		array_push($data,$dataTmp);
		$this->view->dataArray = $data;
		$this->view->call = $call ;
		$this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    public function viewAction()
	{	
		$id = $this->getRequest()->getParam('id');
		$callval = $this->getRequest()->getParam('call');
		if($callval == 'ajaxcall')
			$this->_helper->layout->disableLayout();
		$objName = 'employeeleavetypes';
		$employeeleavetypesform = new Default_Form_employeeleavetypes();
		$employeeleavetypesform->removeElement("submit");
		$elements = $employeeleavetypesform->getElements();
		if(count($elements)>0)
		{
			foreach($elements as $key=>$element)
			{
				if(($key!="Cancel")&&($key!="Edit")&&($key!="Delete")&&($key!="Attachments")){
				$element->setAttrib("disabled", "disabled");
					}
        	}
        }
		$employeeleavetypesmodel = new Default_Model_Employeeleavetypes();	
		try
		{
			if(is_numeric($id) && $id>0)
			{
				$data = $employeeleavetypesmodel->getsingleEmployeeLeavetypeData($id);
				if(!empty($data) && $data != 'norows')
				{
					//echo $data[0]['leavepreallocated'];exit;
					
					if($data[0]['leavepreallocated']==2)
					{
						$data[0]['leavepreallocated']="no";
					}
					else 
					{
						$data[0]['leavepreallocated']="yes";
					}
					
					if($data[0]['leavepredeductable']==2)
					{
						$data[0]['leavepredeductable']="no";
					}
					else 
					{
						$data[0]['leavepredeductable']="yes";
					}

					if($data[0]['cancarryforward']==2)
					{
						$data[0]['cancarryforward']="no";
					}
					else
					{
						$data[0]['cancarryforward']="yes";
					}
					
					if($data[0]['isdonatable']==2)
					{
						$data[0]['isdonatable']="no";
					}
					else
					{
						$data[0]['isdonatable']="yes";
					}
					// XN-277 test
					if($data[0]['isclubbable']==2)
					{
						$data[0]['isclubbable']="no";	
					}
					else 
					{
						$data[0]['isclubbable']="yes";
					}
					//till here 
					if($data[0]['isallocatedinprobation']==2)
					{
					    $data[0]['isallocatedinprobation']="no";
					}
					else
					{
					    $data[0]['isallocatedinprobation']="yes";
					}
					$employeeleavetypesform->populate($data[0]);
					$this->view->form = $employeeleavetypesform;
					$this->view->controllername = $objName;
					$this->view->id = $id;
					$this->view->data = $data[0];
				    $this->view->ermsg = '';
				}
				else
				{
					 $this->view->ermsg = 'norecord';
				}
			}
			else
			{
				$this->view->ermsg = 'nodata';
			}
		}
		catch(Exception $e)
		{
			 $this->view->ermsg = 'nodata';
		}
	}
	
	
	public function editAction()
	{	
	    $auth = Zend_Auth::getInstance();
     	if($auth->hasIdentity()){
					$loginUserId = $auth->getStorage()->read()->id;
		}
		$id = $this->getRequest()->getParam('id');
		$callval = $this->getRequest()->getParam('call');
		if($callval == 'ajaxcall')
			$this->_helper->layout->disableLayout();
		
		$objName = 'employeeleavetypes';
		$employeeleavetypesform = new Default_Form_employeeleavetypes();
		$employeeleavetypesmodel = new Default_Model_Employeeleavetypes();
		$leaveconstraintmodel = new Default_Model_LeaveTypeConstraints();
		$leaveconstraintparammodel = new Default_Model_LeaveTypeConstraintsParams();
		try
		{
			if($id)
			{
				$data = $employeeleavetypesmodel->getsingleEmployeeLeavetypeData($id);
				if(!empty($data) && $data != 'norows')
				{
					if ($data[0]['minimumduration'] == 'Monthly'){
						$data[0]['minimumduration'] = '1';
					}elseif ($data[0]['minimumduration'] == 'Quarterly'){
						$data[0]['minimumduration'] = '2';
					}elseif ($data[0]['minimumduration'] == 'Half-yearly'){
						$data[0]['minimumduration'] = '3';
					}elseif ($data[0]['minimumduration'] == 'Yearly'){
						$data[0]['minimumduration'] = '4';
					}
					if ($data[0]['allotmentmode'] == 'Monthly'){
						$data[0]['allotmentmode'] = '2';
					}elseif ($data[0]['allotmentmode'] == 'Quarterly'){
						$data[0]['allotmentmode'] = '3';
					}elseif ($data[0]['allotmentmode'] == 'Half-yearly'){
						$data[0]['allotmentmode'] = '4';
					}elseif ($data[0]['allotmentmode'] == 'Yearly'){
						$data[0]['allotmentmode'] = '5';
					}else {
						$data[0]['allotmentmode'] = '1';
					}
					$employeeleavetypesform->populate($data[0]);
					$employeeleavetypesform->submit->setLabel('Update');
					$this->view->form = $employeeleavetypesform;
					$this->view->controllername = $objName;
					$this->view->id = $id;
				    $this->view->ermsg = '';
				    $this->editconstraint($id);
				}
				else
				{
					 $this->view->ermsg = 'norecord';
				}
			}
			else
			{
				$this->view->form = $employeeleavetypesform;
			}
			
		}
		catch(Exception $e)
		{
			 $this->view->ermsg = 'nodata';
		}
		$selectedConstraints = array();
		$mainFormInputs = array();
		if($this->getRequest()->getPost()){   
				$postData = json_decode($this->getRequest()->getPost()['formdata'],true);
				foreach ($postData as $postDatum){
					if ($postDatum['form']!=null){
						foreach ($postDatum['form'] as $formdata){
						    $mainFormInputs[$formdata['name']] = $formdata['value'];
						}
					}else{
					      if($postDatum['constraint'] != null){
					      $parameters = array();
						  foreach ($postDatum['constraint'] as $constraint){
						      if($constraint['name'] == 'classname'){
						          $classname = $constraint['value'];
						          continue;
						      }
						      $parameters[$constraint['name']] = $constraint['value'];
					      }
					      Constraint_BaseAbstract::initialize($parameters, $classname);
					      $parameters['classname'] = $classname;
					      array_push($selectedConstraints,$parameters);
					    }
					}
				}
				
				if($mainFormInputs['cancarryforward'] == 2){
				    $mainFormInputs['max_limit'] = $mainFormInputs['numberofdays'];
				}
	   
				if(!$employeeleavetypesform->isValid($mainFormInputs)){
					$messages = $employeeleavetypesform->getMessages();
					foreach ($messages as $key => $val)
					{
						foreach($val as $key2 => $val2)
						{
							$msgarray[$key] = $val2;
							break;
						}
					}
					$this->view->msgarray = $msgarray;
					$this->_helper->json(json_encode($msgarray));
				    return;
				}				
				
				$date = new Zend_Date();
				$actionflag = '';
				$tableid  = ''; 
				$data = array(    'leavetype'=>trim($mainFormInputs['leavetype']),
				                  'numberofdays'=>$mainFormInputs['numberofdays'], 
				                  'leavecode'=>trim($mainFormInputs['leavecode']),
				                  'leavepreallocated'=>trim($mainFormInputs['leavepreallocated']),
				                  'leavepredeductable'=>trim($mainFormInputs['leavepredeductable']),								  
				                  'description'=>trim($mainFormInputs['description']),
								  'modifiedby'=>$loginUserId,
								  'modifieddate'=>gmdate("Y-m-d H:i:s"),
				                  'cancarryforward'=>$mainFormInputs['cancarryforward'],
				                  'max_limit'=>$mainFormInputs['max_limit'],
				                  'allotmentmode'=>$mainFormInputs['allotmentmode'],
				                  'isdonatable'=>$mainFormInputs['isdonatable'],
								  'isclubbable'=>$mainFormInputs['isclubbable'], //XN-277 paritosh
								  'minimumduration'=>$mainFormInputs['minimumduration'],
				                  'isallocatedinprobation'=>$mainFormInputs['isallocatedinprobation']
				   
						);
					if($id!=''){
						$where = array('id=?'=>$id);  
						$actionflag = 2;
					}
					else
					{
					    $data['createdby'] = $loginUserId;
						$data['createddate'] = gmdate("Y-m-d H:i:s");
						$data['isactive'] = 1;
						$where = '';
						$actionflag = 1;
					}
					$Id = $employeeleavetypesmodel->SaveorUpdateEmployeeLeaveTypeData($data, $where);
					$employeeModel = new Default_Model_Employee();
					$employeeleavesModel = new Default_Model_Employeeleaves();
					$employees = $employeeModel->getEmployees('', $loginUserId, '', '', null, null, null);
					if($Id == 'update')
					{
					   foreach($employees as $employee){
					       if($employee['emp_status_id'] == 1 || $employee['emp_status_id'] == 5){
					           $daysSinceJoined = $employeeleavesModel->getDaysBetween($employee['date_of_joining'], date('m/d/y'));
					           $leavedata = $employeeleavesModel->getsingleEmployeeleaveDataByType($employee['user_id'], $id);
					           $used = $leavedata [0] ['used_leaves'];
					           if ((new DateTime ( $leavedata [0] ['modifieddate'] ) < $employeeleavesModel->getFirstDayOfQuarter ( ceil ( date ( 'm' ) / 3 ) ) && $data ['allotmentmode'] == '3')
					           		|| (new DateTime ( $leavedata [0] ['modifieddate'] ) < $employeeleavesModel->getFirstDayOfHalfYear ( ceil ( date ( 'm' ) / 6 ) ) && $data ['allotmentmode'] == '4')) {
					           			$used = 0;
					           	}

					           if ( $data ['allotmentmode'] == '2'){
					           		$leavecount = $employeeleavesModel->countLeaves($data, $daysSinceJoined,$employee['date_of_joining'],$employee['user_id'], $id, $used);

					           }else{
					           		$leavecount = $employeeleavesModel->countLeaves($data, $daysSinceJoined,$employee['date_of_joining'],$employee['user_id'], $id, $used) - $used;
					           }
					           $employeeleavesModel->SaveorUpdateEmployeeLeaveType($employee['user_id'], $leavecount < 0 ? 0 : $leavecount, $used,$loginUserId, $id);
					       }
					   }
		
					   foreach($selectedConstraints as $constraint){
					       $leaveconstraint = $leaveconstraintmodel->getConstraintByClassName($constraint['classname']);
					       $data = array('leavetype_id' => $id,
					           'constraint_id' => $leaveconstraint['id']
					       );
					       $constraintId = $leaveconstraintmodel->saveOrupdateleaveConstraints($data);
					       $constraintparams = $leaveconstraintmodel->getLeaveConstraintParameters($leaveconstraint['id']);
					       foreach ($constraintparams as $constraintparam) {
					           $data = array('parameter_id' => $constraintparam['parameter_id'],
					               'leaveconstraint_id' => $constraintId,
					               'value' => $constraint[$constraintparam['name']]
					           );
					           $leaveconstraintparammodel->saveOrupdateleaveConstraintParameter($data);
					       }
					       
					   }
					   
					   $existingconstraints = $leaveconstraintmodel->getActiveConstraintsbyType($id);
					   foreach ($existingconstraints as $existingconstraint){
					   		$constraintdeleted = true;
					   		foreach ($selectedConstraints as $constraint){
						   		if (array_keys($constraint,$existingconstraint['classname']) ){
						   			$constraintdeleted = false;
						   			break;
						   		}	
					   		}
					   		if($constraintdeleted){
					   			$leaveconstraintmodel->deleteleaveConstraints($existingconstraint['leavetype_id'],$existingconstraint['id']);
					   		}
					   }
					   
					   $tableid = $id;
					   $this->_helper->getHelper("FlashMessenger")->addMessage("Leave type updated successfully.");
					}   
					else
					{
					   foreach($employees as $employee){
					       if($employee['emp_status_id'] == 1){
					           // Only full time employees are eligible for new leave type
					           $daysSinceJoined = $employeeleavesModel->getDaysBetween($employee['date_of_joining'], date('m/d/y'));
					           $leavecount = $employeeleavesModel->countLeaves($data, $daysSinceJoined, $employee['date_of_joining'],$employee['user_id'], $id);
						       $employeeleavesModel->SaveorUpdateEmployeeLeaveType ( $employee ['user_id'], $leavecount < 0 ? 0 : $leavecount, 0, $loginUserId, $Id );
					       }
					   }
					   foreach($selectedConstraints as $constraint){
					       $leaveconstraint = $leaveconstraintmodel->getConstraintByClassName($classname);
					       $data = array('leavetype_id' => $Id,
					           'constraint_id' => $leaveconstraint['id']
					       );
					       $constraintId = $leaveconstraintmodel->saveOrupdateleaveConstraints($data);
					       $constraintparams = $leaveconstraintmodel->getLeaveConstraintParameters($leaveconstraint['id']);
					       foreach ($constraintparams as $constraintparam) {
					           $data = array('parameter_id' => $constraintparam['parameter_id'],
					               'leaveconstraint_id' => $constraintId,
					               'value' => $constraint[$constraintparam['name']]
					           );
					           $leaveconstraintparammodel->saveOrupdateleaveConstraintParameter($data);
					       }
					   }
                       $tableid = $Id; 	
                       $this->_helper->getHelper("FlashMessenger")->addMessage("Leave type added successfully.");					   
					}   
					$menuID = EMPLOYEELEAVETYPES;
					$result = sapp_Global::logManager($menuID,$actionflag,$loginUserId,$tableid);
    			    $this->_helper->json(json_encode("success"));	
			}
		
	}
	
	public function deleteAction()
	{
	     $auth = Zend_Auth::getInstance();
     		if($auth->hasIdentity()){
					$loginUserId = $auth->getStorage()->read()->id;
				}
		$id = $this->_request->getParam('objid');
		 $deleteflag=$this->_request->getParam('deleteflag');
		 $messages['message'] = ''; $messages['msgtype'] = '';$messages['flagtype'] = '';
		 $actionflag = 3;
		    if($id)
			{
			$employeeleavetypesmodel = new Default_Model_Employeeleavetypes();
			$employeeleavesmodel = new Default_Model_Employeeleaves();
			  $data = array('isactive'=>0,'modifieddate'=>gmdate("Y-m-d H:i:s"));
			  $where = array('id=?'=>$id);
                          $leave_data = $employeeleavetypesmodel->getsingleEmployeeLeavetypeData($id);
						  
			  $Id = $employeeleavetypesmodel->SaveorUpdateEmployeeLeaveTypeData($data, $where);
			    if($Id == 'update')
				{
				   $where = array('leavetypeid=?'=>$id);
				   $employeeleavesmodel->SaveorUpdateEmpLeaves($data, $where);
                   sapp_Global::send_configuration_mail("Leave Type", $leave_data[0]['leavetype']);
				   $menuID = EMPLOYEELEAVETYPES;
				   $result = sapp_Global::logManager($menuID,$actionflag,$loginUserId,$id); 
				   $messages['message'] = 'Leave type deleted successfully.';
				    $messages['msgtype'] = 'success';	
				}   
				else
                {
					$messages['message'] = 'Leave type cannot be deleted.';	
					$messages['msgtype'] = 'error';
				}
			}
			else
			{ 
			 $messages['message'] = 'Leave type cannot be deleted.';
			  $messages['msgtype'] = 'error';
			}
			
			// delete success message after delete in view
			if($deleteflag==1)
			{
				if(	$messages['msgtype'] == 'error')
				{
					$this->_helper->getHelper("FlashMessenger")->addMessage(array("error"=>$messages['message'],"msgtype"=>$messages['msgtype'] ,'deleteflag'=>$deleteflag));
				}
				if(	$messages['msgtype'] == 'success')
				{
					$this->_helper->getHelper("FlashMessenger")->addMessage(array("success"=>$messages['message'],"msgtype"=>$messages['msgtype'],'deleteflag'=>$deleteflag));
				}
			}
			$this->_helper->json($messages);
		
	}
	
	public function addconstraintAction(){
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$loginUserId = $auth->getStorage()->read()->id;
			$loginUserdepartment_id = $auth->getStorage()->read()->department_id;
		}
		$constraintclass = $this->_request->getParams()['constraintclass'];
		
		$object = new $constraintclass();
		$data['constraint'] = $object->createform();
		$data['constraintname'] =  $this->_request->getParams()['constraintname'];
		$data['constraintclass'] =  $this->_request->getParams()['constraintclass'];
		$this->view->data=$data;
	}
	
	public function editconstraint($id){
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$loginUserId = $auth->getStorage()->read()->id;
			$loginUserdepartment_id = $auth->getStorage()->read()->department_id;
		}
		$data=array();
		$leavetypeconstraintmodel = new Default_Model_LeaveTypeConstraints();		
		$constraints = $leavetypeconstraintmodel->getActiveConstraintsbyType($id);
		foreach($constraints as $constraint){
			$params=  $leavetypeconstraintmodel->getContraintRequestParameters($constraint['id']);
			$constraintdata['constraintclass'] = $constraint['classname'];
			$object = new $constraintdata['constraintclass']();
			$form= $object->createform();
			$form->populate($params);
			$constraintdata['constraint'] =$form;
			$constraintdata['constraintname'] =$constraint['displayname'];
			array_push($data, $constraintdata);
		}
		$this->view->data = $data;
		
	}
	
	

}

