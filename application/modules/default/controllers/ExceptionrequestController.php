<?php
class Default_ExceptionrequestController extends Zend_Controller_Action {
	public function preDispatch() {
		$ajaxContext = $this->_helper->getHelper ( 'AjaxContext' );
		$ajaxContext->addActionContext ( 'saveexceptionrequestdetails', 'json' )->initContext ();
		$ajaxContext->addActionContext ( 'editpopup', 'json' )->initContext ();
		$ajaxContext->addActionContext ( 'updateexceptiondetails', 'json' )->initContext ();
		$ajaxContext->addActionContext ( 'sendexceptionemailconfirmation', 'json' )->initContext ();
	}
	public function init() {
		$this->_options = $this->getInvokeArg ( 'bootstrap' )->getOptions ();
	}

	public function sendexceptionemailconfirmationAction() {
		session_write_close ();
		$this->_helper->layout->disableLayout ();
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		if (! $this->getRequest ()->getPost ()) {
			return;
		}
		$usersmodel = new Default_Model_Users ();
		$employeesmodel = new Default_Model_Employees ();
		if ($loginUserId != '' && $loginUserId != NULL) {
			$loggedinEmpId = $usersmodel->getUserDetailsByID ( $loginUserId );
			$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );
			if (! empty ( $loggedInEmployeeDetails )) {
				$reportingmanagerId = $loggedInEmployeeDetails [0] ['reporting_manager'];
				$businessunitid = $loggedInEmployeeDetails [0] ['businessunit_id'];

				if ($reportingmanagerId != '' && $reportingmanagerId != NULL)
					$reportingManagerDetails = $usersmodel->getUserDetailsByID ( $reportingmanagerId );
				$employeeemail = $loggedinEmpId [0] ['emailaddress'];
				$userfullname = $loggedinEmpId [0] ['userfullname'];

				if (! empty ( $reportingManagerDetails )) {
					$reportingManageremail = $reportingManagerDetails [0] ['emailaddress'];
					$reportingmanagerName = $reportingManagerDetails [0] ['userfullname'];
				}
			}
		}

		$id = $this->_request->getParam ( 'id' );
		$reason = $this->_request->getParam ( 'reason' );
		$exceptionday = $this->_request->getParam ( 'exceptionday' );
		$from_date = $this->_request->getParam ( 'from_date' );
		$from_date = sapp_Global::change_date ( $from_date, 'database' );
		$to_date = $this->_request->getParam ( 'to_date' );
		$to_date = sapp_Global::change_date ( $to_date, 'database' );
		$appliedexceptioncount = $this->_request->getParam ( 'appliedexceptioncount' );
		/**
		 * MAILING CODE *
		 */
		// $hremail = explode(",",HREMAIL);
		/* Mail to Reporting manager */
		if ($to_date == '' || $to_date == NULL)
			$to_date = $from_date;

		$toemailArr = $reportingManageremail; // $employeeemail
		if (! empty ( $toemailArr )) {
			$options ['subject'] = 'Exception request for approval';
			$options ['header'] = 'Exception Request';
			$options ['toEmail'] = $toemailArr;
			$options ['toName'] = $reportingmanagerName;
			$options ['message'] = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The exception of the below employee is pending for approval:</div>
				</div>
												<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					  <tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $userfullname . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">' . $appliedexceptioncount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
                  </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Exception</td>
                        <td style="color:#5b5b5b">' . $reason . '</td>
                  </tr>
                  <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
                        <td style="color:#5b5b5b">' . $reportingmanagerName . '</td>
                  </tr>
                </tbody></table>

            </div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the exception details.</div>
            </div>
            		</div>';
			$result = sapp_Global::_sendEmail ( $options );
		}
		/* END */
		/* Mail to HR */
		if (defined ( 'LV_HR_' . $businessunitid ) && $businessunitid != '') {
			$options ['subject'] = 'Exception request for approval';
			$options ['header'] = 'Exception Request ';
			$options ['toEmail'] = constant ( 'LV_HR_' . $businessunitid );
			$options ['toName'] = 'Leave management';
			$options ['message'] = '<div>
					<div style = "border:3px solid transparent;border-radius: 15px;background-color:#eeeeee; ">
				<div style="background-color:#5B5A55;border-radius: 10px 10px 0px 0px;">
												<div style="color:#ffffff;padding-left:5px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:5px;font-family:Verdana;font-size:14px;">An exception of the below employee is pending for approval:</div>
					</div>
<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					<tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $userfullname . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">' . $appliedexceptioncount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
                  </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Exception</td>
                        <td style="color:#5b5b5b">' . $reason . '</td>
                  </tr>
                  <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
                        <td style="color:#5b5b5b">' . $reportingmanagerName . '</td>
                  </tr>
                </tbody></table>

            </div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the exception details.</div>
            </div>
            		</div>';
			// $options['cron'] = 'yes';
			$result = sapp_Global::_sendEmail ( $options );
		}

		/* END */
		/* Mail to the applied employee */
		$toemailArr = $employeeemail;
		$options ['subject'] = 'Exception request for approval';
		$options ['header'] = 'Exception Request';
		$options ['toEmail'] = $toemailArr;
		$options ['toName'] = $userfullname;
		$options ['message'] = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">An exception request raised by you is sent for your manager&rsquo;s approval.</div>
				</div>
				<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
				<tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $userfullname . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">' . $appliedexceptioncount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
            	     </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Exception</td>
                        <td style="color:#5b5b5b">' . $reason . '</td>
                  </tr>
                </tbody></table>

            </div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the exception details.</div>
            </div>
            </div>';
		$result = sapp_Global::_sendEmail ( $options );
	}

	public function indexAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
			$loginUserdepartment_id = $auth->getStorage ()->read ()->department_id;
		}

		$exceptionrequestform = new Default_Form_exceptionrequest ();
		$exceptionrequestform->setAttrib ( 'action', BASE_URL . 'exceptionrequest' );
		$exceptionrequestmodel = new Default_Model_Exceptionrequest ();
		$usersmodel = new Default_Model_Users ();
		$employeesmodel = new Default_Model_Employees ();
		$holidaydatesmodel = new Default_Model_Holidaydates ();
		$leavemanagementmodel = new Default_Model_Leavemanagement ();

		$rep_mang_id = '';
		$employeeemail = '';
		$userfullname = '';
		$exceptionrequestdetails = '';
		$reportingManageremail = '';
		$reportingmanagerName = '';
		$businessunitid = '';
		$holidayDateslistArr = array ();
		$dateofjoiningArr = array ();
		$week_startday = '';
		$week_endday = '';
		$ishalf_day = '';

		if ($loginUserId != '' && $loginUserId != NULL) {
			$loggedinEmpId = $usersmodel->getUserDetailsByID ( $loginUserId );
			$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );
			if (! empty ( $loggedInEmployeeDetails )) {
			    if ($loggedInEmployeeDetails [0] ['date_of_joining'] != '') {
			        $date = new DateTime ( $loggedInEmployeeDetails [0] ['date_of_joining'] );
			        $datofjoiningtimestamp = $date->getTimestamp ();
			        $dateofjoining = explode ( "-", $loggedInEmployeeDetails [0] ['date_of_joining'] );
			        
			        $year = $dateofjoining [0];
			        $month = $dateofjoining [1];
			        $day = $dateofjoining [2];
			        $dateofjoiningArr = array (
			            'year' => $year,
			            'month' => $month,
			            'day' => $day,
			            'datetimestamp' => $datofjoiningtimestamp
			        );
			    }
			    
				$reportingmanagerId = $loggedInEmployeeDetails [0] ['reporting_manager'];
				$employeeDepartmentId = $loggedInEmployeeDetails [0] ['department_id'];
				$employeeHolidayGroupId = $loggedInEmployeeDetails [0] ['holiday_group'];

				if ($reportingmanagerId != '' && $reportingmanagerId != NULL)
					$reportingManagerDetails = $usersmodel->getUserDetailsByID ( $reportingmanagerId );
                
					if ($employeeDepartmentId != '' && $employeeDepartmentId != NULL)
				    $weekendDatailsArr = $leavemanagementmodel->getWeekendDetails ( $employeeDepartmentId );
				
				$employeeemail = $loggedinEmpId [0] ['emailaddress'];
				$userfullname = $loggedinEmpId [0] ['userfullname'];
				$businessunitid = $loggedInEmployeeDetails [0] ['businessunit_id'];
				$exceptionrequestdetails = $exceptionrequestmodel->getUserApprovedOrPendingExceptionsData ( $loginUserId );
				if (! empty ( $reportingManagerDetails )) {
					$exceptionrequestform->rep_mang_id->setValue ( $reportingManagerDetails [0] ['userfullname'] );
					$reportingManageremail = $reportingManagerDetails [0] ['emailaddress'];
					$reportingmanagerName = $reportingManagerDetails [0] ['userfullname'];
					$rep_mang_id = $reportingManagerDetails [0] ['id'];
					$rMngr = 'Yes';
				}

				else {
					$msgarray ['rep_mang_id'] = 'Reporting manager is not assigned yet. Please contact your HR.';
					$errorflag = 'false';
				}
				if (! empty ( $weekendDatailsArr )) {
				    $week_startday = $weekendDatailsArr [0] ['weekendstartday'];
				    $week_endday = $weekendDatailsArr [0] ['weekendday'];
				    $ishalf_day = $weekendDatailsArr [0] ['is_halfday'];
				    $isskip_holidays = $weekendDatailsArr [0] ['is_skipholidays'];
				} else {
				    $msgarray ['from_date'] = 'Leave management options are not configured yet.';
				    $msgarray ['to_date'] = 'Leave management options are not configured yet.';
				}

				if ($employeeHolidayGroupId != '' && $employeeHolidayGroupId != NULL) {
					$holidayDateslistArr = $holidaydatesmodel->getHolidayDatesListForGroup ( $employeeHolidayGroupId );
				}

				$this->view->userfullname = $userfullname;
				$this->view->weekendDatailsArr = $weekendDatailsArr;
				$this->view->reportingManagerDetails = $reportingManagerDetails;
				$this->view->holidayDateslistArr = $holidayDateslistArr;
				$this->view->dateofjoiningArr = $dateofjoiningArr;
				$this->view->exceptionrequestdetails = ! empty ( $exceptionrequestdetails ) ? $exceptionrequestdetails : array ();
			} else {
				$msgarray ['rep_mang_id'] = 'Reporting manager is not assigned yet. Please contact your HR.';
			}
		}

		$this->view->form = $exceptionrequestform;

		if ($this->getRequest ()->getPost () && empty ( $filter )) {
			$result = $this->saveexceptionrequest( $exceptionrequestform, $rep_mang_id, $employeeemail, $reportingManageremail, $userfullname, $reportingmanagerName, $businessunitid );
			$this->view->msgarray = $result;
		}
	}
	public function saveexceptionrequestdetailsAction() {
		$this->_helper->layout->disableLayout ();
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}

		$reportingmanagerId = '';
		$employeeDepartmentId = '';
		$businessunitid = '';
		$dateofjoining = '';
		$employeeemail = '';
		$userfullname = '';
		$reportingManageremail = '';
		$reportingmanagerName = '';
		$rep_mang_id = '';
		$errorflag = 'true';
		$week_startday = '';
		$week_endday = '';
		$ishalf_day = '';
		$exceptioncount = '';
		$msgarray = array ();

		$usersmodel = new Default_Model_Users ();
		$employeesmodel = new Default_Model_Employees ();
		$leavemanagementmodel = new Default_Model_Leavemanagement ();
		$exceptionrequestform = new Default_Form_exceptionrequest ();
		$exceptionrequestmodel = new Default_Model_Exceptionrequest ();

		if ($loginUserId != '' && $loginUserId != NULL) {
			$loggedinEmpId = $usersmodel->getUserDetailsByID ( $loginUserId );
			$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );

			if (! empty ( $loggedInEmployeeDetails )) {
				$reportingmanagerId = $loggedInEmployeeDetails [0] ['reporting_manager'];
				$employeeDepartmentId = $loggedInEmployeeDetails [0] ['department_id'];
				$employeeEmploymentStatusId = $loggedInEmployeeDetails [0] ['emp_status_id'];
				$businessunitid = $loggedInEmployeeDetails [0] ['businessunit_id'];
				$dateofjoining = $loggedInEmployeeDetails [0] ['date_of_joining'];

				if ($reportingmanagerId != '' && $reportingmanagerId != NULL)
					$reportingManagerDetails = $usersmodel->getUserDetailsByID ( $reportingmanagerId );

				if ($employeeDepartmentId != '' && $employeeDepartmentId != NULL)
					$weekendDatailsArr = $leavemanagementmodel->getWeekendDetails ( $employeeDepartmentId );
				$employeeemail = $loggedinEmpId [0] ['emailaddress'];
				$userfullname = $loggedinEmpId [0] ['userfullname'];

				if (! empty ( $reportingManagerDetails )) {
					$exceptionrequestform->rep_mang_id->setValue ( $reportingManagerDetails [0] ['userfullname'] );
					$reportingManageremail = $reportingManagerDetails [0] ['emailaddress'];
					$reportingmanagerName = $reportingManagerDetails [0] ['userfullname'];
					$rep_mang_id = $reportingManagerDetails [0] ['id'];
					$rMngr = 'Yes';
				}

				else {
					$msgarray ['rep_mang_id'] = 'Reporting manager is not assigned yet. Please contact your HR.';
					$errorflag = 'false';
				}

				if (! empty ( $weekendDatailsArr )) {
					$week_startday = $weekendDatailsArr [0] ['weekendstartday'];
					$week_endday = $weekendDatailsArr [0] ['weekendday'];
					$ishalf_day = $weekendDatailsArr [0] ['is_halfday'];
					$isskip_holidays = $weekendDatailsArr [0] ['is_skipholidays'];
				}
			}

			else {
				$errorflag = 'false';
				$msgarray ['rep_mang_id'] = 'Reporting manager is not assigned yet. Please contact your HR.';
			}
		}

		$id = $this->_request->getParam ( 'id' );
		$reason = $this->_request->getParam ( 'reason' );
		$exceptionday = $this->_request->getParam ( 'exceptionday' );
		$from_date = $this->_request->getParam ( 'from_date' );
		$from_date = sapp_Global::change_date ( $from_date, 'database' );
		$to_date = $this->_request->getParam ( 'to_date' );
		$to_date = sapp_Global::change_date ( $to_date, 'database' );
		$appliedexceptioncount = $this->_request->getParam ( 'appliedexceptioncount' );

		$userAppliedExceptions = $exceptionrequestmodel->getUsersAppliedExceptions ( $loginUserId );

		if (! empty ( $userAppliedExceptions )) {
			foreach ( $userAppliedExceptions as $exception ) {
				if ($exceptionday == 1)
					$exceptionsDateExists = $exceptionrequestmodel->checkExceptionExists ( $exception ['from_date'], $exception ['to_date'], $from_date, $to_date, $loginUserId );
				else
					$exceptionsDateExists = $exceptionrequestmodel->checkExceptionExists ( $exception ['from_date'], $exception ['to_date'], $from_date, $from_date, $loginUserId );
				if ($exceptionsDateExists [0] ['exceptionexist'] > 0) {
					$errorflag = 'false';
					$msgarray ['from_date'] = 'Exception has already been applied for the above dates.';
					break;
				}
			}
		}

		/* START Validating whether applied date is prior to date of joining */
		if ($dateofjoining >= $from_date && $from_date != '') {
			$errorflag = 'false';
			$msgarray ['from_date'] = ' Exception cannot be applied before date of joining.';
		}
		/* End */

		// get hr_id from leavemanagemnt table based on login user dept id
		$configure_hr_id = $leavemanagementmodel->gethrDetails ( $employeeDepartmentId );
		if (! empty ( $configure_hr_id )) {
			$hr_id = $configure_hr_id [0] ['hr_id'];
		}

		$data = array (
				'user_id' => $loginUserId,
				'reason' => $reason,
				'exceptionday' => $exceptionday,
				'from_date' => $from_date,
				'to_date' => ($to_date != '') ? $to_date : $from_date,
				'exceptionstatus' => 1,
				'rep_mang_id' => $rep_mang_id,
				'hr_id' => $hr_id,
				'appliedexceptioncount' => $appliedexceptioncount,
				'modifiedby' => $loginUserId,
				'modifieddate' => gmdate ( "Y-m-d H:i:s" )
		);

		if ($this->getRequest ()->getPost ()) {
			if ($exceptionrequestform->isValid ( $this->_request->getPost () ) && $errorflag == 'true') {

				$date = new Zend_Date ();
				$actionflag = '';
				$tableid = '';
				if ($id != '') {
					$where = array (
							'id=?' => $id
					);
					$actionflag = 2;
				} else {
					$data ['createdby'] = $loginUserId;
					$data ['createddate'] = gmdate ( "Y-m-d H:i:s" );
					$data ['isactive'] = 1;
					$where = '';
					$actionflag = 1;
				}
				$Id = $exceptionrequestmodel->SaveorUpdateExceptionRequest ( $data, $where );

				if ($Id == 'update') {
					$tableid = $id;
					$this->_helper->getHelper ( "FlashMessenger" )->addMessage ( array (
							"success" => "Exception request updated successfully."
					) );
				} else {
					$tableid = $Id;
					$this->_helper->getHelper ( "FlashMessenger" )->addMessage ( array (
							"success" => "Exception request added successfully."
					) );
				}
				$menuID = EXCEPTIONREQUEST;
				$result = sapp_Global::logManager ( $menuID, $actionflag, $loginUserId, $tableid );
				$data['id'] = $id;
				$this->_helper->json ( array (
						'result' => 'saved',
						'message' => 'Exception request applied successfully.',
						'controller' => 'exceptionrequest',
						'leaverequest' => $data,
						'actionflag' => $actionflag
				) );
			}

			else {
				$messages = $exceptionrequestform->getMessages ();
				if (isset ( $msgarray ['rep_mang_id'] )) {

					$messages ['rep_mang_id'] = array (
							$msgarray ['rep_mang_id']
					);
				}
				if (isset ( $msgarray ['from_date'] )) {

					$messages ['from_date'] = array (
							$msgarray ['from_date']
					);
				}
				if (isset ( $msgarray ['exceptionday'] )) {

					$messages ['exceptionday'] = array (
							$msgarray ['exceptionday']
					);
				}
				if (isset ( $msgarray ['appliedexceptioncount'] )) {

					$messages ['appliedexceptioncount'] = array (
							$msgarray ['appliedexceptioncount']
					);
				}
				$messages ['result'] = 'error';
				$this->_helper->json ( $messages );
			}
		}
	}
	public function calculatebusinessdays($fromDate, $toDate) {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}

		$noOfDays = 0;
		$weekDay = '';
		$employeeDepartmentId = '';
		$employeeGroupId = '';
		$weekend1 = '';
		$weekend2 = '';
		$holidayDatesArr = array ();
		$leaveDatesArr = array ();
		// Calculating the no of days in b/w from date & to date with out taking weekend & holidays....
		$employeesmodel = new Default_Model_Employees ();
		$leavemanagementmodel = new Default_Model_Leavemanagement ();
		$holidaydatesmodel = new Default_Model_Holidaydates ();

		$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );
		if (! empty ( $loggedInEmployeeDetails )) {
			$employeeDepartmentId = $loggedInEmployeeDetails [0] ['department_id'];
			$employeeGroupId = $loggedInEmployeeDetails [0] ['holiday_group'];
			$loggedinEmpId = $usersmodel->getUserDetailsByID ( $loginUserId );

			if ($employeeDepartmentId != '' && $employeeDepartmentId != NULL)
				$weekendDetailsArr = $leavemanagementmodel->getWeekendNamesDetails ( $employeeDepartmentId );

			if (! empty ( $weekendDetailsArr )) {
				if ($weekendDetailsArr [0] ['is_skipholidays'] == 1 && isset ( $employeeGroupId ) && $employeeGroupId != '') {
					$holidayDateslistArr = $holidaydatesmodel->getHolidayDatesListForGroup ( $employeeGroupId );
					$leaveDateslistArr = $leavemanagementmodel->getLeaveDetails ( $loggedinEmpId );
					if (! empty ( $holidayDateslistArr )) {
						for($i = 0; $i < sizeof ( $holidayDateslistArr ); $i ++) {
							$holidayDatesArr [$i] = $holidayDateslistArr [$i] ['holidaydate'];
						}
					}

					if (! empty ( $leaveDateslistArr )) {
						for($i = 0; $i < sizeof ( $leaveDateslistArr ); $i ++) {
							$leaveDatesArr [$i] = $leaveDateslistArr [$i] ['from_date'];
						}
					}
				}
				$weekend1 = $weekendDetailsArr [0] ['daystartname'];
				$weekend2 = $weekendDetailsArr [0] ['dayendname'];
			}

			$fromdate_obj = new DateTime ( $fromDate );
			$weekDay = $fromdate_obj->format ( 'l' );
			while ( $fromDate <= $toDate ) {
				/* if(($weekDay != 'Saturday'||$weekDay != 'Sunday') && (!empty($holidayDates)) && (!in_array($fromDate,$holidayDates))) */
				if (count ( $holidayDatesArr ) > 0) {
					if ($weekDay != $weekend1 && $weekDay != $weekend2 && (! in_array ( $fromDate, $holidayDatesArr ))) {
						$noOfDays ++;
					}
				} else {
					if ($weekDay != $weekend1 && $weekDay != $weekend2) {
						$noOfDays ++;
					}
				}
				$fromdate_obj->add ( new DateInterval ( 'P1D' ) ); // Increment from date by one day...
				$fromDate = $fromdate_obj->format ( DATEFORMAT_PHP );
				$weekDay = $fromdate_obj->format ( 'l' );
			}
		}

		return $noOfDays;
	}
	public function editpopupAction() {
		Zend_Layout::getMvcInstance ()->setLayoutPath ( APPLICATION_PATH . "/layouts/scripts/popup/" );
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		$id = $this->getRequest ()->getParam ( 'id' );
		$userid = $this->getRequest ()->getParam ( 'unitId' );
		if ($id == '')
			$id = $loginUserId;

		$exceptionequestmodel = new Default_Model_Exceptionrequest ();
		$exceptionrequestform = new Default_Form_exceptionrequest ();
		$user_logged_in = 'true';
		$manager_logged_in = 'false';
		$cancel_flag = 'true';
		$approve_flag = 'true';
		$reject_flag = 'true';
		if ($id && is_numeric ( $id ) && $id > 0) {
			$exception_details = $exceptionequestmodel->getExceptionDetails ( $id );
			if (! empty ( $exception_details )) {
				$exception_details = call_user_func_array ( 'array_merge', $exception_details );

				if ($exception_details ['user_id'] == $loginUserId) {
					if ($exception_details ['exceptionstatus'] == 'Approved') {
						if (isset ( $exception_details ['from_date'] )) {
							$leaveDate = date ( $exception_details ['from_date'] );
							$todayDate = date ( DATEFORMAT_PHP );
							if (strtotime ( $todayDate ) >= strtotime ( $leaveDate )) {
								$cancel_flag = 'false';
							}
						}
					}
					$approve_flag = 'false';
					$reject_flag = 'false';
				}

				if ($exception_details ['rep_mang_id'] == $loginUserId || $exception_details ['hr_id'] == $loginUserId) {
					if ($exception_details ['exceptionstatus'] == 'Approved') {
						$approve_flag = 'false';
						$reject_flag = 'false';
					}
					$manager_logged_in = 'true';
				}
			}
		} else {
			$this->view->rowexist = "norows";
		}

		$this->view->form = $exceptionrequestform;
		$this->view->controllername = 'exceptionrequest';
		$this->view->exception_details = $exception_details;
		$this->view->user_logged_in = $user_logged_in;
		$this->view->manager_logged_in = $manager_logged_in;
		$this->view->cancel_flag = $cancel_flag;
		$this->view->approve_flag = $approve_flag;
		$this->view->reject_flag = $reject_flag;
	}
	public function updateexceptiondetailsAction() {
		$this->_helper->layout->disableLayout ();
		$result ['result'] = 'success';
		$result ['msg'] = '';
		$leavestatus = '';
		$subject = '';
		$message = '';
		$successmsg = '';
		$actionflag = 2;
		$user_logged_in = 'true';
		$manager_logged_in = 'false';
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}

		$id = $this->_request->getParam ( 'id' );
		$status = $this->_request->getParam ( 'status' );
		$comments = $this->_request->getParam ( 'comments' );
		$exceptionrequestmodel = new Default_Model_Exceptionrequest ();
		$employeeleavetypesmodel = new Default_Model_Employeeleavetypes ();
		$usersmodel = new Default_Model_Users ();
		if ($id && is_numeric ( $id ) && $id > 0) {
			$exception_details = $exceptionrequestmodel->getExceptionDetails ( $id );
			if (! empty ( $exception_details )) {
				$exception_details = call_user_func_array ( 'array_merge', $exception_details );

				if ($exception_details ['user_id'] == $loginUserId) {
					if (sapp_Global::_decrypt ( $status ) == 'Cancelled') {
						$exceptionstatus = 4;

						$successmsg = 'Exception request cancelled succesfully.';
						$subject = 'Exception request cancelled';
						$message = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
								<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below exception(s) has been cancelled.</div>
								</div>';
					}
				} elseif ($exception_details ['rep_mang_id'] == $loginUserId || ($exception_details ['hr_id'] == $loginUserId)) {
					if (sapp_Global::_decrypt ( $status ) == 'Cancelled') {
						$exceptionstatus = 4;

						$successmsg = 'Exception request cancelled succesfully.';
						$subject = 'Exception request cancelled';
						$message = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
								<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below exception(s) has been cancelled.</div>
								</div>';
					} elseif (sapp_Global::_decrypt ( $status ) == 'Approved') {
						$exceptionstatus = 2;
						$successmsg = 'Exception request approved succesfully.';
						$subject = 'Exception request approved';
						$message = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
								<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below exception(s) has been approved.</div>
								</div>';
					} elseif (sapp_Global::_decrypt ( $status ) == 'Rejected') {
						$exceptionstatus = 3;

						$successmsg = 'Exception request rejected succesfully.';
						$subject = 'Exception request rejected';
						$message = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
								<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below exception(s) has been rejected.</div>
								</div>';
					}
					$manager_logged_in = 'true';
				}

				if (! empty ( $exceptionstatus )) {
					$data = array (
							'exceptionstatus' => $exceptionstatus,
							'approver_comments' => ! empty ( $comments ) ? $comments : NULL,
							'modifiedby' => $loginUserId,
							'modifieddate' => gmdate ( "Y-m-d H:i:s" )
					);
					$where = array (
							'id=?' => $id
					);
					$Id = $exceptionrequestmodel->SaveorUpdateExceptionRequest ( $data, $where );

					$businessunitid = $exception_details ['bunit_id'];
					$userDetails = $usersmodel->getUserDetailsByID ( $exception_details ['user_id'] );
					$employeename = $userDetails [0] ['userfullname'];
					for($i = 1; $i <= 3; $i ++) {
						$toEmail = '';
						$toName = '';
						if ($i == 1) {
							$userDetails = $usersmodel->getUserDetailsByID ( $exception_details ['user_id'] );
							$toEmail = $userDetails [0] ['emailaddress'];
							$toName = $userDetails [0] ['userfullname'];
						} elseif ($i == 2) {
							$repManagerDetails = $usersmodel->getUserDetailsByID ( $exception_details ['rep_mang_id'] );
							$toEmail = $repManagerDetails [0] ['emailaddress'];
							$toName = $repManagerDetails [0] ['userfullname'];
						} elseif ($i == 3) {
							if (defined ( 'LV_HR_' . $businessunitid ) && $businessunitid != '') {
								$toEmail = constant ( 'LV_HR_' . $businessunitid );
								$toName = 'Leave management';
							}
						}

						if ($toEmail != '' && $toName != '') {
							$options ['header'] = 'Exception Request';
							$options ['toEmail'] = $toEmail;
							$options ['toName'] = $toName;
							$options ['subject'] = $subject;
							$options ['message'] = $message;
							$options ['message'] .= '<div style = "padding : 8px 50px 0px 30px">
                			<table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
	                      	<tbody>
		                      <tr bgcolor="#045396">
		                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
		                        <td width="72%" style="color:#ffffff">' . $employeename . '</td>
		                      </tr>
		                      <tr bgcolor="#F5F5F3">
		                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
		                        <td style="color:#5b5b5b">' . $exception_details ['appliedexceptioncount'] . '</td>
		                      </tr>
		                      <tr bgcolor="#D9DCE5">
		                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
		                        <td style="color:#5b5b5b">' . $exception_details ['from_date'] . '</td>
		                      </tr>
		                      <tr bgcolor="#F5F5F3">
		                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
		                        <td style="color:#5b5b5b">' . $exception_details ['to_date'] . '</td>
		            	      </tr>
		                      <tr bgcolor="#D9DCE5">
		                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Exception</td>
		                        <td style="color:#5b5b5b">' . $exception_details ['reason'] . '</td>
	                  		  </tr>
                			</tbody>
                			</table>
							</div>
            				<div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the exception details.</div>
            						</div>
            						</div>';
							sapp_Global::_sendEmail ( $options );
						}
					}

					$menuID = ($manager_logged_in == 'true') ? MANAGEREMPLOYEEVACATIONS : PENDINGLEAVES;
					sapp_Global::logManager ( $menuID, $actionflag, $loginUserId, $id );
					$result ['msg'] = $successmsg;
				}
			}
		} else {
			$result ['result'] = 'fail';
			$result ['msg'] = '';
		}

		$this->_helper->json ( $result );
	}
}