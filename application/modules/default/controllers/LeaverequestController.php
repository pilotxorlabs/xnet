<?php
/*********************************************************************************


 *

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/
class Default_LeaverequestController extends Zend_Controller_Action {
	private $options;
	public function preDispatch() {
		$ajaxContext = $this->_helper->getHelper ( 'AjaxContext' );
		$ajaxContext->addActionContext ( 'gethalfdaydetails', 'json' )->initContext ();
		$ajaxContext->addActionContext ( 'saveleaverequestdetails', 'json' )->initContext ();
		$ajaxContext->addActionContext ( 'updateleavedetails', 'json' )->initContext ();
		$ajaxContext->addActionContext ( 'sendemailconfirmation', 'json' )->initContext ();
		$ajaxContext->addActionContext ( 'compoffrequest', 'json' )->initContext ();
	}
	public function init() {
		$this->_options = $this->getInvokeArg ( 'bootstrap' )->getOptions ();
	}
	public function indexAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
			$loginUserdepartment_id = $auth->getStorage ()->read ()->department_id;
		}
		$leaverequestform = new Default_Form_leaverequest ();
		$leaverequestform->setAttrib ( 'action', BASE_URL . 'leaverequest' );
		$leaverequestmodel = new Default_Model_Leaverequest ();
		$employeeleavetypemodel = new Default_Model_Employeeleavetypes ();
		$leavemanagementmodel = new Default_Model_Leavemanagement ();
		$usersmodel = new Default_Model_Users ();
		$employeesmodel = new Default_Model_Employees ();
		$weekdaysmodel = new Default_Model_Weekdays ();
		$holidaydatesmodel = new Default_Model_Holidaydates ();
		$msgarray = array ();
		$dateofjoiningArr = array ();
		$holidayDateslistArr = array ();
		$rMngr = 'No';
		$availableleaves = '';
		$rep_mang_id = '';
		$employeeemail = '';
		$reportingManageremail = '';
		$week_startday = '';
		$week_endday = '';
		$ishalf_day = '';
		$userfullname = '';
		$reportingmanagerName = '';
		$businessunitid = '';
		$hremailgroup = '';
		$managerrequestdetails = '';
		/*
		 * Start
		 * Queries to fetch user details,reporting manager details and weekend details from users table and employees table
		 */
		if ($loginUserId != '' && $loginUserId != NULL) {
			$loggedinEmpId = $usersmodel->getUserDetailsByID ( $loginUserId );
			$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );

			if (! empty ( $loggedInEmployeeDetails )) {
				if ($loggedInEmployeeDetails [0] ['date_of_joining'] != '') {
					$date = new DateTime ( $loggedInEmployeeDetails [0] ['date_of_joining'] );
					$datofjoiningtimestamp = $date->getTimestamp ();
					$dateofjoining = explode ( "-", $loggedInEmployeeDetails [0] ['date_of_joining'] );

					$year = $dateofjoining [0];
					$month = $dateofjoining [1];
					$day = $dateofjoining [2];
					$dateofjoiningArr = array (
							'year' => $year,
							'month' => $month,
							'day' => $day,
							'datetimestamp' => $datofjoiningtimestamp
					);
				}
				$reportingmanagerId = $loggedInEmployeeDetails [0] ['reporting_manager'];
				$employeeDepartmentId = $loggedInEmployeeDetails [0] ['department_id'];
				$employeeEmploymentStatusId = $loggedInEmployeeDetails [0] ['emp_status_id'];
				$employeeHolidayGroupId = $loggedInEmployeeDetails [0] ['holiday_group'];

				$reportingManagerDetails = $usersmodel->getUserDetailsByID ( $reportingmanagerId );
				$weekendDatailsArr = $leavemanagementmodel->getWeekendDetails ( $employeeDepartmentId );
				$employeeemail = $loggedinEmpId [0] ['emailaddress'];
				$userfullname = $loggedinEmpId [0] ['userfullname'];
				$businessunitid = $loggedInEmployeeDetails [0] ['businessunit_id'];
				if (! empty ( $reportingManagerDetails )) {
					$leaverequestform->rep_mang_id->setValue ( $reportingManagerDetails [0] ['userfullname'] );
					$reportingManageremail = $reportingManagerDetails [0] ['emailaddress'];
					$reportingmanagerName = $reportingManagerDetails [0] ['userfullname'];
					$rep_mang_id = $reportingManagerDetails [0] ['id'];
					$rMngr = 'Yes';
				} else {
					$msgarray ['rep_mang_id'] = 'Reporting manager is not assigned yet. Please contact your HR.';
				}

				if (! empty ( $weekendDatailsArr )) {
					$week_startday = $weekendDatailsArr [0] ['weekendstartday'];
					$week_endday = $weekendDatailsArr [0] ['weekendday'];
					$ishalf_day = $weekendDatailsArr [0] ['is_halfday'];
					$isskip_holidays = $weekendDatailsArr [0] ['is_skipholidays'];
				} else {
					$msgarray ['from_date'] = 'Leave management options are not configured yet.';
					$msgarray ['to_date'] = 'Leave management options are not configured yet.';
				}

				if ($employeeHolidayGroupId != '' && $employeeHolidayGroupId != NULL)
					$holidayDateslistArr = $holidaydatesmodel->getHolidayDatesListForGroup ( $employeeHolidayGroupId );

				if (defined ( 'LV_HR_' . $businessunitid ))
					$hremailgroup = 'hremailgroupexists';
				else
					$hremailgroup = '';

					/* Search Filters */
				$isReportingManagerFlag = 'false';
				$searchRepFlag = 'false';
				$searchMeFlag = 'true';

				$isReportingManager = $employeesmodel->CheckIfReportingManager ( $loginUserId );
				if (! empty ( $isReportingManager ) && $isReportingManager [0] ['count'] > 0) {
					$searchRepFlag = 'true';
				}
				
				$filter = $this->_request->getParam ( 'filter' );
				if (! empty ( $filter )) {
					
					if (in_array ( 2, $filter ))
					{
						$searchMeFlag = 'true';
						$searchRepFlag = 'true';
					}
					else
						$searchRepFlag = 'false';

					if (in_array ( 1, $filter ))
						$searchMeFlag = 'true';
				}

				if ($searchMeFlag == 'true')
					$leaverequestdetails = $leaverequestmodel->getUserApprovedOrPendingLeavesData ( $loginUserId );
					/* Start -For Checking if logged in user is reporting manager */
				
				if (! empty ( $isReportingManager ) && $isReportingManager [0] ['count'] > 0) {
					if ($searchRepFlag == 'true')
						$managerrequestdetails = $leaverequestmodel->getManagerApprovedOrPendingLeavesData ( $loginUserId );
					$isReportingManagerFlag = 'true';
				}
				/* End */
				/* Start -For Checking if logged in user is hr manager for thar particular department */

				// get hr_id from leavemanagemnt table based on login user dept id
				$configure_hr_id = $leavemanagementmodel->gethrDetails ( $loginUserdepartment_id );
				if (! empty ( $configure_hr_id )) {
					if ($configure_hr_id [0] ['hr_id'] == $loginUserId) {
						if ($searchRepFlag == 'true')
							$managerrequestdetails = $leaverequestmodel->getHrApprovedOrPendingLeavesData ( $loginUserId );
						$isReportingManagerFlag = 'true';
					}
				}
				/* End */
				$this->view->userfullname = $userfullname;
				$this->view->loggedinEmpId = $loggedinEmpId;
				$this->view->weekendDatailsArr = $weekendDatailsArr;
				$this->view->reportingManagerDetails = $reportingManagerDetails;
				$this->view->rMngr = $rMngr;
				$this->view->hremailgroup = $hremailgroup;
				$this->view->dateofjoiningArr = $dateofjoiningArr;
				$this->view->leaverequestdetails = ! empty ( $leaverequestdetails ) ? $leaverequestdetails : array ();
				$this->view->holidayDateslistArr = $holidayDateslistArr;
				$this->view->managerrequestdetails = ! empty ( $managerrequestdetails ) ? $managerrequestdetails : array ();
				$this->view->isReportingManagerFlag = $isReportingManagerFlag;
				$this->view->searchRepFlag = $searchRepFlag;
				$this->view->searchMeFlag = $searchMeFlag;
			} else {
				$msgarray ['rep_mang_id'] = 'Reporting manager is not assigned yet. Please contact your HR.';
				$msgarray ['from_date'] = 'Leave management options are not configured yet.';
				$msgarray ['to_date'] = 'Leave management options are not configured yet.';
			}
		}
		/* End */

		/*
		 * Start
		 * Query to fetch and build multioption for Leavetype dropdown
		 */
		$leavetype = $employeeleavetypemodel->getactiveleavetype ();
		if (! empty ( $leavetype )) {
			if (sizeof ( $leavetype ) > 0) {
				foreach ( $leavetype as $leavetyperes ) {
					$leaverequestform->leavetypeid->addMultiOption ( $leavetyperes ['id'] . '!@#' . $leavetyperes ['numberofdays'] . '!@#' . utf8_encode ( $leavetyperes ['leavetype'] ), utf8_encode ( $leavetyperes ['leavetype'] ) );
				}
			}
		} else {
			$msgarray ['leavetypeid'] = ' Leave types are not configured yet.';
		}
		$this->view->leavetype = $leavetype;
		/* End */

		/*
		 * START
		 * Query to get the number of available leaves for the employee
		 */
		$getavailbaleleaves = $leaverequestmodel->getAvailableLeaves ( $loginUserId );
		if (! empty ( $getavailbaleleaves )) {
			$leaverequestform->no_of_days->setValue ( $getavailbaleleaves [0] ['remainingleaves'] );
			$availableleaves = $getavailbaleleaves [0] ['remainingleaves'];
		} else {
			$msgarray ['no_of_days'] = 'You have not been allotted leaves for this financial year. Please contact your HR.';
		}
		$this->view->getavailbaleleaves = $getavailbaleleaves;
		/* END */

		$this->view->form = $leaverequestform;
		$this->view->msgarray = $msgarray;
		$this->view->loginUserId = $loginUserId;

		if ($this->getRequest ()->getPost () && empty ( $filter )) {
			$result = $this->saveleaverequest ( $leaverequestform, $availableleaves, $rep_mang_id, $employeeemail, $reportingManageremail, $week_startday, $week_endday, $ishalf_day, $userfullname, $reportingmanagerName, $businessunitid );
			$this->view->msgarray = $result;
		}

		$this->view->messages = $this->_helper->flashMessenger->getMessages ();
	}

	public function saveleaverequestdetailsAction() {
		$this->_helper->layout->disableLayout ();
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		$leaverequestform = new Default_Form_leaverequest ();
		$leaverequestmodel = new Default_Model_Leaverequest ();
		$employeeleavemodel = new Default_Model_Employeeleaves ();
		$usersmodel = new Default_Model_Users ();
		$employeesmodel = new Default_Model_Employees ();
		if ($loginUserId != '' && $loginUserId != NULL) {
		    $loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );

		    if (! empty ( $loggedInEmployeeDetails )) {

		        $reportingmanagerId = $loggedInEmployeeDetails [0] ['reporting_manager'];

		        if ($reportingmanagerId != '' && $reportingmanagerId != NULL)
		            $reportingManagerDetails = $usersmodel->getUserDetailsByID ( $reportingmanagerId );
		        if (! empty ( $reportingManagerDetails )) {
		            $leaverequestform->rep_mang_id->setValue ( $reportingManagerDetails [0] ['userfullname'] );
		        }
		    }
		}

		$id = $this->_request->getParam ( 'id' );
		$msgarray = array();
		$data = array();
		$errorflag = $leaverequestmodel->validateLeaveRequest($this->_request->getPost (), $loginUserId, $msgarray, $data);
		$employeeleavetypesmodel = new Default_Model_Employeeleavetypes();

		if ($this->getRequest ()->getPost ()) {
			if ($leaverequestform->isValid ( $this->_request->getPost () ) && $errorflag == 'true') {

				$date = new Zend_Date ();
				$actionflag = '';
				$tableid = '';
				if ($id != '') {
					$where = array (
							'id=?' => $id
					);
					$actionflag = 2;
				} else {
					$data ['createdby'] = $loginUserId;
					$data ['createddate'] = gmdate ( "Y-m-d H:i:s" );
					$data ['isactive'] = 1;
					$where = '';
					$actionflag = 1;
				}
				if ($employeeleavetypesmodel->getLeavetypeDataByID($data['leavetypeid'])[0] ['leavepredeductable'] == 1)
					$remainingLeaves = $employeeleavemodel->getEmployeeLeaveLimit($data ['user_id'],$data ['leavetypeid'])['0']['emp_leave_limit'] - $data ['appliedleavescount'];
				else
					$remainingLeaves = $employeeleavemodel->getEmployeeLeaveLimit($data ['user_id'],$data ['leavetypeid'])['0']['emp_leave_limit'];
				$data['no_of_days'] = $remainingLeaves;
				$Id = $leaverequestmodel->SaveorUpdateLeaveRequest ( $data, $where );
				$leavetypeArr = $employeeleavetypesmodel->getLeavetypeDataByID($data['leavetypeid']);
				if (! empty ( $leavetypeArr )) {
					if ($leavetypeArr [0] ['leavepredeductable'] == 1) {
						$updateemployeeleave = $leaverequestmodel->updateemployeeleaves ( $data ['appliedleavescount'], $data ['user_id'], $data ['leavetypeid'] );
					}
				}
				/**
				 * leave request history
				 */
				if ($Id != 'update') {
					$history = 'Leave Request has been sent for Manager Approval by ';
					$leaverequesthistory_model = new Default_Model_Leaverequesthistory ();
					$leave_history = array (
							'leaverequest_id' => $Id,
							'description' => $history,
							// 'emp_name' => ucfirst($auth->getStorage()->read()->userfullname),
							'createdby' => $loginUserId,
							'modifiedby' => $loginUserId,
							'isactive' => 1,
							'createddate' => gmdate ( "Y-m-d H:i:s" ),
							'modifieddate' => gmdate ( "Y-m-d H:i:s" )
					);
					$where = '';
					$leavehistory = $leaverequesthistory_model->saveOrUpdateLeaveRequestHistory ( $leave_history, $where );
				}
				if ($Id == 'update') {
					$tableid = $id;
					$this->_helper->getHelper ( "FlashMessenger" )->addMessage ( array (
							"success" => "Leave request updated successfully."
					) );
				} else {
					$tableid = $Id;
					$this->_helper->getHelper ( "FlashMessenger" )->addMessage ( array (
							"success" => "Leave request added successfully."
					) );
				}
				$menuID = LEAVEREQUEST;
				$result = sapp_Global::logManager ( $menuID, $actionflag, $loginUserId, $tableid );

				$employeeleaves = $employeeleavemodel->getsingleEmployeeleaveDataByType ( $data ['user_id'], $data ['leavetypeid'] );
				$data ['leavetypecount'] = $employeeleaves [0] ['emp_leave_limit'];
				$data ['leavetypetext'] = $leavetypeArr [0] ['leavetype'];

				$this->_helper->json ( array (
						'result' => 'saved',
						'message' => 'Leave request applied successfully.',
						'controller' => 'leaverequest',
						'leaverequest' => $data,
						'actionflag' => $actionflag
				) );
			} else {
				$messages = $leaverequestform->getMessages ();
				if (isset ( $msgarray ['rep_mang_id'] )) {

					$messages ['rep_mang_id'] = array (
							$msgarray ['rep_mang_id']
					);
				}
				if (isset ( $msgarray ['from_date'] )) {

					$messages ['from_date'] = array (
							$msgarray ['from_date']
					);
				}
				if (isset ( $msgarray ['to_date'] )) {

					$messages ['to_date'] = array (
							$msgarray ['to_date']
					);
				}
				if (isset ( $msgarray ['leaveday'] )) {

					$messages ['leaveday'] = array (
							$msgarray ['leaveday']
					);
				}
				if (isset ( $msgarray ['leavetypeid'] )) {

					$messages ['leavetypeid'] = array (
							$msgarray ['leavetypeid']
					);
				}
				if (isset ( $msgarray ['no_of_days'] )) {

					$messages ['no_of_days'] = array (
							$msgarray ['no_of_days']
					);
				}
				if (isset ( $msgarray ['appliedleavesdaycount'] )) {

					$messages ['appliedleavesdaycount'] = array (
							$msgarray ['appliedleavesdaycount']
					);
				}
				$messages ['result'] = 'error';
				$this->_helper->json ( $messages );
			}
		}
	}



	public function sendemailconfirmationAction() {
		session_write_close ();
		$this->_helper->layout->disableLayout ();
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		if (! $this->getRequest ()->getPost ()) {
			return;
		}
		$leaverequest = $this->getRequest ()->getPost ();
		$usersmodel = new Default_Model_Users ();
		$employeesmodel = new Default_Model_Employees ();
		if ($loginUserId != '' && $loginUserId != NULL) {
			$loggedinEmpId = $usersmodel->getUserDetailsByID ( $loginUserId );
			$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );

			$leaveOwnerDetails = $employeesmodel->getLoggedInEmployeeDetails ( $leaverequest['user_id'] );

			$leaveOwner = $usersmodel->getUserDetailsByID ( $leaverequest['user_id'] )['0'];

			$reportingmanagerId = $leaveOwnerDetails [0] ['reporting_manager'];
			$businessunitid = $leaveOwnerDetails [0] ['businessunit_id'];
			$employeeemail = $loggedinEmpId [0] ['emailaddress'];
			$userfullname = $loggedinEmpId [0] ['userfullname'];

			$reportingManagerDetails = $usersmodel->getUserDetailsByID ( $reportingmanagerId );
			if (! empty ( $reportingManagerDetails )) {
				$reportingManageremail = $reportingManagerDetails [0] ['emailaddress'];
				$reportingmanagerName = $reportingManagerDetails [0] ['userfullname'];
			}
		} else {
			return;
		}

		$appliedleavescount = $leaverequest ['appliedleavescount'];
		$leavetypecount = $leaverequest ['leavetypecount'];
		$from_date = $leaverequest ['from_date'];
		$reason = $leaverequest ['reason'];
		$leavetypetext = $leaverequest ['leavetypetext'];
		$to_date = $leaverequest ['to_date'];

		$toemailArr = $reportingManageremail; // $employeeemail
		$message = "The leave request";
		if($loginUserId != $leaveOwner['id']){
		    $message .= ", raised by $userfullname, on behalf";
		}
		if (! empty ( $toemailArr )) {
			$options ['subject'] = 'Leave request for approval (' .$leavetypetext .')';
			$options ['header'] = 'Leave Request';
			$options ['toEmail'] = $toemailArr;
			$options ['toName'] = $reportingmanagerName;
			$options ['message'] = '<div>
										<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
											<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">'. $message .' of the below employee is pending for approval:</div>
											</div>
											<div style = "padding : 8px 50px 0px 30px">
								                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
								                      <tbody><tr bgcolor="#045396">
								                        <td width="28%" style="border-right:2px solid #BBBBBB;color:#ffffff">Employee Name</td>
								                        <td width="72%" style="color:#ffffff">' . $leaveOwner['userfullname'] . '</td>
								                      </tr>
								                      <tr bgcolor="#F5F5F3">
								                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
								                        <td style="color:#5b5b5b">' . $appliedleavescount . '</td>
								                      </tr>
								
								
								                       <tr bgcolor="#D9DCE5">
								                          <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Leave Type </td>
								                          <td style="color:#5b5b5b">' . $leavetypetext . '</td>
								                       </tr>
								
								
								                      <tr bgcolor="#F5F5F3">
								                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Remaining Leaves</td>
								                        <td style="color:#5b5b5b">' . $leavetypecount . '</td>
								                      </tr>
								                      <tr bgcolor="#D9DCE5">
								                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
								                        <td style="color:#5b5b5b">' . $from_date . '</td>
								                      </tr>
								                      <tr bgcolor="#F5F5F3">
								                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
								                        <td style="color:#5b5b5b">' . $to_date . '</td>
								                  </tr>
								                      <tr bgcolor="#D9DCE5">
								                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Leave</td>
								                        <td style="color:#5b5b5b">' . $reason . '</td>
								                  </tr>
								                  <tr bgcolor="#F5F5F3">
								                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
								                        <td style="color:#5b5b5b">' . $reportingmanagerName . '</td>
								                  </tr>
								                </tbody>
				                        		</table>
            								</div>
            								<div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the leave details.</div>
            							</div>
            						</div>';
			$result = sapp_Global::_sendEmail ( $options );
		}
		/* END */
		/* Mail to HR */
		if (defined ( 'LV_HR_' . $businessunitid ) && $businessunitid != '') {
			$options ['subject'] = 'Leave request for approval (' .$leavetypetext .')';
			$options ['header'] = 'Leave Request ';
			$options ['toEmail'] = constant ( 'LV_HR_' . $businessunitid );
			$options ['toName'] = 'Leave management';
			$options ['message'] = '<div>
					<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">'. $message .' of the below employee is pending for approval:</div>
														</div>
<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="border:3px solid #BBBBBB; font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody><tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $leaveOwner['userfullname'] . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">' . $appliedleavescount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Leave Type </td>
                        <td style="color:#5b5b5b">' . $leavetypetext . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Remaining Leaves</td>
                        <td style="color:#5b5b5b">' . $leavetypecount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
                  </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Leave</td>
                        <td style="color:#5b5b5b">' . $reason . '</td>
                  </tr>
                  <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
                        <td style="color:#5b5b5b">' . $reportingmanagerName . '</td>
                  </tr>
                </tbody></table>

            </div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the leave details.</div>
            </div>
            </div>';
			// $options['cron'] = 'yes';
			$result = sapp_Global::_sendEmail ( $options );
		}

		/* END */
		/* Mail to the applied employee */

		$message = "A leave request raised by";
		if($loginUserId != $leaveOwner['id']){
		    $message .= " $userfullname on your behalf";
		}else{
		    $message .= " you";
		}
		$toemailArr = $employeeemail;
		$options ['subject'] = 'Leave request for approval (' .$leavetypetext .')';
		$options ['header'] = 'Leave Request';
		$options ['toEmail'] = $leaveOwner['emailaddress'];
		$options ['toName'] = $leaveOwner['userfullname'];
		$options ['message'] = '<div>
				<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">'. $message .' is sent for your manager&rsquo;s approval.</div>
				</div>
<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					  <tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $leaveOwner['userfullname'] . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No.of Day(s)</td>
                        <td style="color:#5b5b5b">' . $appliedleavescount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                         <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Leave Type </td>
                         <td style="color:#5b5b5b">' . $leavetypetext . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
            	     </tr>
    	             <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Leave</td>
                        <td style="color:#5b5b5b">' . $reason . '</td>
                     </tr>
                </tbody></table>

            </div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the leave details.</div>
            </div>
            </div>';
		$result = sapp_Global::_sendEmail ( $options );
	}
	function calcBusinessDays($dDate1, $dDate2, $constantday) {
		$iWeeks = '';
		$iDateDiff = '';
		$iAdjust = 0;
		if (strtotime ( $dDate2 ) - strtotime ( $dDate1 ) < 0) {
			return "From date should be less than To date"; // error code if dates transposed
		}
		$iWeekday1 = date ( "j", strtotime ( $dDate1 ) );
		$iWeekday2 = date ( "j", strtotime ( $dDate2 ) );
		$iWeekday1 = ($iWeekday1 == 0) ? 7 : $iWeekday1; // change Sunday from 0 to 7
		$iWeekday2 = ($iWeekday2 == 0) ? 7 : $iWeekday2;
		if (($iWeekday1 > $constantday) && ($iWeekday2 > $constantday))
			$iAdjust = 1; // adjustment if both days on weekend

		$iWeekday1 = ($iWeekday1 > $constantday) ? $constantday : $iWeekday1; // only count weekdays
		$iWeekday2 = ($iWeekday2 > $constantday) ? $constantday : $iWeekday2;

		// calculate differnece in weeks ( 60sec * 60min * 24hrs * 7 days = 604800)

		$iWeeks = floor ( (strtotime ( $dDate2 ) - strtotime ( $dDate1 )) / 604800 );

		if ($iWeekday1 <= $iWeekday2) {
			$iDateDiff = ($iWeeks * $constantday) + ($iWeekday2 - $iWeekday1);
		} else {
			$iDateDiff = (($iWeeks + 1) * $constantday) - ($iWeekday1 - $iWeekday2);
		}

		$iDateDiff -= $iAdjust; // take into account both days on weekend

		return ($iDateDiff + 1); // add 1 because dates are inclusive
	}

	function gethalfdaydetailsAction() {
		$this->_helper->layout->disableLayout ();
		$result ['result'] = '';
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		$employeesmodel = new Default_Model_Employees ();
		$leavemanagementmodel = new Default_Model_Leavemanagement ();
		$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );
		$ishalf_day = '';
		if (! empty ( $loggedInEmployeeDetails )) {
			$employeeDepartmentId = $loggedInEmployeeDetails [0] ['department_id'];
			$weekendDatailsArr = $leavemanagementmodel->getWeekendDetails ( $employeeDepartmentId );
			if (! empty ( $weekendDatailsArr ))
				$ishalf_day = $weekendDatailsArr [0] ['is_halfday'];
			else
				$ishalf_day = 'error';
		}
		$result ['result'] = $ishalf_day;
		$this->_helper->_json ( $result );
	}
	public function editpopupAction() {
		Zend_Layout::getMvcInstance ()->setLayoutPath ( APPLICATION_PATH . "/layouts/scripts/popup/" );
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		$leavedetail = $this->getRequest ()->getParam ( 'id' );
		$id = substr($leavedetail, 0, strpos($leavedetail, '_'));;
		$compoff = substr($leavedetail, strpos($leavedetail, '_') + 1);;
		$userid = $this->getRequest ()->getParam ( 'unitId' );
		if ($id == '')
			$id = $loginUserId;

		$leaverequestmodel = new Default_Model_Leaverequest ();
		$leaverequestform = new Default_Form_leaverequest ();
		$compoffrequestmodel = new Default_Model_Compoffrequest();
		$user_logged_in = 'true';
		$manager_logged_in = 'false';
		$cancel_flag = 'true';
		$approve_flag = 'true';
		$reject_flag = 'true';
		$loginUserRole = ($leaverequestmodel->getEmployeeRole ( $loginUserId ));
		if ($id && is_numeric ( $id ) && $id > 0) {
			if ($compoff != '1')
				$leave_details = $leaverequestmodel->getLeaveDetails ( $id );
			else 
			{
				$leave_details = $compoffrequestmodel->getCompoffRequestDetails($id);
				$usersModel = new Default_Model_Users();
				$leave_details['0']['user_name'] = $usersModel->getUserFullName($leave_details['0']['user_id']);
				$leave_details['0']['rep_manager_name'] = $usersModel->getUserFullName($leave_details['0']['rep_mang_id']);
				$leave_details['0']['leavetype_name'] = 'Compensatory Off';
			}
			if (! empty ( $leave_details )) {
				$leave_details = call_user_func_array ( 'array_merge', $leave_details );

				if ($leave_details ['user_id'] == $loginUserId) {
					if ($compoff == '1')
					{
						$leave_details ['leavestatus'] = $leave_details ['status'];
						$leave_details ['from_date'] = $leave_details ['leave_date'];
						$leave_details ['to_date'] = $leave_details ['leave_date'];
					}
					if ($leave_details ['leavestatus'] == 'Approved') {
						if (isset ( $leave_details ['from_date'] )) {
							$leaveDate = date ( $leave_details ['from_date'] );
							$todayDate = date ( "Y-m-d" );
							if (strtotime ( $todayDate ) >= strtotime ( $leaveDate )) {
								$cancel_flag = 'false';
							}
						}
					}
					$approve_flag = 'false';
					$reject_flag = 'false';
				}

				if ($leave_details ['rep_mang_id'] == $loginUserId || $leave_details ['hr_id'] == $loginUserId || $loginUserRole == '1') {
					if ($leave_details ['leavestatus'] == 'Approved') {
						$approve_flag = 'false';
						$reject_flag = 'false';
					}
					$manager_logged_in = 'true';
				}
			}
		} else {
			$this->view->rowexist = "norows";
		}

		$this->view->form = $leaverequestform;
		$this->view->controllername = 'leaverequest';
		$this->view->leave_details = $leave_details;
		$this->view->user_logged_in = $user_logged_in;
		$this->view->manager_logged_in = $manager_logged_in;
		$this->view->cancel_flag = $cancel_flag;
		$this->view->approve_flag = $approve_flag;
		$this->view->reject_flag = $reject_flag;
	}


	public function updateleavedetailsAction() {
		$this->_helper->layout->disableLayout ();
		$result ['result'] = 'success';
		$result ['msg'] = '';
		$leavestatus = '';
		$subject = '';
		$message = '';
		$successmsg = '';
		$actionflag = 2;
		$user_logged_in = 'true';
		$manager_logged_in = 'false';
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}

		$id = $this->_request->getParam ( 'id' );
		$status = $this->_request->getParam ( 'status' );
		$comments = $this->_request->getParam ( 'comments' );
		$notify_me_on = $this->_request->getParam ( 'notify_me_on' );
		$leaverequestmodel = new Default_Model_Leaverequest ();
		$empleavesummarymodel = new Default_Model_EmpLeaveSummary ();

		$employeeleavetypesmodel = new Default_Model_Employeeleavetypes ();
		$usersmodel = new Default_Model_Users ();
		$loginUserRole = ($leaverequestmodel->getEmployeeRole ( $loginUserId ));
		$loginUserName = ($leaverequestmodel->getUserName ( $loginUserId ));
		if ($id && is_numeric ( $id ) && $id > 0) {
			$leave_details = $leaverequestmodel->getLeaveDetails ( $id );
			$leave_request_details = $leaverequestmodel->getLeaveRequestDetails($id);
			if($leave_request_details[0]['parent'] != null){
			    $parentLeaveReqData = $leaverequestmodel->getParentLeaveRequestDetails($leave_request_details[0]['parent']);
			    $parentLeaveData = $employeeleavetypesmodel->getLeavetypeDataByID($parentLeaveReqData[0]['leavetypeid']);
			}
			if (! empty ( $leave_details )) {
				$leave_details = call_user_func_array ( 'array_merge', $leave_details );
				$leavetypeArr = $employeeleavetypesmodel->getLeavetypeDataByID ( $leave_details ['leavetypeid'] );
				if ($leave_details ['user_id'] == $loginUserId) {
					if (sapp_Global::_decrypt ( $status ) == 'Cancelled') {
						$leavestatus = 4;
						if (! empty ( $leavetypeArr )) {
							if ($leavetypeArr [0] ['leavepredeductable'] == 1) {
								$updateemployeeleave = $leaverequestmodel->updatecancelledemployeeleaves ( $leave_details ['appliedleavescount'], $leave_details ['user_id'], $leave_details ['leavetypeid'] );
							}
						}
						if($leave_request_details[0]['parent'] != null){
						    $pData = array('isactive'=>1);
						    $pWhere = array('id=?'=>$parentLeaveReqData[0]['id']);

						    $leaverequestmodel->SaveorUpdateLeaveRequest($pData, $pWhere);

						    if ($parentLeaveData[0] ['leavepredeductable'] == 1) {
						        $updateemployeeleave = $leaverequestmodel->updateemployeeleaves ( $parentLeaveReqData [0]['appliedleavescount'], $parentLeaveReqData [0]['user_id'], $parentLeaveReqData [0]['leavetypeid'] );
						    }

						}
						$successmsg = 'Leave request cancelled succesfully.';
						$subject = 'Leave request cancelled';
						$message = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
								<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;>The below leave(s) has been cancelled by ' . $loginUserName . '.</div>
										</div>';
					}
				} elseif ($leave_details ['rep_mang_id'] == $loginUserId || ($leave_details ['hr_id'] == $loginUserId) || $loginUserRole == '1') {
					if (sapp_Global::_decrypt ( $status ) == 'Cancelled') {
						$leavestatus = 4;
						if (! empty ( $leavetypeArr )) {
							if ($leavetypeArr [0] ['leavepredeductable'] == 1) {
							    $updateemployeeleave = $leaverequestmodel->updatecancelledemployeeleaves ( $leave_details ['appliedleavescount'], $leave_details ['user_id'], $leave_details ['leavetypeid'] );
							}
						}
						if($leave_request_details[0]['parent'] != null){
						    $pData = array('isactive'=>1);
						    $pWhere = array('id=?'=>$parentLeaveReqData[0]['id']);

						    $leaverequestmodel->SaveorUpdateLeaveRequest($pData, $pWhere);

						    if ($parentLeaveData[0] ['leavepredeductable'] == 1) {
						        $updateemployeeleave = $leaverequestmodel->updateemployeeleaves ( $parentLeaveReqData [0]['appliedleavescount'], $parentLeaveReqData [0]['user_id'], $parentLeaveReqData [0]['leavetypeid'] );
						    }

						}
						$successmsg = 'Leave request cancelled succesfully.';
						$subject = 'Leave request cancelled';
						$message = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
								<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below leave(s) has been cancelled by ' . $loginUserName . '.</div>
										</div>';
					} elseif (sapp_Global::_decrypt ( $status ) == 'Approved') {
						$leavestatus = 2;
						$successmsg = 'Leave request approved succesfully.';
						$subject = 'Leave request approved';
						$message = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
								<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below leave(s) has been approved by ' . $loginUserName . '.</div>
										</div>';
					} elseif (sapp_Global::_decrypt ( $status ) == 'Rejected') {
						$leavestatus = 3;
						if (! empty ( $leavetypeArr )) {
						    if ($leavetypeArr [0] ['leavepredeductable'] == 1) {
						        $updateemployeeleave = $leaverequestmodel->updatecancelledemployeeleaves ( $leave_details ['appliedleavescount'], $leave_details ['user_id'], $leave_details ['leavetypeid'] );
						    }
						}
						if($leave_request_details[0]['parent'] != null){
						    $pData = array('isactive'=>1);
						    $pWhere = array('id=?'=>$parentLeaveReqData[0]['id']);

						    $leaverequestmodel->SaveorUpdateLeaveRequest($pData, $pWhere);

						    if ($parentLeaveData[0] ['leavepredeductable'] == 1) {
						        $updateemployeeleave = $leaverequestmodel->updateemployeeleaves ( $parentLeaveReqData [0]['appliedleavescount'], $parentLeaveReqData [0]['user_id'], $parentLeaveReqData [0]['leavetypeid'] );
						    }

						}
						$successmsg = 'Leave request rejected succesfully.';
						$subject = 'Leave request rejected';
						$message = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
								<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;>The below leave(s) has been rejected by ' . $loginUserName . '.</div>
										</div>';
					}
					$manager_logged_in = 'true';
				}

				if (! empty ( $leavestatus )) {
					$data = array (
							'leavestatus' => $leavestatus,
							'approver_comments' => ! empty ( $comments ) ? $comments : NULL,
							'notify_me_on' => $notify_me_on,
							'modifiedby' => $loginUserId,
							'modifieddate' => gmdate ( "Y-m-d H:i:s" )
					);
					$where = array (
							'id=?' => $id
					);

					if($leave_request_details[0]['parent'] != null && $leavestatus != 2){
					  $data['isactive'] = 0;
					}

					$Id = $leaverequestmodel->SaveorUpdateLeaveRequest ( $data, $where );

					/**
					 * leave request history
					 */
					if ($Id == 'update') {
						$leave_status = sapp_Global::_decrypt ( $status );
						if ($leave_status == 'Approved') {
							$leavestatus = 'Approved';
						} else if ($leave_status == 'Rejected') {
							$leavestatus = 'Rejected';
						} else {
							$leavestatus = 'Cancelled';
						}
						$history = 'Leave Request has been ' . $leavestatus . ' by ';
						$leaverequesthistory_model = new Default_Model_Leaverequesthistory ();
						$leave_history = array (
								'leaverequest_id' => $id,
								'description' => $history,
								'createdby' => $loginUserId,
								'modifiedby' => $loginUserId,
								'isactive' => 1,
								'createddate' => gmdate ( "Y-m-d H:i:s" ),
								'modifieddate' => gmdate ( "Y-m-d H:i:s" )
						);
						$where = '';
						$leavehistory = $leaverequesthistory_model->saveOrUpdateLeaveRequestHistory ( $leave_history, $where );
					}

					$menuID = ($manager_logged_in == 'true') ? MANAGEREMPLOYEEVACATIONS : PENDINGLEAVES;
					sapp_Global::logManager ( $menuID, $actionflag, $loginUserId, $id );
					$result ['msg'] = $successmsg;
				}
			}
		} else {
			$result ['result'] = 'fail';
			$result ['msg'] = '';
		}

		$this->_helper->json ( $result );
	}

	public function compoffformAction(){
	    Zend_Layout::getMvcInstance ()->setLayoutPath ( APPLICATION_PATH . "/layouts/scripts/popup/" );
	    $auth = Zend_Auth::getInstance ();
	    if ($auth->hasIdentity ()) {
	    	$loginUserId = $auth->getStorage ()->read ()->id;
	    }
	    $myattendanceModel = new Default_Model_Myattendance();
	    $businessunitid = $myattendanceModel->getEmployeeBusinessUnit($loginUserId);
	    $compensatoryOffModel = new Default_Model_CompensatoryOff();
	    $leaveWindow = $compensatoryOffModel->getCompensatoryOffLeaveWindow($businessunitid);
	    $halfDay = $compensatoryOffModel->getCompensatoryHalfDay($businessunitid);
	    $fullDay = $compensatoryOffModel->getCompensatoryFullDay($businessunitid);
	    $date = $this->getRequest()->getParam('date');
	    $this->view->form = new Default_Form_CompOffApplication();
	    $this->view->date = $date;
        $startDate = new DateTime($date);
        $workingHours = 0;
        $empWorkActivityModel = new Default_Model_EmployeeWorkActivity ();
        $loggedActivities = $empWorkActivityModel->getEmployeeActivities($startDate, $loginUserId, $workingHours, false);
	    $this->view->workingHours = $workingHours;
	    $this->view->leaveWindow = $leaveWindow;
	    $this->view->halfDay = $halfDay;
	    $this->view->fullDay = $fullDay;
	}

	public function compoffrequestAction(){
	    $this->_helper->layout->disableLayout ();
	    $auth = Zend_Auth::getInstance ();
	    if ($auth->hasIdentity ()) {
	        $loginUserId = $auth->getStorage ()->read ()->id;
	    }
	    $leaverequestform = new Default_Form_leaverequest ();
	    $leaverequestmodel = new Default_Model_Leaverequest ();

	    $usersmodel = new Default_Model_Users ();
	    $employeesmodel = new Default_Model_Employees ();
	    $leavemanagementmodel = new Default_Model_Leavemanagement ();
	    $compoffrequestform = new Default_Form_CompOffApplication();
	    $compensatoryoffmodel = new Default_Model_CompensatoryOff();
	    $myattendanceModel = new Default_Model_Myattendance();
	    
	    if ($loginUserId != '' && $loginUserId != NULL) {
	        $loggedinEmpId = $usersmodel->getUserDetailsByID ( $loginUserId );
	        $loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );

	        $employeeemail = $loggedinEmpId [0] ['emailaddress'];

	        if (! empty ( $loggedInEmployeeDetails )) {

	            $reportingmanagerId = $loggedInEmployeeDetails [0] ['reporting_manager'];
	            $reportingManagerDetails = $usersmodel->getUserDetailsByID ( $reportingmanagerId );
	            if (! empty ( $reportingManagerDetails )) {
	                $reportingManageremail = $reportingManagerDetails [0] ['emailaddress'];
	                $reportingmanagerName = $reportingManagerDetails [0] ['userfullname'];
	            }

	            $employeeDepartmentId = $loggedInEmployeeDetails [0] ['department_id'];
	            $configure_hr_id = $leavemanagementmodel->gethrDetails ( $employeeDepartmentId );
	            if (! empty ( $configure_hr_id )) {
	                $hr_id = $configure_hr_id [0] ['hr_id'];
	            }
	        }
	    }

	    $errorflag = 'true';
	    if($this->_request->getPost()){
	        $avail_date = $this->getRequest()->getParam('action_date');
	        $optiontext = $this->_request->getParam ( 'option' );
	        $isHalfDayCompoff = $this->_request->getParam ( 'isHalfDayCompoff' ); 
	        if($optiontext == 'Leave'){

	            $leavedate =  $this->_request->getParam ( 'leave_date' );
	            if(!$leavedate){
	                $msgarray ['leave_date'] = 'Please select leave date.';
	                $errorflag = 'false';
	            }

	            $dummyleaverequest = array("employeeemail" => $employeeemail,
	                "repmanageremail" => $reportingManageremail,
	                "ishalfday" => "1",
	                "reason" => " ",
	                "from_date" => $leavedate,
	                "to_date" => $leavedate,
	                "leaveday" => "1",
	                "appliedleavesdaycount" => "1",
	                "rep_mang_id" => $reportingmanagerName	);


	            $dummyleavetype = array('0' => array("id" => -1,
	                "leavetype" => "Compensatory off",
	                "leavecode" => COMPOFF,
	                "max_limit" => "1",
	            ));
	            $errorflag = $leaverequestmodel->validateLeaveRequest($dummyleaverequest, $loginUserId, $msgarray, $data, $dummyleavetype);

	            $option = 1;
	            $optiontext = $optiontext . " on " . $leavedate;
	        }else if($optiontext == 'Encashment'){
	            $option = 2;
	        }

	        if($errorflag == 'true' && $compoffrequestform->isValid($this->_request->getPost())){
	            $comment = $this->_request->getParam ( 'comment' );

	            //check if it is half day or full day
	            $businessunitId = $employeesmodel->getBusinessUnitId($loginUserId);
		    $compensatoryFullDay = $compensatoryoffmodel->getCompensatoryFullDay($businessunitId);
	            $workingHours = $myattendanceModel->getCombinedActiveEmployeeData($avail_date, $avail_date, $loginUserId, false)['0']['working_hours'];
		    if ($workingHours > $compensatoryFullDay)
	            	$appliedleavescount = 1;
	            else 
            		$appliedleavescount = 0.5;
	            
    	        $data = array('user_id' => $loginUserId,
    	            'option' => $option,
    	            'avail_date' => $avail_date,
    	            'leave_date' => $leavedate,
    	            'status' => 1,
    	            'rep_mang_id' => $reportingmanagerId,
    	            'hr_id' => $hr_id,
    	            'user_comments' => $comment,
    	            'createdby' => $loginUserId,
    	            'modifiedby' => $loginUserId,
    	            'createddate' => gmdate ( "Y-m-d H:i:s" ),
    	            'modifieddate' => gmdate ( "Y-m-d H:i:s" ),
    	            'isactive' => 1,
    	        	'appliedleavescount' => $appliedleavescount
    	        );

    	        $compoffrequestmodel = new Default_Model_Compoffrequest();
    	        $id = $compoffrequestmodel->SaveorUpdateCompoffRequest($data, '');

    	        $data['optiontext'] = $optiontext;
    	        $data['id'] = $id;

    	        $this->_helper->json ( array (
    	            'result' => 'saved',
    	            'message' => 'Compensatory Off request raised successfully.',
    	            'controller' => 'compoffrequest',
    	            'compoffrequest' => $data
    	        ) );

	        }else{
	            $messages = $compoffrequestform->getMessages();
	            if (isset ( $msgarray ['rep_mang_id'] )) {

	                $messages ['leave_date'] = array (
	                    $msgarray ['rep_mang_id']
	                );
	            }
	            if (isset ( $msgarray ['from_date'] )) {

	                $messages ['leave_date'] = array (
	                    $msgarray ['from_date']
	                );
	            }
	            if (isset ( $msgarray ['to_date'] )) {

	                $messages ['leave_date'] = array (
	                    $msgarray ['to_date']
	                );
	            }
	            if (isset ( $msgarray ['leaveday'] )) {

	                $messages ['leave_date'] = array (
	                    $msgarray ['leaveday']
	                );
	            }
	            if (isset ( $msgarray ['leavetypeid'] )) {

	                $messages ['leave_date'] = array (
	                    $msgarray ['leavetypeid']
	                );
	            }
	            if (isset ( $msgarray ['no_of_days'] )) {

	                $messages ['leave_date'] = array (
	                    $msgarray ['no_of_days']
	                );
	            }
	            if (isset ( $msgarray ['appliedleavesdaycount'] )) {

	                $messages ['leave_date'] = array (
	                    $msgarray ['appliedleavesdaycount']
	                );
	            }
	            if (isset ( $msgarray ['leave_date'] )) {

	                $messages ['leave_date'] = array (
	                    $msgarray ['leave_date']
	                );
	            }
	            if (isset ( $msgarray ['comment'] )) {

	                $messages ['comment'] = array (
	                    $msgarray ['comment']
	                );
	            }
	            if (isset ( $msgarray ['option'] )) {

	                $messages ['option'] = array (
	                    $msgarray ['option']
	                );
	            }
	            $messages ['result'] = 'error';
	            $this->_helper->json ( $messages );
	        }

	    }
	}

	public function compoffemailconfirmationAction(){
	    session_write_close ();
	    $auth = Zend_Auth::getInstance ();
	    if ($auth->hasIdentity ()) {
	        $loginUserId = $auth->getStorage ()->read ()->id;
	    }
	    if (! $this->getRequest ()->getPost ()) {
	        return;
	    }
	    $compoffrequest = $this->getRequest ()->getPost ();
	    $usersmodel = new Default_Model_Users ();
	    $employeesmodel = new Default_Model_Employees ();
	    if ($loginUserId != '' && $loginUserId != NULL) {
	        $loggedinEmpId = $usersmodel->getUserDetailsByID ( $loginUserId );
	        $loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );

	        $compoffOwnerDetails = $employeesmodel->getLoggedInEmployeeDetails ( $compoffrequest['user_id'] );

	        $compoffOwner = $usersmodel->getUserDetailsByID ( $compoffrequest['user_id'] )['0'];

	        $reportingmanagerId = $compoffOwnerDetails [0] ['reporting_manager'];
	        $businessunitid = $compoffOwnerDetails [0] ['businessunit_id'];
	        $employeeemail = $loggedinEmpId [0] ['emailaddress'];
	        $userfullname = $loggedinEmpId [0] ['userfullname'];

	        $reportingManagerDetails = $usersmodel->getUserDetailsByID ( $reportingmanagerId );
	        if (! empty ( $reportingManagerDetails )) {
	            $reportingManageremail = $reportingManagerDetails [0] ['emailaddress'];
	            $reportingmanagerName = $reportingManagerDetails [0] ['userfullname'];
	        }
	    } else {
	        return;
	    }

	    $avail_date = $compoffrequest ['avail_date'];
	    $leave_date = $compoffrequest ['leave_date'];
	    $comment = $compoffrequest ['user_comments'];
	    $optiontext = $compoffrequest ['optiontext'];

	    $toemailArr = $reportingManageremail; // $employeeemail
	    $message = "The Compensatory Off request, against " . $avail_date . ",";
	    if($loginUserId != $compoffOwner['id']){
	        $message .= ", raised by $userfullname, on behalf";
	    }
	    if (! empty ( $toemailArr )) {
	        $options ['subject'] = 'Compensatory Off request for approval';
	        $options ['header'] = 'Compensatory Off Request';
	        $options ['toEmail'] = $toemailArr;
	        $options ['toName'] = $reportingmanagerName;
	        $options ['message'] = '<div>
	        		<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">'. $message .' of the below employee is pending for approval:</div>
												</div>
														<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
						<tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $compoffOwner['userfullname'] . '</td>
                      </tr>

                       <tr bgcolor="#F5F5F3">
                          <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reimbursement Option </td>
                          <td style="color:#5b5b5b">' . $optiontext . '</td>
                       </tr>

                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Employee Comment</td>
                        <td style="color:#5b5b5b">' . $comment . '</td>
                  </tr>
                  <tr  bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
                        <td style="color:#5b5b5b">' . $reportingmanagerName . '</td>
                  </tr>
                </tbody></table>';

	        $result = sapp_Global::_sendEmail ( $options );
	    }
	    /* END */
	    /* Mail to HR */
	    if (defined ( 'LV_HR_' . $businessunitid ) && $businessunitid != '') {
	        $options ['subject'] = 'Compensatory Off request for approval';
	        $options ['header'] = 'Compensatory Off Request ';
	        $options ['toEmail'] = constant ( 'LV_HR_' . $businessunitid );
	        $options ['toName'] = 'Leave management';
	        $options ['message'] = '<div>
	        		<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">'. $message .' of the below employee is pending for approval:</div>
</div>
<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					  <tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $compoffOwner['userfullname'] . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reimbursement Option </td>
                        <td style="color:#5b5b5b">' . $optiontext . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Employee Comment</td>
                        <td style="color:#5b5b5b">' . $comment . '</td>
                  </tr>
                  <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
                        <td style="color:#5b5b5b">' . $reportingmanagerName . '</td>
                  </tr>
                </tbody></table>';
	        $result = sapp_Global::_sendEmail ( $options );
	    }

	    /* END */
	    /* Mail to the applied employee */

	    $message = "A Compensatory Off request, against " . $avail_date . ",". " raised by";
	    if($loginUserId != $compoffOwner['id']){
	        $message .= " $userfullname on your behalf";
	    }else{
	        $message .= " you";
	    }
	    $toemailArr = $employeeemail;
	    $options ['subject'] = 'Compensatory Off request for approval';
	    $options ['header'] = 'Compensatory Off Request';
	    $options ['toEmail'] = $compoffOwner['emailaddress'];
	    $options ['toName'] = $compoffOwner['userfullname'];
	    $options ['message'] = '<div>
	    		<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">'. $message .' is sent for your manager&rsquo;s approval.</div>
				</div>
				<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
						<tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff"">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $compoffOwner['userfullname'] . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                          <td style="border-right:2px solid #BBBBBB; color:#5b5b5b""> Reimbursement Option </td>
                          <td style="color:#5b5b5b">' . $optiontext . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b"">Employee Comment</td>
                        <td style="color:#5b5b5b">' . $comment . '</td>
                  	  </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
                        <td style="color:#5b5b5b">' . $reportingmanagerName . '</td>
                  	  </tr>
                </tbody></table>';
	    $result = sapp_Global::_sendEmail ( $options );
	}
}

