<?php
class Default_AttendancereportController extends Zend_Controller_Action {
	private $options;
	private $attendance = array ();
	public function preDispatch() {
		$ajaxContext = $this->_helper->getHelper ( 'AjaxContext' );
	}
	public function init() {
		$this->_options = $this->getInvokeArg ( 'bootstrap' )->getOptions ();
	}
	public function indexAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		
		$attendencereportform = new Default_Form_attendancereport ();
		$this->view->form = $attendencereportform;
	}
	public function downloadreportAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		$startDate = date ( DATEFORMAT_PHP, strtotime ( $this->getRequest ()->getPost () ['fromdate'] ) );
		$endDate = date ( DATEFORMAT_PHP, strtotime ( $this->getRequest ()->getPost () ['todate'] ) );
		$this->_helper->viewRenderer->setNoRender ();
		$this->_helper->layout->disableLayout ();
		
		require_once 'Classes/PHPExcel.php';
		require_once 'Classes/PHPExcel/IOFactory.php';
		$objPHPExcel = new PHPExcel ();
		$columns = range ( 'A', 'Z' );
		$fileName = 'AttendanceReport.xlsx';
		$mainSheet = 'Dashboard';
		$cellName = '';
		$colIndex = 0;
		$columnNames = array (
				"Name",
				"Miss Punch",
				"Absent",
				"Attendence Exception",
				"Leaves and Holidays",
				"Work From Home",
				"Comp Off",
				"Working Days",
				"Total Weekends" 
		);
		$style = array (
				'alignment' => array (
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER 
				) 
		);
		$objPHPExcel->getActiveSheet ()->setCellValue ( 'A1', 'Report Duration :' );
		$objPHPExcel->getActiveSheet ()->setCellValue ( 'B1', $startDate . ' To ' . $endDate );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'A1' )->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'B1' )->getFont ()->setBold ( true );
		// Make first row Headings bold and highlighted in Excel.
		
		$objPHPExcel->getActiveSheet ()->setTitle ( $mainSheet );
		$objPHPExcel->getActiveSheet ()->getDefaultStyle ()->applyFromArray ( $style );
		foreach ( $columnNames as $columnName ) {
			$row = 3;
			$cellName = $columns [$colIndex] . $row;
			
			$objPHPExcel->getActiveSheet ()->setCellValue ( $cellName, $columnName );
			$objPHPExcel->getActiveSheet ()->getStyle ( $cellName )->getFont ()->setBold ( true );
			$objPHPExcel->getActiveSheet ()->getColumnDimension ( $columns [$colIndex] )->setAutoSize ( true );
			$colIndex ++;
		}
		
		// Display field/column values in Excel.
		$row = 4;
		foreach ( $_SESSION ['attendancereport'] as $employeeId => $attendanceData ) {
			$colIndex = 0;
			foreach ( $attendanceData as $attendanceDatum ) {
				$cellName = $columns [$colIndex] . $row;
				$objPHPExcel->getActiveSheet ()->SetCellValue ( $cellName, $attendanceDatum );
				if ($columns [$colIndex] == 'A') {
					$objPHPExcel->createSheet ()->setTitle ( $attendanceDatum );
					$objPHPExcel->getSheetByName ( $attendanceDatum )->getDefaultStyle ()->applyFromArray ( $style );
					$objPHPExcel->getActiveSheet ()->getCell ( $cellName )->getHyperlink ()->setUrl ( "sheet://'" . $attendanceDatum . "'!A2" );
					$objPHPExcel->getActiveSheet ()->getStyle ( $cellName )->applyFromArray ( array (
							'font' => [ 
									'color' => [ 
											'rgb' => '0000FF' 
									],
									'underline' => 'single' 
							] 
					) );
					
					$this->fillAttendenceSheet ( $employeeId, $attendanceDatum, $objPHPExcel, $startDate, $endDate );
					
					$objPHPExcel->setActiveSheetIndexByName ( $mainSheet );
				}
				$colIndex ++;
			}
			$row ++;
		}
		
		sapp_Global::clean_output_buffer ();
		header ( 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
		header ( "Content-Disposition: attachment; filename=\"$fileName\"" );
		header ( 'Cache-Control: max-age=0' );
		sapp_Global::clean_output_buffer ();
		
		$objWriter = PHPExcel_IOFactory::createWriter ( $objPHPExcel, 'Excel2007' );
		$objWriter->save ( 'php://output' );
		
		exit ();
	}
	public function fillAttendenceSheet($employeeId, $sheetName, &$objPHPExcel, $startDate, $endDate) {
		$attendenceModel = new Default_Model_Myattendance ();
		$employeeExceptionModel = new Default_Model_ExceptionRequest ();
		$employeeModel = new Default_Model_Employee ();
		$officehoursModel = new Default_Model_OfficeHours ();
		$columns = range ( 'A', 'Z' );
		
		$employeeInfo = $employeeModel->getActiveEmployeeData ( $employeeId );
		if ($employeeInfo [0] ['businessunit_id'])
			$dailyruleOfficeHours = $officehoursModel->getOfficeHoursRule ( $employeeInfo [0] ['businessunit_id'] );
		$attendenceData = $attendenceModel->getEmployeeAttendence ( $employeeId, $startDate, $endDate, $employeeExceptionModel, false );
		$columnNames = array (
				"Date",
				"Punch-In",
				"Punch-Out",
				"Status",
				"Work Hours",
				"Day Status" 
		);
		$colIndex = 0;
		$objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( 'A1', 'Back to Dashboard' );
		$objPHPExcel->getSheetByName ( $sheetName )->getCell ( 'A1' )->getHyperlink ()->setUrl ( "sheet://'Dashboard'!A2" );
		$objPHPExcel->getSheetByName ( $sheetName )->getStyle ( 'A1' )->applyFromArray ( array (
				'font' => [ 
						'color' => [ 
								'rgb' => '0000FF' 
						],
						'underline' => 'single' 
				] 
		) );
		
		// Adding Headings to the sheet.
		foreach ( $columnNames as $columnName ) {
			$row = 3;
			$cellName = $columns [$colIndex] . $row;
			$objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $columnName );
			$objPHPExcel->getSheetByName ( $sheetName )->getStyle ( $cellName )->getFont ()->setBold ( true );
			$objPHPExcel->getSheetByName ( $sheetName )->getColumnDimension ( $columns [$colIndex] )->setAutoSize ( true );
			$colIndex ++;
		}
		$row = 4;

		foreach ( $attendenceData as $key => $attendence ) {
			if ($dailyruleOfficeHours != null) {
				// Validate office hours for status??
			}
			$colIndex = 0;
			foreach ( $columnNames as $columnName ) {
				$cellName = $columns [$colIndex] . $row;
				if (strcasecmp ( 'date', $columnName ) == 0) {
					$objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $attendence ['punch_date'] );
				} elseif (strcasecmp ( 'Punch-In', $columnName ) == 0) {
					if (isset ( $attendence ['check_in'] )) {
						$objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $attendenceData [$key] ['check_in'] );
					}
				} elseif (strcasecmp ( 'Punch-Out', $columnName ) == 0) {
					if (isset ( $attendence ['check_out'] )) {
						$objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $attendenceData [$key] ['check_out'] );
					}
				} elseif (strcasecmp ( 'Status', $columnName ) == 0) {
					if (isset ( $attendence ['status'] )) {
						$objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $attendence ['status'] );
					}
				} elseif (strcasecmp ( 'Work Hours', $columnName ) == 0) {
					if (isset ( $attendence ['working_hours'] )) {
						$objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $attendenceData [$key] ['working_hours'] );
					}
				} elseif (strcasecmp ( 'Day Status', $columnName ) == 0) {
					if (isset ( $attendence ['overall_status'] )) {
						$objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $attendence ['overall_status'] );
					}
				}
				
				$colIndex ++;
			}
			$row ++;
		}
	}

	public function CheckValidWorkingDay($userId, $onDate, $lwpId, $wfhId) {
        $employeeLeaveRequestModel = new Default_Model_Leaverequest ();
        $employeeAttendanceModel = new Default_Model_Myattendance ();
        $employeeExceptionModel = new Default_Model_ExceptionRequest ();
        $employeeHolidaysModel = new Default_Model_Holidaydates ();

        // Check Valid Holiday
        if($employeeHolidaysModel->checkHolidayForUser($onDate, $userId)) {
            return true;
        }

        // Check Valid Leave
        $leaveData = $employeeLeaveRequestModel->getSingleApprovedLeaveData($userId, $onDate);
        if($leaveData != null) {
            // LWP Check
            if ($leaveData [0] ['leavetypeid'] == $lwpId) {
                return false;
            } else if($leaveData [0] ['leavetypeid'] == $wfhId) {
                // Ignore WFH here for now.. Punch will be checked in below section
            } else {
                // valid leave
                return true;
            }
        }

        // Check Valid Exception
        if ($employeeExceptionModel->checkExceptionExist ( $userId, $onDate , $dayCount)) {
            return true;
        }

        // Check Valid Punch
        $loggedActivities = $employeeAttendanceModel->getLoggedActivityPerDay ( $onDate, $userId );
        if($loggedActivities != null && count($loggedActivities) > 0) {
            return true;
        }

        // No valid approval found
        return false;
    }

	public function getWeekends($id, $lwpId, $wfhId, $fromDate, $toDate) {
		$validWeekends = 0;
		
		while ( $fromDate <= $toDate ) {
			$day = $fromDate->format ( 'w' );
			if ($day == 6 || $day == 0) {
				if ($day == 6) {
					$friday = date_create_from_format ( 'Y-m-d H:i:s', $fromDate->format ( 'Y-m-d H:i:s' ) );
					$friday->modify ( '-1 day' );
					$weekday = date_create_from_format ( 'Y-m-d H:i:s', $fromDate->format ( 'Y-m-d H:i:s' ) );
					$weekday->modify ( '-5 day' );
				} else {
					$friday = date_create_from_format ( 'Y-m-d H:i:s', $fromDate->format ( 'Y-m-d H:i:s' ) );
					$friday->modify ( '-2 day' );
					$weekday = date_create_from_format ( 'Y-m-d H:i:s', $fromDate->format ( 'Y-m-d H:i:s' ) );
					$weekday->modify ( '-6 day' );
				}
				$flag = false;
				while ( $weekday <= $friday ) {
				    if($this->CheckValidWorkingDay($id, $weekday->format ( 'Y-m-d' ), $lwpId, $wfhId)) {
				        $flag = true;
				        break;
                    } else {
                        $weekday->modify ( '+1 day' );
                    }
				}
				if ($flag) {
					
					if ($day == 6) {
						$monday = date_create_from_format ( 'Y-m-d H:i:s', $fromDate->format ( 'Y-m-d H:i:s' ) );
						$monday->modify ( '+2 day' );
					} else {
						$monday = date_create_from_format ( 'Y-m-d H:i:s', $fromDate->format ( 'Y-m-d H:i:s' ) );
						$monday->modify ( '+1 day' );
					}

                    if($this->CheckValidWorkingDay($id, $friday->format ( 'Y-m-d' ), $lwpId, $wfhId) || $this->CheckValidWorkingDay($id, $monday->format ( 'Y-m-d' ), $lwpId, $wfhId)) {
                            $validWeekends ++;
                            if ($day == 6 && $fromDate < $toDate) {
                                $validWeekends ++;
                                $fromDate->modify ( '+1 day' );
                            }
                    } else if ($day == 6 && $fromDate < $toDate) {
					    $fromDate->modify('+1 day');
                    }
				} else {
					if ($day == 6 && $fromDate < $toDate) {
						$fromDate->modify ( '+1 day' );
					}
				}
			}
			$fromDate->modify ( '+1 day' );
		}
		return $validWeekends;
	}
	public function preparereportAction() {
		$this->_helper->viewRenderer->setNoRender ();
		$this->_helper->layout->disableLayout ();
		
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		
		$fromDate = $this->_request->getParams () ['fromdate'];
		$toDate = $this->_request->getParams () ['todate'];
		
		$employeeModel = new Default_Model_Employee ();
		$employeesModel = new Default_Model_Employees ();
		$employeeleaverequestModel = new Default_Model_Leaverequest ();
		$employeeAttendanceModel = new Default_Model_Myattendance ();
		$employeeExceptionModel = new Default_Model_ExceptionRequest ();
		$employeeholidaysModel = new Default_Model_Holidaydates ();
		$employeeleaveTypeModel = new Default_Model_Employeeleavetypes ();
		$mandatoryhoursRulesModel = new Default_Model_MandatoryHours ();
		$missedpunchesModel = new Default_Model_MissedPunches ();
		$compoffrequestModel = new Default_Model_Compoffrequest ();
		$officehoursModel = new Default_Model_OfficeHours ();
		$empWorkActivityModel = new Default_Model_EmployeeWorkActivity ();
		$compensatoryoffModel = new Default_Model_CompensatoryOff ();
		
		$attendancereport = array ();
		$employees = $employeeModel->getEmployeesAttendanceReport ( $loginUserId, $fromDate, $toDate );
		$totalDays = ((new DateTime ( $fromDate ))->diff ( (new DateTime ( $toDate )), true )->days) + 1;
		
		foreach ( $employees as $key => $employee ) {
			$id = $employee ['user_id'];
			$name = $employee ['userfullname'];
			$leavecount = 0;
			$userId = $employee ['user_id'];
			$workFromHome = 0;
			$loggedDays = array ();
			$loggedDayDetails = array ();
			$holidayGroup = $employeesModel->getHolidayGroupForEmployee ( $id );
			$holidays = $employeeholidaysModel->getHolidaysBetween ( $fromDate, $toDate, $holidayGroup ['0'] ['holiday_group'] ) [0] ['holidaydates'];
			$loggedActivities = $employeeAttendanceModel->getLoggedActivity ( $fromDate, $toDate, $userId );
			if ($employee ['businessunit_id'] != null) {
				$dailyruleOfficeHours = $officehoursModel->getOfficeHoursRule ( $employee ['businessunit_id'] );
				$dailyrule = $mandatoryhoursRulesModel->getMandatoryRule ( 'Daily', $employee ['businessunit_id'] );
				$halfdayrule = $mandatoryhoursRulesModel->getMandatoryRule ( 'Half Day', $employee ['businessunit_id'] );
				$weeklymandatoryhrsrule = $mandatoryhoursRulesModel->getMandatoryRule ( 'Weekly', $employee ['businessunit_id'] );
				$missedpunchesrule = $missedpunchesModel->getMispunchRule ( $employee ['businessunit_id'] );
				$compensatoryoffrule = $compensatoryoffModel->getCompensatoryOffRule ( $employee ['businessunit_id'] );
			}
			
			$timezone = $employeeAttendanceModel->getCurrentTimeZone ();
			$modifier = $employeeAttendanceModel->getModifierStringFromOfffset ( $timezone [0] ['offet_value'] );
            $punchDatePre = null;
			foreach ( $loggedActivities as $loggedActivity ) {
				$loggedTime = 0;
				$punchDate = (new DateTime ( $loggedActivity ['punch_date'] ))->format ( DATEFORMAT_PHP );
				
				$activitiesData = $empWorkActivityModel->getActivitiesBetween ( $userId, $punchDate, $dailyruleOfficeHours, $modifier );
				$day = $employeeAttendanceModel->getDayFromDate ( $punchDate );
				if ($activitiesData != null) {
					$dailyActivity = array ();
					$empWorkActivityModel->createActivityInterval ( $activitiesData, $dailyActivity, $modifier );
                    $dailyActivity = $empWorkActivityModel->combineConsecutiveOfficeActivities ( $dailyActivity );
					$workhoursTemp = $empWorkActivityModel->calculateTotalHours ( $dailyActivity );
					$workhours = number_format ( $workhoursTemp, 2 );
					$loggedActivity ['punch_date'] = $punchDate;
					$loggedActivity ['check_in'] = (new DateTime ( $dailyActivity [0] ['punchin'] ))->format ( "H:i:s" );
					if ($dailyActivity [(count ( $dailyActivity )) - 1] ['punchout'] != null) {
						$loggedActivity ['check_out'] = (new DateTime ( $dailyActivity [(count ( $dailyActivity )) - 1] ['punchout'] ))->format ( "H:i:s" );
					} else if(count ( $dailyActivity ) > 1 && $dailyActivity [(count ( $dailyActivity )) - 1] ['punchin'] != null) {
                        $loggedActivity ['check_out'] = (new DateTime ( $dailyActivity [(count ( $dailyActivity )) - 1] ['punchin'] ))->format ( "H:i:s" ); // Might want to get last office punch here instead
                    } else {
						$loggedActivity ['check_out'] = null;
					}

					
					$loggedTime += $employeeAttendanceModel->hoursToSeconds ( $workhours );
				}
				if ($punchDatePre != $punchDate) {
					array_push ( $loggedDays, $loggedActivity );
                    array_push($loggedDayDetails, $dailyActivity);
				}
				$punchDatePre = $punchDate;
			}
			
			$misspunch = 0;
			$leaves = $employeeleaverequestModel->getApprovedLeavesBetween ( $id, $fromDate, $toDate );
			$wfhId = $employeeleaveTypeModel->getEmployeeLeaveTypeByCode ( 'WFH' ) [0] ['leavetype_id'];
			$lwpId = $employeeleaveTypeModel->getEmployeeLeaveTypeByCode ( 'LWP' ) [0] ['leavetype_id'];
			$totalWeekends = $this->getWeekends ( $id, $lwpId, $wfhId, new DateTime ( $fromDate ), new DateTime ( $toDate ) );
			foreach ( $leaves as $leave ) {
				if ($leave ['leavetypeid'] == $wfhId) {
					$workFromHome += $leave ['leavecount'];
				} else if ($leave ['leavetypeid'] == $lwpId) {
				    // LWP - Do nothing
                } else{
					$leavecount += $leave ['leavecount'];
				}
			}
			
			$leavesholidays = $leavecount + $holidays;
			$workingDays = count ( $loggedDays );
			$faultyDays = array ();
			foreach ( $loggedDays as $key => $loggedday ) {
				$isWeekend = $this->getWeekends ( $id, $lwpId, $wfhId, new DateTime ( $loggedday ['punch_date'] ), new DateTime ( $loggedday ['punch_date'] ) );
				$leaveData = $employeeleaverequestModel->getSingleApprovedLeaveData ( $id, $loggedday ['punch_date'] );
				$isHoliday = $employeeholidaysModel->checkHoliday ( $loggedday ['punch_date'], $holidayGroup ['0'] ['holiday_group'] );
				$halfDayFlag = 0;
				$absentFlag = 0;
				if ($isHoliday || $isWeekend == 1) {
					// Holiday and Weekend calculations
					$workingDays --;
				} else if (count ( $leaveData ) != 0) {
					// Leave calculations
					if ($leaveData [0] ['leavetypeid'] != $wfhId) {
						// Not work from home
						if ($leaveData [0] ['leavecount'] == 0.5) {
							// Half day leave calculations
							if ($loggedday ['check_out'] == null) {
								// Mispunch calculations
								$workingDays -= 0.5;
								$misspunch ++;
							} else {
                                // check daily and half day calculations
                                // Office Hours validation
                                if ($dailyruleOfficeHours != null) {
                                    $officehoursModel->validateOfficeHours ( $dailyruleOfficeHours, $loggedDays, $key, $faultyDays, $absentFlag, $halfDayFlag, $loggedDayDetails );
                                }
                                if ($halfdayrule != null) {
                                    $mandatoryhoursRulesModel->halfDayRuleCalculation ( $halfdayrule, $loggedDays [$key] ['working_hours'], $faultyDays, $loggedday, $absentFlag, $halfDayFlag );
                                }
                                if ($dailyrule != null) {
                                    $mandatoryhoursRulesModel->dailyRuleCalculation ( $dailyrule, $loggedDays [$key] ['working_hours'], $faultyDays, $loggedday, $absentFlag, $halfDayFlag );
                                }
                                if ($absentFlag == 1) {
                                    $workingDays --;
                                } else if ($halfDayFlag == 1) {
                                    // Work hours complete
                                    $workingDays -= 0.5;
                                } else {
                                    // Work hours complete
                                    $workingDays -= 0.5;
                                }
							}
						} else {
							// full day leave calculations
							$workingDays --;
						}
					} else {
						// Work from home
						if ($loggedday ['check_out'] == null) {
							// Mispunch calculations
							$misspunch ++;
						} else {
							// Office Hours validation
							if ($dailyruleOfficeHours != null) {
								$officehoursModel->validateOfficeHours ( $dailyruleOfficeHours, $loggedDays, $key, $faultyDays, $absentFlag, $halfDayFlag, $loggedDayDetails );
							}
							if ($halfdayrule != null) {
								$mandatoryhoursRulesModel->halfDayRuleCalculation ( $halfdayrule, $loggedDays [$key] ['working_hours'], $faultyDays, $loggedday, $absentFlag, $halfDayFlag );
							}
							if ($dailyrule != null) {
								$mandatoryhoursRulesModel->dailyRuleCalculation ( $dailyrule, $loggedDays [$key] ['working_hours'], $faultyDays, $loggedday, $absentFlag, $halfDayFlag );
							}
                            if ($absentFlag == 1) {
                                $workingDays --;
                            } else if ($halfDayFlag == 1) {
                                $workingDays -= 0.5;
                            }
						}
					}
				} else if ($employeeExceptionModel->checkExceptionExist ( $id, $loggedday ['punch_date'], $dayCount )) {
					// Exception Calculations
					$workingDays --;
				} else {
					// Check Hours calculation
					if ($loggedday ['check_out'] == null) {
						// Mispunch calculations
						$misspunch ++;
					} else {
						// check daily and half day calculations
						// Office Hours validation
						if ($dailyruleOfficeHours != null) {
							$officehoursModel->validateOfficeHours ( $dailyruleOfficeHours, $loggedDays, $key, $faultyDays, $absentFlag, $halfDayFlag, $loggedDayDetails );
						}
						if ($halfdayrule != null) {
							$mandatoryhoursRulesModel->halfDayRuleCalculation ( $halfdayrule, $loggedDays [$key] ['working_hours'], $faultyDays, $loggedday, $absentFlag, $halfDayFlag );
						}
						if ($dailyrule != null) {
							$mandatoryhoursRulesModel->dailyRuleCalculation ( $dailyrule, $loggedDays [$key] ['working_hours'], $faultyDays, $loggedday, $absentFlag, $halfDayFlag );
						}
						if ($absentFlag == 1) {
							$workingDays --;
						} else if ($halfDayFlag == 1) {
							$workingDays -= 0.5;
						}
					}
				}
			}
			
			// verify week rules
			if ($weeklymandatoryhrsrule != null) {
				$weekViolationReduction = $this->VerifyWeekRules ( $id, $faultyDays, $loggedDays, $weeklymandatoryhrsrule );
				$workingDays += $weekViolationReduction;
			}
			
			$attendanceexception = $employeeExceptionModel->getUsersApprovedExceptionsBetween ( $id, $fromDate, $toDate );
			$absents = $totalDays - ($leavesholidays + $workingDays + $totalWeekends + $attendanceexception);
			if ($compensatoryoffrule != null)
				$compoff = $compoffrequestModel->getEncashmentsByOption ( $fromDate, $toDate, $userId, $compensatoryoffrule );
			else
				$compoff = 0;
			$totalworkingdays = $totalDays + $compoff - $absents - $employeeAttendanceModel->getMissPunchestodeduct ( $misspunch, $missedpunchesrule );
			
			$attendancereport [$id] = array (
					$name,
					$misspunch,
					$absents,
					$attendanceexception,
					$leavesholidays,
					$workFromHome,
					$compoff,
					$totalworkingdays,
					$totalWeekends 
			);
		}
		
		$_SESSION ['attendancereport'] = $attendancereport;
		$messages ['result'] = 'saved';
		$this->_helper->json ( $messages );
	}
	public function VerifyWeekRules($id, $faultyDays, $loggedDays, $weeklymandatoryhrsrule) {
		$weekViolationReduction = 0;
		$fromDate = $this->_request->getParams () ['fromdate'];
		$toDate = $this->_request->getParams () ['todate'];
		$mandatoryhoursRulesModel = new Default_Model_MandatoryHours ();
		$myAttendanceModel = new Default_Model_Myattendance ();
		if ($weeklymandatoryhrsrule != null) {
			$minhrs = $weeklymandatoryhrsrule [0] ['minimum_hours'];
			$minhrsaction = $weeklymandatoryhrsrule [0] ['minimum_hours_action'];
			$maxhrs = $weeklymandatoryhrsrule [0] ['maximum_hours'];
			$maxhrsaction = $weeklymandatoryhrsrule [0] ['maximum_hours_action'];
			
			$weeks = $myAttendanceModel->CalculateWeeks ( $fromDate, $toDate );
			foreach ( $weeks as $week ) {
				if (strtotime ( $week [1] ) < strtotime ( $toDate )) {
					$loggeddatainweek = $myAttendanceModel->getloggeddatainweek ( $id, $week, $faultyDays, $loggedDays );
					$requiredminhours = ($minhrs / 5) * ($loggeddatainweek [1]);
					$requiredmaxhours = ($maxhrs / 5) * ($loggeddatainweek [1]);
					if ($loggeddatainweek [0] < $requiredminhours) {
						if ($minhrsaction == 1)
							$weekViolationReduction --;
						else if ($minhrsaction == 2)
							$weekViolationReduction -= 0.5;
					}
					if ($loggeddatainweek [0] > $requiredmaxhours) {
						if ($maxhrsaction == 1)
							$weekViolationReduction --;
						else if ($maxhrsaction == 2)
							$weekViolationReduction -= 0.5;
					}
				}
			}
		}
		return $weekViolationReduction;
	}
}