<?php

class Default_EmployeerequestsController extends Zend_Controller_Action
{

    public function preDispatch()
    {
    	$ajaxContext = $this->_helper->getHelper('AjaxContext');
    	$ajaxContext->addActionContext('selectedexceptions','json')->initContext();
    	$ajaxContext->addActionContext('selectedcompoff','json')->initContext();
    }

    public function init()
    {
        $this->_options = $this->getInvokeArg('bootstrap')->getOptions();
    }

    public function indexAction()
    {
        $this->_redirect('employeerequests/leave');
    }

    public function leaveAction()
    {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $loginUserId = $auth->getStorage()->read()->id;
        }
        $leaverequestmodel = new Default_Model_Leaverequest();
        $employeeleavetypemodel = new Default_Model_Employeeleavetypes();
        $totalLeaveTypes = $employeeleavetypemodel->getactiveleavetype();

        $call = $this->_request->getParam('call');
        if ($call == 'ajaxcall')
            $this->_helper->layout->disableLayout();

        $view = Zend_Layout::getMvcInstance()->getView();
        $objname = $this->_getParam('objname');
        $refresh = $this->_getParam('refresh');
        $dashboardcall = $this->_getParam('dashboardcall');
        $flag = $this->_request->getParam('flag');
        if ($this->_request->getParams()['leavetypeid']) {
            $leavetypeid = $this->_request->getParams()['leavetypeid'];
        } else {
            $leavetypeid = $totalLeaveTypes[0]['id'];
        }

        $data = array();
        $searchQuery = '';
        $searchArray = array();
        $tablecontent = '';

        if ($refresh == 'refresh') {
            if ($dashboardcall == 'Yes')
                $perPage = DASHBOARD_PERPAGE;
            else
                $perPage = PERPAGE;
            $sort = 'ASC';
            $by = 'leavestatus';
            $pageNo = 1;
            $searchData = '';
        } else {
            $sort = ($this->_getParam('sort') != '') ? $this->_getParam('sort') : 'ASC';
            $by = ($this->_getParam('by') != '') ? $this->_getParam('by') : 'leavestatus';
            if ($dashboardcall == 'Yes')
                $perPage = $this->_getParam('per_page', DASHBOARD_PERPAGE);
            else
                $perPage = $this->_getParam('per_page', PERPAGE);
            $pageNo = $this->_getParam('page', 1);
            /**
             * search from grid - START *
             */
            $searchData = $this->_getParam('searchData');
            $searchData = rtrim($searchData, ',');
        /**
         * search from grid - END *
         */
        }

        $empleaveArray = array(
            'pendingleaves',
            'cancelledleaves',
            'approvedleaves',
            'rejectedleaves'
        );
        $objName = 'employeerequests';
        if (! empty($flag)) {
            if (in_array($flag, $empleaveArray)) {
                $objName = $flag;
                $queryflag = substr($flag, 0, - 6);
            }
        }

        $dataTmp = $leaverequestmodel->getGrid($sort, $by, $perPage, $pageNo, $searchData, $call, $dashboardcall, $objName, $queryflag);

        $leavesCountArray = sapp_Helper::getmanagerLeavesCountByCategory($loginUserId);
        $exceptionCountArray = sapp_Helper::getManagerExceptionsCountByCategory($loginUserId);
        $compoffCountArray = sapp_Helper::getcompoffCountByCategory($loginUserId);
        $leavesCountArray['action'] = "leave";

        array_push($data, $dataTmp);
        $this->view->dataArray = $data;
        $this->view->call = $call;
        $this->view->flag = $flag;
        $this->view->leavesCountArray = $leavesCountArray;
        $this->view->exceptionCountArray = $exceptionCountArray;
        $this->view->compoffCountArray = $compoffCountArray;
        $this->view->totalLeaveTypes = $totalLeaveTypes;
        $this->view->currentleavetypeid = $leavetypeid;
        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    public function exceptionAction()
    {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $loginUserId = $auth->getStorage()->read()->id;
        }

        $exceptionrequestmodel = new Default_Model_ExceptionRequest();
        $employeeleavetypemodel = new Default_Model_Employeeleavetypes();
        $totalLeaveTypes = $employeeleavetypemodel->getactiveleavetype();
        $call = $this->_getParam('call');
        if ($call == 'ajaxcall')
            $this->_helper->layout->disableLayout();

        $view = Zend_Layout::getMvcInstance()->getView();
        $objname = $this->_getParam('objname');
        $refresh = $this->_getParam('refresh');
        $dashboardcall = $this->_getParam('dashboardcall');
        $dashboardcall = $this->_getParam('dashboardcall');
        $flag = $this->_request->getParam('flag');
        if ($this->_request->getParams()['leavetypeid']) {
            $leavetypeid = $this->_request->getParams()['leavetypeid'];
        } else {
            $leavetypeid = $totalLeaveTypes[0]['id'];
        }

        $data = array();
        $searchQuery = '';
        $searchArray = array();
        $tablecontent = '';

        if ($refresh == 'refresh') {
            if ($dashboardcall == 'Yes')
                $perPage = DASHBOARD_PERPAGE;
            else
                $perPage = PERPAGE;
            $sort = 'ASC';
            $by = 'exceptionstatus';
            $pageNo = 1;
            $searchData = '';
        } else {
            $sort = ($this->_getParam('sort') != '') ? $this->_getParam('sort') : 'ASC';
            $by = ($this->_getParam('by') != '') ? $this->_getParam('by') : 'exceptionstatus';
            if ($dashboardcall == 'Yes')
                $perPage = $this->_getParam('per_page', DASHBOARD_PERPAGE);
            else
                $perPage = $this->_getParam('per_page', PERPAGE);
            $pageNo = $this->_getParam('page', 1);
            /**
             * search from grid - START *
             */
            $searchData = $this->_getParam('searchData');
            $searchData = rtrim($searchData, ',');
        /**
         * search from grid - END *
         */
        }

        $exceptionsArray = array(
            'pendingexceptions',
            'cancelledexceptions',
            'approvedexceptions',
            'rejectedexceptions',
            'expiredexceptions'
        );

        $objName = 'employeerequests';
        $queryflag = 'all';
        if (! empty($flag)) {
            if (in_array($flag, $exceptionsArray)) {
                $objName = $flag;
                $queryflag = substr($flag, 0, - 10);
            }
            if ($flag == 'total') {
                $queryflag = 'all';
            }
        }
        $dataTmp = $exceptionrequestmodel->getGrid($sort, $by, $perPage, $pageNo, $searchData, $call, $dashboardcall, $objName, $queryflag);

        $leavesCountArray = sapp_Helper::getManagerLeavesCountByCategory($loginUserId);
        $exceptionCountArray = sapp_Helper::getManagerExceptionsCountByCategory($loginUserId);
        $compoffCountArray = sapp_Helper::getcompoffCountByCategory($loginUserId);
        $exceptionCountArray['action'] = "exception";

        array_push($data, $dataTmp);
        $this->view->dataArray = $data;
        $this->view->call = $call;
        $this->view->flag = $flag;
        $this->view->leavesCountArray = $leavesCountArray;
        $this->view->exceptionCountArray = $exceptionCountArray;
        $this->view->compoffCountArray = $compoffCountArray;
        $this->view->currentleavetypeid = $leavetypeid;
        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    public function compoffAction()
    {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $loginUserId = $auth->getStorage()->read()->id;
        }

        $compoffrequestmodel = new Default_Model_Compoffrequest();
        $employeeleavetypemodel = new Default_Model_Employeeleavetypes();
        $totalLeaveTypes = $employeeleavetypemodel->getactiveleavetype();
        $call = $this->_getParam('call');
        if ($call == 'ajaxcall')
            $this->_helper->layout->disableLayout();

        $view = Zend_Layout::getMvcInstance()->getView();
        $objname = $this->_getParam('objname');
        $refresh = $this->_getParam('refresh');
        $dashboardcall = $this->_getParam('dashboardcall');
        $dashboardcall = $this->_getParam('dashboardcall');
        $flag = $this->_request->getParam('flag');
        if ($this->_request->getParams()['leavetypeid']) {
            $leavetypeid = $this->_request->getParams()['leavetypeid'];
        } else {
            $leavetypeid = $totalLeaveTypes[0]['id'];
        }

        $data = array();
        $searchQuery = '';
        $searchArray = array();
        $tablecontent = '';

        if ($refresh == 'refresh') {
            if ($dashboardcall == 'Yes')
                $perPage = DASHBOARD_PERPAGE;
            else
                $perPage = PERPAGE;
            $sort = 'ASC';
            $by = 'status';
            $pageNo = 1;
            $searchData = '';
        } else {
            $sort = ($this->_getParam('sort') != '') ? $this->_getParam('sort') : 'ASC';
            $by = ($this->_getParam('by') != '') ? $this->_getParam('by') : 'status';
            if ($dashboardcall == 'Yes')
                $perPage = $this->_getParam('per_page', DASHBOARD_PERPAGE);
            else
                $perPage = $this->_getParam('per_page', PERPAGE);
            $pageNo = $this->_getParam('page', 1);
            /**
             * search from grid - START *
             */
            $searchData = $this->_getParam('searchData');
            $searchData = rtrim($searchData, ',');
        /**
         * search from grid - END *
         */
        }

        $compoffArray = array(
            'pendingcompoff',
            'cancelledcompoff',
            'approvedcompoff',
            'rejectedcompoff',
            'expiredcompoff'
        );

        $objName = 'employeerequests';
        $queryflag = 'all';
        if (! empty($flag)) {
            if (in_array($flag, $compoffArray)) {
                $objName = $flag;
                $queryflag = substr($flag, 0, - 7);
            }
            if ($flag == 'total') {
                $queryflag = 'all';
            }
        }
        $dataTmp = $compoffrequestmodel->getGrid($sort, $by, $perPage, $pageNo, $searchData, $call, $dashboardcall, $objName, $queryflag);

        $leavesCountArray = sapp_Helper::getManagerLeavesCountByCategory($loginUserId);
        $exceptionCountArray = sapp_Helper::getManagerExceptionsCountByCategory($loginUserId);
        $compoffCountArray = sapp_Helper::getcompoffCountByCategory($loginUserId);

        $compoffCountArray['action'] = "compoff";

        array_push($data, $dataTmp);
        $this->view->dataArray = $data;
        $this->view->call = $call;
        $this->view->flag = $flag;
        $this->view->leavesCountArray = $leavesCountArray;
        $this->view->exceptionCountArray = $exceptionCountArray;
        $this->view->compoffCountArray = $compoffCountArray;
        $this->view->totalCompoff = $leavesCountArray['total'];
        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    public function viewAction()
    {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $loginUserId = $auth->getStorage()->read()->id;
        }
        $id = $this->getRequest()->getParam('id');
        $callval = $this->getRequest()->getParam('call');
        $type = $this->getRequest()->getParams()['type'];

        $flag = $this->_request->getParam('flag');
        if ($callval == 'ajaxcall')
            $this->_helper->layout->disableLayout();
        $objName = 'employeerequests';
        if ($type == 'leave') {
            $form = new Default_Form_managerleaverequest();

            try {
                if ($id && is_numeric($id) && $id > 0) {
                    $leaverequestmodel = new Default_Model_Leaverequest();
                    $usersmodel = new Default_Model_Users();
                    $flag = 'true';
                    $userid = $leaverequestmodel->getUserID($id);
                    $employeeuserid = $userid[0][user_id];
                    $getreportingManagerArr = $leaverequestmodel->getReportingManagerId($employeeuserid);
                    $reportingManager = $getreportingManagerArr[0]['repmanager'];
                    $loginUserRole = ($leaverequestmodel->getEmployeeRole($loginUserId));
                    if ($reportingManager != $loginUserId && $loginUserRole != '1')
                        $flag = 'false';
                    if (! empty($userid))
                        $isactiveuser = $usersmodel->getUserDetailsByID($userid[0]['user_id']);
                    else
                        $this->view->rowexist = "rows";

                    if (! empty($userid) && ! empty($isactiveuser)) {
                        $data = $leaverequestmodel->getLeaveRequestDetails($id);
                        if (! empty($data)) {
                            $data = $data[0];
                            $reason = $data['reason'];
                            $comments = $data['comments'];
                            $appliedleavescount = $data['appliedleavescount'];
                            $employeeid = $data['user_id'];
                            $leavetypeid = $data['leavetypeid'];
                            $employeeleavetypemodel = new Default_Model_Employeeleavetypes();
                            $usersmodel = new Default_Model_Users();
                            $employeesmodel = new Default_Model_Employees();
                            $businessunitid = '';
                            $loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails($employeeid);
                            if ($loggedInEmployeeDetails[0]['businessunit_id'] != '')
                                $businessunitid = $loggedInEmployeeDetails[0]['businessunit_id'];

                            $employeeleavetypeArr = $employeeleavetypemodel->getsingleEmployeeLeavetypeData($data['leavetypeid']);
                            if ($employeeleavetypeArr != 'norows') {
                                $form->leavetypeid->addMultiOption($employeeleavetypeArr[0]['id'], utf8_encode($employeeleavetypeArr[0]['leavetype']));
                                $data['leavetype'] = $employeeleavetypeArr[0]['leavetype'];
                            }
                            if ($data['leaveday'] == 1) {
                                $form->leaveday->addMultiOption($data['leaveday'], 'Full Day');
                                $data['leaveday'] = 'Full Day';
                            } else {
                                $form->leaveday->addMultiOption($data['leaveday'], 'Half Day');
                                $data['leaveday'] = 'Half Day';
                            }

                            $employeenameArr = $usersmodel->getUserDetailsByID($data['user_id']);
                            $employeeemail = $employeenameArr[0]['emailaddress'];
                            $employeename = $employeenameArr[0]['userfullname'];
                            $form->populate($data);

                            if ($data['leavestatus'] == 'Approved') {
                                $form->managerstatus->setLabel("Cancel");
                                $form->managerstatus->clearMultiOptions();
                                $form->managerstatus->addMultiOption(3, utf8_encode("Cancel"));
                            }
                            $from_date = sapp_Global::change_date($data['from_date'], 'view');
                            $to_date = sapp_Global::change_date($data['to_date'], 'view');
                            $appliedon = sapp_Global::change_date($data['createddate'], 'view');
                            // to show Leave Request history in view
                            $leaverequesthistory_model = new Default_Model_Leaverequesthistory();
                            $leave_history = $leaverequesthistory_model->getLeaveRequestHistory($id);
                            $this->view->leave_history = $leave_history;
                            // end

                            $form->from_date->setValue($from_date);
                            $form->to_date->setValue($to_date);
                            $form->createddate->setValue($appliedon);
                            $form->appliedleavesdaycount->setValue($data['appliedleavescount']);
                            $form->employeename->setValue($employeenameArr[0]['userfullname']);
                            $form->setDefault('leavetypeid', $data['leavetypeid']);
                            $form->setDefault('leaveday', $data['leaveday']);
                            $this->view->controllername = $objName;
                            $this->view->id = $id;
                            $this->view->form = $form;
                            $this->view->data = $data;
                            $this->view->type = $type;
                        } else {
                            $this->view->rowexist = "rows";
                        }
                    } else {
                        $this->view->rowexist = "rows";
                    }
                } else {
                    $this->view->rowexist = "rows";
                }
            } catch (Exception $e) {
                $this->view->rowexist = 'norows';
            }
        } else if ($type == 'exception') {
            $form = new Default_Form_managerexceptionrequest();
            try {
                if ($id && is_numeric($id) && $id > 0) {
                    $exceptionrequestmodel = new Default_Model_ExceptionRequest();
                    $usersmodel = new Default_Model_Users();
                    $flag = 'true';

                    $userid = $exceptionrequestmodel->getUserID($id);
                    $getreportingManagerArr = $exceptionrequestmodel->getReportingManagerId($id);
                    $reportingManager = $getreportingManagerArr[0]['repmanager'];
                    $hrmanager = $getreportingManagerArr[0]['hrmanager'];
                    if ($reportingManager != $loginUserId && $hrmanager != $loginUserId)
                        $flag = 'false';
                    if (! empty($userid))
                        $isactiveuser = $usersmodel->getUserDetailsByID($userid[0]['user_id']);
                    else
                        $this->view->rowexist = "rows";

                    if (! empty($userid) && ! empty($isactiveuser)) {
                        $data = $exceptionrequestmodel->getExceptionRequestDetails($id);
                        if (! empty($data)) {
                            $data = $data[0];
                            $reason = $data['reason'];
                            $appliedexceptioncount = $data['appliedexceptioncount'];
                            $exceptionstatus = $data['exceptionstatus'];
                            $employeeid = $data['user_id'];
                            $usersmodel = new Default_Model_Users();
                            $employeesmodel = new Default_Model_Employees();
                            $businessunitid = '';
                            $loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails($employeeid);
                            if ($loggedInEmployeeDetails[0]['businessunit_id'] != '')
                                $businessunitid = $loggedInEmployeeDetails[0]['businessunit_id'];
                            if ($data['exceptionday'] == 1) {
                                $form->exceptionday->addMultiOption($data['exceptionday'], 'Full Day');
                                $data['exceptionday'] = 'Full Day';
                            } else {
                                $form->exceptionday->addMultiOption($data['exceptionday'], 'Half Day');
                                $data['exceptionday'] = 'Half Day';
                            }

                            $employeenameArr = $usersmodel->getUserDetailsByID($data['user_id']);
                            $employeeemail = $employeenameArr[0]['emailaddress'];
                            $employeename = $employeenameArr[0]['userfullname'];
                            $form->populate($data);

                            if ($data['exceptionstatus'] == 'Approved') {
                                $form->managerstatus->setLabel("Cancel");
                                $form->managerstatus->clearMultiOptions();
                                $form->managerstatus->addMultiOption(3, utf8_encode("Cancel"));
                            }

                            $from_date = sapp_Global::change_date($data['from_date'], 'view');
                            $to_date = sapp_Global::change_date($data['to_date'], 'view');
                            $appliedon = sapp_Global::change_date($data['createddate'], 'view');

                            // to show Exception Request history in view
                            $exceptionrequesthistory_model = new Default_Model_Exceptionrequesthistory();
                            $exception_history = $exceptionrequesthistory_model->getExceptionRequestHistory($id);
                            $this->view->exception_history = $exception_history;
                            // end

                            $form->from_date->setValue($from_date);
                            $form->to_date->setValue($to_date);
                            $form->createddate->setValue($appliedon);
                            $form->appliedexceptioncount->setValue($data['appliedexceptioncount']);
                            $form->employeename->setValue($employeenameArr[0]['userfullname']);
                            $form->setDefault('exceptionday', $data['exceptionday']);
                            $this->view->controllername = $objName;
                            $this->view->id = $id;
                            $this->view->form = $form;
                            $this->view->data = $data;
                            $this->view->type = $type;
                        } else {
                            $this->view->rowexist = "rows";
                        }
                    } else {
                        $this->view->rowexist = "rows";
                    }
                } else {
                    $this->view->rowexist = "rows";
                }
            } catch (Exception $e) {
                $this->view->rowexist = 'norows';
            }
        } else if ($type == 'compoff') {

            $form = new Default_Form_managercompoffrequest();

            try {
                if ($id && is_numeric($id) && $id > 0) {
                    // $this->_redirect('employeeexceptions/edit/id/'.$id);
                    $compoffrequestmodel = new Default_Model_Compoffrequest();
                    $usersmodel = new Default_Model_Users();
                    $flag = 'true';

                    $userid = $compoffrequestmodel->getUserID($id);
                    $getreportingManagerArr = $compoffrequestmodel->getReportingManagerId($id);
                    $reportingManager = $getreportingManagerArr[0]['repmanager'];
                    $hrmanager = $getreportingManagerArr[0]['hrmanager'];
                    if ($reportingManager != $loginUserId && $hrmanager != $loginUserId)
                        $flag = 'false';
                    if (! empty($userid))
                        $isactiveuser = $usersmodel->getUserDetailsByID($userid[0]['user_id']);
                    else
                        $this->view->rowexist = "rows";

                    if (! empty($userid) && ! empty($isactiveuser)) {
                        $data = $compoffrequestmodel->getCompoffRequestDetails($id);
                        if (! empty($data)) {
                            $data = $data[0];
                            $compoffstatus = $data['status'];
                            $employeeid = $data['user_id'];
                            $CompoffType = $data['option'];
                            $usersmodel = new Default_Model_Users();
                            $employeesmodel = new Default_Model_Employees();
                            $businessunitid = '';
                            $loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails($employeeid);
                            if ($loggedInEmployeeDetails[0]['businessunit_id'] != '')
                                $businessunitid = $loggedInEmployeeDetails[0]['businessunit_id'];

                            $employeenameArr = $usersmodel->getUserDetailsByID($data['user_id']);
                            $employeeemail = $employeenameArr[0]['emailaddress'];
                            $employeename = $employeenameArr[0]['userfullname'];
                            $form->populate($data);
                            if ($data['status'] == 'Approved') {
                                $form->managerstatus->setLabel("Cancel");
                                $form->managerstatus->clearMultiOptions();
                                $form->managerstatus->addMultiOption(3, utf8_encode("Cancel"));
                            }
                            $avail_date = sapp_Global::change_date($data['avail_date'], 'view');
                            $appliedon = sapp_Global::change_date($data['createddate'], 'view');
                            $leavedate = sapp_Global::change_date($data['leave_date'], 'view');

                            $form->avail_date->setValue($avail_date);
                            $form->createddate->setValue($appliedon);
                            $form->leave_date->setValue($leavedate);
                            $form->employeename->setValue($employeenameArr[0]['userfullname']);
                            $form->setDefault('availday', $data['availday']);
                            $this->view->controllername = $objName;
                            $this->view->id = $id;
                            $this->view->form = $form;
                            $this->view->data = $data;
                            $this->view->type = $type;
                        } else {
                            $this->view->rowexist = "rows";
                        }
                    } else {
                        $this->view->rowexist = "rows";
                    }
                } else {
                    $this->view->rowexist = "rows";
                }
            } catch (Exception $e) {
                $this->view->rowexist = 'norows';
            }
        }
        if ($this->getRequest()->getPost()) {
            $this->_helper->layout->disableLayout();
            $result = $this->save($form, $employeeid, $data);
        }
    }

    public function save($managerrequestform, $employeeid, $requestdata)
    {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $loginUserId = $auth->getStorage()->read()->id;
        }
        $type = $this->getRequest()->getParams()['type'];
        if ($type == 'exception') {
            if ($managerrequestform->isValid($this->_request->getPost())) {
                $id = $this->_request->getParam('id');
                $managerstatus = $this->_request->getParam('managerstatus');
                $comments = $this->_request->getParam('comments');
                $_SESSION['managercomments'] = $comments;
                $date = new Zend_Date();

                $exceptionrequestmodel = new Default_Model_Exceptionrequest();
                $usersmodel = new Default_Model_Users();
                $actionflag = '';

                $tableid = '';
                $status = '';
                $messagestr = '';
                $successmessagestr = '';

                if ($managerstatus == 1) {
                    $exceptionstatus = "Approved";
                    $updateemployeeexception = $exceptionrequestmodel->updateemployeeexceptions($exceptionstatus, $employeeid, $comments, $id);
                    $status = 2;
                    $messagestr = "Exception request approved";
                    $successmessagestr = "Exception request approved successfully.";
                } else if ($managerstatus == 2) {
                    $exceptionstatus = "Rejected";
                    $updateemployeeexception = $exceptionrequestmodel->updateemployeeexceptions($exceptionstatus, $employeeid, $comments, $id);
                    $status = 3;
                    $messagestr = "Exception request rejected";
                    $successmessagestr = "Exception request rejected successfully.";
                } else if ($managerstatus == 3) {
                    $exceptionstatus = "Cancelled";
                    if ($requestdata['exceptionstatus'] == 'Approved') {

                        $updateemployeeexception = $exceptionrequestmodel->updatecancelledemployeeexceptions($exceptionstatus, $employeeid, $comments, $id);
                    }
                    $status = 4;
                    $messagestr = "Exception request cancelled";
                    $successmessagestr = "Exception request cancelled successfully.";
                }

                if ($managerstatus == 1 || $managerstatus == 2 || $managerstatus == 3) {
                    $data = array(
                        'exceptionstatus' => $status,
                        'approver_comments' => $comments,
                        'modifiedby' => $loginUserId,
                        'modifieddate' => gmdate("Y-m-d H:i:s")
                    );
                    if ($id != '') {
                        $where = array(
                            'id=?' => $id
                        );
                        $actionflag = 2;
                    } else {
                        $data['createdby'] = $loginUserId;
                        $data['createddate'] = gmdate("Y-m-d H:i:s");
                        $data['isactive'] = 1;
                        $where = '';
                        $actionflag = 1;
                    }
                    $Id = $exceptionrequestmodel->SaveorUpdateExceptionRequest($data, $where);
                    if ($Id == 'update') {
                        $tableid = $id;
                        $this->_helper->getHelper("FlashMessenger")->addMessage($successmessagestr);
                    } else {
                        $tableid = $Id;
                        $this->_helper->getHelper("FlashMessenger")->addMessage($successmessagestr);
                    }

                    /**
                     * exception request history
                     */
                    if ($Id == 'update') {

                        if ($managerstatus == 1) {
                            $exceptionstatus = 'Approved';
                        } else if ($managerstatus == 2) {
                            $exceptionstatus = 'Rejected';
                        } else {
                            $exceptionstatus = 'Cancelled';
                        }
                        $history = 'Exception Request has been ' . $exceptionstatus . ' by ';
                        $exceptionrequesthistory_model = new Default_Model_Exceptionrequesthistory();
                        $exception_history = array(
                            'exceptionrequest_id' => $id,
                            'description' => $history,
                            'createdby' => $loginUserId,
                            'modifiedby' => $loginUserId,
                            'isactive' => 1,
                            'createddate' => gmdate("Y-m-d H:i:s"),
                            'modifieddate' => gmdate("Y-m-d H:i:s")
                        );
                        $where = '';
                        $exceptionhistory = $exceptionrequesthistory_model->saveOrUpdateExceptionRequestHistory($exception_history, $where);
                    }

                    $requestdata['type'] = 'Exception';
                    $requestdata['other_type'] = $type;
                    $requestdata['id'] = $id;
                    $requestdata['status'] = $exceptionstatus;
                    $requestdata['daycount'] = $requestdata['appliedexceptioncount'];

                    if ($requestdata['to_date'] == '' || $requestdata['to_date'] == NULL)
                        $requestdata['to_date'] = $requestdata['from_date'];

                    $menuID = MANAGEREMPLOYEEVACATIONS;
                    $result = sapp_Global::logManager($menuID, $actionflag, $loginUserId, $tableid);

                    $this->_helper->json(array(
                        'result' => 'saved',
                        'message' => $messagestr,
                        'request' => $requestdata,
                        'controller' => 'employeerequests'
                    ));
                }
            } else {
                $messages = $managerrequestform->getMessages();
                $messages['result'] = 'error';
                $this->_helper->json($messages);
            }
        } else if ($type == 'leave') {
            if ($managerrequestform->isValid($this->_request->getPost())) {
            	$employeeleavetypemodel = new Default_Model_Employeeleavetypes ();
            	$compOffLeaveTypeId = $employeeleavetypemodel->getEmployeeLeaveIdByLeaveType("Compensatory off");
                $id = $this->_request->getParam('id');
                $managerstatus = $this->_request->getParam('managerstatus');
                $comments = $this->_request->getParam('comments');
                $_SESSION['managercomments'] = $comments;
                $notify_me_on = $this->_request->getParam('notify_me_on');
                $date = new Zend_Date();
                $leaverequestmodel = new Default_Model_Leaverequest();
                $employeeleavetypesmodel = new Default_Model_Employeeleavetypes();
                $employeeleavemodel = new Default_Model_Employeeleaves();
                $usersmodel = new Default_Model_Users();
                $empleavesummarymodel = new Default_Model_EmpLeaveSummary();
                $actionflag = '';
                $tableid = '';
                $status = '';
                $messagestr = '';
                $successmessagestr = '';
                $leavetypeid = $requestdata['leavetypeid'];
                $notificationtypeid = $leaverequestmodel->getNotificationTypeId("Approved Leaves Reminder");
                $checkLeavestatus = $leaverequestmodel->getLeaveStatus($id);
                $leavetypeArr = $employeeleavetypesmodel->getLeavetypeDataByID($leavetypeid);
                $repManagerDetails = $usersmodel->getUserDetailsByID($requestdata['rep_mang_id']);
                $loginUserName = ($leaverequestmodel->getUserName($loginUserId));
                $appliedleavescount = $requestdata['appliedleavescount'];
                if ($requestdata['parent'] != null) {
                    $parentLeaveReqData = $leaverequestmodel->getParentLeaveRequestDetails($requestdata['parent']);
                    $parentLeaveData = $employeeleavetypesmodel->getLeavetypeDataByID($parentLeaveReqData[0]['leavetypeid']);
                }
                if ($managerstatus == 1 && ! empty($leavetypeArr)) {
                    $status = 2;
                    $messagestr = "Leave request approved";
                    $successmessagestr = "Leave request approved successfully.";
                } else if ($managerstatus == 2) {
                    $status = 3;

                    if ($requestdata['parent'] != null) {
                        $pData = array(
                            'isactive' => 1
                        );
                        $pWhere = array(
                            'id=?' => $parentLeaveReqData[0]['id']
                        );

                        $leaverequestmodel->SaveorUpdateLeaveRequest($pData, $pWhere);

                        if ($parentLeaveData[0]['leavepredeductable'] == 1) {
                            $updateemployeeleave = $leaverequestmodel->updateemployeeleaves($parentLeaveReqData[0]['appliedleavescount'], $parentLeaveReqData[0]['user_id'], $parentLeaveReqData[0]['leavetypeid']);
                        }
                    }

                    if ($leavetypeArr[0]['leavepredeductable'] == 1) {
                        $updateemployeeleave = $leaverequestmodel->updatecancelledemployeeleaves($appliedleavescount, $employeeid, $leavetypeid);
                    }
                    $messagestr = "Leave request rejected";
                    $successmessagestr = "Leave request rejected successfully.";
                } else if ($managerstatus == 3 && ! empty($leavetypeArr) && ($leavetypeid != $compOffLeaveTypeId)) {
                    if ($requestdata['parent'] != null) {
                        $pData = array(
                            'isactive' => 1
                        );
                        $pWhere = array(
                            'id=?' => $parentLeaveReqData[0]['id']
                        );

                        $leaverequestmodel->SaveorUpdateLeaveRequest($pData, $pWhere);

                        if ($parentLeaveData[0]['leavepredeductable'] == 1) {
                            $updateemployeeleave = $leaverequestmodel->updateemployeeleaves($parentLeaveReqData[0]['appliedleavescount'], $parentLeaveReqData[0]['user_id'], $parentLeaveReqData[0]['leavetypeid']);
                        }
                    }

                    if ($leavetypeArr[0]['leavepredeductable'] == 1) {
                        $updateemployeeleave = $leaverequestmodel->updatecancelledemployeeleaves($appliedleavescount, $employeeid, $leavetypeid);
                    }
                    $status = 4;
                    $messagestr = "Leave request cancelled";
                    $successmessagestr = "Leave request cancelled successfully.";
                }else if ($managerstatus == 3 && ! empty($leavetypeArr) && ($leavetypeid == $compOffLeaveTypeId))
                {
                	$messagestr = "Compensatory Off request cannot be cancelled.";
                	$successmessagestr = "Compensatory Off request cannot be cancelled.";
                	$this->_helper->json(array(
                			'result' => 'saved',
                			'message' => $messagestr,
                			'request' => $requestdata,
                			'controller' => 'employeerequests'
                	));
                }

                if ($managerstatus == 1 || $managerstatus == 2 || ($managerstatus == 3 && $leavetypeid != $compOffLeaveTypeId)) {
                    $data = array(
                        'leavestatus' => $status,
                        'approver_comments' => $comments,
                        'modifiedby' => $loginUserId,
                        'modifieddate' => gmdate("Y-m-d H:i:s")
                    );

                    if ($checkLeavestatus[0]['leavestatus'] == "Pending") {
                        $notificationdata = array(
                            'notification_type_id' => $notificationtypeid[0]['id'],
                            'notification_date' => $notify_me_on,
                            'notification_request_id' => $id,
                            'created_by' => $loginUserId,
                            'created_date' => gmdate("Y-m-d H:i:s"),
                            'modified_by' => $loginUserId,
                            'modified_date' => gmdate("Y-m-d H:i:s"),
                            'is_active' => 1
                        );

                        if ($notificationdata[notification_date] != '')
                        {
                        	$NotificationId = $leaverequestmodel->SaveorUpdateNotification($notificationdata, '');
                        }

                    }

                    if ($id != '') {
                        $where = array(
                            'id=?' => $id
                        );
                        $actionflag = 2;
                    } else {
                        $data['createdby'] = $loginUserId;
                        $data['createddate'] = gmdate("Y-m-d H:i:s");
                        $data['isactive'] = 1;
                        $where = '';
                        $actionflag = 1;
                    }

                    if ($requestdata['parent'] != null && $managerstatus != 1) {
                        $data['isactive'] = 0;
                    }

                    $Id = $leaverequestmodel->SaveorUpdateLeaveRequest($data, $where);
                    if ($notificationdata[notification_date] != '')
                    {
                    	$NotificationId = $leaverequestmodel->SaveorUpdateNotification($notificationdata, '');
                    }
                    if ($Id == 'update') {
                        $tableid = $id;
                        $this->_helper->getHelper("FlashMessenger")->addMessage($successmessagestr);
                    } else {
                        $tableid = $Id;
                        $this->_helper->getHelper("FlashMessenger")->addMessage($successmessagestr);
                    }

                    if ($Id == 'update') {

                        if ($managerstatus == 1) {
                            $leavestatus = 'Approved';
                        } else if ($managerstatus == 2) {
                            $leavestatus = 'Rejected';
                        } else {
                            $leavestatus = 'Cancelled';
                        }
                        $history = 'Leave Request has been ' . $leavestatus . ' by ';
                        $leaverequesthistory_model = new Default_Model_Leaverequesthistory();
                        $leave_history = array(
                            'leaverequest_id' => $id,
                            'description' => $history,
                            'createdby' => $loginUserId,
                            'modifiedby' => $loginUserId,
                            'isactive' => 1,
                            'createddate' => gmdate("Y-m-d H:i:s"),
                            'modifieddate' => gmdate("Y-m-d H:i:s")
                        );
                        $where = '';
                        $leavehistory = $leaverequesthistory_model->saveOrUpdateLeaveRequestHistory($leave_history, $where);
                    }

                    $requestdata['type'] = 'Leave';
                    $requestdata['other_type'] = $type;
                    $requestdata['id'] = $id;
                    $requestdata['status'] = $leavestatus;
                    $requestdata['daycount'] = $appliedleavescount;

                    if ($requestdata['to_date'] == '' || $requestdata['to_date'] == NULL)
                        $requestdata['to_date'] = $requestdata['from_date'];

                    $menuID = MANAGEREMPLOYEEVACATIONS;
                    $result = sapp_Global::logManager($menuID, $actionflag, $loginUserId, $tableid);

                    $this->_helper->json(array(
                        'result' => 'saved',
                        'message' => $messagestr,
                        'request' => $requestdata,
                        'controller' => 'employeerequests'
                    ));
                }
            } else {
                $messages = $managerrequestform->getMessages();
                $messages['result'] = 'error';
                $this->_helper->json($messages);
            }
        } else if ($type == 'compoff') {

            if ($managerrequestform->isValid($this->_request->getPost())) {
                $id = $this->_request->getParam('id');
                $managerstatus = $this->_request->getParam('managerstatus');
                $comments = $this->_request->getParam('comments');
                $_SESSION['managercomments'] = $comments;
                $date = new Zend_Date();

                $usersmodel = new Default_Model_Users();
                $actionflag = '';

                $tableid = '';
                $status = '';
                $messagestr = '';
                $successmessagestr = '';

                $employeeleavetypesmodel = new Default_Model_Employeeleavetypes();
                $leaverequestmodel = new Default_Model_Leaverequest();
                $employeeleavesModel = new Default_Model_Employeeleaves();
                $compoffleavetype = $employeeleavetypesmodel->getEmployeeLeaveTypeByCode(COMPOFF, '');

                if ($managerstatus == 1) {
                    $status = "Approved";
                    $messagestr = "Compensatory Off request approved";
                    $successmessagestr = "Compensatory Off request approved successfully.";

                    if ($requestdata['option'] == 'Leave') {
                        if (! $compoffleavetype) {
                            $compoffleavetype = array(
                                "leavetype" => "Compensatory off",
                                'createddate' => gmdate("Y-m-d H:i:s"),
                                'modifieddate' => gmdate("Y-m-d H:i:s"),
                                'createdby' => $loginUserId,
                                'modifiedby' => $loginUserId,
                                'leavecode' => COMPOFF,
                                'max_limit' => '1',
                                "isactive" => '1'
                            );
                            $leavetypeid = $employeeleavetypesmodel->SaveorUpdateEmployeeLeaveTypeData($compoffleavetype, '');

                            $employeeleavesModel->SaveorUpdateEmployeeLeaveType($requestdata['user_id'], 1, 1, $loginUserId, $leavetypeid);
                        } else {
                            $leavetypeid = $compoffleavetype[0]['leavetype_id'];
                            $empCompoffLeaves = $employeeleavesModel->getsingleEmployeeleaveDataByType($requestdata['user_id'], $leavetypeid);
                            $employeeleavesModel->SaveorUpdateEmployeeLeaveType($requestdata['user_id'], 1, $empCompoffLeaves[0]['used_leaves'] + 1, $loginUserId, $leavetypeid);
                        }

                        $compoffleaverequest = array(
                            "user_id" => $requestdata['user_id'],
                            "reason" => $requestdata['user_comments'],
                            "from_date" => $requestdata['leave_date'],
                            "to_date" => $requestdata['leave_date'],
                            "leaveday" => "1",
                            'leavetypeid' => $leavetypeid,
                            "appliedleavescount" => "1",
                            "approver_comments" => $comments,
                            'leavestatus' => 2,
                            'no_of_days' => 1,
                            'createddate' => gmdate("Y-m-d H:i:s"),
                            'modifieddate' => gmdate("Y-m-d H:i:s"),
                            'createdby' => $loginUserId,
                            'modifiedby' => $loginUserId,
                            "isactive" => '1'
                        );

                        $Id = $leaverequestmodel->SaveorUpdateLeaveRequest($compoffleaverequest, '');
                    }
                } else if ($managerstatus == 2) {
                    $status = "Rejected";
                    $messagestr = "Compensatory Off request rejected";
                    $successmessagestr = "Compensatory Off request rejected successfully.";
                } else if ($managerstatus == 3) {
                    $status = "Cancelled";
                    if ($requestdata['status'] == 'Approved' && $requestdata['option'] == 'Leave') {
                        $leavetypeid = $compoffleavetype[0]['leavetype_id'];
                        $empCompoffLeaves = $employeeleavesModel->getsingleEmployeeleaveDataByType($requestdata['user_id'], $leavetypeid);
                        $employeeleavesModel->SaveorUpdateEmployeeLeaveType($requestdata['user_id'], 1, $empCompoffLeaves[0]['used_leaves'] - 1, $loginUserId, $leavetypeid);

                        $leavedetails = $leaverequestmodel->getSingleApprovedLeaveDataByType($requestdata['user_id'], $requestdata['leave_date'], $leavetypeid);
                        if ($leavedetails) {
                            $leavedetails[0]['leavestatus'] = 4;
                            $pWhere = array(
                                'id=?' => $leavedetails[0]['id']
                            );
                            $Id = $leaverequestmodel->SaveorUpdateLeaveRequest($leavedetails[0], $pWhere);
                        }
                    }
                    $messagestr = "Compensatory Off request cancelled";
                    $successmessagestr = "Compensatory Off request cancelled successfully.";
                }

                if ($managerstatus == 1 || $managerstatus == 2 || $managerstatus == 3) {
                    $data = array(
                        'status' => $status,
                        'approver_comments' => $comments,
                        'modifiedby' => $loginUserId,
                        'modifieddate' => gmdate("Y-m-d H:i:s")
                    );
                    if ($id != '') {
                        $where = array(
                            'id=?' => $id
                        );
                        $actionflag = 2;
                    } else {
                        $data['createdby'] = $loginUserId;
                        $data['createddate'] = gmdate("Y-m-d H:i:s");
                        $data['isactive'] = 1;
                        $where = '';
                        $actionflag = 1;
                    }
                }

                $compoffrequestmodel = new Default_Model_Compoffrequest();
                $Td = $compoffrequestmodel->SaveorUpdateCompoffRequest($data, $where);
                $requestdata['type'] = 'Compensatory Off';
                $requestdata['other_type'] = $type;
                $requestdata['id'] = $id;
                $requestdata['status'] = $status;
                $requestdata['approver_comments'] = $comments;

                if ($requestdata['option'] == 'Leave') {
                    $requestdata['optiontext'] = "Leave on " . $requestdata['leave_date'];
                } else {
                    $requestdata['optiontext'] = "Encashment";
                }

                $this->_helper->json(array(
                    'result' => 'saved',
                    'message' => $messagestr,
                    'request' => $requestdata,
                    'controller' => 'employeerequests'
                ));
            } else {
                $messages = $managerrequestform->getMessages();
                $messages['result'] = 'error';
                $this->_helper->json($messages);
            }
        }
    }

    public function selectedleavesAction() {
        $this->_helper->layout->disableLayout ();
        $result ['result'] = 'success';
        $result ['msg'] = '';
        $actionflag = '';
        $tableid = '';
        $leavestatus= '';
        $status = '';
        $subject = '';
        $message = '';
        $successmsg = '';
        $leave_request_details = '';

        $messagestr = '';
        $successmessagestr = '';

        $user_logged_in = 'true';
        $manager_logged_in = 'false';
        $auth = Zend_Auth::getInstance ();
        if ($auth->hasIdentity ()) {
            $loginUserId = $auth->getStorage ()->read ()->id;
        }
        $ids = $this->getRequest ()->getParam ( 'ids' );
        $comment = $this->getRequest ()->getParam ( 'comments' );
        $managerstatus = $this->getRequest ()->getParam ( 'status' );


        $leaverequestmodel = new Default_Model_Leaverequest ();
        $employeeleavetypesmodel = new Default_Model_Employeeleavetypes ();
        $usersmodel = new Default_Model_Users ();

         $loginUserRole = ($leaverequestmodel->getEmployeeRole ( $loginUserId ));
         $loginUserName = ($leaverequestmodel->getUserName ( $loginUserId ));

        foreach($ids as $id){
            if ($id && is_numeric ( $id ) && $id > 0) {
                $leave_details = $leaverequestmodel->getLeaveDetails ( $id );
                $leave_request_details = $leaverequestmodel->getLeaveRequestDetails($id);
                $leavetypeid = $leave_details[0]['leavetypeid'];
                $leavetypeArr = $employeeleavetypesmodel->getLeavetypeDataByID($leavetypeid);
                $appliedleavescount = $leave_details[0]['appliedleavescount'];
                if($leave_request_details[0]['parent'] != null){
                    $parentLeaveReqData = $leaverequestmodel->getParentLeaveRequestDetails($leave_request_details[0]['parent']);
                    $parentLeaveData = $employeeleavetypesmodel->getLeavetypeDataByID($parentLeaveReqData[0]['leavetypeid']);
                }
                if ($managerstatus == 'Approve' && ! empty($leavetypeArr)) {
                    $status = 2;
                    $messagestr = "Leave request approved";
                    $successmessagestr = "Leave request approved successfully.";
                }
                else if ($managerstatus == 'Reject') {
                    $status = 3;

                    if ($leave_request_details[0]['parent'] != null) {
                        $pData = array(
                            'isactive' => 1
                        );
                        $pWhere = array(
                            'id=?' => $parentLeaveReqData[0]['id']
                        );

                        $leaverequestmodel->SaveorUpdateLeaveRequest($pData, $pWhere);

                        if ($parentLeaveData[0]['leavepredeductable'] == 1) {
                            $updateemployeeleave = $leaverequestmodel->updateemployeeleaves($parentLeaveReqData[0]['appliedleavescount'], $parentLeaveReqData[0]['user_id'], $parentLeaveReqData[0]['leavetypeid']);
                        }
                    }

                    if ($leavetypeArr[0]['leavepredeductable'] == 1) {
                        $updateemployeeleave = $leaverequestmodel->updatecancelledemployeeleaves($appliedleavescount, $leave_details[0]['user_id'], $leavetypeid);
                    }
                    $messagestr = "Leave request rejected";
                    $successmessagestr = "Leave request rejected successfully.";
                }  else if ($managerstatus == 'Cancel Leave Request' && ! empty($leavetypeArr)) {
                    if ($leave_request_details[0]['parent'] != null) {
                        $pData = array(
                            'isactive' => 1
                        );
                        $pWhere = array(
                            'id=?' => $parentLeaveReqData[0]['id']
                        );

                        $leaverequestmodel->SaveorUpdateLeaveRequest($pData, $pWhere);

                        if ($parentLeaveData[0]['leavepredeductable'] == 1) {
                            $updateemployeeleave = $leaverequestmodel->updateemployeeleaves($parentLeaveReqData[0]['appliedleavescount'], $parentLeaveReqData[0]['user_id'], $parentLeaveReqData[0]['leavetypeid']);
                        }
                    }

                    if ($leavetypeArr[0]['leavepredeductable'] == 1) {
                        $updateemployeeleave = $leaverequestmodel->updatecancelledemployeeleaves($appliedleavescount, $leave_details[0]['user_id'], $leavetypeid);
                    }
                    $status = 4;
                    $messagestr = "Leave request cancelled";
                    $successmessagestr = "Leave request cancelled successfully.";
                }
                if ($managerstatus == 'Approve' || $managerstatus == 'Reject' || $managerstatus == 'Cancel Leave Request') {
                    $data = array(
                        'leavestatus' => $status,
                        'approver_comments' => $comment,
                        'modifiedby' => $loginUserId,
                        'modifieddate' => gmdate("Y-m-d H:i:s")
                    );

                    if ($id != '') {
                        $where = array(
                            'id=?' => $id
                        );
                        $actionflag = 2;
                    } else {
                        $data['createdby'] = $loginUserId;
                        $data['createddate'] = gmdate("Y-m-d H:i:s");
                        $data['isactive'] = 1;
                        $where = '';
                        $actionflag = 1;
                    }

                    if ($leave_request_details[0]['parent'] != null && $managerstatus != 'Approve') {
                        $data['isactive'] = 0;
                    }

                    $Id = $leaverequestmodel->SaveorUpdateLeaveRequest($data, $where);

                    if ($Id == 'update') {
                        $tableid = $id;
                        $this->_helper->getHelper("FlashMessenger")->addMessage($successmessagestr);
                    } else {
                        $tableid = $Id;
                        $this->_helper->getHelper("FlashMessenger")->addMessage($successmessagestr);
                    }

                    if ($Id == 'update') {

                        if ($managerstatus == 'Approve') {
                            $leavestatus = 'Approved';
                        } else if ($managerstatus == 'Reject') {
                            $leavestatus = 'Rejected';
                        } else {
                            $leavestatus = 'Cancelled';
                        }
                        $history = 'Leave Request has been ' . $leavestatus . ' by ';
                        $leaverequesthistory_model = new Default_Model_Leaverequesthistory();
                        $leave_history = array(
                            'leaverequest_id' => $id,
                            'description' => $history,
                            'createdby' => $loginUserId,
                            'modifiedby' => $loginUserId,
                            'isactive' => 1,
                            'createddate' => gmdate("Y-m-d H:i:s"),
                            'modifieddate' => gmdate("Y-m-d H:i:s")
                        );
                        $where = '';
                        $leavehistory = $leaverequesthistory_model->saveOrUpdateLeaveRequestHistory($leave_history, $where);
                    }

                    $leave_request_details[0]['type'] = 'Leave';
                    $leave_request_details[0]['other_type'] = 'leave';
                    $leave_request_details[0]['id'] = $id;
                    $leave_request_details[0]['status'] = $leavestatus;
                    $leave_request_details[0]['daycount'] = $appliedleavescount;

                    if ($leave_request_details[0]['to_date'] == '' || $leave_request_details[0]['to_date'] == NULL)
                        $leave_request_details[0]['to_date'] = $leave_request_details[0]['from_date'];
                }
                $this->sendemail('leave',$id, $leavestatus,$loginUserName);
            }
        }
            $this->_helper->json(array(
            'result' => 'saved',
            'message' => $messagestr,
            'controller' => 'employeerequests'
        ));

        $this->_helper->json ( $result );
    }

    public function selectedexceptionsAction() {
     	$this->_helper->layout->disableLayout ();
    	$result ['result'] = 'success';
    	$result ['msg'] = '';
    	$actionflag = '';
    	$tableid = '';
    	$exceptionstatus= '';
    	$status = '';
    	$subject = '';
    	$message = '';
    	$successmsg = '';

    	$messagestr = '';
    	$successmessagestr = '';

    	$user_logged_in = 'true';
    	$manager_logged_in = 'false';
    	$auth = Zend_Auth::getInstance ();
    	if ($auth->hasIdentity ()) {
    		$loginUserId = $auth->getStorage ()->read ()->id;
    	}
    	$ids = $this->getRequest ()->getParam ( 'ids' );
    	$comment = $this->getRequest ()->getParam ( 'comments' );
    	$managerstatus = $this->getRequest ()->getParam ( 'status' );
    	$leaverequestmodel = new Default_Model_Leaverequest ();
    	$exceptionquestmodel = new Default_Model_ExceptionRequest();
    	$usersmodel = new Default_Model_Users ();

    	$loginUserRole = ($leaverequestmodel->getEmployeeRole ( $loginUserId ));
    	$loginUserName = ($leaverequestmodel->getUserName ( $loginUserId ));

    	foreach($ids as $id){
    		if ($id && is_numeric ( $id ) && $id > 0) {
    			$exception_details = $exceptionquestmodel->getExceptionDetails( $id );
    			$exception_request_details = $exceptionquestmodel->getExceptionRequestDetails($id);
    			$appliedexceptionscount = $exception_details[0]['appliedexceptioncount'];
    			if ($managerstatus == 'Approve') {
    				$status = 2;
    				$messagestr = "Exception request approved";
    				$successmessagestr = "Exception request approved successfully.";
    			}
    			else if ($managerstatus == 'Reject') {
    				$status = 3;
    				$messagestr = "Exception request rejected";
    				$successmessagestr = "Exception request rejected successfully.";
    			}  else if ($managerstatus == 'Cancel Request') {
    				$status = 4;
    				$messagestr = "Exception request cancelled";
    				$successmessagestr = "Exception request cancelled successfully.";
    			}
    			if ($managerstatus == 'Approve' || $managerstatus == 'Reject' || $managerstatus == 'Cancel Request') {
    				$data = array(
    						'exceptionstatus' => $status,
    						'approver_comments' => $comment,
    						'modifiedby' => $loginUserId,
    						'modifieddate' => gmdate("Y-m-d H:i:s")
    				);

    				if ($id != '') {
    					$where = array(
    							'id=?' => $id
    					);
    					$actionflag = 2;
    				} else {
    					$data['createdby'] = $loginUserId;
    					$data['createddate'] = gmdate("Y-m-d H:i:s");
    					$data['isactive'] = 1;
    					$where = '';
    					$actionflag = 1;
    				}

    				$Id = $exceptionquestmodel->SaveorUpdateExceptionRequest($data, $where);

    				if ($Id == 'update') {
    					$tableid = $id;
    					$this->_helper->getHelper("FlashMessenger")->addMessage($successmessagestr);
    				} else {
    					$tableid = $Id;
    					$this->_helper->getHelper("FlashMessenger")->addMessage($successmessagestr);
    				}

    				if ($Id == 'update') {

    					if ($managerstatus == 'Approve') {
    						$exceptionstatus = 'Approved';
    					} else if ($managerstatus == 'Reject') {
    						$exceptionstatus = 'Rejected';
    					} else {
    						$exceptionstatus = 'Cancelled';
    					}
    					$history = 'Exception Request has been ' . $exceptionstatus . ' by ';
    					$exceptionrequesthistory_model = new Default_Model_Exceptionrequesthistory();
    					$exception_history = array(
    							'exceptionrequest_id' => $id,
    							'description' => $history,
    							'createdby' => $loginUserId,
    							'modifiedby' => $loginUserId,
    							'isactive' => 1,
    							'createddate' => gmdate("Y-m-d H:i:s"),
    							'modifieddate' => gmdate("Y-m-d H:i:s")
    					);
    					$where = '';
    					$exceptionhistory = $exceptionrequesthistory_model->saveOrUpdateExceptionRequestHistory($exception_history, $where);
    				}

    				$exception_request_details[0]['type'] = 'Leave';
    				$exception_request_details[0]['other_type'] = 'leave';
    				$exception_request_details[0]['id'] = $id;
    				$exception_request_details[0]['status'] = $exceptionstatus;
    				$exception_request_details[0]['daycount'] = $appliedexceptionscount;

    				if ($exception_request_details[0]['to_date'] == '' || $exception_request_details[0]['to_date'] == NULL)
    					$exception_request_details[0]['to_date'] = $exception_request_details[0]['from_date'];
    			}
    			$this->sendemail('exception',$id, $exceptionstatus,$loginUserName);
    		}
    	}
    	$this->_helper->json(array(
    			'result' => 'saved',
    			'message' => $messagestr,
    			'controller' => 'employeerequests'
    	));

    	$this->_helper->json ( $result );
    }

    public function sendemail($requestType, $id, $status, $loginUserName)
    {
    	$usersmodel = new Default_Model_Users();
    	if ($requestType == 'exception')
    	{
    		$exceptionrequestmodel = new Default_Model_ExceptionRequest();
    		$requestdetails = $exceptionrequestmodel->getExceptionRequestDetails($id);
    		$requesttype = 'Exception';
    		$employee = $requestdetails['0']['user_id'];
    		$reason = $requestdetails['0']['reason'];
    		$appliedexceptioncount = $requestdetails['0']['appliedexceptioncount'];
    		$from_date = $requestdetails['0']['from_date'];
    		$to_date = $requestdetails['0']['to_date'];
    	}
    	else if ($requestType == 'compoff')
    	{
    		$compoffrequestmodel = new Default_Model_Compoffrequest();
    		$requestdetails = $compoffrequestmodel->getCompoffRequestDetails($id);
    		$requesttype = 'Compensatory-Off';
    		$employee = $requestdetails['0']['user_id'];
    		$reason = $requestdetails['0']['user_comments'];
    		if ($requestdetails['0']['option'] == 'Encashment')
    		{
    			$appliedexceptioncount = 'Encashment';
    		}
    		else {
    			$appliedexceptioncount = 'Leave on '.$requestdetails['0']['leave_date'];
    		}
    		$from_date = $requestdetails['0']['leave_date'];
    		$to_date = $requestdetails['0']['leave_date'];
    	}
    	else
    	{
    		$leaverequestmodel = new Default_Model_Leaverequest();
    		$requestdetails = $leaverequestmodel->getLeaveRequestDetails($id);
    		$requesttype = 'Leave';
    		$employee = $requestdetails['0']['user_id'];
    		$reason = $requestdetails['0']['reason'];
    		$appliedexceptioncount = $requestdetails['0']['appliedleavescount'];
    		$from_date = $requestdetails['0']['from_date'];
    		$to_date = $requestdetails['0']['to_date'];
    	}

    	$reportingmanager = $requestdetails['0']['rep_mang_id'];
    	$hr = $requestdetails['0']['hr_id'];
    	$employeemail = $usersmodel->getUserEmailId($employee);
    	$reportingmanagermail = $usersmodel->getUserEmailId($reportingmanager);
    	$hrmail = $usersmodel->getUserEmailId($hr);
    	$userfullname = $usersmodel->getUserFullName($employee);
    	$reportingmanagerName = $usersmodel->getUserFullName($reportingmanager);

    	/**
    	 * MAILING CODE *
    	 */
    	// $hremail = explode(",",HREMAIL);
    	/* Mail to Reporting manager */
    	if ($requestType != 'compoff')
    	{
    	if ($to_date == '' || $to_date == NULL)
    		$to_date = $from_date;

    		$toemailArr = $reportingmanagermail; // $employeeemail
    		if (! empty ( $toemailArr )) {
    			$options ['subject'] = $requesttype.' request '.strtolower($status);
    			$options ['header'] = $requesttype.' Request';
    			$options ['toEmail'] = $toemailArr;
    			$options ['toName'] = $reportingmanagerName;
    			$options ['message'] = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below '.strtolower($requesttype).'(s) has been '.strtolower($status).' by '.$loginUserName.'.</div>
				</div>
												<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					  <tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $userfullname . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">' . $appliedexceptioncount . '</td>
                      </tr>';

    			if ($requesttype == 'Leave')
                {
                    $leavetype = $leaverequestmodel->getLeaveTypeByTypeId($requestdetails['0']['leavetypeid']);
                    $options ['message'] .= '
                        <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Leave Type</td>
                        <td style="color:#5b5b5b">' . $leavetype['0']['leavetype'] . '</td>
                       </tr>
                       <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
                  </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason</td>
                        <td style="color:#5b5b5b">' . $requestdetails['0']['approver_comments'] . '</td>
                  </tr>
                  <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
                        <td style="color:#5b5b5b">' . $reportingmanagerName . '</td>
                  </tr>
                    </tbody></table>';
                }
                else
                {
                    $options ['message'] .= '<tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
                  </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason</td>
                        <td style="color:#5b5b5b">' . $requestdetails['0']['approver_comments'] . '</td>
                  </tr>
                  <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
                        <td style="color:#5b5b5b">' . $reportingmanagerName . '</td>
                    </tr>
                    </tbody></table>';
                }


                $options ['message'] .= '</div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the '.$requestType.' details.</div>
            </div>
            		</div>';
    			$result = sapp_Global::_sendEmail ( $options );
    		}
    		/* END */
    		/* Mail to HR */
    		if (!empty($hrmail)) {
    			$options ['subject'] = $requesttype.' request '.strtolower($status);
    			$options ['header'] = $requesttype.' Request';
    			$options ['toEmail'] = $hrmail;
    			$options ['toName'] = 'Leave management';
    			$options ['message'] = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below '.strtolower($requesttype).'(s) has been '.strtolower($status).' by '.$loginUserName.'.</div>
				</div>
<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					<tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $userfullname . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">' . $appliedexceptioncount . '</td>
                      </tr>';
                if ($requesttype == 'Leave')
                {
                    $leavetype = $leaverequestmodel->getLeaveTypeByTypeId($requestdetails['0']['leavetypeid']);
                    $options ['message'] .= '
                    <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Leave Type</td>
                        <td style="color:#5b5b5b">' . $leavetype['0']['leavetype'] . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
                  </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason</td>
                        <td style="color:#5b5b5b">' . $requestdetails['0']['approver_comments'] . '</td>
                  </tr>
                  <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
                        <td style="color:#5b5b5b">' . $reportingmanagerName . '</td>
                  </tr>
                </tbody></table>';
                }
                else
                {
                    $options ['message'] .= '<tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
                  </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason</td>
                        <td style="color:#5b5b5b">' . $requestdetails['0']['approver_comments'] . '</td>
                  </tr>
                  <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
                        <td style="color:#5b5b5b">' . $reportingmanagerName . '</td>
                  </tr>
                </tbody></table>';
                }


                $options ['message'] .= '</div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the '.$requestType.' details.</div>
            </div>
            		</div>';
    			// $options['cron'] = 'yes';
    			$result = sapp_Global::_sendEmail ( $options );
    		}

    		/* END */
    		/* Mail to the applied employee */
    		$toemailArr = $employeeemail;
    		$options ['subject'] = $requesttype.' request '.strtolower($status);
    		$options ['header'] = $requesttype.' Request';
    		$options ['toEmail'] = $employeemail;
    		$options ['toName'] = $userfullname;
    		$options ['message'] = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below '.strtolower($requesttype).'(s) has been '.strtolower($status).' by '.$loginUserName.'.</div>
				</div>
				<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
				<tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $userfullname . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">' . $appliedexceptioncount . '</td>
                      </tr>';
                if ($requesttype == 'Leave')
                {
                    $leavetype = $leaverequestmodel->getLeaveTypeByTypeId($requestdetails['0']['leavetypeid']);
                    $options ['message'] .= '
                    <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Leave Type</td>
                        <td style="color:#5b5b5b">' . $leavetype['0']['leavetype'] . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
                  </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason</td>
                        <td style="color:#5b5b5b">' . $requestdetails['0']['approver_comments'] . '</td>
                  </tr>
                  <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
                        <td style="color:#5b5b5b">' . $reportingmanagerName . '</td>
                  </tr>
                </tbody></table>';
                }
                else
                {
                    $options ['message'] .= '<tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
            	     </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason</td>
                        <td style="color:#5b5b5b">' . $requestdetails['0']['approver_comments'] . '</td>
                  </tr>
                </tbody></table>';
                }


                $options ['message'] .= '</div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the '.$requestType.' details.</div>
            </div>
            		</div>';
    		$result = sapp_Global::_sendEmail ( $options );
    	}
    	else
    	{
    		if ($to_date == '' || $to_date == NULL)
    			$to_date = $from_date;
    			 
    			$toemailArr = $reportingmanagermail; // $employeeemail
    			if (! empty ( $toemailArr )) {
    				$options ['subject'] = $requesttype.' request '.strtolower($status);
    				$options ['header'] = $requesttype.' Request';
    				$options ['toEmail'] = $toemailArr;
    				$options ['toName'] = $reportingmanagerName;
    				$options ['message'] = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below '.strtolower($requesttype).'(s) has been '.strtolower($status).' by '.$loginUserName.'.</div>
				</div>
												<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					  <tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $userfullname . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reimbursement Option</td>
                        <td style="color:#5b5b5b">' . $appliedexceptioncount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Manager Comments</td>
                        <td style="color:#5b5b5b">' . $requestdetails['0']['approver_comments'] . '</td>
                  </tr>
                </tbody></table>
   
            </div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the '.$requestType.' details.</div>
            </div>
            		</div>';
    				$result = sapp_Global::_sendEmail ( $options );
    			}
    			/* END */
    			/* Mail to HR */
    			if (!empty($hrmail)) {
    				$options ['subject'] = $requesttype.' request '.strtolower($status);
    				$options ['header'] = $requesttype.' Request';
    				$options ['toEmail'] = $hrmail;
    				$options ['toName'] = 'Leave management';
    				$options ['message'] = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below '.strtolower($requesttype).'(s) has been '.strtolower($status).' by '.$loginUserName.'.</div>
				</div>
<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					  <tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $userfullname . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reimbursement Option</td>
                        <td style="color:#5b5b5b">' . $appliedexceptioncount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Manager Comments</td>
                        <td style="color:#5b5b5b">' . $requestdetails['0']['approver_comments'] . '</td>
                  </tr>
                </tbody></table>
   
            </div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the '.$requestType.' details.</div>
            </div>
            		</div>';
    				// $options['cron'] = 'yes';
    				$result = sapp_Global::_sendEmail ( $options );
    			}
    			 
    			/* END */
    			/* Mail to the applied employee */
    			$toemailArr = $employeeemail;
    			$options ['subject'] = $requesttype.' request '.strtolower($status);
    			$options ['header'] = $requesttype.' Request';
    			$options ['toEmail'] = $employeemail;
    			$options ['toName'] = $userfullname;
    			$options ['message'] = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below '.strtolower($requesttype).'(s) has been '.strtolower($status).' by '.$loginUserName.'.</div>
				</div>
				<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					  <tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $userfullname . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reimbursement Option</td>
                        <td style="color:#5b5b5b">' . $appliedexceptioncount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Manager Comments</td>
                        <td style="color:#5b5b5b">' . $requestdetails['0']['approver_comments'] . '</td>
                  </tr>
                </tbody></table>
   
            </div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the '.$requestType.' details.</div>
            </div>
            </div>';
    			$result = sapp_Global::_sendEmail ( $options );
    	}
    }
    
    public function selectedcompoffAction() {
	$this->_helper->layout->disableLayout ();
    	$result ['result'] = 'success';
    	$result ['msg'] = '';
    	$actionflag = '';
    	$tableid = '';
    	$compoffstatus= '';
    	$status = '';
    	$subject = '';
    	$message = '';
    	$successmsg = '';
    	$compoff_request_details = '';
    
    	$messagestr = '';
    	$successmessagestr = '';
    
    	$user_logged_in = 'true';
    	$manager_logged_in = 'false';
    	$auth = Zend_Auth::getInstance ();
    	if ($auth->hasIdentity ()) {
    		$loginUserId = $auth->getStorage ()->read ()->id;
    	}
    	$ids = $this->getRequest ()->getParam ( 'ids' );
    	$comment = $this->getRequest ()->getParam ( 'comments' );
    	$managerstatus = $this->getRequest ()->getParam ( 'status' );
    
    	$leaverequestmodel = new Default_Model_Leaverequest ();
    	$compoffrequestmodel = new Default_Model_Compoffrequest();
    	$usersmodel = new Default_Model_Users ();
    	$employeeleavetypemodel = new Default_Model_Employeeleavetypes ();
    	$compOffLeaveTypeId = $employeeleavetypemodel->getEmployeeLeaveIdByLeaveType("Compensatory off");
    	
    	$loginUserRole = ($compoffrequestmodel->getEmployeeRole ( $loginUserId ));
    	$loginUserName = ($leaverequestmodel->getUserName ( $loginUserId ));
    
    	foreach($ids as $id){
    		if ($id && is_numeric ( $id ) && $id > 0) {
    			$compoff_details = $compoffrequestmodel->getCompoffRequestDetails( $id );
    			$compoff_request_details = $compoffrequestmodel->getCompoffRequestDetails($id);
    			$appliedcompoffcount = $compoff_details[0]['appliedleavescount'];
    			if ($managerstatus == 'Approve') {
    				$status = 2;
    				$messagestr = "Compensatory-Off request approved";
    				$successmessagestr = "Compensatory-Off request approved successfully.";
    			}
    			else if ($managerstatus == 'Reject') {
    				$status = 3;
    				
    				if ($leavetypeArr[0]['leavepredeductable'] == 1) {
    					$updateemployeeleave = $leaverequestmodel->updatecancelledemployeeleaves($appliedcompoffcount, $leave_details[0]['user_id'], $leavetypeid);
    				}
    				$messagestr = "Compensatory-Off request rejected";
    				$successmessagestr = "Compensatory-Off request rejected successfully.";
    			}  else if ($managerstatus == 'Cancel Request') {
    				$status = 4;
    				$messagestr = "Compensatory-Off request cancelled";
    				$successmessagestr = "Compensatory-Off request cancelled successfully.";
    			}
    			if ($managerstatus == 'Approve' || $managerstatus == 'Reject' || $managerstatus == 'Cancel Request') {
    				$data = array(
    						'status' => $status,
    						'approver_comments' => $comment,
    						'modifiedby' => $loginUserId,
    						'modifieddate' => gmdate("Y-m-d H:i:s")
    				);
    
    				if ($id != '') {
    					$where = array(
    							'id=?' => $id
    					);
    					$actionflag = 2;
    				} else {
    					$data['createdby'] = $loginUserId;
    					$data['createddate'] = gmdate("Y-m-d H:i:s");
    					$data['isactive'] = 1;
    					$where = '';
    					$actionflag = 1;
    				}
    
    				$Id = $compoffrequestmodel->SaveorUpdateCompoffRequest($data, $where);
    
    				if ($Id == 'update') {
    					if ($managerstatus == 'Approve') {
    						$compoffstatus = 'Approved';
    						
    						$compoffleaverequest = array(
    								"user_id" => $compoff_details['0']['user_id'],
    								"reason" => $compoff_details['0']['user_comments'],
    								"from_date" => $compoff_details['0']['leave_date'],
    								"to_date" => $compoff_details['0']['leave_date'],
    								"leaveday" => "1",
    								'leavetypeid' => $compOffLeaveTypeId,
    								"appliedleavescount" => "1",
    								"approver_comments" => $compoff_details['0']['approver_comments'],
    								'leavestatus' => 2,
    								'no_of_days' => 1,
    								'createddate' => gmdate("Y-m-d H:i:s"),
    								'modifieddate' => gmdate("Y-m-d H:i:s"),
    								'createdby' => $loginUserId,
    								'modifiedby' => $loginUserId,
    								"isactive" => '1'
    						);
    						
    						$Id = $leaverequestmodel->SaveorUpdateLeaveRequest($compoffleaverequest, '');
    					} else if ($managerstatus == 'Reject') {
    						$compoffstatus = 'Rejected';
    					} else {
    						$compoffstatus = 'Cancelled';
    						
    						$getleaveid = $leaverequestmodel->getLeaveID($compoff_details['0']['user_id'],$compOffLeaveTypeId,$compoff_details['0']['leave_date']);
    						if ($getleaveid != null)
    						{
    							$data = array (
    									'leavestatus' => 4,
    									'modifieddate' => gmdate ( "Y-m-d H:i:s" ),
    									'modifiedby' => $loginUserId
    							);
    							$where = array (
    									'id=?' => $getleaveid['0']['id']
    							);
    							$leaveid = $leaverequestmodel->update($data, $where);
    						}
    					}
    					$tableid = $id;
    					$this->_helper->getHelper("FlashMessenger")->addMessage($successmessagestr);
    				} else {
    					$tableid = $Id;
    					$this->_helper->getHelper("FlashMessenger")->addMessage($successmessagestr);
    				}
    				
    				$compoff_request_details[0]['type'] = 'Compensatory-Off';
    				$compoff_request_details[0]['other_type'] = 'compoff';
    				$compoff_request_details[0]['id'] = $id;
    				$compoff_request_details[0]['status'] = $compoffstatus;
    				$compoff_request_details[0]['daycount'] = $appliedcompoffcount;
    
    				if ($compoff_request_details[0]['to_date'] == '' || $compoff_request_details[0]['to_date'] == NULL)
    					$compoff_request_details[0]['to_date'] = $compoff_request_details[0]['from_date'];
    			}
    			$this->sendemail('compoff',$id, $compoffstatus,$loginUserName);
    		}
    	}
    	$this->_helper->json(array(
    			'result' => 'saved',
    			'message' => $messagestr,
    			'controller' => 'employeerequests'
    	)); 
    
    	$this->_helper->json ( $result );
    }
    
    public function sendemailconfirmationAction()
    {
        session_write_close();
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $loginUserId = $auth->getStorage()->read()->id;
        }
        if (! $this->getRequest()->getPost()) {
            return;
        }
        $mainrequest = $this->getRequest()->getPost();
        $usersmodel = new Default_Model_Users();
        $employeesmodel = new Default_Model_Employees();
        if ($loginUserId != '' && $loginUserId != NULL) {
            $loggedinEmpId = $usersmodel->getUserDetailsByID($loginUserId);
            $requestEmpId = $usersmodel->getUserDetailsByID($mainrequest['user_id'])['0'];
            $loginUserName = $loggedinEmpId[0]['userfullname'];

            $employeeemail = $requestEmpId['emailaddress'];
            $userfullname = $requestEmpId['userfullname'];

            $requestEmpDetails = $employeesmodel->getLoggedInEmployeeDetails($mainrequest['user_id']);

            $reportingmanagerId = $requestEmpDetails[0]['reporting_manager'];

            $businessunitid = $requestEmpDetails[0]['businessunit_id'];
            $reportingManagerDetails = $usersmodel->getUserDetailsByID($reportingmanagerId);
            if (! empty($reportingManagerDetails)) {
                $reportingManageremail = $reportingManagerDetails[0]['emailaddress'];
                $reportingmanagerName = $reportingManagerDetails[0]['userfullname'];
            }
        } else {
            return;
        }

        $status = $mainrequest['status'];
        $type = $mainrequest['type'];
        $othertype = $mainrequest['other_type'];
        $leavereq_type =$mainrequest ['leavetype'];

        $toemailArr = $reportingManageremail;

        $options['header'] = "$type Request";
        $options['toEmail'] = $employeeemail;
        $options['toName'] = $userfullname;
        if ($type==Leave){
        if ($status == 'Approved') {
            $options['subject'] = "$type request approved ( $leavereq_type )";
            $options['message'] = '<div>
	        		<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
            		<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below ' . $othertype . '(s) has been approved by ' . $loginUserName . '.</div>
            				</div>';
        } elseif ($status == 'Rejected') {
            $options['subject'] = "$type request rejected ( $leavereq_type )";
            $options['message'] = '<div>
	        		<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
            		<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below ' . $othertype . '(s) has been rejected by ' . $loginUserName . '.</div>
            				</div>';
        } else {
            $options['subject'] = "$type request cancelled ( $leavereq_type )";
            $options['message'] = '<div>
	        		<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
            		<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below ' . $othertype . '(s) has been cancelled by ' . $loginUserName . '.</div>
            				</div>';
        }
        $options['message'] .= $this->getRequestEmailBody($mainrequest, $userfullname) . '
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="'.BASE_URL.'/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the '.strtolower($type).'  details.</div>
            		</div>
            		</div>';
        $result = sapp_Global::_sendEmail($options);
        }
        else {if ($status == 'Approved') {
            $options['subject'] = "$type request approved ";
            $options['message'] = '<div>
	        		<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
            		<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below ' . $othertype . '(s) has been approved by ' . $loginUserName . '.</div>
            				</div>';
        } elseif ($status == 'Rejected') {
            $options['subject'] = "$type request rejected ";
            $options['message'] = '<div>
	        		<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
            		<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below ' . $othertype . '(s) has been rejected by ' . $loginUserName . '.</div>
            				</div>';
        } else {
            $options['subject'] = "$type request cancelled ";
            $options['message'] = '<div>
	        		<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
            		<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below ' . $othertype . '(s) has been cancelled by ' . $loginUserName . '.</div>
            				</div>';
        }
        $options['message'] .= $this->getRequestEmailBody($mainrequest, $userfullname) . '
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="'.BASE_URL.'/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the '.strtolower($type).' details.</div>
            		</div>
            		</div>';
        $result = sapp_Global::_sendEmail($options);
            
        }
        if (! empty($reportingManageremail) && ! empty($reportingmanagerName)) {
            $options['header'] = "$type Request";
            $options['toEmail'] = $reportingManageremail;
            $options['toName'] = $reportingmanagerName;
            $result = sapp_Global::_sendEmail($options);
        }

        if (defined('LV_HR_' . $businessunitid) && $businessunitid != '') {
            $options['header'] = "$type Request";
            $options['toEmail'] = constant('LV_HR_' . $businessunitid);
            $options['toName'] = "$type Management";
            $result = sapp_Global::_sendEmail($options);
        }
    }

    private function getRequestEmailBody($request, $username)
    {
        if ($request['type'] == "Leave") {

            $body = '<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px;font-family:Arial,Helvetica,sans-serif;font-weight:bold;margin:30px 0 30px 0" bgcolor="#ffffff">
                      <tbody>
	                      <tr bgcolor="#045396">
	                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
	                        <td width="72%" style="color:#ffffff">' . $username . '</td>
	                      </tr>
	                      <tr bgcolor="#F5F5F3">
	                        <td style="border-right:2px solid #BBBBBB;">No. of Day(s)</td>
	                        <td>' . $request['daycount'] . '</td>
	                      </tr>
                          <tr bgcolor="#D9DCE5">
	                        <td style="border-right:2px solid #BBBBBB;">Leave Type</td>
	                        <td>' . $request['leavetype'] . '</td>
	                      </tr>
	                      <tr bgcolor="#F5F5F3">
	                        <td style="border-right:2px solid #BBBBBB;">From</td>
	                        <td>' . $request['from_date'] . '</td>
	                      </tr>
	                      <tr bgcolor="#D9DCE5">
	                        <td style="border-right:2px solid #BBBBBB;">To</td>
	                        <td>' . $request['to_date'] . '</td>
     	            	  </tr>
		    	          <tr bgcolor="#F5F5F3">
	                        <td style="border-right:2px solid #BBBBBB;">Reason</td>
	                        <td>' . $_SESSION ['managercomments'] . '</td>
	                      </tr>
                		</tbody>
                </table>

            </div>';
        }
        else if ($request['type'] == "Exception"){
            $body = '<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px;font-family:Arial,Helvetica,sans-serif;font-weight:bold;margin:30px 0 30px 0" bgcolor="#ffffff">
                      <tbody>
	                      <tr bgcolor="#045396">
	                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
	                        <td width="72%" style="color:#ffffff">' . $username . '</td>
	                      </tr>
	                      <tr bgcolor="#F5F5F3">
	                        <td style="border-right:2px solid #BBBBBB;">No. of Day(s)</td>
	                        <td>' . $request['daycount'] . '</td>
	                      </tr>
	                      <tr bgcolor="#D9DCE5">
	                        <td style="border-right:2px solid #BBBBBB;">From</td>
	                        <td>' . $request['from_date'] . '</td>
	                      </tr>
	                      <tr bgcolor="#F5F5F3">
	                        <td style="border-right:2px solid #BBBBBB;">To</td>
	                        <td>' . $request['to_date'] . '</td>
     	            	  </tr>
		    	          <tr bgcolor="#D9DCE5">
	                        <td style="border-right:2px solid #BBBBBB;">Reason</td>
	                        <td>' . $_SESSION ['managercomments'] . '</td>
	                      </tr>
                		</tbody>
                </table>

            </div>';
        }
        else {
            $body = '<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px;font-family:Arial,Helvetica,sans-serif;font-weight:bold;margin:30px 0 30px 0" bgcolor="#ffffff">
                      <tbody>
	                      <tr bgcolor="#045396">
	                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
	                        <td width="72%" width="72%" style="color:#ffffff">' . $username . '</td>
	                      </tr>
	                      <tr bgcolor="#F5F5F3">
	                        <td style="border-right:2px solid #BBBBBB;">Reimbursement Option</td>
	                        <td>' . $request['optiontext'] . '</td>
     	            	  </tr>
                          <tr bgcolor="#D9DCE5">
	                        <td style="border-right:2px solid #BBBBBB;">Manager Comments </td>
	                        <td>' . $_SESSION ['managercomments'] . '</td>
	                      </tr>
                		</tbody>
                </table>

            </div>';
        }
        return $body;
    }
}