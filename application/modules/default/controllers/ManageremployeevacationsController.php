<?php
/*********************************************************************************


 *

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/

class Default_ManageremployeevacationsController extends Zend_Controller_Action
{

    private $options;
	public function preDispatch()
	{
		$ajaxContext = $this->_helper->getHelper ( 'AjaxContext' );
		$ajaxContext->addActionContext ( 'emailsendconfirmation', 'json' )->initContext ();

	}

    public function init()
    {
        $this->_options= $this->getInvokeArg('bootstrap')->getOptions();

    }

    public function indexAction()
    {
	    $auth = Zend_Auth::getInstance();
     	if($auth->hasIdentity()){
					$loginUserId = $auth->getStorage()->read()->id;
		}
		$leaverequestmodel = new Default_Model_Leaverequest();
        $call = $this->_getParam('call');
		if($call == 'ajaxcall')
				$this->_helper->layout->disableLayout();

		$view = Zend_Layout::getMvcInstance()->getView();
		$objname = $this->_getParam('objname');
		$refresh = $this->_getParam('refresh');
		$dashboardcall = $this->_getParam('dashboardcall');

		$data = array();
		$searchQuery = '';
		$searchArray = array();
		$tablecontent='';

		if($refresh == 'refresh')
		{
		    if($dashboardcall == 'Yes')
				$perPage = DASHBOARD_PERPAGE;
			else
				$perPage = PERPAGE;
			$sort = 'ASC';$by = 'leavestatus';$pageNo = 1;$searchData = '';
		}
		else
		{
			$sort = ($this->_getParam('sort') !='')? $this->_getParam('sort'):'ASC';
			$by = ($this->_getParam('by')!='')? $this->_getParam('by'):'leavestatus';
			if($dashboardcall == 'Yes')
				$perPage = $this->_getParam('per_page',DASHBOARD_PERPAGE);
			else
			    $perPage = $this->_getParam('per_page',PERPAGE);
			$pageNo = $this->_getParam('page', 1);
			/** search from grid - START **/
			$searchData = $this->_getParam('searchData');
			$searchData = rtrim($searchData,',');
			/** search from grid - END **/
		}


		$objName = 'manageremployeevacations';
		$queryflag = '';
		$dataTmp = $leaverequestmodel->getGrid($sort, $by, $perPage, $pageNo, $searchData,$call,$dashboardcall,$objName,$queryflag);

		array_push($data,$dataTmp);
		$this->view->dataArray = $data;
		$this->view->call = $call ;
		$this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    public function emailsendconfirmationAction(){
    	session_write_close();
    	$this->_helper->layout->disableLayout();
    	$auth = Zend_Auth::getInstance ();
    	$usersmodel = new Default_Model_Users ();
    	$leaverequestmodel = new Default_Model_Leaverequest ();
    	if ($auth->hasIdentity ()) {
    		$loginUserId = $auth->getStorage ()->read ()->id;
    	}
    	$loginUserName = ($leaverequestmodel->getUserName ( $loginUserId ));
    	$status = $this->_request->getParam ( 'status' );
    	$id = $this->_request->getParam ( 'id' );
    	if ($id && is_numeric ( $id ) && $id > 0) {
    		$leave_details = $leaverequestmodel->getLeaveDetails ( $id );
    		if (sapp_Global::_decrypt ( $status ) == 'Cancelled') {
    			$leavestatus = 4;
    			$successmsg = 'Leave request cancelled succesfully.';
    			$subject = 'Leave request cancelled';
    			$message = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
    					<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below leave(s) has been cancelled by ' . $loginUserName . '.</div>
    							</div>';
    		}
    		elseif (sapp_Global::_decrypt ( $status ) == 'Approved') {
    			$leavestatus = 2;
    			$successmsg = 'Leave request approved succesfully.';
    			$subject = 'Leave request approved';
    			$message = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
    					<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below leave(s) has been approved by ' . $loginUserName . '.</div>
    							</div>';
    		}
    		elseif (sapp_Global::_decrypt ( $status ) == 'Rejected') {
    			$leavestatus = 3;
    			$successmsg = 'Leave request rejected succesfully.';
    			$subject = 'Leave request rejected';
    			$message = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
    					<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below leave(s) has been rejected by ' . $loginUserName . '.</div>
    							</div>';
    		}
    		$businessunitid = $leave_details ['bunit_id'];
    		$userDetails = $usersmodel->getUserDetailsByID ( $leave_details ['user_id'] );
    		$employeename = $userDetails [0] ['userfullname'];
    		for($i = 1; $i <= 3; $i ++) {
    			$toEmail = '';
    			$toName = '';
    			if ($i == 1) {
    				$userDetails = $usersmodel->getUserDetailsByID ( $leave_details ['user_id'] );
    				$toEmail = $userDetails [0] ['emailaddress'];
    				$toName = $userDetails [0] ['userfullname'];
    			} elseif ($i == 2) {
    				$repManagerDetails = $usersmodel->getUserDetailsByID ( $leave_details ['rep_mang_id'] );
    				$toEmail = $repManagerDetails [0] ['emailaddress'];
    				$toName = $repManagerDetails [0] ['userfullname'];
    			} elseif ($i == 3) {
    				if (defined ( 'LV_HR_' . $businessunitid ) && $businessunitid != '') {
    					$toEmail = constant ( 'LV_HR_' . $businessunitid );
    					$toName = 'Leave management';
    				}
    			}



    			if ($toEmail != '' && $toName != '') {
    				$options ['header'] = 'Leave Request';
    				$options ['toEmail'] = $toEmail;
    				$options ['toName'] = $toName;
    				$options ['subject'] = $subject;
    				$options ['message'] = $message;
    				$options ['message'] .= '<div style = "padding : 8px 50px 30px 30px">
                			<table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:20px 0 20px 0;" bgcolor="#ffffff">
	                      	<tbody>
		                      <tr bgcolor="#045396">
		                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
		                        <td width="72%" style="color:#ffffff;">' . $employeename . '</td>
		                      </tr>
		                      <tr bgcolor="#F5F5F3">
		                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
		                        <td style="color:#5b5b5b">' . $leave_details ['appliedleavescount'] . '</td>
		                      </tr>
		                      <tr bgcolor="#D9DCE5">
		                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
		                        <td style="color:#5b5b5b">' . $leave_details ['from_date'] . '</td>
		                      </tr>
		                      <tr bgcolor="#F5F5F3">
		                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
		                        <td style="color:#5b5b5b">' . $leave_details ['to_date'] . '</td>
		            	      </tr>
		                      <tr bgcolor="#D9DCE5">
		                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Leave</td>
		                        <td style="color:#5b5b5b">' . $leave_details ['reason'] . '</td>
	                  		  </tr>
                			</tbody>
                			</table>
							</div>
            				<div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the leave details.</div>
            						</div>
            						</div>';
    				sapp_Global::_sendEmail ( $options );
    			}
    		}
    	}
    }
     public function viewAction()
	{
	    $auth = Zend_Auth::getInstance();
     		if($auth->hasIdentity()){
					$loginUserId = $auth->getStorage()->read()->id;
			}
		$id = $this->getRequest()->getParam('id');
		$callval = $this->getRequest()->getParam('call');
		if($callval == 'ajaxcall')
			$this->_helper->layout->disableLayout();
		$objName = 'manageremployeevacations';
		$managerleaverequestform = new Default_Form_managerleaverequest();
		$managerleaverequestform->removeElement("submit");
		$elements = $managerleaverequestform->getElements();
		if(count($elements)>0)
		{
			foreach($elements as $key=>$element)
			{
				if(($key!="Cancel")&&($key!="Edit")&&($key!="Delete")&&($key!="Attachments")){
				$element->setAttrib("disabled", "disabled");
					}
        	}
        }

		try
		{
			    if($id && is_numeric($id) && $id>0)
                {
                	$this->_redirect('manageremployeevacations/edit/id/'.$id);
					$leaverequestmodel = new Default_Model_Leaverequest();
					$usersmodel= new Default_Model_Users();
					$flag = 'true';
					$userid = $leaverequestmodel->getUserID($id);
					$employeeuserid = $userid[0][user_id];
					$getreportingManagerArr = $leaverequestmodel->getReportingManagerId($employeeuserid);
					$reportingManager = $getreportingManagerArr[0]['repmanager'];
					$loginUserRole = ($leaverequestmodel->getEmployeeRole($loginUserId));
					if($reportingManager != $loginUserId && $loginUserRole != '1')
					   $flag = 'false';
					if(!empty($userid))
					 $isactiveuser = $usersmodel->getUserDetailsByID($userid[0]['user_id']);
					else
					 $this->view->rowexist = "rows";

					if(!empty($userid) && !empty($isactiveuser) && $flag == 'true')
					{
						$data = $leaverequestmodel->getLeaveRequestDetails($id);
						if(!empty($data) && $data[0]['leavestatus'] == 'Pending')
							{
								$data = $data[0];
								$employeeleavetypemodel = new Default_Model_Employeeleavetypes();
								$usersmodel = new Default_Model_Users();

								$employeeleavetypeArr = $employeeleavetypemodel->getsingleEmployeeLeavetypeData($data['leavetypeid']);
								if($employeeleavetypeArr !='norows')
								{
									$managerleaverequestform->leavetypeid->addMultiOption($employeeleavetypeArr[0]['id'],utf8_encode($employeeleavetypeArr[0]['leavetype']));
								}

								if($data['leaveday'] == 1)
								{
								  $managerleaverequestform->leaveday->addMultiOption($data['leaveday'],'Full Day');
								}
								else
								{
								  $managerleaverequestform->leaveday->addMultiOption($data['leaveday'],'Half Day');
								}

								$employeenameArr = $usersmodel->getUserDetailsByID($data['user_id']);
								$managerleaverequestform->populate($data);

															$from_date = sapp_Global::change_date($data['from_date'], 'view');
															$to_date = sapp_Global::change_date($data['to_date'], 'view');
															$appliedon = sapp_Global::change_date($data['createddate'], 'view');

								$managerleaverequestform->from_date->setValue($from_date);
								$managerleaverequestform->to_date->setValue($to_date);
								$managerleaverequestform->createddate->setValue($appliedon);
								$managerleaverequestform->appliedleavesdaycount->setValue($data['appliedleavescount']);
								$managerleaverequestform->employeename->setValue($employeenameArr[0]['userfullname']);
								$managerleaverequestform->setDefault('leavetypeid',$data['leavetypeid']);
								$managerleaverequestform->setDefault('leaveday',$data['leaveday']);
								$this->view->controllername = $objName;
								$this->view->id = $id;
								$this->view->form = $managerleaverequestform;
								$this->view->data = $data;
							}
						else
							{
								   $this->view->rowexist = "rows";
							}
					}
					else
					{
						   $this->view->rowexist = "rows";
					}
				}
				else
				{
					   $this->view->rowexist = "rows";
				}

        }
        catch(Exception $e)
		{
			 $this->view->rowexist = 'norows';
		}

	}


	public function editAction()
	{
	    $auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
				$loginUserId = $auth->getStorage()->read()->id;
		}
		$id = $this->getRequest()->getParam('id');
		$callval = $this->getRequest()->getParam('call');
		if($callval == 'ajaxcall')
			$this->_helper->layout->disableLayout();


		try
		{
		        if($id && is_numeric($id) && $id>0)
                {
                	$managerleaverequestform = new Default_Form_managerleaverequest();
					$leaverequestmodel = new Default_Model_Leaverequest();
					$usersmodel= new Default_Model_Users();
					$flag = 'true';
					$userid = $leaverequestmodel->getUserID($id);
					$employeeuserid = $userid[0][user_id];
					$getreportingManagerArr = $leaverequestmodel->getReportingManagerId($employeeuserid);
					$reportingManager = $getreportingManagerArr[0]['repmanager'];
					$loginUserRole = ($leaverequestmodel->getEmployeeRole($loginUserId));
					if($reportingManager != $loginUserId && $loginUserRole != '1')
					   $flag = 'false';
					if(!empty($userid))
					 $isactiveuser = $usersmodel->getUserDetailsByID($userid[0]['user_id']);
					else
					 $this->view->rowexist = "rows";

					if(!empty($userid) && !empty($isactiveuser) && $flag=='true')
					{
						$data = $leaverequestmodel->getLeaveRequestDetails($id);
						if(!empty($data) && ($data[0]['leavestatus'] == 'Pending' || $data[0]['leavestatus'] == 'Approved'))
							{
								$data = $data[0];
								$reason = $data['reason'];
								$comments = $data['comments'];
								$appliedleavescount = $data['appliedleavescount'];
								$employeeid = $data['user_id'];
								$leavetypeid = $data['leavetypeid'];
								$employeeleavetypemodel = new Default_Model_Employeeleavetypes();
								$usersmodel = new Default_Model_Users();
								$employeesmodel = new Default_Model_Employees();
								$businessunitid = '';
								$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails($employeeid);
								 if($loggedInEmployeeDetails[0]['businessunit_id'] != '')
									$businessunitid = $loggedInEmployeeDetails[0]['businessunit_id'];

							$employeeleavetypeArr = $employeeleavetypemodel->getsingleEmployeeLeavetypeData($data['leavetypeid']);
								if($employeeleavetypeArr != 'norows')
								{
									$managerleaverequestform->leavetypeid->addMultiOption($employeeleavetypeArr[0]['id'],utf8_encode($employeeleavetypeArr[0]['leavetype']));
									$data['leavetypeid']=$employeeleavetypeArr[0]['leavetype'];
								}
								else
					            {
								   $data['leavetypeid'] ="...";
						        }

								if($data['leaveday'] == 1)
								{
								  $managerleaverequestform->leaveday->addMultiOption($data['leaveday'],'Full Day');
								  $data['leaveday']=	'Full Day';
								}
								else
								{
								  $managerleaverequestform->leaveday->addMultiOption($data['leaveday'],'Half Day');
								  $data['leaveday']='Half Day';
								}

								$employeenameArr = $usersmodel->getUserDetailsByID($data['user_id']);
								$employeeemail = $employeenameArr[0]['emailaddress'];
								$employeename = $employeenameArr[0]['userfullname'];
								$managerleaverequestform->populate($data);

								if($data['leavestatus'] == 'Approved') {
									$managerleaverequestform->managerstatus->setLabel("Cancel");
									$managerleaverequestform->managerstatus->clearMultiOptions();
	                                $managerleaverequestform->managerstatus->addMultiOption(3,utf8_encode("Cancel"));
								}

															$from_date = sapp_Global::change_date($data['from_date'], 'view');
															$to_date = sapp_Global::change_date($data['to_date'], 'view');
															$appliedon = sapp_Global::change_date($data['createddate'], 'view');
								 //to show Leave Request history in view
								 $leaverequesthistory_model = new Default_Model_Leaverequesthistory();
								 $leave_history = $leaverequesthistory_model->getLeaveRequestHistory($id);
							  	 $this->view->leave_history = $leave_history;
							    //end
								$managerleaverequestform->from_date->setValue($from_date);
								$managerleaverequestform->to_date->setValue($to_date);
								$managerleaverequestform->createddate->setValue($appliedon);
								$managerleaverequestform->appliedleavesdaycount->setValue($data['appliedleavescount']);
								$managerleaverequestform->employeename->setValue($employeenameArr[0]['userfullname']);
								$managerleaverequestform->setDefault('leavetypeid',$data['leavetypeid']);
								$managerleaverequestform->setDefault('leaveday',$data['leaveday']);
								$this->view->id = $id;
								$this->view->form = $managerleaverequestform;
								$this->view->data = $data;
								$managerleaverequestform->setAttrib('action',BASE_URL.'manageremployeevacations/edit/id/'.$id);
							}
							else
							{
								   $this->view->rowexist = "rows";
							}
					}
					else
					{
						   $this->view->rowexist = "rows";
					}
                }
				else
				{
					   $this->view->rowexist = "rows";
				}

		}
		catch(Exception $e)
		{
			 $this->view->rowexist = 'norows';
		}
		if($this->getRequest()->getPost()){
      		$result = $this->save($managerleaverequestform,$appliedleavescount,$employeeemail,$employeeid,$employeename,$from_date,$to_date,$reason,$businessunitid,$leavetypeid,$data);
		    $this->view->msgarray = $result;
		}
	}

	public function save($managerleaverequestform,$appliedleavescount,$employeeemail,$employeeid,$userfullname,$from_date,$to_date,$reason,$businessunitid,$leavetypeid,$leavereqdata)
	{
		$auth = Zend_Auth::getInstance();
     	if($auth->hasIdentity()){
					$loginUserId = $auth->getStorage()->read()->id;
		}
     		if($managerleaverequestform->isValid($this->_request->getPost())){
			    $id = $this->_request->getParam('id');
			    $managerstatus = $this->_request->getParam('managerstatus');
			    $comments = $this->_request->getParam('comments');
			    $notify_me_on = $this->_request->getParam('notify_me_on');
				$date = new Zend_Date();
				$leaverequestmodel = new Default_Model_Leaverequest();
				$employeeleavetypesmodel = new Default_Model_Employeeleavetypes();
				$employeeleavemodel = new Default_Model_Employeeleaves();
				$usersmodel = new Default_Model_Users();
				$empleavesummarymodel = new Default_Model_EmpLeaveSummary ();
				$actionflag = '';
				$tableid  = '';
				$status = '';
				$messagestr = '';
				$successmessagestr = '';
				//$leavetypetext = '';
				$leavetypeArr = $employeeleavetypesmodel->getLeavetypeDataByID($leavetypeid);
				$repManagerDetails = $usersmodel->getUserDetailsByID($leavereqdata['rep_mang_id']);
				$loginUserName = ($leaverequestmodel->getUserName($loginUserId));
				if($leavereqdata['parent'] != null){
					$parentLeaveReqData = $leaverequestmodel->getParentLeaveRequestDetails($leavereqdata['parent']);
					$parentLeaveData = $employeeleavetypesmodel->getLeavetypeDataByID($parentLeaveReqData[0]['leavetypeid']);

				}
				if(!empty($repManagerDetails)) {
					$repMgrEmail = $repManagerDetails[0]['emailaddress'];
					$repMgrName = $repManagerDetails[0]['userfullname'];
				}
				if($managerstatus == 1 && !empty($leavetypeArr))
				{
				  $status = 2;
				  $messagestr = "Leave request approved";
				 $successmessagestr  = "Leave request approved successfully.";
				  //$leavetypetext = $leavetypeArr[0]['leavetype'];
				}else if($managerstatus == 2)
				{
				  $status = 3;

				  if($leavereqdata['parent'] != null){
				    $pData = array('isactive'=>1);
				    $pWhere = array('id=?'=>$parentLeaveReqData[0]['id']);

				    $leaverequestmodel->SaveorUpdateLeaveRequest($pData, $pWhere);

				    if ($parentLeaveData[0] ['leavepredeductable'] == 1) {
				          $updateemployeeleave = $leaverequestmodel->updateemployeeleaves ( $parentLeaveReqData [0]['appliedleavescount'], $parentLeaveReqData [0]['user_id'], $parentLeaveReqData [0]['leavetypeid'] );
				    }

				  }

				  if($leavetypeArr[0]['leavepredeductable'] == 1) {
				      $updateemployeeleave = $leaverequestmodel->updatecancelledemployeeleaves($appliedleavescount,$employeeid,$leavetypeid);
				  }
				  $messagestr = "Leave request rejected";
				  $successmessagestr  = "Leave request rejected successfully.";
				}else if($managerstatus == 3 && !empty($leavetypeArr))
				{
				   if($leavereqdata['parent'] != null){
				        $pData = array('isactive'=>1);
				        $pWhere = array('id=?'=>$parentLeaveReqData[0]['id']);

				        $leaverequestmodel->SaveorUpdateLeaveRequest($pData, $pWhere);

				        if ($parentLeaveData[0] ['leavepredeductable'] == 1) {
				            $updateemployeeleave = $leaverequestmodel->updateemployeeleaves ( $parentLeaveReqData [0]['appliedleavescount'], $parentLeaveReqData [0]['user_id'], $parentLeaveReqData [0]['leavetypeid'] );
				        }

				    }

				    if($leavetypeArr[0]['leavepredeductable'] == 1) {
				        $updateemployeeleave = $leaverequestmodel->updatecancelledemployeeleaves($appliedleavescount,$employeeid,$leavetypeid);
				    }
					$status = 4;
				  	$messagestr = "Leave request cancelled";
					$successmessagestr  = "Leave request cancelled successfully.";
				}

				  if($managerstatus == 1 || $managerstatus == 2 || $managerstatus == 3)
				  {
				   $data = array( 'leavestatus'=>$status,
				   				  'approver_comments'=> $comments,
				   				  'notify_me_on'=> $notify_me_on,
				                  'modifiedby'=>$loginUserId,
								  'modifieddate'=>gmdate("Y-m-d H:i:s")
						);
						if($id!=''){
							$where = array('id=?'=>$id);
							$actionflag = 2;
						}
						else
						{
							$data['createdby'] = $loginUserId;
							$data['createddate'] = gmdate("Y-m-d H:i:s");
							$data['isactive'] = 1;
							$where = '';
							$actionflag = 1;
						}

						if($leavereqdata['parent'] != null && $managerstatus != 1){
						    $data['isactive'] = 0;
						}

						$Id = $leaverequestmodel->SaveorUpdateLeaveRequest($data, $where);
						    if($Id == 'update')
							{
							   $tableid = $id;
							   $this->_helper->getHelper("FlashMessenger")->addMessage($successmessagestr);
							}
							else
							{
							   $tableid = $Id;
								$this->_helper->getHelper("FlashMessenger")->addMessage($successmessagestr);
							}

							/**
					leave request history
					**/
					if($Id == 'update')
					{

						if($managerstatus == 1)
						{
							$leavestatus='Approved';
						}
						else if($managerstatus == 2)
						{
							$leavestatus='Rejected';
						}
						else
						{
							$leavestatus='Cancelled';
						}
						$history = 'Leave Request has been '.$leavestatus.' by ';
						$leaverequesthistory_model = new Default_Model_Leaverequesthistory();
						$leave_history = array(
										'leaverequest_id' =>$id ,
										'description' => $history,
										'createdby' => $loginUserId,
										'modifiedby' => $loginUserId,
										'isactive' => 1,
										'createddate' =>gmdate("Y-m-d H:i:s"),
										'modifieddate'=>gmdate("Y-m-d H:i:s"),
									   );
					    $where = '';
						$leavehistory = $leaverequesthistory_model->saveOrUpdateLeaveRequestHistory($leave_history,$where);
					}

                            /** MAILING CODE **/

							if($to_date == '' || $to_date == NULL)
								$to_date = $from_date;


							/* Mail to Employee */
								$options['header'] = 'Leave Request';
								$options['toEmail'] = $employeeemail;
								$options['toName'] = $userfullname;
								if($messagestr ==  'Leave request approved'){
									$options['subject'] = $messagestr;
									$options['message'] = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
											<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below leave(s) has been approved by '.$loginUserName.'.</div>
													</div>';
								}elseif($messagestr == 'Leave request rejected'){
									$options['subject'] = $messagestr;
									$options['message'] = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
											<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below leave(s) has been rejected by '.$loginUserName.'. </div>
						</div>';
								}else{
									$options['subject'] = $messagestr;
									$options['message'] = '<div>
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
											<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below leave(s) has been cancelled by '.$loginUserName.'.</div>
					</div>';
								}
								$options['message'] .= '<div style = "padding : 8px 50px 30px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:20px 0 20px 0;" bgcolor="#ffffff">
                      <tbody>
					  <tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff;">'.$userfullname.'</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">'.$appliedleavescount.'</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">'.$from_date.'</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">'.$to_date.'</td>
            	     </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Leave</td>
                        <td style="color:#5b5b5b">'.$reason.'</td>
                  </tr>
                   </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Comment</td>
                        <td style="color:#5b5b5b">'.$comments.'</td>
                  </tr>

                </tbody></table>

            </div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="'.BASE_URL.'/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the leave details.</div>
            		</div>
            		</div>';
                                $result = sapp_Global::_sendEmail($options);
							/* END */


		/* Mail to Reporting Manager */
            if(!empty($repMgrEmail) && !empty($repMgrName)) {
								$options['header'] = 'Leave Request';
								$options['toEmail'] = $repMgrEmail;
								$options['toName'] = $repMgrName;
								if($messagestr == 'Leave request approved'){
									$options['subject'] = $messagestr;
									$options['message'] = '<div>
				<div style = "border:3px solid transparent;border-radius: 15px;background-color:#eeeeee; ">
				<div style="background-color:#5B5A55;border-radius: 10px 10px 0px 0px;">
											<div style="color:#ffffff;padding-left:5px;font-family:Verdana;font-size:14px;">Hi,</div>
											<div style="color:#ffffff;padding-left:5px;font-family:Verdana;font-size:14px;">The below leave(s) has been approved by '.$loginUserName.'.</div>
													</div>';
								}elseif($messagestr == 'Leave request rejected'){
									$options['subject'] = $messagestr;
									$options['message'] = '<div>
				<div style = "border:3px solid transparent;border-radius: 15px;background-color:#eeeeee; ">
				<div style="background-color:#5B5A55;border-radius: 10px 10px 0px 0px;">
											<div style="color:#ffffff;padding-left:5px;font-family:Verdana;font-size:14px;">Hi,</div>
											<div style="color:#ffffff;padding-left:5px;font-family:Verdana;font-size:14px;">The below leave(s) has been rejected by '.$loginUserName.'. </div>
											</div>';
								}else{
									$options['subject'] = $messagestr;
									$options['message'] = '<div>
				<div style = "border:3px solid transparent;border-radius: 15px;background-color:#eeeeee; ">
				<div style="background-color:#5B5A55;border-radius: 10px 10px 0px 0px;">
											<div style="color:#ffffff;padding-left:5px;font-family:Verdana;font-size:14px;">Hi,</div>
											<div style="color:#ffffff;padding-left:5px;font-family:Verdana;font-size:14px;">The below leave(s) has been cancelled by '.$loginUserName.'. </div>
											</div>';
								}
								$options['message'] .= '<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					<tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">'.$userfullname.'</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">'.$appliedleavescount.'</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">'.$from_date.'</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">'.$to_date.'</td>
            	     </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Leave</td>
                        <td style="color:#5b5b5b">'.$reason.'</td>
                  </tr>
                </tbody></table>

            </div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="'.BASE_URL.'/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the leave details.</div>
            		</div>
            		</div>';
                                $result = sapp_Global::_sendEmail($options);
            }
							/* END */

							/* Mail to HR */
								if (defined('LV_HR_'.$businessunitid) && $businessunitid !='')
								{
								$options['header'] = 'Leave Request';
								$options['toEmail'] = constant('LV_HR_'.$businessunitid);
								$options['toName'] = 'Leave Management';
								if($messagestr == 'Leave request approved'){
									$options['subject'] = $messagestr;
									$options['message'] = '<div>
				<div style = "border:3px solid transparent;border-radius: 15px;background-color:#eeeeee; ">
				<div style="background-color:#5B5A55;border-radius: 10px 10px 0px 0px;">
											<div>Hi,</div>
											<div>The below leave(s) has been approved by '.$loginUserName.'.</div>
								</div>';
								}elseif($messagestr == 'Leave request rejected'){
									$options['subject'] = $messagestr;
									$options['message'] = '<div>
				<div style = "border:3px solid transparent;border-radius: 15px;background-color:#eeeeee; ">
				<div style="background-color:#5B5A55;border-radius: 10px 10px 0px 0px;">
											<div style="color:#ffffff;padding-left:5px;font-family:Verdana;font-size:14px;">Hi,</div>
											<div style="color:#ffffff;padding-left:5px;font-family:Verdana;font-size:14px;">The below leave(s) has been rejected by '.$loginUserName.'.</div>
													</div>';
								}else{
									$options['subject'] = $messagestr;
									$options['message'] = '<div>
				<div style = "border:3px solid transparent;border-radius: 15px;background-color:#eeeeee; ">
				<div style="background-color:#5B5A55;border-radius: 10px 10px 0px 0px;">
											<div style="color:#ffffff;padding-left:5px;font-family:Verdana;font-size:14px;">Hi,</div>
											<div style="color:#ffffff;padding-left:5px;font-family:Verdana;font-size:14px;">The below leave(s) has been cancelled by '.$loginUserName.'.</div>
													</div>';
								}
								$options['message'] .= '<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
	                      <tr bgcolor="#045396">
	                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
	                        <td width="72%" style="color:#ffffff">'.$userfullname.'</td>
	                      </tr>
	                      <tr bgcolor="#F5F5F3">
	                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
	                        <td style="color:#5b5b5b">'.$appliedleavescount.'</td>
	                      </tr>
	                      <tr bgcolor="#D9DCE5">
	                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
	                        <td style="color:#5b5b5b">>'.$from_date.'</td>
	                      </tr>
	                      <tr bgcolor="#F5F5F3">
	                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
	                        <td style="color:#5b5b5b">'.$to_date.'</td>
     	            	  </tr>
		    	          <tr bgcolor="#D9DCE5">
	                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Leave</td>
	                        <td style="color:#5b5b5b">'.$reason.'</td>
	                      </tr>
                		</tbody>
                </table>

            </div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="'.BASE_URL.'/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the leave details.</div>
            		</div>
            		</div>';
                                $result = sapp_Global::_sendEmail($options);
								}
							/* END */
						$menuID = MANAGEREMPLOYEEVACATIONS;
						$result = sapp_Global::logManager($menuID,$actionflag,$loginUserId,$tableid);
	    			    $this->_redirect('manageremployeevacations');
					}

			}else
			{
     			$messages = $managerleaverequestform->getMessages();
				foreach ($messages as $key => $val)
					{
						foreach($val as $key2 => $val2)
						 {
							$msgarray[$key] = $val2;
							break;
						 }
					}
				return $msgarray;
			}
	}

	public function deleteAction()
	{
	     $auth = Zend_Auth::getInstance();
     		if($auth->hasIdentity()){
					$loginUserId = $auth->getStorage()->read()->id;
				}
		 $id = $this->_request->getParam('objid');
		 $messages['message'] = '';
		 $actionflag = 3;
		    if($id)
			{
			$holidaygroupsmodel = new Default_Model_Holidaygroups();
			  $data = array('isactive'=>0);
			  $where = array('id=?'=>$id);
			  $Id = $holidaygroupsmodel->SaveorUpdateGroupData($data, $where);
			    if($Id == 'update')
				{
				   $menuID = HOLIDAYGROUPS;
				   $result = sapp_Global::logManager($menuID,$actionflag,$loginUserId,$id);
				   $messages['message'] = 'Holiday group deleted successfully.';
				}
				else
                   $messages['message'] = 'Holiday group cannot be deleted.';
			}
			else
			{
			 $messages['message'] = 'Holiday group cannot be deleted.';
			}
			$this->_helper->json($messages);

	}
}

