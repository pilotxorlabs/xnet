<?php 
class Default_WorklogreportController extends Zend_Controller_Action {
    private $options;
    
    public function preDispatch() {
        $ajaxContext = $this->_helper->getHelper ( 'AjaxContext' );
    }
    public function init() {
        $this->_options = $this->getInvokeArg ( 'bootstrap' )->getOptions ();
    }
    public function indexAction() {        
        $orgModel = new Default_Model_Organisationinfo();
        $organisationInfo = $orgModel->getOrganisationData(1);
        
        
        $configurationModel = new Timemanagement_Model_Configuration();
        $activerecordArr = $configurationModel->getActiveRecord();
        
        if(!empty($activerecordArr))
            $this->view->dataArray = $activerecordArr;
        if($organisationInfo['org_startdate'] != null){
            $org_start_date = (new DateTime($organisationInfo['org_startdate']))->format('Y');
            $org_start_date = $org_start_date . ','  . ((new DateTime($organisationInfo['org_startdate']))->format('m') - 1) . ',';
            $org_start_date = $org_start_date . (new DateTime($organisationInfo['org_startdate']))->format('d');
            
            $this->view->data= $org_start_date;
        }
    }
    public function downloadreportAction() {
        $auth = Zend_Auth::getInstance ();
        if ($auth->hasIdentity ()) {
            $loginUserId = $auth->getStorage ()->read ()->id;
        }
        $worklogModel = new Api_Model_Worklogs();
        $tm_activities = $worklogModel->getActivities();
        $duration = $this->_request->getParams () ['duration'];
        $startDate= ''; $endDate= '';        
        if($duration == 4){
            $startDate=$this->getRequest ()->getPost () ['from_date'] ;
            $endDate= $this->getRequest ()->getPost () ['to_date'] ;
        }else{
            $worklogModel->getdurationDates($duration, $startDate, $endDate);
        }
        $this->_helper->viewRenderer->setNoRender ();
        $this->_helper->layout->disableLayout ();
        
        require_once 'Classes/PHPExcel.php';
        require_once 'Classes/PHPExcel/IOFactory.php';
        $objPHPExcel = new PHPExcel ();
        $columns = $worklogModel->createColumnsArray( 'ZZ' );
        $fileName = 'Worklog Report.xlsx';
        $mainSheet = 'Dashboard';
        $cellName = '';
        $colIndex = 0;
        fputs ( $fp, "\n" );
        $columnNames =  array (	"Employee Name", "Missed Entry", "Incomplete Entry");
        foreach ($tm_activities as $activity){
            if($activity['activity_code'] == 'NOC'){
                array_splice($columnNames, 3,0,$activity['activity_code']);
            }else{
                array_push($columnNames, $activity['activity_code']);
            }
        }
        $style = array (
            'alignment' => array (
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        
        
        $objPHPExcel->getActiveSheet ()->setCellValue ( 'A1', 'Worklog Report ['.$startDate .' To ' .$endDate.']');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:S1');
        $objPHPExcel->getActiveSheet ()->getStyle ( 'A1:S1' )->getFont ()->setBold ( true ) ->setName('Arial')->setSize(14);
        $objPHPExcel->getActiveSheet ()->getStyle ( 'A1:S1' )->applyFromArray ( $style );
        // Make first row Headings bold and highlighted in Excel.
        
        $objPHPExcel->getActiveSheet ()->setTitle ( $mainSheet );
        $objPHPExcel->getActiveSheet ()->getDefaultStyle ()->applyFromArray ( $style );
        foreach ( $columnNames as $columnName ) {
            $row = 3;
            $cellName = $columns [$colIndex] . $row;
            
            $objPHPExcel->getActiveSheet ()->setCellValue ( $cellName, $columnName );
            $objPHPExcel->getActiveSheet ()->getStyle ( $cellName )->getFont ()->setBold ( true );
            $objPHPExcel->getActiveSheet ()->getColumnDimension ( $columns [$colIndex] )->setAutoSize ( true );
            $objPHPExcel->getActiveSheet ()->getStyle ( $cellName )->applyFromArray ( array (
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'd8d8d8')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            ) );
            $colIndex ++;
        }
        
        // Display field/column values in Excel.
        $row = 4;
        
        $dailySheetData = $_SESSION ['dailySheetData'];
        foreach ( $_SESSION ['worklogReport'] as $employeeId => $worklogData ) {
            $colIndex = 0;
            foreach ( $worklogData as $worklogDatum ) {
                $cellName = $columns [$colIndex] . $row;
                $objPHPExcel->getActiveSheet ()->SetCellValue ( $cellName, $worklogDatum );
                $objPHPExcel->getActiveSheet ()->getStyle ( $cellName )->applyFromArray ( array (
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                    
                ) );
                if ($columns [$colIndex] == 'B') {
                    $objPHPExcel->getActiveSheet ()->getStyle ( $cellName )->applyFromArray ( array (
                        'font' => [
                            'color' => [
                                'rgb' => '9C0006'
                            ]
                        ],
                        'fill' => [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'E6B9B8')
                        ],
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                                 )
                            )
                        
                    ) );
                    
                }
                if ($columns [$colIndex] == 'C') {
                    $objPHPExcel->getActiveSheet ()->getStyle ( $cellName )->applyFromArray ( array (
                        'font' => [
                            'color' => [
                                'rgb' => '9C6500'
                            ]
                        ],
                        'fill' => [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFEB9C')
                        ],
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                        
                    ) );
                    
                }
                
                if ($columns [$colIndex] == 'D') {
                    $objPHPExcel->getActiveSheet ()->getStyle ( $cellName )->applyFromArray ( array (
                        'font' => [
                            'color' => [
                                'rgb' => 'E46D0A'
                            ]
                        ],
                        'fill' => [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FCD5B4')
                        ],
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                        
                    ) );
                    
                }
                
                if ($columns [$colIndex] == 'A') {
                    $objPHPExcel->createSheet ()->setTitle ( $worklogDatum );
                    $objPHPExcel->getSheetByName ( $worklogDatum )->getDefaultStyle ()->applyFromArray ( $style );
                    $objPHPExcel->getActiveSheet ()->getCell ( $cellName )->getHyperlink ()->setUrl ( "sheet://'" . $worklogDatum . "'!A2" );
                    $objPHPExcel->getActiveSheet ()->getStyle ( $cellName )->applyFromArray ( array (
                        'font' => [
                            'color' => [
                                'rgb' => '0000FF'
                            ],
                            'underline' => 'single'
                        ]
                    ) );
                    $this->fillWorklogSheet( $employeeId, $worklogDatum, $objPHPExcel, $dailySheetData[$employeeId]);
                    
                   $objPHPExcel->setActiveSheetIndexByName ( $mainSheet );
                }
                $colIndex ++;
            }
            $row ++;
        }
        
        sapp_Global::clean_output_buffer ();
        header ( 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
        header ( "Content-Disposition: attachment; filename=\"$fileName\"" );
        header ( 'Cache-Control: max-age=0' );
        sapp_Global::clean_output_buffer ();
        
        $objWriter = PHPExcel_IOFactory::createWriter ( $objPHPExcel, 'Excel2007' );
        $objWriter->save ( 'php://output' );
        
        exit ();
    }
    
    public function fillWorklogSheet($employeeId, $sheetName, &$objPHPExcel,$worklogData) {
        
        $columns = range ( 'A', 'Z' );        
        $columnNames = array (
            "Date",
            "Project",
            "Task",
            "JIRA ID",
            "External ID",
            "Summary",
            "Pln hours",
            "Activity Code"
        );
        $colIndex = 0;
        $objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( 'A1', 'Back to Dashboard' );
        $objPHPExcel->getSheetByName ( $sheetName )->getCell ( 'A1' )->getHyperlink ()->setUrl ( "sheet://'Dashboard'!A2" );
        $objPHPExcel->getSheetByName ( $sheetName )->getStyle ( 'A1' )->applyFromArray ( array (
            'font' => [
                'color' => [
                    'rgb' => '0000FF'
                ],
                'underline' => 'single'
            ]
        ) );
        
        // Adding Headings to the sheet.
        foreach ( $columnNames as $columnName ) {
            $row = 3;
            $cellName = $columns [$colIndex] . $row;
            $objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $columnName );
            $objPHPExcel->getSheetByName ( $sheetName )->getStyle ( $cellName )->getFont ()->setBold ( true );
            $objPHPExcel->getSheetByName ( $sheetName )->getColumnDimension ( $columns [$colIndex] )->setAutoSize ( true );
            $objPHPExcel->getSheetByName ( $sheetName )->getStyle ( $cellName )->applyFromArray ( array (
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'd8d8d8')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            ) );
            $colIndex ++;
        }
        $row = 4;
        // displaing the othe values.
        $rowstart = 4;
        $rowend = 4;
        $plannedHours = 0;
        foreach ( $worklogData as $key => $worklogDatum) {
            $colIndex = 0;
            if($worklogDatum['status'] == 'Weekend'){
                $objPHPExcel->getActiveSheet ()->setCellValue ( 'B'.$row, 'Weekend');               
                $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':H'.$row);
                $objPHPExcel->getActiveSheet ()->getStyle ( 'A'.$row.':H'.$row)->applyFromArray ( array (
                    'font' => [
                        'color' => [
                            'rgb' => '006100'
                        ]
                    ],
                    'fill' => [
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'C6EfCE')
                    ],
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                    
                ) );
            }else if($worklogDatum['status'] == 'Holiday'){
                $objPHPExcel->getActiveSheet ()->setCellValue ('B'.$row, 'Holiday');
                $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':H'.$row);
                $objPHPExcel->getActiveSheet ()->getStyle ('A'.$row.':H'.$row)->applyFromArray ( array (
                    'font' => [
                        'color' => [
                            'rgb' => '376091'
                        ]
                    ],
                    'fill' => [
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'B6DDE8')
                    ],
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                    
                ) );
            }else if($worklogDatum['status'] == 'Leave/Absent'){
                $objPHPExcel->getActiveSheet ()->setCellValue ( 'B'.$row, 'Leave/Absent');
                $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':H'.$row);
                $objPHPExcel->getActiveSheet ()->getStyle ('A'.$row.':H'.$row)->applyFromArray ( array (
                    'font' => [
                        'color' => [
                            'rgb' => '376091'
                        ]
                    ],
                    'fill' => [
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'B6DDE8')
                    ],
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                    
                ) );
            }else if($worklogDatum['status'] == 'Missed Entry'){
                $objPHPExcel->getActiveSheet ()->setCellValue ( 'B'.$row, 'No Entry');
                $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':H'.$row);
                $objPHPExcel->getActiveSheet ()->getStyle ('A'.$row.':H'.$row)->applyFromArray ( array (
                    'font' => [
                        'color' => [
                            'rgb' => '9C0006'
                        ]
                    ],
                    'fill' => [
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'FFC7CE')
                    ],
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                    
                ) );
            }
            if($key == 0 ){
                $plannedHours += $worklogDatum['work_hours'];
            }elseif($worklogDatum['work_date'] == $worklogData[$key - 1]['work_date']){
                $plannedHours += $worklogDatum['work_hours'];
                $rowend ++; 
            }else{
               if(isset($worklogData[$key - 1]['work_hours'])){      
                   $this->highlightFilledEntries($plannedHours, $objPHPExcel, $rowstart, $rowend);
                    
               }
               $rowstart = $row;
               $rowend = $row;
               $plannedHours = $worklogDatum['work_hours'];
               
            }
            if($key == (count($worklogData) -1) && isset($worklogData[$key]['work_hours'])){
                $this->highlightFilledEntries($plannedHours, $objPHPExcel, $rowstart, $rowend);
            }
            foreach ( $columnNames as $columnName ) {
                $cellName = $columns [$colIndex] . $row;
                
                if (strcasecmp ( 'date', $columnName ) == 0) {
                    $objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $worklogDatum['work_date'] );
                    
                } elseif (strcasecmp ( 'Project', $columnName ) == 0) {
                    if (isset ( $worklogDatum['project_name'] )) {
                        $objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $worklogData[$key] ['project_name'] );
                    }
                } elseif (strcasecmp ( 'Task', $columnName ) == 0) {
                    if (isset ( $worklogDatum['task'] )) {
                        $objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $worklogData[$key] ['task'] );
                    }
                } elseif (strcasecmp ( 'JIRA ID', $columnName ) == 0) {
                    if (isset ( $worklogDatum['jira_id'] )) {
                        $objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $worklogDatum['jira_id'] );
                    }
                } elseif (strcasecmp ( 'External ID', $columnName ) == 0) {
                    if (isset ( $worklogDatum['external_id'] )) {
                        $objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $worklogData[$key]['external_id'] );
                    }
                } elseif (strcasecmp ( 'Summary', $columnName ) == 0) {
                    if (isset ( $worklogDatum['summary'] )) {
                        $objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $worklogDatum['summary'] );                        
                        $objPHPExcel->getSheetByName ( $sheetName )->getStyle($cellName)->getAlignment()->setWrapText(true);
                    }
                }elseif (strcasecmp ( 'Pln hours', $columnName ) == 0) {
                    if (isset ( $worklogDatum['work_hours'] )) {
                        $objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $worklogDatum['work_hours'] );
                    }
                }elseif (strcasecmp ( 'Activity Code', $columnName ) == 0) {
                    if (isset ( $worklogDatum['activity_code'] )) {
                        $objPHPExcel->getSheetByName ( $sheetName )->setCellValue ( $cellName, $worklogDatum['activity_code'] );
                    }
                }
                $objPHPExcel->getActiveSheet ()->getStyle ( $cellName )->applyFromArray ( array (
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                    
                ) );
                
                $colIndex ++;
            }       
            
            $row ++;
        }
        $objPHPExcel->getSheetByName ( $sheetName )->getColumnDimension('F')->setAutoSize(false);
        $objPHPExcel->getSheetByName ( $sheetName )->getColumnDimension('F')->setWidth(90);
        
        
    }
    
    public function highlightFilledEntries($plannedHours,&$objPHPExcel,$rowstart,$rowend){        
        $time_configurationModel = new Timemanagement_Model_Configuration();
        $timeconfiguration = $time_configurationModel->getActiveRecord();
        if($plannedHours < $timeconfiguration[0]['workhours']){
            $objPHPExcel->getActiveSheet ()->getStyle ( 'A'.$rowstart.':A'.$rowend)->applyFromArray ( array (
                'font' => [
                    'color' => [
                        'rgb' => '9C6500'
                    ]
                ],
                'fill' => [
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'FFEB9C')
                ],
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'alignment' => array (
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
                
            ) );
        }else{
            $objPHPExcel->getActiveSheet ()->getStyle ( 'A'.$rowstart.':A'.$rowend)->applyFromArray ( array (
                'font' => [
                    'color' => [
                        'rgb' => '006100'
                    ]
                ],
                'fill' => [
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'C6EfCE')
                ],
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'alignment' => array (
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
                
            ) );
        }          
        $objPHPExcel->getActiveSheet()->mergeCells('A'.$rowstart.':A'.$rowend);
    }
    
    public function preparereportAction() {
        $this->_helper->viewRenderer->setNoRender ();
        $this->_helper->layout->disableLayout ();
        $auth = Zend_Auth::getInstance ();
        $fromDate = '';
        $toDate = '';        
        $worklogModel = new Api_Model_Worklogs();
        if ($auth->hasIdentity ()) {
            $loginUserId = $auth->getStorage ()->read ()->id;
            $loginUserRole = $auth->getStorage()->read()->emprole;
        }
        $duration = $this->_request->getParams () ['duration'];
        if($duration == 4){
            $fromDate = $this->_request->getParams () ['fromdate'];
            $toDate = $this->_request->getParams () ['todate'];
        }else{
            $worklogModel->getdurationDates($duration, $fromDate, $toDate);
        }
        
        $employeeModel = new Default_Model_Employee ();
        $employeesModel = new Default_Model_Employees ();
        $employeeholidaysModel = new Default_Model_Holidaydates ();
        $employeeleaverequestModel = new Default_Model_Leaverequest ();
        $employeeleaveTypeModel = new Default_Model_Employeeleavetypes ();
        $time_configurationModel = new Timemanagement_Model_Configuration();
        $empworkActivityModel = new Default_Model_EmployeeWorkActivity();
        $empAttendanceModel = new Default_Model_Myattendance();
        $worklogReport = array ();
        $dailySheetData = array();
        if($employeeModel->isHrEmployee($loginUserId) || $loginUserRole == SUPERADMINROLE){
            $myteam = $employeeModel->getEmployees('', $loginUserId);
        }else{
            $myteam = $employeeModel->getEmployeesUnderRM($loginUserId);
        }     
        $timeconfiguration = $time_configurationModel->getActiveRecord();
        $totalDays = ((new DateTime ( $fromDate ))->diff ( (new DateTime ( $toDate )), true )->days) + 1;        
        $tm_activities = $worklogModel->getActivities();

        foreach ($myteam as $member){
            $id = $member['user_id'];
            $name = $member['userfullname'];
            $nocHours = 0;    
            $startDate = new DateTime($fromDate);
            $endDate = new DateTime($toDate);
            $holidayGroup = $employeesModel->getHolidayGroupForEmployee ( $id );
            $holidays = $employeeholidaysModel->getHolidaysBetween ( $fromDate, $toDate, $holidayGroup ['0'] ['holiday_group'] ) [0] ['holidaydates'];            
            $leaves = $employeeleaverequestModel->getApprovedLeavesBetween ( $id, $fromDate, $toDate );
            $wfhId = $employeeleaveTypeModel->getEmployeeLeaveTypeByCode ( 'WFH' ) [0] ['leavetype_id'];
            $loggedDays = $worklogModel->getWorkloggedWithinByUserId($id, $fromDate, $toDate);
            $incompleteEntries = $worklogModel->getIncompleteEntriesCount($loggedDays, $timeconfiguration[0]['workhours'],$nocHours);
            $missedEntries = 0;
            foreach ( $leaves as $leave ) {
                if ($leave ['leavetypeid'] != $wfhId) {
                    $leavecount += $leave ['leavecount'];
                }
            };
            while ($startDate <= $endDate){
                $dailyData = $worklogModel->getWorklogDataOn($id, $startDate);
                if(count($dailyData) == 0){                    
                    if(count($empworkActivityModel->getActivitiesOn($id, $startDate)) > 0 ){
                        $missedEntries ++;
                    }
                }
                $startDate->modify('+1 day');
            }
            $worklogReport[$id] = array($name,
                $missedEntries,
                $incompleteEntries
                );
            foreach ($tm_activities as $activity){
                $hours = 0;
                if($activity['activity_code'] == 'NOC'){
                    $hours = $nocHours + $worklogModel->getWorkHoursFromActivities($id, $activity['activity_code'], $fromDate, $toDate);
                    array_splice($worklogReport[$id], 3,0,$hours);
                }else{
                    $hours = $worklogModel->getWorkHoursFromActivities($id, $activity['activity_code'], $fromDate, $toDate);       
                    array_push($worklogReport[$id], $hours);
                }
            }     

            
            $worklogModel = new Api_Model_Worklogs();
            $worklogData = $worklogModel->getWorklogDataForSheet( $id, new DateTime($fromDate), new DateTime($toDate) );            
            $dailySheetData[$id] = $worklogData;
            
        }
        
        
        $_SESSION ['worklogReport'] = $worklogReport;
        $_SESSION ['dailySheetData'] = $dailySheetData;
        $messages ['result'] = 'saved';
        $this->_helper->json ( $messages );
    }

    
}