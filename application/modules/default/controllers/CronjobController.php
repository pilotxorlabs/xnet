<?php
class Default_CronjobController extends Zend_Controller_Action {
	private $options;
	public function preDispatch() {
	}
	public function init() {
		$this->_options = $this->getInvokeArg ( 'bootstrap' )->getOptions ();
	}
	public function indexAction() {
		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout ()->disableLayout ();

		$date = new Zend_Date ();

		$email_model = new Default_Model_EmailLogs ();
		$cron_model = new Default_Model_Cronstatus ();
		// appraisal notifications
		$this->checkperformanceduedate ();
		// feed forward notifications
		// $this->checkffduedate();

		$cron_status = $cron_model->getActiveCron ( 'General' );
		if ($cron_status == 'yes') {
			try {
				// updating cron status table to in-progress
				$cron_data = array (
						'cron_status' => 1,
						'cron_type' => 'General',
						'started_at' => gmdate ( "Y-m-d H:i:s" )
				);

				$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, '' );

				if ($cron_id != '') {
					$mail_data = $email_model->getNotSentMails ();
					if (count ( $mail_data ) > 0) {
						foreach ( $mail_data as $mdata ) {
							$options = array ();
							$options ['header'] = $mdata ['header'];
							$options ['message'] = $mdata ['message'];
							$options ['subject'] = $mdata ['emailsubject'];
							$options ['toEmail'] = $mdata ['toEmail'];
							$options ['toName'] = $mdata ['toName'];
							if ($mdata ['cc'] != '')
								$options ['cc'] = $mdata ['cc'];
							if ($mdata ['bcc'] != '')
								$options ['bcc'] = $mdata ['bcc'];
								// to send email

							$mail_status = sapp_Mail::_email ( $options );

							$mail_where = array (
									'id=?' => $mdata ['id']
							);
							$new_maildata ['modifieddate'] = gmdate ( "Y-m-d H:i:s" );

							if ($mail_status === true) {
								$new_maildata ['is_sent'] = 1;
								// to udpate email log table that mail is sent.
								$id = $email_model->SaveorUpdateEmailData ( $new_maildata, $mail_where );
							}
						} // end of for loop
					} // end of mails count if
					  // updating cron status table to completed.
					$cron_data = array (
							'cron_status' => 0,
							'completed_at' => gmdate ( "Y-m-d H:i:s" )
					);
					$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, "id = " . $cron_id );
				} // end of cron status id if
			} catch ( Exception $e ) {
			}
		} // end of cron status if
	} // end of index action

	/**
	 * This action is used to send mails to employees for passport expiry,and credit card expiry(personal details screen)
	 */
	public function empdocsexpiryAction() {
		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout ()->disableLayout ();

		$email_model = new Default_Model_EmailLogs ();
		$cron_model = new Default_Model_Cronstatus ();

		$cron_status = $cron_model->getActiveCron ( 'Emp docs expiry' );

		if ($cron_status == 'yes') {
			try {
				// updating cron status table to in-progress
				$cron_data = array (
						'cron_status' => 1,
						'cron_type' => 'Emp docs expiry',
						'started_at' => gmdate ( "Y-m-d H:i:s" )
				);

				$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, '' );

				if ($cron_id != '') {
					$calc_date = new DateTime ( date ( 'Y-m-d' ) );
					$calc_date->add ( new DateInterval ( 'P1M' ) );
					$print_date = $calc_date->format ( DATEFORMAT_PHP );
					$calc_date = $calc_date->format ( 'Y-m-d' );
					$mail_data = $email_model->getEmpDocExpiryData ( $calc_date );
					if (count ( $mail_data ) > 0) {
						foreach ( $mail_data as $mdata ) {
							$view = $this->getHelper ( 'ViewRenderer' )->view;
							$this->view->emp_name = $mdata ['name'];
							$this->view->docs_arr = $mdata ['docs'];
							$this->view->expiry_date = $print_date;
							$text = $view->render ( 'mailtemplates/empdocsexpirycron.phtml' );
							$options ['subject'] = APPLICATION_NAME . ': Documents expiry';
							$options ['header'] = 'Greetings from ' . APPLICATION_NAME;
							$options ['toEmail'] = $mdata ['email'];
							$options ['toName'] = $mdata ['name'];
							$options ['message'] = $text;

							sapp_Global::_sendEmail ( $options );
						}
					}
					$cron_data = array (
							'cron_status' => 0,
							'completed_at' => gmdate ( "Y-m-d H:i:s" )
					);
					$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, "id = " . $cron_id );
				} // end of cron status id if
			} catch ( Exception $e ) {
			}
		} // end of cron status if
	} // end of emp expiry action

	/**
	 * This action is used to send mails to employees for passport expiry,and credit card expiry(visa and immigration screen)
	 */
	public function empexpiryAction() {
		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout ()->disableLayout ();

		$email_model = new Default_Model_EmailLogs ();
		$cron_model = new Default_Model_Cronstatus ();

		$cron_status = $cron_model->getActiveCron ( 'Employee expiry' );

		$earr = array (
				'i94' => 'I94',
				'visa' => 'Visa',
				'passport' => 'Passport'
		);
		if ($cron_status == 'yes') {
			try {
				// updating cron status table to in-progress
				$cron_data = array (
						'cron_status' => 1,
						'cron_type' => 'Employee expiry',
						'started_at' => gmdate ( "Y-m-d H:i:s" )
				);

				$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, '' );

				if ($cron_id != '') {
					$calc_date = new DateTime ( date ( 'Y-m-d' ) );
					$calc_date->add ( new DateInterval ( 'P1M' ) );
					$print_date = $calc_date->format ( DATEFORMAT_PHP );
					$calc_date = $calc_date->format ( 'Y-m-d' );
					$mail_data = $email_model->getEmpExpiryData ( $calc_date );
					if (count ( $mail_data ) > 0) {
						foreach ( $mail_data as $mdata ) {
							$base_url = 'http://' . $this->getRequest ()->getHttpHost () . $this->getRequest ()->getBaseUrl ();
							$view = $this->getHelper ( 'ViewRenderer' )->view;
							$this->view->emp_name = $mdata ['userfullname'];
							$this->view->etype = $earr [$mdata ['etype']];
							$this->view->expiry_date = $print_date;
							$text = $view->render ( 'mailtemplates/empexpirycron.phtml' );
							$options ['subject'] = APPLICATION_NAME . ': ' . $earr [$mdata ['etype']] . ' renewal';
							$options ['header'] = 'Greetings from ' . APPLICATION_NAME;
							$options ['toEmail'] = $mdata ['emailaddress'];
							$options ['toName'] = $mdata ['userfullname'];
							$options ['message'] = $text;
							$options ['cron'] = 'yes';

							sapp_Global::_sendEmail ( $options );
						}
					}
					$cron_data = array (
							'cron_status' => 0,
							'completed_at' => gmdate ( "Y-m-d H:i:s" )
					);
					$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, "id = " . $cron_id );
				} // end of cron status id if
			} catch ( Exception $e ) {
			}
		} // end of cron status if
	} // end of emp expiry action

	/**
	 * This action is to remind managers to approve leaves of his team members before end of month.
	 */
	public function leaveapproveAction() {
		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout ()->disableLayout ();

		$email_model = new Default_Model_EmailLogs ();
		$cron_model = new Default_Model_Cronstatus ();

		$cron_status = $cron_model->getActiveCron ( 'Approve leave' );

		if ($cron_status == 'yes') {
			try {
				// updating cron status table to in-progress
				$cron_data = array (
						'cron_status' => 1,
						'cron_type' => 'Approve leave',
						'started_at' => gmdate ( "Y-m-d H:i:s" )
				);

				$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, '' );

				if ($cron_id != '') {
					$from_date = date ( 'Y-m-01' );
					$to_date = date ( 'Y-m-d' );
					$mail_data = $email_model->getLeaveApproveData ( $from_date, $to_date );
					if (count ( $mail_data ) > 0) {
						foreach ( $mail_data as $mdata ) {
							$base_url = 'http://' . $this->getRequest ()->getHttpHost () . $this->getRequest ()->getBaseUrl ();
							$view = $this->getHelper ( 'ViewRenderer' )->view;
							$this->view->emp_name = $mdata ['mng_name'];
							$this->view->team = $mdata ['team'];
							$text = $view->render ( 'mailtemplates/leaveapprovecron.phtml' );
							$options ['subject'] = APPLICATION_NAME . ': Leave(s) pending for approval';
							$options ['header'] = 'Pending Leaves';
							$options ['toEmail'] = $mdata ['mng_email'];
							$options ['toName'] = $mdata ['mng_name'];
							$options ['message'] = $text;
							$options ['cron'] = 'yes';

							sapp_Global::_sendEmail ( $options );
						}
					}
					$cron_data = array (
							'cron_status' => 0,
							'completed_at' => gmdate ( "Y-m-d H:i:s" )
					);
					$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, "id = " . $cron_id );
				} // end of cron status id if
			} catch ( Exception $e ) {
			}
		} // end of cron status if
	} // end of leave approve action
	/**
	 * This action is to send email to HR group when due date of requisition is completed.
	 */
	public function requisitionAction() {
		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout ()->disableLayout ();

		$email_model = new Default_Model_EmailLogs ();
		$cron_model = new Default_Model_Cronstatus ();

		$cron_status = $cron_model->getActiveCron ( 'Requisition expiry' );

		if ($cron_status == 'yes') {
			try {
				// updating cron status table to in-progress
				$cron_data = array (
						'cron_status' => 1,
						'cron_type' => 'Requisition expiry',
						'started_at' => gmdate ( "Y-m-d H:i:s" )
				);

				$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, '' );

				if ($cron_id != '') {
					$calc_date = new DateTime ( date ( 'Y-m-d' ) );
					$calc_date->add ( new DateInterval ( 'P15D' ) );
					$print_date = $calc_date->format ( DATEFORMAT_PHP );
					$calc_date = $calc_date->format ( 'Y-m-d' );
					$mail_data = $email_model->getRequisitionData ( $calc_date );
					if (count ( $mail_data ) > 0) {
						foreach ( $mail_data as $did => $mdata ) {
							if (defined ( "REQ_HR_" . $did )) {
								$base_url = 'http://' . $this->getRequest ()->getHttpHost () . $this->getRequest ()->getBaseUrl ();
								$view = $this->getHelper ( 'ViewRenderer' )->view;
								$this->view->emp_name = "HR";
								$this->view->print_date = $print_date;
								$this->view->req = $mdata ['req'];
								$this->view->base_url = $base_url;
								$text = $view->render ( 'mailtemplates/requisitioncron.phtml' );
								$options ['subject'] = APPLICATION_NAME . ': Renew requisition expiry';
								$options ['header'] = 'Requisition Expiry';
								$options ['toEmail'] = constant ( "REQ_HR_" . $did );
								$options ['toName'] = "HR";
								$options ['message'] = $text;
								$options ['cron'] = 'yes';

								sapp_Global::_sendEmail ( $options );
							}
						}
					}
					$cron_data = array (
							'cron_status' => 0,
							'completed_at' => gmdate ( "Y-m-d H:i:s" )
					);
					$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, "id = " . $cron_id );
				} // end of cron status id if
			} catch ( Exception $e ) {
			}
		} // end of cron status if
	} // end of requisition action.

	/**
	 * This action is used to send notification to inactive users.
	 */
	public function inactiveusersAction() {
		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout ()->disableLayout ();

		$email_model = new Default_Model_EmailLogs ();
		$cron_model = new Default_Model_Cronstatus ();

		$cron_status = $cron_model->getActiveCron ( 'Inactive users' );

		if ($cron_status == 'yes') {
			try {
				// updating cron status table to in-progress
				$cron_data = array (
						'cron_status' => 1,
						'cron_type' => 'Inactive users',
						'started_at' => gmdate ( "Y-m-d H:i:s" )
				);

				$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, '' );

				if ($cron_id != '') {
					$calc_date = new DateTime ( date ( 'Y-m-d' ) );
					$calc_date->sub ( new DateInterval ( 'P3M' ) );
					$print_date = $calc_date->format ( DATEFORMAT_PHP );
					$calc_date = $calc_date->format ( 'Y-m-d' );
					$mail_data = $email_model->getInactiveusersData ( $calc_date );
					if (count ( $mail_data ) > 0) {
						foreach ( $mail_data as $did => $mdata ) {
							$base_url = 'http://' . $this->getRequest ()->getHttpHost () . $this->getRequest ()->getBaseUrl ();
							$view = $this->getHelper ( 'ViewRenderer' )->view;
							$this->view->emp_name = $mdata ['userfullname'];
							$this->view->print_date = $print_date;
							$this->view->user_id = $mdata ['employeeId'];
							$this->view->base_url = $base_url;
							$text = $view->render ( 'mailtemplates/inactiveusercron.phtml' );
							$options ['subject'] = APPLICATION_NAME . ': XNet account inactivated';
							$options ['header'] = 'Employee inactivated';
							$options ['toEmail'] = $mdata ['emailaddress'];
							$options ['toName'] = $mdata ['userfullname'];
							$options ['message'] = $text;
							$options ['cron'] = 'yes';

							sapp_Global::_sendEmail ( $options );
						}
					}
					$cron_data = array (
							'cron_status' => 0,
							'completed_at' => gmdate ( "Y-m-d H:i:s" )
					);
					$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, "id = " . $cron_id );
				} // end of cron status id if
			} catch ( Exception $e ) {
			}
		} // end of cron status if
	} // end of inactiveusers action.

	/**
	 * This action is used to save update json in logmanager(removes 30 days before content and saves in logmanagercron) .
	 */
	public function logcronAction() {
		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout ()->disableLayout ();

		$logmanager_model = new Default_Model_Logmanager ();
		$logmanagercron_model = new Default_Model_Logmanagercron ();
		$logData = $logmanager_model->getLogManagerData ();
		$i = 0;
		if (count ( $logData ) > 0) {
			foreach ( $logData as $record ) {
				if (isset ( $record ['log_details'] ) && ! empty ( $record ['log_details'] )) {
					$id = $record ['id'];
					$menuId = $record ['menuId'];
					$actionflag = $record ['user_action'];
					$userid = $record ['last_modifiedby'];
					$keyflag = $record ['key_flag'];
					$date = $record ['last_modifieddate'];
					$jsondetails = '{"testjson":[' . $record ['log_details'] . ']}';
					$jsonarr = @get_object_vars ( json_decode ( $jsondetails ) );
					$mainTableJson = '';
					$cronTableJson = '';
					if (! empty ( $jsonarr )) {
						$mainJsonArrayCount = count ( $jsonarr ['testjson'] );
						foreach ( $jsonarr ['testjson'] as $key => $json ) {
							$jsonVal = @get_object_vars ( $json );
							if (! empty ( $jsonVal )) {
								$jsondate = explode ( ' ', $jsonVal ['date'] );
								$datetime1 = new DateTime ( $jsondate [0] );
								$datetime2 = new DateTime ();
								$interval = $datetime1->diff ( $datetime2 );
								$interval = $interval->format ( '%a' );
								if ($interval > 30) {
									if ($cronTableJson == '') {
										$cronTableJson .= json_encode ( $jsonVal );
									} else {
										$cronTableJson .= ',' . json_encode ( $jsonVal );
									}
									if (isset ( $jsonVal ['recordid'] ) && $jsonVal ['recordid'] != '') {
										$keyflag = $jsonVal ['recordid'];
									}
								} else {
									if ($mainTableJson == '') {
										$mainTableJson .= json_encode ( $jsonVal );
									} else {
										$mainTableJson .= ',' . json_encode ( $jsonVal );
									}
								}
							}
							if (($mainJsonArrayCount - 1) == $key) { // if all are greater than 30 days
								if ($mainTableJson == '') {
									$mainTableJson .= json_encode ( $jsonVal );
								}
							}
						}
						try {

							if ($cronTableJson != '' && $mainTableJson != '') {
								$result = $logmanager_model->UpdateLogManagerWhileCron ( $id, $mainTableJson );
								if ($result) {
									$InsertId = $logmanagercron_model->InsertLogManagerCron ( $menuId, $actionflag, $cronTableJson, $userid, $keyflag, $date );
								}
								$i ++;
							}
						} catch ( Exception $e ) {
							echo $e->getMessage ();
							exit ();
						}
					}
				}
			}
		}
	}
	public function checkperformanceduedate() {
		$app_init_model = new Default_Model_Appraisalinit ();
		$app_ratings_model = new Default_Model_Appraisalemployeeratings ();
		$active_appraisal_Arr = $app_init_model->getActiveAppraisals ();
		$appraisalPrivMainModel = new Default_Model_Appraisalqsmain ();
		$app_manager_model = new Default_Model_Appraisalmanager ();
		$usersmodel = new Default_Model_Users ();
		$current_day = new DateTime ( 'now' );
		$current_day->sub ( new DateInterval ( 'P1D' ) );
		if (! empty ( $active_appraisal_Arr )) {
			foreach ( $active_appraisal_Arr as $appval ) {

				if ($appval ['managers_due_date'])
					$manager_due_date = new DateTime ( $appval ['managers_due_date'] );
				else
					$manager_due_date = '';
				if ($appval ['employees_due_date'])
					$emp_due_date = new DateTime ( $appval ['employees_due_date'] );
				else
					$emp_due_date = '';

				$due_date = ($appval ['enable_step'] == 2) ? $emp_due_date : $manager_due_date;

				$interval = $current_day->diff ( $due_date );
				$interval->format ( '%d' );
				$interval = $interval->days;

				$appIdArr = array ();
				$appIdList = '';
				if ($interval <= 2) {

					if ($appval ['enable_step'] == 2) {

						$employeeidArr = $app_ratings_model->getEmployeeIds ( $appval ['id'], 'cron' );
						if (! empty ( $employeeidArr )) {
							$empIdArr = array ();
							$empIdList = '';
							foreach ( $employeeidArr as $empval ) {
								array_push ( $empIdArr, $empval ['employee_id'] );
							}
							if (! empty ( $empIdArr )) {
								$empIdList = implode ( ',', $empIdArr );
								$employeeDetailsArr = $app_manager_model->getUserDetailsByEmpID ( $empIdList ); // Fetching employee details

								if (! empty ( $employeeDetailsArr )) {
									$empArr = array ();
									foreach ( $employeeDetailsArr as $emp ) {
										array_push ( $empArr, $emp ['emailaddress'] ); // preparing Bcc array
									}

									$optionArr = array (
											'subject' => 'Self Appraisal Submission Pending',
											'header' => 'Performance Appraisal',
											'toemail' => SUPERADMIN_EMAIL,
											'toname' => 'Super Admin',
											'bcc' => $empArr,
											'message' => "<div style='padding: 0; text-align: left; font-size:14px; font-family:Arial, Helvetica, sans-serif;'>
														<span style='color:#3b3b3b;'>Hi, </span><br />
														<div style='padding:20px 0 0 0;color:#3b3b3b;'>Self appraisal submission is pending.</div>
														<div style='padding:20px 0 10px 0;'>Please <a href=" . BASE_URL . " target='_blank' style='color:#b3512f;'>click here</a> to login  to your <b>" . APPLICATION_NAME . "</b> account to check the details.</div>
														</div> ",
											'cron' => 'yes'
									);
									sapp_PerformanceHelper::saveCronMail ( $optionArr );
								}
							}
						}
					} else {

						$getLine1ManagerId = $appraisalPrivMainModel->getLine1ManagerIdMain ( $appval ['id'] );
						if (! empty ( $getLine1ManagerId )) {
							$empArr = array ();
							foreach ( $getLine1ManagerId as $val ) {
								array_push ( $empArr, $val ['emailaddress'] ); // preparing Bcc array
							}
							$optionArr = array (
									'subject' => 'Manager Appraisal Submission Pending',
									'header' => 'Performance Appraisal',
									'toemail' => SUPERADMIN_EMAIL,
									'toname' => 'Super Admin',
									'bcc' => $empArr,
									'message' => "<div style='padding: 0; text-align: left; font-size:14px; font-family:Arial, Helvetica, sans-serif;'>
														<span style='color:#3b3b3b;'>Hi, </span><br />
														<div style='padding:20px 0 0 0;color:#3b3b3b;'>Manager appraisal submission is pending.</div>
														<div style='padding:20px 0 10px 0;'>Please <a href=" . BASE_URL . " target='_blank' style='color:#b3512f;'>click here</a> to login  to your <b>" . APPLICATION_NAME . "</b> account to check the details.</div>
														</div> ",
									'cron' => 'yes'
							);
							sapp_PerformanceHelper::saveCronMail ( $optionArr );
						}
					}
				}
			}
		}
	}
	public function checkffduedate() {
		$ffinitModel = new Default_Model_Feedforwardinit ();
		$ffEmpRatModel = new Default_Model_Feedforwardemployeeratings ();

		$ffDataArr = $ffinitModel->getFFbyBUDept ( '', 'yes' );
		$ffIdArr = array ();
		$ffIdList = '';
		$current_day = new DateTime ( 'now' );
		$current_day->sub ( new DateInterval ( 'P1D' ) );

		if (! empty ( $ffDataArr )) {
			foreach ( $ffDataArr as $ffval ) {
				if ($ffval ['status'] == 1) {
					if ($ffval ['ff_due_date'])
						$due_date = new DateTime ( $ffval ['ff_due_date'] );
					else
						$due_date = '';
					$interval = $current_day->diff ( $due_date );
					$interval->format ( '%d' );
					$interval = $interval->days;
					if ($interval <= 2) {
						array_push ( $ffIdArr, $ffval ['id'] );
					}
				}
			}
		}
		if (! empty ( $ffIdArr )) {
			$ffIdList = implode ( ',', $ffIdArr );
		}
		if ($ffIdList != '') {
			$ffEmpsStatusData = $ffEmpRatModel->getEmpsFFStatus ( $ffIdList, 'cron' );
			if (! empty ( $ffEmpsStatusData )) {
				$empIdArr = array ();
				foreach ( $ffEmpsStatusData as $empval ) {
					array_push ( $empIdArr, $empval ['emailaddress'] );
				}
				$optionArr = array (
						'subject' => 'Manager Feedforward submission pending',
						'header' => 'Feedforward',
						'toemail' => SUPERADMIN_EMAIL,
						'toname' => 'Super Admin',
						'bcc' => $empIdArr,
						'message' => "<div style='padding: 0; text-align: left; font-size:14px; font-family:Arial, Helvetica, sans-serif;'>
														<span style='color:#3b3b3b;'>Hi, </span><br />
														<div style='padding:20px 0 0 0;color:#3b3b3b;'>Mangaer feedforward is pending.</div>
														<div style='padding:20px 0 10px 0;'>Please <a href=" . BASE_URL . " target='_blank' style='color:#b3512f;'>click here</a> to login  to your <b>" . APPLICATION_NAME . "</b> account to check the details.</div>
														</div> ",
						'cron' => 'yes'
				);
				sapp_PerformanceHelper::saveCronMail ( $optionArr );
			}
		}
	}

	/**
	 * This action is to expire unapproved exceptions of employees after end of month.
	 */
	public function expireexceptionAction() {
		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout ()->disableLayout ();

		$email_model = new Default_Model_EmailLogs ();
		$exception_request_model = new Default_Model_ExceptionRequest ();
		$cron_model = new Default_Model_Cronstatus ();

		$cron_status = $cron_model->getActiveCron ( 'Exception expiry' );
		if ($cron_status == 'yes') {
			try {
				// updating cron status table to in-progress
				$cron_data = array (
						'cron_status' => 1,
						'cron_type' => 'Exception expiry',
						'started_at' => gmdate ( "Y-m-d H:i:s" )
				);

				$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, '' );

				if ($cron_id != '') {

					$lastmonth = date ( 'd' ) < AUDIT_START_DAY ? date ( 'm' ) - 1 : date ( 'm' );
					$to_date = date ( '2018-' . $lastmonth . '-' . AUDIT_START_DAY );
					$exception_request_model->expirePendingExceptionsData ( $to_date );
					$cron_data = array (
							'cron_status' => 0,
							'completed_at' => gmdate ( "Y-m-d H:i:s" )
					);
					$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, "id = " . $cron_id );
					echo 'Cron executed successfully';
				} // end of cron status id if
			} catch ( Exception $e ) {
				echo 'Cron Failed';
			}
		} else {
			echo 'Some cron running';
		} // end of cron status if
	} // end of exception expire action

	/**
	 * This action is to update employee leaves.
	 */
	public function updateleavesAction() {
		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout ()->disableLayout ();

		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}

		$cron_model = new Default_Model_Cronstatus ();

		$cron_status = $cron_model->getActiveCron ( 'Exception expiry' );
		if ($cron_status == 'yes') {
			try {
				// updating cron status table to in-progress
				$cron_data = array (
						'cron_status' => 1,
						'cron_type' => 'Update Leaves',
						'started_at' => gmdate ( "Y-m-d H:i:s" )
				);

				$cron_id = $cron_model->SaveorUpdateCronStatusData ( $cron_data, '' );

				if ($cron_id != '') {
					$employeeleavesTypes = new Default_Model_Employeeleavetypes ();
					$employeeModel = new Default_Model_Employee ();
					$employeeleavesModel = new Default_Model_Employeeleaves ();
					$leavetypes = $employeeleavesTypes->getactiveleavetype ();
					$employees = $employeeModel->getEmployees ( '', $loginUserId, '', '', null, null, null );
					foreach ( $leavetypes as $leavetype ) {
						foreach ( $employees as $employee ) {
						    if ($employee ['emp_status_id'] == 1 || $employee['emp_status_id'] == 5) {
								$daysSinceJoined = $employeeleavesModel->getDaysBetween ( $employee ['date_of_joining'], date ( 'm/d/y' ) );
								$leavedata = $employeeleavesModel->getsingleEmployeeleaveDataByType ( $employee ['user_id'], $leavetype ['id'] );
								$used = $leavedata [0] ['used_leaves'] == null ? 0 : $leavedata [0] ['used_leaves'];
								if ((new DateTime ( $leavedata [0] ['modifieddate'] ) < $employeeleavesModel->getFirstDayOfQuarter ( ceil ( date ( 'm' ) / 3 ) ) && $leavetype ['allotmentmode'] == 'Quarterly') || (new DateTime ( $leavedata [0] ['modifieddate'] ) < $employeeleavesModel->getFirstDayOfHalfYear ( ceil ( date ( 'm' ) / 6 ) ) && $leavetype ['allotmentmode'] == 'Half-yearly')) {
									$used = 0;
								}

								if($leavetype ['allotmentmode'] == 'Monthly'){
									$leavecount = $employeeleavesModel->countLeaves ( $leavetype, $daysSinceJoined, $employee ['date_of_joining'], $employee ['user_id'],$leavetype ['id'],$used );
								}else{
									$leavecount = $employeeleavesModel->countLeaves ( $leavetype, $daysSinceJoined, $employee ['date_of_joining'], $employee ['user_id'],$leavetype ['id'],$used ) - $used;
								}
								$employeeleavesModel->SaveorUpdateEmployeeLeaveType ( $employee ['user_id'], $leavecount < 0 ? 0 : $leavecount, $used, $loginUserId, $leavetype ['id'] );
							}
						}
					}
					echo 'Cron executed successfully';
				} // end of cron status id if
			} catch ( Exception $e ) {
				echo 'Cron Failed';
			}
		} else {
			echo 'Some cron running';
		} // end of cron status if
	} // end of update leave action

	/**
	 * This action is to send email reminder to Employee when forget to fill the worklog sheet for the day
	 */
	public function worklogsheetreminderAction() {
		$auth = Zend_Auth::getInstance ();
		$loginUserId = $auth->getStorage ()->read ()->id;
		$usersmodel = new Default_Model_Users ();
		$employeesmodel = new Default_Model_Employees ();
		$employeeModel = new Default_Model_Employee ();
		$leaverequestmodel = new Default_Model_Leaverequest ();
		$empTSModel = new Timemanagement_Model_MyTimesheet ();
		$worklogs = new Api_Model_Worklogs();
		$holidayModel = new Default_Model_Holidaydates ();
		$employeeworkactivityModel = new Default_Model_Employeeworkactivity();
		$db = Zend_Db_Table::getDefaultAdapter ();
		$date = new Zend_Date ();
		$currentdate = $date->get ( 'MMM dd YYYY' );
		$datebeforecurrentday = $date->subDay ( 1 )->get ( 'YYYY-MM-dd' );
		$datebeforecurrentdayformail = $date->get ( 'MMM-dd, YYYY' );
		$employees = $employeeModel->getEmployees ( '', $loginUserId, '', '', null, null, null );
		foreach ( $employees as $employee ) {
			$holidayGroup = $employeesmodel->getHolidayGroupForEmployee ( $employee ['user_id'] );
			$wasHoliday = $holidayModel->checkHoliday ( $datebeforecurrentday, $holidayGroup [0] ['holiday_group'] );
			$leavecount = $leaverequestmodel->getUserLeavesDataWithinDate ( $employee ['user_id'], $datebeforecurrentday );
			$worklogcount = $empTSModel->getWorklogForDate ( $employee ['user_id'], $datebeforecurrentday );
			$isEditable = $worklogs->isLocked($datebeforecurrentday, $employee ['user_id']);
			$ispunchpresent = $employeeworkactivityModel->getActivitiesOn($employee ['user_id'], new DateTime($datebeforecurrentday));
			if ($worklogcount == 0 && ! $wasHoliday && $leavecount == 0 && $isEditable && (!empty($ispunchpresent))) {
				$loggedInEmployeeRepMangerID = $employeesmodel->getRepMangerID ( $employee ['user_id'] );
				if ($loggedInEmployeeRepMangerID != '' && $loggedInEmployeeRepMangerID != NULL) {
					$loggedinEmpreportingmanagerId = $usersmodel->getUserDetailsByID ( $loggedInEmployeeRepMangerID [0] ['repmangerid'] );
				}
				$reportingmanageremail = $loggedinEmpreportingmanagerId [0] ['emailaddress'];
				if ($reportingmanageremail != null) {
					$options ['subject'] = 'Please update Work-log sheet';
					$options ['header'] = 'Worklog Reminder' ;
					$options ['toEmail'] = $employee ['emailaddress'];
					$options ['cc'] = array($reportingmanageremail, PMG_MAIL);
					$options ['toName'] = $employee ['userfullname'];
					$options ['message'] = '<div>
	        		<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi '.$employee ['userfullname'].'</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">This mail is regarding worklog sheet update.</div>
</div>
<div style = "padding : 8px 30px 20px 30px;font-family:Verdana;font-size:14px;">
<div>We are writing to inform you, work-log sheet was not updated for ' . $datebeforecurrentdayformail . '</div>

				<div>You are requested to update it at the earliest. </div>
				<div> Note: Each day miss will attract -10% in MIS scoring for non-updating of work-log sheet.</div> 
				</div>
				</div>';
				sapp_Global::_sendEmail ( $options );
				}
			}
		}
		echo 'Cron executed successfully';
	}

	/**
	 * This action is to send an email as a reminder to the RM on the selected date
	 */
	public function approvedleavesreminderAction(){

		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout ()->disableLayout ();
		$auth = Zend_Auth::getInstance ();
		$leaverequest = new Default_Model_Leaverequest();
		$usersmodel = new Default_Model_Users ();
		$employeeModel = new Default_Model_Employee ();
		$date = new Zend_Date ();
		$currentdate = $date->get ( 'YYYY-MM-dd' );
		$notificationDetails = $leaverequest->getNotificationDetails();
		foreach ($notificationDetails as $notificationDetail){
			$notificationType = $leaverequest->getNotificationTypeById($notificationDetail['notification_type_id']);
			if($notificationType[0]['notification_type'] == "Approved Leaves Reminder"){
				if($notificationDetail['notification_date'] != '' && $notificationDetail['notification_date'] != NULL && $notificationDetail['notification_date'] == $currentdate){
					$notificationRequestDetail = $leaverequest->getNotificationRequestDetails($notificationDetail['notification_request_id']);
					if(!empty($notificationRequestDetail)){
						$reportingmanagerDetails = $usersmodel->getUserDetailsByID ( $notificationRequestDetail[0]['rep_mang_id'] );
						$reportingmanageremail = $reportingmanagerDetails[0]['emailaddress'];
						$employeeDetails = $employeeModel->getEmp_from_summary($notificationRequestDetail[0]['user_id']);
						$employeeName = $employeeDetails['userfullname'];
						$leaveType = $leaverequest->getLeaveTypeByTypeId($notificationRequestDetail[0]['leavetypeid']);
						if($notificationRequestDetail[0]['modifiedby'] != 1){
							$approverDetails = $employeeModel->getEmp_from_summary($notificationRequestDetail[0]['modifiedby']);
							$approverName = $approverDetails['userfullname'];
						}
						else{
							$approverName = 'Super Admin';
						}

						if ($reportingmanageremail != null){
							$options ['subject'] = 'Leave reminder for '. $employeeName .': '.$notificationRequestDetail[0]['from_date']. ' to '. $notificationRequestDetail[0]['to_date'];
							$options ['toEmail'] = $reportingmanageremail;
							$options ['message'] = '<div>
				<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
    				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
						<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
						<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">'.$employeeName.' has the following approved leave(s) upcoming.</div>
                    </div>
                <div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					  <tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Leave type</td>
                         <td width="72%" style="color:#ffffff">'.$leaveType[0]['leavetype'].'</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Count of leave(s)</td>
                        <td style="color:#5b5b5b">'.$notificationRequestDetail[0]['appliedleavescount'].'</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From date</td>
                        <td style="color:#5b5b5b">'.$notificationRequestDetail[0]['from_date'].'</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To date</td>
                        <td style="color:#5b5b5b">'.$notificationRequestDetail[0]['to_date'].'</td>
            	     </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason</td>
                        <td style="color:#5b5b5b">'.$notificationRequestDetail[0]['reason'].'</td>
                  </tr>
                   </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Approved on</td>
                        <td style="color:#5b5b5b">'.$notificationRequestDetail[0]['modifieddate'].'</td>
                  </tr>

                   </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Approved by</td>
                        <td style="color:#5b5b5b">'.$approverName.'</td>
                  </tr>

                </tbody></table>

            </div>';
							sapp_Global::_sendEmail ( $options );
						}
						else{
							continue;
						}
					}
					else{
						continue;
					}
				}
				else{
					echo 'Cron cannot be executed.';
					return;
				}
			}
		}
		echo 'Cron executed successfully';
	}

	public function nocAction(){

		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout ()->disableLayout ();
		$auth = Zend_Auth::getInstance ();
		$employeeModel = new Default_Model_Employee ();
		$worlogsModel = new Api_Model_Worklogs();
		$employessUserIds = $employeeModel->getUserId();
		$date = new Zend_Date ();
		$currentdate = $date->get ( 'YYYY-MM-dd' );
		$work_date = new DateTime($currentdate);
		$workDate = $work_date->format('Y-m-d');
		$projectID = $worlogsModel->getNonActiveProjectIdByProjectName('NOC');
		$activityId = $worlogsModel->getActivityId('NOC');

 		foreach($employessUserIds as $employessUserId){
			$result = $worlogsModel->isWorklogAddedToday($employessUserId['user_id']);
			if(count($result)==0){
				/*Start Save data in tm_worklogs*/
				$data = array(
						'user_id' => $employessUserId['user_id'],
						'work_id_1' => ' ',
						'work_id_2' => ' ',
						'summary'=> 'NOC',
						'work_hours'=>' ',
						'work_date'=>$workDate,
						'activity_id'=> $activityId,
	 					'project_id'=> $projectID,
						'task_id'=>' ',
						'created' =>gmdate("Y-m-d H:i:s"),
						'modified'=> gmdate("Y-m-d H:i:s"),
						'is_enabled'=>2,
						'is_active'=>2
				);
				$where = '';
				$worlogsModel->saveWorkLogs($data, $where);
			}
		}

		echo 'Cron executed successfully';
	}
	
	/**
	 * Cron job to mark today ending probation complete and employement status Full Time
	 */
	public function allprobationcompleteAction() {
		
		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout ()->disableLayout ();
	
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
	
		$cron_model = new Default_Model_Cronstatus ();
	
			try {
				// updating cron status table to in-progress
				$cron_data = array (
						'cron_status' => 1,
						'cron_type' => 'Probation Complete',
						'started_at' => gmdate ( "Y-m-d H:i:s" )
				);
	
					$employeeModel = new Default_Model_Employee ();
					$probationdetailsModel = new Default_Model_Probationdetails();
					$employees = $employeeModel->getUsers ();
					$currentdate = date('Y-m-d');
						foreach ( $employees as $employee ) {
							$probationRule = $probationdetailsModel->getProbationRule($employee['user_id'],$currentdate);
							if ($probationRule != null)
							{
								//Update Probation Status
								$probationdetailsModel->updateProbationStatus($employee['user_id'],$probationRule['0']['id']);
								//Verify if there is any more Probation Durations
								if (($probationdetailsModel->verifyOtherProbationExists($employee['user_id'],$currentdate)) == null)
								{
									$emp_status = 'FT';
									$workCodeName = $employeeModel->getWorkCodeName($emp_status);
									$employeeModel->updateStatusEmployee($employee['user_id'],$workCodeName);
									$workCode = 'Full Time';
									$employeeModel->updateStatusEmployeeSummary($employee['user_id'],$workCodeName,$workCode);
								}
							}
						}
					echo 'Cron executed successfully';
			} catch ( Exception $e ) {
				echo 'Cron Failed';
			}
	} 
	
	public function notifyprobationendAction() {
	    
	    $this->_helper->viewRenderer->setNoRender ( true );
	    $this->_helper->layout ()->disableLayout ();
	    $cron_model = new Default_Model_Cronstatus ();
	    
	    try {
	        // updating cron status table to in-progress
	        $cron_data = array (
	            'cron_status' => 1,
	            'cron_type' => 'Probation Complete',
	            'started_at' => gmdate ( "Y-m-d H:i:s" )
	        );
	        
	        $employeeModel = new Default_Model_Employee ();
	        $probationdetailsModel = new Default_Model_Probationdetails();
	        $currentdate = new DateTime();
	        $currentdate->setTime(0,0,0);        
	        $probationDetails = $probationdetailsModel->getInProgressProbation();
	        $usersmodel = new Default_Model_Users ();
	        $employeesmodel = new Default_Model_Employees ();
	        foreach ( $probationDetails as $probationDetail) {	            
	            $sendMail = false;	
	            $endDate = new DateTime($probationDetail['end_date']);
	           
	            if($probationDetail['notify_on'] != null ){
                    if(new DateTime($probationDetail['notify_on']) == $currentdate ){
                        $sendMail = true;
                    }
                }
                if($currentdate == new DateTime($probationDetail['end_date'])){
                    $sendMail = true;
                }
                if($currentdate == (new DateTime($probationDetail['end_date']))->modify('-1 day')){
                    $sendMail = true;
                }
                if($currentdate == (new DateTime($probationDetail['end_date']))->modify('-7 days')){
                    $sendMail = true;
                }
	          
	            if($sendMail){
	                $employee = $employeeModel->getEmployeeDataForProbationMail($probationDetail['user_id']);
	                $options ['subject'] = 'Probation of '. $employee['userfullname'].' is completing on '. $probationDetail['end_date'];
	                $options ['toEmail'] = '' ;
	                if(defined ( 'LV_HR_' . $employee['businessunit_id'])){
	                    $options ['toEmail'] =  constant('LV_HR_' . $employee['businessunit_id']);
	                }
	                $options ['cc'] = array($employee['emp_email'],$employee['rep_manager_email']);
	                $options ['message'] = '<div>
                    <div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
        				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
    						<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
    						<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">The below mentioned employee\'s probation period is ending. Please take necessary action.</div>
                        </div>
	                <div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
	                <tbody>
                    <tr bgcolor="#045396">
                        <td width="35%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
    	                <td width="65%" style="color:#ffffff">'.$employee['userfullname'].'</td>
	                </tr>
	                <tr bgcolor="#F5F5F3">
    	                <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Employee Code</td>
    	                <td style="color:#5b5b5b">'.$employee['employeeId'].'</td>
	                </tr>
                    <tr bgcolor="#D9DCE5">
    	                <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Employee Department</td>
    	                <td style="color:#5b5b5b">'.$employee['deptname'].'</td>
	                </tr>
	                <tr bgcolor="#F5F5F3">
    	                <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
    	                <td style="color:#5b5b5b">'.$employee['rep_manager'].'</td>
	                </tr>
                    <tr bgcolor="#D9DCE5">
	                   <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Probation Start date</td>
	                   <td style="color:#5b5b5b">'.$probationDetail['start_date'].'</td>
	                </tr>
	                <tr bgcolor="#F5F5F3">
	                   <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Probation End date</td>
	                   <td style="color:#5b5b5b">'.$probationDetail['end_date'].'</td>
	                </tr>
	                
	                
	                </tbody></table>
	                
	                </div>';
							sapp_Global::_sendEmail ( $options );
	            }
	        }
	        echo 'Cron executed successfully';
	    } catch ( Exception $e ) {
	        echo 'Cron Failed';
	    }
	} 
}

