<?php
class Default_EmpleavesummaryController extends Zend_Controller_Action {
	private $options;
	public function preDispatch() {
		$ajaxContext = $this->_helper->getHelper ( 'AjaxContext' );
		$ajaxContext->addActionContext ( 'saveleaverequestdetails', 'json' )->initContext ();
	}
	public function init() {
		$this->_options = $this->getInvokeArg ( 'bootstrap' )->getOptions ();
	}
	public function calculatebusinessdays($fromDate, $toDate) {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}

		$noOfDays = 0;
		$weekDay = '';
		$employeeDepartmentId = '';
		$employeeGroupId = '';
		$weekend1 = '';
		$weekend2 = '';
		$holidayDatesArr = array ();
		// Calculating the no of days in b/w from date & to date with out taking weekend & holidays....
		$employeesmodel = new Default_Model_Employees ();
		$leavemanagementmodel = new Default_Model_Leavemanagement ();
		$holidaydatesmodel = new Default_Model_Holidaydates ();

		$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );
		if (! empty ( $loggedInEmployeeDetails )) {
			$employeeDepartmentId = $loggedInEmployeeDetails [0] ['department_id'];
			$employeeGroupId = $loggedInEmployeeDetails [0] ['holiday_group'];

			if ($employeeDepartmentId != '' && $employeeDepartmentId != NULL)
				$weekendDetailsArr = $leavemanagementmodel->getWeekendNamesDetails ( $employeeDepartmentId );

			if (! empty ( $weekendDetailsArr )) {
				if ($weekendDetailsArr [0] ['is_skipholidays'] == 1 && isset ( $employeeGroupId ) && $employeeGroupId != '') {
					$holidayDateslistArr = $holidaydatesmodel->getHolidayDatesListForGroup ( $employeeGroupId );
					if (! empty ( $holidayDateslistArr )) {
						for($i = 0; $i < sizeof ( $holidayDateslistArr ); $i ++) {
							$holidayDatesArr [$i] = $holidayDateslistArr [$i] ['holidaydate'];
						}
					}
				}
				$weekend1 = $weekendDetailsArr [0] ['daystartname'];
				$weekend2 = $weekendDetailsArr [0] ['dayendname'];
			}

			$fromdate_obj = new DateTime ( $fromDate );
			$weekDay = $fromdate_obj->format ( 'l' );
			while ( $fromDate <= $toDate ) {
				/* if(($weekDay != 'Saturday'||$weekDay != 'Sunday') && (!empty($holidayDates)) && (!in_array($fromDate,$holidayDates))) */
				if (count ( $holidayDatesArr ) > 0) {
					if ($weekDay != $weekend1 && $weekDay != $weekend2 && (! in_array ( $fromDate, $holidayDatesArr ))) {
						$noOfDays ++;
					}
				} else {
					if ($weekDay != $weekend1 && $weekDay != $weekend2) {
						$noOfDays ++;
					}
				}
				$fromdate_obj->add ( new DateInterval ( 'P1D' ) ); // Increment from date by one day...
				$fromDate = $fromdate_obj->format ( 'Y-m-d' );
				$weekDay = $fromdate_obj->format ( 'l' );
			}
		}

		return $noOfDays;
	}
public function indexAction()
    {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $loginUserId = $auth->getStorage()->read()->id;
            $loginUserRole = $auth->getStorage ()->read ()->emprole;
            $loginUserdepartment_id = $auth->getStorage ()->read ()->department_id;
        }
        $leaverequestmodel = new Default_Model_Leaverequest();
        $employeeleavetypemodel = new Default_Model_Employeeleavetypes();
        $totalLeaveTypes = $employeeleavetypemodel->getactiveleavetype();
        $empleavesummaryform = new Default_Form_empleavesummary();
        $employeesmodel = new Default_Model_Employees ();
        $employeeModel = new Default_Model_Employee ();
        $empleavesummaryform->setAttrib ( 'action', BASE_URL . 'empleavesummary' );
        $employeeleavetypemodel = new Default_Model_Employeeleavetypes ();
        $usersmodel = new Default_Model_Users ();
        $leavemanagementmodel = new Default_Model_Leavemanagement ();
        $holidaydatesmodel = new Default_Model_Holidaydates ();
        
        $employeeemail = '';
        $userfullname = '';
        $businessunitid = '';
        $reportingManageremail = '';
        $reportingmanagerName = '';
        $rep_mang_id = '';
        $rMngr = 'No';
        $week_startday = '';
        $week_endday = '';
        $ishalf_day = '';
        $businessunitid = '';
        $hremailgroup = '';
        $managerrequestdetails = '';
        $availableleaves = '';
        $members = array ();
        $dateofjoiningArr = array ();
        $msgarray = array ();
        $holidayDateslistArr = array ();        
        
        if ($loginUserId != '' && $loginUserId != NULL){
            $loggedinEmpId = $usersmodel->getUserDetailsByID ( $loginUserId );
            $loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );
            if (! empty ( $loggedInEmployeeDetails )) {
                if ($loggedInEmployeeDetails [0] ['date_of_joining'] != '') {
                    $date = new DateTime ( $loggedInEmployeeDetails [0] ['date_of_joining'] );
                    $datofjoiningtimestamp = $date->getTimestamp ();
                    $dateofjoining = explode ( "-", $loggedInEmployeeDetails [0] ['date_of_joining'] );
                    
                    $year = $dateofjoining [0];
                    $month = $dateofjoining [1];
                    $day = $dateofjoining [2];
                    $dateofjoiningArr = array (
                        'year' => $year,
                        'month' => $month,
                        'day' => $day,
                        'datetimestamp' => $datofjoiningtimestamp
                    );
                }
                $reportingmanagerId = $loggedInEmployeeDetails [0] ['reporting_manager'];
                $employeeDepartmentId = $loggedInEmployeeDetails [0] ['department_id'];
                $employeeEmploymentStatusId = $loggedInEmployeeDetails [0] ['emp_status_id'];
                $employeeHolidayGroupId = $loggedInEmployeeDetails [0] ['holiday_group'];
                
                $reportingManagerDetails = $usersmodel->getUserDetailsByID ( $reportingmanagerId );
                $weekendDatailsArr = $leavemanagementmodel->getWeekendDetails ( $employeeDepartmentId );
                $employeeemail = $loggedinEmpId [0] ['emailaddress'];
                $userfullname = $loggedinEmpId [0] ['userfullname'];
                $businessunitid = $loggedInEmployeeDetails [0] ['businessunit_id'];
                
                if (! empty ( $reportingManagerDetails )) {
                    $empleavesummaryform->rep_mang_id->setValue ( $reportingManagerDetails [0] ['userfullname'] );
                    $reportingManageremail = $reportingManagerDetails [0] ['emailaddress'];
                    $reportingmanagerName = $reportingManagerDetails [0] ['userfullname'];
                    $rep_mang_id = $reportingManagerDetails [0] ['id'];
                    $rMngr = 'Yes';
                } else {
                    $msgarray ['rep_mang_id'] = 'Reporting manager is not assigned yet. Please contact your HR.';
                }
                
                if (! empty ( $weekendDatailsArr )) {
                    $week_startday = $weekendDatailsArr [0] ['weekendstartday'];
                    $week_endday = $weekendDatailsArr [0] ['weekendday'];
                    $ishalf_day = $weekendDatailsArr [0] ['is_halfday'];
                    $isskip_holidays = $weekendDatailsArr [0] ['is_skipholidays'];
                } else {
                    $msgarray ['from_date'] = 'Leave management options are not configured yet.';
                    $msgarray ['to_date'] = 'Leave management options are not configured yet.';
                }
                
                if ($employeeHolidayGroupId != '' && $employeeHolidayGroupId != NULL)
                    $holidayDateslistArr = $holidaydatesmodel->getHolidayDatesListForGroup ( $employeeHolidayGroupId );
                
               if (defined ( 'LV_HR_' . $businessunitid ))
                        $hremailgroup = 'hremailgroupexists';
               else
                        $hremailgroup = '';
                            
               /* Search Filters */
                $isReportingManagerFlag = 'false';
                $searchRepFlag = 'false';
                $searchMeFlag = 'true';
                
                $isReportingManager = $employeesmodel->CheckIfReportingManager ( $loginUserId );
                if (! empty ( $isReportingManager ) && $isReportingManager [0] ['count'] > 0) {
                    $searchRepFlag = 'true';
                }
                
                $filter = $this->_request->getParam ( 'filter' );
                if (! empty ( $filter )) {
                    
                    if (in_array ( 2, $filter ))
                    {
                        $searchMeFlag = 'true';
                        $searchRepFlag = 'true';
                    }
                    else
                        $searchRepFlag = 'false';
                        
                        if (in_array ( 1, $filter ))
                            $searchMeFlag = 'true';
                }
                
                if ($searchMeFlag == 'true')
                    $leaverequestdetails = $leaverequestmodel->getUserApprovedOrPendingLeavesData ( $loginUserId );
                    /* Start -For Checking if logged in user is reporting manager */
                    
                    if (! empty ( $isReportingManager ) && $isReportingManager [0] ['count'] > 0) {
                        if ($searchRepFlag == 'true')
                            $managerrequestdetails = $leaverequestmodel->getManagerApprovedOrPendingLeavesData ( $loginUserId );
                            $isReportingManagerFlag = 'true';
                    }
                    /* End */
                    /* Start -For Checking if logged in user is hr manager for thar particular department */
                    
                    // get hr_id from leavemanagemnt table based on login user dept id
                    $configure_hr_id = $leavemanagementmodel->gethrDetails ( $loginUserdepartment_id );
                    if (! empty ( $configure_hr_id )) {
                        if ($configure_hr_id [0] ['hr_id'] == $loginUserId) {
                            if ($searchRepFlag == 'true')
                                $managerrequestdetails = $leaverequestmodel->getHrApprovedOrPendingLeavesData ( $loginUserId );
                                $isReportingManagerFlag = 'true';
                        }
                    }
                    
                $this->view->userfullname = $userfullname;
                $this->view->loggedinEmpId = $loggedinEmpId;
                $this->view->weekendDatailsArr = $weekendDatailsArr;
                $this->view->reportingManagerDetails = $reportingManagerDetails;
                $this->view->rMngr = $rMngr;
                $this->view->hremailgroup = $hremailgroup;
                $this->view->dateofjoiningArr = $dateofjoiningArr;
                $this->view->leaverequestdetails = ! empty ( $leaverequestdetails ) ? $leaverequestdetails : array ();
                $this->view->holidayDateslistArr = $holidayDateslistArr;
                $this->view->managerrequestdetails = ! empty ( $managerrequestdetails ) ? $managerrequestdetails : array ();
                $this->view->isReportingManagerFlag = $isReportingManagerFlag;
                $this->view->searchRepFlag = $searchRepFlag;
                $this->view->searchMeFlag = $searchMeFlag;
            }
            
            else {
                $msgarray ['rep_mang_id'] = 'Reporting manager is not assigned yet. Please contact your HR.';
                $msgarray ['from_date'] = 'Leave management options are not configured yet.';
                $msgarray ['to_date'] = 'Leave management options are not configured yet.';
            }
        }      
        
        $call = $this->_request->getParam('call');
        if ($call == 'ajaxcall')
            $this->_helper->layout->disableLayout();

        $view = Zend_Layout::getMvcInstance()->getView();
        $objname = $this->_getParam('objname');
        $refresh = $this->_getParam('refresh');
        $dashboardcall = $this->_getParam('dashboardcall');
        $flag = $this->_request->getParam('flag');
        if ($this->_request->getParams()['leavetypeid']) {
            $leavetypeid = $this->_request->getParams()['leavetypeid'];
        } else {
            $leavetypeid = $totalLeaveTypes[0]['id'];
        }

        $data = array();
        $searchQuery = '';
        $searchArray = array();
        $tablecontent = '';

        if ($refresh == 'refresh') {
            if ($dashboardcall == 'Yes')
                $perPage = DASHBOARD_PERPAGE;
            else
                $perPage = PERPAGE;
            $sort = 'ASC';
            $by = 'leavestatus';
            $pageNo = 1;
            $searchData = '';
        } else {
            $sort = ($this->_getParam('sort') != '') ? $this->_getParam('sort') : 'ASC';
            $by = ($this->_getParam('by') != '') ? $this->_getParam('by') : 'leavestatus';
            if ($dashboardcall == 'Yes')
                $perPage = $this->_getParam('per_page', DASHBOARD_PERPAGE);
            else
                $perPage = $this->_getParam('per_page', PERPAGE);
            $pageNo = $this->_getParam('page', 1);
            /**
             * search from grid - START *
             */
            $searchData = $this->_getParam('searchData');
            $searchData = rtrim($searchData, ',');
        /**
         * search from grid - END *
         */
        }

        $empleaveArray = array(
            'pendingleaves',
            'cancelledleaves',
            'approvedleaves',
            'rejectedleaves'
        );
        $objName = 'empleavesummary';
        if (! empty($flag)) {
            if (in_array($flag, $empleaveArray)) {
                $objName = $flag;
                $queryflag = substr($flag, 0, - 6);
            }
        }

        $dataTmp = $leaverequestmodel->getGrid($sort, $by, $perPage, $pageNo, $searchData, $call, $dashboardcall, $objName, $queryflag);
        $_SESSION['grid_data'] = $dataTmp;
        $leavesCountArray = sapp_Helper::getmanagerLeavesCountByCategory($loginUserId);
        $exceptionCountArray = sapp_Helper::getManagerExceptionsCountByCategory($loginUserId);
        $compoffCountArray = sapp_Helper::getcompoffCountByCategory($loginUserId);
        $leavesCountArray['action'] = "leave";

        /*
         * Start
         * Query to fetch and build multioption for Leavetype dropdown
         */
        $leavetype = $employeeleavetypemodel->getactiveleavetype ();
        if (! empty ( $leavetype )) {
            if (sizeof ( $leavetype ) > 0) {
                foreach ( $leavetype as $leavetyperes ) {
                    $empleavesummaryform->leavetypeid->addMultiOption ( $leavetyperes ['id'] . '!@#' . $leavetyperes ['numberofdays'] . '!@#' . utf8_encode ( $leavetyperes ['leavetype'] ), utf8_encode ( $leavetyperes ['leavetype'] ) );
                }
            }
        } else {
            $msgarray ['leavetypeid'] = ' Leave types are not configured yet.';
        }
        $this->view->leavetype = $leavetype;
        /* END */
        
        /*
         * START
         * Query to fetch and build multioption for employee dropdown
         */
        if ($employeeModel->isHrEmployee ( $loginUserId ) || $loginUserRole == SUPERADMINROLE) {
            $myteam = $employeeModel->getEmployees ( '', $loginUserId, '', '', null, null, null );
        } else {
            $myteam = $employeeModel->getEmployeesUnderRM ( $loginUserId );
        }
        
        foreach ( $myteam as $member ) {
            
            $employeeInfo = $employeeModel->getActiveEmployeeData ( $member ['user_id'] );
            if ($employeeInfo [0] ['businessunit_id'] != null) {
//                 $mem ['user_id'] = $member ['user_id'];
//                 $mem ['userfullname'] = $member ['userfullname'];
//                 array_push ( $members, $mem );
                $empleavesummaryform->employeename->addMultiOption ( $member ['user_id'] . '!@#' . utf8_encode ( $member ['userfullname'] ), utf8_encode ( $member ['userfullname'] ) );
            }
        }
        $this->view->data = $members;
        /* END */
        
        /*
         * START
         * Query to get the number of available leaves for the employee
         */
        $getavailbaleleaves = $leaverequestmodel->getAvailableLeaves ( $loginUserId );
        if (! empty ( $getavailbaleleaves )) {
            $empleavesummaryform->no_of_days->setValue ( $getavailbaleleaves [0] ['remainingleaves'] );
            $availableleaves = $getavailbaleleaves [0] ['remainingleaves'];
        } else {
            $msgarray ['no_of_days'] = 'You have not been allotted leaves for this financial year. Please contact your HR.';
        }
        $this->view->getavailbaleleaves = $getavailbaleleaves;
        /* END */        
        
        array_push($data, $dataTmp);
        $this->view->dataArray = $data;
        $this->view->call = $call;
        $this->view->flag = $flag;
        $this->view->leavesCountArray = $leavesCountArray;
        $this->view->exceptionCountArray = $exceptionCountArray;
        $this->view->compoffCountArray = $compoffCountArray;
        $this->view->totalLeaveTypes = $totalLeaveTypes;
        $this->view->form = $empleavesummaryform;
        $this->view->msgarray = $msgarray;
        $this->view->currentleavetypeid = $leavetypeid;
        $this->view->loginUserId = $loginUserId;
        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    public function exceptionAction()
    {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $loginUserId = $auth->getStorage()->read()->id;
        }

        $exceptionrequestmodel = new Default_Model_ExceptionRequest();
        $employeeleavetypemodel = new Default_Model_Employeeleavetypes();
        $totalLeaveTypes = $employeeleavetypemodel->getactiveleavetype();
        $call = $this->_getParam('call');
        if ($call == 'ajaxcall')
            $this->_helper->layout->disableLayout();

        $view = Zend_Layout::getMvcInstance()->getView();
        $objname = $this->_getParam('objname');
        $refresh = $this->_getParam('refresh');
        $dashboardcall = $this->_getParam('dashboardcall');
        $dashboardcall = $this->_getParam('dashboardcall');
        $flag = $this->_request->getParam('flag');
        if ($this->_request->getParams()['leavetypeid']) {
            $leavetypeid = $this->_request->getParams()['leavetypeid'];
        } else {
            $leavetypeid = $totalLeaveTypes[0]['id'];
        }

        $data = array();
        $searchQuery = '';
        $searchArray = array();
        $tablecontent = '';

        if ($refresh == 'refresh') {
            if ($dashboardcall == 'Yes')
                $perPage = DASHBOARD_PERPAGE;
            else
                $perPage = PERPAGE;
            $sort = 'ASC';
            $by = 'exceptionstatus';
            $pageNo = 1;
            $searchData = '';
        } else {
            $sort = ($this->_getParam('sort') != '') ? $this->_getParam('sort') : 'ASC';
            $by = ($this->_getParam('by') != '') ? $this->_getParam('by') : 'exceptionstatus';
            if ($dashboardcall == 'Yes')
                $perPage = $this->_getParam('per_page', DASHBOARD_PERPAGE);
            else
                $perPage = $this->_getParam('per_page', PERPAGE);
            $pageNo = $this->_getParam('page', 1);
            /**
             * search from grid - START *
             */
            $searchData = $this->_getParam('searchData');
            $searchData = rtrim($searchData, ',');
        /**
         * search from grid - END *
         */
        }

        $exceptionsArray = array(
            'pendingexceptions',
            'cancelledexceptions',
            'approvedexceptions',
            'rejectedexceptions',
            'expiredexceptions'
        );

        $objName = 'empleavesummary';
        $queryflag = 'all';
        if (! empty($flag)) {
            if (in_array($flag, $exceptionsArray)) {
                $objName = $flag;
                $queryflag = substr($flag, 0, - 10);
            }
            if ($flag == 'total') {
                $queryflag = 'all';
            }
        }
        $dataTmp = $exceptionrequestmodel->getGrid($sort, $by, $perPage, $pageNo, $searchData, $call, $dashboardcall, $objName, $queryflag);
        $_SESSION['grid_data'] = $dataTmp;
        $leavesCountArray = sapp_Helper::getManagerLeavesCountByCategory($loginUserId);
        $exceptionCountArray = sapp_Helper::getManagerExceptionsCountByCategory($loginUserId);
        $compoffCountArray = sapp_Helper::getcompoffCountByCategory($loginUserId);
        $exceptionCountArray['action'] = "exception";

        array_push($data, $dataTmp);
        $this->view->dataArray = $data;
        $this->view->call = $call;
        $this->view->flag = $flag;
        $this->view->leavesCountArray = $leavesCountArray;
        $this->view->exceptionCountArray = $exceptionCountArray;
        $this->view->compoffCountArray = $compoffCountArray;
        $this->view->currentleavetypeid = $leavetypeid;
        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    public function compoffAction()
    {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $loginUserId = $auth->getStorage()->read()->id;
        }

        $compoffrequestmodel = new Default_Model_Compoffrequest();
        $employeeleavetypemodel = new Default_Model_Employeeleavetypes();
        $totalLeaveTypes = $employeeleavetypemodel->getactiveleavetype();
        $call = $this->_getParam('call');
        if ($call == 'ajaxcall')
            $this->_helper->layout->disableLayout();

        $view = Zend_Layout::getMvcInstance()->getView();
        $objname = $this->_getParam('objname');
        $refresh = $this->_getParam('refresh');
        $dashboardcall = $this->_getParam('dashboardcall');
        $dashboardcall = $this->_getParam('dashboardcall');
        $flag = $this->_request->getParam('flag');
        if ($this->_request->getParams()['leavetypeid']) {
            $leavetypeid = $this->_request->getParams()['leavetypeid'];
        } else {
            $leavetypeid = $totalLeaveTypes[0]['id'];
        }

        $data = array();
        $searchQuery = '';
        $searchArray = array();
        $tablecontent = '';

        if ($refresh == 'refresh') {
            if ($dashboardcall == 'Yes')
                $perPage = DASHBOARD_PERPAGE;
            else
                $perPage = PERPAGE;
            $sort = 'ASC';
            $by = 'status';
            $pageNo = 1;
            $searchData = '';
        } else {
            $sort = ($this->_getParam('sort') != '') ? $this->_getParam('sort') : 'ASC';
            $by = ($this->_getParam('by') != '') ? $this->_getParam('by') : 'status';
            if ($dashboardcall == 'Yes')
                $perPage = $this->_getParam('per_page', DASHBOARD_PERPAGE);
            else
                $perPage = $this->_getParam('per_page', PERPAGE);
            $pageNo = $this->_getParam('page', 1);
            /**
             * search from grid - START *
             */
            $searchData = $this->_getParam('searchData');
            $searchData = rtrim($searchData, ',');
        /**
         * search from grid - END *
         */
        }

        $compoffArray = array(
            'pendingcompoff',
            'cancelledcompoff',
            'approvedcompoff',
            'rejectedcompoff',
            'expiredcompoff'
        );

        $objName = 'empleavesummary';
        $queryflag = 'all';
        if (! empty($flag)) {
            if (in_array($flag, $compoffArray)) {
                $objName = $flag;
                $queryflag = substr($flag, 0, - 7);
            }
            if ($flag == 'total') {
                $queryflag = 'all';
            }
        }
        $dataTmp = $compoffrequestmodel->getGrid($sort, $by, $perPage, $pageNo, $searchData, $call, $dashboardcall, $objName, $queryflag);
        $_SESSION['grid_data'] = $dataTmp;
        $leavesCountArray = sapp_Helper::getManagerLeavesCountByCategory($loginUserId);
        $exceptionCountArray = sapp_Helper::getManagerExceptionsCountByCategory($loginUserId);
        $compoffCountArray = sapp_Helper::getcompoffCountByCategory($loginUserId);

        $compoffCountArray['action'] = "compoff";

        array_push($data, $dataTmp);
        $this->view->dataArray = $data;
        $this->view->call = $call;
        $this->view->flag = $flag;
        $this->view->leavesCountArray = $leavesCountArray;
        $this->view->exceptionCountArray = $exceptionCountArray;
        $this->view->compoffCountArray = $compoffCountArray;
        $this->view->totalCompoff = $leavesCountArray['total'];
        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }
	public function viewAction() {
		$id = intval ( $this->getRequest ()->getParam ( 'id' ) );
		$callval = $this->getRequest ()->getParam ( 'call' );
		$type = $this->getRequest()->getParams()['type'];
		if ($callval == 'ajaxcall')
			$this->_helper->layout->disableLayout ();
		$objName = 'pendingleaves';
		$reportingmanagerStatus = '';
		$leaverequestform = new Default_Form_leaverequest ();
		$leaverequestform->removeElement ( "submit" );
		$elements = $leaverequestform->getElements ();
		if (count ( $elements ) > 0) {
			foreach ( $elements as $key => $element ) {
				if (($key != "Cancel") && ($key != "Edit") && ($key != "Delete") && ($key != "Attachments")) {
					$element->setAttrib ( "disabled", "disabled" );
				}
			}
		}
		$leaverequestmodel = new Default_Model_Leaverequest ();
		if (is_int ( $id ) && $id != 0) {
			try {
				if ($id) {
					$data = $leaverequestmodel->getLeaveRequestDetails ( $id );
					if (! empty ( $data )) {
						$data = $data [0];
						$employeeleavetypemodel = new Default_Model_Employeeleavetypes ();
						$usersmodel = new Default_Model_Users ();

						$employeeleavetypeArr = $employeeleavetypemodel->getsingleEmployeeLeavetypeData ( $data ['leavetypeid'] );
						if ($employeeleavetypeArr != 'norows') {
							$leaverequestform->leavetypeid->addMultiOption ( $employeeleavetypeArr [0] ['id'], utf8_encode ( $employeeleavetypeArr [0] ['leavetype'] ) );
							$data ['leavetypeid'] = $employeeleavetypeArr [0] ['leavetype'];
						}

						if ($data ['leaveday'] == 1) {
							$leaverequestform->leaveday->addMultiOption ( $data ['leaveday'], 'Full Day' );
							$data ['leaveday'] = 'Full Day';
						} else {
							$leaverequestform->leaveday->addMultiOption ( $data ['leaveday'], 'Half Day' );
							$data ['leaveday'] = 'Half Day';
						}

						$repmngrnameArr = $usersmodel->getUserDetailsByID ( $data ['rep_mang_id'], 'all' );
						$leaverequestform->populate ( $data );

						$from_date = sapp_Global::change_date ( $data ["from_date"], 'view' );
						$to_date = sapp_Global::change_date ( $data ["to_date"], 'view' );
						$appliedon = sapp_Global::change_date ( $data ["createddate"], 'view' );
						$leaverequestform->from_date->setValue ( $from_date );
						$leaverequestform->to_date->setValue ( $to_date );
						$leaverequestform->createddate->setValue ( $appliedon );
						$leaverequestform->appliedleavesdaycount->setValue ( $data ['appliedleavescount'] );
						if (! empty ( $repmngrnameArr )) {
							$reportingmanagerStatus = $repmngrnameArr [0] ['isactive'];
							$leaverequestform->rep_mang_id->setValue ( $repmngrnameArr [0] ['userfullname'] );
							$data ['rep_mang_id'] = $repmngrnameArr [0] ['userfullname'];
						}
						$leaverequestform->setDefault ( 'leavetypeid', $data ['leavetypeid'] );
						$leaverequestform->setDefault ( 'leaveday', $data ['leaveday'] );
						$this->view->controllername = $objName;
						$this->view->id = $id;
						$this->view->data = $data;
						$this->view->form = $leaverequestform;
						$this->view->reportingmanagerStatus = $reportingmanagerStatus;
					} else {
						$this->view->rowexist = "rows";
					}
				} else {
					$this->view->rowexist = "rows";
				}
			} catch ( Exception $e ) {
				$this->view->rowexist = "norows";
			}
		} else {
			$this->view->rowexist = "norows";
		}
		if ($type == 'exception') {
			$form = new Default_Form_managerexceptionrequest();
			try {
				if ($id && is_numeric($id) && $id > 0) {
					$exceptionrequestmodel = new Default_Model_ExceptionRequest();
					$usersmodel = new Default_Model_Users();
					$flag = 'true';

					$userid = $exceptionrequestmodel->getUserID($id);
					$getreportingManagerArr = $exceptionrequestmodel->getReportingManagerId($id);
					$reportingManager = $getreportingManagerArr[0]['repmanager'];
					$hrmanager = $getreportingManagerArr[0]['hrmanager'];
					if ($reportingManager != $loginUserId && $hrmanager != $loginUserId)
						$flag = 'false';
						if (! empty($userid))
							$isactiveuser = $usersmodel->getUserDetailsByID($userid[0]['user_id']);
							else
								$this->view->rowexist = "rows";

								if (! empty($userid) && ! empty($isactiveuser)) {
									$data = $exceptionrequestmodel->getExceptionRequestDetails($id);
									if (! empty($data)) {
										$data = $data[0];
										$reason = $data['reason'];
										$appliedexceptioncount = $data['appliedexceptioncount'];
										$exceptionstatus = $data['exceptionstatus'];
										$employeeid = $data['user_id'];
										$usersmodel = new Default_Model_Users();
										$employeesmodel = new Default_Model_Employees();
										$businessunitid = '';
										$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails($employeeid);
										if ($loggedInEmployeeDetails[0]['businessunit_id'] != '')
											$businessunitid = $loggedInEmployeeDetails[0]['businessunit_id'];
											if ($data['exceptionday'] == 1) {
												$form->exceptionday->addMultiOption($data['exceptionday'], 'Full Day');
												$data['exceptionday'] = 'Full Day';
											} else {
												$form->exceptionday->addMultiOption($data['exceptionday'], 'Half Day');
												$data['exceptionday'] = 'Half Day';
											}

											$employeenameArr = $usersmodel->getUserDetailsByID($data['user_id']);
											$employeeemail = $employeenameArr[0]['emailaddress'];
											$employeename = $employeenameArr[0]['userfullname'];
											$form->populate($data);

											if ($data['exceptionstatus'] == 'Approved') {
												$form->managerstatus->setLabel("Cancel");
												$form->managerstatus->clearMultiOptions();
												$form->managerstatus->addMultiOption(3, utf8_encode("Cancel"));
											}

											$from_date = sapp_Global::change_date($data['from_date'], 'view');
											$to_date = sapp_Global::change_date($data['to_date'], 'view');
											$appliedon = sapp_Global::change_date($data['createddate'], 'view');

											// to show Exception Request history in view
											$exceptionrequesthistory_model = new Default_Model_Exceptionrequesthistory();
											$exception_history = $exceptionrequesthistory_model->getExceptionRequestHistory($id);
											$this->view->exception_history = $exception_history;
											// end

											$form->from_date->setValue($from_date);
											$form->to_date->setValue($to_date);
											$form->createddate->setValue($appliedon);
											$form->appliedexceptioncount->setValue($data['appliedexceptioncount']);
											$form->employeename->setValue($employeenameArr[0]['userfullname']);
											$form->setDefault('exceptionday', $data['exceptionday']);
											$this->view->controllername = $objName;
											$this->view->id = $id;
											$this->view->form = $form;
											$this->view->data = $data;
											$this->view->type = $type;
									} else {
										$this->view->rowexist = "rows";
									}
								} else {
									$this->view->rowexist = "rows";
								}
				} else {
					$this->view->rowexist = "rows";
				}
			} catch (Exception $e) {
				$this->view->rowexist = 'norows';
			}
		} else if ($type == 'compoff') {

			$form = new Default_Form_managercompoffrequest();

			try {
				if ($id && is_numeric($id) && $id > 0) {
					// $this->_redirect('employeeexceptions/edit/id/'.$id);
					$compoffrequestmodel = new Default_Model_Compoffrequest();
					$usersmodel = new Default_Model_Users();
					$flag = 'true';

					$userid = $compoffrequestmodel->getUserID($id);
					$getreportingManagerArr = $compoffrequestmodel->getReportingManagerId($id);
					$reportingManager = $getreportingManagerArr[0]['repmanager'];
					$hrmanager = $getreportingManagerArr[0]['hrmanager'];
					if ($reportingManager != $loginUserId && $hrmanager != $loginUserId)
						$flag = 'false';
						if (! empty($userid))
							$isactiveuser = $usersmodel->getUserDetailsByID($userid[0]['user_id']);
							else
								$this->view->rowexist = "rows";

								if (! empty($userid) && ! empty($isactiveuser)) {
									$data = $compoffrequestmodel->getCompoffRequestDetails($id);
									if (! empty($data)) {
										$data = $data[0];
										$compoffstatus = $data['status'];
										$employeeid = $data['user_id'];
										$CompoffType = $data['option'];
										$usersmodel = new Default_Model_Users();
										$employeesmodel = new Default_Model_Employees();
										$businessunitid = '';
										$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails($employeeid);
										if ($loggedInEmployeeDetails[0]['businessunit_id'] != '')
											$businessunitid = $loggedInEmployeeDetails[0]['businessunit_id'];

											$employeenameArr = $usersmodel->getUserDetailsByID($data['user_id']);
											$employeeemail = $employeenameArr[0]['emailaddress'];
											$employeename = $employeenameArr[0]['userfullname'];
											$form->populate($data);
											if ($data['status'] == 'Approved') {
												$form->managerstatus->setLabel("Cancel");
												$form->managerstatus->clearMultiOptions();
												$form->managerstatus->addMultiOption(3, utf8_encode("Cancel"));
											}
											$avail_date = sapp_Global::change_date($data['avail_date'], 'view');
											$appliedon = sapp_Global::change_date($data['createddate'], 'view');
											$leavedate = sapp_Global::change_date($data['leave_date'], 'view');

											$form->avail_date->setValue($avail_date);
											$form->createddate->setValue($appliedon);
											$form->leave_date->setValue($leavedate);
											$form->employeename->setValue($employeenameArr[0]['userfullname']);
											$form->setDefault('availday', $data['availday']);
											$this->view->controllername = $objName;
											$this->view->id = $id;
											$this->view->form = $form;
											$this->view->data = $data;
											$this->view->type = $type;
									} else {
										$this->view->rowexist = "rows";
									}
								} else {
									$this->view->rowexist = "rows";
								}
				} else {
					$this->view->rowexist = "rows";
				}
			} catch (Exception $e) {
				$this->view->rowexist = 'norows';
			}
		}
	}
	public function editpopupAction() {
		Zend_Layout::getMvcInstance ()->setLayoutPath ( APPLICATION_PATH . "/layouts/scripts/popup/" );
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		$id = $this->getRequest ()->getParam ( 'id' );
		$leaverequestmodel = new Default_Model_Leaverequest ();
		$data = $leaverequestmodel->getLeaveRequestDetails ( $id );

		$usersmodel = new Default_Model_Users ();
		$employeesmodel = new Default_Model_Employees ();
		$leavemanagementmodel = new Default_Model_Leavemanagement ();
		$employeeleavetypemodel = new Default_Model_Employeeleavetypes ();
		$empleavesummaryform = new Default_Form_empleavesummary ();
		$leavetype = $employeeleavetypemodel->getactiveleavetype ();

		$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $data [0] ['user_id'] );

		if (! empty ( $loggedInEmployeeDetails )) {
			if ($loggedInEmployeeDetails [0] ['date_of_joining'] != '') {
				$date = new DateTime ( $loggedInEmployeeDetails [0] ['date_of_joining'] );
				$datofjoiningtimestamp = $date->getTimestamp ();
				$dateofjoining = explode ( "-", $loggedInEmployeeDetails [0] ['date_of_joining'] );

				$year = $dateofjoining [0];
				$month = $dateofjoining [1];
				$day = $dateofjoining [2];
				$dateofjoiningArr = array (
						'year' => $year,
						'month' => $month,
						'day' => $day,
						'datetimestamp' => $datofjoiningtimestamp
				);
			}

			$weekendDatailsArr = $leavemanagementmodel->getWeekendDetails ( $loggedInEmployeeDetails [0] ['department_id'] );
		}
		foreach ( $leavetype as $leavetyperes ) {
			$value = $leavetyperes ['id'] . '!@#' . $leavetyperes ['numberofdays'] . '!@#' . utf8_encode ( $leavetyperes ['leavetype'] );
			$empleavesummaryform->leavetypeid->addMultiOption ( $value, utf8_encode ( $leavetyperes ['leavetype'] ) );

			if ($leavetyperes ['id'] == $data [0] ['leavetypeid']) {
				$empleavesummaryform->leavetypeid->setValue ( $value );
			}
		}
		if ($data != null) {
			$empleavesummaryform->appliedleavesdaycount->setValue ( $data [0] ['appliedleavescount'] );
			$fromDate = new DateTime ( $data [0] ['from_date'] );
			$toDate = new DateTime ( $data [0] ['to_date'] );
			$empleavesummaryform->from_date->setValue ( $fromDate->format ( 'd-m-Y' ) );
			$empleavesummaryform->to_date->setValue ( $toDate->format ( 'd-m-Y' ) );
			$empleavesummaryform->reason->setValue ( $data [0] ['reason'] );
			$empleavesummaryform->userid->setValue ( $data [0] ['user_id'] );
			$empleavesummaryform->id->setValue ( $data [0] ['id'] );
			$empleavesummaryform->leaveday->setValue ( $data [0] ['leaveday'] );

		}

		$this->view->form = $empleavesummaryform;
		$this->view->controllername = 'empleavesummary';
		$this->view->dateofjoiningArr = $dateofjoiningArr;
		$this->view->weekendDatailsArr = $weekendDatailsArr;
	}

	public function saveleaverequestdetailsAction() {
	    $this->_helper->layout->disableLayout ();
	    $auth = Zend_Auth::getInstance ();
	    if ($auth->hasIdentity ()) {
	        $loginUserId = $auth->getStorage ()->read ()->id;
	    }

	    $editLeaveRequest = $this->_request->getPost ();
	    $leaverequestmodel = new Default_Model_Leaverequest();
	    $employeeleavetypesmodel = new Default_Model_Employeeleavetypes();

	    $originalId = $editLeaveRequest['id'];
	    $original = $leaverequestmodel->getsinglePendingLeavesData($originalId)[0];

	    if($original['leavestatus']=='Rejected' || $original['leavestatus']=='Cancelled') {

	    	$messages ['leavetypeid'] = array (
	    			'Leave request cannot be edited'
	    	);

	        $this->_helper->json($messages);
	    }

	    $dataarr = array('isactive'=>0);
	    $where = array('id=?'=>$originalId);

	    $leaverequestmodel->SaveorUpdateLeaveRequest($dataarr, $where);

	    $leavetypeoriginal = $employeeleavetypesmodel->getLeavetypeDataByID($original['leavetypeid']);
	    if(!empty($leavetypeoriginal)) {
	        if($leavetypeoriginal[0]['leavepredeductable'] == 1) {
	            $updateemployeeleave = $leaverequestmodel->updatecancelledemployeeleaves($original['appliedleavescount'],$original['user_id'],$original['leavetypeid']);
	        }
	    }

	    $msgarray = array();
	    $data = array();
	    $errorflag = $leaverequestmodel->validateLeaveRequest($editLeaveRequest, $original['user_id'], $msgarray, $data);
	    $leaveRequestForm = new Default_Form_empleavesummary();
	    if($errorflag == 'true' && $leaveRequestForm->isValid($this->_request->getPost ()) ){
	        $data['parent'] = $originalId;
	        $where = '';
	        $data ['createdby'] = $loginUserId;
	        $data ['createddate'] = gmdate ( "Y-m-d H:i:s" );
	        $id = $leaverequestmodel->SaveorUpdateLeaveRequest($data, $where);

	        $leavetypenew = $employeeleavetypesmodel->getLeavetypeDataByID($data['leavetypeid']);
	        if (! empty ( $leavetypenew )) {
	            if ($leavetypenew[0] ['leavepredeductable'] == 1) {
	                $updateemployeeleave = $leaverequestmodel->updateemployeeleaves ( $data ['appliedleavescount'], $data ['user_id'], $data ['leavetypeid'] );
	            }
	        }

	        $employeeleavemodel = new Default_Model_Employeeleaves ();
	        $employeeleaves = $employeeleavemodel->getsingleEmployeeleaveDataByType ( $data ['user_id'], $data ['leavetypeid'] );
	        $data ['leavetypecount'] = $employeeleaves [0] ['emp_leave_limit'];
	        $data ['leavetypetext'] = $leavetypenew [0] ['leavetype'];

	        $this->_helper->json ( array (
	            'result' => 'saved',
	            'message' => 'Leave request edited successfully.',
	            'controller' => 'empleavesummary',
	            'leaverequest' => $data
	        ) );
	    }else{
	        // Restore original leave incase of validation failure.
	        $dataarr = array('isactive'=>1);
	        $where = array('id=?'=>$originalId);
	        $leaverequestmodel->SaveorUpdateLeaveRequest($dataarr, $where);
	        if(!empty($leavetypeoriginal)) {
	            if($leavetypeoriginal[0]['leavepredeductable'] == 1) {
	                $updateemployeeleave = $leaverequestmodel->updateemployeeleaves ($original['appliedleavescount'],$original['user_id'],$original['leavetypeid']);
	            }
	        }

	        $messages = $leaveRequestForm->getMessages ();

	        if (isset ( $msgarray ['from_date'] )) {

	        	$messages ['from_date'] = array (
	        			$msgarray ['from_date']
	        	);
	        }
	        if (isset ( $msgarray ['to_date'] )) {

	        	$messages ['to_date'] = array (
	        			$msgarray ['to_date']
	        	);
	        }
	        if (isset ( $msgarray ['leaveday'] )) {

	        	$messages ['leaveday'] = array (
	        			$msgarray ['leaveday']
	        	);
	        }
	        if (isset ( $msgarray ['leavetypeid'] )) {

	        	$messages ['leavetypeid'] = array (
	        			$msgarray ['leavetypeid']
	        	);
	        }
	        if (isset ( $msgarray ['appliedleavesdaycount'] )) {

	        	$messages ['appliedleavesdaycount'] = array (
	        			$msgarray ['appliedleavesdaycount']
	        	);
	        }
	        $messages ['result'] = 'error';
	        $this->_helper->json ( $messages );
	    }

	}

	public function empleavesummarypdfAction()
	{
		$this->_helper->layout->disableLayout ();
		$leaverequestmodel = new Default_Model_Leaverequest ();
		$emp_report_data = $leaverequestmodel->getEmployeeSummaryReportData ();
		$griddata = $emp_report_data ['tableheader'];
		$emp_arr = $emp_report_data ['emp_arr'];
		$field_names = array ();
		$field_widths = array ();
		$data ['field_name_align'] = array ();

		foreach ( $griddata as $column_key => $column_name ) {
			if ($column_name == 'Department Name')
				$column_name = 'Department';
			$field_names [] = array (
						'field_name' => $column_key,
						'field_label' => $column_name
				);
			if ($column_key == 'departmentname')
				$field_widths [] = 28;
			else if ($column_key == 'to_date')
				$field_widths [] = 20;
			else if ($column_key == 'appliedleavescount')
				$field_widths [] = 28;
			else if ($column_key == 'leavestatus')
				$field_widths [] = 18;
			else
				$field_widths [] = 25;

			$data ['field_name_align'] [] = 'C';
		}
		if (count ( $cols_param_arr ) != 7) {
			$totalPresentFieldWidth = array_sum ( $field_widths );
			foreach ( $field_widths as $key => $width ) {
				$field_widths [$key] = ($width * 180) / $totalPresentFieldWidth;
			}
		}
		$data = array (
				'grid_no' => 1,
				'project_name' => '',
				'object_name' => 'Employee Leave Summary',
				'grid_count' => 1,
				'file_name' => 'EmployeeLeaveSummaryRpt.pdf'
		);
		$pdf = $this->_helper->PdfHelper->generateReport ( $field_names, $emp_arr, $field_widths, $data );

		$this->_helper->json ( array (
				'file_name' => $data ['file_name']
		) );
	}

	public function exportemployeesummaryexcelAction()
	{
		$this->_helper->layout->disableLayout ();
		$leaverequestmodel = new Default_Model_Leaverequest ();
		$emp_report_data = $leaverequestmodel->getEmployeeSummaryReportData ();
		$griddata = $emp_report_data ['tableheader'];
		$emp_arr = $emp_report_data ['emp_arr'];

		require_once 'Classes/PHPExcel.php';
		require_once 'Classes/PHPExcel/IOFactory.php';
		$objPHPExcel = new PHPExcel ();

		$letters = range ( 'A', 'Z' );
		$count = 0;
		$filename = "EmployeeLeaveSummaryRpt.xlsx";
		$cell_name = "";

		// Make first row Headings bold and highlighted in Excel.
		foreach ( $griddata as $names ) {
			$i = 1;
			$cell_name = $letters [$count] . $i;
			$names = html_entity_decode ( $names, ENT_QUOTES, 'UTF-8' );

			$objPHPExcel->getActiveSheet ()->SetCellValue ( $cell_name, $names );
			// Make bold cells
			$objPHPExcel->getActiveSheet ()->getStyle ( $cell_name )->getFont ()->setBold ( true );
			$objPHPExcel->getActiveSheet ()->getStyle ( $cell_name )->applyFromArray ( array (
					'fill' => array (
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array (
									'rgb' => '82CAFF'
							)
					)
			) );
			$objPHPExcel->getActiveSheet ()->getColumnDimension ( $letters [$count] )->setAutoSize ( true );
			$i ++;
			$count ++;
		}

		// Display field/column values in Excel.
		$i = 2;
		foreach ( $emp_arr as $emp_data ) {
			$count1 = 0;
			foreach ( $griddata as $column_key => $column_name ) {
				// display field/column values
				$cell_name = $letters [$count1] . $i;

				if ($column_key == 'userfullname') {
					$value = isset ( $emp_data ['userfullname'] ) ? (! empty ( $emp_data ['prefix_name'] ) ? ($emp_data ['prefix_name'] . ". " . $emp_data ['userfullname']) : $emp_data ['userfullname']) : "";
				} elseif ($column_key == 'date_of_joining') {
					$value = isset ( $emp_data ['date_of_joining'] ) ? sapp_Global::change_date ( $emp_data ['date_of_joining'], "view" ) : "";
				} else {
					$value = isset ( $emp_data [$column_key] ) ? $emp_data [$column_key] : "";
				}
				$value = html_entity_decode ( $value, ENT_QUOTES, 'UTF-8' );

				$objPHPExcel->getActiveSheet ()->SetCellValue ( $cell_name, $value );
				$count1 ++;
			}
			$i ++;
		}

		sapp_Global::clean_output_buffer ();
		header ( 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
		header ( "Content-Disposition: attachment; filename=\"$filename\"" );
		header ( 'Cache-Control: max-age=0' );
		sapp_Global::clean_output_buffer ();

		$objWriter = PHPExcel_IOFactory::createWriter ( $objPHPExcel, 'Excel2007' );
		$objWriter->save ( 'php://output' );

		exit ();
	}

	// To download pdf
	public function downloadreportAction(){
		$file_name = $this->_getParam('file_name', NULL);
		if(!empty($file_name)){
			$file = BASE_PATH.'/downloads/reports/'.$this->_getParam('file_name');
			$status = sapp_Global::downloadReport($file);
		}

	}

}


