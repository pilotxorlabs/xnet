<?php
/*********************************************************************************
 *  This file is part of Sentrifugo.
 *  Copyright (C) 2015 Sapplica
 *
 *  Sentrifugo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Sentrifugo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Sentrifugo.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Sentrifugo Support <support@sentrifugo.com>
 ********************************************************************************/

class Default_MypayslipController extends Zend_Controller_Action
{
    
    private $options;
    public function preDispatch()
    {
        
        $ajaxContext = $this->_helper->getHelper('AjaxContext');  
    }
    
    public function init()
    {
        $this->_options= $this->getInvokeArg('bootstrap')->getOptions();
        
    }
    
    public function indexAction()
    {
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $loginUserId = $auth->getStorage()->read()->id;
            $loginUserdepartment_id = $auth->getStorage()->read()->department_id;
        }
       
        $mypayslipmodel = new Default_Model_Mypayslip();
        $mypayslipform = new Default_Form_mypayslip();
        $this->view->form = $mypayslipform;
    }
    
    public function checkAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $loginUserId = $auth->getStorage()->read()->id;
            $loginUserdepartment_id = $auth->getStorage()->read()->department_id;
        }
        if($this->getPaySlip($loginUserId)){
            $this->_helper->json("success");
        }
    }
    
    public function downloadAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
    	$auth = Zend_Auth::getInstance();
    	if($auth->hasIdentity()){
    		$loginUserId = $auth->getStorage()->read()->id;
    		$loginUserdepartment_id = $auth->getStorage()->read()->department_id;
    	}
    	
    	$paySlip = $this->getPaySlip($loginUserId);
    	$this->_response->setHeader("Content-Length", strlen($paySlip['payslip']));
    	$this->_response->setHeader("Content-Type: application/pdf");
    	$this->_response->setHeader("Content-Disposition", "attachment;filename=" . "\"" . $paySlip['file_name'] . ".pdf\"");
    	$this->_response->setBody($paySlip['payslip']);
    	return $this->_response;
    }
    
    private function getPaySlip($loginUserId){
        $months = array(
            '1'=>'January','2'=>'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June',
            '7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November',
            '12'=>'December');
        $mypayslipmodel = new Default_Model_Mypayslip();
        $year = $this->_getParam('year');
        $month = $months[$this->_getParam('month')];
        
        return $mypayslipmodel->getUserPaySlip($loginUserId, $year, $month);
    }
}

