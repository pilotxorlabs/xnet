
<?php
/*********************************************************************************


 *

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/
class Default_PendingleavesController extends Zend_Controller_Action {
	private $options;
	public function preDispatch() {
		$ajaxContext = $this->_helper->getHelper ( 'AjaxContext' );
		$ajaxContext->addActionContext ( 'sendEmailCancelConfirmation', 'json' )->initContext ();
	}
	public function init() {
		$this->_options = $this->getInvokeArg ( 'bootstrap' )->getOptions ();
	}

	public function sendemailcancelconfirmation(){
		session_write_close ();
		$this->_helper->layout->disableLayout ();
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
			$loginUserEmail = $auth->getStorage ()->read ()->emailaddress;
			$loginUserName = $auth->getStorage ()->read ()->userfullname;
		}
		$id = $this->getRequest()->getParams()['id'];
		//if ($id == null)
		
		$leaverequestmodel = new Default_Model_Leaverequest ();
		$usersmodel = new Default_Model_Users ();
		$employeesmodel = new Default_Model_Employees ();
		$employeeleavetypesmodel = new Default_Model_Employeeleavetypes ();
		$compoffrequestmodel = new Default_Model_Compoffrequest();
		$compOffLeaveTypeId = $employeeleavetypesmodel->getEmployeeLeaveIdByLeaveType("Compensatory off");
		$currentLeaveType = $_SERVER['HTTP_REFERER'];
		if (strpos( $currentLeaveType, 'compoff' ) !== false)
			$type = 'compoff';
		if (strpos( $currentLeaveType, 'leavetypeid' ) !== false)
		{
			$leavtypeid = substr(strrchr($currentLeaveType, 'leavetypeid'),12);
		}
		
		if (($type == 'compoff') || ($leavtypeid == $compOffLeaveTypeId))
		{
			$id = $this->_request->getParam ( 'objid' );
			$data = $compoffrequestmodel->getCompoffRequestDetails($id);
			$data = $data['0'];
			$leaveOwner = $usersmodel->getUserDetailsByID ( $data ['user_id'] ) ['0'];
			//$hr = $employeesmodel->gethrId($leaveOwner['id']);
			//$to_date = $data['leave_date'];
			$from_date = $data ['leave_date'];
			//$reason = $data ['user_comments'];
			//$leavetypeid = $compOffLeaveTypeId;
			$msgstr = 'comp-off(s) against '.$data['avail_date'];
			$title = 'Compensatory-Off Request';
			
			$avail_date = $data ['avail_date'];
			$leave_date = $data ['leave_date'];
			$comment = $data ['user_comments'];
			if ($data ['option'] == 'Encashment')
				$optiontext = $data ['option'];
			else 
				$optiontext = 'Leave on '.$data['leave_date'];
			$hrmail = $usersmodel->getUserEmailId($data['hr_id']);
		}
		else 
		{
			$data = $leaverequestmodel->getsinglePendingLeavesData ( $id );
			$leaveOwner = $usersmodel->getUserDetailsByID ( $data ['0'] ['user_id'] ) ['0'];
			$to_date = $data ['to_date'];
			$from_date = $data ['from_date'];
			$reason = $data ['reason'];
			$leavetypeid = $data ['leavetypeid'];
			$msgstr = 'leave(s)';
			$title = 'Leave';
		}
		
		$appliedleavesdaycount = $data ['appliedleavescount'];
		
		$repmngrnameArr = $usersmodel->getUserDetailsByID ( $data ['rep_mang_id'] );
		$reportingmanageremail = $repmngrnameArr [0] ['emailaddress'];
		$reportingmanagername = $repmngrnameArr [0] ['userfullname'];
		$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );
		if ($loggedInEmployeeDetails [0] ['businessunit_id'] != '')
			$businessunitid = $loggedInEmployeeDetails [0] ['businessunit_id'];

		if ($to_date == '' || $to_date == NULL)
			$to_date = $from_date;
			/* Mail to Employee */
			$cancelMessage = "The below ".$msgstr." has been cancelled";
			if ($loginUserId != $leaveOwner ['id']) {
				$cancelMessage = $cancelMessage . " by $loginUserName on your behalf.";
			}
			$options ['subject'] = $title.' request cancelled';
			$options ['header'] = $title.' Request';
			$options ['toEmail'] = $leaveOwner ['emailaddress'];
			$options ['toName'] = $leaveOwner ['userfullname'];
			if (($type != 'compoff') && ($leavtypeid != $compOffLeaveTypeId))
			{
				$options ['message'] = '<div>
				<div style = "border:3px solid transparent;border-radius: 15px;background-color:#eeeeee; ">
				<div style="background-color:#5B5A55;border-radius: 10px 10px 0px 0px;">
					<div style="color:#ffffff;padding-left:5px;">Hi,</div>
								<div style="color:#ffffff;padding-left:5px;">' . $cancelMessage . '.</div>
										</div>
								<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					  <tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $leaveOwner ['userfullname'] . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">' . $appliedleavesdaycount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
            	     </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Leave</td>
                        <td style="color:#5b5b5b">' . $reason . '</td>
                  </tr>
                </tbody></table>
				
            </div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the leave details.</div>
            		</div>
            		</div>';
			}
			else 
			{
				$options ['message'] = '<div>
	        		<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">'. $cancelMessage .'</div>
												</div>
														<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
						<tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $leaveOwner['userfullname'] . '</td>
                      </tr>
		
                       <tr bgcolor="#F5F5F3">
                          <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reimbursement Option </td>
                          <td style="color:#5b5b5b">' . $optiontext . '</td>
                       </tr>
		
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Employee Comment</td>
                        <td style="color:#5b5b5b">' . $comment . '</td>
                  </tr>
                  <tr  bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
                        <td style="color:#5b5b5b">' . $reportingmanagername . '</td>
                  </tr>
                </tbody></table>';
			}
			
			$result = sapp_Global::_sendEmail ( $options );
			/* End */

			/* Mail to Reporting Manager */

			$ownerFullName = $leaveOwner ['userfullname'];
			$cancelMessage = "The below ".$msgstr." has been cancelled";
			if ($loginUserId != $leaveOwner ['id']) {
				$cancelMessage = $cancelMessage . " by $loginUserName on $ownerFullName's behalf.";
			}
			$options ['subject'] = $title.' request cancelled';
			$options ['header'] = $title.' Request';
			$options ['toEmail'] = $reportingmanageremail;
			$options ['toName'] = $reportingmanagername;
			if (($type != 'compoff') && ($leavtypeid != $compOffLeaveTypeId))
			{
				$options ['message'] = '<div>
				<div style = "border:3px solid transparent;border-radius: 15px;background-color:#eeeeee; ">
				<div style="background-color:#5B5A55;border-radius: 10px 10px 0px 0px;">
					<div style="color:#ffffff;padding-left:5px;">Hi,</div>
								<div style="color:#ffffff;padding-left:5px;">' . $cancelMessage . '.</div>
					</div>
								<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					  <tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $leaveOwner ['userfullname'] . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">' . $appliedleavesdaycount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
            	     </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Leave</td>
                        <td style="color:#5b5b5b">' . $reason . '</td>
                  </tr>
                </tbody></table>
				
            </div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the leave details.</div>
            		</div>
            		</div>';
			}
			else
			{
				$options ['message'] = '<div>
	        		<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">'. $cancelMessage .'</div>
												</div>
														<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
						<tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $leaveOwner['userfullname'] . '</td>
                      </tr>
			
                       <tr bgcolor="#F5F5F3">
                          <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reimbursement Option </td>
                          <td style="color:#5b5b5b">' . $optiontext . '</td>
                       </tr>
			
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Employee Comment</td>
                        <td style="color:#5b5b5b">' . $comment . '</td>
                  </tr>
                  <tr  bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
                        <td style="color:#5b5b5b">' . $reportingmanagername . '</td>
                  </tr>
                </tbody></table>';
			}
			$result = sapp_Global::_sendEmail ( $options );
			/* End */

			/* Mail to HR */
			if (!empty($hrmail)) {

				$options ['subject'] = $title.' request cancelled';
				$options ['header'] = $title.' Request';
				$options ['toEmail'] = $hrmail;
    			$options ['toName'] = 'Leave management';
				if (($type != 'compoff') && ($leavtypeid != $compOffLeaveTypeId))
				{
					$options ['message'] = '<div>
				<div style = "border:3px solid transparent;border-radius: 15px;background-color:#eeeeee; ">
				<div style="background-color:#5B5A55;border-radius: 10px 10px 0px 0px;">
						<div style="color:#ffffff;padding-left:5px;">Hi,</div>
								<div style="color:#ffffff;padding-left:5px;">' . $cancelMessage . '.</div>
						</div>
								<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
					  <tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $leaveOwner ['userfullname'] . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
                        <td style="color:#5b5b5b">' . $appliedleavesdaycount . '</td>
                      </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
                        <td style="color:#5b5b5b">' . $from_date . '</td>
                      </tr>
                      <tr bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
                        <td style="color:#5b5b5b">' . $to_date . '</td>
            	     </tr>
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Leave</td>
                        <td style="color:#5b5b5b">' . $reason . '</td>
                  </tr>
                </tbody></table>
					
            </div>
            <div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the leave details.</div>
            		</div>
            		</div>';
				}
				else
				{
					$options ['message'] = '<div>
	        		<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
				<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
												<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">'. $cancelMessage .'</div>
												</div>
														<div style = "padding : 8px 50px 0px 30px">
                <table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
                      <tbody>
						<tr bgcolor="#045396">
                        <td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Employee Name</td>
                        <td width="72%" style="color:#ffffff">' . $leaveOwner['userfullname'] . '</td>
                      </tr>
				
                       <tr bgcolor="#F5F5F3">
                          <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reimbursement Option </td>
                          <td style="color:#5b5b5b">' . $optiontext . '</td>
                       </tr>
				
                      <tr bgcolor="#D9DCE5">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Employee Comment</td>
                        <td style="color:#5b5b5b">' . $comment . '</td>
                  </tr>
                  <tr  bgcolor="#F5F5F3">
                        <td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reporting Manager</td>
                        <td style="color:#5b5b5b">' . $reportingmanagername . '</td>
                  </tr>
                </tbody></table>';
				}
				$options ['cron'] = 'yes';
				$result = sapp_Global::_sendEmail ( $options );
			}
	}


	public function indexAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		$leaverequestmodel = new Default_Model_Leaverequest ();
		$call = $this->_getParam ( 'call' );
		if ($call == 'ajaxcall')
			$this->_helper->layout->disableLayout ();

		$view = Zend_Layout::getMvcInstance ()->getView ();

		$employeeleavetypemodel = new Default_Model_Employeeleavetypes ();
		$compoffrequestmodel = new Default_Model_Compoffrequest();
		$totalLeaveTypes = $employeeleavetypemodel->getactiveleavetype ();
		$compOffLeaveTypeId = $employeeleavetypemodel->getEmployeeLeaveIdByLeaveType("Compensatory off");

		$objname = $this->_getParam ( 'objname' );
		$refresh = $this->_getParam ( 'refresh' );
		$dashboardcall = $this->_getParam ( 'dashboardcall' );
		$flag = $this->_request->getParam ( 'flag' );
		if($flag == 'sendemailcancelconfirmation'){
			$this->sendemailcancelconfirmation();
		}
		if ($this->_request->getParams () ['leavetypeid']) {
			$leavetypeid = $this->_request->getParams () ['leavetypeid'];
		} else {
			$leavetypeid = $totalLeaveTypes [0] ['id'];
		}
		if (! empty ( $flag ) && $flag == 'delete') {
			$this->_helper->layout->disableLayout ();
			$this->deleteAction ();
		} else {
			$data = array ();
			$searchQuery = '';
			$searchArray = array ();
			$tablecontent = '';

			if ($refresh == 'refresh') {
				if ($dashboardcall == 'Yes')
					$perPage = DASHBOARD_PERPAGE;
				else
					$perPage = PERPAGE;

				$sort = 'DESC';
				$by = 'modifieddate';
				$pageNo = 1;
				$searchData = '';
			} else {
				$sort = ($this->_getParam ( 'sort' ) != '') ? $this->_getParam ( 'sort' ) : 'DESC';
				$by = ($this->_getParam ( 'by' ) != '') ? $this->_getParam ( 'by' ) : 'modifieddate';
				if ($dashboardcall == 'Yes')
					$perPage = $this->_getParam ( 'per_page', DASHBOARD_PERPAGE );
				else
					$perPage = $this->_getParam ( 'per_page', PERPAGE );
				$pageNo = $this->_getParam ( 'page', 1 );
				// search from grid - START
				$searchData = $this->_getParam ( 'searchData' );
				$searchData = rtrim ( $searchData, ',' );
				// search from grid - END
			}
			$leavesArray = array (
					'pendingleaves',
					'cancelledleaves',
					'approvedleaves',
					'rejectedleaves',
					'total'
			);
			$objName = 'pendingleaves';
			$queryflag = 'all';
			if (! empty ( $flag )) {
				if (in_array ( $flag, $leavesArray )) {
					$objName = $flag;
					$queryflag = substr ( $flag, 0, - 6 );
				}
				if ($flag == 'total') {
					$queryflag = 'all';
				}
			}
			try {
				if ($leavetypeid != $compOffLeaveTypeId)
					$dataTmp = $leaverequestmodel->getGrid ( $sort, $by, $perPage, $pageNo, $searchData, $call, $dashboardcall, $objName, $queryflag, '', '', $leavetypeid );
				else
					$dataTmp = $compoffrequestmodel->getGrid($sort, $by, $perPage, $pageNo, $searchData, $call, $dashboardcall, $objName, $queryflag, '', '', $leavetypeid);
			} catch ( Exception $e ) {
				echo $e;
			}

			if ($leavetypeid == $compOffLeaveTypeId)
				$leavesCountArray = sapp_Helper::getEmployeeCompoffCountByCategory ( $loginUserId, $leavetypeid );
			else 
				$leavesCountArray = sapp_Helper::getLeavesCountByCategory ( $loginUserId, $leavetypeid );
			
			$compoffsCountArray = sapp_Helper::getEmployeeCompoffCountByCategory ( $loginUserId, $leavetypeid );

			array_push ( $data, $dataTmp );
			$this->view->dataArray = $data;
			$this->view->call = $call;
			$this->view->objName = 'all';
			$this->view->flag = $flag;
			$this->view->leavesCountArray = $leavesCountArray;
			$this->view->totalLeaveTypes = $totalLeaveTypes;
			$this->view->currentleavetypeid = $leavetypeid;
			$this->view->compoffid = $compOffLeaveTypeId;
			$this->view->totalcompoffs = $compoffsCountArray['total'];
			$this->view->messages = $this->_helper->flashMessenger->getMessages ();
		}
	}
	public function viewAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
		}
		$leaverequestmodel = new Default_Model_Leaverequest ();
		$employeeleavetypemodel = new Default_Model_Employeeleavetypes ();
		$compoffrequestmodel = new Default_Model_Compoffrequest();
		$compOffLeaveTypeId = $employeeleavetypemodel->getEmployeeLeaveIdByLeaveType("Compensatory off");
		$id = $this->getRequest ()->getParam ( 'id' );
		$type = $this->getRequest()->getParams()['type'];

		try {
			if($type == $compOffLeaveTypeId)
				$useridArr = $compoffrequestmodel->getUserID ( $id );
			else
				$useridArr = $leaverequestmodel->getUserID ( $id );

			if (! empty ( $useridArr )) {
				$user_id = $useridArr [0] ['user_id'];
				if ($user_id == $loginUserId) {
					$callval = $this->getRequest ()->getParam ( 'call' );
					if ($callval == 'ajaxcall')
						$this->_helper->layout->disableLayout ();
					$objName = 'pendingleaves';
					$leaverequestform = new Default_Form_leaverequest ();
					$leaverequestform->removeElement ( "submit" );
					$elements = $leaverequestform->getElements ();
					if (count ( $elements ) > 0) {
						foreach ( $elements as $key => $element ) {
							if (($key != "Cancel") && ($key != "Edit") && ($key != "Delete") && ($key != "Attachments")) {
								$element->setAttrib ( "disabled", "disabled" );
							}
						}
					}
					if($type == $compOffLeaveTypeId)
						$data = $compoffrequestmodel->getCompOffById ( $id );
					else
						$data = $leaverequestmodel->getsinglePendingLeavesData ( $id );
					$data = $data [0];
					if (! empty ( $data )) {
						$employeeleavetypemodel = new Default_Model_Employeeleavetypes ();
						$usersmodel = new Default_Model_Users ();
						if($type != $compOffLeaveTypeId)
							$employeeleavetypeArr = $employeeleavetypemodel->getsingleEmployeeLeavetypeData ( $data ['leavetypeid'] );
						if ($employeeleavetypeArr != 'norows') {
							$leaverequestform->leavetypeid->addMultiOption ( $employeeleavetypeArr [0] ['id'], utf8_encode ( $employeeleavetypeArr [0] ['leavetype'] ) );
							$data ['leavetypeid'] = $employeeleavetypeArr [0] ['leavetype'];
						} else {
							$data ['leavetypeid'] = "...";
						}

						if ($data ['leaveday'] == 1) {
							$leaverequestform->leaveday->addMultiOption ( $data ['leaveday'], 'Full Day' );
							$data ['leaveday'] = 'Full Day';
						} else {
							$leaverequestform->leaveday->addMultiOption ( $data ['leaveday'], 'Half Day' );
							$data ['leaveday'] = 'Half Day';
						}

						$repmngrnameArr = $usersmodel->getUserDetailsByID ( $data ['rep_mang_id'], 'all' );
						$leaverequestform->populate ( $data );

						$from_date = sapp_Global::change_date ( $data ["from_date"], 'view' );
						$to_date = sapp_Global::change_date ( $data ["to_date"], 'view' );
						$appliedon = sapp_Global::change_date ( $data ["createddate"], 'view' );
						
						$leaverequestform->from_date->setValue ( $from_date );
						$leaverequestform->to_date->setValue ( $to_date );
						$leaverequestform->createddate->setValue ( $appliedon );
						$leaverequestform->appliedleavesdaycount->setValue ( $data ['appliedleavescount'] );
						if (! empty ( $repmngrnameArr )) {
							$leaverequestform->rep_mang_id->setValue ( $repmngrnameArr [0] ['userfullname'] );
							$data ['rep_mang_id'] = $repmngrnameArr [0] ['userfullname'];
						} else {
							$leaverequestform->rep_mang_id->setValue ( '' );
							$data ['rep_mang_id'] = $repmngrnameArr [0] ['userfullname'];
						}

						// to show Leave Request history in view
						$leaverequesthistory_model = new Default_Model_Leaverequesthistory ();
						$leave_history = $leaverequesthistory_model->getLeaveRequestHistory ( $id );
						$this->view->leave_history = $leave_history;
						// end
						$leaverequestform->setDefault ( 'leavetypeid', $data ['leavetypeid'] );
						$leaverequestform->setDefault ( 'leaveday', $data ['leaveday'] );
						$this->view->controllername = $objName;
						
						if($type == $compOffLeaveTypeId)
						{
							$data['reason'] = $data['user_comments'];
							$data['leavetypeid'] = 'Compensatory Off';
							$leaverequestform->setDefault ( 'leaveday', $data['appliedleavescount']=='1'?'Full Day':'Half Day' );
							$leaverequestform->setDefault ( 'from_date', $data['leave_date'] );
							$leaverequestform->setDefault ( 'to_date', $data['leave_date'] );
							$data['leavestatus'] = $data['status'];
							$leaverequestform->setDefault ( 'appliedleavesdaycount', $data['appliedleavescount'] );
						}
						
						$this->view->id = $id;
						$this->view->form = $leaverequestform;
						$this->view->data = $data;
						$this->view->reportingmanagerStatus = (! empty ( $repmngrnameArr )) ? $repmngrnameArr [0] ['isactive'] : '';
					}

					else {
						$this->view->rowexist = "rows";
					}
				} else {
					$this->view->rowexist = "rows";
				}
			} else {
				$this->view->rowexist = "norows";
			}
		} catch ( Exception $e ) {
			$this->view->rowexist = "norows";
		}
	}
	public function customAction() {
		$leaveId = $this->_request->getParam ( 'leaveId' );
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
			$loginUserEmail = $auth->getStorage ()->read ()->emailaddress;
			$loginUserName = $auth->getStorage ()->read ()->userfullname;
		}
		if ($leaveId) {
			$leaverequestmodel = new Default_Model_Leaverequest ();
			$usersmodel = new Default_Model_Users ();
			$employeesmodel = new Default_Model_Employees ();
			$employeeleavetypesmodel = new Default_Model_Employeeleavetypes ();

			$data = $leaverequestmodel->getsinglePendingLeavesData ( $leaveId );
			$leaveOwner = $usersmodel->getUserDetailsByID ( $data ['0'] ['user_id'] ) ['0'];
			$data = $data [0];
			$repmngrnameArr = $usersmodel->getUserDetailsByID ( $data ['rep_mang_id'] );
			$leavetypeArr = $employeeleavetypesmodel->getLeavetypeDataByID ( $data ['leavetypeid'] );

			/* Mail to Reporting Manager */
			$ownerFullName = $leaveOwner ['userfullname'];
			$message = $leaveOwner ['userfullname'] . ' has the following leaves pending for approval.';

			$options ['subject'] = 'Leave approval reminder for ' . $leaveOwner ['userfullname'];
			$options ['header'] = 'Leave Approval Reminder';
			$options ['toEmail'] = $repmngrnameArr [0] ['emailaddress'];
			$options ['cc'] = $leaveOwner ['emailaddress'];
			$options ['toName'] = $repmngrnameArr [0] ['userfullname'];

			$options ['message'] = '<div>
										<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
											<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
												<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
									<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">' . $message . '.</div>
											</div>
									<div style = "padding : 8px 50px 0px 30px">
											<table width="100%" cellspacing="0" cellpadding="8" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:30px 0 30px 0;" bgcolor="#ffffff">
												<tbody>
													<tr bgcolor="#045396">
														<td width="28%" style="border-right:2px solid #BBBBBB; color:#ffffff">Leave Type</td>
														<td width="72%" style="color:#ffffff">' . $leavetypeArr [0] ['leavetype'] . '</td>
													</tr>
													<tr bgcolor="#F5F5F3">
														<td style="border-right:2px solid #BBBBBB; color:#5b5b5b">No. of Day(s)</td>
														<td style="color:#5b5b5b">' . $data ['appliedleavescount'] . '</td>
													</tr>
													<tr bgcolor="#D9DCE5">
														<td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Applied on</td>
														<td style="color:#5b5b5b">' . $data ['createddate'] . '</td>
													</tr>
													<tr bgcolor="#F5F5F3">
														<td style="border-right:2px solid #BBBBBB; color:#5b5b5b">From</td>
														<td style="color:#5b5b5b">' . $data ['from_date'] . '</td>
													</tr>
													<tr bgcolor="#D9DCE5">
														<td style="border-right:2px solid #BBBBBB; color:#5b5b5b">To</td>
														<td style="color:#5b5b5b">' . $data ['to_date'] . '</td>
													</tr>
													<tr bgcolor="#F5F5F3">
														<td style="border-right:2px solid #BBBBBB; color:#5b5b5b">Reason for Leave</td>
														<td style="color:#5b5b5b">' . $data ['reason'] . '</td>
													</tr>
												</tbody>
											</table>
									</div>
            						<div style="padding:20px 0 10px 5px;font-family:Verdana;font-size:14px;">Please <a href="' . BASE_URL . '/index/popup" target="_blank" style="color:#b3512f;">click here</a> to login and check the leave details.</div>
            								</div>
            								</div>';
			$result = sapp_Global::_sendEmail ( $options );

			if ($_SESSION ['reminderArray'] != null) {
				array_push ( $_SESSION ['reminderArray'], $leaveId );
			} else {
				$reminderArray = array ();
				array_push ( $reminderArray, $leaveId );
				$_SESSION ['reminderArray'] = $reminderArray;
			}
		}
		/* End */
	}
	public function deleteAction() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$loginUserId = $auth->getStorage ()->read ()->id;
			$loginUserEmail = $auth->getStorage ()->read ()->emailaddress;
			$loginUserName = $auth->getStorage ()->read ()->userfullname;
		}
		$employeeleavetypemodel = new Default_Model_Employeeleavetypes ();
		$compOffLeaveTypeId = $employeeleavetypemodel->getEmployeeLeaveIdByLeaveType("Compensatory off");
		$wfhLeaveTypeId = $employeeleavetypemodel->getEmployeeLeaveIdByLeaveType("Work from Home");
		$currentLeaveType = $_SERVER['HTTP_REFERER'];
		if (strpos( $currentLeaveType, 'compoff' ) !== false)
			$type = 'compoff';
		if (strpos( $currentLeaveType, 'leavetypeid' ) !== false)
		{
			$leavtypeid = substr(strrchr($currentLeaveType, 'leavetypeid'),12);
		}
		$id = $this->_request->getParam ( 'objid' );
		$messages ['message'] = '';
		$actionflag = 5;
		$businessunitid = '';
		$leavetypetext = '';
		if ($id) {
			$leaverequestmodel = new Default_Model_Leaverequest ();
			$usersmodel = new Default_Model_Users ();
			$employeesmodel = new Default_Model_Employees ();
			$employeeleavetypesmodel = new Default_Model_Employeeleavetypes ();
			if (($leavtypeid != $compOffLeaveTypeId) && ($type != 'compoff'))
			{
				$msgstring = 'Leave';
				$data = $leaverequestmodel->getsinglePendingLeavesData ( $id );
				$leaveOwner = $usersmodel->getUserDetailsByID ( $data ['0'] ['user_id'] ) ['0'];
				$data = $data [0];
				$leavetypeArr = $employeeleavetypesmodel->getLeavetypeDataByID ( $data ['leavetypeid'] );
				if ($leavetypeArr['0']['leavetype'] == 'Compensatory off') {
							$messages ['message'] = 'Compensatory Offs cannot be modified here.';
							$messages ['msgtype'] = 'error';
							$this->_helper->json ( $messages );
							return false;
				}
				if ($data ['leavestatus'] == 'Approved' && $loginUserId == $leaveOwner ['id']) {
					if (isset ( $data ['from_date'] )) {
						$leaveDate = date ( $data ['from_date'] );
						$todayDate = date ( "Y-m-d" );
						if (strtotime ( $todayDate ) >= strtotime ( $leaveDate )) {
							$messages ['message'] = 'Leave request cannot be cancelled';
							$messages ['msgtype'] = 'error';
							$this->_helper->json ( $messages );
							return false;
						}
					}
				}
				if ($data ['leavestatus'] == 'Rejected' || $data ['leavestatus'] == 'Cancelled') {
					$messages ['message'] = 'Leave request cannot be cancelled';
					$messages ['msgtype'] = 'error';
					$this->_helper->json ( $messages );
					return false;
				}
				
				$loggedInEmployeeDetails = $employeesmodel->getLoggedInEmployeeDetails ( $loginUserId );
				if ($loggedInEmployeeDetails [0] ['businessunit_id'] != '')
					$businessunitid = $loggedInEmployeeDetails [0] ['businessunit_id'];
				
					$dataarr = array (
							'leavestatus' => 4,
							'modifieddate' => gmdate ( "Y-m-d H:i:s" ),
							'modifiedby' => $loginUserId
					);
					$where = array (
							'id=?' => $id
					);
				
					if ($data ['parent'] != null) {
						$dataarr ['isactive'] = 0;
					}
				
					$Id = $leaverequestmodel->SaveorUpdateLeaveRequest ( $dataarr, $where );
					 if ($Id == 'update' && $data['leavetypeid'] == $wfhLeaveTypeId)
					{
						$officeHourModel = new Default_Model_OfficeHours();
						$officeHourule = $officeHourModel->getOfficeHoursRule($businessunitid);
						$officeStartTime = $officeHourule['start_time'];
						$officeEndTime = $officeHourule['end_time'];
						$update = $leaverequestmodel->deleteEntriesForCancelledWFH($data['user_id'],$data['from_date'],$data['to_date'],$officeStartTime,$officeEndTime);
					}
					if (! empty ( $leavetypeArr )) {
						if ($leavetypeArr [0] ['leavepredeductable'] == 1) {
							$updateemployeeleave = $leaverequestmodel->updatecancelledemployeeleaves ( $data ['appliedleavescount'], $data ['user_id'], $data ['leavetypeid'] );
						}
					}
				
					if ($data ['parent'] != null) {
						$parentLeaveReqData = $leaverequestmodel->getParentLeaveRequestDetails ( $data ['parent'] );
						$parentLeaveData = $employeeleavetypesmodel->getLeavetypeDataByID ( $parentLeaveReqData [0] ['leavetypeid'] );
						$pData = array (
								'isactive' => 1
						);
						$pWhere = array (
								'id=?' => $parentLeaveReqData [0] ['id']
						);
				
						$leaverequestmodel->SaveorUpdateLeaveRequest ( $pData, $pWhere );
				
						if ($parentLeaveData [0] ['leavepredeductable'] == 1) {
							$updateemployeeleave = $leaverequestmodel->updateemployeeleaves ( $parentLeaveReqData [0] ['appliedleavescount'], $parentLeaveReqData [0] ['user_id'], $parentLeaveReqData [0] ['leavetypeid'] );
						}
					}
					// saving in leaverequest history table
					$history = 'Leave Request has been Cancelled by ';
				
					$leaverequesthistory_model = new Default_Model_Leaverequesthistory ();
					$leave_history = array (
							'leaverequest_id' => $id,
							'description' => $history,
							// 'emp_name' => ucfirst($auth->getStorage()->read()->userfullname),
							'createdby' => $loginUserId,
							'modifiedby' => $loginUserId,
							'isactive' => 1,
							'createddate' => gmdate ( "Y-m-d H:i:s" ),
							'modifieddate' => gmdate ( "Y-m-d H:i:s" )
					);
					$where = '';
					$leavehistory = $leaverequesthistory_model->saveOrUpdateLeaveRequestHistory ( $leave_history, $where );
					// end
					// $data = $leaverequestmodel->getsinglePendingLeavesData($id);
					// $data = $data[0];
					$appliedleavesdaycount = $data ['appliedleavescount'];
					$to_date = $data ['to_date'];
					$from_date = $data ['from_date'];
					$reason = $data ['reason'];
					$leavetypeid = $data ['leavetypeid'];
					$repmngrnameArr = $usersmodel->getUserDetailsByID ( $data ['rep_mang_id'] );
					$reportingmanageremail = $repmngrnameArr [0] ['emailaddress'];
					$reportingmanagername = $repmngrnameArr [0] ['userfullname'];
					$messages ['message'] = $msgstring.' request cancelled succesfully.';
					$messages ['msgtype'] = 'success';
			}
			else
			{
				$msgstring = 'Comp-Off';
				$compoffrequestmodel = new Default_Model_Compoffrequest();
				$compoffdata = $compoffrequestmodel->getCompOffById( $id );
				//$leaveOwner = $usersmodel->getUserDetailsByID ( $compoffdata ['0'] ['user_id'] ) ['0'];
				if ($compoffdata['0'] ['status'] == 'Approved' && $loginUserId == $compoffdata ['0'] ['user_id']) {
					$messages ['message'] = $msgstring.' request cannot be cancelled';
					$messages ['msgtype'] = 'error';
					$this->_helper->json ( $messages );
					return false;
				}
				if (($compoffdata ['0'] ['status'] == 'Rejected' || $compoffdata ['0'] ['status'] == 'Cancelled')&& $loginUserId == $compoffdata ['0'] ['user_id']) {
					$messages ['message'] = $msgstring.' request cannot be cancelled';
					$messages ['msgtype'] = 'error';
					$this->_helper->json ( $messages );
					return false;
				}
				if ($compoffdata ['0'] ['status'] == 'Cancelled') {
					$messages ['message'] = $msgstring.' request is already cancelled';
					$messages ['msgtype'] = 'error';
					$this->_helper->json ( $messages );
					return false;
				}
				$datacmp = array (
						'status' => 4,
						'modifieddate' => gmdate ( "Y-m-d H:i:s" ),
						'modifiedby' => $loginUserId
				);
				$where = array (
						'id=?' => $id
				);
				
				if ($data ['parent'] != null) {
					$dataarr ['isactive'] = 0;
				}
				
				$Id = $compoffrequestmodel->SaveorUpdateCompoffRequest( $datacmp, $where );
				
				if ($Id == 'update') {
					$getleaveid = $leaverequestmodel->getLeaveID($compoffdata['0'] ['user_id'],$compOffLeaveTypeId,$compoffdata['0'] ['leave_date']);
					if ($getleaveid != null)
					{
						$data = array (
								'leavestatus' => 4,
								'modifieddate' => gmdate ( "Y-m-d H:i:s" ),
								'modifiedby' => $loginUserId
						);
						$where = array (
								'id=?' => $getleaveid['0']['id']
						);
						$leaveid = $leaverequestmodel->update($data, $where);
					}
					$menuID = PENDINGLEAVES;
					$this->sendemailcancelconfirmation();
					$result = sapp_Global::logManager ( $menuID, $actionflag, $loginUserId, $id );
					$this->_helper->json ( array (
							'result' => 'saved',
							'message' => $msgstring.' request cancelled succesfully.',
							'msgtype' => 'success',
							'controller' => 'pendingleaves',
							'leaverequest' => $data
					) );
				} else {
					$messages ['message'] = $msgstring.' request cannot be cancelled';
					$messages ['msgtype'] = 'error';
				}
			}
		} else {
			$messages ['message'] = $msgstring.' request cannot be cancelled';
			$messages ['msgtype'] = 'error';
		}
		$this->_helper->json ( $messages );
	}
}
