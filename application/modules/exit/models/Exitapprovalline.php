<?php
/********************************************************************************* 

 *  Copyright (C) 2014 Sapplica
 *   

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/
class Exit_Model_Exitapprovalline extends Zend_Db_Table_Abstract
{
	protected $_name = 'main_exit_approval_line';
	private $db;


	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function SaveorUpdateExitApprovalLine($data, $where) {
		if ($where != '') {
			$this->update ( $data, $where );
			return 'update';
		} else {
			$this->insert ( $data );
			$id = $this->getAdapter ()->lastInsertId ( 'main_exit_approval_line' );
			return $id;
		}
	}
	
	public function getExitApprovalLines($bunit_id,$dept_id)
	{
		$where = 'e.isactive = 1';
	
		if($bunit_id)
			$where .= ' AND e.businessunit_id = '.$bunit_id;
		
		if($dept_id)
		 $where .= ' AND e.department_id = '.$dept_id; 
			
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "SELECT jobtitlename FROM main_jobtitles as j INNER JOIN main_exit_approval_line as e on j.id = e.approvalline where ".$where."";
		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
	
	public function getemployeesListWithSpecificUserList($jobtitle)
	{
		$where = 'e.isactive = 1';
		
		$db = Zend_Db_Table::getDefaultAdapter ();
		$query = "SELECT userfullname FROM main_users as u INNER JOIN main_jobtitles as j on u.jobtitle_id = j.id where jobtitlename = '".$jobtitle."'";
		$result = $db->query ( $query )->fetchAll ();
		return $result;
	}
}
?>