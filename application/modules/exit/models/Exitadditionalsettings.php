<?php
/********************************************************************************* 

 *  Copyright (C) 2014 Sapplica
 *   

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/
class Exit_Model_Exitadditionalsettings extends Zend_Db_Table_Abstract
{
	protected $_name = 'main_exit_additional_settings';
	private $db;


	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function saveExitAdditionalSettings($data,$where = '', $con = '')
	{
		if($con == 'add' && !empty($data))
		{
			$this->insert($data);
			$id = $this->getAdapter()->lastInsertId($this->_name);
			return $id;
		}
		else if($con == 'edit' && !empty($where))
		{
			$this->update($data,$where);
			return 'update';
		}
		else if($con == 'edit' && empty($where))
		{
			$this->insert($data);
			$id = $this->getAdapter()->lastInsertId($this->_name);
			return $id;
		}
	}
	
	public function getSettings($businessunit_id,$department_id)
	{
		$res = $this->select()
		->setIntegrityCheck(false)
		->from(array('epaConfig' => 'main_exit_additional_settings'),'epaConfig.*')
		->where('epaConfig.isactive = 1 AND epaConfig.businessunit_id = '.$businessunit_id.' AND epaConfig.department_id = '.$department_id);
	
		return $this->fetchAll($res)->toArray();
	}
	
	public function deleteApprovalLine($businessunit_id,$department_id,$appline_id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		if(empty($department_id))
			$query = "DELETE FROM main_exit_additional_settings WHERE businessunit_id=$businessunit_id and approval_line=(SELECT id FROM main_jobtitles where jobtitlename = '".$appline_id."')";
		else
			$query = "DELETE FROM main_exit_additional_settings WHERE businessunit_id=$businessunit_id and department_id=$department_id and approval_line=(SELECT id FROM main_jobtitles where jobtitlename = '".$appline_id."')";
		$data = $db->query($query)->execute();
        return $data;
	}
	
	public function isaplexists($businessunit_id,$department_id,$appline_id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$query = "SELECT count(*) FROM main_exit_additional_settings where businessunit_id=$businessunit_id and department_id = $department_id and approval_line = $appline_id";
		$data = $db->query($query)->fetchAll();
		return $data[0]['count(*)'];
	}
	
	public function emptyAdditionalSettings($businessunit_id,$department_id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$query = "DELETE FROM main_exit_additional_settings WHERE businessunit_id=$businessunit_id and department_id=$department_id ";
		$data = $db->query($query)->execute();
		return $data;
	}
}
?>