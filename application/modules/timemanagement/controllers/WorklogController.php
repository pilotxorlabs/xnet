<?php
class Timemanagement_WorklogController extends Zend_Controller_Action
{
	private $options;
	public function preDispatch()
	{
		if(!sapp_Helper::checkTmEnable())
			$this->_redirect('error');
	}

	public function init()
	{
		$this->_options= $this->getInvokeArg('bootstrap')->getOptions();

	}
	public function indexAction()
	{
		$usersModel = new Timemanagement_Model_Users();
		$worklogs = new Api_Model_Worklogs();
		$success = $this->_request->getParam('success');

		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();

		$selYrMon = $this->_getParam('selYrMon');
		$flag = $this->_getParam('flag');
		
		$approvedAlert = $usersModel->getEmpApprovalStatusDteails($data->id);
		
		$empDoj = $usersModel->getEmployeeDoj($data->id);
		
		$doj_date = strtotime($empDoj['date_of_joining']);
		$created_date = strtotime($empDoj['createddate']);
		
		if($created_date > $doj_date)
		{
		    $this->view->empDoj=$empDoj['date_of_joining'];
		    $dateEmpDoj = date('Y-m',strtotime($empDoj['date_of_joining']));
		}
		else{
		    $this->view->empDoj=$empDoj['createddate'];
		    $dateEmpDoj = date('Y-m',strtotime($empDoj['createddate']));
		}
		

		$now = new DateTime();

		if($selYrMon != '') {
			$selYrMon = date('Y-m',strtotime($selYrMon));
			if($flag == 'next') {
				if($selYrMon < $now->format('Y-m'))
				$selYrMon = date_create($selYrMon.'next month')->format('Y-m');
			}else if($flag == 'pre'){
				if($selYrMon > $dateEmpDoj)
				$selYrMon = date_create($selYrMon.'last month')->format('Y-m');
				else
				$selYrMon  = $dateEmpDoj;
			}
		}

		$selYrMon = ($selYrMon != '')?$selYrMon:$now->format('Y-m');

		$yrMon = explode('-', $selYrMon);
		$empHolidaysWeekendsData = $usersModel->getEmployeeHolidaysNWeekends($data->id, $yrMon[0],$yrMon[1]);
		$firstday = $yrMon[0]."-".$yrMon[1].'-01';
		$noOfDaysMonth = date("t", mktime(0, 0, 0, $yrMon[1], 1, $yrMon[0]));
		$lastday =   $yrMon[0]."-".$yrMon[1]."-".$noOfDaysMonth;

		$empLeavesData = $usersModel->getEmpLeaves($data->id,$firstday,$lastday,'all');

		$timeLoggedData = $worklogs->getTimeDetails($data->id, $firstday, $lastday);

		$this->view->empHolidaysWeekends = $empHolidaysWeekendsData[0];
		$this->view->selYrMon =  $selYrMon;
		$this->view->leavesData = $empLeavesData;
		$this->view->empWorkLogData = $timeLoggedData;
		$this->view->success = $success;		
		$this->view->delete = $this->_request->getParam('delete');

	}

	public function worklogAction()
	{
		$worklogs = new Api_Model_Worklogs();
		$employeesModel = new Default_Model_Employees ();
		$employeeholidaysModel = new Default_Model_Holidaydates ();
		$leaveRequestModel = new Default_Model_Leaverequest();

		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
		{
			$loginUserId = $auth->getStorage()->read()->id;
		}
		$now = new DateTime();
		$selDate ='';
		$selYrMon = $this->_getParam('selYrMon');

		if($selYrMon != '')
		{
			$selDay = $this->_getParam('day');
			if($selDay != '')
			{
				$selDate =$selYrMon."-".$selDay;
			}
		}
		if($selDate != '')
		{
			$date = new DateTime($selDate);
		}
		else
		{
			$date = $now;
		}

		$pathinfo = $this->getRequest()->getRequestUri();
		$uri = explode('/', $pathinfo);
		$logdate = $selYrMon ."-".$uri[count($uri)-1];
		$workDate = str_replace('-','/',date("d-m-Y", strtotime($logdate)));
		$workDateForView = str_replace('-','/',date("m-d-Y", strtotime($logdate)));
		$this->view->selDate = $date->format('d M Y');
		$this->view->selYrMon = $date->format('Y-m');
		$this->view->workDate = $workDateForView;
		$logdate = str_replace('/', '-', $workDate);
		$logDate = date("Y-m-d", strtotime($logdate));
		$logsExist = $worklogs->checkLogsAlreadyExist($loginUserId, $logDate);

		$empProjects = $worklogs->empProjects($loginUserId);
		$empTasks = $worklogs->empTasks($loginUserId);
		$activityCode = $worklogs->getActivityCode();
		$projectTasksMap = array();
		for($i = 0; $i < count ( $empProjects ); $i ++){
		    $projectId = $worklogs->getProjectIdByProjectName($empProjects[$i]['project_name']);
		    $employeeTasks = $worklogs->getTaskByProject($loginUserId,$projectId[0]['id']);
		    $projectTasksMap[$empProjects[$i]['project_name']] = array();
		    for($j = 0; $j < count ( $employeeTasks ); $j ++){
		        if (! in_array ($employeeTasks[$j]['task'] , $projectTasksMap[$empProjects[$i]['project_name']]))
		        {
		            array_push($projectTasksMap[$empProjects[$i]['project_name']], $employeeTasks[$j]['task']);
		       }		       
		    }
		}
		
		if(!empty($logsExist))
		{
			$filledEmpProjects = array ();
			$filledEmpTasks = array();
			$filledEmpActivityCodes = array();
			$internalId = array();
			$externalId = array();
			$summary = array();
			$workhours = array();
			foreach ($logsExist as $logs)
			{
				$projectName = $worklogs->getProjectNameByProjectId($logs['project_id']);
				if (!$this->isCotained($projectName[0], $empProjects, 'project_name' ))
				{
					array_push ($empProjects, $projectName[0] );
				}
				array_push ($filledEmpProjects, $projectName[0] );

				$taskName = $worklogs->getTaskNameByTaskId($logs['task_id']);
				if (! $this->isCotained($taskName[0], $empTasks, 'task' ))
				{
					array_push ($empTasks, $taskName[0] );
				}
				array_push ($filledEmpTasks, $taskName[0] );

				$activityCodes = $worklogs->getActivityCodeByActivityId($logs['activity_id']);
				if (! $this->isCotained($activityCodes[0], $activityCode, 'activity_code' ))
				{
					array_push ($activityCode, $activityCodes[0] );
				}
				$logs['summary'] = trim(preg_replace('/\s+/',' ', $logs['summary']));
				$logs['summary'] = htmlspecialchars($logs['summary']);
				$logs['work_id_1'] = trim(preg_replace('/\s+/',' ', $logs['work_id_1']));
				$logs['work_id_2'] = trim(preg_replace('/\s+/',' ', $logs['work_id_2']));
				array_push ($filledEmpActivityCodes, $activityCodes[0] );
				array_push ($internalId, $logs['work_id_1'] );
				array_push ($externalId, $logs['work_id_2'] );
				array_push ($summary, $logs['summary'] );
				array_push ($workhours, $logs['work_hours'] );
			}
			$this->view->internalId = $internalId;
			$this->view->externalId = $externalId;
			$this->view->summary = $summary;
			$this->view->workhours = $workhours;
			$this->view->filledEmpProjects = $filledEmpProjects;
			$this->view->filledEmpTasks = $filledEmpTasks;
			$this->view->filledEmpActivityCodes = $filledEmpActivityCodes;


		}

			$this->view->empProjects = $empProjects;
			$this->view->empTasks = $empTasks;
			$this->view->activityCode = $activityCode;
			$this->view->empProjectTaskMap = $projectTasksMap;

 		$this->view->empId = $loginUserId;
 		$this->view->logExists = $logsExist;
        $this->view->logdate = $logdate;
 		$isEditable = $worklogs->isLocked($logdate, $loginUserId);
 		$this->view->isEditable = $isEditable;
        $this->view->loginUserId=$loginUserId;
        $exceptionModel = new Default_Model_ExceptionRequest ();
        $myattendancemodel = new Default_Model_Myattendance ();
        $officeHoursModel = new Default_Model_OfficeHours ();
        $activityModel= new Default_Model_EmployeeWorkActivity();
        $employeeModel= new Default_Model_Employee();
        $employeeInfo = $employeeModel->getActiveEmployeeData ( $loginUserId );
        if($employeeInfo [0] != null && $employeeInfo [0] ['businessunit_id'] != null) {
            $officeHoursRule = $officeHoursModel->getOfficeHoursRule($employeeInfo [0] ['businessunit_id']);
        }
        $dailyActivity = array ();
        $timezone = $myattendancemodel->getCurrentTimeZone ();
        $modifier = $myattendancemodel->getModifierStringFromOfffset ( $timezone [0] ['offet_value'] );
        //Check punch in office hours window
        $activitiesData = $activityModel->getActivitiesBetween ( $loginUserId, $logdate, $officeHoursRule, $modifier );
        if(!empty($activitiesData))
            $punchActivity=true;
        else $punchActivity=false;
        $this->view->punchActivity=$punchActivity;
        //check Exceptions if exception exist
        $tempDate=date("Y-m-d", strtotime($logdate));
        $dayException=$exceptionModel->checkExceptionEntry($loginUserId, $tempDate);
        $this->view->dayException=$dayException;


	}

	/* public function isLocked($logdate, $loginUserId){
		$worklogs = new Api_Model_Worklogs();
		$employeesModel = new Default_Model_Employees ();
		$employeeholidaysModel = new Default_Model_Holidaydates ();
		$leaveRequestModel = new Default_Model_Leaverequest();

		//Check if worklog sheet for the selected date should be editable or not
		$isEditable = true;
		$lockingPeriod = $worklogs->getLockingPeriod();
		$workLogLockingPeriod = $lockingPeriod[0]['worklog_locking_period'];//Get this value using tm_configurations table
		$now = time();
		$toDate = date("Y-m-d",$now);
		$worklog_date = strtotime($logdate);
		$startDate = date("Y-m-d",$worklog_date);
		$weekendCount = $worklogs->getWeekends(new DateTime ( $startDate ), new DateTime ( $toDate ));
		$weekendArray = $worklogs->getWeekendsArray(new DateTime ( $startDate ), new DateTime ( $toDate ));
		$holidayGroup = $employeesModel->getHolidayGroupForEmployee ( $loginUserId );
		$holidays = $employeeholidaysModel->getHolidaysBetween ( $startDate, $toDate, $holidayGroup ['0'] ['holiday_group'] ) [0] ['holidaydates'];
		$holidaysArray = $employeeholidaysModel->getHolidaysArrayBetween ( $startDate, $toDate, $holidayGroup ['0'] ['holiday_group'] );
		$holidayWeekendUnion = $worklogs->array_union($weekendArray, $holidaysArray);
		$leavesArray = $leaveRequestModel->getApprovedLeavesArrayBetween($loginUserId, new DateTime($startDate), new DateTime($toDate));
		$holidayWeekendLeaveUnion = $worklogs->array_union($holidayWeekendUnion, $leavesArray);
		$additionaGraceDueToLeavesOrHoliday = count($holidayWeekendLeaveUnion);
		$datediff = $now - $worklog_date;
		$years = floor($datediff / (365*60*60*24));
		$months = floor(($datediff - $years * 365*60*60*24) / (30*60*60*24));
		$noOfDays = floor(($datediff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		switch($workLogLockingPeriod){
			case "Daily":
				if($noOfDays>(1+$additionaGraceDueToLeavesOrHoliday)){
					$isEditable = false;
				}
				break;
			case "Weekly":
				$dayOfWeek = (new DateTime($startDate))->format('w');
				$nofDaysToSunday = $dayOfWeek==0?0:(7-$dayOfWeek);
				$lastAllowedDate = strtotime($nofDaysToSunday.' days', $worklog_date);
				$datediff = $now - $lastAllowedDate;
				$noOfDays = round($datediff / (60 * 60 * 24));
				if($noOfDays>($additionaGraceDueToLeavesOrHoliday)){
					$isEditable = false;
				}
				break;
			case "Monthly":
				$yearLogDate = (new DateTime($startDate))->format('Y');
				$monthLogDate = (new DateTime($startDate))->format('m');
				$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $monthLogDate, $yearLogDate);
				$nofDaysToMonthEnd = $daysInMonth - $monthLogDate;
				$lastAllowedDate = strtotime($nofDaysToMonthEnd.' days', $worklog_date);
				$datediff = $now - $lastAllowedDate;
				$noOfDays = round($datediff / (60 * 60 * 24));
				if($noOfDays>1){
					$isEditable = false;
				}
				break;
		}
		return $isEditable;
	} */

	private function isCotained($associativeArray, $arrayOfArrays, $key){
		$isPresent = false;
		for($i=0; $i<count($arrayOfArrays); $i++){
			if($arrayOfArrays[$i][$key] == $associativeArray[$key]){
				$isPresent = true;
				break;
			}
		}
		return $isPresent;
	}

	public function submitworklogAction()
	{
		$worklogs_model = new Api_Model_Worklogs();
		$users_model = new Default_Model_Users();
    	$logs = $this->_request->getParam('logs');
    	$params = json_decode($logs,true);
    	if($params[0]['deleteLastLog']){
    	    try {
    	        $worklogDate = new DateTime($params[0]['work_date']);
    	        $worklogs_model->disableWorklogs($worklogDate->format(DATEFORMAT_PHP), $params[0]['empId']);  
    	        
    	        $messages['result']='deleted';
    	        $this->_helper->json($messages);
    	    } catch (Exception $e) {
    	        $messages['result']='Error: Unable to delete logs';
    	        $this->_helper->json($messages);
    	    }
    	}
    	$timeSheet = $this->_request->getParams('timeSheet');
    	try
    	{
    		$result = $worklogs_model->emplogs($logs, $timeSheet);
    		if($result == ''){
    			$configurationModel = new Timemanagement_Model_Configuration();
    			$checkWorklogMails = $configurationModel->checkWorklogMails();
    			if ($checkWorklogMails == 'Enabled')
    			{
    				$emailId = $users_model->getUserEmailId($params['0']['empId']);
    				$userName = $users_model->getUserFullName($params['0']['empId']);
    				$workAckDate = date('d-M-Y',strtotime($params[0]['work_date']));
    				$options ['subject'] = 'Worklog Submission Acknowledgement - '.$workAckDate;
    				$options ['header'] = 'Worklog - '.$workAckDate;
    				$options ['toEmail'] = $emailId;
    				$options ['toName'] = $userName;
    				$options ['message'] = '
    				<div style="margin:0px !important;padding:0px !important;">
							<div style = "overflow: auto;width:100%;border-radius: 15px;background-color:#eeeeee; ">
								<div style="overflow: auto;margin-top:-4px;width:100%;background-color:#5b5a55;border-radius:15px 15px 0px 0px;display: inline-block;">
									<div style="color:#ffffff;padding-left:8px;height:22px;font-family:Verdana;font-size:14px;">Hi,</div>
									<div style="color:#ffffff;padding-left:8px;height:35px;font-family:Verdana;font-size:14px;">Your worklog has been submitted successfully for '.$workAckDate.'.</div>
								</div>
    				<div style = "padding : 8px 50px 30px 30px">
                			<table width="100%" cellspacing="0" cellpadding="5" border="0" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; margin:20px 0 20px 0;" bgcolor="#ffffff">
	                      	<tbody>
		                      <tr bgcolor="#045396">
		                        <td width="1%" style="border-right:2px solid #BBBBBB; color:#ffffff"></td>
		                        <td width="1%" style="border-right:2px solid #BBBBBB; color:#ffffff">Project</td>
			    				<td width="1%" style="border-right:2px solid #BBBBBB; color:#ffffff">Task</td>
			    				<td width="2%" style="border-right:2px solid #BBBBBB; color:#ffffff">Internal ID</td>
			    				<td width="2%" style="border-right:2px solid #BBBBBB; color:#ffffff">External ID</td>
			    				<td width="20%" style="border-right:2px solid #BBBBBB; color:#ffffff">Summary</td>
			    				<td width="1%" style="border-right:2px solid #BBBBBB; color:#ffffff">Hours</td>
			    				<td width="3%" style="color:#ffffff">Activity Code</td>
		                      </tr>
		            ';
    				 
    				for($i=0;$i<sizeof($params);$i++)
    				{
    					if($i%2 == 0)
    						$options ['message'] .= '<tr bgcolor="#F5F5F3">';
    						else
    							$options ['message'] .= '<tr bgcolor="#D9DCE5">';
    							$sNo = ($i + 1).'.';
    							$options ['message'] .= '
    						<td style="border-right:2px solid #BBBBBB; color:#5b5b5b">'.$sNo.'</td>
    						<td style="border-right:2px solid #BBBBBB; color:#5b5b5b">'.$params[$i]['project_name'].'</td>
    						<td style="border-right:2px solid #BBBBBB; color:#5b5b5b">'.$params[$i]['task_name'].'</td>
    						<td style="border-right:2px solid #BBBBBB; color:#5b5b5b">'.$params[$i]['work_id_1'].'</td>
    						<td style="border-right:2px solid #BBBBBB; color:#5b5b5b">'.$params[$i]['work_id_2'].'</td>
    						<td style="border-right:2px solid #BBBBBB; color:#5b5b5b">'.$params[$i]['summary'].'</td>
    						<td style="border-right:2px solid #BBBBBB; color:#5b5b5b">'.$params[$i]['work_hours'].'</td>
    						<td style="color:#5b5b5b">'.$params[$i]['activity'].'</td>
    						</tr>';
    				}
    				 
    				$options ['message'] .= '</tbody>
                			</table>
		                    </div>
							</div>
		                      </div>';
    				$mail_id = sapp_Global::_sendEmail ( $options );
    			}
    			$messages['result']='Logs has been added successfully.';
    		}else{
    			$messages['result']= $result;
    		}
    		$this->_helper->json($messages);
    	}
    	catch(Exception $e)
    	{
    		$messages['result']='Error: '.$result;
    		$this->_helper->json($messages);
    	}
	}

	public function viewWorkLogAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
		{
			$loginUserId = $auth->getStorage()->read()->id;
		}
		$seldate = $this->_getParam('$date');
		$worklogs_model = new Api_Model_Worklogs();
		$data = '';
		$checkLogsExist = '';
		if($seldate != '')
		{
			$seldate = str_replace(' ','-',date("Y-m-d", strtotime($seldate)));
			$checkLogsExist = $worklogs_model->checkLogsAlreadyExist($loginUserId, $seldate);
		}

		$this->view->selDate = $date->format('d M Y');
		$this->view->selYrMon = $date->format('Y-m');
		$this->view->empId = $loginUserId;
		if(!empty($checkLogsExist))
		{
			$data = "[";
			for ($i=0; $i<count($checkLogsExist); $i++)
			{
				$data = $data."[";
				$project_id = $checkLogsExist[$i]['project_id'];
				if($project_id != '' || $project_id != null)
				{
					$project_names = $worklogs_model->getProjectNameByProjectId($project_id);
					$project_name = $project_names[0]['project_name'];
					$data = $data."'".$project_name."'";
				}

				$task_id = $checkLogsExist[$i]['task_id'];
				if($task_id != '' || $task_id != null)
				{
					$task_names = $worklogs_model->getTaskNameByTaskId($task_id);
				}

			}
			$data = $data."];";
		}
		$this->view->data = $checkLogsExist[$i]["project_id"];
	}
	public function checkedLastworklog($loginUserId,$prevDay,&$empdate = null)
		{
			$datebeforecurrentday = $prevDay->get ( 'YYYY-MM-dd' );
			$empTSModel = new Timemanagement_Model_MyTimesheet ();
			$worklogs = new Api_Model_Worklogs();
			$empWorkActivityModel = new Default_Model_EmployeeWorkActivity();
			$isEditable = $worklogs->isLocked($datebeforecurrentday, $loginUserId);
			$worklogcount = $empTSModel->getWorklogForDate ( $loginUserId, $datebeforecurrentday );
			$officeAttedance = $empWorkActivityModel->getActivitiesOn($loginUserId,new DateTime($datebeforecurrentday));
			if(!$isEditable){
				return ;
			}
			else {
			    if($worklogcount == 0 && (count($officeAttedance)) > 0){
					$empdate = $datebeforecurrentday;
					$_SESSION['worklogdate'] = $empdate;
				}
				$this->checkedLastworklog($loginUserId, $prevDay->subDay ( 1 ),$empdate);
			}

	}
	public function worklogreminderpopupAction() {
		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout->disableLayout ();
		$auth = Zend_Auth::getInstance ();
		$loginUserId = $auth->getStorage ()->read ()->id;
		$leaverequest = new Default_Model_Leaverequest();
		$empRole = $leaverequest->getEmployeeRole ( $loginUserId );
		unset($_SESSION ['worklogdate']);
		$date = new Zend_Date ();
		$datebeforecurrentday = $date->subDay ( 1 );
		if($empRole != 1)
		$this->checkedLastworklog ( $loginUserId, $datebeforecurrentday);
		if (isset ( $_SESSION ['worklogdate'] )) {
			$this->_helper->json(array("success",$_SESSION ['worklogdate'])) ;
		}
		$this->_helper->json( "No Reminder" );

	}
}
