<?php

class Api_Bootstrap extends Zend_Application_Module_Bootstrap
{
    protected function _initAppAutoload() {
        
        $auth= Zend_Auth::getInstance();
        $storage = $auth->getStorage()->read();
    }
    
    
    /** URL Masking */
    public function _initRoute()
    {
        $front = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();
        
        $attendancelogroute = new Zend_Rest_Route($front,array(), array(
            'api' => array('attendance')
        ));
        $router->addRoute('attendance',$attendancelogroute);
        
        $worklogsroute = new Zend_Rest_Route($front,array(), array(
        		'api' => array('worklogs')
        ));
        $router->addRoute('worklogs',$worklogsroute);
        
        
    }
}

