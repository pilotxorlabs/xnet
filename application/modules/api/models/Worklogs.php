<?php
class Api_Model_Worklogs extends Zend_Db_Table_Abstract
{
	protected $_name = 'tm_worklogs';
	protected $_primary = 'id';

	public function emplogs($logs, $timeSheet)
	{
		$result = "";
		$workid1 = '';
		$workid2 = '';
		$summary = '';
		$work_hours = '';
		$work_date = '';
		$activity = '';
		$project_name = '';
		$task_name = '';
		$project_id = '';
		$year = '';
		$month = '';
		$weekOfMonth = '';
		$weekOfYear = '';
		$checkLogsExist = '';

		$employee = new Default_Model_Employee();
		$myTsModel = new Timemanagement_Model_MyTimesheet();
		$logsarr = json_decode($logs, true);
		$firstIteration = true;
		$isFirstEntry = true;
		foreach ($logsarr as $arr)
		{
			try{
				if($timeSheet == true)
				{
					if((empty($arr["empId"])) || ($arr["empId"] == null))
					{
						$result = $result."Unable to insert row. Reason: The user id empty";
						continue;
					}
					$validUserId = $employee->isUserIdValid($arr["empId"]);
					if(empty($validUserId))
					{
						$result = $result."Unable to insert row. Reason: The user id ".$arr["empId"]." doesn't exist";
						continue;
					}
					$userid = $validUserId[0]["id"];
				}

				if($timeSheet == false)
				{
					//Get employee id
					$employeeId = $arr["empId"];

					if(($employeeId == null) || ($employeeId == ''))
					{
						$result = $result."Unable to insert row. Reason: The employee id is empty.";
						continue;
					}

					//Get details from employee id
					$useridArray = $employee->getUserIdByEmpId($employeeId);
					if((empty($useridArray)))
					{
						$result = $result."Unable to insert row. Reason: The user id for employee id ".$employeeId." doesn't exist";
						continue;
					}
					//Get user id
					$userid = $useridArray[0]['user_id'];
				}

				$workid1= $arr["work_id_1"];
				$workid2 = $arr["work_id_2"];
				$summary = $arr["summary"];
				$work_hours = $arr["work_hours"];
				$work_date = $arr["work_date"];
				$activity_code = $arr["activity"];
				$task_name = $arr["task_name"];
				$project_name = $arr["project_name"];

				//Replace forwardslash from work date
				if($work_date != '')
				{
					$work_date = str_replace('/', '-', $work_date);
					$isValidDate = $this->isValidDateTimeString($work_date, "m-d-Y", "Asia/Kolkata");
					if($isValidDate == false)
					{
						$result = $result."Unable to insert row for ".$employeeId.". Reason: Date ".$work_date." is invalid.";
						continue;
					}
					$parts = explode('-', $work_date);
					$workdate = $parts[1] . '-' . $parts[0] . '-' . $parts[2];
					$work_date = date("Y-m-d", strtotime($workdate));
					$year = date("Y", strtotime($workdate));
					$month = date("m", strtotime($workdate));
					$dayOfWeek = date(l, strtotime($workdate));
					$weekOfYear = date("W", strtotime($workdate));
					$weekOfMonth = $this->weekOfMonth($workdate,$weekOfYear);
				}

				//Do not save log when work date is empty
				else
				{
					$work_date = '';
					$result = $result."Unable to insert row where summary is ".$summary.". Reason: Work Date is empty.";
					continue;
				}

				if(trim($workid1) == null || trim($workid1) == '')
				{
					if($timeSheet == false)
					{
						$workid1 = "Dummy";
					}
					else
					{
						$result = $result."Unable to insert row where summary is ".$summary.". Reason: Work_Id_1 is null/blank.";
						continue;
					}
				}

				if(($summary == '') || ($summary == null))
				{
					$result = $result."Unable to insert row for user id: ".$userid." and work date ".$work_date.". Reason: Summary is empty.";
					continue;
				}

				if(($activity_code == '') || ($activity_code == null))
				{
					$result = $result."Unable to insert row for user id: ".$userid.", work date ".$work_date." and summary ".$summary.". Reason: activity code is empty.";
					continue;
				}
				//Get activity id
				$activityid = $this->getActivityId($activity_code);
				$activity_id = $activityid[0]['id'];

				if($work_hours != '')
				{
					$isValidHours = $this->isValidTime($work_hours);
					if(!$isValidHours)
					{
						$result = $result."Unable to insert row for user id: ".$userid.", work date ".$work_date." and summary ".$summary.". Reason: Work hours is more than 24 hours.";
						continue;
					}
				}

				if(($work_hours == '') || ($work_hours == null))
				{
					$result = $result."Unable to insert row for user id: ".$userid.", work date ".$work_date." and summary ".$summary.". Reason: Work hours is empty.";
					continue;
				}

				if(trim($task_name) == null || trim($task_name) == '')
				{
					if($timeSheet == false)
					{
						$taskid= $this->getTaskIdByTaskName("Internal");
						$task_id = $taskid[0]['id'];
					}
					else
					{
						$result = $result."Unable to insert row for user id: ".$userid.", work date ".$work_date." and summary ".$summary.". Reason: Task name is empty.";
						continue;
					}
				}

				//Get task id by task name
				else
				{
					$taskid = $this->getTaskIdByTaskName($task_name);
					$task_id = $taskid[0]['id'];
				}

				if(trim($project_name) == null || trim($project_name) == '')
				{
					if($timeSheet == false)
					{
						$projectid= $this->getNonActiveProjectIdByProjectName("Dummy");
						$project_id = $projectid[0]['id'];
					}

					else
					{
						$result = $result."Unable to insert row for user id: ".$userid.", work date ".$work_date." and summary ".$summary.". Reason: Project name is empty.";
						continue;
					}
				}

				//Get project id by project name
				else
				{
					$projectid = $this->getProjectIdByProjectName($project_name);
					$project_id =  $projectid[0]['id'];
				}


				if ($firstIteration == true)
				{
					$checkLogsExist = $this->checkLogsAlreadyExist($userid, $work_date);

					if(!empty($checkLogsExist))
					{
						$updateData = array('is_enabled'=>2,'is_active'=>2);
						$where = array('user_id=?'=>$userid, 'work_date=?'=>$work_date);
						$this->saveWorkLogs($updateData, $where);
					}
					$firstIteration = false;
				}

				$checkNOCLogsExist = $this->checkNOCLogsAlreadyExist($userid, $work_date);
				if(!empty($checkNOCLogsExist))
				{
					$where = array('user_id=?'=>$userid, 'work_date=?'=>$work_date, 'summary=?'=>'NOC', 'is_active=?'=>2);
					$this->deleteWorkLogs($where);
				}
				/*Start Save data in tm_worklogs*/
				$data = array(
						'user_id' => $userid,
						'work_id_1' => $workid1,
						'work_id_2' => $workid2,
						'summary'=> $summary,
						'work_hours'=>$work_hours,
						'work_date'=>$work_date,
						'activity_id'=>$activity_id,
						'project_id'=>$project_id,
						'task_id'=>$task_id,
						'created' =>gmdate("Y-m-d H:i:s"),
					    'modified'=> gmdate("Y-m-d H:i:s"),
						'is_enabled'=> $timeSheet != true ? 2 : 1,
						'is_active'=>1
				);
				$where = '';
				$this->saveWorkLogs($data, $where);
				/*End data in tm_worklogs*/
				/*Start Save data in tm_emp_timesheets*/
				if($timeSheet == true)
				{
					$checkTSLogsExist = $this->checkTSLogsExist($userid, $weekOfMonth, $weekOfYear);
					$existingWorkHours = null;
					if(!empty($checkTSLogsExist))
					{
						if($dayOfWeek == 'Sunday')
						{
							$updateTSData = array('sun_date'=>$work_date, 'sun_duration'=>$work_hours);
							$existingWorkHours = $checkTSLogsExist[0]['sun_duration'];
						}
						else if($dayOfWeek == 'Monday')
						{
							$updateTSData = array('mon_date'=>$work_date, 'mon_duration'=>$work_hours);
							$existingWorkHours = $checkTSLogsExist[0]['mon_duration'];
						}
						else if($dayOfWeek == 'Tuesday')
						{
							$updateTSData = array('tue_date'=>$work_date, 'tue_duration'=>$work_hours);
							$existingWorkHours = $checkTSLogsExist[0]['tue_duration'];
						}
						else if($dayOfWeek == 'Wednesday')
						{
							$updateTSData = array('wed_date'=>$work_date, 'wed_duration'=>$work_hours);
							$existingWorkHours = $checkTSLogsExist[0]['wed_duration'];
						}
						else if($dayOfWeek == 'Thursday')
						{
							$updateTSData = array('thu_date'=>$work_date, 'thu_duration'=>$work_hours);
							$existingWorkHours = $checkTSLogsExist[0]['thu_duration'];
						}
						else if($dayOfWeek == 'Friday')
						{
							$updateTSData = array('fri_date'=>$work_date, 'fri_duration'=>$work_hours);
							$existingWorkHours = $checkTSLogsExist[0]['fri_duration'];
						}
						else if($dayOfWeek == 'Saturday')
						{
							$updateTSData = array('sat_date'=>$work_date, 'sat_duration'=>$work_hours);
							$existingWorkHours = $checkTSLogsExist[0]['sat_duration'];
						}
						if($checkTSLogsExist[0]['week_duration']!=null && $work_hours!=null){
							$durationForWeek = ((float)$checkTSLogsExist[0]['week_duration'] + (float)$work_hours);
							if($existingWorkHours!=null && $isFirstEntry){
								$durationForWeek = $durationForWeek - (float)$existingWorkHours;
							}
							$updateTSData['week_duration'] = $durationForWeek;
						}else if($checkTSLogsExist[0]['week_duration']==null && $work_hours!=null){
							$updateTSData[week_duration] = ((float)$work_hours);
						}
						$where = array('emp_id=?'=>$userid, 'ts_week=?'=>$weekOfMonth, 'cal_week=?'=>$weekOfYear);
						$myTsModel->updateTimesheetRecord($updateTSData, $where);
					}

					else
					{
						$dataTS = array(
								'emp_id' => $userid,
								'project_task_id' => $task_id,
								'project_id' => $project_id,
								'ts_year'=> $year,
								'ts_month'=>$month,
								'ts_week'=>$weekOfMonth,
								'cal_week'=>$weekOfYear,
								'created' =>gmdate("Y-m-d H:i:s"),
								'modified'=> gmdate("Y-m-d H:i:s"),
								'is_active'=>1,
								'sun_date'=>$dayOfWeek == 'Sunday'? $work_date  : null,
								'sun_duration'=>$dayOfWeek == 'Sunday' ? $work_hours : null,
								'mon_date'=>$dayOfWeek == 'Monday' ? $work_date : null,
								'mon_duration'=> $dayOfWeek == 'Monday' ? $work_hours : null,
								'tue_date'=>$dayOfWeek == 'Tuesday' ? $work_date  : null,
								'tue_duration'=> $dayOfWeek == 'Tuesday' ? $work_hours : null,
								'wed_date'=>$dayOfWeek == 'Wednesday' ? $work_date : null,
								'wed_duration'=> $dayOfWeek == 'Wednesday' ? $work_hours: null,
								'thu_date'=>$dayOfWeek == 'Thursday' ? $work_date : null,
								'thu_duration'=>$dayOfWeek == 'Thursday' ? $work_hours: null,
								'fri_date'=>$dayOfWeek == 'Friday' ? $work_date : null,
								'fri_duration'=>$dayOfWeek == 'Friday' ? $work_hours : null,
								'sat_date'=>$dayOfWeek == 'Saturday' ? $work_date : null,
								'sat_duration'=>$dayOfWeek == 'Saturday' ? $work_hours : null,
								'week_duration'=>$work_hours
						);
						$myTsModel->SaveOrUpdateTimesheet($dataTS);
					}
				}
				/*End data in tm_emp_timesheets*/
			}catch(Exception $e){
				$result = $result."Unable to insert row where summary is ".$summary.". Reason: ".$e;
				throw $e;
			}
			$isFirstEntry = false;
		}
		return $result;
	}

	public function saveWorkLogs($data, $where)
	{
		if($where != '')
		{
			$this->update($data, $where);
			return 'update';
		}

		else
		{
			$this->insert($data);
			$id=$this->getAdapter()->lastInsertId('tm_worklogs');
			return $id;
		}
	}

	public function deleteWorkLogs($where)
	{
			$this->delete($where);
			return 'delete';
	}

	public function isWorklogAddedToday($userId){
		$result = $this->select()
		->setIntegrityCheck(false)
		->from(array('t'=>'tm_worklogs'),array('t.id'))
		->where('t.user_id='.$userId.' AND t.work_date = CURDATE()');
		return $this->fetchAll($result)->toArray();
	}

	public function empProjects($employeeId)
	{
		$result = $this->select()
		->setIntegrityCheck(false)
		->from(array('t'=>'tm_project_employees'),array('tp.project_name'))
		->join(array('tp'=>'tm_projects'), 't.project_id = tp.id')
		->where('t.emp_id=\''.$employeeId.'\' AND t.is_active = 1 AND tp.is_active = 1 AND tp.project_status = \'in-progress\' AND end_date IS NOT NULL AND DATE(end_date) >= DATE(NOW())');
		return $this->fetchAll($result)->toArray();
	}

	public function empTasks($employeeId)
	{
		$result = $this->select()
		->setIntegrityCheck(false)
		->from(array('t'=>'tm_project_task_employees'),array('tmt.task'))
		->join(array('tpt'=>'tm_project_tasks'), 't.project_task_id = tpt.id')
		->join(array('tmt'=>'tm_tasks'), 'tpt.task_id = tmt.id')
		->where('t.emp_id=\''.$employeeId.'\' AND t.is_active = 1');
		return $this->fetchAll($result)->toArray();
	}
	
	public function getTaskByProject($employeeId,$projectId)
	{
	    $result = $this->select()
	    ->setIntegrityCheck(false)
	    ->from(array('t'=>'tm_project_task_employees'),array('tmt.task'))
	    ->join(array('tmt'=>'tm_tasks'), 't.task_id = tmt.id')
	    ->where('t.emp_id=\''.$employeeId.'\' AND t.project_id=\''.$projectId.'\' AND t.is_active = 1');
	    return $this->fetchAll($result)->toArray();
	}

	public function getProjectIdByProjectName($projectName)
	{
		$result = $this->select()
		->setIntegrityCheck(false)
		->from(array('t'=>'tm_projects'),array('t.id'))
		->where('t.project_name=\''.$projectName.'\' AND t.is_active = 1');
		return $this->fetchAll($result)->toArray();
	}

	public function getNonActiveProjectIdByProjectName($projectName)
	{
		$result = $this->select()
		->setIntegrityCheck(false)
		->from(array('t'=>'tm_projects'),array('t.id'))
		->where('t.project_name=\''.$projectName.'\' AND t.is_active = 2');
		return $this->fetchAll($result)->toArray();
	}

	public function getProjectNameByProjectId($projectId)
	{
		$result = $this->select()
		->setIntegrityCheck(false)
		->from(array('t'=>'tm_projects'),array('t.project_name'))
		->where('t.id=\''.$projectId.'\' AND t.is_active = 1');
		return $this->fetchAll($result)->toArray();
	}

	public function getActivityId($activitycode)
	{
		$result = $this->select()
		->setIntegrityCheck(false)
		->from(array('ta'=>'tm_Activities'),array('ta.id'))
		->where('ta.activity_code=\''.$activitycode.'\'');
		return $this->fetchAll($result)->toArray();
	}

	public function getActivityCode()
	{
		$result = $this->select()
		->setIntegrityCheck(false)
		->from(array('ta'=>'tm_Activities'),array('ta.activity_code'));
		return $this->fetchAll($result)->toArray();
	}

	public function getActivityCodeByActivityId($activityId)
	{
		$result = $this->select()
		->setIntegrityCheck(false)
		->from(array('ta'=>'tm_Activities'),array('ta.activity_code'))
		->where('ta.id=\''.$activityId.'\'');
		return $this->fetchAll($result)->toArray();
	}

	public function getTaskIdByTaskName($taskName)
	{
		$result = $this->select()
		->setIntegrityCheck(false)
		->from(array('t'=>'tm_tasks'),array('t.id'))
		->where('t.task=\''.$taskName.'\' AND t.is_active = 1');
		return $this->fetchAll($result)->toArray();
	}

	public function getTaskNameByTaskId($taskId)
	{
		$result = $this->select()
		->setIntegrityCheck(false)
		->from(array('t'=>'tm_tasks'),array('t.task'))
		->where('t.id=\''.$taskId.'\' AND t.is_active = 1');
		return $this->fetchAll($result)->toArray();
	}

	public function getTaskIdByTaskAssigned($userId)
	{
			$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('tm'=>'tm_project_task_employees'),array('tm.*'))
			->where('ed.emp_id =\''.$userId.'\' AND ed.is_active = 1');
			return $this->fetchAll($select)->toArray();
	}

	public function weekOfMonth($date,$weekOfYear) {
		//Get the first day of the month.
		$firstOfMonth = date("Y-m-01", strtotime($date));
		$weekFirstOfMonth = date("W", strtotime($firstOfMonth));
		//Apply above formula.
		$weekOfMonths = ($weekOfYear - $weekFirstOfMonth) + 1;
		return $weekOfMonths;
	}

	public function checkLogsAlreadyExist($userId, $workDate)
	{
		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('tm'=>'tm_worklogs'),array('tm.*'))
		->where('tm.user_id =\''.$userId.'\' AND tm.work_date =\''.$workDate.'\' AND tm.is_active = 1');
		return $this->fetchAll($select)->toArray();
	}

	public function checkNOCLogsAlreadyExist($userId, $workDate)
	{
		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('tm'=>'tm_worklogs'),array('tm.*'))
		->where('tm.user_id =\''.$userId.'\' AND tm.summary= \'NOC\' AND tm.work_date =\''.$workDate.'\' AND tm.is_active = 2');
		return $this->fetchAll($select)->toArray();
	}

	public function getTimeDetails($userId, $firstday, $lastday)
	{
	    $db = Zend_Db_Table::getDefaultAdapter ();
	    $query = "Select duration , work_date from
                    (SELECT SUM(work_hours) as duration, work_date
                     FROM tm_worklogs where user_id = ".$userId." and work_date >= STR_TO_DATE('" . $firstday . "','" . DATEFORMAT_MYSQL . "') and work_date <= STR_TO_DATE('" . $lastday . "','" . DATEFORMAT_MYSQL . "') and is_active = 1
                     group by work_date)
                  as tm_worklogs";
	    return $db->query ( $query )->fetchAll ();
	}

	public function checkTSLogsExist($userId, $tsWeek, $calWeek)
	{
		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('tm'=>'tm_emp_timesheets'),array('tm.*'))
		->where('tm.emp_id =\''.$userId.'\' AND tm.ts_week =\''.$tsWeek.'\' AND tm.cal_week =\''.$calWeek.'\' AND tm.is_active = 1');
		return $this->fetchAll($select)->toArray();
	}

	public function isDateValid($workDate,$year,$month)
	{
		$day = date("d", strtotime($workDate));
		$isValid = checkdate($month, $day, $year);
		return $isValid;
	}

	public function isValidDateTimeString($str_dt, $str_dateformat, $str_timezone) {

		$date = DateTime::createFromFormat($str_dateformat, $str_dt, new DateTimeZone($str_timezone));

		return $date && DateTime::getLastErrors()["warning_count"] == 0 && DateTime::getLastErrors()["error_count"] == 0;

	}

	public function isValidTime($work_hours){
		if($work_hours > 24){
			return false;
		}
		return true;
	}

	public function getLockingPeriod()
	{
		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('tc'=>'tm_configuration'),array('tc.worklog_locking_period'));
		return $this->fetchAll($select)->toArray();
	}

	public function getWeekends($fromDate, $toDate) {
		$weekends = 0;

		while ( $fromDate <= $toDate ) {

			if ($fromDate->format ( 'w' ) == 0 || $fromDate->format ( 'w' ) == 6) {
				$weekends++;
			}
			$fromDate->modify ( '+1 day' );
		}
		return $weekends;
	}

	public function getWeekendsArray($fromDate, $toDate) {
		$weekendsArray = array();
		$fromDate = clone $fromDate;
		while ( $fromDate <= $toDate ) {
			if ($fromDate->format ( 'w' ) == 0 || $fromDate->format ( 'w' ) == 6) {
				array_push($weekendsArray,$fromDate->format('Y-m-d'));
			}
			$fromDate->modify ( '+1 day' );
		}
		return $weekendsArray;
	}

	public function array_union($x, $y)
	{
		$aunion=  array_merge(
				array_intersect($x, $y),
				array_diff($x, $y),
				array_diff($y, $x)
				);
		return $aunion;
	}
	public function isLocked($logdate, $loginUserId){
		$worklogs = new Api_Model_Worklogs();
		$employeesModel = new Default_Model_Employees ();
		$employeeholidaysModel = new Default_Model_Holidaydates ();
		$leaveRequestModel = new Default_Model_Leaverequest();

		//Check if worklog sheet for the selected date should be editable or not
		$isEditable = true;
		$lockingPeriod = $worklogs->getLockingPeriod();
		$workLogLockingPeriod = $lockingPeriod[0]['worklog_locking_period'];//Get this value using tm_configurations table
		$now = time();
		$toDate = date("Y-m-d",$now);
		$worklog_date = strtotime($logdate);
		$startDate = date("Y-m-d",$worklog_date);
		$weekendCount = $worklogs->getWeekends(new DateTime ( $startDate ), new DateTime ( $toDate ));
		$weekendArray = $worklogs->getWeekendsArray(new DateTime ( $startDate ), new DateTime ( $toDate ));
		$holidayGroup = $employeesModel->getHolidayGroupForEmployee ( $loginUserId );
		$holidays = $employeeholidaysModel->getHolidaysBetween ( $startDate, $toDate, $holidayGroup ['0'] ['holiday_group'] ) [0] ['holidaydates'];
		$holidaysArray = $employeeholidaysModel->getHolidaysArrayBetween ( $startDate, $toDate, $holidayGroup ['0'] ['holiday_group'] );
		$holidayWeekendUnion = $worklogs->array_union($weekendArray, $holidaysArray);
		$leavesArray = $leaveRequestModel->getApprovedLeavesArrayBetween($loginUserId, new DateTime($startDate), new DateTime($toDate));
		$holidayWeekendLeaveUnion = $worklogs->array_union($holidayWeekendUnion, $leavesArray);
		
		$additionaGraceDueToLeavesOrHoliday = count($holidayWeekendLeaveUnion);

		//gathering data to check day punches
		$myattendancemodel = new Default_Model_Myattendance ();
        $officeHoursModel = new Default_Model_OfficeHours ();
        $activityModel= new Default_Model_EmployeeWorkActivity();
        $employeeModel= new Default_Model_Employee();
        $employeeInfo = $employeeModel->getActiveEmployeeData ( $loginUserId );
        if($employeeInfo [0] != null && $employeeInfo [0] ['businessunit_id'] != null) {
            $officeHoursRule = $officeHoursModel->getOfficeHoursRule($employeeInfo [0] ['businessunit_id']);
        }
        $dailyActivity = array ();
        $timezone = $myattendancemodel->getCurrentTimeZone ();
        $modifier = $myattendancemodel->getModifierStringFromOfffset ( $timezone [0] ['offet_value'] );



		foreach ($holidayWeekendLeaveUnion as $offDay){

            //Check punch for each offDay in office hours window
            $activitiesData = $activityModel->getActivitiesBetween ( $loginUserId, $offDay, $officeHoursRule, $modifier );
            if(!empty($activitiesData))
                $punchActivity=true;
            else $punchActivity=false;

            if($punchActivity==false) {
                if (new DateTime($offDay) == new DateTime($logdate)) {
                    $additionaGraceDueToLeavesOrHoliday--;
                    break;
                }
            }
            else $additionaGraceDueToLeavesOrHoliday--;
		}
		$datediff = $now - $worklog_date;
		$years = floor($datediff / (365*60*60*24));
		$months = floor(($datediff - $years * 365*60*60*24) / (30*60*60*24));
		$noOfDays = floor(($datediff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		switch($workLogLockingPeriod){
			case "Daily":
				if($noOfDays>(1+$additionaGraceDueToLeavesOrHoliday)){
					$isEditable = false;
				}
				break;
			case "Weekly":
				$dayOfWeek = (new DateTime($startDate))->format('w');
				$nofDaysToSunday = $dayOfWeek==0?0:(7-$dayOfWeek);
				$lastAllowedDate = strtotime($nofDaysToSunday.' days', $worklog_date);
				$datediff = $now - $lastAllowedDate;
				$noOfDays = round($datediff / (60 * 60 * 24));
				if($noOfDays>($additionaGraceDueToLeavesOrHoliday)){
					$isEditable = false;
				}
				break;
			case "Monthly":
				$yearLogDate = (new DateTime($startDate))->format('Y');
				$monthLogDate = (new DateTime($startDate))->format('m');
				$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $monthLogDate, $yearLogDate);
				$nofDaysToMonthEnd = $daysInMonth - $monthLogDate;
				$lastAllowedDate = strtotime($nofDaysToMonthEnd.' days', $worklog_date);
				$datediff = $now - $lastAllowedDate;
				$noOfDays = round($datediff / (60 * 60 * 24));
				if($noOfDays>1){
					$isEditable = false;
				}
				break;
		}
		return $isEditable;
	}
	
	public function getdurationDates($duration, &$fromdate, &$todate){
	    switch ($duration){
	        case '1' :
	        case 'Last Week': $todate = (new dateTime())->modify('last friday');
	                           $fromdate = (new DateTime($todate->format( DATEFORMAT_PHP)))->modify('-4 days');
	                          break;
	        case '2' :
	        case 'Current Month': $todate = (new dateTime())->modify('last day of this month');
                    	          $fromdate = (new dateTime())->modify('first day of this month');
                    	          break;
	        case '3' :
	        case 'Last Month': $todate = (new dateTime())->modify('last day of last month');
                    	       $fromdate = (new dateTime())->modify('first day of last month');
                    	       break;
	    }
	    $fromdate = $fromdate->format(DATEFORMAT_PHP);
	    $todate= $todate->format(DATEFORMAT_PHP);
	    
	}
	
	public function getWorkloggedWithinByUserId($id, $fromDate,$toDate){
	    $query = "Select sum(work_hours) as work_hours, wl.work_date, wl.user_id FROM xnet.tm_worklogs wl where wl.user_id = ".$id." and wl.work_date between STR_TO_DATE('".$fromDate."','".DATEFORMAT_MYSQL."') and STR_TO_DATE('".$toDate."','".DATEFORMAT_MYSQL."') and wl.is_active = 1 group by wl.work_date";
	    $db = Zend_Db_Table::getDefaultAdapter ();
	    $result = $db->query ( $query )->fetchAll ();
	    return $result;
	}
	
	public function getIncompleteEntriesCount($workLogged,$threshold_hours,&$nocHours){
	    $count = 0;
	    foreach ($workLogged as $key => $log){	        
            if($log['work_hours']< $threshold_hours){
                $nocHours += ($threshold_hours - $log['work_hours']);
                $count++;
            } 
	    }
	    
	    return $count;
	}
	
	public function getWorkHoursFromActivities($id,$activityCode,$fromDate,$toDate){
	    $query = "SELECT sum(work_hours) as sum FROM tm_worklogs wl
                    inner join  tm_activities ac on wl.activity_id = ac.id
                    where wl.work_date between str_to_date('".$fromDate."','".DATEFORMAT_MYSQL."') and  str_to_date('".$toDate."','".DATEFORMAT_MYSQL."') and ac.activity_code = '".$activityCode."' and wl.user_id=".$id." and wl.is_active = 1;";
	    $db = Zend_Db_Table::getDefaultAdapter ();
	    $result = $db->query ( $query )->fetchAll ();
	    return $result[0]['sum'] == null ? 0 :  $result[0]['sum'];
	}
	
	public function getWorklogDataForSheet($id, $startDate,$endDate){	
	    $fromDate = new DateTime($startDate->format(DATEFORMAT_PHP));
	    $toDate = new DateTime($endDate->format(DATEFORMAT_PHP));
	    $employeeModel = new Default_Model_Employee ();
	    $employeesModel = new Default_Model_Employees ();
	    $employeeholidaysModel = new Default_Model_Holidaydates ();
	    $employeeleaverequestModel = new Default_Model_Leaverequest ();
	    $employeeleaveTypeModel = new Default_Model_Employeeleavetypes ();
	    $time_configurationModel = new Timemanagement_Model_Configuration();
	    $empworkActivityModel = new Default_Model_EmployeeWorkActivity();
	    $empAttendanceModel = new Default_Model_Myattendance();
	    $holidayGroup = $employeesModel->getHolidayGroupForEmployee ( $id );	
	    $wfhId = $employeeleaveTypeModel->getEmployeeLeaveTypeByCode ( 'WFH' ) [0] ['leavetype_id'];	    
	    $worklogData = array();
	    while ($fromDate <= $toDate){
	        $dailyData = $this->getWorklogDataOn($id, $fromDate);
	        if(count($dailyData) == 0){
	            $holidayCount = $employeeholidaysModel->getHolidaysBetween ( $fromDate->format(DATEFORMAT_PHP), $fromDate->format(DATEFORMAT_PHP), $holidayGroup ['0'] ['holiday_group'] ) ;
	            $dailyData ['work_date'] = $fromDate->format(DATEFORMAT_PHP);
	            if(count($empworkActivityModel->getActivitiesOn($id, $fromDate)) > 0 ){
	                $dailyData ['status'] = 'Missed Entry';
	            }elseif($this->isWeekend ($fromDate)) {
    	            $dailyData ['status'] = 'Weekend';
    	        }elseif($holidayCount [0] ['holidaydates'] > 0){
    	            $dailyData ['status'] = 'Holiday';
    	        }else {
    	            $dailyData ['status'] = 'Leave/Absent';
    	        }
    	        array_push($worklogData,$dailyData);
	        }else{
	            foreach ($dailyData as $dailyDatum){
	                array_push($worklogData,$dailyDatum);
	            }
	        }
	        $fromDate->modify('+1 day');
	    }
	    
	    return $worklogData;
	}
	
	public function isWeekend($date){
	    $day = $date->format ( 'w' );
	    if($day == 0 || $day == 6)
	        return true;
	        return false;
	}
	
	public function getWorklogDataOn($id,$date){
	    $query = "Select wl.work_date, pr.project_name, ts.task, wl.work_id_1 as jira_id, wl.work_id_2 as external_id ,wl.summary, wl.work_hours, ac.activity_code
                    from tm_worklogs wl 
                    inner join tm_activities ac on wl.activity_id = ac.id
                    inner join tm_tasks ts on wl.task_id = ts.id 
                    inner join tm_projects pr on wl.project_id = pr.id
                    where wl.user_id = ".$id." and wl.is_active = 1 and wl.work_date = str_to_date('".$date->format(DATEFORMAT_PHP)."','".DATEFORMAT_MYSQL."');";
	    $db = Zend_Db_Table::getDefaultAdapter ();
	    return $db->query ( $query )->fetchAll ();
	    
	}
	
	public function getActivities(){
	    $query = "select activity_code from tm_activities;";
	    $db = Zend_Db_Table::getDefaultAdapter ();
	    return $db->query ( $query )->fetchAll ();
	}
	
	public function createColumnsArray($end_column, $first_letters = '')
	{
	    $columns = array();
	    $length = strlen($end_column);
	    $letters = range('A', 'Z');
	    
	    // Iterate over 26 letters.
	    foreach ($letters as $letter) {
	        // Paste the $first_letters before the next.
	        $column = $first_letters . $letter;
	        
	        // Add the column to the final array.
	        $columns[] = $column;
	        
	        // If it was the end column that was added, return the columns.
	        if ($column == $end_column)
	            return $columns;
	    }
	    
	    // Add the column children.
	    foreach ($columns as $column) {
	        // Don't itterate if the $end_column was already set in a previous itteration.
	        // Stop iterating if you've reached the maximum character length.
	        if (!in_array($end_column, $columns) && strlen($column) < $length) {
	            $new_columns = $this->createColumnsArray($end_column, $column);
	            // Merge the new columns which were created with the final columns array.
	            $columns = array_merge($columns, $new_columns);
	        }
	    }
	    
	    return $columns;
	}
	
    public function disableWorklogs($date,$id){
        $query = "update tm_worklogs set is_active=2 where work_date=str_to_date('".$date."','".DATEFORMAT_MYSQL."') and user_id=".$id.";";
        $db = Zend_Db_Table::getDefaultAdapter ();
        return $db->query ( $query );
    }

}