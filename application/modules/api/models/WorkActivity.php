<?php
class Api_Model_WorkActivity extends Zend_Db_Table_Abstract {
	protected $_name = 'main_emp_work_activity';
	protected $_primary = 'id';
	public function saveEmployeeAttendance($rowEntry) {
		$this->insert ( $rowEntry );
		$id = $this->getAdapter ()->lastInsertId ( $this->_name );
		return $id;
	}
	private function getuserid($emp_id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'e' => 'main_employees_summary' 
		), array (
				'e.user_id' 
		) )->where ( "e.employeeId = '" . $emp_id . "'" );
		
		$result = $this->fetchRow ( $result );
		if($result != null) {
            return $result ['user_id'];
        } else {
		    return null;
        }
	}
	public function prepareRowData($params) {
	    $userId = $this->getuserid ( $params ['empid'] );
	    if($userId != null) {
            $data = array(
                'user_id' => $userId,
                'punch_time' => $params ['punchdate'] . " " . $params ['punchtime'],
                'status' => $params ['status'],
                'machine_id' => $params ['machineid']
            );

            return $data;
        } else {
	        return null;
        }
	}
}