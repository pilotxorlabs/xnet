<?php
class Api_Model_Attendance extends Zend_Db_Table_Abstract {
	protected $_name = 'main_emp_attendence';
	protected $_primary = 'id';
	public function saveOrUpdateEmployeeAttendance($rowEntry) {
		$result = $this->isemployeePresent ( $rowEntry ['user_id'], $rowEntry ['punch_date'] );
		if ($result != null) {
			$this->update ( $rowEntry, array (
					'id=?' => $result->toArray () ['id'] 
			) );
			return 'update';
		} else {
			$this->insert ( $rowEntry );
			$id = $this->getAdapter ()->lastInsertId ( $this->_name );
			return $id;
		}
	}
	public function isemployeePresent($used_id, $punch_date) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'e' => 'main_emp_attendence' 
		), array (
				'e.*' 
		) )->where ( "e.user_id = " . $used_id . " AND e.punch_date = STR_TO_DATE('" . $punch_date . "','%Y-%m-%d')" );
		return $this->fetchRow ( $result );
	}
	private function getuserid($emp_id) {
		$result = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'e' => 'main_employees_summary' 
		), array (
				'e.user_id' 
		) )->where ( "e.employeeId = '" . $emp_id . "'" );
		
		$result = $this->fetchRow ( $result )->toArray ();
		return $result ['user_id'];
	}
	public function prepareRowData($params) {
		$data = array (
				'user_id' => $this->getuserid ( $params ['empid'] ),
				'punch_date' => $params ['punchdate'],
				'check_in' => $params ['punchin'],
				'check_out' => $params ['punchout'],
				'status' => $params ['status'],
				'overall_status' => $params ['overallStatus'],
				'working_hours' => $params ['totalhours'],
				'machine_in' => $params ['machinein'],
				'machine_out' => $params ['machineout'] 
		);
		
		return $data;
	}
}