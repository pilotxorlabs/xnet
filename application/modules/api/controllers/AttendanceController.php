<?php
class Api_AttendanceController extends Zend_Rest_Controller {
	public function init() {
		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout->disableLayout ();
	}
	public function indexAction() {
		$parameters = $this->_getAllParams ();
		if (count ( $parameters ) > 4 && array_key_exists ( "source", $parameters ) && $parameters ['source'] == 'xnet') {
			$workActivity_model = new Api_Model_WorkActivity ();
			$rowEntry = $workActivity_model->prepareRowData ( $parameters );
			if($rowEntry != null) {
                $workActivity_model->saveEmployeeAttendance($rowEntry);
            } else {
			    throw new Zend_Controller_Request_Exception("Not a valid employee ID");
            }
		} else {
			$attendance_model = new Api_Model_Attendance ();
			$rowEntry = $attendance_model->prepareRowData ( $parameters );
			$attendance_model->saveOrUpdateEmployeeAttendance ( $rowEntry );
		}
	}
	public function postAction() {
	}
	public function putAction() {
	}
	public function getAction() {
	}
	public function deleteAction() {
	}
}
