﻿using System;
using System.Data.SqlTypes;
using System.IO;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Text;

namespace SyncXNet
{
    public class StoredProcedure
    {
        [Microsoft.SqlServer.Server.SqlProcedure]
        public static void syncAttendence(SqlString emp_id, SqlString punch_date, SqlString punch_in, SqlString punch_out, SqlString total_hours, SqlString inlet, SqlString outlet ,out SqlString returnval)
        {
            string empid = (string)emp_id;
            string punchdate = (string)punch_date;
            string punchin = (string)punch_in;
            string punchout = (string)punch_out;
            string totalHours = (string)total_hours;
            string inmachine = (string)inlet;
            string outmachine = (string)outlet;
            Stream stream = null;
            Stream newStream = null;
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            try
            {

                writeLog(DateTime.Now + ": Entering SP.");
                ResourceManager rm = new ResourceManager("SyncXNet.Resource", Assembly.GetExecutingAssembly());
                string data = "username=" + rm.GetString("Username") + "&password=" + rm.GetString("Password"); //replace <value>
                byte[] dataStream = Encoding.UTF8.GetBytes(data);
                CookieContainer cookies = new CookieContainer();
                writeLog(DateTime.Now + ": Authenticating for URL: "+ rm.GetString("LoginURL"));
                request = (HttpWebRequest)WebRequest.Create(rm.GetString("LoginURL"));
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.CookieContainer = cookies;
                request.ServicePoint.Expect100Continue = false;

                newStream = request.GetRequestStream();
                newStream.Write(dataStream, 0, dataStream.Length);

                if (newStream != null)
                    newStream.Close();
                writeLog(DateTime.Now + ": stream written. making auth request: " );
                response = (HttpWebResponse)request.GetResponse();
                if ("/index.php/index".Equals(response.ResponseUri.AbsolutePath)){
                    writeLog(DateTime.Now + ": Authentication Failure. Kindly check the username and password");
                    returnval = "Authentication Failure :" + rm.GetString("LoginURL");
                }

                stream = response.GetResponseStream();
                writeLog(DateTime.Now + ": Authentication done." );
                empid = empid.TrimEnd(new char[] { ' ' });
                String restURL = rm.GetString("RestURL") + "?empid=XOR" + empid.TrimStart(new char[] { '0' }) + "&punchdate=" + punchdate;
                if (!string.IsNullOrEmpty(punchin))
                {
                    restURL += "&punchin=" + punchin;
                }
                if (!string.IsNullOrEmpty(punchout))
                {
                    restURL += "&punchout=" + punchout;
                }

                if (!string.IsNullOrEmpty(totalHours))
                {
                    restURL += "&totalhours=" + totalHours;
                }
                if (!string.IsNullOrEmpty(inmachine))
                {
                    restURL += "&machinein=" + inmachine;
                }
                if (!string.IsNullOrEmpty(outmachine))
                {
                    restURL += "&machineout=" + outmachine;
                }


                if (!string.IsNullOrEmpty(punchout) && !string.IsNullOrEmpty(totalHours))
                {
                    float hours = float.Parse(totalHours);
                    if (hours < 4)
                        restURL += "&status=AA&overallStatus=Absent";
                    else if (hours >= 4 && hours < 8)
                        restURL += "&status=PA&overallStatus=Half Day";
                    else
                        restURL += "&status=PP&overallStatus=Full Day";
                }
                else
                {
                    restURL += "&status=AA&overallStatus=Absent";
                }

                writeLog(DateTime.Now + ": Rest URL:" + restURL);
                HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(restURL);
                request1.CookieContainer = cookies;
                request1.Method = "GET";
                response = (HttpWebResponse)request1.GetResponse();

                writeLog(DateTime.Now + ": Tables updated sucessfully for empid " + empid);
                returnval = "tables updated sucessfully for :"  + restURL;

            }
            catch (Exception ex)
            {
                writeLog(DateTime.Now + ": Sync Failed for empid " + empid + " due to : " + ex.Message.ToString());
                writeLog(DateTime.Now + ": " + ex.StackTrace);
                returnval =  "Sync Falid due to: " + ex.Message.ToString() ;
            }
            finally
            {
                if (stream !=null)
                    stream.Close();
                if (newStream != null)
                    newStream.Close();
                if (response != null)
                    response.Close();

            }
        }

        public static void writeLog(string strMessage)
        {
            File.AppendAllText(Path.GetTempPath() + "SyncXNetlog.txt", strMessage+ "\r\n");
            
        }

    }

}
