const syncRequest = require('sync-request');
const request = require('request');
const cookie = require('cookie');
var xCookie = null;
var url = 'http://172.16.98.25/index.php';

var authenticate = () => {
  return new Promise((resolve,reject) => {
    request.post({
      url: url + '/index/loginpopupsave?username=' + encodeURIComponent('ak@xorlabs.com') + '&password=' + encodeURIComponent('xorlabs1234')
    }, (err,response) => {
      if(err){
        reject(err);
      }else{
        xCookie = cookie.parse(response.headers['set-cookie'][0]);
        resolve();
      }
    })
  });
}

var postWorkLogs = (worklogs) => {
  var res = syncRequest('POST', url + "/api/worklogs",{
    headers : {
      'Cookie': `PHPSESSID=${xCookie.PHPSESSID}`
    },
    body : worklogs
  });
  if(res.statusCode != 200){
    return "Error. Server responded with " + res.statusCode;
  }
  return "Success";
};

module.exports = {
  authenticate,
  postWorkLogs
}
