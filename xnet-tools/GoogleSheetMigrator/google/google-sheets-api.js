const fs = require('fs');
const {google} = require('googleapis');
const OAuth2Client = google.auth.OAuth2;
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
const TOKEN_PATH = __dirname + '/' + 'credentials.json';
const opener = require('opener');
const http = require('http');
const url = require('url');
const worklog = require('../worklogs/operations');
const today = (new Date().getMonth() + 1) + '/' + new Date().getDate() + '/' + new Date().getFullYear();
var config = require('config.json')(__dirname + '/' + 'sheets.json');
const jiraApi = require('../jira/jira-access');
const xnetApi = require('../xnet/access');

var readCredentials = () => {
  return JSON.parse(fs.readFileSync(__dirname + '/' + 'client_secret.json'));
}

function authorize(callback) {
  const {client_secret, client_id, redirect_uris} = readCredentials().web;
  const oAuth2Client = new OAuth2Client(client_id, client_secret,redirect_uris[0]);

  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(undefined, oAuth2Client);
  });
}

function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Please authorize me to access your data. I will patiently wait till then....');
  var server = http.createServer((req,res)=>{
    var query = url.parse(req.url,true).query;
    if(query.code == undefined){
      return res.end();
    }
    res.end();
    server.close();
    oAuth2Client.getToken(query.code.toString(), (err, token) => {
      if (err) return callback('The API returned an error: ' + err);
      oAuth2Client.setCredentials(token);
      console.log('Thank you for trusting me with your data. Expect no foulplay.');
      callback(undefined, oAuth2Client);
    });
  }).listen(8080);
  opener(authUrl);
}

function printRows(auth){
  var config = require('config.json')(__dirname + '/' + 'sheets.json');
  const allSheets = config.sheets;
  allSheets.forEach((sheet) => {
    console.log(`Started reading google sheet for ${sheet.name} ${sheet.empId}`);
    printRowsBySheet(auth,sheet)
  });
}

function printRowsBySheet(auth,sheet) {
  var config = require('config.json')(__dirname + '/' + 'sheets.json');
  var file = __dirname + '/' + `${sheet.name}-${sheet.empId}.log`;
  fs.writeFileSync(file, '');
  const sheets = google.sheets({version: 'v4', auth});
  sheets.spreadsheets.values.get({
    spreadsheetId: sheet.id,
    majorDimension: config.options.majorDimension,
    range: `${sheet.name}${config.options.range}`,
  }, (err, response) => {
    if (err) return console.log('The API returned an ' + err);
    const rows = response.data.values;
    var flag = false;
    var current = {};
    var worklogs = [];
    for(var row = 1;row < rows.length;row++){
      if(!flag){
        current = rows[row][4];
        fs.appendFileSync(file, `Preparing worklogs for ` + current + '\n');
        flag = true;
      }
      if(rows[row][3]){
        var worklog = {
          empId: sheet.empId,
          work_id_1: rows[row][0].trim(),
          work_id_2: rows[row][1].trim(),
          summary: rows[row][2].trim(),
          work_date: rows[row][4].trim(),
          task_name: "Legacy"
        };
        var issue = jiraApi.getIssueSync(rows[row][0]);
        if(issue){
          worklog.project_name = issue.fields.project.name;
          worklog.summary = issue.fields.summary;
        }else{
          worklog.project_name = "Legacy";
          worklog.work_id_1 = `LG-${row}`;
          if(!rows[row][2]){
            worklog.summary = "No summary found.";
          }
        }

        worklog.work_hours = rows[row][3];
        worklog.activity = rows[row][5] ? rows[row][5] : "ZZZ";
        worklogs.push(worklog);
      }

      if((row) == (rows.length - 1) || current != rows[row + 1][4]){
        flag = false;
        if(worklogs.length){
          fs.appendFileSync(file, `Sending worklogs for ` + current + '\n');
          var worklogsString = JSON.stringify(worklogs,undefined,2);
          fs.appendFileSync(file, worklogsString + '\n');
          if(xnetApi.postWorkLogs(worklogsString) == "Success"){
            fs.appendFileSync(file, `Worklogs post successful for ` + current + '\n');
          }else{
            fs.appendFileSync(file, `Worklogs post failed for` + current + '\n');
          }
        }else{
          fs.appendFileSync(file, `No worklogs found for ` + current + '\n');
        }
        worklogs = [];
      }
    }
	console.log(`Done writing google sheet for ${sheet.name} ${sheet.empId}`);
  });
}

module.exports = {
    authorize,
    printRows,
    printRowsBySheet
}
