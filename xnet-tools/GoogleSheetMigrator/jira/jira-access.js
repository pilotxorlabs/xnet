const request = require('request');
const fs = require('fs');
const url = 'https://mhwconsulting.atlassian.net';
const jql = 'updatedDate >= startOfDay() and updatedDate < endOfDay() and assignee was currentUser()';
const badError = 'Unable to login to JIRA. Please check your internet connectivity.';
const syncRequest = require('sync-request');
var authentication = undefined;

var validate = (auth,callback) => {
  request({
    url : url + '/rest/api/2/search?jql=' + encodeURIComponent(jql),
    method : 'GET',
    json: true,
    auth
  },(err,response)=>{
    if(err){
      callback(badError);
    }else if (response.statusCode == 401){
      callback('Invalid login access to JIRA. Please try again with valid credentials.');
    }else{
      authentication = auth;
      callback(undefined);
    }
  })
}

var getIssueAsync = (issue, callback) => {
  request({
      url : url + '/rest/api/2/issue/' + issue.key,
      method : 'GET',
      json: true,
      auth: authentication
    }, (err,response) => {
      if(err){
        callback(badError)
      }else{
        callback(undefined,response.body);
      }
    })
}

var getIssueSync = (issue) => {
  var res = syncRequest('GET', url + '/rest/api/2/issue/' + issue, {
    headers:{
      authorization: 'Basic ' + new Buffer(authentication.user + ':' + authentication.pass, 'ascii').toString('base64')
    }
  });

  if(res.statusCode != '200'){
    return "";
  }
  return JSON.parse(res.getBody('utf8'));
}

module.exports = {
  getIssueAsync,
  getIssueSync,
  validate
}
