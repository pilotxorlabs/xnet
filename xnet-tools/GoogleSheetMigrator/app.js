var fs = require('fs');
const jiraApi = require('./jira/jira-access');
const worklog = require('./worklogs/operations');
const googleApi = require('./google/google-sheets-api');
const xnet = require('./xnet/access');
const prompt = require('prompt');
const csvFilePath=('./google/google_sheet_details.csv');
const csv=require('csvtojson');
const jsonfile = require('jsonfile');
var file = ('./google/sheets.json');	

  csv()
		.fromFile(csvFilePath)
		.then((jsonObj)=>{
			var json = JSON.stringify(jsonObj, undefined, 2);

			json = json.replace(/\\'/g, '\'');
			json = json.replace(/\\"/g, '"');
			json = json.replace(/\\0/g, '\0');
			json = json.replace(/\\\\/g, '\\');
			fileContent = '{ "sheets":';
			fileContent += json;
			fileContent += ', "options": { "majorDimension": "ROWS", "range": "!B:G"} }'
			fs.writeFile(file,fileContent,function (err) {
			if (err) console.error(err)
		  });	     
		});	

var schema = {
    properties: {
      'jira-username': {
        required: true
      },
      'jira-password': {
        required: true,
        hidden: true
      }
    }
  };
  
prompt.start();

prompt.get(schema, (err, result) => {
  const jAuth = {
    user: result['jira-username'],
    pass: result['jira-password']
  };
  jiraApi.validate(jAuth, (err)=> {
    if(err){
      return console.log(err);
    }
	csv();
    xnet.authenticate().then((response) => {
      googleApi.authorize((err, auth) => {
        if(err){
          return console.log(err);
        }		
        googleApi.printRows(auth);
      })
    }).catch((err) => {
      return console.log(err);
    })
  })
});
