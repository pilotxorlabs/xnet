1. Update the csv file named google_sheet_details in xnet-tools\GoogleSheetMigrator\google
2. Open it in excel.
3. Add the values for columns:- id, name, empId
   where id will be the google sheet id i.e. the value in between edit and d in url
   for example:- https://docs.google.com/spreadsheets/d/1mCQK0Tgm3KH99BNBGxcexck0tgpEAdB7z/edit
   In the above example "1mCQK0Tgm3KH99BNBGxcexck0tgpEAdB7z" is the id that will be added under column id in csv
Note: id is different for each employee and one row will contains single employee information.