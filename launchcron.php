<?php
    require_once dirname(__FILE__).'/public/db_constants.php';
    $fp = fopen(dirname(__FILE__).'/logs/cron.log', 'w');
    $cookiefile = dirname(__FILE__)."/cookie";
    $url = "http://localhost/index.php";
    
    
    function getSuperUser(){
        try{
            $conn = new PDO("mysql:host=".SENTRIFUGO_HOST.";dbname=".SENTRIFUGO_DBNAME, SENTRIFUGO_USERNAME, SENTRIFUGO_PASSWORD);
            $stmt = $conn->prepare("SELECT * FROM main_users where id=1");
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            return $stmt->fetch();
        }
        catch(Exception $e){
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
    
    
    $user = getSuperUser();
        
    $username =  $user['emailaddress'];
    $password =  $user['emppassword'];
       
    $ch = curl_init("$url/index/loginpopupsave");
    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=$username&password=$password");
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_STDERR, $fp);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiefile);
    curl_exec($ch);
        
        
    $ch = curl_init("$url/cronjob/$argv[1]");
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiefile);
    curl_exec($ch);
    

?>