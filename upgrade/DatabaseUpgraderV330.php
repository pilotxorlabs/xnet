<?php

require_once 'DatabaseUpgradeAbstract.php';

class DatabaseUpgrader_V330 extends DatabaseUpgrader_Abstract
{
    
    public function __construct(){
        $this->nextUpgrader = new DatabaseUpgrader_V331();
        $this->majorVersion = 3;
        $this->minorVersion = 3;
        $this->maintVersion = 0;
    }
    /**
     * {@inheritDoc}
     * @see DatabaseUpgrader_Abstract::updateSchema()
     */
    protected function updateSchema()
    {
		$query = "CREATE TABLE IF NOT EXISTS `tbl_logs_upload_paysilp` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`upload_id` int(11) NOT NULL,`filename` varchar(200) DEFAULT NULL,`status` enum('invalid','valid','upload failed','uploaded') DEFAULT NULL,`reason` varchar(500) DEFAULT NULL,`logged_at` timestamp NULL DEFAULT NULL,`uploaded_by` int(11) DEFAULT NULL,PRIMARY KEY (`id`)) ";
		$this->pdo->exec($query);

		/*$query = "ALTER TABLE main_leaverequest CHANGE COLUMN leavestatus leavestatus ENUM('Pending for approval', 'Approved', 'Rejected', 'Cancel', 'Cancelled') NULL DEFAULT 'Pending for approval'";
		$this->pdo->exec($query);
		
		$query = "update main_leaverequest set leavestatus = 5 where leavestatus = 4";
		$this->pdo->exec($query);
		
		$query = "ALTER TABLE main_leaverequest CHANGE COLUMN leavestatus leavestatus ENUM('Pending for approval', 'Approved', 'Rejected', 'Cancelled', 'Cancel') NULL DEFAULT 'Pending for approval'";
		$this->pdo->exec($query);*/
		
    }

    /**
     * {@inheritDoc}
     * @see DatabaseUpgrader_Abstract::executeQueries()
     */
    protected function executeQueries()
    {
        /*$query = "update main_leaverequest set leavestatus = 4 where leavestatus = 5";
        $this->pdo->exec($query);*/
        
    }

    
}

