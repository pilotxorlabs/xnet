<?php
/********************************************************************************* 

 *  Copyright (C) 2014 Sapplica
 *   

 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 \*  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  If not, see <http://www.gnu.org/licenses/>.
 *

 ********************************************************************************/ 

require_once '../application/modules/default/library/sapp/Global.php';
require_once '../public/constants.php';
require_once '../public/db_constants.php';
require_once '../public/db_upgrade_constant.php';

require_once 'UpgraderFactory.php';

$code = 200;

try {
    $pdo = new PDO('mysql:host='.SENTRIFUGO_HOST.';dbname='.SENTRIFUGO_DBNAME.'',SENTRIFUGO_USERNAME, SENTRIFUGO_PASSWORD,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    $stmt2 = $pdo->prepare("select version from main_patches_version where id = (select max(id) from main_patches_version where isactive = 1)");
    $stmt2->execute();
    $dbversion = $stmt2->fetch();
    
    $upgrader = Upgrader_Factory::produceUpgrader($dbversion['version']);
    $nextUpgrader = $upgrader->getNextUpgrader();
    if($nextUpgrader){
       update_constant("UPGRADING");
       $message = "Database successfully upgraded to " . $nextUpgrader->upgrade();
       update_constant("SUCCESS",$message);
    }else{
       $message = "Database is in sync with the code.";
    }
    
    echo json_encode(array(
        'status' => $code == 200,
        'message' => $message
    ));

}catch(Exception $e){
    $code = 500;
    update_constant("ERROR", $e->getMessage());
    echo json_encode(array(
        'status' => $code == 200,
        'message' => $e->getMessage()
    ));
}

function update_constant($status,$message  = '')
{
    $filename = "../public/db_upgrade_constant.php";
    if(file_exists($filename))
    {
        $db_content = "<?php
			       defined('DB_UPGRADE_STATUS') || define('DB_UPGRADE_STATUS','".$status."');
                   defined('DB_UPGRADE_MESSAGE') || define('DB_UPGRADE_MESSAGE','".$message."');
			     ?>";
        try{
            $handle = fopen($filename, "w+");
            fwrite($handle,trim($db_content));
            fclose($handle);
            return true;
        }
        catch (Exception $e)
        {
            
        }
    }
}
?>  		
