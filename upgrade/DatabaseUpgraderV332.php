<?php
require_once 'DatabaseUpgradeAbstract.php';

class DatabaseUpgrader_V332 extends DatabaseUpgrader_Abstract
{

    public function __construct()
    {
        $this->nextUpgrader = new DatabaseUpgrader_V334();
        $this->majorVersion = 3;
        $this->minorVersion = 3;
        $this->maintVersion = 2;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see DatabaseUpgrader_Abstract::updateSchema()
     */
    protected function updateSchema()
    {
        $query = "SET FOREIGN_KEY_CHECKS=0;
				  DROP TABLE IF EXISTS `main_notification_type`;
				  CREATE TABLE `main_notification_type` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `notification_type` varchar(255) NOT NULL,
				  `description` varchar(255),
				  `created_by` bigint(20) unsigned DEFAULT NULL,
				  `created_date` datetime DEFAULT NULL,
				  `modified_by` bigint(20) unsigned DEFAULT NULL,
				  `modified_date` datetime DEFAULT NULL,
				  `is_active` tinyint(1) DEFAULT '1',
				  PRIMARY KEY (`id`)) ENGINE=InnoDB;
				  SET FOREIGN_KEY_CHECKS=1";
        $this->pdo->exec($query);

        $query = "CREATE TABLE IF NOT EXISTS `main_notification` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `notification_type_id` int(11) NOT NULL,
                  `notification_date` date NOT NULL,
                  `notification_request_id` bigint(20) NOT NULL,
				  `created_by` bigint(20) unsigned DEFAULT NULL,
				  `created_date` datetime DEFAULT NULL,
				  `modified_by` bigint(20) unsigned DEFAULT NULL,
				  `modified_date` datetime DEFAULT NULL,
				  `is_active` tinyint(1) DEFAULT '1',
                  PRIMARY KEY (`id`),
                  CONSTRAINT `FK_main_notification` FOREIGN KEY (`notification_type_id`) REFERENCES main_notification_type(`id`) ON DELETE CASCADE
                )";
        $this->pdo->exec($query);
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see DatabaseUpgrader_Abstract::executeQueries()
     */
    protected function executeQueries()
    {
        $query = "INSERT INTO `main_notification_type` (`notification_type`, `description`,`created_date`,`modified_date`,`is_active`) VALUES ('Approved Leaves Reminder','This is used to send reminder for approved leaves.',NOW(),NOW(),1)";
        $this->pdo->exec($query);

        $query = "Select count(*) as count from main_menu Where menuName = 'Employee Requests'";
        $result = $this->pdo->query($query)->fetch();
        if ($result['count'] == 0) {
            $query = "INSERT INTO `main_menu` (`menuName`, `url`, `helpText`, `toolTip`, `iconPath`, `parent`, `menuOrder`, `nav_ids`, `isactive`, `modulename`, `segment_flag`) VALUES ('Employee Requests', '/employeerequests', 'Employee Requests', 'Employee Requests', '1346863776_vacation_request.jpg', '34', '3', ',4,34,223', '1', 'default', '2')";
            $this->pdo->exec($query);
            $query = "Select id  from main_menu Where menuName = 'Employee Requests'";
            $result = $this->pdo->query($query)->fetch();
            $query = "INSERT INTO `main_privileges` (`role`,`group_id`, `object`, `addpermission`, `editpermission`, `deletepermission`, `viewpermission`, `uploadattachments`, `viewattachments`, `createdby`, `modifiedby`, `isactive`,`customactionpermission`)
	    	VALUES ('1',NULL, '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
	    			,(NULL,'1', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
    				,(NULL,'2', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
    				,(NULL,'3', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
	    			,(NULL,'4', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
	    			,(NULL,'6', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')";
            $this->pdo->exec($query);
        }

        	$query = "Select id  from main_menu Where menuName = 'Employee Leave'";
        	$result = $this->pdo->query($query)->fetch();
        	if($result['id']!= null){
        	$query = "DELETE FROM main_privileges where object= ".$result['id'];
        	$this->pdo->exec($query);
        	$query = "DELETE FROM main_menu Where menuName = 'Employee Leave'";
        	$this->pdo->exec($query);

        }

        $query = "Select id  from main_menu Where menuName = 'Employee Exceptions'";
        $result = $this->pdo->query($query)->fetch();
        if($result['id']!= null){
        	$query = "DELETE FROM main_privileges where object= ".$result['id'];
        	$this->pdo->exec($query);
        	$query = "DELETE FROM main_menu Where menuName = 'Employee Exceptions'";
        	$this->pdo->exec($query);

        }

		$query = "UPDATE `main_durationformat` SET `description` = 'Hours and Minutes' WHERE `id` = 1";
		$this->pdo->exec ($query);
		$query = "UPDATE `main_durationformat` SET `description` = 'Hours, Minutes and Seconds' WHERE `id` = 2";
		$this->pdo->exec ($query);
		$query = "UPDATE `main_durationformat` SET `description` = 'Hours.Minutes' WHERE `id` = 3";
		$this->pdo->exec ($query);
	}
}

