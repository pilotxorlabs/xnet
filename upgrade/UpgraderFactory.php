<?php

require_once 'DatabaseUpgraderV320.php';
require_once 'DatabaseUpgraderV330.php';
require_once 'DatabaseUpgraderV331.php';
require_once 'DatabaseUpgraderV332.php';
require_once 'DatabaseUpgraderV334.php';
require_once 'DatabaseUpgraderV335.php';
require_once 'DatabaseUpgraderV336.php';
require_once 'DatabaseUpgraderV338.php';
require_once 'DatabaseUpgraderV340.php';
require_once 'DatabaseUpgraderV342.php';


class Upgrader_Factory
{
    public static function produceUpgrader($version){
        if($version === '3.2'){
            return new DatabaseUpgrader_V320();
        }else if($version == "3.3.0"){
            return new DatabaseUpgrader_V330();
        }else if($version == "3.3.1"){
        	return new DatabaseUpgrader_V331();
        }else if($version == "3.3.2"){
        	return new DatabaseUpgrader_V332();
        }else if($version == "3.3.4"){
            return new DatabaseUpgrader_V334();
        }
        else if($version == "3.3.5"){
        	return new DatabaseUpgrader_V335();
        }
        else if($version == "3.3.6"){
            return new DatabaseUpgrader_V336();
        }
        else if($version == "3.3.8"){
            return new DatabaseUpgrader_V338();
        }
        else if($version == "3.4.0"){
        	return new DatabaseUpgrader_V340();
        }
        else if($version == "3.4.2"){
        	return new DatabaseUpgrader_V342();
        }
    }
}

