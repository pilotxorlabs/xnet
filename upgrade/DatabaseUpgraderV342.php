<?php
require_once 'DatabaseUpgradeAbstract.php';

class DatabaseUpgrader_V342 extends DatabaseUpgrader_Abstract{
    
    public function __construct(){
        $this->nextUpgrader = null;
        $this->majorVersion = 3;
        $this->minorVersion = 4;
        $this->maintVersion = 2;
    }
    
    protected function updateSchema(){
    	if(! $this->checkColumnExists('tm_configuration','worklog_submission_emails')){
    		$query = "ALTER TABLE `tm_configuration` ADD COLUMN `worklog_submission_emails` enum('Disabled','Enabled') NOT NULL;";
    		$this->pdo->exec($query);
    	}
    }
    
    /**
    *
    * {@inheritdoc}
    *
    * @see DatabaseUpgrader_Abstract::executeQueries()
    */
    protected function executeQueries()
    {	
    	
    }
}