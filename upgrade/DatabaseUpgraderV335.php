<?php
require_once 'DatabaseUpgradeAbstract.php';

class DatabaseUpgrader_V335 extends DatabaseUpgrader_Abstract{
    
    public function __construct(){
        $this->nextUpgrader = new DatabaseUpgrader_V336();
        $this->majorVersion = 3;
        $this->minorVersion = 3;
        $this->maintVersion = 5;
    }
    
    protected function updateSchema(){
    	
    	$query = "DROP TABLE IF EXISTS `main_compensatory_off`;
        CREATE TABLE `main_compensatory_off` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`halfDay` int(11) DEFAULT NULL,
    	`fullDay` int(11) DEFAULT NULL,
    	`leaveWindow` int(11) DEFAULT NULL,
		`considerationWindow` varchar(255) DEFAULT NULL,
		`configuration_id` int(11) NOT NULL,
		PRIMARY KEY (`id`))";
    	$this->pdo->exec($query);
    	
    	if (! $this->checkColumnExists("main_compoffrequest","appliedleavescount")) {
	    	$query = "ALTER TABLE `main_compoffrequest` ADD COLUMN `appliedleavescount` float";
	    	$this->pdo->exec($query);
    	}
    }
    
    /**
    *
    * {@inheritdoc}
    *
    * @see DatabaseUpgrader_Abstract::executeQueries()
    */
    protected function executeQueries()
    {
		$query = "UPDATE main_compoffrequest SET appliedleavescount = '1' WHERE appliedleavescount is NULL";
    	$this->pdo->exec($query);
		
     	$query = "Select count(*) as count from main_configuration_types where configuration_type_id = 4";
		$result = $this->pdo->query ( $query )->fetch();
    	
    	if ($result['count'] == 0)
    	{
    		$query = "INSERT INTO main_configuration_types (configuration_type_id,configuration_name) VALUES ('4','Compensatory Off')";
    		$this->pdo->exec ( $query );
    	}
    	

    	if (! $this->checkColumnExists("main_organisationinfo","probation")) {
    		$query = "ALTER TABLE `main_organisationinfo` ADD COLUMN `probation` INT(11) NULL DEFAULT 6";
    		$this->pdo->exec ( $query );
    	}
    	if (! $this->checkColumnExists("main_businessunits","probation")) {
    		$query = "ALTER TABLE `main_businessunits` ADD COLUMN `probation` INT(11) NULL DEFAULT 6";
    		$this->pdo->exec ( $query );
    	}
    	if (! $this->checkColumnExists("main_departments","probation")) {
    		$query = "ALTER TABLE `main_departments` ADD COLUMN `probation` INT(11) NULL DEFAULT 6";
    		$this->pdo->exec ( $query );
    	} 	 				
    }
}