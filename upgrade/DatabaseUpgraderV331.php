<?php
require_once 'DatabaseUpgradeAbstract.php';

class DatabaseUpgrader_V331 extends DatabaseUpgrader_Abstract
{

    public function __construct()
    {
        $this->nextUpgrader = new DatabaseUpgrader_V332();
        $this->majorVersion = 3;
        $this->minorVersion = 3;
        $this->maintVersion = 1;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see DatabaseUpgrader_Abstract::updateSchema()
     */
    protected function updateSchema()
    {
        $query = "DROP TABLE IF EXISTS `main_compoff_history`;
        CREATE TABLE `main_compoff_history` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `compoff_request_id` int(11) unsigned DEFAULT NULL,
        `description` varchar(500) DEFAULT NULL,
        `createdby` int(11) DEFAULT NULL,
        `modifiedby` int(11) DEFAULT NULL,
        `createddate` datetime DEFAULT NULL,
        `modifieddate` datetime DEFAULT NULL,
        `isactive` tinyint(1) DEFAULT NULL,
        PRIMARY KEY (`id`)) ENGINE=InnoDB;";
        $this->pdo->exec($query);

        $query = "DROP TABLE IF EXISTS `main_compoff_summary`;
        CREATE TABLE `main_compoff_summary` (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `compoff_request_id` bigint(20) unsigned DEFAULT NULL,
        `user_id` bigint(20) DEFAULT NULL,
        `user_name` varchar(255) DEFAULT NULL,
        `department_id` bigint(20) unsigned DEFAULT NULL,
        `department_name` varchar(255) DEFAULT NULL,
        `bunit_id` bigint(20) unsigned DEFAULT NULL,
        `buss_unit_name` varchar(255) DEFAULT NULL,
        `approver_comments` text,
        `option` enum('Leave','Encashment') DEFAULT NULL,
        `leave_date` date DEFAULT NULL,
        `notify_me_on` date DEFAULT NULL,
        `compoffstatus` enum('Pending for Approval','Approved','Rejected','Cancelled') DEFAULT NULL,
        `rep_mang_id` int(11) unsigned DEFAULT NULL,
        `rep_manager_name` varchar(255) DEFAULT NULL,
        `hr_id` int(11) DEFAULT NULL,
        `hr_name` varchar(255) DEFAULT NULL,
        `createdby` int(11) unsigned DEFAULT NULL,
        `modifiedby` int(11) unsigned DEFAULT NULL,
        `createddate` datetime DEFAULT NULL,
        `modifieddate` datetime DEFAULT NULL,
        `isactive` tinyint(1) DEFAULT NULL,
         PRIMARY KEY (`id`)) ENGINE=InnoDB";
        $this->pdo->exec($query);

        $query = "DROP TABLE IF EXISTS `main_compoffrequest`;
        CREATE TABLE `main_compoffrequest` (
        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `user_id` bigint(20) unsigned DEFAULT NULL,
        `approver_comments` text,
        `option` enum('Leave','Encashment') DEFAULT NULL,
        `leave_date` date DEFAULT NULL,
        `avail_date` date DEFAULT NULL,
        `status` enum('Pending for approval','Approved','Rejected','Cancelled') DEFAULT NULL,
        `rep_mang_id` int(11) unsigned DEFAULT NULL,
        `hr_id` int(11) DEFAULT NULL,
        `createdby` int(11) unsigned DEFAULT NULL,
        `modifiedby` int(11) unsigned DEFAULT NULL,
        `createddate` datetime DEFAULT NULL,
        `modifieddate` datetime DEFAULT NULL,
        `isactive` tinyint(1) DEFAULT NULL,
        `user_comments` text,
        PRIMARY KEY (`id`)) ENGINE=InnoDB";
        $this->pdo->exec($query);

        $query = "DROP TABLE IF EXISTS `main_attendance_configuration`;
        CREATE TABLE `main_attendance_configuration` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`business_unit_id` int(11) DEFAULT NULL,
		`configuration_type_id` int(11) DEFAULT NULL,
		PRIMARY KEY (`id`))";
        $this->pdo->exec($query);

        $query = "DROP TABLE IF EXISTS `main_office_hours`;
        CREATE TABLE `main_office_hours` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`work_culture` varchar(255) DEFAULT NULL,
		`start_time` time DEFAULT NULL,
		`end_time` time DEFAULT NULL,
		`entry_time` time DEFAULT NULL,
		`entry_time_action` varchar(155) DEFAULT NULL,
		`exit_time` time DEFAULT NULL,
		`exit_time_action` varchar(255) DEFAULT NULL,
		`configuration_id` int(11) NOT NULL,
		`is_entry_threshold` tinyint(1) DEFAULT NULL,
		`is_exit_threshold` varchar(45) DEFAULT NULL,
		PRIMARY KEY (`id`))";
        $this->pdo->exec($query);

        $query = "DROP TABLE IF EXISTS `main_mandatory_hours`;
        CREATE TABLE `main_mandatory_hours` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`duration` enum('Half Day','Daily','Weekly','Monthly','Quarterly','Half Yearly','Yearly') DEFAULT NULL,
		`minimum_hours` int(11) DEFAULT NULL,
		`minimum_hours_action` varchar(255) DEFAULT NULL,
		`maximum_hours` int(11) DEFAULT NULL,
		`maximum_hours_action` varchar(255) DEFAULT NULL,
		`configuration_id` int(11) NOT NULL,
		PRIMARY KEY (`id`))";
        $this->pdo->exec($query);

        $query = "DROP TABLE IF EXISTS `main_configuration_types`;
        CREATE TABLE `main_configuration_types`(
		`configuration_type_id` int(11) NOT NULL AUTO_INCREMENT,
		`configuration_name` varchar(255) DEFAULT NULL,
		PRIMARY KEY (`configuration_type_id`))";
        $this->pdo->exec($query);

        $query = "DROP TABLE IF EXISTS `main_attendance_action`;
        CREATE TABLE `main_attendance_action` (
		`action_id` int(11) NOT NULL,
		`action_name` varchar(255) DEFAULT NULL,
		PRIMARY KEY (`action_id`))";
        $this->pdo->exec($query);

        $query = "DROP TABLE IF EXISTS `main_missed_punches`;
        CREATE TABLE `main_missed_punches` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`threshold` int(11) DEFAULT NULL,
		`action` varchar(255) DEFAULT NULL,
		`configuration_id` int(11) NOT NULL,
		PRIMARY KEY (`id`))";
        $this->pdo->exec($query);

        $query = "DROP TABLE IF EXISTS `main_durationformat`;
        CREATE TABLE `main_durationformat` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`durationformat` varchar(20) DEFAULT NULL,
		`example` varchar(30) DEFAULT NULL,
		`description` varchar(60) DEFAULT NULL,
		`isactive` int(11) DEFAULT NULL,
		PRIMARY KEY (`id`))";
        $this->pdo->exec($query);

		$query = "SET FOREIGN_KEY_CHECKS=0;
        DROP TABLE IF EXISTS `tm_activities`;
        CREATE TABLE `tm_activities` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `activity` varchar(100) NOT NULL,
        `activity_code` varchar(45) NOT NULL,
        PRIMARY KEY (`id`)
        ) ";
        $this->pdo->exec($query);
		
        $query = "CREATE TABLE IF NOT EXISTS `tm_worklogs` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `user_id` int(11),
        `work_id_1` varchar(45) NOT NULL,
        `work_id_2` varchar(45),
        `summary` text NOT NULL,
        `work_hours` float(10,2) NOT NULL,
        `work_date` date NOT NULL,
        `activity_id` bigint(20) NOT NULL,
        `project_id` bigint(20) NOT NULL,
        `task_id` bigint(20) NOT NULL,
        `created` datetime DEFAULT NULL,
        `modified` datetime DEFAULT NULL,
        `is_enabled` tinyint(1) DEFAULT '1',
        `is_active` tinyint(1) DEFAULT '1',
        PRIMARY KEY (`id`))";
        $this->pdo->exec($query);

        $query = "CREATE TABLE IF NOT EXISTS `main_emp_work_activity` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `user_id` int(11) NOT NULL,
                  `punch_time` datetime NOT NULL,
                  `status` enum('WFH','OFFICE') NOT NULL,
                  `machine_id` int(11) DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `id_UNIQUE` (`id`)
                )";
		$this->pdo->exec ( $query );
		
		if (! $this->checkColumnExists("main_privileges","customactionpermission")) {
		    $query = "ALTER TABLE `main_privileges` ADD COLUMN `customactionpermission` VARCHAR(10) NULL DEFAULT 'No'";
		    $this->pdo->exec ( $query );
		}
		if(! $this->checkColumnExists("main_employeeleavetypes","isclubbable")){
		    $query= "ALTER TABLE `main_employeeleavetypes` ADD COLUMN `isclubbable` int(11) DEFAULT '1'";
		    $this->pdo->exec ($query);
		}
		if(! $this->checkColumnExists("main_sitepreference","durationformatid")){
		    $query = "ALTER TABLE `main_sitepreference` ADD COLUMN `durationformatid` int(10) Unique ";
		    $this->pdo->exec($query);
		}
		
		if (! $this->checkColumnExists("tm_configuration","worklog_locking_period")) {
		    $query = "ALTER TABLE `tm_configuration` ADD `worklog_locking_period` VARCHAR(100) NOT NULL AFTER `ts_block_dates_range`";
		    $this->pdo->exec ( $query );
		}
	}
	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see DatabaseUpgrader_Abstract::executeQueries()
	 */
	protected function executeQueries() {
	    $query = "Select count(*) as count from main_menu Where menuName = 'Attendance Configuration'";
	    $result = $this->pdo->query ( $query )->fetch();
	    if($result['count'] == 0){
    		$query = "INSERT INTO `main_menu` (`menuName`, `url`, `helpText`, `toolTip`, `iconPath`, `parent`, `menuOrder`, `nav_ids`, `isactive`, `modulename`, `segment_flag`) VALUES ('Attendance Configuration', '/attendanceconfiguration', 'Attendance Configuration', 'Attendance Configuration', 'attendance-configuration.jpg', '113', '18', ',3,113,224', '1', 'default', '2')";
    		$this->pdo->exec ( $query );    		
    		$query = "Select id  from main_menu Where menuName = 'Attendance Configuration'";
    		$result = $this->pdo->query ( $query )->fetch();
    		$query = "INSERT INTO `main_privileges` (`role`, `object`, `addpermission`, `editpermission`, `deletepermission`, `viewpermission`, `uploadattachments`, `viewattachments`, `createdby`, `modifiedby`, `isactive`) VALUES ('1', '". $result['id'] ."', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', '1', '1', '1')";
    		$this->pdo->exec($query);
    		$query = "INSERT INTO `main_privileges` (`group_id`, `object`, `addpermission`, `editpermission`, `deletepermission`, `viewpermission`, `uploadattachments`, `viewattachments`, `createdby`, `modifiedby`, `isactive`)
	    	VALUES ('1', '". $result['id'] ."', 'Yes', 'yes', 'yes', 'Yes', 'No', 'No', '1', '1', '1')
    				,('2', '". $result['id'] ."', 'yes', 'yes', 'yes', 'Yes', 'No', 'No', '1', '1', '1')
    				,('3', '". $result['id'] ."', 'yes', 'yes', 'yes', 'Yes', 'No', 'No', '1', '1', '1')
	    			,('4', '". $result['id'] ."', 'yes', 'yes', 'yes', 'Yes', 'No', 'No', '1', '1', '1')
	    			,('6', '". $result['id'] ."', 'yes', 'yes', 'yes', 'Yes', 'No', 'No', '1', '1', '1')";
    		$this->pdo->exec($query);
	    }

        $query = "Select count(*) as count from main_menu Where menuName = 'Employee Requests'";
        $result = $this->pdo->query($query)->fetch();
        if ($result['count'] == 0) {
            $query = "INSERT INTO `main_menu` (`menuName`, `url`, `helpText`, `toolTip`, `iconPath`, `parent`, `menuOrder`, `nav_ids`, `isactive`, `modulename`, `segment_flag`) VALUES ('Employee Requests', '/employeerequests', 'Employee Requests', 'Employee Requests', '1346863776_vacation_request.jpg', '34', '3', ',4,34,223', '1', 'default', '2')";
            $this->pdo->exec($query);
            $query = "Select id  from main_menu Where menuName = 'Employee Requests'";
            $result = $this->pdo->query($query)->fetch();
            $query = "INSERT INTO `main_privileges` (`role`,`group_id`, `object`, `addpermission`, `editpermission`, `deletepermission`, `viewpermission`, `uploadattachments`, `viewattachments`, `createdby`, `modifiedby`, `isactive`,`customactionpermission`)
	    	VALUES ('1',NULL, '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
	    			,(NULL,'1', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
    				,(NULL,'2', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
    				,(NULL,'3', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
	    			,(NULL,'4', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
	    			,(NULL,'6', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')";
            $this->pdo->exec($query);
        }

        $query = "INSERT INTO `main_attendance_action` (`action_id`,`action_name`) VALUES ('1','Default to threshold')";
        $this->pdo->exec($query);

        $query = "INSERT INTO `main_attendance_action` (`action_id`,`action_name`) VALUES ('2','Absent')";
        $this->pdo->exec($query);

        $query = "INSERT INTO `main_attendance_action` (`action_id`,`action_name`) VALUES ('3','Half Day')";
        $this->pdo->exec($query);

        $query = "INSERT INTO `main_attendance_action` (`action_id`,`action_name`) VALUES ('4','Ignore')";
        $this->pdo->exec($query);

        $query = "INSERT INTO `main_configuration_types` (`configuration_type_id`,`configuration_name`) VALUES ('1','Office Hours')";
        $this->pdo->exec($query);

        $query = "INSERT INTO `main_configuration_types` (`configuration_type_id`,`configuration_name`) VALUES ('2','Mandatory Hours')";
        $this->pdo->exec($query);

        $query = "INSERT INTO `main_configuration_types` (`configuration_type_id`,`configuration_name`) VALUES ('3','Missed Punches');";
        $this->pdo->exec($query);
        
        $query = "INSERT INTO `tm_activities` (`activity`, `activity_code`) VALUES 
        ('Development Activity','DEV'),('Testing Activity','TES'),
        ('Automation Activity','AUT'),('HLD Activity','HLD'),
        ('LLD Activity','LLD'),('POC Activity','POC'),
        ('Regression Activity','REG'),('Training Activity','TRA'),
        ('Meeting Activity','MTG'),('Call Activity','CAL'),
        ('Delivery related Activities','DEL'),('Re-Deliver Activity','RED'),
        ('Functional Review Activity','FRV'),('Code Review Activity','CRV'),
        ('Requirement Analysis','RAA'),('Unit Test Activity','UNT'),
        ('Code Check Activity','CCN'),('HLD Review Activity','HLR'),
        ('LLD Review Activity','LLR'),('POC Review Activity','POR'),
        ('No Activity','NOC'),('Build related Activities','BUL'),
        ('Support Activity','SUP'),('Planning Activity','PLN'),
        ('Leave','LEV'),('Halt due to Infrastructure Issues','IIH'),
        ('Test case writing','TSW'),('Test case execution','TSE'),
        ('Issue reproduction','ISR'),('Other QA tasks (like: Regression sheet creation, env setup)','QAT'),
        ('Adhoc Tasks (Any tasks which is not related to project)','ADH')";
        $this->pdo->exec($query);

        $query = "INSERT INTO `main_durationformat`
				(`durationformat`,
				`example`,
				`description`,
				`isactive`)
				VALUES
				(
				 'NNh NNm',
				'9h 10m',
				'hours and minutes',
				'1')";

        $this->pdo->exec($query);

        $query = "INSERT INTO `main_durationformat`
				(`durationformat`,
				`example`,
				`description`,
				`isactive`)
				VALUES
				(
				 'NNh NNm NNs',
				'9h 10m 30s',
				'Hours minutes and seconds',
				'1')";

        $this->pdo->exec($query);

        $query = "INSERT INTO `main_durationformat`
				(`durationformat`,
				`example`,
				`description`,
				`isactive`)
				VALUES
				(
				 'NN.NN',
				'9.10',
				'hours.minutes',
				'1')";

        $this->pdo->exec($query);
    }
}

