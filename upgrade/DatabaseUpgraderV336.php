<?php
require_once 'DatabaseUpgradeAbstract.php';

class DatabaseUpgrader_V336 extends DatabaseUpgrader_Abstract{
    
    public function __construct(){
        $this->nextUpgrader = new DatabaseUpgrader_V338();
        $this->majorVersion = 3;
        $this->minorVersion = 3;
        $this->maintVersion = 6;
    }
    
    protected function updateSchema(){
    	    	    	
    	if(! $this->checkColumnExists('tm_configuration','workhours')){
    	    $query = "ALTER TABLE `tm_configuration` ADD COLUMN `workhours` FLOAT NULL DEFAULT 8 AFTER `modified`;";
    	    $this->pdo->exec($query);
    	}
    	
    	$query = "DROP TABLE IF EXISTS `main_empprobationdetails`;
        CREATE TABLE `main_empprobationdetails` (
		  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		  `user_id` bigint(20) DEFAULT NULL,
		  `start_date` date DEFAULT NULL,
		  `end_date` date DEFAULT NULL,
		  `duration` bigint(20) DEFAULT NULL,
		  `status` enum('Yet to Start','In progress','Complete'),
		  `comments` varchar(500) DEFAULT NULL,
		  `isactive` tinyint(1) DEFAULT '1',
		   PRIMARY KEY (`id`))";
    	$this->pdo->exec($query);
    }
    
    /**
    *
    * {@inheritdoc}
    *
    * @see DatabaseUpgrader_Abstract::executeQueries()
    */
    protected function executeQueries()
    {
		
		$query = "Select count(*) as count from main_menu Where menuName = 'Worklog Report'";
    	$result = $this->pdo->query ( $query )->fetch();
    	if($result['count'] == 0){
    	    $query = "INSERT INTO `main_menu` (`menuName`, `url`, `helpText`, `toolTip`, `iconPath`, `parent`, `menuOrder`, `nav_ids`, `isactive`, `modulename`, `segment_flag`) VALUES ('Worklog Report', '/worklogreport', 'Worklog Report', 'Worklog Report', 'Attendance_Report.png', '223', '2', ',2,223,226', '1', 'default', '2')";
    	    $result = $this->pdo->exec ( $query );
	        $query = "Select id  from main_menu Where menuName = 'Worklog Report'";
	        $result = $this->pdo->query($query)->fetch();
	        $query = "INSERT INTO `main_privileges` (`role`,`group_id`, `object`, `addpermission`, `editpermission`, `deletepermission`, `viewpermission`, `uploadattachments`, `viewattachments`, `createdby`, `modifiedby`, `isactive`,`customactionpermission`)
    	         VALUES ('1',NULL, '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
    			,(NULL,'1', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
				,(NULL,'2', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
				,(NULL,'3', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
    			,(NULL,'4', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')
    			,(NULL,'6', '" . $result['id'] . "', 'No', 'No', 'No', 'Yes', 'No', 'No', '1', '1', '1','NO')";
	        $this->pdo->exec($query);
	       
    	}
    	$query = "Select count(*) as count from main_menu Where menuName = 'Employee Leave Summary'";
    	$result = $this->pdo->query($query)->fetch();
    	if ($result['count'] != 0) {
    		$query = "Select id  from main_menu Where menuName = 'Employee Leave Summary'";
    		$result = $this->pdo->query($query)->fetch();
    		$query = "UPDATE `main_privileges` SET `editpermission`='Yes', `deletepermission`='Yes' WHERE (`role`='1' OR `group_id` = '3') AND `object` = '" . $result['id'] . "'";
    		$this->pdo->exec($query);
    	}
    	
    }
}