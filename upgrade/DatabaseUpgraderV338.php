<?php
require_once 'DatabaseUpgradeAbstract.php';

class DatabaseUpgrader_V338 extends DatabaseUpgrader_Abstract{
    
    public function __construct(){
        $this->nextUpgrader = new DatabaseUpgrader_V340();
        $this->majorVersion = 3;
        $this->minorVersion = 3;
        $this->maintVersion = 8;
    }
    
    protected function updateSchema(){
    	    	    	
    	if(! $this->checkColumnExists('main_empprobationdetails','notify_on')){
    	    $query = "ALTER TABLE `main_empprobationdetails` ADD COLUMN `notify_on` DATE NULL AFTER `isactive`;";
    	    $this->pdo->exec($query);
    	}    	
    	
    }
    
    /**
    *
    * {@inheritdoc}
    *
    * @see DatabaseUpgrader_Abstract::executeQueries()
    */
    protected function executeQueries()
    {
        $query = "Select count(*) as count from main_roles Where rolename = 'Service'";
        $result = $this->pdo->query ( $query )->fetch();
        if($result['count'] == 0){
            $query = " INSERT INTO `main_roles` (`rolename`, `roletype`, `levelid`, `createdby`, `modifiedby`, `createddate`, `modifieddate`, `isactive`) VALUES ('Service', 'Service', '0', '0', '0', '2015-07-29 08:45:49', '2015-07-29 08:45:49', '1');";
            $result = $this->pdo->exec ( $query );
        }
        $query = "Select id from main_roles Where rolename = 'Service'";
        $serviceRole = $this->pdo->query ( $query )->fetch();
        $query = "Select count(*) as count from main_users Where emprole =".$serviceRole['id'];
        $result = $this->pdo->query ( $query )->fetch();
        if($result['count'] == 0){
            $query = "INSERT INTO `main_users` (`emprole`, `userstatus`, `firstname`, `lastname`, `userfullname`, `emailaddress`, `backgroundchk_status`, `emptemplock`, `emppassword`, `createdby`, `modifiedby`, `createddate`, `modifieddate`, `isactive`, `employeeId`, `tourflag`, `themes`) VALUES ('".$serviceRole['id']."', 'old', 'Service', 'Man', 'Service Man', 'service@xorlab.com', 'Yet to start', '0', '833e37d0911f6a0e1ca1e0f1e2651439', '1', '1', '2018-09-19 07:27:40', '2018-09-19 07:33:30', '1', 'XOR000', '1', 'default');";
            $result = $this->pdo->exec ( $query );
        }
        
        $query = "Select id from main_menu Where menuName = 'Employee Requests'";
        $result = $this->pdo->query($query)->fetch();
        if ($result['id'] != NULL) {
        	$query = "UPDATE `main_menu` SET `nav_ids`=',4,34,".$result['id'] ."' WHERE `id`='" . $result['id'] . "'";
        	$this->pdo->exec($query);
        }
		
    	
    }
}