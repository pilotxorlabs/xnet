<?php
require_once 'DatabaseUpgradeAbstract.php';

class DatabaseUpgrader_V340 extends DatabaseUpgrader_Abstract{
    
    public function __construct(){
        $this->nextUpgrader = new DatabaseUpgrader_V342();
        $this->majorVersion = 3;
        $this->minorVersion = 4;
        $this->maintVersion = 0;
    }
    
    protected function updateSchema(){
    	$query = "DROP TABLE IF EXISTS `main_exit_approval_line`;
        CREATE TABLE `main_exit_approval_line` (
		  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		  `approvalline` varchar(500) DEFAULT NULL,
		  `businessunit_id` int(11) DEFAULT NULL,
          `department_id` int(11) DEFAULT NULL,
		  `isactive` tinyint(1) DEFAULT '1',
		   PRIMARY KEY (`id`))";
    	$this->pdo->exec($query);
    	
    	$query = "DROP TABLE IF EXISTS `main_exit_additional_settings`;
        CREATE TABLE `main_exit_additional_settings` (
		  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		  `businessunit_id` int(11) DEFAULT NULL,
          `department_id` int(11) DEFAULT NULL,
          `approval_line` int(11) DEFAULT NULL,
          `approval_manager_id` int(11) DEFAULT NULL,
		  `isactive` tinyint(1) DEFAULT '1',
		   PRIMARY KEY (`id`))";
    	$this->pdo->exec($query);
    	
    	$query = "DROP TABLE IF EXISTS `main_exit_additional_process`;
        CREATE TABLE `main_exit_additional_process` (
		  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		  `exit_id` int(11) DEFAULT NULL,
          `approval_line_manager` int(11) DEFAULT NULL,
          `approval_line_status` enum('Pending','Approved','Rejected') DEFAULT 'Pending',
          `approval_line_comments` varchar(1000) DEFAULT NULL,
		  `isactive` tinyint(1) DEFAULT '1',
		   PRIMARY KEY (`id`))";
    	$this->pdo->exec($query);
    }
    
    /**
    *
    * {@inheritdoc}
    *
    * @see DatabaseUpgrader_Abstract::executeQueries()
    */
    protected function executeQueries()
    {	
    	
    }
}