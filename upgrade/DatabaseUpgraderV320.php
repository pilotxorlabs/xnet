<?php

require_once 'DatabaseUpgradeAbstract.php';

class DatabaseUpgrader_V320 extends DatabaseUpgrader_Abstract
{
    
    public function __construct(){
        $this->nextUpgrader = new DatabaseUpgrader_V330();
        $this->majorVersion = 3;
        $this->minorVersion = 2;
        $this->maintVersion = 0;
    }
    /**
     * {@inheritDoc}
     * @see DatabaseUpgrader_Abstract::updateSchema()
     */
    protected function updateSchema()
    {
        // TODO Auto-generated method stub
        
    }

    /**
     * {@inheritDoc}
     * @see DatabaseUpgrader_Abstract::executeQueries()
     */
    protected function executeQueries()
    {
        // TODO Auto-generated method stub
        
    }

    
    

}

