<?php
require_once 'DatabaseUpgradeAbstract.php';

class DatabaseUpgrader_V334 extends DatabaseUpgrader_Abstract{
    
    public function __construct(){
        $this->nextUpgrader = new DatabaseUpgrader_V335();
        $this->majorVersion = 3;
        $this->minorVersion = 3;
        $this->maintVersion = 4;
    }
    
    protected function updateSchema(){
      if (! $this->checkColumnExists("main_employeeleavetypes","isallocatedinprobation")) {
            $query = "ALTER TABLE `main_employeeleavetypes` ADD COLUMN `isallocatedinprobation` TINYINT(1) NOT NULL DEFAULT 2 AFTER `isclubbable`";
            $this->pdo->exec ( $query );
        }    
    }
    
    /**
    *
    * {@inheritdoc}
    *
    * @see DatabaseUpgrader_Abstract::executeQueries()
    */
    protected function executeQueries()
    {
      
        
		$query = "Update main_menu set iconPath = 'Attendance_Report.png' where menuName = 'Attendence Report'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'Attendance.png' where menuName = 'Attendance'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'Download_Pay_slip.png' where menuName = 'Download Payslip'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'Employee_Exceptions.png' where menuName = 'Employee Exceptions'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'Employee_Request.png' where menuName = 'Employee Requests'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'Exception_Request.png' where menuName = 'Exception Request'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'Finance.png' where menuName = 'Finance'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'My_Attendance.png' where menuName = 'My Attendance'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'My_Exceptions.png' where menuName = 'My Exceptions'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'My_Team_Attendance.png' where menuName = 'My Team Attendance'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'My_Team_Members.png' where menuName = 'My Team Members'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'reports.png' where menuName = 'Reports'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'Upload_Pay_slip.png' where menuName = 'Upload Payslip'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = '1346866425_time_zone.png' where menuName = 'Time Zones'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = '1346866486_days.png' where menuName = 'Days List'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = '1346866587_currency.png' where menuName = 'Currency'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = '1346866596_currency_converter.png' where menuName = 'Currency Conversions'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = '1346869824_months.png' where menuName = 'Months List'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = '1346869916_job-titlesb.png' where menuName = 'Job Titles'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = '1346870194_pending-vacation-requests.png' where menuName = 'My Leave'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = '1346871311_number_format.png' where menuName = 'Number Formats'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = '1346871964_emp_screening_-_updated.png' where menuName = 'Employee/Candidate Screening'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = '1346871975_screening_type_-_updated.png' where menuName = 'Screening Types'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = '1346933046_marital_status.png' where menuName = 'Marital Status'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = '1346933620_all-employees.png' where menuName = 'Employees'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = '1347027390_countries.png' where menuName = 'Countries'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = '1347027817_my_team_performance_appraisal.png' where menuName = 'Team Appraisal History'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'agency-list.png' where menuName = 'Agencies'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'announcements.png' where menuName = 'Announcements'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'appraisal_settings.png' where menuName = 'Appraisal Settings'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'appraisal-skills.png' where menuName = 'Appraisals'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'appraise_your_manager.png' where menuName = 'Appraise Your Manager'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'approved-requisitions.png' where menuName = 'Approved Requisitions'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'attendance-status.png' where menuName = 'Attendance Status'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'bank-account.png' where menuName = 'Bank Account Types'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'categories.png' where menuName = 'Categories'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'cities.png' where menuName = 'Cities'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'competency-level.png' where menuName = 'Competency Levels'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'configurations.png' where menuName = 'Configuration'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'configurations.png' where menuName = 'General'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'cv-management.png' where menuName = 'Candidates'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'department-icon.png' where menuName = 'Departments'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'email-contacts.png' where menuName = 'Email Contacts'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'employee_status.png' where menuName = 'Employment Status'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'employee_status.png' where menuName = 'Employee Status'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'employee-leaves-summary.png' where menuName = 'Employee Leave Summary'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'employee-tabs.png' where menuName = 'Employee Tabs'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'ethnic-codes.png' where menuName = 'Ethnic Codes'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'exit-types.png' where menuName = 'Exit Types'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'geo-groups.png' where menuName = 'Geo Groups'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'identity-codes.png' where menuName = 'Identity Codes'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'identity-documents.png' where menuName = 'Identity Documents'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'initialize_appraisal.png' where menuName = 'Initialize Appraisal'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'initialize_feedforward.png' where menuName = 'Initialize Feedforward'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'key_performance_indicator.png' where menuName = 'KPI List'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'languages.png' where menuName = 'Languages'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'leave-management-options.png' where menuName = 'Leave Management'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'leave-management-options.png' where menuName = 'Leave Management Options'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'leave-types.png' where menuName = 'Leave Types'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'licence-type.png' where menuName = 'License Types'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'manage-holiday-dates.png' where menuName = 'Manage Holidays'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'manage-holiday-group.png' where menuName = 'Manage Holiday Group'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'manage-module.png' where menuName = 'Modules'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'manage-module.png' where menuName = 'Service Request'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'manager_status.png' where menuName = 'Manager Status'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'manage-users.png' where menuName = 'External Users'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'military-service-type.png' where menuName = 'Military Service Types'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'my_team_appraisal.png' where menuName = 'My Team Appraisal'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'my-details-done.png' where menuName = 'My Details'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'my-holiday-calendar.png' where menuName = 'My Holiday Calendar'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'my-team.png' where menuName = 'My Team'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'nationality.png' where menuName = 'Nationalities'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'nationality-contex-code.png' where menuName = 'Nationality Context Codes'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'organization-hierarchy.png' where menuName = 'Organization Hierarchy'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'organization-structure.png' where menuName = 'Organization Structure'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'parameters.png' where menuName = 'Parameters'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'performance_appraisal.png' where menuName = 'Performance Appraisal'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'performance_appraisal.png' where menuName = 'My Performance Appraisal'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'performance_appraisal.png' where menuName = 'Appraisals'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'positions.png' where menuName = 'Positions'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'prefix.png' where menuName = 'Prefixes'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'questions.png' where menuName = 'Questions'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'race-codes.png' where menuName = 'Race Codes'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'ratings.png' where menuName = 'Ratings'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'rejected-leaves.png' where menuName = 'Rejected Leaves'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'rejected-requisitions.png' where menuName = 'Rejected Requisitions'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'remuneration_basis.png' where menuName = 'Remuneration Basis'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'reports.png' where menuName = 'Analytics'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'reports.png' where menuName = 'Reports'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'request.png' where menuName = 'Request Types'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'roles-privileges.png' where menuName = 'Roles & Privileges'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'schedule-interview.png' where menuName = 'Interviews'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'self_appraisal.png' where menuName = 'Self Appraisal'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'settings.png' where menuName = 'Settings'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'site-preferences.png' where menuName = 'Site Preferences'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'skills.png' where menuName = 'Skills'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'states.png' where menuName = 'States'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'time-management.png' where menuName = 'Time'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'veteran-status.png' where menuName = 'Veteran Status'";
		$this->pdo->exec ($query);
		
		$query = "Update main_menu set iconPath = 'work-eligibility-document.png' where menuName = 'Work Eligibility Document Types'";
		$this->pdo->exec ($query);

		$query = "Update main_menu set menuName = 'Attendance Report', url = '/attendancereport', helpText = 'Attendance Report' where menuName = 'Attendence Report'";
		$this->pdo->exec ($query);
		
		if (! $this->checkColumnExists("main_leaverequest","leavestatusnew")) {
			$query = "ALTER TABLE `main_leaverequest` ADD COLUMN `leavestatusnew` enum('Pending','Approved','Rejected','Cancelled')";
			$this->pdo->exec ( $query );
		}
		
		$query = "UPDATE main_leaverequest SET leavestatusnew = leavestatus+0 where leavestatus != ''";
		$this->pdo->exec ($query);
		
		$query = "ALTER TABLE main_leaverequest DROP leavestatus";
		$this->pdo->exec ($query);
		
		$query = "ALTER TABLE main_leaverequest CHANGE `leavestatusnew` `leavestatus` enum('Pending','Approved','Rejected','Cancelled')";
		$this->pdo->exec ($query);
		
		if (! $this->checkColumnExists("main_leaverequest_summary","leavestatusnew")) {
			$query = "ALTER TABLE `main_leaverequest_summary` ADD COLUMN `leavestatusnew` enum('Pending','Approved','Rejected','Cancelled')";
			$this->pdo->exec ( $query );
		}
		
		$query = "UPDATE main_leaverequest_summary SET leavestatusnew = leavestatus+0 where leavestatus != ''";
		$this->pdo->exec ($query);
		
		$query = "ALTER TABLE main_leaverequest_summary DROP leavestatus";
		$this->pdo->exec ($query);
		
		$query = "ALTER TABLE main_leaverequest_summary CHANGE `leavestatusnew` `leavestatus` enum('Pending','Approved','Rejected','Cancelled')";
		$this->pdo->exec ($query);
		
		if (! $this->checkColumnExists("main_compoffrequest","statusnew")) {
			$query = "ALTER TABLE `main_compoffrequest` ADD COLUMN `statusnew` enum('Pending','Approved','Rejected','Cancelled')";
			$this->pdo->exec ( $query );
		}
		
		$query = "UPDATE main_compoffrequest SET statusnew = status+0 where status != ''";
		$this->pdo->exec ($query);
		
		$query = "ALTER TABLE main_compoffrequest DROP status";
		$this->pdo->exec ($query);
		
		$query = "ALTER TABLE main_compoffrequest CHANGE `statusnew` `status` enum('Pending','Approved','Rejected','Cancelled')";
		$this->pdo->exec ($query);
		
		if (! $this->checkColumnExists("main_compoff_summary","compoffstatusnew")) {
			$query = "ALTER TABLE `main_compoff_summary` ADD COLUMN `compoffstatusnew` enum('Pending','Approved','Rejected','Cancelled')";
			$this->pdo->exec ( $query );
		}
		
		$query = "UPDATE main_compoff_summary SET compoffstatusnew = compoffstatus+0 where compoffstatus != ''";
		$this->pdo->exec ($query);
		
		$query = "ALTER TABLE main_compoff_summary DROP compoffstatus";
		$this->pdo->exec ($query);
		
		$query = "ALTER TABLE main_compoff_summary CHANGE `compoffstatusnew` `compoffstatus` enum('Pending','Approved','Rejected','Cancelled')";
		$this->pdo->exec ($query);
		
		if (! $this->checkColumnExists("main_exceptionrequest","exceptionstatusnew")) {
			$query = "ALTER TABLE `main_exceptionrequest` ADD COLUMN `exceptionstatusnew` enum('Pending','Approved','Rejected','Cancelled','Expired')";
			$this->pdo->exec ( $query );
		}
		
		$query = "UPDATE main_exceptionrequest SET exceptionstatusnew = exceptionstatus+0 where exceptionstatus != ''";
		$this->pdo->exec ($query);
		
		$query = "ALTER TABLE main_exceptionrequest DROP exceptionstatus";
		$this->pdo->exec ($query);
		
		$query = "ALTER TABLE main_exceptionrequest CHANGE `exceptionstatusnew` `exceptionstatus` enum('Pending','Approved','Rejected','Cancelled','Expired')";
		$this->pdo->exec ($query);
		
		if (! $this->checkColumnExists("main_exceptionrequest_summary","exceptionstatusnew")) {
			$query = "ALTER TABLE `main_exceptionrequest_summary` ADD COLUMN `exceptionstatusnew` enum('Pending','Approved','Rejected','Cancelled','Expired')";
			$this->pdo->exec ( $query );
		}
		
		$query = "UPDATE main_exceptionrequest_summary SET exceptionstatusnew = exceptionstatus+0 where exceptionstatus != ''";
		$this->pdo->exec ($query);
		
		$query = "ALTER TABLE main_exceptionrequest_summary DROP exceptionstatus";
		$this->pdo->exec ($query);
		
		$query = "ALTER TABLE main_exceptionrequest_summary CHANGE `exceptionstatusnew` `exceptionstatus` enum('Pending','Approved','Rejected','Cancelled','Expired')";
		$this->pdo->exec ($query); 
		
		$query = "Select count(*) as count from main_menu Where menuName = 'Roles & Privileges'";
		$result = $this->pdo->query ( $query )->fetch();
		if($result['count'] != 0){
		    $query = "Select id  from main_menu Where menuName = 'Roles & Privileges'";
		    $result = $this->pdo->query ( $query )->fetch();
		    $roles = "Select count(*) as count from main_privileges Where role is NULL and group_id = '3' and object = '". $result['id'] ."' ";
		    $rolesResult = $this->pdo->query ( $roles )->fetch();
		    if($rolesResult['count'] == 0){
		        $query = "INSERT INTO `main_privileges` (`group_id`, `object`, `addpermission`, `editpermission`, `deletepermission`, `viewpermission`, `uploadattachments`, `viewattachments`, `createdby`, `modifiedby`, `createddate`,`modifieddate`, `isactive`)
                VALUES ('3', '". $result['id'] ."', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', '1', '1', NOW(),NOW(),'1')";
		        $this->pdo->exec($query);
		    }		    
		}
		
    }
}