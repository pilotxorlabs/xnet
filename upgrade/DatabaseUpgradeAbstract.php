<?php

abstract class DatabaseUpgrader_Abstract
{
    protected $majorVersion;
    protected $minorVersion;
    protected $maintVersion;
    protected $nextUpgrader;
    protected $pdo;
    
    protected abstract function updateSchema();
    protected abstract function executeQueries();

    public final function addVersion(){
        $query = "insert  into main_patches_version(version,createddate,modifieddate,isactive) values ('".$this->getVersion()."',NOW(),NOW(),1)";
        $this->pdo->exec($query);
    }
    
    public function getNextUpgrader(){
        return $this->nextUpgrader;
    }
    
    public function getVersion(){
        return $this->majorVersion . "." . $this->minorVersion . "." . $this->maintVersion;
    }
    
    public function setPdo($pdo){
        $this->pdo = $pdo;
    }
    
    public final function upgrade(){
        try{
            if(!$this->pdo){
                $this->pdo = new PDO('mysql:host='.SENTRIFUGO_HOST.';dbname='.SENTRIFUGO_DBNAME.'',SENTRIFUGO_USERNAME, SENTRIFUGO_PASSWORD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, array(PDO::ATTR_PERSISTENT => true)));
            }
            $this->pdo->beginTransaction();
            $this->updateSchema();
            $this->executeQueries();
            $this->addVersion();
            
            $this->pdo->commit();
            if($this->nextUpgrader != null){
                $this->nextUpgrader->pdo = $this->pdo;
                return $this->nextUpgrader->upgrade();
            }
        }catch(Exception $ex){
            $this->pdo->rollback();
            throw new Exception("Error occured while trying to upgrade database to " . $this->getVersion());
        }
        return $this->getVersion();
    }
    
    public function checkColumnExists($tableName,$colName){
        $query = "SELECT * FROM information_schema.columns
				WHERE table_name = '".$tableName."'
				AND column_name = '".$colName."'
                AND table_schema = '".SENTRIFUGO_DBNAME."';";
        $rowCountObject = $this->pdo->query ( $query );
        $rowCount = $rowCountObject->fetchAll ();
        if ($rowCount == null || count ( $rowCount ) == 0 || $rowCount [0] == null) {
            return false;
        }
        return true;
    }
    
    public function checkColumnValueExists($tableName,$colName, $colValue){
    	$query = "SELECT * FROM ".$tableName."
				WHERE ".$colName." = '".$colValue."';";
    	$rowCountObject = $this->pdo->query ( $query );
    	$rowCount = $rowCountObject->fetchAll ();
    	if ($rowCount == null || count ( $rowCount ) == 0 || $rowCount [0] == null) {
    		return false;
    	}
    	return true;
    }
    
}

